# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kpartsaver VERSION\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-06-28 20:03JST\n"
"Last-Translator: Taiki Komoda <kom@kde.gr.jp>\n"
"Language-Team: Japanese <Kdeveloper@kde.gr.jp>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: kpartsaver.cpp:62
msgid "KPart Screensaver"
msgstr "KPartスクリーンセーバ"

#: kpartsaver.cpp:122
msgid "The screensaver isn't configured yet"
msgstr "スクリーンセーバはまだ設定されていません"

#: kpartsaver.cpp:266
msgid "All of your files are unsupported"
msgstr "あなたの全てのファイルはサポートされていません"

#: kpartsaver.cpp:349
msgid "Select media files"
msgstr "メディアファイルを選択"

#: rc.cpp:1
msgid "Media Screensaver"
msgstr "メディアスクリーンセーバ"

#: rc.cpp:3
msgid "&Down"
msgstr "下に(&D)"

#: rc.cpp:4
msgid "&Up"
msgstr "上に(&U)"

#: rc.cpp:5
msgid "&Add"
msgstr "追加(&A)"

#: rc.cpp:7
msgid "Settings"
msgstr "設定"

#: rc.cpp:8
msgid "Only show one randomly chosen medium"
msgstr "ランダムに選択したメディアのみを表示"

#: rc.cpp:9
msgid "Switch to another medium after a delay"
msgstr "一定時間のあとに他のメディアに変更"

#: rc.cpp:10
msgid "Delay:"
msgstr "遅延時間:"

#: rc.cpp:11
msgid "Choose next medium randomly"
msgstr "次のメディアをランダムに選択"

#: rc.cpp:12
msgid "seconds"
msgstr "秒"
