# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: libkwinquartz_config VERSION\n"
"POT-Creation-Date: 2001-07-12 18:37+0200\n"
"PO-Revision-Date: 2001-07-22 23:39GMT\n"
"Last-Translator: Noboru Sinohara <shinobo@leo.bekkoame.ne.jp>\n"
"Language-Team:  <ja@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2\n"

#: config.cpp:38
msgid "Decoration Settings"
msgstr "装飾の設定"

#: config.cpp:40
msgid "Draw window frames using &titlebar colors"
msgstr "ウィンドウのフレームにタイトルバー色を使う(&t)"

#: config.cpp:42
msgid ""
"When selected, the window decoration borders are drawn using the titlebar "
"colors. Otherwise, they are drawn using normal border colors instead."
msgstr ""
"選択されていると、ウィンドウ装飾枠はタイトルバー色"
"を使って描かれます. "
"そうでない場合、代わりに標準の枠の色が使われます. "
