msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-08-20 15:11GMT-0300\n"
"Last-Translator: E. A. Tacão <tacao@conectiva.com.br>\n"
"Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: kio_nfs.cpp:986
msgid "An RPC error occurred."
msgstr "Ocorreu um erro de RPC."

#: kio_nfs.cpp:1030
msgid "No space left on device"
msgstr "Não há mais espaço no dispositivo"

#: kio_nfs.cpp:1033
msgid "Read only file system"
msgstr "Sistema de arquivos somente leitura"

#: kio_nfs.cpp:1036
msgid "Filename too long"
msgstr "O nome do arquivo é muito longo"

#: kio_nfs.cpp:1043
msgid "Disk quota exceeded"
msgstr "Excedida a quota de disco"
