# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-01-06 01:29+0100\n"
"PO-Revision-Date: 2000-10-30 16:34GMT\n"
"Last-Translator: Iñaki Ibarrola Atxa <etxaurre@teleline.es>\n"
"Language-Team: Euskara <linux-eu@chanae.alphanet.ch>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.6\n"

#: kbbgame.cpp:101
msgid "Score: 0000"
msgstr "Puntuazioa: 0000"

#: kbbgame.cpp:103
msgid "Placed: 00 / 00"
msgstr "Jarritak: 00 / 00"

#: kbbgame.cpp:105
msgid "Run: yesno"
msgstr "Abiarazi: baiez"

#: kbbgame.cpp:107
msgid "Size: 00 x 00"
msgstr "Neurria: 00 x 00"

#: kbbgame.cpp:308 kbbgame.cpp:430
msgid "Do you really want to give up this game?"
msgstr "Benetan utzi nahi duzu joko hau?"

#: kbbgame.cpp:361
msgid ""
"Your final score is: %1\n"
"You did really well!"
msgstr ""
"Zure puntuazioa: %1\n"
"Oso ondo egin duzu!"

#: kbbgame.cpp:364
msgid ""
"Your final score is: %1\n"
"I guess you need more practice."
msgstr ""
"Zure puntuazioa: %1\n"
"Uste dut praktika gehiago behar duzula."

#: kbbgame.cpp:370
msgid ""
"You should place %1 balls!\n"
"You have placed %2."
msgstr ""
"%1 bola jarri behar duzu!\n"
"%2 jarri dituzu."

#: kbbgame.cpp:447
msgid "Run: "
msgstr "Abiarazi: "

#: kbbgame.cpp:453
msgid "Size: "
msgstr "Neurria: "

#: kbbgame.cpp:457
msgid "Placed: "
msgstr "Jarritak: "

#: kbbgame.cpp:470
#, c-format
msgid "Score: %1"
msgstr "Puntuazioa :%1"

#: kbbgame.cpp:484 kbbgame.cpp:512
msgid "This will be the end of the current game!"
msgstr "Hau oraingo joakoaren bukaera izango da!"

#: kbbgame.cpp:741
#, fuzzy
msgid "&Give Up"
msgstr "&Utzi"

#: kbbgame.cpp:742
msgid "&Done"
msgstr "&Egina"

#: kbbgame.cpp:743
msgid "&Resize"
msgstr "Neu&rria aldatu"

#: kbbgame.cpp:748
msgid "&Size"
msgstr "&Neurria"

#: kbbgame.cpp:750
msgid "  8 x  8 "
msgstr ""

#: kbbgame.cpp:751
msgid " 10 x 10 "
msgstr ""

#: kbbgame.cpp:752
msgid " 12 x 12 "
msgstr ""

#: kbbgame.cpp:755
msgid "&Balls"
msgstr "&Bolak"

#: kbbgame.cpp:757
msgid " 4 "
msgstr ""

#: kbbgame.cpp:758
msgid " 6 "
msgstr ""

#: kbbgame.cpp:759
msgid " 8 "
msgstr ""

#: kbbgame.cpp:761
msgid "&Tutorial"
msgstr "&Tutoretza"

#: main.cpp:21
msgid "KDE Blackbox Game"
msgstr "KDE Blackbox Jokoa"

#: main.cpp:29
msgid "KBlackBox"
msgstr "KBlackBox"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#~ msgid ""
#~ "KBlackBox logical game\n"
#~ "author: Robert Cimrman\n"
#~ "e-mail: cimrman3@students.zcu.cz"
#~ msgstr ""
#~ "KBlackBox joko logikoa\n"
#~ "egilea: Robert Cimrman\n"
#~ "e-mail: cimrman3@students.zcu.cz"

#~ msgid "&New"
#~ msgstr "&Berria"

#~ msgid "Exit"
#~ msgstr "Atera"

#~ msgid "New"
#~ msgstr "Berria"

#~ msgid "Give up"
#~ msgstr "Etsi"

#~ msgid "Done"
#~ msgstr "Egina"
