# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 16:05+0200\n"
"PO-Revision-Date: 2001-05-23 00:50EET\n"
"Last-Translator: Andris Maziks <andris.m@delfi.lv>\n"
"Language-Team: Latvian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: kdebugdialog.cpp:47 klistdebugdialog.cpp:34
msgid "Debug Settings"
msgstr "Atkļūdošanas Uzstādījumi"

#: kdebugdialog.cpp:52
msgid "Debug area:"
msgstr "Atkļūdošanas zona:"

#: kdebugdialog.cpp:68
msgid "Message Box"
msgstr "Ziņojumu Bokss"

#: kdebugdialog.cpp:69
msgid "Shell"
msgstr "Shells"

#: kdebugdialog.cpp:70
msgid "Syslog"
msgstr "Syslogs"

#: kdebugdialog.cpp:71
msgid "None"
msgstr "Nav"

#: kdebugdialog.cpp:80 kdebugdialog.cpp:105 kdebugdialog.cpp:130
#: kdebugdialog.cpp:155
msgid "Output to:"
msgstr "Izvads uz:"

#: kdebugdialog.cpp:87 kdebugdialog.cpp:112 kdebugdialog.cpp:137
#: kdebugdialog.cpp:162
msgid "Filename:"
msgstr "Failavārds:"

#: kdebugdialog.cpp:151
msgid "Fatal error"
msgstr "Fatāla kļūda"

#: kdebugdialog.cpp:174
msgid "Abort on fatal errors"
msgstr "Aborts pie fatālām kļūdām"

#: main.cpp:72
msgid "Show the fully-fledged dialog instead of the default list dialog."
msgstr "Rādīt pilnīgi-slīdošu dialogu  noklusētā saraksta dialoga vietā."

#: main.cpp:80
msgid "KDebugDialog"
msgstr "KDebugDialogs"

#: main.cpp:81
msgid "A dialog box for setting preferences for debug output"
msgstr "Dialoga logs atkļūdošanas izvades opciju uzstādīšanai"

#: main.cpp:83
msgid "Maintainer"
msgstr "Uzturētājs"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Andris Maziks"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "andris.m@delfi.lv"
