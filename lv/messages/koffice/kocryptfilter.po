# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-08-22 18:56+0200\n"
"PO-Revision-Date: 2001-08-25 23:48EEST\n"
"Last-Translator: Andris Maziks <andzha@latnet.lv>\n"
"Language-Team: Latvian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: kocryptexport.cc:152 kocryptimport.cc:143
msgid "There was an internal error preparing the passphrase."
msgstr "Iekšēja kļūda sagatavojot paroles frāzi."

#: kocryptexport.cc:153 kocryptexport.cc:168 kocryptexport.cc:263
msgid "Encrypted Document Export"
msgstr "Šifrēta Dokumenta Eksports"

#: kocryptexport.cc:167
msgid "There was an internal error preparing the cipher."
msgstr "Iekšēja kļūda sagatavojot šifru."

#: kocryptexport.cc:262
msgid "Internal error writing file.  Please file a bug report."
msgstr "Iekšēja faila rakstīšanas kļūda.  Lūdzu sūtiet kļūdas ziņojumu."

#: kocryptimport.cc:144 kocryptimport.cc:156 kocryptimport.cc:174
#: kocryptimport.cc:188 kocryptimport.cc:202 kocryptimport.cc:323
msgid "Encrypted Document Import"
msgstr "Šifrēta Dokumenta Imports"

#: kocryptimport.cc:155
msgid "There was an internal error in the cipher code."
msgstr "Iekšēja kļūda šifra kodā."

#: kocryptimport.cc:173
msgid "This is the wrong document type or the document is corrupt."
msgstr "Šis ir nepareizs dokumenta tips vai dokuments ir bojāts."

#: kocryptimport.cc:187 kocryptimport.cc:201
msgid "I don't understand this version of the file format."
msgstr "Es nesaprotu šīs versijas faila formātu."

#: kocryptimport.cc:322
msgid "This document is either corrupt or has been tampered with."
msgstr "Šis dokuments ir vai nu bojāts vai ticis saspiests ar."

#: pwdprompt.cc:50
msgid "Enter the passphrase for the document:"
msgstr "Ievadiet paroles frāzi dokumentam:"

#: pwdprompt.cc:52
msgid "Enter a passphrase for the document:"
msgstr "Ievadiet paroles frāzi dokumentam:"

#: pwdprompt.cc:54
msgid "&Ok"
msgstr "&Ok"
