# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-05-25 02:20+0200\n"
"PO-Revision-Date: 2001-05-23 02:04EET\n"
"Last-Translator: Andris Maziks <andris.m@delfi.lv>\n"
"Language-Team: Latvian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: main.cpp:48
msgid "lan:/ and rlan:/ configuration"
msgstr "lan:/ un rlan:/ konfigurācija"

#: main.cpp:52 main.cpp:65
msgid "LISa daemon"
msgstr "LISa dēmons"

#: main.cpp:60
msgid ""
"<center>The LAN Information Server LISa can be configured only by the "
"<b>system administrator (root)</b>.<br><br> Login as root or <i>su</i> to "
"root and come back again :-)</center>"
msgstr ""
"<center>LAN Informācijas Serveri LISa var konfigurēt tikai <b>sistēmas "
"administrators (root)</b>.<br><br> Piesakieties kā root vai <i>su</i> lai "
"root un nāk atkal atpakaļ :-)</center>"

#: main.cpp:67
msgid "ResLISa daemon"
msgstr "ResLISa dēmons"

#: main.cpp:103
msgid ""
"<h1>LAN Browsing</h1>Here you setup your <b>\"Network Neighbourhood\"</b>, "
"you can use either the LISa daemon and the lan:/ ioslave or the ResLISa "
"daemon and the rlan:/ ioslave.<br><br>About the <b>LAN ioslave</b> "
"configuration:<br> If you select <i>if available</i> the ioslave will check "
"whether the host supports this service when you open this host. Please note "
"that paranoia people might consider even this an attack.<br><i>Always</i> "
"means that you will always see the links for the services no matter whether "
"they are actually offered by the host. <i>Never</i> means that you will "
"never have the links to the services. In both cases you won't contact the "
"host, so that nobody might ever consider you an attacker.<br><br>More "
"information about <b>LISa</b> can be found at  <a "
"href=\"http://lisa-home.sourceforge.net\">the LISa Homepage</a> or contact "
"Alexander Neundorf &lt;<a "
"href=\"mailto:neundorf@kde.org\">neundorf@kde.org</a>&gt;."
msgstr ""
"<h1>LAn Pārlūkošana</h1>Šeit jūs uzstādat jūsu <b>\"Tīkla "
"Kaimiņus\"</b>, jūs varat izmantot LISa dēmonu un lan:/ iovergu vai "
"ResLISa dēmonu un rlan:/ iovergu.<br><br>Par <b>LAN iovergu</b> "
"konfigurāciju:<br> Ja jūs izvēlaties <i>ja piejams</i> iovergs "
"pārbaudīs vai resursdators atbalsta šo servisu kad jūs atverat šo "
"resursdatoru. Lūdzu atcerieties, ka paranoiski cilvēki to var uztvert kā "
"uzbrukumu.<br><i>Vienmēr</i> nozīmē, ka jūs vienmēr redzat saitus uz "
"servisiem neatkarīgi no tā vai tos resursdators reāli piedāvā. "
"<i>Nekad</i> nozīmē, ka jums nekad nebūs saites uz servisiem. Abos "
"gadījumos, jūs nekontaktējieties ar resursdatoru, līdz ar to neviens "
"nevar uzskatīt jūs par uzbrucēju.<br><br>Vairāk informācijas par "
"<b>LISa</b> jūs varat atrast http://lisa-home.sourceforge.net. vai "
"kontaktējoties ar  Alexander Neundorf &lt;neundorf@kde.org&gt;"

#: kcmlisa.cpp:37 kcmreslisa.cpp:35
msgid "Autosetup..."
msgstr "Autouzstādīšana..."

#: kcmlisa.cpp:40 kcmreslisa.cpp:38
msgid "Send NetBIOS broadcasts using nmblookup for searching"
msgstr "Sūtīt NetBIOS pārraides izmantojot nmblookup meklēšanai"

#: kcmlisa.cpp:41 kcmreslisa.cpp:40
msgid "only hosts running smb servers will answer"
msgstr "tikai resursdatori ar darbojošamies smb serveriem atbildēs"

#: kcmlisa.cpp:46
msgid "Scan these addresses"
msgstr "Skanēt šīs adreses"

#: kcmlisa.cpp:47 kcmlisa.cpp:49
msgid "enter ranges or whole subnets to ping, see README"
msgstr "ievadiet diapazonus, vai visus apakštīklus, lai pingotu, skatīt LASIMANI"

#: kcmlisa.cpp:52 kcmreslisa.cpp:42
msgid "Additionally check the following hosts"
msgstr "Papildus pārbaudīt sekojošus resursdatorus"

#: kcmlisa.cpp:53 kcmreslisa.cpp:44
msgid "The names of the hosts you want to check"
msgstr "pārbaudāmo resursdatoru vārdi"

#: kcmlisa.cpp:58 kcmreslisa.cpp:49
msgid "Allowed addresses"
msgstr "Atļautās adreses"

#: kcmlisa.cpp:59 kcmlisa.cpp:61 kcmreslisa.cpp:50 kcmreslisa.cpp:52
msgid "usually your network address/subnet mask (e.g. 192.168.0.0/255.255.255.0;)"
msgstr "parasti jūsu tīkla adrese/subneta maska (t.i. 192.168.0.0/255.255.255.0;)"

#: kcmlisa.cpp:66
msgid "Broadcast network address"
msgstr "Pārraides tīkla adrese"

#: kcmlisa.cpp:67 kcmlisa.cpp:69
msgid "your network address/subnet mask (e.g. 192.168.0.0/255.255.255.0;)"
msgstr "jūsu tīkla adrese/subneta maska (t.i. 192.168.0.0/255.255.255.0;)"

#: kcmlisa.cpp:73 kcmreslisa.cpp:55
msgid "Advanced settings"
msgstr "Advancētie rekvizīti"

#: kcmlisa.cpp:75 kcmreslisa.cpp:57
msgid "Report unnamed hosts"
msgstr "Ziņot nenosauktus resursdatorus"

#: kcmlisa.cpp:76 kcmreslisa.cpp:58
msgid "report hosts without DNS names"
msgstr "ziņot resursdatorus bez DNS vārdiem"

#: kcmlisa.cpp:79 kcmreslisa.cpp:61
msgid "Always scan twice"
msgstr "Vienmēr skanēt divreiz"

#: kcmlisa.cpp:80
msgid "check the hosts twice"
msgstr "pārbaudīt resursdatorus divreiz"

#: kcmlisa.cpp:85
msgid "Update interval"
msgstr "Atjaunināšanas intervāls"

#: kcmlisa.cpp:86 kcmlisa.cpp:89 kcmreslisa.cpp:68 kcmreslisa.cpp:71
msgid "search hosts after this number of seconds"
msgstr "meklēt resursdatorus pēc šī sekunžu skaita"

#: kcmlisa.cpp:88 kcmreslisa.cpp:70
msgid " sec"
msgstr " sek"

#: kcmlisa.cpp:94 kcmreslisa.cpp:77
msgid "Wait for replies after first scan"
msgstr "Gaidīt atbildes pēc pirmās skanēšanas"

#: kcmlisa.cpp:95 kcmlisa.cpp:98 kcmlisa.cpp:112 kcmlisa.cpp:115
#: kcmreslisa.cpp:78 kcmreslisa.cpp:81 kcmreslisa.cpp:95 kcmreslisa.cpp:98
msgid "how long to wait for replies to the ICMP echo requests from hosts"
msgstr "cik ilgi gaidīt uz ICMP eho pieprasījumiem no resursdatoriem atbildi"

#: kcmlisa.cpp:97 kcmlisa.cpp:114 kcmreslisa.cpp:80 kcmreslisa.cpp:97
msgid " 1/100 sec"
msgstr " 1/100 sek"

#: kcmlisa.cpp:103 kcmreslisa.cpp:86
msgid "Send pings at once"
msgstr "Sūtīt pingus vienlaicīgi"

#: kcmlisa.cpp:104 kcmlisa.cpp:106 kcmreslisa.cpp:87 kcmreslisa.cpp:89
msgid "send this number of ping packets at once"
msgstr "vienlaicīgi sūtīt šo pinga pakešu skaitu"

#: kcmlisa.cpp:111
msgid "Wait for replies after second scan"
msgstr "Gaidīt atbildes pēc otrās skanēšanas"

#: kcmlisa.cpp:200 kcmreslisa.cpp:179
msgid ""
"Sorry, it seems you don't have any network interfaces installed on your "
"system."
msgstr ""
"Atvainojiet, izskatās, ka jūsu sistēmā nav instalēta neviena tīkla "
"starpseja."

#: kcmlisa.cpp:240
msgid ""
"The LISa daemon is now configured correctly, hopefully.<br>"
"Make sure that it is started with root privileges. A good idea would be to "
"start it when your system boots. (<code>lisa --kde2</code>)"
msgstr ""
"LISa dēmons cerams, tagad ir konfigurēts korekti.<br>Pārliecinaties, ka "
"tas ir startēts ar root privilēģijām. Laba ideja varētu būt startēt "
"to pi jūsu sistēmas sāknēšanas. (<code>lisa --kde2</code>)"

#: kcmlisa.cpp:246 kcmreslisa.cpp:208
msgid ""
"You have more than one network interface installed, I can't                  "
"      setup automatically.<br><br>I found the following:<br><br>"
msgstr ""
"Jums ir instalēta vairāk par vienu tīkla starpseju, Es nevaru             "
"           uzstādīt automātiski.<br><br>Es atradu sekojošo:<br><br>"

#: kcmreslisa.cpp:67
msgid "Update interval:"
msgstr "Atjaunināšanas intervāls:"

#: kcmreslisa.cpp:94
msgid "Wait for replies after second scan "
msgstr "Gaidīt atbildes pēc otrās skanēšanas "

#: kcmreslisa.cpp:202
msgid ""
"The ResLISa daemon is now configured correctly, hopefully.<br>"
"Make sure that the reslisa binary is installed <i>suid root</i>."
msgstr ""
"ResLISa dēmons cerams tagad ir konfigurēts korekti.<br>"
"Pārliecinaties, ka reslisa binārija ir instalēta <i>suid root</i>."

#: kcmkiolan.cpp:41
msgid "Provide FTP service links (TCP, port 21)"
msgstr "Sniedz FTP servisa saiti (TCP, ports 21)"

#: kcmkiolan.cpp:42
msgid "Provide HTTP service links (TCP, port 80)"
msgstr "Sniedz HTTP servisa saiti (TCP, ports 80)"

#: kcmkiolan.cpp:43
msgid "Provide NFS service links (TCP, port 2049)"
msgstr "Sniedz NFS servisa saiti (TCP, ports 2049)"

#: kcmkiolan.cpp:44
msgid "Provide SMB service links (TCP, port 139)"
msgstr "Sniedz SMB servisa saiti (TCP, ports 139)"

#: kcmkiolan.cpp:45
msgid "Show only the hostname (without the domain suffix)"
msgstr "Rādīt tikai resursdatora vārdu (bez domēna sufiksu)"

#: kcmkiolan.cpp:47
msgid "use KDE 1 style configuration file search"
msgstr "izmantot KDE 1 stila konfigurācijas failu meklēšanu"

#: portsettingsbar.cpp:27
msgid "if available"
msgstr "ja pieejams"

#: portsettingsbar.cpp:28
msgid "always"
msgstr "vienmēr"

#: portsettingsbar.cpp:29
msgid "never"
msgstr "nekad"
