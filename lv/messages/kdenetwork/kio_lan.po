# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-08-22 18:51+0200\n"
"PO-Revision-Date: 2001-04-27 20:58EET\n"
"Last-Translator: Andris Maziks <andris.m@delfi.lv>\n"
"Language-Team: Latvian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: kio_lan.cpp:188 kio_lan.cpp:206 kio_lan.cpp:385
#, c-format
msgid "Received unexpected data from %1"
msgstr "Saņemti neparedzēti dati no %1"

#: kio_lan.cpp:642
msgid "No hosts allowed in rlan:/ url"
msgstr "Rlan:/ url resursdatori nav atļauti"

#~ msgid ""
#~ "\n"
#~ "Your LAN Browsing is not yet configured.\n"
#~ "Go to the KDE Control Center and open Network->LAN Browsing\n"
#~ "and configure the settings you will find there."
#~ msgstr ""
#~ "\n"
#~ "Jūsu LAN Pārlūkošana pašlaik nav konfigurēta.\n"
#~ "Ejiet uz KDE Vadības Centru un atveriet Tīkls->LAN Pārlūkošana\n"
#~ "un konfigurējiet šeit atrodamos uzstādījumus."
