# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-07-02 17:49+0200\n"
"PO-Revision-Date: 2001-08-20 16:32GMT+0700\n"
"Last-Translator: Thanomsub Noppaburana <donga_n@hotmail.com>\n"
"Language-Team: Thai\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: buttons.cpp:128 buttons.cpp:242
msgid "Menu"
msgstr "เมนู"

#: buttons.cpp:129 buttons.cpp:244
msgid "Sticky"
msgstr "ปุ่มยึด"

#: buttons.cpp:130 buttons.cpp:246
msgid "Spacer"
msgstr "ช่องว่าง"

#: buttons.cpp:132 buttons.cpp:250
msgid "Minimize"
msgstr "ย่อเล็กสุด"

#: buttons.cpp:133 buttons.cpp:252
msgid "Maximize"
msgstr "ขยายใหญ่สุด"

#: buttons.cpp:553
msgid "KDE"
msgstr ""

#: kwindecoration.cpp:68
msgid "Window Decoration"
msgstr "รูปแบบหน้าต่าง"

#: kwindecoration.cpp:70
msgid ""
"Select the window decoration. This is the look and feel of both the window "
"borders and the window handle."
msgstr ""
"เลือกรูปแบบหน้าต่าง "
"เพื่อให้แสดงกับทั้งกรอบหน"
"้าต่าง "
"และส่วนที่เป็นหน้าต่าง"

#: kwindecoration.cpp:75
msgid "General options (if available)"
msgstr "ตัวเลือกทั่วไป (ถ้ามี)"

#: kwindecoration.cpp:77
msgid "Use custom titlebar button &positions"
msgstr ""
"ใช้ตำแหน่งปุ่มหัวเรื่องหน"
"้าต่างที่กำหนดเอง"

#: kwindecoration.cpp:79
msgid ""
"The appropriate settings can be found in the \"Buttons\" Tab. Please note "
"that this option is not available on all styles yet!"
msgstr ""
"ส่วนการตั้งค่านี้ "
"จะสามารถพบได้ในแท็บ \"ปุ่ม\" "
"โปรดจำไว้ว่า "
"ตัวเลือกนี้ยังไม่สามารถใช"
"้กับรูปแบบทั้งหมดได้ !"

#: kwindecoration.cpp:82
msgid "&Show window button tooltips"
msgstr ""
"แสดงทูลทิบของปุ่มบนหน้าต่"
"าง"

#: kwindecoration.cpp:84
msgid ""
"Enabling this checkbox will show window button tooltips. If this checkbox is "
"off, no window button tooltips will be shown."
msgstr ""
"เปิดใช้ตัวเลือกนี้ "
"เพื่อให้แสดงทูลทิปของปุ่ม"
"บนหน้าต่าง หากไม่เปิดใช้ "
"จะไม่แสดงทูลทิปของปุ่มบนห"
"น้าต่าง"

#: kwindecoration.cpp:96
msgid "Titlebar Button Position"
msgstr ""
"ตำแหน่งปุ่มหัวเรื่องหน้าต"
"่าง"

#: kwindecoration.cpp:101
msgid ""
"To add or remove titlebar buttons, simply <i>drag</i> items between the "
"available item list and the titlebar preview. Similarly, drag items within "
"the titlebar preview to re-position them."
msgstr ""
"เพื่อเพิ่ม หรือลบ "
"ปุ่มบนหัวเรื่องหน้าต่าง "
"ทำได้ง่ายๆ โดยใช้การ <i>ลาก</i> "
"จากที่มีในรายการ "
"และที่อยู่บนหัวเรื่องหน้า"
"ต่างตัวอย่าง "

#: kwindecoration.cpp:119
msgid "&General"
msgstr "ทั่วไป"

#: kwindecoration.cpp:120
msgid "&Buttons"
msgstr "ปุ่ม"

#: kwindecoration.cpp:121 kwindecoration.cpp:205
msgid "&Configure ["
msgstr "ปรับแต่ง ["

#: kwindecoration.cpp:122 kwindecoration.cpp:206
msgid "]"
msgstr ""

#: kwindecoration.cpp:187
msgid "KDE2 default"
msgstr "ค่าปริยาย KDE2"

#: kwindecoration.cpp:313
msgid ""
"<H3>No Configurable Options Available</H3>"
"Sorry, no configurable options are available for the currently selected "
"decoration."
msgstr ""
"ขออภัย "
"<H3>ไม่มีตัวเลือกปรับแต่ง</H3> "
"ไม่มีตัวเลือกปรับแต่ง "
"สำหรับรูปแบบที่เลือกอยู่ใ"
"นปัจจุบัน"

#: kwindecoration.cpp:463
msgid ""
"<h1>Window Manager Decoration</h1>This module allows you to choose the "
"window border decorations, as well as titlebar button positions and custom "
"decoration options."
msgstr ""
"<h1>"
"ตัวจัดการรูปแบบหน้าต่าง</h1> "
"โมดูลนี้ ให้คุณสามารถเลือก "
"รูปแบบหน้าต่างต่างๆ ได้ "
"รวมถึงการปรับแต่งตำแหน่งป"
"ุ่มหัวเรื่องหน้าต่าง "
"และตัวเลือกอื่นๆ "
"ของรูปแบบหน้าต่างได้"
