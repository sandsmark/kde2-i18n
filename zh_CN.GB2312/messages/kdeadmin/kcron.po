# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Wang Jian <lark@linux.net.cn>, 2000
#
#
msgid ""
msgstr ""
"Project-Id-Version: kcron\n"
"POT-Creation-Date: 2001-06-01 12:38+0200\n"
"PO-Revision-Date: 2001-01-09 23:10+0800\n"
"Last-Translator: Wang Jian <lark@linux.net.cn>\n"
"Language-Team: zh_CN <i18n-translation@lists.marsec.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ctcron.cpp:61
msgid "(System Crontab)"
msgstr "(系统 Crontab)"

#: ctmonth.cpp:34
msgid "every month "
msgstr "每月 "

#: ctmonth.cpp:51
msgid "January"
msgstr "一月"

#: ctmonth.cpp:51
msgid "February"
msgstr "二月"

#: ctmonth.cpp:52
msgid "March"
msgstr "三月"

#: ctmonth.cpp:52
msgid "April"
msgstr "四月"

#: ctmonth.cpp:53
msgid ""
"_: May long\n"
"May"
msgstr "五月"

#: ctmonth.cpp:53
msgid "June"
msgstr "六月"

#: ctmonth.cpp:54
msgid "July"
msgstr "七月"

#: ctmonth.cpp:54
msgid "August"
msgstr "八月"

#: ctmonth.cpp:55
msgid "September"
msgstr "九月"

#: ctmonth.cpp:55
msgid "October"
msgstr "十月"

#: ctmonth.cpp:56
msgid "November"
msgstr "十一月"

#: ctmonth.cpp:56
msgid "December"
msgstr "十二月"

#: ctdom.cpp:35 ctdow.cpp:60 cttask.cpp:287
msgid "every day "
msgstr "每天 "

#: ctdom.cpp:52
msgid "1st"
msgstr "1"

#: ctdom.cpp:52
msgid "2nd"
msgstr "2"

#: ctdom.cpp:53
msgid "3rd"
msgstr "3"

#: ctdom.cpp:53
msgid "4th"
msgstr "4"

#: ctdom.cpp:54
msgid "5th"
msgstr "5"

#: ctdom.cpp:54
msgid "6th"
msgstr "6"

#: ctdom.cpp:55
msgid "7th"
msgstr "7"

#: ctdom.cpp:55
msgid "8th"
msgstr "8"

#: ctdom.cpp:56
msgid "9th"
msgstr "9"

#: ctdom.cpp:56
msgid "10th"
msgstr "10"

#: ctdom.cpp:57
msgid "11th"
msgstr "11"

#: ctdom.cpp:57
msgid "12th"
msgstr "12"

#: ctdom.cpp:58
msgid "13th"
msgstr "13"

#: ctdom.cpp:58
msgid "14th"
msgstr "14"

#: ctdom.cpp:59
msgid "15th"
msgstr "15"

#: ctdom.cpp:59
msgid "16th"
msgstr "16"

#: ctdom.cpp:60
msgid "17th"
msgstr "17"

#: ctdom.cpp:60
msgid "18th"
msgstr "18"

#: ctdom.cpp:61
msgid "19th"
msgstr "19"

#: ctdom.cpp:61
msgid "20th"
msgstr "20"

#: ctdom.cpp:62
msgid "21st"
msgstr "21"

#: ctdom.cpp:62
msgid "22nd"
msgstr "22"

#: ctdom.cpp:63
msgid "23rd"
msgstr "23"

#: ctdom.cpp:63
msgid "24th"
msgstr "24"

#: ctdom.cpp:64
msgid "25th"
msgstr "25"

#: ctdom.cpp:64
msgid "26th"
msgstr "26"

#: ctdom.cpp:65
msgid "27th"
msgstr "27"

#: ctdom.cpp:65
msgid "28th"
msgstr "28"

#: ctdom.cpp:66
msgid "29th"
msgstr "29"

#: ctdom.cpp:66
msgid "30th"
msgstr "30"

#: ctdom.cpp:67
msgid "31st"
msgstr "31"

#: ctdow.cpp:62
msgid "weekday "
msgstr "一周内天 "

#: ctdow.cpp:80
msgid "Mon"
msgstr "星期一"

#: ctdow.cpp:80
msgid "Tue"
msgstr "星期二"

#: ctdow.cpp:81
msgid "Wed"
msgstr "星期三"

#: ctdow.cpp:81
msgid "Thu"
msgstr "星期四"

#: ctdow.cpp:82
msgid "Fri"
msgstr "星期五"

#: ctdow.cpp:82
msgid "Sat"
msgstr "星期六"

#: ctdow.cpp:83
msgid "Sun"
msgstr "星期日"

#: cttask.cpp:220
msgid "Translators: See README.translators!"
msgstr "OK, done"

#: cttask.cpp:222
#, fuzzy
msgid "%H:%M"
msgstr "%H:%M"

#: cttask.cpp:223
msgid "DAYS_OF_MONTH of MONTHS"
msgstr "MONTHS DAYS_OF_MONTH"

#: cttask.cpp:224
msgid "every DAYS_OF_WEEK"
msgstr "每个 DAYS_OF_WEEK"

#: cttask.cpp:225
msgid "DOM_FORMAT as well as DOW_FORMAT"
msgstr "DOM_FORMAT 以及 DOW_FORMAT"

#: cttask.cpp:226
msgid "At TIME"
msgstr "在 TIME"

#: cttask.cpp:227
msgid "TIME_FORMAT, DATE_FORMAT"
msgstr "DATE_FORMAT 的 TIME_FORMAT"

#: cttask.cpp:273
msgid ", and "
msgstr "，和 "

#: cttask.cpp:275
msgid " and "
msgstr " 和 "

#: cttask.cpp:277
msgid ", "
msgstr "，"

#: ktapp.cpp:52 ktapp.cpp:451
msgid "Task Scheduler"
msgstr "任务调度器"

#: ktapp.cpp:90
msgid ""
"You can use this application to schedule programs to run in the background.\n"
"To schedule a new task now, click on the Tasks folder and select Edit/New "
"from the menu."
msgstr ""
"您可以使用这个程序来调度在后台运行的程序。如果要调"
"度一个新的\n"
"任务，在‘任务’夹上单击，然后选择菜单中的 "
"编辑/新建。"

#: ktapp.cpp:90
msgid "Welcome to the Task Scheduler"
msgstr "欢迎使用任务调度器"

#: ktapp.cpp:135
msgid "&Print"
msgstr "打印(&P)"

#: ktapp.cpp:141
msgid "Cu&t"
msgstr "剪切(&T)"

#: ktapp.cpp:145
msgid "&New..."
msgstr "新建(&N)..."

#: ktapp.cpp:146
msgid "M&odify..."
msgstr "修改(&O)..."

#: ktapp.cpp:149 kttask.cpp:104 ktvariable.cpp:73
msgid "&Enabled"
msgstr "启用(&E)"

#: ktapp.cpp:151
msgid "&Run Now"
msgstr "现在运行(&R)"

#: ktapp.cpp:198 ktapp.cpp:335 ktapp.cpp:342 ktapp.cpp:361 ktapp.cpp:368
#: ktapp.cpp:375 ktapp.cpp:382 ktapp.cpp:389 ktapp.cpp:396 ktapp.cpp:413
#: ktapp.cpp:420 ktapp.cpp:432 ktapp.cpp:444
msgid "Ready."
msgstr "就绪。"

#: ktapp.cpp:299
msgid ""
"Scheduled tasks have been modified.\n"
"Do you want to save changes?"
msgstr ""
"调度的任务已经被修改了。\n"
"您要保存吗？"

#: ktapp.cpp:333
msgid "Saving..."
msgstr "正在保存..."

#: ktapp.cpp:340
msgid "Printing..."
msgstr "正在打印..."

#: ktapp.cpp:358
msgid "Cutting to clipboard..."
msgstr "正在剪切到剪贴板..."

#: ktapp.cpp:366
msgid "Copying to clipboard..."
msgstr "正在复制到剪贴板..."

#: ktapp.cpp:373
msgid "Pasting from clipboard..."
msgstr "正在从剪贴板粘贴..."

#: ktapp.cpp:380
msgid "Adding new entry..."
msgstr "正在添加新的项目..."

#: ktapp.cpp:387
msgid "Modifying entry..."
msgstr "正在修改项目..."

#: ktapp.cpp:394
msgid "Deleting entry..."
msgstr "正在删除项目..."

#: ktapp.cpp:403
msgid "Disabling entry..."
msgstr "正在禁用项目..."

#: ktapp.cpp:409
msgid "Enabling entry..."
msgstr "正在启用项目..."

#: ktapp.cpp:418
msgid "Running command..."
msgstr "正在运行命令..."

#: ktapp.cpp:514
msgid "Save tasks and variables."
msgstr "保存任务和变量。"

#: ktapp.cpp:517
msgid "Print all or current crontab(s)."
msgstr "打印所有或当前的 crontab。"

#: ktapp.cpp:520
#, c-format
msgid "Exit %1."
msgstr "退出 %1。"

#: ktapp.cpp:524
msgid "Cut the selected task or variable and put on the clipboard."
msgstr "剪切选中的任务或变量并放置在剪贴板。"

#: ktapp.cpp:527
msgid "Copy the selected task or variable to the clipboard."
msgstr "把选中的任务或变量复制到剪贴板。"

#: ktapp.cpp:530
msgid "Paste task or variable from the clipboard."
msgstr "从剪贴板上粘贴任务和变量。"

#: ktapp.cpp:533
msgid "Create a new task or variable."
msgstr "建立一个新的任务或变量。"

#: ktapp.cpp:536
msgid "Edit the selected task or variable."
msgstr "编辑选中的任务或变量。"

#: ktapp.cpp:539
msgid "Delete the selected task or variable."
msgstr "删除选中的任务或变量。"

#: ktapp.cpp:542
msgid "Enable/disable the selected task or variable."
msgstr "启用/禁用选中的任务或变量。"

#: ktapp.cpp:545
msgid "Run the selected task now."
msgstr "运行选中的任务。"

#: ktapp.cpp:549
msgid "Enable/disable the tool bar."
msgstr "启用/禁用工具条。"

#: ktapp.cpp:552
msgid "Enable/disable the status bar."
msgstr "启用/禁用状态条。"

#: ktview.cpp:82
msgid "Users/Tasks/Variables"
msgstr "用户/任务/变量"

#: ktview.cpp:84
msgid "Tasks/Variables"
msgstr "任务/变量"

#: ktview.cpp:86
msgid "Value"
msgstr "值"

#: ktview.cpp:87
msgid "Description"
msgstr "描述"

#: ktview.cpp:253
msgid ""
"_: user on host\n"
"%1 <%2> on %3"
msgstr "%1 <%2> 在 %3"

#: ktview.cpp:261
msgid "Scheduled Tasks"
msgstr "已调度的任务"

#: ktlisttask.cpp:45 ktlistvar.cpp:45
msgid "Disabled"
msgstr "已禁用"

#: ktlisttask.cpp:59 ktlistvar.cpp:68
msgid "Disabled."
msgstr "已禁用。"

#: ktlisttask.cpp:64
#, fuzzy
msgid "Modify Task"
msgstr "编辑任务"

#: ktlisttasks.cpp:42
msgid "Tasks"
msgstr "任务"

#: ktlisttasks.cpp:48
msgid "Edit Task"
msgstr "编辑任务"

#: ktlisttasks.cpp:75
msgid "Task Name:"
msgstr "任务名："

#: ktlisttasks.cpp:76
msgid "Program:"
msgstr "程序："

#: ktlisttasks.cpp:77 ktlistvars.cpp:72
msgid "Description:"
msgstr "描述："

#: ktlisttasks.cpp:86
msgid "No tasks..."
msgstr "没有任务..."

#: ktlistvar.cpp:74
#, fuzzy
msgid "Modify Variable"
msgstr "编辑变量"

#: ktlistvars.cpp:43
msgid "Edit Variable"
msgstr "编辑变量"

#: ktlistvars.cpp:70
msgid "Variable:"
msgstr "变量："

#: ktlistvars.cpp:71
msgid "Value:"
msgstr "值："

#: ktlistvars.cpp:81
msgid "No variables..."
msgstr "没有变量..."

#: ktlistvars.cpp:98
msgid "Variables"
msgstr "变量"

#: kttask.cpp:47
msgid "&Run as:"
msgstr "运行为(&R)："

#: kttask.cpp:73
msgid "&Comment:"
msgstr "注释(&C)："

#: kttask.cpp:85
msgid "&Program:"
msgstr "程序(&P)："

#: kttask.cpp:100
msgid "&Browse..."
msgstr "浏览(&B)..."

#: kttask.cpp:109
#, fuzzy
msgid "&Silent"
msgstr "打印(&P)"

#: kttask.cpp:118
msgid "Months"
msgstr "月"

#: kttask.cpp:137
msgid "Days of the Month"
msgstr "月中的天"

#: kttask.cpp:165
msgid "Days of the Week"
msgstr "周中的天"

#: kttask.cpp:184
msgid "Daily"
msgstr "每天"

#: kttask.cpp:190
msgid "Run Every Day"
msgstr "每天运行"

#: kttask.cpp:195
msgid "Hours"
msgstr "小时"

#: kttask.cpp:200
msgid "AM"
msgstr "AM"

#: kttask.cpp:223
msgid "PM"
msgstr "PM"

#: kttask.cpp:236
msgid "Minutes"
msgstr "分钟"

#: kttask.cpp:402
msgid ""
"Please enter the following to schedule the task:\n"
msgstr ""
"请输入以下内容来调度任务：\n"

#: kttask.cpp:408
msgid "the program to run"
msgstr "要运行的程序"

#: kttask.cpp:420
msgid "the months"
msgstr "月份"

#: kttask.cpp:441
msgid "either the days of the month or the days of the week"
msgstr "月中的天或星期中的天"

#: kttask.cpp:457
msgid "the hours"
msgstr "小时"

#: kttask.cpp:473
msgid "the minutes"
msgstr "分钟"

#: kttask.cpp:501
#, fuzzy
msgid "Cannot locate program.  Please re-enter."
msgstr "找不到程序。请重新输入。"

#: kttask.cpp:509
msgid "Program is not an executable file.  Please re-enter."
msgstr "程序不是一个可执行文件。请重新输入。"

#: kttask.cpp:577
msgid "Only local or mounted files can be executed by crontab."
msgstr "Crontab只能执行本地或安装的文件。"

#: ktvariable.cpp:37
msgid "&Variable:"
msgstr "变量(&V)："

#: ktvariable.cpp:55
msgid "Va&lue:"
msgstr "值(&L)："

#: ktvariable.cpp:64
msgid "Co&mment:"
msgstr "注释(&M)："

#: ktvariable.cpp:106
msgid "Override default home directory."
msgstr "替代默认的主目录。"

#: ktvariable.cpp:111
msgid "Email output to specified account."
msgstr "把输出 Email 给特定的账号。"

#: ktvariable.cpp:116
msgid "Override default shell."
msgstr "替代默认的 shell。"

#: ktvariable.cpp:121
msgid "Directories to search for program files."
msgstr "搜索程序文件的目录。"

#: ktvariable.cpp:133
msgid "Please enter the variable name."
msgstr "请输入变量名。"

#: ktvariable.cpp:140
msgid "Please enter the variable value."
msgstr "请输入变量值。"

#: ktprintopt.cpp:27
msgid "Cron Options"
msgstr ""

#: ktprintopt.cpp:31
msgid "Print Cron&tab"
msgstr "打印 Cron&tab"

#: ktprintopt.cpp:34
msgid "Print &All Users"
msgstr "打印所有用户(&A)"

#: main.cpp:26
msgid "KDE Task Scheduler"
msgstr "KDE 任务调度器"

#: main.cpp:28
msgid "KCron"
msgstr "KCron"

#: main.cpp:53
msgid "KCron fatal error: Unable to read or write file."
msgstr "KCron 严重错误：无法读或写文件。"

#: main.cpp:58
msgid "KCron fatal error: Unknown."
msgstr "KCron 严重错误：未知。"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Wang Jian"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "lark@linux.net.cn"
