# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <kursat@linux.org.tr>, 1999.
# 
msgid ""
msgstr ""
"Project-Id-Version: kpoker\n"
"POT-Creation-Date: 2001-04-19 01:40+0200\n"
"PO-Revision-Date: 2000-10-14 17:57+0200\n"
"Last-Translator: Muhsin Yeşilyılmaz <muhsinyy@hotmail.com>\n"
"Language-Team: Turkish Translation Team <kde-i18n-tr@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-9\n"
"Content-Transfer-Encoding: 8-bit\n"

#: kpoker.cpp:283
msgid "Clicking on draw means you adjust your bet"
msgstr "Çık üzerine tıkladığınızda koyduğunuz parayı ayarlarsınız."

#: kpoker.cpp:285
msgid "Clicking on draw means you are out"
msgstr "Çık üzerine tıkladığınızda oyundan çıkmış olursunuz."

#: kpoker.cpp:327 kpoker.cpp:546 somestuff.cpp:78
msgid "&Draw!"
msgstr "Çı&k!"

#: kpoker.cpp:383 newgamedlg.cpp:114
msgid "You"
msgstr "Sen"

#: kpoker.cpp:425 kpoker.cpp:464
msgid "Nobody"
msgstr "Hiçkimse"

#: kpoker.cpp:431 kpoker.cpp:466 kpoker.cpp:685 kpoker.cpp:697 kpoker.cpp:885
msgid "Nothing"
msgstr "Hiç"

#: kpoker.cpp:456
#, c-format
msgid "Pot: %1"
msgstr "Pot: %1"

#: kpoker.cpp:590 somestuff.cpp:90
#, c-format
msgid "You won %1"
msgstr "%1 kazandın!"

#: kpoker.cpp:597
msgid "%1 won %2"
msgstr "%1 %2 kazandı"

#: kpoker.cpp:656
msgid "&See!"
msgstr "&Gördüm!"

#: kpoker.cpp:687
msgid "One Pair"
msgstr "Per"

#: kpoker.cpp:688
msgid "Two Pairs"
msgstr "Döper"

#: kpoker.cpp:689
msgid "3 of a kind"
msgstr "Üçlü"

#: kpoker.cpp:690
msgid "Full House"
msgstr "Full"

#: kpoker.cpp:691
msgid "4 of a kind"
msgstr "Kare"

#: kpoker.cpp:692
msgid "Straight"
msgstr "Renk"

#: kpoker.cpp:693
msgid "Flush"
msgstr "Kent"

#: kpoker.cpp:694
msgid "Straight Flush"
msgstr "Floş"

#: kpoker.cpp:695
msgid "Royal Flush"
msgstr "Floş Royal"

#: kpoker.cpp:709
msgid "You Lost"
msgstr "Kaybettin"

#: kpoker.cpp:709
msgid "Oops, you went bankrupt.\nStarting a new game.\n"
msgstr ""
"Hoppaa - beş parasız kaldın.\n"
"Yeni oyuna başlanıyor.\n"

#: kpoker.cpp:767
msgid "You won %1!"
msgstr "%1 kazandın!"

#: kpoker.cpp:770
msgid "Game Over"
msgstr "Oyun Bitti"

#: kpoker.cpp:876
msgid "You are the only player with money!\nSwitching to one player rules..."
msgstr ""
"Sen cebinde parası olan tek oyuncusun!\n"
"Bir oyuncu kurallarına geçiliyor..."

#: kpoker.cpp:877
msgid "You Won"
msgstr "Kazandın"

#: top.cpp:81
msgid "Soun&d"
msgstr "&Ses"

#: top.cpp:85
msgid "&Blinking Cards"
msgstr "&Yanıp Sönen kartlar"

#: top.cpp:89
msgid "&Adjust bet is default"
msgstr "&Görülen öntanımlı"

#: top.cpp:178
msgid "Do you want to save this game?"
msgstr "Bu oyunu kaydetmek istiyor musunuz?"

#: top.cpp:192
msgid "Last hand: "
msgstr "Son el: "

#: top.cpp:194
msgid "Last winner: "
msgstr "Son kazanan: "

#: top.cpp:202
msgid "Click a card to hold it"
msgstr "Kartı tutmak için üzerine tıklayın"

#: main.cpp:25
msgid "KDE Card Game"
msgstr "KDE İskambil Oyunu"

#: main.cpp:29
msgid "KPoker"
msgstr "KPoker"

#: main.cpp:32
msgid "For a full list of credits see helpfile\nAny suggestions, bug reports etc. are welcome"
msgstr "Öneriler, arıza bildirimleri kabul edilir."

#: main.cpp:35
msgid "Current maintainer"
msgstr "Mevcut programcılar"

#: main.cpp:37
msgid "Original author"
msgstr "Ana programcı"

#: somestuff.cpp:49
msgid "Draw"
msgstr "Çık"

#: somestuff.cpp:51
msgid "Exchange Card 1"
msgstr "1. Kartı Değiştir"

#: somestuff.cpp:53
msgid "Exchange Card 2"
msgstr "2. Kartı Değiştir"

#: somestuff.cpp:55
msgid "Exchange Card 3"
msgstr "3. Kartı Değiştir"

#: somestuff.cpp:57
msgid "Exchange Card 4"
msgstr "4. Kartı Değiştir"

#: somestuff.cpp:59
msgid "Exchange Card 5"
msgstr "5. Kartı Değiştir"

#: somestuff.cpp:128
msgid "draw new cards"
msgstr "yeni kart çek"

#: somestuff.cpp:129
msgid "the current pot"
msgstr "mevcut pot"

#: betbox.cpp:50
msgid "Adjust Bet"
msgstr "Bet Ayarla"

#: betbox.cpp:51
msgid "Fold"
msgstr "Çevir"

#: optionsdlg.cpp:44
msgid "All changes will be activated in the next round"
msgstr "Tüm değişiklikler sonraki oyunda etkinleştirilecektir"

#: optionsdlg.cpp:51
msgid "Maximal bet:"
msgstr "En çok bet:"

#: optionsdlg.cpp:55
msgid "Minimal bet:"
msgstr "En az bet:"

#: playerbox.cpp:54
msgid "Held"
msgstr "Tutuldu"

#: playerbox.cpp:83 playerbox.cpp:122
#, c-format
msgid "Money of %1"
msgstr "%1'in parası"

#: playerbox.cpp:103
#, c-format
msgid "Cash: %1"
msgstr "Nakit: %1"

#: playerbox.cpp:108
#, c-format
msgid "Bet: %1"
msgstr "Görme: %1"

#: playerbox.cpp:111
#, c-format
msgid "Cash per round: %1"
msgstr "Oyun başına para: %1"

#: playerbox.cpp:115
msgid "Out"
msgstr "Dışarı"

#: newgamedlg.cpp:48
msgid "Try loading a game"
msgstr "Bir oyun yüklemeyi dene"

#: newgamedlg.cpp:52
msgid "The following values are used if loading from config fails"
msgstr ""
"Eğer dosyadan yükleme başarısız olursa aşağıdaki değerler kullanılır"

#: newgamedlg.cpp:61
msgid "How many players do you want?"
msgstr "Kaç oyuncu istersiniz?"

#: newgamedlg.cpp:65
msgid "Your Name"
msgstr "Adınız"

#: newgamedlg.cpp:70
msgid "Start money of the players"
msgstr "Oyuncuların başlangıç paraları"

#: newgamedlg.cpp:75
msgid "The names of your enemies"
msgstr "Düşmanlarının isimleri"

#: newgamedlg.cpp:81
msgid "Show this dialog every time on startup"
msgstr "Açılışta bu pencereyi göster"

#: newgamedlg.cpp:117
#, c-format
msgid "Computer %1"
msgstr "Bilgisayar %1"

#: newgamedlg.cpp:163
msgid "Player"
msgstr "Oyuncu"

#: _translatorinfo.cpp:1
msgid "_: NAME OF TRANSLATORS\nYour names"
msgstr "Görkem Çetin"

#: _translatorinfo.cpp:3
msgid "_: EMAIL OF TRANSLATORS\nYour emails"
msgstr "gorkem@kde.org"
