# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-07-22 17:17+0200\n"
"Last-Translator: Ričardas Čepas <rch@richard.eu.org>\n"
"Language-Team: Lithuanian <kde-i18n-lt@master.kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8.1\n"

#: kio_help.cpp:133
msgid ""
"<html>The requested help file could not be found. Check that you have "
"installed the documentation.</html>"
msgstr ""
"<html>"
"Negaliu rasti pagalbos bylos, kurios prašėte.  Patikrinkite, ar jūs "
"įdiegėte dokumentaciją</html>"

#: kio_help.cpp:157
msgid "Looking up correct file"
msgstr "Ieškau teisingos bylos"

#: kio_help.cpp:195
msgid "Preparing document"
msgstr "Paruošiu dokumentą"

#: kio_help.cpp:206
msgid "Saving to cache"
msgstr "Išsaugau į kešą"

#: kio_help.cpp:215
msgid "Using cached version"
msgstr "Naudoju kešo versiją"

#: kio_help.cpp:223
msgid "The requested help file could not be parsed:"
msgstr "Paprašyta pagalbos byla negali būti išnagrinėta:"

#: kio_help.cpp:268
msgid "Looking up section"
msgstr "Ieškau skyriaus"

#: kio_help.cpp:279
msgid "<html>Couldn't find filename %1 in %2</html>"
msgstr "<html>Nerandu bylos %1 esančios %2</html>"

#: meinproc.cpp:72 xml2man.cpp:32
msgid "Stylesheet to use"
msgstr "Naudoti stilių lentelę"

#: meinproc.cpp:73 xml2man.cpp:33
msgid "output whole document to stdout"
msgstr "išvesti visą dokumentą į stdout"

#: meinproc.cpp:74
msgid "create a ht://dig compatible index"
msgstr "sukurti ht://dig suderinamą rodyklę"

#: meinproc.cpp:75
msgid "create a cache file for the document"
msgstr "sukurti kešo bylą dokumentui"

#: meinproc.cpp:76 xml2man.cpp:34
msgid "The file to transform"
msgstr "Byla transformavimui"

#: meinproc.cpp:85
msgid "XML-Translator"
msgstr "XML vertėjas"

#: meinproc.cpp:87 xml2man.cpp:45
msgid "KDE Translator for XML"
msgstr "KDE vertėjas skirtas XML"

#: meinproc.cpp:164
#, c-format
msgid "Couldn't write to cache file %1."
msgstr "Negaliu rašyti į kešo bylą %1."

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Ričardas Čepas"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "rch@richard.eu.org"

#: xml2man.cpp:43
msgid "XML2MAN Processor"
msgstr "XML2MAN procesorius"

#: xslt.cpp:39
msgid "Reading document"
msgstr "Skaitau dokumentą"

#: xslt.cpp:49
msgid "XMLize document"
msgstr "XMLize dokumentas"

#: xslt.cpp:63
msgid "Parsing document"
msgstr "Nagrinėju dokumentą"

#: xslt.cpp:79
msgid "Parsing stylesheet"
msgstr "Nagrinėju stilių lentelę"

#: xslt.cpp:108
msgid "Applying stylesheet"
msgstr "Pritaikau stilių lentelę"

#: xslt.cpp:114
msgid "Writing document"
msgstr "Rašau dokumentą"
