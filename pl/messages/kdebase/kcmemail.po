msgid ""
msgstr ""
"Project-Id-Version: kcmemail\n"
"POT-Creation-Date: 2001-07-23 12:10+0200\n"
"PO-Revision-Date: 2001-07-24 21:58+0200\n"
"Last-Translator: Jacek Stolarczyk <jacek@mer.chemia.polsl.gliwice.pl>\n"
"Language-Team: Polish <pl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.4\n"

#: email.cpp:155
msgid "New Email Profile"
msgstr "Nowy profil pocztowy"

#: email.cpp:162
msgid "Name:"
msgstr "Nazwa:"

#: email.cpp:185
msgid "Oops, you need to enter a name please. Thanks."
msgstr "Ojej, trzeba podać nazwę. Dziękuję."

#: email.cpp:187
msgid "This email profile already exists, and cannot be created again"
msgstr "Profil o takiej nazwie już istnieje i nie może być ponownie utworzony"

#: email.cpp:187
msgid "Oops"
msgstr "Ojej"

#: email.cpp:264
msgid ""
"<h1>email</h1> This module allows you to enter basic email information for "
"the current user. The information here is used, among other things, for "
"sending bug reports to the KDE developers when you use the bug report "
"dialog.<p> Note that email programs like KMail and Empath offer many more "
"features, but they provide their own configuration facilities."
msgstr ""
"<h1>poczta</h1> Moduł pozwala podać podstawowe informacje o poczcie dla "
"bieżącego użytkownika. Podane tu informacje są używane przy wysyłaniu "
"raportów o błędach do programistów KDE, gdy używane jest okienko "
"dialogowe do wysyłania raportu.<p> Zwróć uwagę, że programy pocztowe "
"jak KMail czy Empath mogą oferować wiele dodatkowych opcji, ale do nich "
"używają osobnej konfiguracji."

#: email.cpp:290
msgid "Do you wish to discard changes to the current profile?"
msgstr "Chcesz porzucić zmiany w bieżącym profilu?"

#: email.cpp:291
msgid "Do you wish to save changes to the current profile?"
msgstr "Chcesz zapisać zmiany w bieżącym profilu?"

#: rc.cpp:1
msgid "Email"
msgstr "e-mail"

#: rc.cpp:2
msgid "C&urrent Profile:"
msgstr "&Bieżący profil:"

#: rc.cpp:3
msgid "&New Profile..."
msgstr "&Nowy profil..."

#: rc.cpp:4
msgid "User Information"
msgstr "Informacja o użytkowniku"

#: rc.cpp:5
msgid ""
"You can set a reply address if you want replies to your email messages to go "
"to a different address than the email address above. Most likely, you should "
"leave the reply address blank, so replies go to the email address you "
"entered above.<p><em> Please note:</em> <ul><li>"
"You do not need to enter the same email address as above.</li><li>"
"You should not use a reply address if you frequently use discussion mailing "
"lists.</li></ul>"
msgstr ""
"Można podać adres do odpowiedzi, jeżeli chcesz otrzymywać odpowiedzi na "
"swoje listy na inny adres niż ten podany powyżej. W większości "
"przypadków należy jednak pozostawić to pole puste, by odpowiedzi mogły "
"trafić na podany wyżej adres.<p> <em>Zwróć uwagę, że:</em> <ul><li>"
"Nie trzeba podawać tu tego samego adresu co powyżej.</li><li>Nie powinno "
"się używać odrębnego adresu do odpowiedzi, jeśli często korzysta się "
"z list dyskusyjnych.</li></ul>"

#: rc.cpp:6
msgid ""
"Enter your email address here, e.g. \"john@doe.com\" (without the quotation "
"marks). This information is mandatory if you want to use email.<p>\n"
"Do <em>not</em>"
" enter something like John Doe &lt;john@doe.com&gt;\", just a plain email "
"address. Your email address must not contain any blank spaces."
msgstr ""
"Podaj swój adres e-mail, np. \"kowalski@firma.pl\" (bez cudzysłowów). "
"Wypełnienie pola jest konieczne by móc używać poczty.<p>\n"
"<em>Nie</em> należy wpisywać adresu w formacie \"Jan Kowalski "
"&lt;kowalski@firma.pl&gt;\", po prostu podać sam adres. Adres nie może "
"zawierać odstępów (spacji, tabulacji, itp.)."

#: rc.cpp:7
msgid "Organi&zation:"
msgstr "Organi&zacja:"

#: rc.cpp:8
msgid "Reply-&To Address:"
msgstr "Adres do o&dpowiedzi:"

#: rc.cpp:9
msgid ""
"Here you can enter the name of your organization, company or university. "
"This field is <em>optional</em>. However, if you are using a business "
"account and communicate with persons working for other companies, providing "
"the name of your organization is recommended."
msgstr ""
"Podaj tutaj nazwę swojej organizacji, instytucji, firmy bądź uczelni. To "
"pole jest <em>opcjonalne</em>. Jednak używając konta w przedsiębiorstwie "
"i utrzymując kontakt z pracownikami innych przedsiębiorstw, podanie nazwy "
"swojego jest zalecane."

#: rc.cpp:10
msgid "&Email Address:"
msgstr "&Adres e-mail:"

#: rc.cpp:11
msgid ""
"Enter your full name here, e.g. \"John Doe\" (without the quotation\n"
"marks).  Some people like to provide a nick name only. You can leave this "
"field blank and still use email. However, providing your full name is "
"<em>recommended</em> as this makes it much easier for your recipient to "
"browse his or her email."
msgstr ""
"Podaj swoje imię i nazwisko, np. \"Jan Kowalski\" (bez cudzysłowów). "
"Niektórzy ludzie preferują podanie jedynie przezwiska. Pole to można "
"zostawić puste i mimo tego dalej używać poczty. Podanie pełnego imienia "
"i nazwiska jest jednak <em>zalecane</em>, gdyż bardzo ułatwia odbiorcom "
"przeglądanie poczty."

#: rc.cpp:12
msgid "&Full Name:"
msgstr "&Imię i nazwisko:"

#: rc.cpp:13
msgid "Preferred Email Client"
msgstr "Ulubiony program pocztowy"

#: rc.cpp:14
msgid ""
"Enter the path to your preferred email client (KMail, Mutt, etc.) here or "
"choose it with the <em>Browse...</em> button. If no client is specified "
"here, KMail will be used (if available) instead.<p/> You can also use "
"several placeholders which will be replaced with the following "
"values:<p/><ul><li>%s: Subject</li><li>%c: Carbon Copy (CC)</li><li>%b: "
"Blind Carbon Copy</li><li>%B: Body template</li><li>%t: Recipient's "
"address</li></ul>"
msgstr ""
"Podaj w tym polu ścieżkę do wybranego programu pocztowego (KMail, mutt, "
"itd.) lub naciśnij przycisk <em>Przeglądaj...</em>. Jeśli nie zostanie "
"podany żaden program pocztowy, wtedy użyty zostanie KMail (o ile jest "
"dostępny).<p/> Można także użyć kilku pól, które będą podstawione "
"przez następujące wartości:<p/><ul><li>%s: Temat</li><li>%c: Kopia do "
"(Cc)</li><li>%b: Ukryta kopia do (Bcc)</li><li>%B: Wzorzec "
"treści</li><li>%t: Adres odbiorcy</li></ul>"

#: rc.cpp:15
msgid ""
"Press this button to select your favorite email client. Please note that the "
"file you select has to have the executable attribute set in order to be "
"accepted.<br> You can also use several placeholders which will be replaced "
"with the actual values when the email client is called:<ul> <li>%t: "
"Recipient's address</li> <li>%s: Subject</li> <li>%c: Carbon Copy (CC)</li> "
"<li>%b: Blind Carbon Copy (BCC)</li> <li>%B: Template body text</li> <li>"
"%A: Attachment </li> </ul>"
msgstr ""
"Naciśnij ten przycisk by wybrać program pocztowy. Zwróć uwagę, że "
"wybrany plik musi być plikem wykonywalnym, by mógł zostać "
"zaakceptowany.<br> Można także podac kilka pól, które zostana "
"podstawione przez właściwe wartości, gdy program pocztowy zostanie "
"wywołany:<ul> <li>%t: Adres odbiorcy</li> <li>%s: Temat</li> <li>%c: Kopia "
"do (CC)</li> <li>%b: Ukryta kopia do (BCC)</li> <li>%B: Wzorzec treści "
"wiadomości</li> <li>%A: Załącznik </li> </ul>"

#: rc.cpp:16
msgid "&Run In Terminal"
msgstr "&Uruchom w terminalu"

#: rc.cpp:17
msgid ""
"Activate this option if you want the selected email client to be executed in "
"a terminal (e.g. <em>Konsole</em>)."
msgstr ""
"Włącz tę opcję jeśli chcesz by wybrany program pocztowy został "
"uruchomiony w terminalu (t.j. np. <em>Konsola</em>)."
