# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: libkwinmodernsys\n"
"POT-Creation-Date: 2001-07-12 18:37+0200\n"
"PO-Revision-Date: 2001-07-16 20:57GMT\n"
"Last-Translator: Marcin Giedz <mgiedz@elka.pw.edu.pl>\n"
"Language-Team: Polish <pl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: config.cpp:28
msgid "Decoration Settings"
msgstr "Ustawienia wystroju"

#: config.cpp:29
msgid "&Show resize handle"
msgstr "&Pokaż uchwyt zmiany rozmiaru"

#: config.cpp:30
msgid ""
"When selected, all windows are drawn with a resize handle at the lower right "
"corner."
msgstr ""
"Kiedy jest zaznaczone, wszystkie okna rysowane są z uchwytem zmiany "
"rozmiaru w prawym, dolnym rogu."
