# Esperantaj mesaĝoj por "kabalone"
# Copyright (C) 1998 Free Software Foundation, Inc.
# Wolfram Diestel <diestel@rzaix340.rz.uni-leipzig.de>, 1998.
#
#
msgid ""
msgstr ""
"Project-Id-Version: kabalone\n"
"POT-Creation-Date: 2001-06-10 17:19+0200\n"
"PO-Revision-Date: 2001-01-08 19:46GMT\n"
"Last-Translator: Wolfram Diestel <wolfram@steloj.de>\n"
"Language-Team: Esperanto <eo@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.7\n"

#: rc.cpp:4
msgid "&Level"
msgstr "&Nivelo"

#: rc.cpp:5
msgid "&Computer plays"
msgstr "Komputilo &ludas"

#: AbTop.cpp:268 rc.cpp:8
msgid "Configure Evaluation..."
msgstr "Agordu poentadon..."

#: rc.cpp:9
msgid "Moves"
msgstr "Movoj"

#: rc.cpp:10
msgid "Push Out"
msgstr "Elŝovo"

#: Move.cpp:85 rc.cpp:11
msgid "Push"
msgstr "Ŝovo"

#: rc.cpp:12
msgid "Normal"
msgstr "Normala"

#: rc.cpp:13
msgid "For every move possible the given points are added to the Evaluation."
msgstr "Por ĉiu eble movo la donitaj poentoj estas aldonataj al la sumo."

#: rc.cpp:14
msgid "Position"
msgstr "Pozicio"

#: rc.cpp:15
msgid "Inner Ring 3"
msgstr "Interna ringo 3"

#: rc.cpp:16
msgid "Outer most Ring"
msgstr "Plej ekstera ringo"

#: rc.cpp:17
msgid "Middle Position"
msgstr "Meza pozicio"

#: rc.cpp:18
msgid "Inner Ring 2"
msgstr "Interna ringo 2"

#: rc.cpp:19 rc.cpp:20 rc.cpp:21 rc.cpp:23
msgid "+/-"
msgstr "+/-"

#: rc.cpp:22
msgid "Inner most Ring"
msgstr "Plej interna ringo"

#: rc.cpp:24
msgid ""
"For every ball, the given points are added to the evaluation depending on "
"the balls position. The bonus for a given position is changed randomly in "
"the +/- range."
msgstr ""
"Por ĉiu pilko la donitaj poentoj estas aldonataj al la sumo depende de la "
"pilka pozicio. La krompoentoj por certa pozicio arbitre varias en la +/- "
"intervalo."

#: rc.cpp:25
msgid "In-A-Row"
msgstr "En unu linio"

#: rc.cpp:26
msgid "Three In-A-Row"
msgstr "Tri en unu linio"

#: rc.cpp:27
msgid "Two In-A-Row"
msgstr "Du en unu linio"

#: rc.cpp:28
msgid "Four In-A-Row"
msgstr "Kvar en unu linio"

#: rc.cpp:29
msgid "Five In-A-Row"
msgstr "Kvin en unu linio"

#: rc.cpp:30
msgid "For a number of balls In-a-Row, the given points are added to the evaluation"
msgstr ""
"Por nombro da pilkoj en unu linio, la donitaj poentoj estas aldonataj al la "
"sumo."

#: rc.cpp:31
msgid "Count"
msgstr "Nombro"

#: rc.cpp:32
msgid "4 Balls more"
msgstr "4 pliaj pilkoj"

#: rc.cpp:33
msgid "3 Balls more"
msgstr "3 pliaj pilkoj"

#: rc.cpp:34
msgid "5 Balls more"
msgstr "5 pliaj pilkoj"

#: rc.cpp:35
msgid "2 Balls more"
msgstr "2 pliaj pilkoj"

#: rc.cpp:36
msgid "1 Ball more"
msgstr "1 plia pilko"

#: rc.cpp:37
msgid ""
"For a difference in the number of balls, the given points are added to the "
"evaluation. A difference of 6 only can be a lost/won game."
msgstr ""
"Por diferenco en la nombro da pilkoj, la donitaj poentoj estas aldonataj al "
"la sumo. Diferenco de nur 6 povas gajnigi aŭ malgajnigi la ludon."

#: rc.cpp:38
msgid "Evaluation Schemes"
msgstr "Poentadagordoj"

#: rc.cpp:39
msgid "Save as ..."
msgstr "Sekurigu kiel..."

#: rc.cpp:41
msgid ""
"Here your Evaluation Scheme, defined in all other tabs of this dialog, can "
"be stored."
msgstr ""
"Jen vi sekurigas vian poentadoagordon difinitan en la aliaj paĝoj de tiu "
"ĉi dialogo."

#: rc.cpp:42
msgid "Evaluation of actual Position:"
msgstr "Poentado de nuna pozicio:"

#: Move.cpp:22
msgid "RightDown"
msgstr "Dekstre-Malsupren"

#: Move.cpp:23
msgid "LeftDown"
msgstr "Maldekstre-Malsupren"

#: Move.cpp:25
msgid "LeftUp"
msgstr "Maldekstre-Supren"

#: Move.cpp:26
msgid "RightUp"
msgstr "Dekstre-Supren"

#: Move.cpp:84
msgid "Out"
msgstr "Eksteren"

#: BoardWidget.cpp:885 BoardWidget.cpp:926 BoardWidget.cpp:957
#, c-format
msgid "Board value: %1"
msgstr "Rezulto: %1"

#: AbTop.cpp:129
msgid "&Stop Search"
msgstr "Ĉ&esu serĉi"

#: AbTop.cpp:132
msgid "Take &Back"
msgstr "&Reprenu"

#: AbTop.cpp:136
msgid "&Forward"
msgstr "&Antaŭen"

#: AbTop.cpp:140
msgid "&Hint"
msgstr "&Propono"

#: AbTop.cpp:146
msgid "&Restore Position"
msgstr "&Restarigu situacion"

#: AbTop.cpp:151
msgid "&Save Position"
msgstr "&Sekurigu situacion"

#: AbTop.cpp:158
msgid "&Network Play"
msgstr "&Retludado"

#: AbTop.cpp:176
msgid "&Move Slow"
msgstr "Mo&vu malrapide"

#: AbTop.cpp:180
msgid "&Render Balls"
msgstr "&Pentru pilkojn"

#: AbTop.cpp:184
msgid "&Spy"
msgstr "&Observu"

#: AbTop.cpp:191
msgid "&Easy"
msgstr "&Facila"

#: AbTop.cpp:197
msgid "&Normal"
msgstr "&Normala"

#: AbTop.cpp:203
msgid "&Hard"
msgstr "&Malfacila"

#: AbTop.cpp:209
msgid "&Challange"
msgstr "&Defia"

#: AbTop.cpp:215
msgid "&Red"
msgstr "&Ruĝan"

#: AbTop.cpp:221
msgid "&Yellow"
msgstr "&Flavan"

#: AbTop.cpp:227
msgid "&Both"
msgstr "&Ambaŭ"

#: AbTop.cpp:233
msgid "&None"
msgstr "&Neniun"

#: AbTop.cpp:567 AbTop.cpp:671
msgid "Press %1 for a new game"
msgstr "Premu %1 por komenci ludon"

#: AbTop.cpp:592 AbTop.cpp:651 AbTop.cpp:655
#, c-format
msgid "Move %1"
msgstr "Movo %1"

#: AbTop.cpp:611 Spy.cpp:79
msgid "Spy"
msgstr "Observu"

#: AbTop.cpp:664 AbTop.cpp:684
msgid "Red"
msgstr "Ruĝa"

#: AbTop.cpp:665 AbTop.cpp:684
msgid "Yellow"
msgstr "Flava"

#: AbTop.cpp:677
msgid "Red won"
msgstr "Ruĝo gajnis"

#: AbTop.cpp:677
msgid "Yellow won"
msgstr "Flavo gajnis"

#: AbTop.cpp:686
msgid "I am thinking..."
msgstr "Mi pensas..."

#: AbTop.cpp:686
msgid "It's your turn!"
msgstr "Via vico!"

#: kabalone.cpp:16
msgid "KDE Implementation of Abalone"
msgstr "KDE-realigo de Abalono"

#: kabalone.cpp:21
msgid "Use 'host' for network game"
msgstr "Uzu 'komputilo'n por retludo"

#: kabalone.cpp:23
msgid "Use 'port' for network game"
msgstr "Uzu 'pordo'n por retludo"

#: kabalone.cpp:29
msgid "KAbalone"
msgstr "Abalono"

#: Spy.cpp:25
msgid "Actual examined position:"
msgstr "Aktuale ekzamenata situacio:"

#: Spy.cpp:50
msgid "Best move so far:"
msgstr "Plej bona movo ĝis nun:"

#: EvalDlgImpl.cpp:36
msgid "Current"
msgstr "Nuna"

#: EvalDlgImpl.cpp:250
msgid "Save Scheme as ..."
msgstr "Sekurigu agordon kiel..."

#: EvalDlgImpl.cpp:251
msgid "Name for Scheme"
msgstr "Nomo de agordo"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Wolfram Diestel"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "<wolfram@steloj.de>"

#~ msgid "&Modify"
#~ msgstr "Ŝ&anĝu"

#~ msgid "Right"
#~ msgstr "Dekstren"

#~ msgid "Left"
#~ msgstr "Maldekstren"

#~ msgid "&New"
#~ msgstr "&Nova"
