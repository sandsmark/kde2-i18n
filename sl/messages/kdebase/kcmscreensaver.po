# KDEBASE translation to Slovenian language.
# Copyright (C) 2000 Free Software Foundation, Inc.
# Roman Maurer <roman.maurer@amis.net>, 2000.
# Marko Samastur <markos@elite.org>, 1999.
# $Id: kcmscreensaver.po 101380 2001-06-10 18:40:36Z scripty $
# $Source$
#
msgid ""
msgstr ""
"Project-Id-Version: kcmscreensaver\n"
"POT-Creation-Date: 2001-06-10 02:05+0200\n"
"PO-Revision-Date: 2000-09-13 23:30+0200\n"
"Last-Translator: Gregor Rakar <gregor.rakar@kiss.uni-lj.si>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: scrnsave.cpp:186
msgid "&Enable screensaver"
msgstr "&Vklopi ohranjevalnik zaslona"

#: scrnsave.cpp:191
msgid ""
"Check this box if you would like to enable a screen saver. If you have power "
"saving features enabled for your display, you may still enable a screen "
"saver."
msgstr ""
"Vklopite to izbiro, če želite vključiti ohranjevalnik zaslona. "
"Ohranjevalnik boste lahko uporabljali tudi, če imate vključene varčevalne "
"funkcije monitorja."

#: scrnsave.cpp:195
msgid "Screen Saver"
msgstr "Ohranjevalnik zaslona"

#: scrnsave.cpp:204
msgid ""
"This is a list of the available screen savers. Select the one you want to "
"use."
msgstr ""
"To je seznam dostopnih ohranjevalnikov zaslona. Izberite tistega, ki ga "
"želite uporabljati."

#: scrnsave.cpp:208
msgid "&Setup ..."
msgstr "Na&stavi ..."

#: scrnsave.cpp:212
msgid ""
"If the screen saver you selected has customizable features, you can set them "
"up by clicking this button."
msgstr ""
"Če je izbrani ohranjevalnik zaslona nastavljiv, ga lahko nastavite s "
"pritiskom ta na gumb."

#: scrnsave.cpp:215
msgid "&Test"
msgstr "&Preizkus"

#: scrnsave.cpp:219
msgid ""
"You can try out the screen saver by clicking this button. (Also, the preview "
"image shows you what the screen saver will look like.)"
msgstr ""
"Ohranjevalnik lahko preskusite s pritiskom na gumb. Prav tako si ga lahko "
"ogledate v sliki."

#: scrnsave.cpp:231
msgid "Here you can see a preview of the selected screen saver."
msgstr "Tu lahko pogledate predogled izbranega ohranjevalnika zaslona."

#: scrnsave.cpp:233
msgid "Settings"
msgstr "Nastavitve"

#: scrnsave.cpp:240
msgid "&Wait for"
msgstr "&Počakaj"

#: scrnsave.cpp:245
msgid " min."
msgstr " min."

#: scrnsave.cpp:251
msgid ""
"Choose the period of inactivity (from 1 to 120 minutes) after which the "
"screen saver should start."
msgstr ""
"Izberite periodo nedejavnosti (med 1 in 120 minut), po kateri se bo zagnal "
"ohranjevalnik zaslona."

#: scrnsave.cpp:258
msgid "&Require password"
msgstr "&Zahtevaj geslo"

#: scrnsave.cpp:264
msgid ""
"If you check this option, the display will be locked when the screen saver "
"starts. To restore the display, enter your account password at the prompt."
msgstr ""
"Če vklopite to izbiro, se bo zaslon zaklenil ob zagonu ohranjevalnika "
"zaslona. Odklenite ga tako, da vpišete geslo svojega računa."

#: scrnsave.cpp:273
msgid "&Priority"
msgstr "&Prednost"

#: scrnsave.cpp:285
msgid ""
"Use this slider to change the processing priority for the screen saver over "
"other jobs that are being executed in the background. For a "
"processor-intensive screen saver, setting a higher priority may make the "
"display smoother at the expense of other jobs."
msgstr ""
"Ta drsnik lahko uporabite za spremembo računske prednosti ohranjevalnika "
"zaslona pred ostalimi programi, ki tečejo v ozadju. Za računsko intenziven "
"ohranjevalnik bo večja prednost pripomogla k bolj tekočemu izrisu na "
"račun drugih programov."

#: scrnsave.cpp:295
msgid ""
"_: Low Priority\n"
"Low"
msgstr "Nizka"

#: scrnsave.cpp:298
msgid ""
"_: High Priority\n"
"High"
msgstr "Visoka"

#: scrnsave.cpp:799
msgid ""
"<h1>Screen saver</h1> This module allows you to enable and configure a "
"screen saver. Note that you can enable a screen saver even if you have power "
"saving features enabled for your display.<p> Besides providing an endless "
"variety of entertainment and preventing monitor burn-in, a screen saver also "
"gives you a simple way to lock your display if you are going to leave it "
"unattended for a while. If you want the screen saver to lock the screen, "
"make sure you enable the \"Require password\" feature of the screen saver. "
"If you don't, you can still explicitly lock the screen using the desktop's "
"\"Lock Screen\" action."
msgstr ""
"<h1>Ohranjevalnik zaslona</h1> Ta modul omogoča vklop in nastavitev "
"ohranjevalnika zaslona. Le-tega lahko vklopite tudi, če imate vklopljene "
"varčevalne nastavitve zaslona.<p> Razen brezmejne zabave in preprečitve "
"vžgane slike vam omogoča tudi enostavno zaklepanje zaslona za začasno "
"odsotnost. Če želite zakleniti zaslon, vklopite izbiro \"Zahtevaj geslo\" "
"ohranjevalnika zaslona. Če ne, lahko še vedno uporabite izbiro namizja "
"\"Zakleni zaslon\"."
