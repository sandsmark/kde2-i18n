# SLOVENIAN TRANSLATION OF K DESKTOP ENVIRONMENT.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>, 2001.
# $Id: kcmlaunch.po 107319 2001-07-23 14:55:54Z scripty $
# $Source$
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase 2.2\n"
"POT-Creation-Date: 2001-06-12 14:31+0200\n"
"PO-Revision-Date: 2001-07-20 17:48GMT\n"
"Last-Translator: Grega Fajdiga <gregor.fajdiga@telemach.net>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: kcmlaunch.cpp:50
msgid "Form1"
msgstr "Oblika1"

#: kcmlaunch.cpp:56
msgid "Busy Cursor"
msgstr "Kazalec za zaposlenost"

#: kcmlaunch.cpp:58
msgid ""
"<h1>Busy Cursor</h1>\n"
"KDE offers a busy cursor for application startup notification.\n"
"To enable the busy cursor, check 'Enable Busy Cursor'.\n"
"To have the cursor blinking, check 'Enable blinking' below.\n"
"It may occur, that some applications are not aware of this startup\n"
"notification. In this case, the cursor stops blinking after the time\n"
"given in the section 'Startup indication timeout'"
msgstr ""
"<h1>Kazalec za zaposlenost</h1>\n"
"KDE ponuja kazalec za zaposlenost kot obvestilo ob zagonu\n"
"programa Da bi kazalec za zaposlenost omogočili, potrdite\n"
"'Omogoči kazalec za zaposlenost'. Da bi kazalec utripal,\n"
"omogočite 'Omogoči utripanje' spodaj. Zgodi se lahko, da \n"
"se nekateri programi ne zavedajo tega obvestila ob zagonu.\n"
"V tem primeru kazalec preneha utripati po času navedenem v\n"
"odseku 'Zakasnitev obvestila o zagonu'"

#: kcmlaunch.cpp:77 kcmlaunch.cpp:120
msgid "Startup indication timeout (seconds) :"
msgstr "Zakasnitev obvestila o zagonu (sekunde):"

#: kcmlaunch.cpp:86
msgid "Enable Busy Cursor "
msgstr "Omogoči kazalec za zaposlenost"

#: kcmlaunch.cpp:94
msgid "Enable blinking"
msgstr "Omogoči utripanje"

#: kcmlaunch.cpp:102
msgid "Taskbar Notification"
msgstr "Obvestilo v opravilni vrstici"

#: kcmlaunch.cpp:103
msgid ""
"<H1>Taskbar Notification</H1>\n"
"You can enable a second method of startup notification which is\n"
"used by the taskbar where a button with a rotating disk appears,\n"
"symbolizing that your started application is loading.\n"
"It may occur, that some applications are not aware of this startup\n"
"notification. In this case, the button disappears after the time\n"
"given in the section 'Startup indication timeout'"
msgstr ""
"<H1>Obvestilo v opravilni vrstic</H1>\n"
"Omogočite lahko tudi drugo metodo za obvestilo o zagonu, ki jo\n"
"uporablja opravilna vrstica, pri kateri se prikaže vrteči se disk, ki\n"
"simbolizira zagon programa.\n"
"Zgodi se lahko, da se nekateri programi ne zavedajo tega obvestila\n"
" ob zagonu. V tem primeru kazalec preneha utripati po času\n"
" navedenem v odseku 'Zakasnitev obvestila o zagonu'"

#: kcmlaunch.cpp:131
msgid "Enable Taskbar Notification "
msgstr "Omogoči obvestilo v opravilni vrstici"

#: kcmlaunch.cpp:327
msgid "<h1>Launch</h1> You can configure the application-launch feedback here."
msgstr "<h1>Zagon</h1> Tu lahko nastavite odziv programa, ki se zaganja."
