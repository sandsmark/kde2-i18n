# SLOVENIAN TRANSLATION OF K DESKTOP ENVIRONMENT.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>, 2001.
# $Id: kpartsaver.po 103798 2001-06-25 16:14:47Z romanm $
# $Source$
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase 2.2\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-06-23 21:45+0100\n"
"Last-Translator: Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: kpartsaver.cpp:62
msgid "KPart Screensaver"
msgstr "Ohranjevalnik zaslona KPart"

#: kpartsaver.cpp:122
msgid "The screensaver isn't configured yet"
msgstr "Ohranjevalnik še ni nastavljen"

#: kpartsaver.cpp:266
msgid "All of your files are unsupported"
msgstr "Vse vaše datoteke so nepodprte"

#: kpartsaver.cpp:349
msgid "Select media files"
msgstr "Izberite večpredstavnostne datoteke"

#: rc.cpp:1
msgid "Media Screensaver"
msgstr "Večpredstavni ohranjevalnik zaslona."

#: rc.cpp:3
msgid "&Down"
msgstr "D&ol"

#: rc.cpp:4
msgid "&Up"
msgstr "&Gor"

#: rc.cpp:5
msgid "&Add"
msgstr "Dod&aj"

#: rc.cpp:7
msgid "Settings"
msgstr "Nastavitve"

#: rc.cpp:8
msgid "Only show one randomly chosen medium"
msgstr "Prikaži le enega naključno izbranega posrednika"

#: rc.cpp:9
msgid "Switch to another medium after a delay"
msgstr "Preklopi na drugega posrednika po zakasnitvi"

#: rc.cpp:10
msgid "Delay:"
msgstr "Zamik:"

#: rc.cpp:11
msgid "Choose next medium randomly"
msgstr "Izberi naslednjega posrednika naključno"

#: rc.cpp:12
msgid "seconds"
msgstr "sekund"
