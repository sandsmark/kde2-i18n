# KDEGRAPHICS translation to Slovenian language.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Roman Maurer <roman.maurer@amis.net>, 2000.
# Marko Samastur <markos@elite.org>, 1999.
# $Id: kdvi.po 117034 2001-10-09 14:22:35Z coolo $
# $Source$
#
msgid ""
msgstr ""
"Project-Id-Version: kdvi\n"
"POT-Creation-Date: 2001-09-03 09:46+0200\n"
"PO-Revision-Date: 2001-09-05 22:09+0200\n"
"Last-Translator: Roman Maurer <roman.maurer@amis.net>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: dvi_init.cpp:153
msgid "DVI file doesn't start with preamble."
msgstr "Datoteka DVI se ne začne z vzglavjem."

#: dvi_init.cpp:155
msgid "Wrong version of DVI output for this program."
msgstr "Napačna različica izhoda DVI za ta program."

#: dvi_init.cpp:201
msgid "DVI file corrupted"
msgstr "Datoteka DVI je pokvarjena."

#: dvi_init.cpp:210
msgid "Wrong version of DVI output for this program"
msgstr "Napačna različica izhoda DVI za ta program."

#: dvi_init.cpp:221
msgid "Postamble doesn't begin with POST"
msgstr "Vznožje se ne začne s POST."

#: dvi_init.cpp:224
msgid "Postamble doesn't match preamble"
msgstr "Vznožje ne ustreza vzglavju."

#: dvi_init.cpp:234
msgid "Non-fntdef command found in postamble"
msgstr "Ukaz, ki ni fntdef, v vznožju."

#: dvi_init.cpp:236
msgid "Not all pixel files were found"
msgstr "Ni moč najti vseh datotek s pikami."

#: dviwin.cpp:214 dviwin.cpp:303
msgid "Another export command is currently running"
msgstr "Trenutno že teče nek drug ukaz za izvoz"

#: dviwin.cpp:233
msgid ""
"KDVI could not locate the program 'dvipdfm' on your computer. That program "
"is\n"
"absolutely needed by the export function. You can, however, convert\n"
"the DVI-file to PDF using the print function of KDVI, but that will often\n"
"produce which print ok, but are of inferior quality if viewed in the \n"
"Acrobat Reader. It may be wise to upgrade to a more recent version of your\n"
"TeX distribution which includes the 'dvipdfm' program.\n"
"\n"
"Hint to the perplexed system administrator: KDVI uses the shell's PATH "
"variable\n"
"when looking for programs."
msgstr ""
"KDVI v vašem računalniku ne najde programa ,dvipdfm`. Funkcija \n"
"za izvoz brezpogojno potrebuje ta program. Datoteko DVI lahko sicer \n"
"pretvorite v PDF tako, da jo v KDVI natisnete v ta format, vendar to \n"
"pogosto pomeni, da se bo sicer natisnila v redu, v bralniku Acrobat Reader\n"
"pa bo videti slabše kakovosti. Morda se vam izplača nadgraditi TeX\n"
"na novejšo različico, ki vključuje program ,dvipdfm`.\n"
"\n"
"Namig za prenapete skrbnike sistemov: KDVI išče programe s pomočjo\n"
"lupinske spremenljivke PATH."

#: dviwin.cpp:244 dviwin.cpp:313
msgid "Export File As"
msgstr "Izvozi datoteko kot"

#: dviwin.cpp:249 dviwin.cpp:318
msgid ""
"The file %1\n"
"exists. Shall I overwrite that file?"
msgstr ""
"Datoteka %1\n"
"obstaja. Ali naj pišemo prek nje?"

#: dviwin.cpp:250 dviwin.cpp:319
msgid "Overwrite file"
msgstr "Piši prek datoteke"

#: dviwin.cpp:257
msgid "Using dvipdfm to export the file to PDF"
msgstr "Za izvoz v PDF se uporablja dvipdfm"

#: dviwin.cpp:259
msgid ""
"KDVI is currently using the external program 'dvipdfm' to convert your "
"DVI-file to PDF. Sometimes that can take a while because dvipdfm needs to "
"generate its own bitmap fonts Please be patient."
msgstr ""
"KDVI trenutno za pretvorbo iz DVI v PDF uporablja zunanji program ,dvipdfm`. "
"Včasih lahko to traja precej časa, saj mora dvipdfm narediti svoje lastne "
"bitne pisave. Prosim, bodite potrpežljivi."

#: dviwin.cpp:263
msgid "Waiting for dvipdf to finish..."
msgstr "Čakam, da program dvipdf konča..."

#: dviwin.cpp:266 dviwin.cpp:339
msgid "Please be patient"
msgstr "Prosim, bodite potrpežljivi"

#: dviwin.cpp:282
msgid "Export: %1 to PDF"
msgstr "Izvoz: %1 v PDF"

#: dviwin.cpp:330
msgid "Using dvips to export the file to PostScript"
msgstr "Uporablja se dvips za izvoz datoteke v postscript"

#: dviwin.cpp:332
msgid ""
"KDVI is currently using the external program 'dvips' to convert your "
"DVI-file to PostScript. Sometimes that can take a while because dvips needs "
"to generate its own bitmap fonts Please be patient."
msgstr ""
"KDVI trenutno za pretvorbo iz DVI v postscript uporablja zunanji program "
",dvips`. Včasih lahko to traja precej časa, saj mora dvips narediti svoje "
"lastne bitne pisave. Prosim, bodite potrpežljivi."

#: dviwin.cpp:336
msgid "Waiting for dvips to finish..."
msgstr "Čakam, da dvips konča..."

#: dviwin.cpp:399
msgid ""
"Failed to copy the DVI-file <strong>%1</strong> to the temporary file "
"<strong>%2</strong>. The export or print command is aborted."
msgstr ""
"Datoteke DVI <strong>%1</strong> ni bilo moč prepisati v začasno datoteko "
"<strong>%2</strong>. Izvoz ali tiskanje je prekinjeno."

#: dviwin.cpp:417
msgid "Export: %1 to PostScript"
msgstr "Izvoz: %1 v postscript"

#: dviwin.cpp:476
msgid ""
"The external program used to export the DVI-file\n"
"reported an error. You might wish to look at the\n"
"document info dialog for a precise error report."
msgstr ""
"Zunanji program za izvoz datoteke DVI\n"
"je sporočil napako. Za natančnejše poročilo\n"
"o napaki boste morda želeli pogledati\n"
"pogovorno okno s podatki o dokumentu."

#: dviwin.cpp:520
msgid ""
"The change in Metafont mode will be effective\n"
"only after you start kdvi again!"
msgstr ""
"Sprememba v načinu Metafont bo v veljavi šele\n"
"potem, ko boste ponovno zagnali kdvi!"

#: dviwin.cpp:585 dviwin.cpp:680
msgid ""
"File corruption!\n"
"\n"
msgstr ""
"Pokvarjena datoteka!\n"
"\n"

#: dviwin.cpp:587
msgid ""
"\n"
"\n"
"Most likely this means that the DVI file\n"
"is broken, or that it is not a DVI file."
msgstr ""
"\n"
"\n"
"To najverjetneje pomeni, da je datoteka DVI\n"
"pokvarjena, ali pa sploh ni datoteka DVI."

#: dviwin.cpp:670
msgid ""
"File error!\n"
"\n"
msgstr ""
"Napaka v datoteki!\n"
"\n"

#: dviwin.cpp:671
msgid ""
"The file does not exist\n"
msgstr ""
"Datoteke ni.\n"

#: dviwin.cpp:682
msgid ""
"\n"
"\n"
"Most likely this means that the DVI file\n"
msgstr ""
"\n"
"\n"
"To najverjetneje pomeni, da je datoteka DVI\n"

#: dviwin.cpp:684
msgid ""
"\n"
"is broken, or that it is not a DVI file."
msgstr ""
"\n"
"pokvarjena, ali pa sploh ni datoteka DVI."

#: font.cpp:43
msgid "Can't find font "
msgstr "Ni moč najti pisave "

#: font.cpp:56
msgid "The GF format for font file %1 is no longer supported"
msgstr "Zapis GF za datoteko s pisavami %1 ni več podprt."

#: font.cpp:62
#, c-format
msgid "Cannot recognize format for font file %1"
msgstr "Zapisa datoteke s pisavami %1 ni moč prepoznati."

#: font.cpp:147
msgid "Character %1 not defined in font %2"
msgstr "Znak %1 ni definiran v pisavi %2."

#: font.cpp:157
#, c-format
msgid "Font file disappeared: %1"
msgstr "Datoteka s pisavami je izginila: %1"

#: fontpool.cpp:36
msgid "KDVI is currently generating bitmap fonts..."
msgstr "KDVI trenutno generira bitne pisave..."

#: fontpool.cpp:37
msgid "Aborts the font generation. Don't do this."
msgstr "Prekine generiranje pisav. Ne storite tega."

#: fontpool.cpp:38
msgid ""
"KDVI is currently generating bitmap fonts which are needed to display your "
"document. For this, KDVI uses a number of external programs, such as "
"MetaFont. You can find the output of these programs later in the document "
"info dialog."
msgstr ""
"KDVI trenutno generira bitne pisave, ki jih potrebuje za prikaz vašega "
"dokumenta. KDVI pri tem uporablja številne zunanje programe, kot je "
"MetaFont. Izhod teh programov lahko pozneje najdete v pogovornem oknu s "
"podatki o dokumentu."

#: fontpool.cpp:41
msgid "KDVI is generating fonts. Please wait."
msgstr "KDVI generira pisave. Prosim, počakajte."

#: fontpool.cpp:130
msgid "Could not allocate memory for a font structure!"
msgstr "Ni moč dodeliti pomnilnika za strukturo pisav!"

#: fontpool.cpp:147
msgid "The fontlist is currently empty."
msgstr "Seznam pisav je trenutno prazen."

#: fontpool.cpp:157
msgid "virtual"
msgstr "navidezna"

#: fontpool.cpp:159
msgid "regular"
msgstr "navadna"

#: fontpool.cpp:220
msgid "Locating fonts..."
msgstr "Iskanje pisav..."

#: fontpool.cpp:233
msgid "Font Generation"
msgstr "Generiranje pisav"

#: fontpool.cpp:363
msgid "Font not found - KDVI"
msgstr "Pisave ni moč najti - KDVI"

#: fontpool.cpp:364
msgid ""
"There were problems running the kpsewhich program.\n"
"KDVI will not work if TeX is not installed on your\n"
"system or if the kpsewhich program cannot be found\n"
"in the standard search path.\n"
"\n"
msgstr ""
"Pri poganjanju programa kpsewhich so nastale težave.\n"
"KDVI ne deluje, če TeX ni nameščen v vašem sistemu\n"
"ali če programa kpsewhich ni moč najti v standardni\n"
"iskalni poti.\n"
"\n"

#: fontpool.cpp:368
msgid ""
"KDVI was not able to locate all the font files \n"
"which are necessary to display the current DVI file. \n"
"Some characters are therefore left blank, and your \n"
"document might be unreadable."
msgstr ""
"KDVI ni mogel najti vseh datotek s pisavami,\n"
"ki rabijo za prikaz trenutne datoteke DVI.\n"
"Nekateri znaki so zato prazni, in vašega dokumenta\n"
"se ne dá brati."

#: fontpool.cpp:372
msgid ""
"\n"
"\n"
"Experts will find helpful information in the 'MetaFont'-\n"
"section of the document info dialog"
msgstr ""
"\n"
"\n"
"Strokovnjaki lahko najdejo koristne podatke v razdelku\n"
",MetaFont` v pogovornem oknu s podatki o dokumentu."

#: fontpool.cpp:379
msgid ""
"\n"
"Automatic font generation is switched off.\n"
msgstr ""
"\n"
"Samodejno generiranje pisav je izključeno.\n"

#: fontpool.cpp:487
msgid "Currently generating %1 at %2 dpi"
msgstr "Trenutno se generira %1 ločljivosti %2 dpi"

#: fontprogress.cpp:30
msgid "Abort"
msgstr "Prekini"

#: fontprogress.cpp:34
msgid "What's going on here?"
msgstr "Kaj se dogaja?"

#: fontprogress.cpp:48
msgid "%v of %m"
msgstr "%v od %m"

#: infodialog.cpp:22
msgid "DVI File"
msgstr "Datoteka DVI"

#: infodialog.cpp:25
msgid "Information on the currently loaded DVI-file."
msgstr "Podatki o trenutno naloženi datoteki DVI"

#: infodialog.cpp:33
msgid "Information on currently loaded fonts."
msgstr "Podatki o trenutno naloženih pisavah"

#: infodialog.cpp:34
msgid ""
"This text field shows detailed information about the currently loaded fonts. "
"This is useful for experts who want to locate problems in the setup of TeX "
"or KDVI."
msgstr ""
"To besedilno polje prikazuje podrobne podatke o trenutno naloženih pisavah. "
"To je uporabno za strokovnjake, ki lahko odkrijejo napake v nastavitvah TeXa "
"ali KDVI."

#: infodialog.cpp:38
msgid "External Programs"
msgstr "Zunanji programi"

#: infodialog.cpp:41
msgid "No output from any external program received."
msgstr "Ni prejetega izhoda iz kakšnega zunanjega programa."

#: infodialog.cpp:42
msgid "Output of external programs."
msgstr "Izhod iz zunanjih programov."

#: infodialog.cpp:43
msgid ""
"KDVI uses external programs, such as MetaFont, dvipdfm or dvips. This text "
"field shows the output of these programs. That is useful for experts who "
"want to find problems in the setup of TeX or KDVI."
msgstr ""
"KDVI uporablja zunanje programe, kot so MetaFont, dvipdfm ali dvips. To "
"besedilno polje prikazuje izhod teh programov. Strokovnjaki lahko s pomočjo "
"teh podatkov najdejo napake v nastavitvah TeXa ali KDVI."

#: infodialog.cpp:59
msgid "There is no DVI file loaded at the moment."
msgstr "Trenutno ni naložene datoteke DVI."

#: infodialog.cpp:77
msgid "Filename"
msgstr "Datoteka"

#: infodialog.cpp:78
msgid "File Size"
msgstr "Velikost datoteke"

#: infodialog.cpp:80
msgid "#Pages"
msgstr "#Strani"

#: infodialog.cpp:81
msgid "Generator/Date"
msgstr "Generator/Datum"

#: kdvi_multipage.cpp:81
msgid "Document &Info"
msgstr "&Podatki o dokumentu"

#: kdvi_multipage.cpp:82
msgid "PostScript"
msgstr "Postscript"

#: kdvi_multipage.cpp:83
msgid "PDF"
msgstr "PDF"

#: kdvi_multipage.cpp:85
msgid "&DVI Options"
msgstr "Možnosti &DVI"

#: kdvi_multipage.cpp:86
msgid "About the KDVI plugin..."
msgstr "O vstavku KDVI..."

#: kdvi_multipage.cpp:87
msgid "Help on the KDVI plugin..."
msgstr "Pomoč pri vstavku KDVI..."

#: kdvi_multipage.cpp:88
msgid "Report Bug in the KDVI plugin..."
msgstr "Poročaj o hrošču v vstavku KDVI..."

#: kdvi_multipage.cpp:115
#, c-format
msgid "Loading file %1"
msgstr "Datoteka %1 se nalaga"

#: kdvi_multipage.cpp:145
msgid "*.dvi *.DVI|TeX Device Independent files (*.dvi)"
msgstr "*.dvi *.DVI|TeXovske od naprave neodvisne datoteke (*.dvi)"

#: kdvi_multipage.cpp:240
msgid "the KDVI plugin"
msgstr "vstavek KDVI"

#: kdvi_multipage.cpp:245
msgid ""
"A previewer for Device Independent files (DVI files) produced by the TeX "
"typesetting system.<br>Based on kdvi 0.4.3 and on xdvik, version "
"18f.<br><hr>For latest information, visit <a "
"href=\"http://devel-home.kde.org/~kdvi\">KDVI's Homepage</a>."
msgstr ""
"Ogledovalnik za datoteke DVI (angl. Device Independent), ki jih izdeluje "
"stavni sistem TeX.<br>Temelji na kdvi 0.4.3 in xdvik različice "
"18f.<br><hr>Najnovejše podatke najdete na <a "
"href=\"http://devel-home.kde.org/~kdvi\">domači strani KDVI</a>."

#: kdvi_multipage.cpp:251
msgid "Authors"
msgstr "Avtorji"

#: kdvi_multipage.cpp:252
msgid ""
"Stefan Kebekus<br><a "
"href=\"http://btm8x5.mat.uni-bayreuth.de/~kebekus\">"
"http://btm8x5.mat.uni-bayreuth.de/~kebekus</a><br><a "
"href=\"mailto:kebekus@kde.org\">kebekus@kde.org</a><br>"
"Current maintainer of kdvi. Major rewrite of version 0.4.3.Implementation of "
"hyperlinks.<br><hr>Markku Hinhala<br>Author of kdvi 0.4.3<hr>Nicolai "
"Langfeldt<br>Maintainer of xdvik<hr>Paul Vojta<br> Author of "
"xdvi<br><hr>Many others. Really, lots of people who were involved in kdvi, "
"xdvik and xdvi. I apologize to those who I did not mention here. Please send "
"me an email if you think your name belongs here."
msgstr ""
"Stefan Kebekus<br><a "
"href=\"http://btm8x5.mat.uni-bayreuth.de/~kebekus\">"
"http://btm8x5.mat.uni-bayreuth.de/~kebekus</a><br><a "
"href=\"mailto:kebekus@kde.org\">kebekus@kde.org</a><br>"
"Trenutni vzdrževalec kdvi. Večji prepis različice 0.4.3. Izvedba "
"hiperpovezav.<br>Markku Hinhala<br><hr>Avtor kdvi 0.4.3<hr>Nicolai "
"Langfeldt<br>Vzdrževalec xdvik<hr>Paul Vojta<br>Avtor xdvi<br><hr>In "
"številni drugi. Zares veliko ljudi, ki so bili vpleteni v kdvi, xdvik in "
"xdvi. Opravičujem se vsem tistim, ki jih tu nisem omenil. Prosim, pošljite "
"mi e-pismo, če mislite, da sem sodi tudi vaše ime."

#: kdvi_multipage.cpp:278
msgid "KDVI"
msgstr "KDVI"

#: kdvi_multipage.cpp:378
msgid ""
"The list of pages you selected was empty.\n"
"Maybe you made an error in selecting the pages, \n"
"e.g. by giving in invalid range like '7-2'."
msgstr ""
"Seznam strani, ki ste jih izbrali, je bil prazen. \n"
"Morda ste se zmotili pri izbiranju strani, npr. \n"
"tako, da ste podali neveljaven obseg strani, \n"
"kot je ,7-2`."

#: optiondialog.cpp:44
msgid "Preferences"
msgstr "Nastavitve"

#: optiondialog.cpp:117
msgid "Metafont mode:"
msgstr "Način Metafont:"

#: optiondialog.cpp:119
msgid "LaserJet 4 is usually a good choice."
msgstr "LaserJet 4 je navadno dobra izbira."

#: optiondialog.cpp:120
msgid ""
"Chooses the type of bitmap fonts used for the display. As a general rule, "
"the higher the dpi value, the better quality of the output. On the other "
"hand, large dpi fonts use more resources and make KDVI slower. \n"
"If you are low on hard disk space, or have a slow machine, you may want to "
"choose the same setting that is also used by dvips. That way you avoid to "
"generate several bitmap versions of the same font."
msgstr ""
"Izbere vrsto bitnih pisav, ki se uporabljajo za zaslon. Splošno pravilo je, "
"da večja ločljivost v dpi pomeni tudi boljšo kakovost izhoda. Po drugi "
"strani pa pisave visoke ločljivosti porabljajo več virov in upočasnijo "
"KDVI.\n"
"Če vam zmanjkuje prostora v disku ali pa imate počasen stroj, boste morda "
"želeli uporabiti isto nastavitev, kot jo uporablja dvips. Tako boste "
"preprečili, da bo se različice iste bitne pisave generirale večkrat v "
"različnih ločljivostih."

#: optiondialog.cpp:127
msgid "Generate missing fonts"
msgstr "Ustvari manjkajoče pisave"

#: optiondialog.cpp:128 optiondialog.cpp:143 optiondialog.cpp:146
msgid "If in doubt, switch on!"
msgstr "Če ste v dvomih, to omogočite!"

#: optiondialog.cpp:129
msgid ""
"Allows KDVI to use MetaFont to produce bitmap fonts. Unless you have a very "
"specific reason, you probably want to switch this on."
msgstr ""
"Dovoli KDVI, da za izdelavo bitnih pisav uporabi MetaFont. Če nimate zelo "
"posebnega razloga, boste verjetno to želeli omogočiti."

#: optiondialog.cpp:138
msgid "Rendering"
msgstr "Upodobitev"

#: optiondialog.cpp:142
msgid "Show PostScript specials"
msgstr "Prikaži postscriptne vstavke"

#: optiondialog.cpp:144
msgid ""
"Some DVI files contain PostScript graphics. If switched on, KDVI will use "
"the ghostview PostScript interpreter to display these. You probably want to "
"switch this option on, unless you have a DVI-file whose PostScript part is "
"broken, or too large for your machine."
msgstr ""
"Nekatere datoteke DVI vsebujejo postscriptne grafike. Če je to vključeno, "
"bo KDVI za prikaz le-teh uporabljal postscriptni tolmač ghostview. Verjetno "
"želite vključiti to izbiro, razen, če nimate datoteke DVI, katere "
"postscriptni del je pokvarjen ali prevelik za vaš stroj."

#: optiondialog.cpp:145
msgid "Show Hyperlinks"
msgstr "Prikaži hiperpovezave"

#: optiondialog.cpp:147
msgid ""
"For your convenience, some DVI files contain hyperlinks which are "
"corss-references or point to external documents. You probably want to switch "
"this option on, unless you are annoyed by the blue underlines which KDVI "
"uses to mark the hyperlinks."
msgstr ""
"Nekatere datoteke DVI vsebujejo hiperpovezave, ki predstavljajo navzkrižne "
"reference ali kažejo na zunanje dokumente. Verjetno želite vključiti to "
"izbiro, če vas le ne moti, da KDVI označuje hiperpovezave tako, da jih "
"podčrta z modro."

#: pk.cpp:202
msgid "Unexpected %1 in PK file %2"
msgstr "Nepričakovana %1 v PK-datoteki %2."

#: pk.cpp:266
msgid "The character %1 is too large in file %2"
msgstr "Znak %1 v datoteki %2 je prevelik."

#: pk.cpp:366 pk.cpp:453
msgid "Wrong number of bits stored:  char. %1, font %2"
msgstr "Shranjeno napačno število bitov: znak %1, pisava %2."

#: pk.cpp:368 pk.cpp:455
msgid "Bad pk file (%1), too many bits"
msgstr "Slaba datoteka PK (%1), preveč bitov."

#: pk.cpp:471 vf.cpp:86
msgid "Checksum mismatch"
msgstr "Neujemanje kontrolne vsote"

#: pk.cpp:472
msgid "in font file "
msgstr "v datoteki s pisavo "

#: pk.cpp:477
msgid "Font has non-square aspect ratio "
msgstr "Pisava ima nekvadratno elipsasto razmerje "

#: pk.cpp:482
msgid "Could not allocate memory for a glyph table."
msgstr "Ni moč dodeliti pomnilnika za tabelo glyph."

#: psgs.cpp:79
msgid "Generating PostScript graphics..."
msgstr "Izdeluje se postscriptna slika..."

#: rc.cpp:2
msgid "Export as..."
msgstr "Izvozi kot..."

#: rc.cpp:3
msgid "Settings"
msgstr "Nastavitve"

#: special.cpp:95
msgid "Malformed parameter in the epsf special command."
msgstr "Zmaličen parameter v posebnem ukazu epsf."

#: special.cpp:221
#, c-format
msgid ""
"File not found:\n"
" %1"
msgstr ""
"Datoteke ni moč najti:\n"
" %1"

#: special.cpp:340
msgid "The special command \""
msgstr "Posebni ukaz \""

#: special.cpp:340
msgid "\" is not implemented."
msgstr "\" ni izveden."

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Marko Samastur, Roman Maurer"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "markos@elite.org, roman.maurer@amis.net"

#: util.cpp:77
msgid "Fatal Error! "
msgstr "Usodna napaka! "

#: util.cpp:80
msgid ""
"Fatal Error!\n"
"\n"
msgstr ""
"Usodna napaka!\n"
"\n"

#: util.cpp:82
msgid ""
"\n"
"\n"
"This probably means that either you found a bug in KDVI,\n"
"or that the DVI file, or auxiliary files (such as font files, \n"
"or virtual font files) were really badly broken.\n"
"KDVI will abort after this message. If you believe that you \n"
"found a bug, or that KDVI should behave better in this situation\n"
"please report the problem."
msgstr ""
"\n"
"\n"
"To najverjetneje pomeni, da ste našli hrošča v KDVI, ali pa gre za \n"
"okvaro v datoteki DVI ali zunanjih datotekah (kot so datoteke s \n"
"pravimi ali navideznimi pisavami).\n"
"Po tem sporočilu se bo program KDVI prekinil. Če mislite, da ste \n"
"našli hrošča, ali da bi se KDVI moral v tem položaju obnašati bolje, \n"
"nam, prosimo, poročajte o tej težavi."

#: util.cpp:105
msgid "Cannot allocate %1 bytes for %2."
msgstr "Ni moč dodeliti %1 bajtov za %2."

#: vf.cpp:87
msgid " in font file "
msgstr " v datoteki s pisavo "

#: vf.cpp:101
msgid "Could not allocate memory for a macro table."
msgstr "Ni moč dodeliti pomnilnika za tabelo makrov."

#: vf.cpp:118
msgid "Virtual character "
msgstr "Navidezni znak "

#: vf.cpp:118
msgid " in font "
msgstr " v pisavi "

#: vf.cpp:119
msgid " ignored."
msgstr " prezrt."

#: vf.cpp:148
#, c-format
msgid "Wrong command byte found in VF macro list: %1"
msgstr "Napačen ukazni bajt v seznamu makroukazov VF: %1"
