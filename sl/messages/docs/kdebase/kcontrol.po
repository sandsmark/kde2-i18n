# SLOVENIAN TRANSLATION OF KDE DOCUMENTATION.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>, 2001.
# $Id: kcontrol.po 112565 2001-08-30 11:45:16Z romanm $
# $Source$
#
msgid ""
msgstr ""
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-07-21 23:18+0200\n"
"Last-Translator: Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2\n"

#: index.docbook:83 index.docbook:111
msgid "The &kcontrolcenter;"
msgstr "Nadzorno središče KDE"

#: index.docbook:86
msgid ""
"<firstname>Michael</firstname> <surname>McBride</surname> "
"<affiliation><address><email>mpmcbride7@yahoo.com</email></address>"
"</affiliation>"
msgstr ""
"<firstname>Michael</firstname> <surname>McBride</surname> "
"<affiliation><address><email>mpmcbride7@yahoo.com</email></address>"
"</affiliation>"

#: index.docbook:97
msgid "This documentation describes &kde;'s control center."
msgstr "Ta dokumentacija opisuje Nadzorno središče &kde;."

#: index.docbook:101
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:102
msgid "kcontrol"
msgstr "kcontrol"

#: index.docbook:103
msgid "configuration"
msgstr "nastavitev"

#: index.docbook:104
msgid "settings"
msgstr "nastavitve"

#: index.docbook:105
msgid "module"
msgstr "modul"

#: index.docbook:113
msgid ""
"The &kcontrolcenter; (from now on referred to simply as <quote>the control "
"center</quote>) provides you with a centralized and convenient way to "
"configure all of your &kde; settings."
msgstr ""
"Nadzorno središče KDE (od sedaj naprej preprosto le <quote>nadzorno "
"središče</quote>) ponuja centraliziran in udoben način nastavljanja "
"lastnosti &kde;."

#: index.docbook:119
msgid ""
"The control center is made up of multiple modules. Each module is a separate "
"application, but the control center organizes all of these programs into a "
"convenient location."
msgstr ""
"Nadzorno središče je zgrajeno iz mnogih modulov. Vsak modul je samostojen "
"program, vendar pa Nadzorno središče uredi vse te programe na eni sami, "
"udobni lokaciji."

#: index.docbook:125
msgid "Each control center module can be executed individually"
msgstr "Vsak modul nadzornega središča se lahko izvede samostojno"

#: index.docbook:125
msgid ""
"See section entitled <link linkend=\"control-center-run-indiv\">Running "
"individual control center modules</link> for more information."
msgstr ""
"Poglejte razdelek <link linkend=\"control-center-run-indiv\">Zagon "
"posameznih modulov</link> za več informacij."

#: index.docbook:136
msgid ""
"The control center groups the configuration modules into categories, so they "
"are easy to locate. Within each category, the control center shows all the "
"modules in a list, so it is easier to find the right configuration module."
msgstr ""
"Nadzorno središče združuje nastavitvene module v kategorije tako, da jih "
"je lahko najti. Znotraj vsake kategorije nadzorno središče prikaže vse "
"module v obliki seznama, tako da je lažje najti ustrezen nastavitveni modul."

#: index.docbook:145
msgid "Using The &kcontrolcenter;"
msgstr "Uporaba nadzornega središča;"

#: index.docbook:147
msgid ""
"This next section details the use of the control center itself. For "
"information on individual modules, please see <link "
"linkend=\"module\">Control Center Modules</link>"
msgstr ""
"Naslednji razdelek podrobno opisuje uporabo nadzornega središča samega. Za "
"informacije o posameznih modulih poglejte prosim v <link "
"linkend=\"module\">Moduli nadzornega središča</link>"

#: index.docbook:154
msgid "Starting the &kcontrol;"
msgstr "Zagon &kcontrol;-a"

#: index.docbook:156
msgid "The &kcontrolcenter; can be started 3 ways:"
msgstr "Nadzorno središče lahko poženete na 3 načine:"

#: index.docbook:160
msgid ""
"By selecting <menuchoice><guimenu>K Button</guimenu><guimenuitem>Control "
"Center</guimenuitem></menuchoice> from the &kde; Panel."
msgstr ""
"Z izbiro <menuchoice><guimenu>gumba K </guimenu><guimenuitem>Nadzorno "
"središče</guimenuitem></menuchoice> s &kde; pulta."

#: index.docbook:160
msgid "By pressing <keycombo action=\"simul\">&Alt;<keycap>F2</keycap></keycombo>."
msgstr "S pritiskom <keycombo action=\"simul\">&Alt;<keycap>F2</keycap></keycombo>."

#: index.docbook:160
msgid ""
"This will bring up a dialog box. Type "
"<userinput><command>kcontrol</command></userinput>, and click "
"<guibutton>Run</guibutton>."
msgstr ""
"To bo prikazalo pogovorno okno. Vtpikajte "
"<userinput><command>kcontrol</command></userinput> in kliknite "
"<guibutton>Poženi</guibutton>."

#: index.docbook:160
msgid "You can type <command>kcontrol &amp;</command> at any command prompt."
msgstr "Na katerikoli lupini lahko vtipkate <command>kcontrol &amp;</command>."

#: index.docbook:187
msgid "All three of these methods are equivalent, and produce the same result."
msgstr "Vse tri metode so enakovredne in dajo enak rezultat."

#: index.docbook:194
msgid "The &kcontrolcenter; Screen"
msgstr "Zaslon nadzornega središča"

#: index.docbook:196
msgid ""
"When you start the control center, you are presented with a window, which "
"can be divided into 3 functional parts."
msgstr ""
"Ko poženete nadzorno središče se pojavi okno, ki je lahko razdeljeno na 3 "
"funkcijske dele."

#: index.docbook:201
msgid "Screenshot"
msgstr "Posnetek zaslona"

#: index.docbook:201
msgid "The &kde; Control Center Screen"
msgstr "Zaslon nadzornega središča &kde;"

#: index.docbook:212
msgid ""
"Across the top is a menubar. The menubar will provide you with quick access "
"to most of &kcontrolcenter;'s features. The menus are detailed in <link "
"linkend=\"control-center-menus\">The &kde; Control Center Menus</link>."
msgstr ""
"Na vrhu je izbirna letev. Ta vam ponuja hiter dostop do večine možnosti, "
"ki jih ponuja Nadzorno središče. Menuji so podrobneje opisani v <link "
"linkend=\"control-center-menus\">Menuji nadzornega središča &kde;</link>"

#: index.docbook:219
msgid ""
"Along the left hand side, is a column. This is a where you choose which "
"module to configure. You can learn how to navigate through the modules in "
"the section called <link linkend=\"module-intro\">Navigating Modules</link>."
msgstr ""
"Na levi strani je stolpec. Tukaj izbirate katere module želite nastaviti. O "
"tem, kako navigirati po modulih se lahko poučite v razdelku <link "
"linkend=\"module-intro\">Navigacijski moduli</link>."

#: index.docbook:226
msgid ""
"The grey box on the lower right side (with the wizard), shows you some "
"useful system information."
msgstr ""
"Siv pravokotnik (s čarovnikom) vam prikazuje nekatere uporabne sistemske "
"informacije."

#: index.docbook:231
msgid ""
"In this example, we are running &kde; 2.2, we started &kcontrolcenter; as "
"user <systemitem class=\"username\">wicky</systemitem>, the computer is "
"named <systemitem class=\"systemname\">btnrg1</systemitem>, it is a &Linux; "
"system, kernel version 2.4.4, on a Pentium II."
msgstr ""
"V tem primeru delamo v &kde; 2.2, Nadzorno središče pa smo pognali kot "
"uporabnik <systemitem class=\"username\">matej</systemitem>, računalnik se "
"imenuje <systemitem class=\"systemname\">streznik</systemitem>, operacijski "
"sistem je &Linux;, jedro različice 2.2.14-5, na Pentiumu."

#: index.docbook:241
msgid "The &kcontrol; Menus"
msgstr "Menuji &kcontrol;-a"

#: index.docbook:243
msgid "This next section gives you a brief description of what each menu item does."
msgstr ""
"Naslednji razdelek vam poda kratek opis, kaj dela vsak ukaz posebej v "
"menujih."

#: index.docbook:249
msgid "<guimenu>File</guimenu> Menu"
msgstr "Menu <guimenu>Datoteka</guimenu>"

#: index.docbook:251
msgid "The <guimenu>File</guimenu> menu has a single entry."
msgstr "Menu <guimenu>Datoteka</guimenu> ima le en vnos."

#: index.docbook:255
msgid ""
"<shortcut> <keycombo action=\"simul\">&Ctrl;<keycap>Q</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>Q</accel>uit</guimenuitem>"
msgstr ""
"<shortcut> <keycombo action=\"simul\">&Ctrl;<keycap>Q</keycap></keycombo> "
"</shortcut> <guimenu><accel>D</accel>atoteka</guimenu> "
"<guimenuitem><accel>K</accel>ončaj</guimenuitem>"

#: index.docbook:255
msgid "Closes the control center."
msgstr "Zapre nadzorno središče."

#: index.docbook:276
msgid "<guimenu>View</guimenu> Menu"
msgstr "Menu <guimenu>Videz</guimenu>"

#: index.docbook:278
msgid "These options determine how the module selection looks and acts."
msgstr "Te možnosti določajo, kako izgleda in deluje izbira modulov."

#: index.docbook:282
msgid ""
"<guimenu><accel>V</accel>iew</guimenu> "
"<guimenuitem><accel>M</accel>ode</guimenuitem>"
msgstr ""
"<guimenu><accel>V</accel>idez</guimenu> "
"<guimenuitem><accel>N</accel>ačin</guimenuitem>"

#: index.docbook:282
msgid ""
"Determines whether to use <guimenuitem>Tree</guimenuitem> view, or "
"<guimenuitem>Icon</guimenuitem> view for your modules."
msgstr ""
"Določa, ali naj se <guimenuitem>prikaže drevo</guimenuitem> ali pa "
"<guimenuitem>ikone</guimenuitem>."

#: index.docbook:282
msgid ""
"With <guimenuitem>Tree</guimenuitem> view, each submenu appearing as an "
"indented list."
msgstr ""
"V <guimenuitem>drevesnem pogledu</guimenuitem> se vsak podmenu pokaže kot "
"zamaknjen seznam."

#: index.docbook:282
msgid ""
"With <guimenuitem>Icon</guimenuitem>"
" view, when you click on a category, the categories disappear and are "
"replaced with the module list. You then use an <guiicon>up</guiicon> button "
"to return to your categories."
msgstr ""
"V <guimenuitem>ikonskem</guimenuitem> pogledu povzroči klik na kategorijo, "
"da le ta izgine in je nadomeščena s seznamom modulov. Da se vrnete v vaše "
"kategorije uporabite gumb <guiicon>navzgor</guiicon>."

#: index.docbook:282
msgid ""
"<guimenu><accel>V</accel>iew</guimenu> <guisubmenu>Icon "
"<accel>s</accel>ize</guisubmenu>"
msgstr ""
"<guimenu><accel>V</accel>idez</guimenu> <guisubmenu><accel>V</accel>elikost "
"ikon</guisubmenu>"

#: index.docbook:282
msgid ""
"Using this option, you can choose <guimenuitem>Small</guimenuitem>, "
"<guimenuitem>Medium</guimenuitem>, or <guimenuitem>Large</guimenuitem>"
" icons to select your modules."
msgstr ""
"Z uporabo te možnosti lahko izberete <guimenuitem>majhne</guimenuitem>, "
"<guimenuitem>srednje</guimenuitem> ali <guimenuitem>velike</guimenuitem> "
"ikone za izbiro modulov."

#: index.docbook:282
msgid ""
"This menu item only controls the icon size if you are in <guimenuitem>Icon "
"View</guimenuitem>. Users who use the <guimenuitem>Tree View</guimenuitem>, "
"will not observe any effects with this option."
msgstr ""
"Ta vnos v menuju nadzoruje samo velikost ikon, če ste v pogledu "
"<guimenuitem>Pokaži ikone</guimenuitem>. Uporabniki, ki uporabljajo "
"<guimenuitem>Prikaži drevo</guimenuitem>, ne bodo opazili nobenih učinkov "
"s to možnostjo."

#: index.docbook:333
msgid "<guimenu>Modules</guimenu> Menu"
msgstr "Menu <guimenu>Moduli</guimenu>"

#: index.docbook:335
msgid ""
"The modules menu, is a shortcut to take you directly to any module in the "
"control center."
msgstr ""
"Menu modulov je bližnjica, ki vas neposredno odpelje do kateregakoli modula "
"v nadzornem središču."

#: index.docbook:343
msgid "<guimenu>Help</guimenu> Menu"
msgstr "Menu <guimenu>Pomoč</guimenu>"

#: index.docbook:345
msgid ""
"<shortcut> <keycombo action=\"simul\"><keycap>F1</keycap></keycombo> "
"</shortcut> <guimenu><accel>H</accel>elp</guimenu> "
"<guimenuitem><accel>C</accel>ontents</guimenuitem>"
msgstr ""
"<shortcut> <keycombo action=\"simul\"><keycap>F1</keycap></keycombo> "
"</shortcut> <guimenu><accel>P</accel>omoč</guimenu> "
"<guimenuitem><accel>V</accel>sebina</guimenuitem>"

#: index.docbook:345
msgid "Clicking on this, will take you to the top of this document."
msgstr "Klik na to vas bo pripeljal na vrh tega dokumenta."

#: index.docbook:345
msgid ""
"<shortcut> <keycombo action=\"simul\">&Ctrl;<keycap>F1</keycap></keycombo> "
"</shortcut> <guimenu><accel>H</accel>elp</guimenu> <guimenuitem>What's "
"<accel>T</accel>his?</guimenuitem>"
msgstr ""
"<shortcut> <keycombo  action=\"simul\">&Ctrl;<keycap>F1</keycap></keycombo> "
"</shortcut> <guimenu><accel>P</accel>omoč</guimenu> <guimenuitem>Kaj je "
"<accel>t</accel>o?</guimenuitem>"

#: index.docbook:345
msgid ""
"Selecting this option, changes the cursor to an arrow with a question mark. "
"Now if you click on any component of the control panel pane, you get short "
"context sensitive help about that item."
msgstr ""
"Izbira te možnosti spremeni kurzor v puščico z vprašajem. Če sedaj "
"kliknete na katerokoli komponento, dobite kratko kontekstno občutljivo "
"pomoč o tem objektu."

#: index.docbook:345
msgid ""
"This is the fastest way to get helpful advice on what any of the controls "
"will do."
msgstr ""
"To je najhitrejši način, da prejmete uporaben nasvet o tem, kaj bo katera "
"izmed kontrol naredila."

#: index.docbook:345
msgid ""
"<guimenu><accel>H</accel>elp</guimenu> <guimenuitem>Report "
"Bug...</guimenuitem>"
msgstr ""
"<guimenu><accel>P</accel>omoč</guimenu> <guimenuitem>Poročaj o "
"hrošču...</guimenuitem>"

#: index.docbook:345
msgid ""
"If you discover a bug, you can select this option to automatically collect "
"bug information and submit it to the &kde; developers."
msgstr ""
"Če odkrijete hrošča lahko izberete to možnost, da samodejno zberete "
"informacije o hrošču in jih posredujete razvijalcem &kde;."

#: index.docbook:345
msgid ""
"<guimenu><accel>H</accel>elp</guimenu> <guimenuitem><accel>A</accel>"
"bout KDE Control Center</guimenuitem>"
msgstr ""
"<guimenu><accel>P</accel>omoč</guimenu> <guimenuitem><accel>O</accel> "
"Nadzorno središče KDE...</guimenuitem>"

#: index.docbook:345
msgid ""
"Selecting this option displays a dialog box containing copyright, author and "
"license information."
msgstr ""
"Izbira te možnosti prikaže pogovorno okno, ki vsebuje copyright, podatke o "
"avtorjih in licenci."

#: index.docbook:345
msgid ""
"<guimenu><accel>H</accel>elp</guimenu> <guimenuitem>About "
"<accel>K</accel>DE</guimenuitem>"
msgstr ""
"<guimenu><accel>P</accel>omoč</guimenu> <guimenuitem>O "
"<accel>K</accel>DE...</guimenuitem>"

#: index.docbook:345
msgid ""
"<action>This will display a dialog box with contact information</action> "
"reguarding the &kde; project."
msgstr ""
"<action>To bo prikazalo pogovorno okno s kontaktnimi informacijami</action>"
", ki zadevajo projekt &kde;."

#: index.docbook:432
msgid "Exiting The &kde; Control Center"
msgstr "Zapuščanje nadzornega središča &kde;"

#: index.docbook:434
msgid "You can exit the control center one of three ways:"
msgstr "Nadzorno središče lahko zapustite na enega izmed treh načinov:"

#: index.docbook:438
msgid ""
"Select <menuchoice><guimenu>File</guimenu> "
"<guimenuitem>Quit</guimenuitem></menuchoice> from the menu bar."
msgstr ""
"Izberite <menuchoice><guimenu>Datoteka</guimenu> "
"<guimenuitem>Končaj</guimenuitem></menuchoice> v menujski vrstici."

#: index.docbook:438
msgid ""
"Type <keycombo action=\"simul\">&Ctrl;<keycap>Q</keycap></keycombo> on the "
"keyboard."
msgstr "Pritisnite <keycombo action=\"simul\">&Ctrl;<keycap>Q</keycap></keycombo>."

#: index.docbook:438
msgid ""
"Click on the <guiicon>Close</guiicon> button on the frame surrounding the "
"control center."
msgstr ""
"Kliknite na gumb <guiicon>Zapri</guiicon> na okvirju, ki obdaja nadzorno "
"središče."

#: index.docbook:463
msgid "Running Individual Modules"
msgstr "Zagon posameznih modulov"

#: index.docbook:465
msgid ""
"You can run individual modules without running kcontrol by selecting "
"<menuchoice><guimenu>K Button</guimenu> <guisubmenu>Preferences</guisubmenu>"
" </menuchoice>"
" from the &kde; panel. You can then select the module you want to run in the "
"submenus."
msgstr ""
"Posamezne module lahko poženete, ne da bi pognali kcontrol z izbiro "
"<menuchoice><guimenu>K gumba</guimenu> "
"<guisubmenu>Priljubljenosti</guisubmenu> </menuchoice> preko &kde; pulta. V "
"podmenujih lahko izberete modul, ki ga želite pognati."

#: index.docbook:479
msgid "The &kcontrol; Modules"
msgstr "Moduli &kcontrol;-a"

#: index.docbook:481
msgid ""
"In order to make it as easy as possible, the &kcontrol; has organized "
"similar options into groups. Each group is called a module. When you click "
"on the name of a module in the left window, you will be presented with the "
"options of the module on the right."
msgstr ""
"Z namenom narediti čim bolj preprosto kot je mogoče, ima &kcontrol; "
"organizirane podobne lastnosti po skupinah. Vsaka skupina se imenuje modul. "
"Ko kliknete na ime modula v levem oknu, boste videli možnosti, ki jih "
"ponuja modul na desni strani."

#: index.docbook:488
msgid "Each module will have some or all of the following buttons:"
msgstr "Vsak modul bo imel nekaj ali vse od naslednjih možnosti:"

#: index.docbook:492
msgid "Apply"
msgstr "Uveljavi"

#: index.docbook:492
msgid ""
"Clicking this button will save all changes to &kde;. If you have changed "
"anything, clicking <guibutton>Apply</guibutton> will take the changes in "
"effect."
msgstr ""
"Klik na ta gumb bo shranil vse spremembe v &kde;. Če ste kaj spremenili, bo "
"klik na <guibutton>Uveljavi</guibutton> uveljavilo učinek."

#: index.docbook:492
msgid "Reset"
msgstr "Resetiraj"

#: index.docbook:492
msgid ""
"This button will <quote>Reset</quote> the module. The exact effect will "
"depend on the module."
msgstr ""
"Ta gumb bo <quote>resetiral</quote> modul. Točen učinek bo odvisen od "
"modula."

#: index.docbook:492 index.docbook:560
msgid "Help"
msgstr "Pomoč"

#: index.docbook:492
msgid ""
"This button will give you help specific to the current module. The button "
"will show you a short summary help page in the left window. At the bottom of "
"that window, you can click on a link to get more detailed help."
msgstr ""
"Ta gumb vam bo odprl pomoč, ki se nanaša na trenutni modul. Gumb vam bo "
"pokazal kratko stran z vsebino v levem oknu. Na dnu tega okna lahko kliknite "
"na povezavo, da dobite bolj podrobne informacije."

#: index.docbook:492
msgid "Default"
msgstr "Privzeto"

#: index.docbook:492
msgid ""
"This button will restore this module to its default values. You must click "
"<guibutton>OK</guibutton> to save the options."
msgstr ""
"Ta gumb bo obnovil ta modul na njegove privzete vrednosti. Da shranite "
"možnosti morate klikniti na <guibutton>V redu</guibutton>."

#: index.docbook:537
msgid ""
"You must save the options of one module using <guibutton>Apply</guibutton> "
"before you can change to a different module."
msgstr ""
"Preden lahko zamenjate na drug modul, morate najprej shraniti možnosti "
"prvega modula z gumbom <guibutton>Uveljavi</guibutton>."

#: index.docbook:537
msgid ""
"If you try to change without saving your options, you will be asked if you "
"want to save your changes, or discard them."
msgstr ""
"Če želite zamenjati brez da bi shranili spremembe, boste vprašani, če "
"želite shraniti vaše spremembe ali če jih želite zavreči."

#: index.docbook:550
msgid "Navigating Modules"
msgstr "Navigacijski moduli"

#: index.docbook:552
msgid ""
"This is a list of the <emphasis>standard</emphasis> configuration modules "
"(sorted by category) provided by the <application role=\"package\">KDE "
"base</application> package. Please note that there may be many more modules "
"on your system if you install additional software."
msgstr ""
"To je seznam <emphasis>standardnih</emphasis> nastavitvenih modulov "
"(razporejenih po kategorijah), ki pridejo s paketom <application "
"role=\"package\">KDE base</application>"
". Upoštevajte prosim, da je lahko na vašem sistemu mnogo več modulov, če "
"namestite dodatno programsko opremo."

#: index.docbook:560
msgid "File Browsing"
msgstr "Brskanje po datotekah"

#: index.docbook:560
msgid ""
"<link linkend=\"file-assoc\">File Associations</link>, <link "
"linkend=\"file-manager\">File Manager</link>,"
msgstr ""
"<link linkend=\"file-assoc\">Datotečne asociacije</link>, <link "
"linkend=\"file-manager\">Upravljalnik datotek</link>,"

#: index.docbook:560
msgid "<link linkend=\"help-index\">Index</link>,"
msgstr "<link linkend=\"help-index\">Indeks</link>,"

#: index.docbook:560
msgid "Information"
msgstr "Informacije"

#: index.docbook:560
msgid ""
"<link linkend=\"dma\">DMA-Channels</link>, <link "
"linkend=\"devices\">Devices</link>, <link "
"linkend=\"ioports\">IO-Ports</link>, <link "
"linkend=\"interrupts\">Interrupts</link>, <link "
"linkend=\"memory\">Memory</link>, <link linkend=\"pci\">PCI</link>, <link "
"linkend=\"pcmcia\">PCMCIA</link>, <link "
"linkend=\"partitions\">Partitions</link>, <link "
"linkend=\"processor\">Processor</link>, <link linkend=\"scsi\">SCSI</link>, "
"<link linkend=\"sambastatus\">Samba Status</link>, <link "
"linkend=\"soundinfo\">Sound</link>, <link linkend=\"usb\">USB Devices</link>"
" <link linkend=\"xserver\">X-Server</link>"
msgstr ""
"<link linkend=\"dma\">Kanali DMA</link>, <link "
"linkend=\"devices\">Naprave</link>, <link linkend=\"ioports\">V/I "
"vrata</link>, <link linkend=\"interrupts\">Prekinitve</link>, <link "
"linkend=\"memory\">Pomnilnik</link>, <link linkend=\"pci\">PCI</link>"
", <link linkend=\"pcmcia\">PCMCIA</link>, <link "
"linkend=\"partitions\">Particije</link> <link "
"linkend=\"processor\">Procesor</link>, <link linkend=\"scsi\">SCSI</link>, "
"<link linkend=\"sambastatus\">Status sambe</link>, <link "
"linkend=\"soundinfo\">Zvok</link>, <link linkend=\"usb\">USB "
"naprave</link><link linkend=\"xserver\">Strežnik X</link>,"

#: index.docbook:560
msgid "Look and Feel"
msgstr "Videz in občutek"

#: index.docbook:560
msgid ""
"<link linkend=\"background\">Background</link>, <link "
"linkend=\"color\">Colors</link>, <link linkend=\"desktop\">Desktop</link>, "
"<link linkend=\"fonts\">Fonts</link>, <link linkend=\"icons\">Icons</link>, "
"<link linkend=\"key-bindings\">Key Bindings</link>, <link "
"linkend=\"launch-feedback\">Launch feedback</link>, <link "
"linkend=\"panel\">Panel</link>, <link "
"linkend=\"screensaver\">Screensaver</link>, <link "
"linkend=\"style\">Style</link>, <link linkend=\"sys-notify\">System "
"Notifications</link>, <link linkend=\"taskbar\">Taskbar</link>, <link "
"linkend=\"theme-manager\">Theme Manager</link>, <link "
"linkend=\"window-behavior\">Window Behavior</link>, <link "
"linkend=\"window-deco\">Window Decoration</link>."
msgstr ""
"<link linkend=\"background\">Ozadje</link>, <link "
"linkend=\"color\">Barve</link>, <link linkend=\"desktop\">Namizje</link>, "
"<link linkend=\"fonts\">Pisave</link>, <link linkend=\"icons\">Ikone</link>"
", <link linkend=\"key-bindings\">Tipkovne vezi</link>, <link "
"linkend=\"launch-feedback\">Launch feedback</link>, <link "
"linkend=\"panel\">Pult</link>, <link linkend=\"screensaver\">Ohranjevalnik "
"zaslona</link>, <link linkend=\"style\">Slog</link>, <link "
"linkend=\"sys-notify\">Sistemska obvestila</link>, <link "
"linkend=\"taskbar\">Opravilna vrstica</link>, <link "
"linkend=\"theme-manager\">Theme Manager</link>,<link "
"linkend=\"window-behavior\">Obnašanje oken</link>, <link "
"linkend=\"window-deco\">Okraski oken</link>,"

#: index.docbook:560
msgid "Network"
msgstr "Omrežje"

#: index.docbook:560
msgid ""
"<link linkend=\"e-mail\">E-Mail</link>, <link linkend=\"lan-browsing\">LAN "
"Browsing</link>, <link linkend=\"timeouts\">Preferences</link>, <link "
"linkend=\"protocols\">Protocols</link>, <link "
"linkend=\"socks\">SOCKS</link>, <link linkend=\"talk\">Talk "
"Configuration</link> <link linkend=\"windows-shares\">Windows Shares</link>"
msgstr ""
"<link linkend=\"e-mail\">E-pošta</link>, <link "
"linkend=\"lan-browsing\">Brskanje po LAN</link>, <link "
"linkend=\"timeouts\">Nastavitve</link>, <link "
"linkend=\"protocols\">Protokoli</link>, <link "
"linkend=\"socks\">SOCKS</link>, <link linkend=\"talk\">Nastavitev "
"Talk</link>, <link linkend=\"windows-shares\">Deljeni diski iz Windows</link>"

#: index.docbook:560
msgid "Peripherals"
msgstr "Strojni dodatki"

#: index.docbook:560
msgid "<link linkend=\"kbd\">Keyboard</link>, <link linkend=\"mouse\">Mouse</link>"
msgstr ""
"<link linkend=\"kbd\">Tipkovnica</link>, <link "
"linkend=\"mouse\">Miška</link>"

#: index.docbook:560
msgid "Personalization"
msgstr "Prilagoditev"

#: index.docbook:560
msgid ""
"<link linkend=\"accessibility\">Accesibility</link>, <link "
"linkend=\"locale\">Country &amp; Language</link>, <link "
"linkend=\"crypto\">Crypto</link>, <link "
"linkend=\"passwords\">Passwords</link> <link "
"linkend=\"spell-checking\">Spell Checking</link>."
msgstr ""
"<link linkend=\"accessibility\">Dostopnost</link>, <link "
"linkend=\"locale\">Država &amp; jezik</link>, <link "
"linkend=\"crypto\">Kripto</link>, <link linkend=\"passwords\">Gesla</link> "
"<link linkend=\"spell-checking\">Spell Checking</link>"

#: index.docbook:560
msgid "Power Control"
msgstr "Nadzor energije"

#: index.docbook:560
msgid ""
"<link linkend=\"battery-monitor\">Battery Monitor</link>, <link "
"linkend=\"energy\">Energy</link>, <link linkend=\"powerctrl\">Laptop Power "
"Control</link>, <link linkend=\"lowbatcrit\">Low Battery Critical</link>, "
"<link linkend=\"lowbatwarn\">Low Battery Warning</link>"
msgstr ""
"<link linkend=\"battery-monitor\">Nadzornik baterije</link>, <link "
"linkend=\"energy\">Energija</link>, <link linkend=\"powerctrl\">Nadzor "
"energije za prenosnike</link>, <link linkend=\"lowbatcrit\">Baterija "
"kritično prazna</link>, <link linkend=\"lowbatwarn\">Opozorilo o prazni "
"bateriji</link>"

#: index.docbook:560
msgid "Sound"
msgstr "Zvok"

#: index.docbook:560
msgid ""
"<link linkend=\"midi\">Midi</link>, <link linkend=\"mixer\">Mixer</link>, "
"<link linkend=\"sndserver\">Sound Server</link>, <link "
"linkend=\"bell\">System Bell</link>"
msgstr ""
"<link linkend=\"midi\">Midi</link> <link linkend=\"mixer\">Mešalna "
"miza</link>, <link linkend=\"sndserver\">Zvočni strežnik</link>, <link "
"linkend=\"bell\">Sistemski zvonec</link>"

#: index.docbook:560
msgid "System"
msgstr "Sistem"

#: index.docbook:560
msgid ""
"<link linkend=\"datetime\">Date and Time</link>, <link "
"linkend=\"system-control\">&kde; System Control</link>, <link "
"linkend=\"konsole\">&konsole;</link>, <link linkend=\"login-manager\">Login "
"Manager</link>, <link linkend=\"print-system\">Print System</link>, <link "
"linkend=\"print-manager\">Printing Manager</link>, <link "
"linkend=\"sessions\">Session Manager</link>,"
msgstr ""
"<link linkend=\"datetime\">Datum in čas</link>, <link "
"linkend=\"system-control\">&kde; System Control</link>, <link "
"linkend=\"konsole\">&konsole;</link>, <link "
"linkend=\"login-manager\">Upravljalnik prijav</link>, <link "
"linkend=\"print-system\">Sistem tiskanja</link>, <link "
"linkend=\"print-manager\">Upravljalnik tiskanja</link>, <link "
"linkend=\"sessions\">Upravljalnik sej</link>"

#: index.docbook:560
msgid "Web Browsing"
msgstr "Brskanje po spletu"

#: index.docbook:560
msgid ""
"<link linkend=\"cookie\">Cookies</link>, <link linkend=\"ebrowse\">Enhanced "
"Browsing</link>, <link linkend=\"konq-browsing\">Konqueror Browser</link>, "
"<link linkend=\"nsplugins\">Netscape Plugins</link>, <link "
"linkend=\"proxies\">Proxies &amp; Cache</link>, <link "
"linkend=\"stylesheets\">Stylesheets</link>, <link "
"linkend=\"user-agent\">User Agent</link>,"
msgstr ""
"<link linkend=\"cookie\">Piškotki</link>, <link "
"linkend=\"ebrowse\">Izboljšano brskanje</link>, <link "
"linkend=\"konq-browsing\">Brskalnik Konqueror</link>, <link "
"linkend=\"nsplugins\">Vstavki za Netscape</link>, <link "
"linkend=\"proxies\">Posredniki &amp; cache</link>, <link "
"linkend=\"stylesheets\">Stylesheets</link>, <link "
"linkend=\"user-agent\">User Agent</link>,"

#: index.docbook:730
msgid "Laptop Modules Notes"
msgstr "Moduli za prenosne računalnike"

#: index.docbook:732
msgid ""
"In order to use the laptop modules, you must have the kernel "
"<acronym>APM</acronym> package installed in your kernel. Useful information "
"on how to do this can be found at <ulink "
"url=\"http://www.cs.utexas.edu/users/kharker/linux-laptop/apm.html\"> "
"http://www.cs.utexas.edu/users/kharker/linux-laptop/apm.html</ulink> and in "
"the Battery Powered Linux mini-HOWTO at <ulink "
"url=\"http://metalab.unc.edu/LDP/HOWTO/mini/Battery-Powered.html\"> "
"http://metalab.unc.edu/LDP/HOWTO/mini/Battery-Powered.html</ulink>."
msgstr ""
"Če želite uporabljati module za prenosnike, morate imeti nameščen paket "
"<acronym>APM</acronym>"
" v vašem jedru. Uporabne informacije kako to naredite lahko najdete na "
"<ulink "
"url=\"http://www.cs.utexas.edu/users/kharker/linux-laptop/apm.html\">"
"http://www.cs.utexas.edu/users/kharker/linux-laptop/apm.html</ulink> in v "
"mini-HOWTO Battery Powered Linux na <ulink "
"url=\"http://metalab.unc.edu/LDP/HOWTO/mini/Battery-Powered.html\">"
"http://metalab.unc.edu/LDP/HOWTO/mini/Battery-Powered.html</ulink>."

#: index.docbook:743
msgid ""
"If you want the <guimenuitem>suspend</guimenuitem> and "
"<guimenuitem>standby</guimenuitem> menu commands to work then you should "
"install the &Linux; <application>apmd</application> package (version 2.4 or "
"later). If you want to use them from non-root accounts you must mark the "
"<application>apm</application> command <quote>set uid root</quote>."
msgstr ""
"Če hočete, da vam delujeta ukaza <guimenuitem>izključen</guimenuitem> in "
"<guimenuitem>ustavljen</guimenuitem>, bi morali namestiti &Linux; "
"<application>apmd</application>"
" paket (različica 2.4 ali novejša). Če jih želite uporabljati z ne-root "
"računom, morate označiti <application>apm</application> z ukazom "
"<quote>nastavi uid root</quote>."

#: index.docbook:751
msgid ""
"To do this log on as <systemitem class=\"username\">root</systemitem> and "
"enter:"
msgstr ""
"Da naredite to, se prijavite kot <systemitem "
"class=\"username\">root</systemitem> in vnesite:"

#: index.docbook:755
msgid ""
"<prompt>%</prompt><userinput><command>chown</command> <option>root "
"/usr/bin/apm</option>;<command>chmod</command> <option>+s "
"/usr/bin/apm</option></userinput>"
msgstr ""
"<prompt>%</prompt><userinput><command>chown</command> <option>root "
"/usr/bin/apm</option>;<command>chmod</command> <option>+s "
"/usr/bin/apm</option></userinput>"

#: index.docbook:760
msgid ""
"By doing this you allow any user of your system to put it into the suspend "
"states - if you are the only user this should not be a problem."
msgstr ""
"S tem dovolite vsakemu uporabniku vašega sistema, da ga postavi v status "
"izključen - če ste edini uporabnik, to nebi smela biti težava."

#: index.docbook:765
msgid ""
"Also note that any program which has <systemitem "
"class=\"username\">root</systemitem> access, can be a potential security "
"problem. You should carefully determine if there is are any security "
"concerns <emphasis>before</emphasis> giving any program <systemitem "
"class=\"username\">root</systemitem> permissions."
msgstr ""
"Vedite tudi, da vsak program, ki ima dostop od <systemitem "
"class=\"username\">korenskega</systemitem> uporabnika, je potencialno lahko "
"varnostni problem. Pazljivo bi morali določiti, če ste lahko zaskrbleni "
"zaradi varnosti <emphasis>preden</emphasis> date kateremukoli programu "
"<systemitem class=\"username\">root</systemitem> dovoljenja."

#: index.docbook:777
msgid "Modules"
msgstr "Moduli"

#: index.docbook:864
msgid "Credits and License"
msgstr "Zahvala in licence"

#: index.docbook:866
msgid "Kcontrol"
msgstr "Kcontrol"

#: index.docbook:867
msgid "Program copyright 1997 The KDE Control Center Developers"
msgstr "Program: copyright 1997 Razvijalci KDE nadzornega središča"

#: index.docbook:868 index.docbook:878
msgid "Contributors:"
msgstr "Prispevali so:"

#: index.docbook:870 index.docbook:880
msgid "Matthias Hoelzer-Kluepfel <email>hoelzer@kde.org</email>"
msgstr "Matthias Hoelzer-Kluepfel <email>hoelzer@kde.org</email>"

#: index.docbook:870
msgid "Matthias Elter <email>elter@kde.org</email>"
msgstr "Matthias Elter <email>elter@kde.org</email>"

#: index.docbook:875
msgid ""
"Documentation <trademark class=\"copyright\">copyright 2000 Michael "
"McBride</trademark> <email>mpmcbride7@yahoo.com</email>"
msgstr ""
"Dokumentacija: <trademark class=\"copyright\">copyright 2000 Michael "
"McBride</trademark> <email>mpmcbride7@yahoo.com</email>"

#: index.docbook:880
msgid "Paul Campbell <email>paul@taniwha.com</email>"
msgstr "Paul Campbell <email>paul@taniwha.com</email>"

#: index.docbook:880
msgid "Helge Deller <email>helge.deller@ruhruni-bochum.de</email>"
msgstr "Helge Deller <email>helge.deller@ruhruni-bochum.de</email>"

#: index.docbook:880
msgid "Mark Donohoe"
msgstr "Mark Donohoe"

#: index.docbook:880
msgid "Pat Dowler"
msgstr "Pat Dowler"

#: index.docbook:880
msgid "Duncan Haldane <email>duncan@kde.org</email>"
msgstr "Duncan Haldane <email>duncan@kde.org</email>"

#: index.docbook:880
msgid "Steffen Hansen <email>stefh@mip.ou.dk</email>."
msgstr "Steffen Hansen <email>stefh@mip.ou.dk</email>."

#: index.docbook:880
msgid "Martin Jones <email>mjones@kde.org</email>"
msgstr "Martin Jones <email>mjones@kde.org</email>"

#: index.docbook:880
msgid "Jost Schenck <email>jost@schenck.de</email>"
msgstr "Jost Schenck <email>jost@schenck.de</email>"

#: index.docbook:880
msgid "Jonathan Singer <email>jsinger@leeta.net</email>"
msgstr "Jonathan Singer <email>jsinger@leeta.net</email>"

#: index.docbook:880
msgid "Thomas Tanghus <email>tanghus@earthling.net</email>"
msgstr "Thomas Tanghus<email>tanghus@earthling.net</email>"

#: index.docbook:880
msgid "Krishna Tateneni <email>tateneni@pluto.njcc.com></email>"
msgstr "Krishna Tateneni <email>tateneni@pluto.njcc.com></email>"

#: index.docbook:880
msgid "Ellis Whitehead <email>ewhitehe@uni-freiburg.de</email>"
msgstr "Ellis Whitehead <email>ewhitehe@uni-freiburg.de</email>"

#: index.docbook:896
msgid "CREDIT_FOR_TRANSLATORS"
msgstr "<para>Prevod: Matej Badalič <email>matej_badalic@slo.net</email></para>"
