# SLOVENIAN TRANSLATION OF KDE DOCUMENTATION.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>, 2001.
# $Id: kcontrol_kcmkbrowse.po 109122 2001-08-04 16:17:29Z scripty $
# $Source$
#
msgid ""
msgstr ""
"Project-Id-Version: documentation\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-08-04 10:05+0200\n"
"Last-Translator: Roman Maurer <roman.maurer@amis.net>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: index.docbook:7
msgid ""
"<firstname>Krishna</firstname> <surname>Tateneni</surname> "
"<affiliation><address><email> tateneni@pluto.njcc.com "
"</email></address></affiliation>"
msgstr ""
"<firstname>Krishna</firstname> <surname>Tateneni</surname> "
"<affiliation><address><email>tateneni@pluto.njcc.com</email></address>"
"</affiliation>"

#: index.docbook:14
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit "
"role=\"translator\"><contrib>Prevod:</contrib><firstname>Matej</firstname>"
"<surname>Badalič</surname><affiliation><address><email>"
"matej_badalic@slo.net</email></address></affiliation></othercredit>"

#: index.docbook:21
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:22
msgid "KControl"
msgstr "KControl"

#: index.docbook:23
msgid "konqueror"
msgstr "konqueror"

#: index.docbook:24
msgid "browsing"
msgstr "brskanje"

#: index.docbook:29
msgid "Browsing With &konqueror;"
msgstr "Brskanje s &konqueror;jem"

#: index.docbook:31
msgid ""
"The &konqueror; Browser module of &kcontrol; allows you to select various "
"options for the appearance and behavior of &konqueror;, the integrated web "
"browser of &kde;."
msgstr ""
"Modul &kcontrol; brskalnika &konqueror; vam omogoča, da izberete različne "
"možnosti za izgled in obnašanje &konqueror;-ja, integriranega spletnega "
"brskalnika &kde;."

#: index.docbook:35
msgid ""
"There are five main tabs in this control center module, which are described "
"in detail in the following sections. Briefly, the five tabs are:"
msgstr ""
"V tem modulu nadzornega središča je pet glavnih jezičkov, ki so podrobno "
"opisani v naslednjih razdelkih. Na kratko, teh pet jezičkov je:"

#: index.docbook:39
msgid "<guilabel>HTML</guilabel>:"
msgstr "<guilabel>HTML</guilabel>:"

#: index.docbook:39
msgid "Options for handling of links and images in <abbrev>HTML</abbrev> pages."
msgstr "Možnost za ravnanje s povezavami in slikami v <abbrev>HTML</abbrev> straneh."

#: index.docbook:39
msgid "<guilabel>Appearance</guilabel>:"
msgstr "<guilabel>Videz</guilabel>:"

#: index.docbook:39
msgid "Options for the fonts used in displaying web pages."
msgstr "Možnosti za uporabo pisav pri prikazovanju spletnih strani."

#: index.docbook:39
msgid "<guilabel>Java</guilabel>:"
msgstr "<guilabel>Java</guilabel>:"

#: index.docbook:39
msgid ""
"Options for handling Java code embedded in web pages, as well as the "
"security policies to be applied to such code."
msgstr ""
"Možnosti za ravnanje s kodo v Javi, vključeno v spletnih straneh, kot tudi "
"taktiko varnosti, ki naj se uveljavi za to kodo."

#: index.docbook:39
msgid "<guilabel>JavaScript</guilabel>:"
msgstr "<guilabel>JavaScript</guilabel>:"

#: index.docbook:39
msgid "Options for handling JavaScript code embedded in web pages."
msgstr "Možnosti za ravnanje s kodo JavaScript, vključeno v spletnih straneh."

#: index.docbook:39 index.docbook:270
msgid "Plugins"
msgstr "Vstavki"

#: index.docbook:39
msgid "Options about Plugins"
msgstr "Možnosti vstavkov"

#: index.docbook:72
msgid "HTML"
msgstr "HTML"

#: index.docbook:74
msgid ""
"The first option you can enable on this page is <guilabel>Enable completion "
"of forms</guilabel>"
". If you check this box, &konqueror; will try to remember what you answer to "
"form questions, and will try to fill in forms for you with the answers you "
"previously used."
msgstr ""
"Prva izbira, ki jo lahko omogočite na tej strani, je <guilabel>Omogoči "
"dopolnjevanje obrazcev</guilabel>. Če obkljukate to polje, se bo "
"&konqueror; skušal spomniti, kaj ste že odgovorili na vprašanja v "
"obrazcu, in bo skušal namesto vas izpolniti obrazec z odgovori, ki ste jih "
"vpisali kdaj prej."

#: index.docbook:79
msgid ""
"You can configure the number of form items &konqueror; remembers with the "
"slider below labelled <guilabel>Maximum completions</guilabel>"
msgstr ""
"Število obrazc postavka, ki si jih &konqueror; zapomni, lahko nastavite s "
"spodnjim drsnikom, označenim z <guilabel>Največ dopolnitev</guilabel>"

#: index.docbook:83
msgid ""
"Of course, anything &konqueror; fills in a form with, you can still edit "
"before submitting the form!"
msgstr ""
"Seveda lahko vse, kar &konqueror; zapiše v obrazec, še vedno urejate, "
"preden obrazec pošljete!"

#: index.docbook:86
msgid ""
"The next setting is <guilabel>Underline links</guilabel>. /ou can choose to "
"underline links <guilabel>Always</guilabel>"
". If this option is selected, any text on web pages that acts as a link will "
"be shown in an underlined font. While many web pages do use color to "
"distinguish text that acts as a link, underlining makes it very easy to spot "
"links."
msgstr ""
"Naslednja nastavitev je <guilabel>Podčrtamo povezave</guilabel>. Če "
"želite imeti povezave vedno podčrtane, vključite "
"<guilabel>Always</guilabel>. Če je ta možnost izbrana, bo vsak tekst na "
"spletni strani, ki deluje kot povezava, prikazan v podčrtani pisavi. Ker "
"veliko spletnih strani uporablja barve za razločevanje teksta, ki deluje "
"kot povezava, vam podčrtavanje poenostavi pri prepoznavanju povezav."

#: index.docbook:93
msgid ""
"If you don't like underlined links, you can choose "
"<guilabel>Never</guilabel>, so that no links are underlined. Or you can "
"choose a middle ground, <guilabel>Hover</guilabel>, so that links are "
"underlined when the mouse cursor is resting over them, and not underlined "
"the rest of the time."
msgstr ""
"Če ne marate podčrtanih povezav, lahko izberete "
"<guilabel>Nikoli</guilabel>, da nobena povezava ni podčrtana. Lahko pa "
"izberete srednjo površino, <guilabel>Lebdenje</guilabel>, da povezave "
"postanejo podčrtane le, kadar nad njimi počiva kazalec miške."

#: index.docbook:99
msgid ""
"Finally there are two last checkbox options. The first is <guilabel>Change "
"cursor over links</guilabel>. If this option is selected, the shape of the "
"cursor will change (usually to a hand) whenever it moves over a hyperlink. "
"This makes it easy to identify links, especially when they are in the form "
"of images."
msgstr ""
"Končno sta tu še dve potrditveni polji. Prvo je <guilabel>Spremenimo "
"kazalec nad povezavami</guilabel>. Če je ta možnost izbrana, se bo oblika "
"kazalca spremenila (ponavadi v roko) vsakič, ko se bo premaknil nad "
"hiperpovezavo. To poenostavi prepoznavanje povezav, posebno kadar so v "
"obliki slik."

#: index.docbook:106
msgid ""
"Finally, the checkbox labeled <guilabel>Automatically load "
"images</guilabel>, allows you to control whether images on web pages are "
"loaded by default. Unless you have a very slow connection, you will probably "
"want to leave this option selected, as there are many web pages that are "
"difficult to use without images. If you don't select the option to "
"automatically load images, you can still view the text on the page, and then "
"load the images if you need them."
msgstr ""
"Nazadnje še potrditveno polje z oznako <guilabel>Samodejno naložimo "
"slike</guilabel>, ki vam omogoča nadzorovati ali naj se slike na spletnih "
"straneh naložijo kot privzeto. Če nimate zelo počasne povezave, boste "
"verjetno pustili to možnost izbrano, ker obstaja veliko spletnih strani, ki "
"se jih težko uporablja brez slik. Če ne izberete možnosti za samodejno "
"nalaganje slik, lahko še vedno gledate tekst na strani in potem naložite "
"slike, če jih potrebujete."

#: index.docbook:118
msgid "Appearance"
msgstr "Videz"

#: index.docbook:120
msgid ""
"Under this tab, you can select various options related to the use of fonts. "
"Although the shapes and sizes of fonts are often part of the design of a web "
"page, you can select some default settings for &konqueror; to use."
msgstr ""
"Pod tem jezičkom lahko izbirate različne možnosti v zvezi z uporabo "
"pisav. Čeprav sta oblika in velikost pisave pogosto del oblikovanja spletne "
"strani, lahko izberete nekaj privzetih nastavitev, da jih bo &konqueror; "
"uporabil."

#: index.docbook:125
msgid ""
"The first thing you can set here is the font size. There are two settings "
"which work together to allow you a comfortable browsing experience."
msgstr ""
"Prva stvar, ki jo lahko tu nastavite, je velikost pisave. To sta dve "
"nastavitvi, ki skupaj delujeta tako, da vam olajšata udobno brskanje."

#: index.docbook:129
msgid ""
"Firstly, you can set a <guilabel>Minimum Font Size</guilabel>. This means, "
"even if the font size is set specifically in the page you are viewing, "
"&konqueror; will ignore that instruction and never show smaller fonts than "
"you set here."
msgstr ""
"Najprej nastavite <guilabel>Najmanjšo velikost pisave</guilabel>. To "
"pomeni, da bo &konqueror; prezrl navodila, čeprav so specifično podana v "
"strani, in ne bo nikoli prikazal manjših pisav, kot jih nastavite tu."

#: index.docbook:134
msgid ""
"Next you can set a <guilabel>Medium Font Size</guilabel>. This is not only "
"the default size of text, used when the page doesn't specify sizes, but it "
"is also used as the base size that relative font sizes are calculated "
"against. That is, the <acronym>HTML</acronym> instruction "
"<quote>smaller</quote>, it means smaller than the size you set for this "
"option."
msgstr ""
"Potem nastavite <guilabel>Srednjo velikost pisave</guilabel>. To ni le "
"privzeta velikost besedila, kadar stran ne določa velikosti, temveč tudi "
"osnovna velikost, na osnovi katere se izračunajo relativne velikosti. Se "
"pravi, da ukaz <quote>smaller</quote> v <acronym>HTML</acronym> pomeni "
"manjšo velikost pisave od tiste, nastavljene pri tej izbiri."

#: index.docbook:141
msgid ""
"For either option, you can select the exact font size in points by using the "
"up/down spin control (or just typing) next to the option label."
msgstr ""
"Za obe izbiri lahko izberete točno velikost pisave v pikah tako, da "
"uporabite gumba za gor/dol (ali pa jo napišete) poleg oznake izbire."

#: index.docbook:145
msgid ""
"The remaining options are for the fonts to be associated with different "
"types of markup used in <acronym>HTML</acronym> pages. Note that many web "
"pages may override these settings. If you click anywhere on a control which "
"shows a font name, a list of font names appears, and you can select a "
"different font if you like. (If there are a lot of fonts, a vertical "
"scrollbar appears in the list to allow you to scroll through all of the "
"fonts.)"
msgstr ""
"Preostale možnosti so za pisave, ki so asocirane z različnimi tipi "
"označevalcev uporabljenih v <acronym>HTML</acronym> straneh. Vedite, da "
"veliko spletnih strani lahko razveljavi te nastavitve. Če kliknete kjerkoli "
"na kontrolo, ki prikazuje ime pisave, se pojavi seznam imen pisav, ter tako, "
"če želite, lahko izberete različno pisavo. (Če je pisav veliko, se v "
"seznamu pojavi navpični drsni trak, ki vam omogoča pregled vseh pisav.)"

#: index.docbook:153
msgid ""
"You can set a font for each <quote>type</quote> of markup, for each "
"<guilabel>Charset</guilabel>, by changing the character set in the first "
"drop down box, and then selecting a font for each category below. This would "
"take quite some time, so you may just want to set up the fonts for your "
"default character set. Most English speaking users will use iso8859-1"
msgstr ""
"Pisavo lahko nastavite za vsako <quote>vrsto</quote> označevanja za vsak "
"<guilabel>Znakovni nabor</guilabel>, če spremenite nabor znakov v prvem "
"padajočem menuju, in nato izberete pisavo za vsako od spodnjih kategorij. "
"To lahko traja precej časa, zato lahko nastavite le pisave za svoj privzeti "
"znakovni nabor. Večina slovensko govorečih uporabnikov bo uporabljala "
"iso8859-2, angleško govoreči pa iso8859-1."

#: index.docbook:160
msgid ""
"Below this, you can set a <guilabel>Font size adjustment for this "
"encoding</guilabel>. Sometimes the fonts you want to use for a particular "
"encoding or language are much larger or smaller than average, so you can use "
"this setting to bring them into line."
msgstr ""
"Pod tem lahko nastavite <guilabel>Prilagoditve velikosti pisave za to "
"kodiranje</guilabel>. Včasih so namreč pisave, ki bi jih radi uporabljali "
"za določeno kodiranje ali jezik, prevelike ali premajhne glede na "
"običajne, zato lahko uporabite to nastavitev in jih uskladite z drugimi."

#: index.docbook:165
msgid ""
"You can set a default encoding that &konqueror; should assume pages are when "
"rendering them. The default setting is <guilabel>Use language "
"encoding</guilabel>, but you can change it to any encoding available in the "
"list. You can even force the use of your default encoding using the "
"<guilabel>Enforce default Encoding charset</guilabel>, in which case "
"&konqueror; will ignore any language or charset from the page or server, and "
"will use your setting."
msgstr ""
"Nastavite lahko privzeto kodiranje, ki ga bo &konqueror; privzel za strani, "
"ko jih bo upodabljal. Privzeta nastavitev je <guilabel>Uporabljaj kodiranje "
"jezika</guilabel>"
", a to lahko spremenite v vsako kodiranje iz seznama. Lahko celo vsilite "
"svoje privzeto kodiranje z uporabo <guilabel>Vsili privzeti nabor "
"znakov</guilabel>. V tem primeru bo &konqueror; prezrl vse podatke o jeziku "
"ali naboru znakov s strani iz strežnika in bo uporabljal vaše nastavitve."

#: index.docbook:174
msgid ""
"This setting could render many pages completely unreadable. You should "
"probably only select it if you are sure it's the correct solution to any "
"problems you are having."
msgstr ""
"S to nastavitvijo lahko postanejo številne strani popolnoma neberljive. "
"Verjetno bi jo morali izbrati le, če ste prepričani, da je pravilna "
"rešitev vaših težav, kakršne pač imate."

#: index.docbook:183
msgid "Java"
msgstr "Java"

#: index.docbook:185
msgid ""
"Java allows applications to be downloaded and run by a web browser, provided "
"you have the necessary software installed on your machine. Many web sites "
"make use of Java (for example, online banking services or interactive gaming "
"sites). You should be aware that running programs from unknown sources could "
"pose a threat to the security of your computer, even if the potential extent "
"of the damage is not great."
msgstr ""
"Java omogoča aplikacijam, da se prenesejo in zaženejo s spletnim "
"brskalnikom, pod pogojem, da imate nameščeno ustrezno programsko opremo na "
"vaši mašini. Veliko spletnih strani uporablja Javo (na primer, omrežni "
"bančni servisi ali interaktivne strani za igranje). Opozorjeni bi morali "
"biti, da zaganjanje programov iz neznanih izvorov, lahko pomeni grožnjo "
"varnostemu sistemu vašega računalnika, četudi potencialni obseg škode ni "
"velik."

#: index.docbook:193
msgid ""
"The checkboxes under <guilabel>Global Settings</guilabel>"
" allows you to turn Java support on for all web sites by default. You can "
"also select to turn Java on or off for specific hosts. To add a policy for a "
"specific host, click the <guilabel>Add...</guilabel> button to bring up a "
"dialog in which you can type the host name and then choose to accept or "
"reject Java code from that particular host, which will add the domain to the "
"list on the left of the page."
msgstr ""
"Potrditvena polja združena pod <guilabel>Globalne nastavitve</guilabel>"
" vam omogočajo, da vključite podporo za javo za vse spletne strani kot "
"privzeto. Lahko tudi izberete, da vključite ali izključite javo za "
"določene gostitelje. Da dodate taktiko za določen gostitelj, kliknite na "
"gumb <guilabel>Dodaj...</guilabel>, da se vam bo pojavilo pogovorno okno v "
"katerem boste lahko vtipkali ime gostitelja in potem izbrali za sprejeti ali "
"zavrniti javansko kodo za tega gostitelja, kar bo dodalo domeno na seznam na "
"levi strani."

#: index.docbook:201 index.docbook:252
msgid ""
"You can select a host in the list, and click the "
"<guilabel>Change...</guilabel> button to choose a different policy for that "
"host. Clicking the <guilabel>Delete</guilabel>"
" button removes the policy for the selected host; after deletion, the global "
"settings will then apply to that host. You can import policies from a file "
"by clicking the <guilabel>Import...</guilabel> button. To save the current "
"list to a compressed archive file, click the <guilabel>Export...</guilabel> "
"button."
msgstr ""
"Iz seznama lahko izberete gostitelja in kliknite na gumb "
"<guilabel>Spremeni...</guilabel>, da izberete drugačno taktiko za tega "
"gostitelja. Klik na gumb <guilabel>Izbriši</guilabel> odstrani taktiko za "
"izbarnega gostitelja; po izbrisu, se bodo uveljavile splošne nastavitve za "
"tega gostitelja. Taktike lahko uvozite iz datoteke s klikom na gumb "
"<guilabel>Uvozi...</guilabel>. Da shranite trenuten seznam v stisnjeno "
"arhivsko datoteko, kliknite gumb <guilabel>Izvozi...</guilabel>."

#: index.docbook:210
msgid ""
"Finally, the group of controls labeled <guilabel>Java Runtime "
"Settings</guilabel>"
" allows you to set some options for the way in which Java should run. If you "
"select the <guilabel>Show Java Console</guilabel> option, &konqueror; will "
"open a console window from which Java applications can read and write text. "
"While most Java applications will not require such a console, it could be "
"helpful in diagnosing problems with Java applications."
msgstr ""
"Nazadnje še skupina kontrol z oznako <guilabel>Java Runtime "
"Settings</guilabel> vam omogoča, da nastavite nekaj možnosti za način po "
"katerem naj bi se Java zaganjala. Če izberete možnost <guilabel>Prikaži "
"javansko konzolo</guilabel>, bo &konqueror; odprl konzolo iz katere lahko "
"Java aplikacije berejo in pišejo besedilo. Ker večina Java aplikacij ne "
"potrebuje take konzole, je to lahko zelo v pomoč pri diagnozi problemov z "
"Java aplikacijami."

#: index.docbook:218
msgid ""
"The <guilabel>Shutdown Applet Server when inactive</guilabel> checkbox "
"allows you to save resources by closing the Java Applet Server when it is "
"not in use, rather than leaving it running in the background. Leaving this "
"disabled may make Java applets start up faster, but it will use system "
"resources when you are not using a Java applet. If you enable this, you can "
"set a timeout."
msgstr ""
"Potrditveno polje <guilabel>Izključi strežnik vstavkov, kadar ni "
"dejaven</guilabel> vam omogoča, da hranite vire tako, da zaprete strežnik "
"javanskih vstavkov, kadar se ne uporablja, namesto da bi ga pustili, da "
"teče v ozadju. Če pustite to onemogočeno, se bodo morda javanski vstavki "
"zaganjali hitreje, vendar bodo sistemski viri rabljeni tudi, kadar ne boste "
"uporabljali javanskega vstavka. Če to omogočite, lahko nastavite čas "
"zakasnitve."

#: index.docbook:225
msgid ""
"You can either opt to have &konqueror; automatically detect the Java "
"installation on your system, or specify the path to the installation "
"yourself by selecting <guilabel>Use user-specified Java</guilabel>. You may "
"want to choose the latter method, for instance, if you have multiple Java "
"installations on your system, and want to specify which one to use. If the "
"Java Virtual Machine you are using requires any special startup options, you "
"can type them in the text box labeled <guilabel>Additional Java "
"Arguments</guilabel>."
msgstr ""
"Lahko tudi izberete, da vam &konqueror; samodejno zazna Java namestitev na "
"vašem sistemu, ali pa da sami določite pot do namestite z izbiro "
"<guilabel>Uporabi Javo, ki jo določi uporabnik</guilabel>. Zadnjo izbiro "
"boste želeli izbrati, če imate več Java namestitev na vašem sistemu in "
"boste želeli določiti katera naj se uporablja. Če Java Virtual Machine ki "
"jo uporabljate zahteva posebne zagonske nastavitve, jih lahko vtipkate v "
"vnosno polje z oznako <guilabel>Dodatni Java argumenti</guilabel>."

#: index.docbook:237
msgid "JavaScript"
msgstr "JavaScript"

#: index.docbook:239
msgid "Despite the name, JavaScript is not related at all to Java."
msgstr "Kljub imenu JavaScript ni povezan z Javo."

#: index.docbook:241
msgid "The first part of this page works the same as the Java page settings."
msgstr "Prvi del te strani deluje enako kot nastavitve javanske strani."

#: index.docbook:244
msgid ""
"The checkboxes under <guilabel>Global Settings</guilabel>"
" allows you to turn JavaScript support on for all web sites by default. You "
"can also select to turn JavaScript on or off for specific hosts. To add a "
"policy for a specific host, click the <guilabel>Add...</guilabel> button to "
"bring up a dialog in which you can type the host name and then choose to "
"accept or reject JavaScript code from that particular host, which will add "
"the domain to the list on the left of the page."
msgstr ""
"Potrditvena polja združena pod <guilabel>Globalne nastavitve</guilabel>"
" vam omogočajo, da vključite podporo za JavaScript za vse spletne strani "
"kot privzeto. Lahko tudi izberete, da vključite ali izključite JavaScript "
"za določene gostitelje. Da dodate taktiko za določen gostitelj, kliknite "
"na gumb <guilabel>Dodaj...</guilabel>, da se vam bo pojavilo pogovorno okno "
"v katerem boste lahko vtipkali ime gostitelja in potem izbrali za sprejeti "
"ali zavrniti JavaScript kodo za tega gostitelja, kar bo domeno dodalo na "
"seznam na levi strani strani."

#: index.docbook:261
msgid ""
"The final set of options on this page determine what happens when a page "
"uses JavaScript to try to open a new window. You can set &konqueror; to "
"<guilabel>Allow</guilabel> all such requests, <guilabel>Ask</guilabel> each "
"time a request is made, or <guilabel>Deny</guilabel> all popup requests."
msgstr ""
"Zadnji nabor izbir na tej strani določa, kaj se zgodi, kadar stran "
"uporablja JavaScript, da odpre novo okno. &konqueror; lahko nastavite, da "
"<guilabel>Dovoli</guilabel> vse takšne zadeve, vas vsakič "
"<guilabel>Vpraša</guilabel>, ali <guilabel>Zavrne</guilabel>"
" vse zahteve po novih oknih."

#: index.docbook:272
msgid ""
"There is currently only one option on this page: <guilabel>Enable Plugins "
"globally</guilabel>"
". If you disable this checkbox, then &konqueror; will not use any plugins. "
"If you enable it, then any installed and configured plugins that it can find "
"will be used by &konqueror;"
msgstr ""
"Trenutno je na tej strani le ena izbira: <guilabel>Povsod omogoči "
"vstavke</guilabel>. Če to polje onemogočite, &konqueror; ne bo uporabljal "
"nobenih vstavkov. Če ga omogočite, bo &konqueror; uporabljal vse "
"nameščene in prikrojene vstavke."

#: index.docbook:280
msgid "Section Author"
msgstr "Avtorji razdelka"

#: index.docbook:282
msgid "This section written by:"
msgstr "Ta razdelek je napisal:"

#: index.docbook:283
msgid "Krishna Tateneni <email>tateneni@pluto.njcc.com</email>."
msgstr "Krishna Tateneni<email>tateneni@pluto.njcc.com</email>."

#: index.docbook:286
msgid "Additional material by Lauri Watts <email>lauri@kde.org</email>"
msgstr "Dodaten meterial je prispevala Lauri Watts <email>lauri@kde.org</email>"

#: index.docbook:289
msgid "CREDIT_FOR_TRANSLATORS"
msgstr "<para>Prevod: Matej Badalič <email>matej_badalic@slo.net</email></para>"
