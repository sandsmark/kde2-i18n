# SLOVENIAN TRANSLATION OF KDE DOCUMENTATION.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>, 2001.
# $Id: kioslave_floppy.po 106245 2001-07-15 20:48:40Z coolo $
# $Source$
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase 2.2\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-07-13 20:18+0200\n"
"Last-Translator: Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.1beta\n"

#: index.docbook:2
msgid "Floppy"
msgstr "Floppy"

#: index.docbook:4
msgid ""
"The floppy ioslave gives you easy access to the floppy disk drives installed "
"on your system."
msgstr ""
"Disketni ioslave vam omogoča lahek dostop do disketnih pogonov, ki so "
"nameščeni v vašem sistemu."

#: index.docbook:9
msgid ""
"The drive letter becomes the first subdirectory in the floppy &URL;. Let's "
"say there is a file <filename>logo.png</filename> on your floppy disk in "
"drive A, then the &URL; will be "
"<userinput><command>floppy:</command><replaceable>/a/logo.png</replaceable>"
"</userinput>"
msgstr ""
"Črka pogona postane prvi podimenik v &URL; diskete.Denimo, da imate na "
"vaši disketi v pogonu A datoteko <filename>logo.png</filename>, potem bo "
"&URL; "
"<userinput><command>floppy:</command><replaceable>/a/logo.png</replaceable>"
"</userinput>"

#: index.docbook:15
msgid ""
"If you want to access drive B, "
"<userinput><command>floppy:/b</command></userinput> will do it. "
"<command>floppy:/</command> is a shortcut for <command>floppy:/a</command>."
msgstr ""
"Če želite dostopati do pogona B, bi bila bližnjica takšna: "
"<userinput><command>floppy:/b</command></userinput>, "
"<command>floppy:/</command> je bližnjica za <command>floppy:/a</command>"

#: index.docbook:20
msgid ""
"Note that <command>floppy:/logo.png</command> means you have a disk drive "
"named <filename>logo.png</filename>."
msgstr ""
"Upoštevajte, da <command>floppy:/logo.png</command>"
" poeni, da imate disketo z imenom <filename>logo.png</filename>."

#: index.docbook:23
msgid ""
"To use it you need to have the mtools package installed, and the floppy "
"ioslave supports everything the various mtools command line utilities "
"support. You don't have to mount your floppy disks, simply enter "
"<userinput>floppy:/</userinput> in any &kde; 2.x app and you are able to "
"read and write from your floppy drive."
msgstr ""
"Da ga lahko uporabljate morate imeti nameščen paket mtools in disketni "
"ioslave bo podprl vse, kar podpirajo pripomočki mtools.Ni vam treba "
"priklapljati disket, preprosto vnesite <userinput>floppy:/</userinput> v "
"vsakem programu &kde; 2.x in že boste lahko brali in pisali na disketo."

#: index.docbook:30
msgid ""
"According to the mtools documentation ZIP and JAZ drives are also supported, "
"you could try <command>floppy:/z</command> and <command>floppy:/j</command> "
"to access them. Due to missing hardware this is not tested."
msgstr ""
"Sodeč po dokumentaciji mtools sta podprta tudi pogona ZIP in JAZ ,tako da "
"poskusite uporabiti <command>floppy:/z</command> in "
"<command>floppy:/j</command>. Zaradi manjkajoče strojne opreme to ni "
"preverjeno."

#: index.docbook:35
msgid ""
"Currently it is only possible to access one floppy drive exactly once at a "
"moment, it is not possible to read and write at the same time on the same "
"floppy."
msgstr ""
"Trenutno je možno dostopati do disketnega pogona le natančno enkrat v "
"trenutku, ne morete brati in pisati hkrati naisto disketo."

#: index.docbook:38
msgid "Author: Alexander Neundorf <email>neundorf@kde.org</email>"
msgstr "Avtor: Alexander Neundorf <email>neundorf@kde.org</email>"
