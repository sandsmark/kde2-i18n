# SLOVENIAN TRANSLATION OF KDE DOCUMENTATION.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>, 2001.
# $Id: kcontrol_kcmfonts.po 108375 2001-07-29 22:11:02Z scripty $
# $Source$
#
msgid ""
msgstr ""
"Project-Id-Version: documentation\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-07-28 13:10+0200\n"
"Last-Translator: Andrej Vernekar <andrej.vernekar@kiss.uni-lj.si>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2\n"

#: index.docbook:5
msgid "<firstname>Mike</firstname> <surname>McBride</surname>"
msgstr "<firstname>Mike</firstname> <surname>McBride</surname>"

#: index.docbook:9
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit "
"role=\"translator\"><contrib>Prevod:</contrib><firstname>Matej</firstname>"
"<surname>Badalič</surname><affiliation><address><email>"
"matej_badalic@slo.net</email></address></affiliation></othercredit>"

#: index.docbook:16
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:17
msgid "KControl"
msgstr "KControl"

#: index.docbook:18
msgid "fonts"
msgstr "pisave"

#: index.docbook:22
msgid "Fonts"
msgstr "Pisave"

#: index.docbook:24
msgid ""
"This module is designed to allow you to easily select different fonts for "
"different parts of the KDE Desktop."
msgstr ""
"Ta modul je oblikovan zato, da vam omogoča poenostavljen način izbiranja "
"pisav za različne dele KDE namizja."

#: index.docbook:27
msgid ""
"The panel consists of 7 different font groups to give you a lot of "
"flexibiliity in configuring your fonts:"
msgstr ""
"Pult je sestavljen iz 7 različnih skupin pisav, da vam da veliko "
"fleksibilnost v nastavitvah vaših pisav:"

#: index.docbook:30
msgid ""
"<guilabel>General</guilabel> (Used everywhere the other font groups don't "
"apply)"
msgstr ""
"<guilabel>Splošno</guilabel> (Uporabi se povsod tam, kjer se druge skupine "
"pisav ne uveljavijo)"

#: index.docbook:30
msgid ""
"<guilabel>Fixed width</guilabel> (Anywhere a non-proportional font is "
"specified)"
msgstr ""
"<guilabel>Stalna širina</guilabel> (Povsod kjer so določene ne-sorazmerne "
"pisave)"

#: index.docbook:30
msgid "<guilabel>Desktop Icon</guilabel> (Fonts to use for Desktop icon text)"
msgstr "<guilabel>Ikona namizja</guilabel> (Pisave za besedilo ikon na namizju)"

#: index.docbook:30
msgid "<guilabel>File Manager</guilabel> (Font to use in the &kde; file manager)"
msgstr ""
"<guilabel>Upravitelj datotek</guilabel> (Pisave uporabljene v datotečnem "
"urejevalniku &kde;)"

#: index.docbook:30
msgid "<guilabel>Toolbar</guilabel> (Font used in &kde; application toolbars)"
msgstr ""
"<guilabel>Orodjarna</guilabel> (Pisave uporabljene v orodnih vrsticah &kde; "
"aplikacij)"

#: index.docbook:30
msgid "<guilabel>Menu</guilabel> (Font used in &kde; application menus)"
msgstr "<guilabel>Menu</guilabel> (Pisave uporabljene v menujih &kde; aplikacij)"

#: index.docbook:30
msgid "<guilabel>Window Title</guilabel> (Font used in the window title)"
msgstr "<guilabel>Naslov okna</guilabel> (Pisave uporabljene v naslovih oken)"

#: index.docbook:53
msgid ""
"Each font has a corresponding <guibutton>choose</guibutton> button. By "
"clicking on this button, a dialog box appears. You can use this dialog box "
"to choose a new font, font style, size and character set. Then press "
"<guibutton>OK</guibutton>."
msgstr ""
"Vsaka pisava ima tudi primeren gumb z oznako <guibutton>Izberi</guibutton>. "
"S klikom na ta gumb, se pojavi pogovorno okno. To pogovorno okno lahko "
"uporabite, da izberete novo pisavo, slog pisave, velikost in razporeditev "
"znakov. Potem pritisnite gumb <guibutton>V redu</guibutton>."

#: index.docbook:58
msgid ""
"An example of the font you have chosen will be displayed in the space "
"between the font group name and the choose button."
msgstr ""
"Primer pisave, ki ste jo izbrali se prikaže v predelu med skupinami pisav "
"in gumbom za izbiranje."

#: index.docbook:61
msgid ""
"When you are done, simply click <guibutton>OK</guibutton>"
", and all necessary components of &kde; will be restarted so your changes "
"can take affect immediatly."
msgstr ""
"Ko ste končali, preprosto kliknite <guibutton>V redu</guibutton>, da bodo "
"vse potrebne komponente &kde;-ja ponovno zagnane tako, da se bodo vaše "
"spremembe takoj uveljavile."

#: index.docbook:65
msgid ""
"Finally, there is a check box to enable Anti Aliasing for fonts. The ability "
"to use Anti Aliased fonts and icons requires that you have support in both X "
"and the &Qt; toolkit, and that you have suitable fonts installed, and are "
"using the built in font serving capabilities of the X server. If you still "
"are having problems, please contact the appropriate &kde; mailing list, or "
"check the <acronym>FAQ</acronym>."
msgstr ""
"Končno je tu še potrditveno polje za, ki omogoča glajenje pisav. Da lahko "
"uporabite glajenje pisav, morate imeti to podprto tako v X-ih kot v &Qt; in "
"namestiti primerne pisave, uporabljati pa morate vgrajene strežniške "
"sposobnosti strežnika X. Če imate še vedno težave, kontaktirajte "
"ustrezen poštni seznam &kde; ali pa preverite <acronym>FAQ</acronym>."

#: index.docbook:74
msgid "Section Author"
msgstr "Avtorji razdelka"

#: index.docbook:76
msgid "This section written by Mike McBride <email>mpmcbride7@yahoo.com</email>"
msgstr "Ta razdelek je napisal Mike McBride <email>mpmcbride7@yahoo.com</email>"

#: index.docbook:79
msgid "Slightly updated for &kde; 2.2 by Lauri Watts <email>lauri@kde.org</email>"
msgstr ""
"Za &kde; 2.2 je dokument nekoliko osvežila Lauri Watts "
"<email>lauri@kde.org</email>"

#: index.docbook:82
msgid "CREDIT_FOR_TRANSLATORS"
msgstr "<para>Prevod: Matej Badalič <email>matej_badalic@slo.net</email></para>"
