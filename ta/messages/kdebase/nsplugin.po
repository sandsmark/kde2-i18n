# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-08-02 20:17GMT\n"
"Last-Translator: Thuraiappah Vaseeharan <tvasee@usa.net>\n"
"Language-Team: ¾Á¢ú <tamilinix@yahoogroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: plugin_part.cpp:110
msgid "plugin"
msgstr "¦ºÕ¸ø"

#: plugin_part.cpp:200
#, c-format
msgid "Loading Netscape plugin for %1"
msgstr "%1 ­ü¸¡É ¦¿ðÍ§¸ô ¦ºÕ¸¨Ä ²üÚ¸¢ÈÐ"

#: plugin_part.cpp:210
msgid "Unable to load Netscape plugin for "
msgstr "­¾üÌÃ¢Â ¦¿ðÍ§¸ô ¦ºÕ¸¨Ä ²üÈ ÓÊÂÅ¢ø¨Ä"

#: pluginscan.cpp:139
msgid "Netscape plugin mimeinfo"
msgstr "¦¿ðÍ§¸ô ¦ºÕ¸ø¸û"

#: pluginscan.cpp:200
msgid "Unnamed plugin"
msgstr "¦ÀÂÃüÈ ¦ºÕ¸ø"

#: pluginscan.cpp:344 pluginscan.cpp:347
msgid "Netscape plugin viewer"
msgstr "¦¿ðÍ§¸ô ¦ºÕ¸ü ¸¡ðÊ"

#: pluginscan.cpp:383
msgid "nspluginscan"
msgstr "nspluginscan"

#: rc.cpp:1
msgid "Netscape Plugin Config"
msgstr "¦¿ðÍ§¸ô ¦ºÕ¸ø¸û"

#: rc.cpp:2
msgid "Scan"
msgstr "ÅÕ¼ø"

#: rc.cpp:3
msgid "&Scan for new plugins"
msgstr "Ò¾¢Â ¦ºÕ¸ø¸Ùì¸¡¸ ÅÕÎ"

#: rc.cpp:4
msgid "Click here to scan for newly installed netscape plugins now."
msgstr ""
"Ò¾¢¾¡¸ ¦¿ð§¸ô ¦ºÕ¸ø¸ÙìÌ ÅÕ¼ ­í§¸ ìÇ¢ì "
"¦ºöÂ×õ."

#: rc.cpp:5
msgid "Scan for new plugins at &KDE startup"
msgstr "KDE ¦¾¡¼íÌõ §À¡Ð Ò¾¢Â ¦ºÕ¸ø¸Ùì¸¡¸ ÅÕÎ"

#: rc.cpp:6
msgid ""
"If this option is enabled, KDE will look for new netscape plugins everytime "
"it starts up. This makes it easier for you if you often install new plugins, "
"but it may also slow down KDE startup. You might want to disable this "
"option, especially if you seldom install plugins."
msgstr ""
"­ó¾ Å¢ÕôÀò¨¾ò §¾÷× ¦ºö¾¡ø, ´ù¦Å¡Õ Ó¨È KDE "
"¬ÃõÀ¢ìÌõ §À¡Ðõ «Ð Ò¾¢Â ¦¿ð§¸ô ¦ºÕ¸ø¸¨Çò "
"§¾Îõ. ¿£í¸û «Êì¸Ê Ò¾¢Â ¦ºÕ¸ø¸¨Ç ¿¢ÚÅ¢É¡ø "
"­Ð Á¢¸×õ ÀÂýÀÎõ. ¬É¡ø, ­Ð KDE ¦¾¡¼íÌõ "
"§Å¸ò¨¾ì Ì¨ÈìÌõ. «¾É¡ø, ¿£í¸û ±ô§À¡¾¡ÅÐ "
"¾¡ý ¦ºÕ¸ø¸¨Ç ¿¢Ú×Å£÷¸¦ÇÉ¢ý ­ù Å¢ÕôÀò¨¾ "
"Ó¼ì¸ø ¿Äõ."

#: rc.cpp:7
msgid "Scan Directories"
msgstr "«¨¼×¸¨Ç ÅÕÎ"

#: rc.cpp:9
msgid "&New"
msgstr "Ò¾¢Â"

#: rc.cpp:10
msgid "&Down"
msgstr "¸£ú"

#: rc.cpp:11
msgid "&Up"
msgstr "§Áø"

#: rc.cpp:12
msgid "..."
msgstr "..."

#: rc.cpp:13
msgid "Plugins"
msgstr "¦ºÕ¸ø¸û"

#: rc.cpp:15
msgid "Value"
msgstr "Á¾¢ôÒ"

#: rc.cpp:16
msgid "Here you can see a list of the netscape plugins KDE has found."
msgstr ""
"KDE ¸ñÎÀ¢Êò¾ ¦¿ðÍ§¸ô ¦ºÕ¸ø¸Ç¢ý ÀðÊÂ¨Ä ­í§¸ "
"À¡÷ì¸Ä¡õ."

#: rc.cpp:17
msgid "Use &artsdsp to pipe plugin sound through aRts"
msgstr ""
"¦ºÕ¸ø ´Ä¢¨Â aRts ­ë§¼ ¦ºÖò¾ &artsdsp ­¨Éô "
"ÀÂýÀÎò¾×õ"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""
"º¢ÅÌÁ¡÷ ºñÓ¸Íó¾Ãõ,§¸¡Á¾¢ º¢ÅÌÁ¡÷,Ð¨ÃÂôÀ¡ "
"Åº£¸Ãý"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "sshanmu@yahoo.com,gomathiss@hotmail.com,t_vasee@yahoo.com"

#: control/nsconfig.cpp:137
msgid ""
"Do you want to apply your changes before the scan? Otherwise the changes "
"will be lost."
msgstr ""
"ÅÕ¼ Óý ¯í¸¸û Á¡üÈí¸¨Çô À¢Ã§Â¡¸¢ì¸ "
"Å¢ÕôÀÁ¡? «ý§Èø «õÁ¡üÈí¸û ­Æì¸ôÀÎõ."

#: control/nsconfig.cpp:146
msgid "Scanning for plugins"
msgstr "¦ºÕ¸ø¸Ùì¸¡¸ ÅÕÎ¸¢ÈÐ"

#: control/nsconfig.cpp:154
msgid ""
"The nspluginscan executable can't be found. Netscape plugins won't be "
"scanned."
msgstr ""
"nspluginscan ¦ºÂÄ¢¨Âì ¸ñÎÀ¢Êì¸ ÓÊÂÅ¢ø¨Ä. "
"¦¿ðÍ§¸ô ¦ºÕ¸ø¸û ÅÕ¼ôÀ¼¡Ð."

#: control/nsconfig.cpp:183
msgid ""
"<h1>Netscape Plugins</h1>"
" The Konqueror web browser can use netscape plugins to show special content, "
"just like the Navigator does. Please note that how you have to install "
"netscape plugins may depend on your distribution. A typical place to install "
"them is for example '/opt/netscape/plugins'."
msgstr ""
"<h1>¦¿ð§¸ô ¦ºÕ¸ø¸û</h1> §¿Å¢§¸ð¼÷ §À¡ø "
"¸¡ý¦¸¡Ã÷ Å¨Ä §Á§Ä¡Ê, ¦¿ð§¸ô ¦ºÕ¸ø¸¨Ç "
"ÀÂýÀÎò¾¢ º¢ÈôÒ ¾¸Å¨Ä ¸¡ðÎõ. ¦¿ð§¸ô "
"¦ºÕ¸ø¸¨Ç ±ôÀÊ ¿¢ÚÅ §ÅñÎõ ±ýÀÐ ¯í¸û "
"ÀÃôÀ£ðÊüÌ ¾Ìó¾¡Ú §À¡ø ­ÕìÌõ. ¯¾¡Ã½Á¡¸, ´Õ "
"­¼õ, '/opt/netscape/plugins'."

#: control/nsconfig.cpp:308
msgid "Select plugin scan directory"
msgstr "¦ºÕø ÅÕ¼ø «¨¼Å¢¨Éò §¾÷ó¦¾Îì¸×õ"

#: control/nsconfig.cpp:393
msgid "Netscape Plugins"
msgstr "¦¿ðÍ§¸ô ¦ºÕ¸ø¸û"

#: control/nsconfig.cpp:416
msgid "Plugin"
msgstr "¦ºÕ¸ø"

#: control/nsconfig.cpp:433
msgid "MIME type"
msgstr "MIME Å¨¸"

#: control/nsconfig.cpp:438
msgid "Description"
msgstr "Å¢ÅÃõ"

#: control/nsconfig.cpp:443
msgid "Suffixes"
msgstr "Å¢Ì¾¢¸û"

#: viewer/nsplugin.cpp:459
#, c-format
msgid "Requesting %1"
msgstr "%1 §¸ð¸¢ÈÐ"

#: viewer/viewer.cpp:224
msgid "Error connecting to DCOP server"
msgstr "DCOP ÀÃ¢Á¡È¢Ô¼ý ¦¾¡¼÷Ò¦¸¡ûÅ¾¢ü ¾ÅÚ"

#: viewer/viewer.cpp:225
msgid ""
"There was an error connecting to the Desktop\n"
"communications server.  Please make sure that\n"
"the 'dcopserver' process has been started, and\n"
"then try again.\n"
msgstr ""
"§Á¨ºò ¦¾¡¼÷Òî §º¨ÅÂ¸òÐ¼ý ¦¾¡¼÷Ò ¦¸¡ûÅ¾¢ø "
"¾ÅÚ §¿÷ó¾Ð.\n"
"'dcopserver' ±Ûõ ¿¢Ãø ¬ÃõÀ¢òÐûÇ¦¾ýÀ¨¾ "
"¯Ú¾¢¦ºö¾À¢ý\n"
"Á£ñÎõ ÓÂÄ×õ\n"
