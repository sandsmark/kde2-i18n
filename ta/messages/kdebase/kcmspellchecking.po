# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 02:05+0200\n"
"PO-Revision-Date: 2001-07-16 20:25GMT\n"
"Last-Translator: Thuraiappah Vaseeharan <tvasee@usa.net>\n"
"Language-Team: ¾Á¢ú <tamilinix@yahoogroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: spellchecking.cpp:46
msgid "Spell Checking Settings"
msgstr "±ØòÐôÀ¢¨Æ ¾¢Õò¾ «¨ÁôÒì¸û"

#: spellchecking.cpp:78
msgid ""
"<h1>Spell Checking</h1><p>This control module allows you to configure the "
"KDE spell checking system.  You can configure:<ul><li> which spell checking "
"program to use<li> which types of spelling errors are identified<li> which "
"dictionary is used by default.</ul><br>The KDE spell checking system "
"(KSpell) provides support for two common spell checking utilities: ASpell "
"and ISpell.  This allows you to share dictionaries between KDE applications "
"and non-KDE applications.</p>"
msgstr ""
"<h1>Spell Checking</h1><p>This control module allows you to configure the "
"KDE spell checking system.  You can configure:<ul><li> which spell checking "
"program to use<li> which types of spelling errors are identified<li> which "
"dictionary is used by default.</ul><br>The KDE spell checking system "
"(KSpell) provides support for two common spell checking utilities: ASpell "
"and ISpell.  This allows you to share dictionaries between KDE applications "
"and non-KDE applications.</p>"
