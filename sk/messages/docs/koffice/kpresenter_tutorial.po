# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-07-27 16:49CET\n"
"Last-Translator: Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>\n"
"Language-Team: Slovak <sk-i18n@lists.linux.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.3\n"

#: index.docbook:8
msgid "A Step-By-Step Tutorial"
msgstr "Krok za krokom"

#: index.docbook:10
msgid ""
"In this chapter, &kpresenter; is introduced using a simple tutorial. We "
"shall walk through the most basic steps that are involved in creating a "
"presentation, and adding some basic effects."
msgstr ""
"Táto kapitola obsahuje krok za krokom úvod do programu &kpresenter;. Pomocou "
"najjednoduchších krokov vám ukáže, ako vytvoriť prezentáciu a ako do nej "
"pridať základné efekty."

#: index.docbook:17
msgid "Start a new document"
msgstr "Vytvorenie nového dokumentu"

#: index.docbook:19
msgid "When you start &kpresenter;, the Choose Template dialog appears."
msgstr "Po spustení programu &kpresenter; sa objaví dialóg pre výber šablóny."

#: index.docbook:23
msgid "The <guilabel>Choose a Template</guilabel> dialog"
msgstr "Dialóg <guilabel>Vybrať</guilabel> šablónu"

#: index.docbook:34
msgid ""
"The default option is <guilabel>Start with an empty document</guilabel>. "
"Select the template labeled <guilabel>Title</guilabel> (highlighted in red) "
"by clicking on it. This also selects <guilabel>Create new document from a "
"template</guilabel>."
msgstr ""
"Štandardná voľba je <guilabel>Začať s prázdnym dokumentom</guilabel>. "
"Kliknutím vyberte šablónu označenú ako <guilabel>Title</guilabel> (označená "
"červenou farbou). Tým sa automaticky vyberie <guilabel>Vytvoriť nový "
"dokument zo šablóny</guilabel>."

#: index.docbook:41
msgid ""
"Now click <guibutton>OK</guibutton>. This brings up the slide editor window, "
"where you can view and edit the slides (and objects contained in them) in "
"your document. At the moment, we just have one slide, with one object on it, "
"which is a text box."
msgstr ""
"Teraz stlačte tlačidlo <guibutton>OK</guibutton>. Tým sa zobrazí okno "
"editora obrázkov, kde môžete prezerať a upravovať obrázky prezentácie (a "
"objekty v nich). Momentálne máme iba jeden obrázok, ktorý obsahuje iba jeden "
"textový box."

#: index.docbook:48
msgid "The slide editor"
msgstr "Editor obrázkov"

#: index.docbook:56
msgid ""
"Double-click the text box. The cursor changes to a vertical bar to show that "
"you can now type some text."
msgstr ""
"Dvakrát kliknite na textový box. Kurzor sa zmení na vertikálnu čiaru aby "
"zvýraznil, že teraz môžete zadať nejaký text."

#: index.docbook:61
msgid "The text insertion cursor"
msgstr "Kurzor pre vkladanie textu"

#: index.docbook:70
msgid "Go ahead, type some text!"
msgstr "Tak tam niečo napíšte!"

#: index.docbook:74
msgid "Adding text"
msgstr "Pridanie textu"

#: index.docbook:83
msgid ""
"Click away from the text to de-select the text box when you are done typing."
msgstr "Po napísaní kliknite mimo textu, aby ste zrušili výber textového boxu."

#: index.docbook:91
msgid "Add a new page"
msgstr "Pridanie novej stránky"

#: index.docbook:93
msgid ""
"Let's now add a new slide to our document. To do so, click the "
"<guimenu>Insert</guimenu> menu, and then click on <guimenuitem>Page</"
"guimenuitem>."
msgstr ""
"Teraz skúsime pridať do prezentácie nový obrázok. Kliknite na menu "
"<guimenu>Vložiť</guimenu> a vyberte <guimenuitem>Stránka</guimenuitem>."

#: index.docbook:99
msgid "Inserting a page from the menu"
msgstr "Vloženie stránky z menu"

#: index.docbook:108
msgid "This brings up the <guilabel>Insert Page</guilabel> dialog."
msgstr "Tým sa zobrazí dialóg <guilabel>Vložiť stránku</guilabel>."

#: index.docbook:112
msgid "The <guilabel>Insert Page</guilabel> dialog"
msgstr "Dialóg <guilabel>Vložiť stránku</guilabel>"

#: index.docbook:122
msgid ""
"Click <guibutton>OK</guibutton> to accept the defaults, which will add a new "
"page after page 1."
msgstr ""
"Stlačením <guibutton>OK</guibutton> použijeme štandardné nastavenie, čím "
"pridáme novú stránku za stránku 1."

#: index.docbook:127
msgid ""
"The <guilabel>Choose</guilabel> dialog comes up so that we can decide what "
"the new slide should look like. This time, double click on the "
"<guilabel>One</guilabel> template (highlighted in red.)"
msgstr ""
"Opäť sa zobrazí dialóg <guilabel>Vybrať</guilabel> aby sme určili, ako má "
"nová stránka vyzerať. Teraz dvakrát klikneme na šablónu <guilabel>One "
"Column</guilabel> (zvýraznená červernou farbou)."

#: index.docbook:133
msgid "Choosing a template for the new page"
msgstr "Výber šablóny pre novú stránku"

#: index.docbook:142
msgid ""
"The new slide now appears in the editing window. To change between pages of "
"your presentation, you can select slides in the pane to the left "
"(highlighted in red for this screenshot)."
msgstr ""
"V okne sa objaví nový obrázok. Pomocou ľavej časti okna môžete prechádzať "
"medzi obrázkami v prezentácii (zvýraznené červenou farbou)."

#: index.docbook:148
msgid "The slides list"
msgstr "Zoznam obrázkov"

#: index.docbook:156
msgid ""
"The newly inserted page has two text boxes. There is one for a title, and "
"another to contain a bulleted list of items."
msgstr ""
"Stránka, ktorú sme práve vložili, obsahuje dva textové boxy. Jeden je určený "
"pre titulok stránky a druhý bude obsahovať zoznam položiek s odrážkami."

#: index.docbook:161
msgid "The new page"
msgstr "Nová stránka"

#: index.docbook:169
msgid ""
"Double-click and type a title. Then double-click on the second text box. "
"Note that a bullet automatically appears when you start typing. Type some "
"text and end the paragraph by pressing the <keycap>Enter</keycap> or "
"<keycap>Return</keycap> key. As you type new paragraphs, bullets "
"automatically appear in front of them."
msgstr ""
"Dvakrát kliknite a napíšte titulok. Potom dvakrát kliknite na druhý box. "
"Všimnite si, že odrážky sa automaticky zobrazia v momente, keď začnete písať "
"text. Niečo napíšte a ukončite odstavec pomocou klávesu <keycap>Enter</"
"keycap> alebo <keycap>Return</keycap>. Ako píšete ďalšie odstavce, odrážky "
"sa pred nimi automaticky zobrazujú."

#: index.docbook:177
msgid "Adding text to the second page"
msgstr "Pridávanie textu na druhý obrázok"

#: index.docbook:185
msgid "You can de-select the text box by clicking away from it."
msgstr "Výber textového boxu zrušíte kliknutím mimo neho."

#: index.docbook:192
msgid "Insert a picture"
msgstr "Vloženie kresby"

#: index.docbook:194
msgid ""
"Let's go back to the first page now. Use the list of slides on the left of "
"your screen."
msgstr ""
"Teraz sa vráťme späť na prvý obrázok. Použite na to zoznam obrázkov vľavo."

#: index.docbook:199
msgid ""
"In this section, we'll liven our presentation up a bit by adding a nice logo "
"to the title page. To do so, the first step is to click on the "
"<guimenu>Insert</guimenu> menu item, and then on <guimenuitem>Picture</"
"guimenuitem>."
msgstr ""
"V tejto kapitole oživíme prezentáciu pridaním pekného loga na titulnú "
"stránku. Najprv kliknite na menu <guimenu>Vložiť</guimenu> a vyberte "
"<guimenuitem>Obrázok</guimenuitem>."

#: index.docbook:206
msgid "Using the menu to add an image"
msgstr "Použitie menu pre pridanie kresby"

#: index.docbook:214
msgid ""
"This brings up a file selection dialog. To learn about this or other "
"standard &kde; dialog boxes in detail, please consult the &kde; "
"documentation. You can browse by clicking on <guiicon>folder</guiicon> icons "
"or by using the <guiicon>browser</guiicon> style buttons on the toolbar "
"(highlighted in red.) Clicking the <guiicon>up arrow</guiicon> takes you up "
"one directory level."
msgstr ""
"Tým sa zobrazí dialóg pre výber súboru. Ovládanie tohto a aj ostatných "
"štandardných dialógov &kde; je detailne popísané v dokumentácii pre "
"prostredie &kde;. Pomocou kliknutia na <guiicon>priečinok</guiicon> môžete "
"medzi nimi prechádzať, rovnako ako pomocou tlačidiel v štýle "
"<guiicon>prehliadačov</guiicon> na paneli (zvýraznené červenou farbou). "
"Kliknutím na <guiicon>šipku hore</guiicon> sa presuniete o jednu úroveň "
"priečinkov vyššie."

#: index.docbook:223
msgid ""
"Find the file named <filename>koffice-logo.png</filename>, which may be in a "
"different directory than the one shown in the screenshot below. You can also "
"choose any other graphic file if you like! Select the file, and click "
"<guibutton>Open</guibutton>."
msgstr ""
"Nájdite súbor <filename>koffice-logo.png</filename>, ktorý je možno v inom "
"priečinku, než aký je zobrazený na obrázku dole. Ale kľudne môžete vybrať aj "
"iný obrázok, ak chcete! Vyberte súbor a stlačte <guibutton>Otvoriť</"
"guibutton>."

#: index.docbook:230
msgid "Choosing a picture to add"
msgstr "Výber pridávaného obrázka"

#: index.docbook:238
msgid ""
"The logo graphic is now visible in the top left corner of the editing "
"window. There are selection handles (little black squares) visible around "
"the border of the graphic."
msgstr ""
"Grafické logo je teraz v ľavom hornom rohu okna. Okolo neho sú viditeľné "
"ovládacie prvky (malé čierne štvorce)."

#: index.docbook:244
msgid "The newly added image"
msgstr "Práve pridaný obrázok"

#: index.docbook:252
msgid ""
"Place the mouse cursor anywhere in the middle of the logo, and drag it to "
"the middle of the title page."
msgstr ""
"Umiestnite kurzor myši do stredu obrázka a ťahajte ho do stredu titulnej "
"stránky."

#: index.docbook:257
msgid "Moving the image into place"
msgstr "Presun obrázku na miesto"

#: index.docbook:265
msgid "That's it. Now you have a picture on the title page!"
msgstr "A je to. Teraz máme na titulnej strane obrázok!"

#: index.docbook:272
msgid "Add a shadow to the title text"
msgstr "Pridanie tieňa titulku"

#: index.docbook:274
msgid ""
"Let's continue enhancing our title page by adding a shadow behind the title. "
"<mousebutton>Right</mousebutton> click anywhere on the title text. This "
"achieves two things: the text box containing the title is selected, and a "
"menu pops-up."
msgstr ""
"Teraz vylepšíme titulnú stranu pridaním tieňa za titulok. Stlačte "
"<mousebutton>pravé</mousebutton> tlačidlo myši niekde na titulku. Tým sa "
"stanú dve veci: textový box titulku sa vyberie a zobrazí sa kontextové menu."

#: index.docbook:281
msgid "Select the <guimenuitem>Shadow</guimenuitem> option in the pop-up menu."
msgstr "Z menu vyberte možnosť <guimenuitem>Tieň</guimenuitem>"

#: index.docbook:285
msgid "The context menu"
msgstr "Kontextové menu"

#: index.docbook:293
msgid ""
"The <guilabel>Shadow</guilabel> dialog pops up. The distance between the "
"shadow and the text is currently 0 so the shadow cannot be seen (this part "
"of the dialog box is highlighted in red.)"
msgstr ""
"Zobrazí sa dialóg <guilabel>Tieň</guilabel>. Vzdialenosť medzi tieňom a "
"textom je momentálne 0, takže tieň nie je vidieť (je to červená časť "
"dialógu)."

#: index.docbook:299
msgid "The <guilabel>Shadow</guilabel> dialog"
msgstr "Dialóg <guilabel>Tieň</guilabel>"

#: index.docbook:308
msgid ""
"Increase the distance value to 3. The effect of changing the distance can be "
"seen in the preview window. Now click <guibutton>OK</guibutton>."
msgstr ""
"Hodnotu vzdialenosti zvýšte na 3. V okne náhľadu je vidieť, ako sa efekt "
"prejaví. Teraz stačte <guibutton>OK</guibutton>."

#: index.docbook:313
msgid "Adding a shadow to the title"
msgstr "Pridanie tieňa titulku"

#: index.docbook:321
msgid "Now the title has a shadow!"
msgstr "A titulok má tieň!"

#: index.docbook:325
msgid "The new shadowed title"
msgstr "Nový tieňovaný titulok"

#: index.docbook:336
msgid "Change the color of the title text"
msgstr "Zmena farby textu titulku"

#: index.docbook:338
msgid ""
"Let's finish by changing the color of the title text from black to blue. To "
"do so, select the title text by double-clicking the text box."
msgstr ""
"Skončíme zmenou farby titulku z čiernej na modrú. Vyberte textový box "
"titulku dvojitým kliknutím."

#: index.docbook:343
msgid ""
"Change the color to blue by clicking on the <guiicon>dark blue</guiicon> "
"icon in the color palette on the right side of the editing window (this icon "
"is highlighted in red.)"
msgstr ""
"Farbu zmeníte kliknutím na ikonu <guiicon>tmavomodrej farby</guiicon> na "
"palete farieb vpravo (táto ikona je zvýraznená červenou farbou.)"

#: index.docbook:349
msgid "The color palette"
msgstr "Paleta farieb"

#: index.docbook:357
msgid ""
"Changing the color of the selected text to blue changes its appearance. The "
"exact color that highlighted text turns depends on your system color scheme."
msgstr ""
"Zmenou farby sa vybraný text zmení. Presná farba, na ktorú sa vybraný text "
"zmení, závisí na vašom aktuálnom nastavení farebnej schémy."

#: index.docbook:363
msgid "Highlighted text"
msgstr "Zvýraznený text"

#: index.docbook:371
msgid "Now click away from the text to de-select it."
msgstr "Teraz kliknite mimo text, aby ste zrušili jeho výber."

#: index.docbook:375
msgid "The finished title"
msgstr "Dokončený titulok"

#: index.docbook:383
msgid ""
"Now that there are two slides, why not try a slide show! To start the slide "
"show, press the <guiicon>play</guiicon> button (the grey arrow) on the top "
"toolbar. The first slide should appear on your screen."
msgstr ""
"Takže máme dva obrázky, skúsme teda spustiť prezentáciu! Stlačte tlačidlo "
"<guiicon>Štart</guiicon> (šedá šípka) v hornom paneli nástrojov. Na "
"obrazovke sa objaví prvý obrázok."

#: index.docbook:389
msgid "The first slide"
msgstr "Prvý obrázok"

#: index.docbook:397
msgid ""
"To advance from the first slide to the next, just click anywhere on the "
"screen, or use the <keycap>Page Down</keycap> key."
msgstr ""
"Kliknutím kdekoľvek prejdete na nasledujúci obrázok. Môžete použiť aj "
"tlačidlo <keycap>Page Down</keycap>."

#: index.docbook:402
msgid "The second slide"
msgstr "Druhý obrázok"

#: index.docbook:410
msgid ""
"To exit the slide show, <mousebutton>right</mousebutton> click, and then "
"select the <guimenuitem>Exit presentation</guimenuitem> option from the pop-"
"up menu."
msgstr ""
"Pre ukončenie prezentácie kliknite <mousebutton>pravým</mousebutton> "
"tlačidlom myši a z kontextového menu vyberte <guimenuitem>Koniec "
"prezentácie</guimenuitem>."
