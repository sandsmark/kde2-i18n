# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-08-08 17:08CET\n"
"Last-Translator: Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>\n"
"Language-Team: Slovak <sk-i18n@lists.linux.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.3\n"

#: index.docbook:5
msgid "<firstname>Mike</firstname> <surname>McBride</surname>"
msgstr "<firstname>Mike</firstname> <surname>McBride</surname>"

#: index.docbook:9
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Stanislav</firstname> "
"<surname>Višňovský</surname> <affiliation> <address><email>visnovsky@nenya."
"ms.mff.cuni.cz</email></address> </affiliation><contrib>Preklad</contrib></"
"othercredit>"

#: index.docbook:16
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:17
msgid "KControl"
msgstr "KControl"

#: index.docbook:18
msgid "mouse"
msgstr "myš"

#: index.docbook:21
msgid "Mouse"
msgstr "Myš"

#: index.docbook:23
msgid ""
"This module allows you to configure your pointing device. Your pointing "
"device may be a mouse, a trackball, a touchpad, or another piece of hardware "
"that performs a similar function."
msgstr ""
"Tento modul nastavuje vašu myš, trackball, touchpad alebo iný hardvér "
"fungujúci ako myš."

#: index.docbook:27
msgid ""
"This module is divided into 2 tabs: <link linkend=\"mouse-general\">General</"
"link> and <link linkend=\"mouse-advanced\">Advanced</link>"
msgstr ""
"Tento modul je rozdelený na dve záložky: <link linkend=\"mouse-general"
"\">Všeobecné</link> and <link linkend=\"mouse-advanced\">Rozšírené</link>"

#: index.docbook:32
msgid "General"
msgstr "Všeobecné"

#: index.docbook:34
msgid "Button Mapping"
msgstr "Mapovanie tlačidiel"

#: index.docbook:34
msgid ""
"If you are left-handed, you may prefer to swap the functions of the "
"<mousebutton>left</mousebutton> and <mousebutton>right</mousebutton> buttons "
"on your pointing device by choosing the <quote>left-handed</quote> option. "
"If your pointing device has more than two buttons, only those that function "
"as the <mousebutton>left</mousebutton> and <mousebutton>right</mousebutton> "
"buttons are affected. For example, if you have a three-button mouse, the "
"<mousebutton>middle</mousebutton> button is unaffected."
msgstr ""
"Ak ste ľavák, možno máte radi vymenené funkcie <mousebutton>ľavého</"
"mousebutton> a <mousebutton>pravého</mousebutton> tlačidla myši pomocou "
"voľby <quote>pre ľavákov</quote>. Ak má vaša myš viac ako dve tlačidlá, "
"vymenené budú iba <mousebutton>ľavé</mousebutton> a <mousebutton>pravé</"
"mousebutton> tlačidlá. Napríklad pre trojtlačidlovú myš nebude "
"<mousebutton>stredné</mousebutton> tlačidlo zmenené."

#: index.docbook:34
msgid "Single-click to open files and folders"
msgstr "Jednoduché kliknutie otvára súbory a priečinky"

#: index.docbook:34
msgid ""
"This is the default setting for &kde;. Clicking once on an icon will open "
"it. To select you can drag around the icon(s) or <keycombo action=\"simul"
"\">&Ctrl;<mousebutton>Right</mousebutton></keycombo> click, or simply click "
"and hold to drag it."
msgstr ""
"Toto štandardné nastavenie &kde;. Jednoduchým kliknutím na ikonu ju "
"otvoríte. Aby ste ju iba vybrali, musíte ju vybrať ťahaním alebo použiť "
"<keycombo action=\"simul\">&Ctrl;<mousebutton>pravé</mousebutton></keycombo> "
"alebo jednoducho kliknúť, a ťahať ju."

#: index.docbook:34
msgid "Change pointer shape over icons"
msgstr "Zmeniť tvar kurzora ak je nad ikonou"

#: index.docbook:34
msgid ""
"When this option is checked, the shape of the mouse pointer changes whenever "
"it is over an icon."
msgstr ""
"Ak je zapnutá táto možnosť, tvar myšy sa zmení kedykoľvek kurzor prejde "
"ponad ikonu."

#: index.docbook:34
msgid ""
"This option should be checked in most situations. It gives more visual "
"feedback and says, in essence, if you click here, something will happen."
msgstr ""
"Táto voľba by mala byť obvykle zapnutá. Dáva viac informácií, predovšetkým "
"hovorí, že ak teraz kliknete, niečo sa stane."

#: index.docbook:34
msgid "Use double-click to activate or open"
msgstr "Dvojité kliknutie aktivuje/otvára"

#: index.docbook:34
msgid ""
"If this option is not checked, icons/files will be opened with a single "
"click of the <mousebutton>left</mousebutton> mouse-button. This default "
"behavior is consistent with what you would expect when you click links in "
"most web browsers. If checked however, icons/files will be opened with a "
"double click, while a single click will only select the icon or file. This "
"is the behavior you may know from other desktops or operating systems."
msgstr ""
"Ak táto voľba nie je aktívna, ikony a súbory budú otvárané jednoduchým "
"kliknutím <mousebutton>ľavého</mousebutton> tlačidla myši. Toto chovanie "
"odpovedá tomu, čo očakávate od web prehliadača. Ale ak použijete túto voľbu, "
"ikony a súbory sa budú otvárať dvojitým kliknutím, jednoduché iba ikonu/"
"súbor vyberie. Toto je chovanie, ktoré poznáte z iných operačných systémov a "
"prostredí."

#: index.docbook:34
msgid "Visual feedback on activation"
msgstr "Vizuálna odozva pri aktivácii"

#: index.docbook:34
msgid ""
"When this option is checked, &kde; gives you visual feedback whenever you "
"click on something and activate it."
msgstr ""
"Táto voľba nastaví, aby &kde; vždy vizuálne zobrazilo, keď kliknete a tým "
"niečo aktivujete."

#: index.docbook:34
msgid "Large Cursor"
msgstr "Veľký kurzor"

#: index.docbook:34
msgid "This will give &kde; a larger mouse cursor."
msgstr "Toto nastaví väčší kurzor myši."

#: index.docbook:34
msgid "The large cursor will not be seen until &kde; is restarted."
msgstr "Aby sa použil veľký kurzor, musíte &kde; reštartovať."

#: index.docbook:34
msgid "Automatically Select Icons"
msgstr "Automaticky vyberať ikony"

#: index.docbook:34
msgid ""
"If you check this option, pausing the mouse pointer over an icon on the "
"screen will automatically select that icon. This may be useful when single "
"clicks opens icons, and you only want to select the icon without opening it."
msgstr ""
"Pri zapnutej tejto voľbe nechanie kurzoru nad ikonou ju automaticky vyberie. "
"To sa hodí pri aktivácii jednoduchým kliknutím, keď chcete ikonu iba vybrať "
"a nie ju otvoriť."

#: index.docbook:34
msgid ""
"You can still select an icon or file without opening it, even when "
"<guilabel>Single Click Opens</guilabel> is checked. This is done by either "
"holding down the &Ctrl; key and then clicking, or by dragging a box around "
"the icon(s) or file(s) which you wish to select."
msgstr ""
"Ikonu ale môžete vybrať aj tak, dokonca aj so zapnutým <guilabel>Jednoduché "
"kliknutie otvára súbory a priečinky</guilabel>. Môžete to urobiť buď držaním "
"klávesu &Ctrl; pri klikaní, alebo výberom ťahaním."

#: index.docbook:34
msgid ""
"If you have activated this option, you can use the slider to select how long "
"the mouse pointer must rest on an icon before it becomes selected."
msgstr ""
"V prípade aktivovanej tejto voľby môžete posuvníkom nastaviť, ako dlho musí "
"myš nad ikonou zostať, aby ju vybrala."

#: index.docbook:128
msgid "Advanced"
msgstr "Pokročilé"

#: index.docbook:130
msgid "Pointer Acceleration"
msgstr "Zrýchlenie ukazovátka"

#: index.docbook:130
msgid ""
"This option allows you to change the relationship between the distance that "
"the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)"
msgstr ""
"Táto možnosť mení vzťah medzi vzdialenosťou na obrazovke a relatívnym "
"posunom fyzického zariadenia (myši, trackballu apod)."

#: index.docbook:130
msgid ""
"A high value for the acceleration multiplier will lead to large movements of "
"the mouse pointer on the screen, even when you only make a small movement "
"with the physical device."
msgstr ""
"Vysoké číslo zrýchlenia znamená rýchly posun kurzoru po obrazovke aj v "
"prípade, že fyzické zariadenie posuniete iba o kúsok."

#: index.docbook:130
msgid ""
"A multiplier between <guilabel>1x</guilabel> and <guilabel>3x</guilabel> "
"will works well for many systems. With a multiplier over <guilabel>3x</"
"guilabel> the mouse pointer may become difficult to control."
msgstr ""
"Na väčšine počítačov dobre funguje zrýchlenie <guilabel>1x</guilabel> až "
"<guilabel>3x</guilabel>. Ak použijete väčšiu hodnotu, kurzor myši sa bude "
"ťažko ovládať."

#: index.docbook:130
msgid "Pointer Threshold"
msgstr "Prah citlivosti"

#: index.docbook:130
msgid ""
"The threshold is the smallest distance that the mouse pointer must move on "
"the screen before acceleration has any effect. If the movement is within the "
"threshold, the mouse pointer moves as if the acceleration were set to "
"<guilabel>1x</guilabel>."
msgstr ""
"Prah je najmenšia vzdialenosť, o ktorú sa musí myš posunúť, aby sa pohyb "
"začal zrýchľovať. Ak ňou pohybujete na vzdialenosť menšiu, kurzor sa "
"pohybuje ako keby bolo zrýchlenie nastavené na <guilabel>1x</guilabel>."

#: index.docbook:130
msgid ""
"Thus, when you make small movements with the physical device (&eg; mouse), "
"you still have fine control of the mouse pointer on the screen, whereas "
"larger movements of the physical device will move the mouse pointer rapidly "
"to different areas on the screen."
msgstr ""
"Takže pri malých posunoch fyzického zariadenia máte stále dostatočnú "
"kontrolu nad kurzorom na obrazovke, ale pre veľký posun fyzického zariadenia "
"presunie kurzor po obrazovke rýchlo."

#: index.docbook:130
msgid ""
"You can set the threshold value by dragging the slider button or by clicking "
"the up/down arrows on the spin-button to the left of the slider."
msgstr ""
"Hodnotu prahu môžete nastaviť ťahaním posuvníka alebo nastavením hodnoty "
"vľavo."

#: index.docbook:130
msgid ""
"In general, the higher you set the <guilabel>Pointer Acceleration</guilabel> "
"value, the higher you'll want to set the <guilabel>Drag Threshold</guilabel> "
"value. For example, A <guilabel>Drag Threshold</guilabel> of "
"<guilabel>4pixels</guilabel> may be appropriate for a <guilabel>Pointer "
"Acceleration</guilabel> of <guilabel>2x</guilabel>, but <guilabel>10pixels</"
"guilabel> might be better for <guilabel>3x</guilabel>."
msgstr ""
"Všeobecne sa dá povedať, že vyššia hodnota <guilabel>Zrýchlenie ukazovátka</"
"guilabel> znamená použitie vyššej hodnoty <guilabel>Prah citlivosti</"
"guilabel>. Napríklad, <guilabel>Prah citlivosti</guilabel> nastavený na "
"<guilabel>4body</guilabel> odpovedá hodnote <guilabel>Zrýchlenie ukazovátka</"
"guilabel> <guilabel>2x</guilabel>, ale <guilabel>10bodov</guilabel> sa pre "
"<guilabel>3x</guilabel> hodí viac."

#: index.docbook:130
msgid "Double Click Interval"
msgstr "Interval dvojitého kliknutia"

#: index.docbook:130
msgid ""
"This is the maximum amount of time between clicks for &kde; to register a "
"double click. If you click twice, and the time between those two clicks is "
"less than this number, &kde; recognises that as a double click. If the time "
"between these two clicks is greater than this number, &kde; recognises those "
"as two <emphasis>seperate</emphasis> single clicks."
msgstr ""
"Toto je maximálny čas medzi dvoma kliknutiami, aby ich &kde; považovalo za "
"dvojité kliknutie. Ak dvakrát kliknete a doba je menšia ako táto hodnota, "
"&kde; to rozozná ako dvojité kliknutie. Ak je čas medzi nimi väčší, &kde; to "
"rozozná ako dve <emphasis>oddelené</emphasis> jednoduché kliknutia."

#: index.docbook:130
msgid ""
"<guilabel>Drag Start Time</guilabel> and <guilabel>Drag Start Distance</"
"guilabel>"
msgstr ""
"<guilabel>Štartovací čas uchopenia</guilabel> and <guilabel>Štartovacia "
"vzdialenosť uchopenia</guilabel>"

#: index.docbook:130
msgid ""
"If you <itemizedlist> <listitem><para>Click with the mouse</para></listitem> "
"<listitem><para>drag within the time specified in <guilabel>Drag Start Time</"
"guilabel>, and </para></listitem> <listitem><para>move a distance equal to "
"or greater than the number (of pixels) specified in <guilabel>Drag Start "
"Distance</guilabel></para> </listitem> </itemizedlist> &kde; will drag the "
"selected item."
msgstr ""
"Ak <itemizedlist> <listitem><para>Kliknite myšou</para></listitem> "
"<listitem><para>a ťaháte ju aspoň <guilabel>Štartovací čas uchopenia</"
"guilabel>, a </para></listitem> <listitem><para>presuniete kurzor aspoň o "
"počet bodov zadaný v <guilabel>Štartovacia vzdialenosť uchopenia</guilabel></"
"para> </listitem> </itemizedlist> &kde; bude vybranú položku ťahať."

#: index.docbook:130
msgid "Mouse Wheel Scrolls By"
msgstr "Koliesko myši posúva o"

#: index.docbook:130
msgid ""
"If you have a wheel mouse, use the slider to determine how many lines of "
"text one <quote>step</quote> of the mouse wheel will scroll."
msgstr ""
"Ak máte myš s kolieskom, pomocou tohto posuvníka môžete určiť, o koľko "
"riadkov textu bude jeden <quote>krok</quote> kolieska myši."

#: index.docbook:225
msgid "Section Author"
msgstr "Autor kapitoly"

#: index.docbook:227
msgid "Ellis Whitehead <email>kde@ellisw.net</email>"
msgstr "Ellis Whitehead <email>kde@ellisw.net</email>"

#: index.docbook:228
msgid "Updated by: Mike McBride <email>mpmcbride7@yahoo.com</email>"
msgstr "Aktualizoval Mike McBride <email>mpmcbride7@yahoo.com</email>"

#: index.docbook:230
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<para>Preklad dokumentácie Stanislav Višňovský <email>visnovsky@nenya.ms.mff."
"cuni.cz</email></para>"
