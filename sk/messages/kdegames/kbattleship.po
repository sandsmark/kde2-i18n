# kbattleships Slovak translation
# Copyright (C) 2001 Free Software Foundation, Inc.
# Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: kbattleships for KDE 2.2\n"
"POT-Creation-Date: 2001-06-10 17:19+0200\n"
"PO-Revision-Date: 2001-07-18 21:04CET\n"
"Last-Translator: Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>\n"
"Language-Team: Slovak <sk-i18n@linux.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: rc.cpp:3
msgid "Chat Widget"
msgstr "Prvok pre rozhovor"

#: rc.cpp:4
msgid "Enter a message here"
msgstr "Sem zadajte správu"

#: rc.cpp:5
msgid "Send"
msgstr "Poslať"

#: rc.cpp:6
msgid "Press here to send the message"
msgstr "Pre poslanie správy toto stlačte"

#: rc.cpp:8
msgid "Chat Dialog:"
msgstr "Rozhovor:"

#: rc.cpp:9
msgid "Connect to Server"
msgstr "Pripojiť sa k serveru"

#: rc.cpp:10 rc.cpp:100 rc.cpp:108
msgid "Nickname:"
msgstr "Prezývka:"

#: rc.cpp:11
msgid "Server:    "
msgstr "Server:    "

#: rc.cpp:12 rc.cpp:101 rc.cpp:109
msgid "Enter a name that identifys you in the game"
msgstr "Zadajte meno, ktoré chcete používať pri hre"

#: rc.cpp:13 rc.cpp:98
msgid "Port:"
msgstr "Port:"

#: rc.cpp:14
msgid "Choose a port to connect to"
msgstr "Vyberte port, na ktorý sa pripojiť"

#: rc.cpp:15
msgid "&Connect"
msgstr "&Pripojiť"

#: rc.cpp:16
msgid "Press here to connect to a server"
msgstr "Stlačením sa pripojíte na server"

#: rc.cpp:17 rc.cpp:104 rc.cpp:112
msgid "C&ancel"
msgstr "&Zrušiť"

#: rc.cpp:18 rc.cpp:106 rc.cpp:114
msgid "Press here to return to the game"
msgstr "Stlačením sa vrátite do hry"

#: rc.cpp:20
msgid "Rank"
msgstr "Hodnosť"

#: rc.cpp:21
msgid "Name"
msgstr "Meno"

#: rc.cpp:22
msgid "Score"
msgstr "Skóre"

#: rc.cpp:23 rc.cpp:119
msgid "Shots"
msgstr "Strely"

#: rc.cpp:24 rc.cpp:121
msgid "Hits"
msgstr "Zásahy"

#: rc.cpp:25 rc.cpp:123
msgid "Water"
msgstr "Voda"

#: rc.cpp:26 rc.cpp:27 rc.cpp:28 rc.cpp:29 rc.cpp:30 rc.cpp:33 rc.cpp:34
#: rc.cpp:35 rc.cpp:36 rc.cpp:37 rc.cpp:38 rc.cpp:40 rc.cpp:41 rc.cpp:42
#: rc.cpp:43 rc.cpp:44 rc.cpp:45 rc.cpp:48 rc.cpp:49 rc.cpp:51 rc.cpp:52
#: rc.cpp:53 rc.cpp:55 rc.cpp:58 rc.cpp:59 rc.cpp:61 rc.cpp:62 rc.cpp:66
#: rc.cpp:67 rc.cpp:68 rc.cpp:69 rc.cpp:74 rc.cpp:75 rc.cpp:76 rc.cpp:80
#: rc.cpp:81 rc.cpp:82 rc.cpp:83 rc.cpp:84 rc.cpp:85 rc.cpp:120
msgid "Shows all shots"
msgstr "Ukáže všetky strely"

#: rc.cpp:31
msgid "#8"
msgstr "#8"

#: rc.cpp:32
msgid "#2"
msgstr "#2"

#: rc.cpp:39
msgid "#6"
msgstr "#6"

#: rc.cpp:46
msgid "#10"
msgstr "#10"

#: rc.cpp:47
msgid "#7"
msgstr "#7"

#: rc.cpp:50
msgid "#4"
msgstr "#4"

#: rc.cpp:54 rc.cpp:56 rc.cpp:60 rc.cpp:63 rc.cpp:70 rc.cpp:72 rc.cpp:73
#: rc.cpp:77 rc.cpp:78 rc.cpp:79
msgid "-"
msgstr "-"

#: rc.cpp:57
msgid "#5"
msgstr "#5"

#: rc.cpp:64
msgid "#9"
msgstr "#9"

#: rc.cpp:65
msgid "#3"
msgstr "#3"

#: rc.cpp:71
msgid "#1"
msgstr "#1"

#: rc.cpp:87
msgid "Enemy Client Information"
msgstr "Informácie o klientovi súpera"

#: rc.cpp:88
msgid "Client Identifier:"
msgstr "Identifikátor klienta:"

#: rc.cpp:89
msgid "Client Information:"
msgstr "Informácie o klientovi:"

#: rc.cpp:90
msgid "CV"
msgstr "CV"

#: rc.cpp:91
msgid "Client Version:"
msgstr "Verzia klienta:"

#: rc.cpp:92
msgid "Protocol Version:"
msgstr "Verzia protokolu:"

#: rc.cpp:93
msgid "PV"
msgstr "PV"

#: rc.cpp:94 rc.cpp:95
msgid "CI"
msgstr "CI"

#: rc.cpp:97
msgid "Start Server"
msgstr "Štart serveru"

#: rc.cpp:99
msgid "Choose a port where the server listens on"
msgstr "Vyberte port, na ktorom počúva server"

#: rc.cpp:103 rc.cpp:111
msgid "Press here to start the server"
msgstr "Stlačením spustíte server"

#: rc.cpp:107
msgid "Start Game"
msgstr "Spustiť hru"

#: rc.cpp:115
msgid "Form1"
msgstr "Form1"

msgid "0"
msgstr "0"

#: rc.cpp:117
msgid ":"
msgstr ":"

#: rc.cpp:122
msgid "Shows all hit ships"
msgstr "Ukáže všetky zasiahnuté lode"

#: rc.cpp:124
msgid "Shows all water shots"
msgstr "Ukáže všetky strely do vody"

#: kbattleship.cpp:39
msgid ""
"You don't have KBattleship Pictures installed. The game cannot run without "
"them!"
msgstr "Nemáte nainštalované obrázky pre Lode. Hra bez nich nefunguje."

#: kbattleship.cpp:72 kbattleship.cpp:1067
msgid "     Player 1: %1     "
msgstr "     Hráč 1: %1     "

#: kbattleship.cpp:73 kbattleship.cpp:1073
msgid "     Player 2: %1     "
msgstr "     Hráč 2: %1     "

#: kbattleship.cpp:74 kbattleship.cpp:364 kbattleship.cpp:618
#: kbattleship.cpp:713 kbattleship.cpp:1090 kbattleship.cpp:1102
msgid "Ready"
msgstr "Pripravený"

#: kbattleship.cpp:80 kbattleship.cpp:368
msgid "&Connect to server"
msgstr "&Pripojiť na server"

#: kbattleship.cpp:81 kbattleship.cpp:369
msgid "&Start server"
msgstr "S&pustiť server"

#: kbattleship.cpp:82 kbattleship.cpp:279 kbattleship.cpp:1099
#: kbattleship.cpp:1194
msgid "S&ingle Player"
msgstr "Je&den hráč"

#: kbattleship.cpp:84
msgid "&Highscore"
msgstr "&Najlepšie skóre"

#: kbattleship.cpp:85
msgid "&Enemy Info..."
msgstr "Informácie o &súperovi..."

#: kbattleship.cpp:88
msgid "&Play sounds"
msgstr "&Hrať zvuky"

#: kbattleship.cpp:89
msgid "&Show grid"
msgstr "&Ukázať mriežku"

#: kbattleship.cpp:146
#, c-format
msgid "KBattleship %1"
msgstr "Lode %1"

#: kbattleship.cpp:179
msgid "Sending Message..."
msgstr "Posielam správu..."

#: kbattleship.cpp:282 kbattleship.cpp:340
msgid "You won the game :)"
msgstr "Vyhrali ste :)"

#: kbattleship.cpp:285 kbattleship.cpp:654 kbattleship.cpp:1200
msgid "Do you want to restart the game?"
msgstr "Chcete znovu-spustiť hru?"

#: kbattleship.cpp:335
msgid "Waiting for enemy to shoot.."
msgstr "Čakám na strelu nepriateľa."

#: kbattleship.cpp:353 kbattleship.cpp:359
msgid "Enemy disconnected."
msgstr "Súbor odpojený."

#: kbattleship.cpp:370
msgid "S&ingle game"
msgstr "&Jedna hra"

#: kbattleship.cpp:486
msgid "Waiting for computer player to start the match..."
msgstr "Čakám, kým počítač začne hru..."

#: kbattleship.cpp:496 konnectionhandling.cpp:114 konnectionhandling.cpp:138
msgid "Waiting for other player to place the ships..."
msgstr "Čakám, kým súper umiestni lode..."

#: kbattleship.cpp:498
msgid "Waiting for other player to start the match..."
msgstr "Čakám, kým súper spustí hru..."

#: kbattleship.cpp:611
msgid "Loading Connect-Server dialog..."
msgstr "Načítavam dialóg pre pripojenie..."

#: kbattleship.cpp:638
msgid "The client asks for restarting the game. Do you accept?"
msgstr "Klient požaduje znovu-spustenie hry. Súhlasíte?"

#: kbattleship.cpp:642 kbattleship.cpp:658 kbattleship.cpp:1161
#: konnectionhandling.cpp:120 konnectionhandling.cpp:172
msgid "Please place your ships. Use the \"Shift\" key to place the ships vertically."
msgstr ""
"Prosím, umiestnite vaše lode. Pre vertikálne umiestnenie použite kláves "
"\"Shift\"."

#: kbattleship.cpp:672
msgid "Do you want to ask the server restarting the game?"
msgstr "Chcete požiadať server o znovu-spustenie hry?"

#: kbattleship.cpp:676
msgid "Waiting for an answer..."
msgstr "Čakám na odpoveď..."

#: kbattleship.cpp:706
msgid "Loading Start-Server dialog..."
msgstr "Načítavam dialóg pre spustenie serveru..."

#: kbattleship.cpp:746
msgid "&Stop server"
msgstr "Za&staviť server"

#: kbattleship.cpp:749
msgid "Waiting for a player..."
msgstr "Čakám na hráča..."

#: kbattleship.cpp:966
msgid "Dis&connect from server"
msgstr "O&dpojiť od serveru"

#: kbattleship.cpp:1083
msgid "Loading Single-Game dialog..."
msgstr "Načítavam dialóg pre hru pre jedného hráča..."

#: kbattleship.cpp:1125
msgid "&Stop game"
msgstr "Za&staviť hru"

#: kbattleship.cpp:1128
msgid "Waiting for the AI player to place the ships..."
msgstr "Čakám, kým súper - počítač umiestni lode..."

#: kbattleship.cpp:1185 konnectionhandling.cpp:126 konnectionhandling.cpp:193
msgid "Enemy has shot. Shoot now"
msgstr "Súper vystrelil. Teraz vy"

#: kbattleship.cpp:1197 konnectionhandling.cpp:144 konnectionhandling.cpp:204
msgid "You lost the game :("
msgstr "Prehrali ste :("

#: kbattleshipserver.cpp:38
msgid ""
"Failed to bind to local port \"%1\"\n"
"\n"
"Please check if another KBattleship server instance\n"
"is running or another application uses this port."
msgstr ""
"Nepodarilo sa použiť lokálny port \"%1\"\n"
"\n"
"Prosím, overte, či nebeží iná inštancia serveru Lode\n"
"alebo či iná aplikácie nepoužíva tento port."

#: kbattleshipserver.cpp:64
msgid "The connection broke down!"
msgstr "Spojenie spadlo!"

#: kbattleshipsound.cpp:60
msgid "Couldn't connect to aRts Soundserver. Sound deactivated"
msgstr "Nepodarilo sa spojiť so zvukovým serverom aRts. Zvuk vypnutý"

#: kbattleshipsound.cpp:77
msgid "You don't have KBattleship Sounds installed. Sound deactivated"
msgstr "Nemáte nainštalované zvuky pre Lode. Zvuk vypnutý"

#: konnectionhandling.cpp:77
msgid "Connection to client lost. Aborting the game!"
msgstr "Spojenie s klientom stratené. Ruším hru!"

#: konnectionhandling.cpp:99
msgid ""
"Connection dropped by enemy. The client's protocol implementation (%1) is "
"not compatible with our (%2) version!"
msgstr ""
"Súper prerušil spojenie. Implementácia protokolu klienta (%1) nie je "
"kompatibilná s vašou verziou (%2)!"

#: konnectionhandling.cpp:163
msgid ""
"Connection to client dropped. The client's protocol implementation (%1) is "
"not compatible with our (%2) version!"
msgstr ""
"Klient prerušil spojenie. Implementácia protokolu klienta (%1) nie je "
"kompatibilná s vašou verziou (%2)!"

#: konnectionhandling.cpp:171
msgid "We got a player. Let's start..."
msgstr "Prihlásil sa hráč. Môžeme začať..."

#: konnectionhandling.cpp:181
msgid "You can shoot now"
msgstr "Teraz strieľate vy"

#: konnectionhandling.cpp:223
msgid "Connection refused by other host!"
msgstr "Spojenie odmietnuté!"

#: konnectionhandling.cpp:227
msgid "Couldn't lookup host!"
msgstr "Nie je možné nájsť hostiteľa!"

#: konnectionhandling.cpp:231
msgid "Couldn't connect to server!"
msgstr "Nepodarilo sa spojiť so serverom!"

#: konnectionhandling.cpp:235
#, c-format
msgid "Unknown Error; No: %1"
msgstr "Neznáma chyba číslo: %1"

#: konnectionhandling.cpp:244
msgid "Connection to server lost. Aborting the game!"
msgstr "Spojenie stratené. Ruším hru!"

#: kshiplist.cpp:126
msgid "You can't place the ship here."
msgstr "Sem nie je možné umiestniť loď."

#: main.cpp:30
msgid "Project Founder, GUI Handling, Client/Server"
msgstr "Zakladateľ projektu, spracovanie rozhrania, Klient/Server"

#: main.cpp:31
msgid "Dialog Stuff, Client/Server"
msgstr "Dialógy, Klient/Server"

#: main.cpp:32
msgid "Computer Player"
msgstr "Hráč - počítač"

#: main.cpp:33
msgid "Icon"
msgstr "Ikona"

#: main.cpp:34
msgid "Sounds"
msgstr "Zvuky"

#: main.cpp:35
msgid "GFX"
msgstr "GFX"

#: main.cpp:36
msgid "Non-Latin1 Support"
msgstr "Podpora pre nie-Latin1"

#: main.cpp:37
msgid "Various improvements"
msgstr "Rôzne zlepšenia"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Stanislav Višňovský"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "visnovsky@nenya.ms.mff.cuni.cz"

#: main.h:6
msgid "KBattleship"
msgstr "Lode"

#: main.h:8
msgid "The KDE Battleship clone"
msgstr "KDE klon hry Lode"
