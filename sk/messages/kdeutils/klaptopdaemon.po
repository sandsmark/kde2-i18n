# klaptopdaemon Slovak translation.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: klaptomdaemon for KDE 2.2\n"
"POT-Creation-Date: 2001-05-17 01:52+0200\n"
"PO-Revision-Date: 2001-07-24 13:17CET\n"
"Last-Translator: Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>\n"
"Language-Team: Slovak <sk-i18n@linux.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.3\n"

#: laptop_daemon.cpp:47
msgid "KDE Laptop Daemon"
msgstr "KDE Laptop Démon"

#: laptop_daemon.cpp:427
msgid "KLaptop"
msgstr "KLaptop"

#: kpcmcia.cpp:98 kpcmcia.cpp:165
msgid "Empty slot."
msgstr "Prázdny slot."

#: kpcmciainfo.cpp:56
msgid "PCMCIA and CardBus Slots"
msgstr "Sloty PCMCIA a CardBus"

#: kpcmciainfo.cpp:66 kpcmciainfo.cpp:95
msgid "Ready."
msgstr "Pripravený."

#: kpcmciainfo.cpp:134
#, c-format
msgid "Card Slot %1"
msgstr "Slot karty %1"

#: kpcmciainfo.cpp:179 kpcmciainfo.cpp:290
msgid "&Eject"
msgstr "&Vysunúť"

#: kpcmciainfo.cpp:180 kpcmciainfo.cpp:293
msgid "&Suspend"
msgstr "Z&astaviť"

#: kpcmciainfo.cpp:181
msgid "&Reset"
msgstr "&Reštartovať"

#: kpcmciainfo.cpp:201
msgid "Resetting card..."
msgstr "Resetujem kartu..."

#: kpcmciainfo.cpp:208
msgid "Inserting new card..."
msgstr "Vkladám novú kartu..."

#: kpcmciainfo.cpp:212
msgid "Ejecting card..."
msgstr "Vysúvam kartu..."

#: kpcmciainfo.cpp:223
msgid "Suspending card..."
msgstr "Pozastavujem kartu..."

#: kpcmciainfo.cpp:226
msgid "Resuming card..."
msgstr "Znovu spúšťam kartu..."

#: kpcmciainfo.cpp:237
msgid "Card Type: %1 "
msgstr "Typ karty: %1"

#: kpcmciainfo.cpp:240
#, c-format
msgid "Driver: %1"
msgstr "Ovládač: %1"

#: kpcmciainfo.cpp:243
msgid "IRQ: %1%2"
msgstr "IRQ: %1%2"

#: kpcmciainfo.cpp:247
msgid " (used for memory)"
msgstr " (použité pre pamäť)"

#: kpcmciainfo.cpp:250
msgid " (used for memory and I/O)"
msgstr " (použité pre pamäť a I/O)"

#: kpcmciainfo.cpp:253
msgid " (used for CardBus)"
msgstr " (použité pre CardBus)"

#: kpcmciainfo.cpp:259 kpcmciainfo.cpp:264
msgid "none"
msgstr "nič"

#: kpcmciainfo.cpp:262
#, c-format
msgid "I/O Port(s): %1"
msgstr "I/O Port(y): %1"

#: kpcmciainfo.cpp:267
msgid "Bus: %1 bit %2"
msgstr "Zbernica: %1 bitov %2"

#: kpcmciainfo.cpp:269
msgid "Bus: unknown"
msgstr "Zbernica: neznáma"

#: kpcmciainfo.cpp:270
msgid "PC Card"
msgstr "PC Card"

#: kpcmciainfo.cpp:270
msgid "Cardbus"
msgstr "Cardbus"

#: kpcmciainfo.cpp:272
#, c-format
msgid "Device: %1"
msgstr "Zariadenie: %1"

#: kpcmciainfo.cpp:275
msgid "Power: +%1V"
msgstr "Napájanie: +%1V"

#: kpcmciainfo.cpp:278
msgid "Programming Power: +%1V, +%2V"
msgstr "Programovacie napájanie: +%1V, +%2V"

#: kpcmciainfo.cpp:281
#, c-format
msgid "Configuration Base: 0x%1"
msgstr "Základ nastavenia: 0x%1"

#: kpcmciainfo.cpp:283
msgid "Configuration Base: none"
msgstr "Základ nastavenia: žiadna"

#: kpcmciainfo.cpp:295
msgid "Resu&me"
msgstr "&Obnoviť"

#: daemondock.cpp:52
msgid "Setup..."
msgstr "Nastavenia..."

#: daemondock.cpp:55
msgid "Standby..."
msgstr "Pohotovostný režim..."

#: daemondock.cpp:56
msgid "&Lock && Suspend..."
msgstr "&Zamknúť && Pozastaviť..."

#: daemondock.cpp:57
msgid "&Suspend..."
msgstr "&Pozastaviť..."

#: daemondock.cpp:58
msgid "&Lock && Hibernate..."
msgstr "&Zamknúť && hibernovať..."

#: daemondock.cpp:59
msgid "&Hibernate..."
msgstr "&Hibernovať..."

#: daemondock.cpp:119
msgid "Power Manager Not Found"
msgstr "Chýba riadenie napájania"

#: daemondock.cpp:124
msgid "%1% charged"
msgstr "%1% nabité"

#: daemondock.cpp:128
msgid "%1:%2 minutes left"
msgstr "%1:%2 zostávajúcich minút"

#: daemondock.cpp:134
msgid "Charging"
msgstr "Nabíjanie"

#: daemondock.cpp:136
msgid "Not Charging"
msgstr "Bez nabíjania"

#: daemondock.cpp:145
#, c-format
msgid "Slot %1"
msgstr "Slot %1"

#: daemondock.cpp:154
msgid "Card slots..."
msgstr "Sloty kariet..."

#: daemondock.cpp:161
msgid "Details..."
msgstr "Detaily..."

#: daemondock.cpp:166
msgid "Eject"
msgstr "Vysunúť"

#: daemondock.cpp:169
msgid "Suspend"
msgstr "Prerušiť"

#: daemondock.cpp:172
msgid "Resume"
msgstr "Obnoviť"

#: daemondock.cpp:175
msgid "Reset"
msgstr "Reset"

#: daemondock.cpp:180
msgid "Actions"
msgstr "Akcie"

#: daemondock.cpp:186
msgid "Ready"
msgstr "Pripravený"

#: daemondock.cpp:188
msgid "Busy"
msgstr "Zaneprázdnený"

#: daemondock.cpp:190
msgid "Suspended"
msgstr "Pozastavený"

#: daemondock.cpp:360
msgid "Laptop Power Management not available"
msgstr "Riadenie napájania pre laptop nie je prístupné"

#: daemondock.cpp:364
msgid "Plugged in - fully charged"
msgstr "V sieti - plne nabitý"

#: daemondock.cpp:369
msgid "Plugged in - %1% charged (%2:%3 minutes)"
msgstr "V sieti - %1% nabité (%2:%3 minút)"

#: daemondock.cpp:372
msgid "Plugged in - %1% charged"
msgstr "V sieti - %1% nabité"

#: daemondock.cpp:379
msgid "Running on batteries - %1% charged (%2:%3 minutes)"
msgstr "Beh na batérie - %1% nabité (%2:%3 minút)"

#: daemondock.cpp:382
msgid "Running on batteries  - %1% charged"
msgstr "Beh na batérie - %1% nabité"

#: notify.cpp:30
msgid "Battery power is running out!"
msgstr "Vybitá batéria!"

#: notify.cpp:47
msgid "Battery Power is running out!"
msgstr "Vybitá batéria!"

#: notify.cpp:55
msgid "Charge Left: % %1"
msgstr "Batéria nabitá: % %1"

#: notify.cpp:56
#, c-format
msgid "Minutes Left: %1"
msgstr "Uplynulý čas: %1"

#: notify.cpp:75
msgid "Suspend Now"
msgstr "Pozastaviť teraz"

#: portable.cpp:191
msgid ""
"Your computer doesn't have the Linux APM (Advanced\n"
"Power Management) software installed, or doesn't have\n"
"the APM kernel drivers installed - check out the Linux Laptop-HOWTO\n"
"document for information how to install APM\n"
"it is available at http://www.linuxdoc.org/HOWTO/Laptop-HOWTO.html"
msgstr ""
"Nemáte nainštalovaný program Linux APM (Advanced Power Management - \n"
"Rozšírené riadenie napájania), alebo nemáte nainštalovaný ovládač "
"APM.\n"
"Informácie ako nainštalovať APM nájdete v Laptop-HOWTO dokumente,\n"
"alebo na stránke http://www.linuxdoc.org/HOWTO/Laptop-HOWTO.html"

#: portable.cpp:201
msgid ""
"\n"
"If you make /usr/bin/apm setuid then you will also\n"
"be able to choose 'suspend' and 'standby' in the\n"
"above dialog - check out the help button below to\n"
"find out how to do this"
msgstr ""
"\n"
"Ak ste nastavili SETUID bit na program /usr/bin/apm, \n"
"potom máte možnosť zvoliť si 'pohotovostný', alebo \n"
"'pozastavený' režim v dialógovom okne nad týmto textom.\n"
"Bližšie informácie ako to urobiť získate vyvolaním pomocníka\n"
"tlačidlom pod týmto textom."

#: portable.cpp:271 portable.cpp:572 portable.cpp:736
msgid "No PCMCIA controller detected"
msgstr "Riadiaca jednotka PCMCIA nenájdená"

#: portable.cpp:275
msgid "Card 0:"
msgstr "Karta 0:"

#: portable.cpp:277
msgid "Card 1:"
msgstr "Karta 1:"

#: portable.cpp:559 portable.cpp:723
msgid " "
msgstr " "

#: portable.cpp:713
msgid ""
"Your computer or operating system is not supported by the current version of "
"the\n"
"KDE laptop control panels. If you want help porting these panels to work "
"with it\n"
"please contact paul@taniwha.com."
msgstr ""
"Váš počítač, alebo operačný systém nepodporuje aktuálnu verziu KDE "
"laptop ovládacieho panelu.\n"
"Pre prípadné otázky a pomoc s portovaním týchto panelov, prosím, "
"kontaktujte paul@taniwha.com."

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Marek Zima,Stanislav Višňovský"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "marek_zima@yahoo.com,visnovsky@nenya.ms.mff.cuni.cz"
