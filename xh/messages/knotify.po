# K Desktop Environment - kdelibs
# Copyright (C) 2001 translate.org.za
# Antoinette Dekeni <antoinette@transalate.org.za>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: knotify\n"
"POT-Creation-Date: 2001-05-24 11:56+0200\n"
"PO-Revision-Date: 2001-09-05 00:28GMT\n"
"Last-Translator: Antoinette Dekeni <antoinette@transalate.org.za>\n"
"Language-Team: Xhosa <xhosa@translate.org.za>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: knotify.cpp:78
msgid "KNotify"
msgstr "i-KNotify"

#: knotify.cpp:79
msgid "KDE Notification Server"
msgstr "Umncedisi weSaziso se-KDE"

#: knotify.cpp:82
msgid "Sound support"
msgstr "Inkxaso yengxolo"

#: knotify.cpp:83
msgid "Current Maintainer"
msgstr "Umlondolozi waNgoku"

#: knotify.cpp:338
msgid "Notification"
msgstr "Isaziso"

#: knotify.cpp:347
msgid "Catastrophe!"
msgstr "Ingozi enkulu!"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Antoinette Dekeni"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "antoinette@transalate.org.za"
