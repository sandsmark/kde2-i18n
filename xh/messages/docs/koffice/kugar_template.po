# K Desktop Environment - docs/koffice
# Copyright (C) 2001 translate.org.za
# Antoinette Dekeni <antoinette@transalate.org.za>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: kugar_template\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-10-29 13:50SAST\n"
"Last-Translator: Antoinette Dekeni <antoinette@translate.org.za>\n"
"Language-Team: Xhosa <xhosa@translate.org.za>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: index.docbook:7
msgid "KugarTemplate"
msgstr "KugarTemplate"

#: index.docbook:9
msgid ""
"The <sgmltag class=\"element\">KugarTemplate</sgmltag> element defines "
"report attributes relating to page size, orientation, and margins."
msgstr ""
"I <sgmltag class=\"element\">KugarTemplate</sgmltag> isiqalelo sichaza "
"iziphumo zengxelo ezayame kubungakanani bephepha, uhlenga-hlengiso nee "
"layini."

#: index.docbook:14
msgid ""
"&lt;!ELEMENT KugarTemplate (ReportHeader, PageHeader, Detail, PageFooter, "
"ReportFooter)&gt;\n"
"&lt;!ATTLIST KugarTemplate\n"
"PageSize        CDATA #REQUIRED\n"
"PageOrientation CDATA #REQUIRED\n"
"TopMargin       CDATA #REQUIRED\n"
"BottomMargin    CDATA #REQUIRED\n"
"LeftMargin      CDATA #REQUIRED\n"
"RightMargin     CDATA #REQUIRED&gt;"
msgstr ""
"&lt;!ISIQALELO KugarTemplate (ReportHeader, PageHeader, Inkcukacha, "
"PageFooter, ReportFooter)&gt;\n"
"&lt;!ATTLIST KugarTemplate\n"
"Ubungakanani bephepha        CDATA #IYAFUNWA\n"
"Uhlenga-hlengiso Lwephepha CDATA #IYAFUNWA\n"
"Ilayini Ephezulu       CDATA #IYAFUNWA\n"
"Ilayini Esezantsi    CDATA #IYAFUNWA\n"
"Ilayini Esekhohlo      CDATA #IYAFUNWA\n"
"Ilayini Esekunene     CDATA #IYAFUNWA&gt;"

#: index.docbook:25
msgid "Elements"
msgstr "Iziqalelo"

#: index.docbook:25
msgid ""
"The <sgmltag class=\"element\">KugarTemplate</sgmltag> element contains the "
"following required elements:"
msgstr ""
"I <sgmltag class=\"element\">KugarTemplate</sgmltag> isiqalelo siqulathe "
"iziqalelo ezilandelayo ezifunwayo:"

#: index.docbook:25
msgid "ReportHeader"
msgstr "iHeader yengxelo"

#: index.docbook:25
msgid ""
"The <sgmltag class=\"element\">ReportHeader</sgmltag> element defines report "
"sections that are usually printed at the beginning of the report."
msgstr ""
"I <sgmltag class=\"element\">iHeader Yengxelo</sgmltag> isiqalelo sichaza "
"amacandelo engxelo ashicilelwe kwisiqalo sengxelo."

#: index.docbook:25
msgid "PageHeader"
msgstr "iHeader yephepha"

#: index.docbook:25
msgid ""
"The <sgmltag class=\"element\">PageHeader</sgmltag> element defines report "
"sections that are usually printed at the top of every page of the report."
msgstr ""
"I <sgmltag class=\"element\">iHeader Yephepha</sgmltag> isiqalelo sichaza "
"amacandelo engxelo ashicilelwe ngentla kwephepha ngalinye lengxelo."

#: index.docbook:25
msgid "Detail"
msgstr "Inkcukacha"

#: index.docbook:25
msgid ""
"The <sgmltag class=\"element\">Detail</sgmltag> element defines the report "
"section that contains the report data."
msgstr ""
"I <sgmltag class=\"element\">Inkcukacha</sgmltag> isiqalelo sichaza icandelo "
"lengxelo eliqulathe idata yengxelo."

#: index.docbook:25
msgid "PageFooter"
msgstr "iFooter yephepha"

#: index.docbook:25
msgid ""
"The <sgmltag class=\"element\">PageFooter</sgmltag> element defines report "
"sections that are usually printed at the end of the every page in the report."
msgstr ""
"I <sgmltag class=\"element\">iFooter Yephepha</sgmltag> isiqalelo sichaza "
"amacandelo engxelo ashicilelwa kwisiphelo sephepha ngalinye kwingxelo."

#: index.docbook:25
msgid "ReportFooter"
msgstr "iFooter yengxelo"

#: index.docbook:25
msgid ""
"The <sgmltag class=\"element\">ReportFooter</sgmltag> element defines report "
"sections that are usually printed at the end of the report."
msgstr ""
"I <sgmltag class=\"element\">iFooter Yengxelo</sgmltag> isiqalelo sichaza "
"amacandelo engxelo ashicileilwe kwisiphelo sengxelo."

#: index.docbook:25
msgid "Attributes"
msgstr "Iziphumo"

#: index.docbook:25
msgid "PageSize"
msgstr "Ubungakanani bephepha"

#: index.docbook:25
msgid "Sets the size of the report page. The following values are available:"
msgstr "Icwangcisa ubungakanani bephepha lengxelo. Amaxabiso alandelyo akhona:"

#: index.docbook:25
msgid "Value"
msgstr "Ixabiso"

#: index.docbook:25
msgid "Page Size"
msgstr "Ubungakanani bephepha"

#: index.docbook:25
msgid "<entry>0</entry>"
msgstr "<entry>0</entry>"

#: index.docbook:25
msgid "<entry>A4</entry>"
msgstr "<entry>A4</entry>"

#: index.docbook:25
msgid "<entry>1</entry>"
msgstr "<entry>1</entry>"

#: index.docbook:25
msgid "<entry>B5</entry>"
msgstr "<entry>B5</entry>"

#: index.docbook:25
msgid "<entry>2</entry>"
msgstr "<entry>2</entry>"

#: index.docbook:25
msgid "Letter"
msgstr "Ileta"

#: index.docbook:25
msgid "<entry>3</entry>"
msgstr "<entry>3</entry>"

#: index.docbook:25
msgid "Legal"
msgstr "Esemthethweni"

#: index.docbook:25
msgid "<entry>4</entry>"
msgstr "<entry>4</entry>"

#: index.docbook:25
msgid "Executive"
msgstr "Umntu osequmrhwini lokukhupha umthetho"

#: index.docbook:25
msgid "<entry>5</entry>"
msgstr "<entry>5</entry>"

#: index.docbook:25
msgid "<entry>A0</entry>"
msgstr "<entry>A0</entry>"

#: index.docbook:25
msgid "<entry>6</entry>"
msgstr "<entry>6</entry>"

#: index.docbook:25
msgid "<entry>A1</entry>"
msgstr "<entry>A1</entry>"

#: index.docbook:25
msgid "<entry>7</entry>"
msgstr "<entry>7</entry>"

#: index.docbook:25
msgid "<entry>A2</entry>"
msgstr "<entry>A2</entry>"

#: index.docbook:25
msgid "<entry>8</entry>"
msgstr "<entry>8</entry>"

#: index.docbook:25
msgid "<entry>A3</entry>"
msgstr "<entry>A3</entry>"

#: index.docbook:25
msgid "<entry>9</entry>"
msgstr "<entry>9</entry>"

#: index.docbook:25
msgid "<entry>A5</entry>"
msgstr "<entry>A5</entry>"

#: index.docbook:25
msgid "<entry>10</entry>"
msgstr "<entry>10</entry>"

#: index.docbook:25
msgid "<entry>A6</entry>"
msgstr "<entry>A6</entry>"

#: index.docbook:25
msgid "<entry>11</entry>"
msgstr "<entry>11</entry>"

#: index.docbook:25
msgid "<entry>A7</entry>"
msgstr "<entry>A7</entry>"

#: index.docbook:25
msgid "<entry>12</entry>"
msgstr "<entry>12</entry>"

#: index.docbook:25
msgid "<entry>A8</entry>"
msgstr "<entry>A8</entry>"

#: index.docbook:25
msgid "<entry>13</entry>"
msgstr "<entry>13</entry>"

#: index.docbook:25
msgid "<entry>A9</entry>"
msgstr "<entry>A9</entry>"

#: index.docbook:25
msgid "<entry>14</entry>"
msgstr "<entry>14</entry>"

#: index.docbook:25
msgid "<entry>B0</entry>"
msgstr "<entry>B0</entry>"

#: index.docbook:25
msgid "<entry>15</entry>"
msgstr "<entry>15</entry>"

#: index.docbook:25
msgid "<entry>B1</entry>"
msgstr "<entry>B1</entry>"

#: index.docbook:25
msgid "<entry>16</entry>"
msgstr "<entry>16</entry>"

#: index.docbook:25
msgid "<entry>B2</entry>"
msgstr "<entry>B2</entry>"

#: index.docbook:25
msgid "<entry>17</entry>"
msgstr "<entry>17</entry>"

#: index.docbook:25
msgid "<entry>B3</entry>"
msgstr "<entry>B3</entry>"

#: index.docbook:25
msgid "<entry>18</entry>"
msgstr "<entry>18</entry>"

#: index.docbook:25
msgid "<entry>B4</entry>"
msgstr "<entry>B4</entry>"

#: index.docbook:25
msgid "<entry>19</entry>"
msgstr "<entry>19</entry>"

#: index.docbook:25
msgid "<entry>B6</entry>"
msgstr "<entry>B6</entry>"

#: index.docbook:25
msgid "<entry>20</entry>"
msgstr "<entry>20</entry>"

#: index.docbook:25
msgid "<entry>B7</entry>"
msgstr "<entry>B7</entry>"

#: index.docbook:25
msgid "<entry>21</entry>"
msgstr "<entry>21</entry>"

#: index.docbook:25
msgid "<entry>B8</entry>"
msgstr "<entry>B8</entry>"

#: index.docbook:25
msgid "<entry>22</entry>"
msgstr "<entry>22</entry>"

#: index.docbook:25
msgid "<entry>B9</entry>"
msgstr "<entry>B9</entry>"

#: index.docbook:25
msgid "<entry>23</entry>"
msgstr "<entry>23</entry>"

#: index.docbook:25
msgid "Comm10E"
msgstr "Comm10E"

#: index.docbook:25
msgid "<entry>24</entry>"
msgstr "<entry>24</entry>"

#: index.docbook:25
msgid "<entry>DLE</entry>"
msgstr "<entry>DLE</entry>"

#: index.docbook:25
msgid "<entry>25</entry>"
msgstr "<entry>25</entry>"

#: index.docbook:25
msgid "Folio"
msgstr "Iphepha elisongwe kanye"

#: index.docbook:25
msgid "<entry>26</entry>"
msgstr "<entry>26</entry>"

#: index.docbook:25
msgid "Ledger"
msgstr "Incwadi egcina amatyala omtyali kunye nomtyalwa"

#: index.docbook:25
msgid "<entry>27</entry>"
msgstr "<entry>27</entry>"

#: index.docbook:25
msgid "Tabloid"
msgstr "Iphephandaba elincinane"

#: index.docbook:25
msgid "PageOrientation"
msgstr "Uhlenga-hlengiso lwephepha"

#: index.docbook:25
msgid "Sets the report page orientation."
msgstr "Icwangcisa uhlenga-hlengiso lwephepha lengxelo."

#: index.docbook:25
msgid "Orientation"
msgstr "Uhlengahlengiso"

#: index.docbook:25
msgid "Portrait"
msgstr "Umzobi"

#: index.docbook:25
msgid "Landscape"
msgstr "Ubuhle belizwe"

#: index.docbook:25
msgid "TopMargin"
msgstr "Ilayini ephezulu"

#: index.docbook:25
msgid "Sets the top margin of the report page"
msgstr "Icwangcisa ilayini ephezulu yephepha lengxelo"

#: index.docbook:25
msgid "BottomMargin"
msgstr "Ilayini esezantsi"

#: index.docbook:25
msgid "Sets the bottom margin of the report page"
msgstr "Icwangcisa ilayini esezantsi yephepha lengxelo"

#: index.docbook:25
msgid "LeftMargin"
msgstr "Ilayini esekhohlo"

#: index.docbook:25
msgid "Sets the left margin of the report page"
msgstr "Icwangcisa ilayini esekhohlo yephepha lengxelo"

#: index.docbook:25
msgid "RightMargin"
msgstr "Ilayini esekunene"

#: index.docbook:25
msgid "Sets the right margin of the report page"
msgstr "Icwangcisa ilayini esekunene yephepha lengxelo"
