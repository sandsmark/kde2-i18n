# K Desktop Environment - others
# Copyright (C) 2001 translate.org.za
# Antoinette Dekeni <antoinette@transalate.org.za>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: kuickshow\n"
"POT-Creation-Date: 2001-08-04 14:54+0200\n"
"PO-Revision-Date: 2001-10-09 16:08SAST\n"
"Last-Translator: Antoinette Dekeni <antoinette@translate.org.za>\n"
"Language-Team: Xhosa <xhosa@translate.org.za>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: generalwidget.cpp:20
msgid "Settings"
msgstr "Izicwangciso"

#: generalwidget.cpp:26
msgid "Fullscreen mode"
msgstr ""

#: generalwidget.cpp:28
msgid "Preload next image"
msgstr ""

#: generalwidget.cpp:31
msgid "Background color"
msgstr "Umbala wesiqalo"

#: generalwidget.cpp:34
msgid "Show only files with extension: "
msgstr ""

#: generalwidget.cpp:37
msgid "Slideshow delay (1/10 s): "
msgstr ""

#: generalwidget.cpp:40
msgid "default: 30 = 3 seconds"
msgstr ""

#: generalwidget.cpp:57
msgid "Quality / Speed"
msgstr ""

#: generalwidget.cpp:62
msgid "Fast rendering"
msgstr ""

#: generalwidget.cpp:63
msgid "Dither in HiColor (15/16bit) modes"
msgstr ""

#: generalwidget.cpp:66
msgid "Dither in LowColor (<=8bit) modes"
msgstr ""

#: generalwidget.cpp:69
msgid "Use own color palette"
msgstr ""

#: generalwidget.cpp:73
msgid "Fast palette remapping"
msgstr ""

#: generalwidget.cpp:76
msgid "Maximum cache size (MB): "
msgstr ""

#: generalwidget.cpp:78
msgid "0 = don't limit"
msgstr ""

#: kuickconfigdlg.cpp:27
msgid "KuickShow Configuration"
msgstr ""

#: kuickconfigdlg.cpp:32
msgid "General"
msgstr "Ngokubanzi"

#: kuickconfigdlg.cpp:35
msgid "Modifications"
msgstr ""

#: kuickconfigdlg.cpp:38
msgid "Viewer Shortcuts"
msgstr ""

#: kuickconfigdlg.cpp:46
msgid "Browser Shortcuts"
msgstr ""

#: defaultswidget.cpp:22
msgid "Apply default image modifications"
msgstr ""

#: defaultswidget.cpp:27
msgid "Scaling"
msgstr ""

#: defaultswidget.cpp:28
msgid "Shrink image to screensize, if larger"
msgstr ""

#: defaultswidget.cpp:31
msgid "Scale image to screensize, if smaller, up to factor:"
msgstr ""

#: defaultswidget.cpp:39
msgid "Geometry"
msgstr "Ulwazi-lwezibalo"

#: defaultswidget.cpp:40 imagewindow.cpp:699
msgid "Flip vertically"
msgstr ""

#: defaultswidget.cpp:42 imagewindow.cpp:697
msgid "Flip horizontally"
msgstr ""

#: defaultswidget.cpp:44
msgid "Rotate image about"
msgstr ""

#: defaultswidget.cpp:47
msgid "0 degrees"
msgstr ""

#: defaultswidget.cpp:48
msgid "90 degrees"
msgstr ""

#: defaultswidget.cpp:49
msgid "180 degrees"
msgstr ""

#: defaultswidget.cpp:50
msgid "270 degrees"
msgstr ""

#: defaultswidget.cpp:54
msgid "Adjustments"
msgstr "Unyenyiso"

#: defaultswidget.cpp:58 imagewindow.cpp:702
msgid "Brightness"
msgstr "Ukuqaqamba"

#: defaultswidget.cpp:63 imagewindow.cpp:703
msgid "Contrast"
msgstr "Umahluko phakathi kwezinto"

#: defaultswidget.cpp:67 imagewindow.cpp:704
msgid "Gamma"
msgstr "Unobumba wesithathu wesi Grike"

#: defaultswidget.cpp:71
msgid "Preview"
msgstr "Imboniso"

#: defaultswidget.cpp:74
msgid "Original"
msgstr ""

#: defaultswidget.cpp:77
msgid "Modified"
msgstr "Iguqulwe kancinane"

#: imagewindow.cpp:111
msgid "Scroll Up"
msgstr "Rola phezulu"

#: imagewindow.cpp:112
msgid "Scroll Down"
msgstr "Rola ezantsi"

#: imagewindow.cpp:113
msgid "Scroll Left"
msgstr "Rola ekhohlo"

#: imagewindow.cpp:114
msgid "Scroll Right"
msgstr "Rola ekunene"

#: imagewindow.cpp:115
msgid "Zoom In"
msgstr "Yenza ingxolo enkulu ngaphakathi"

#: imagewindow.cpp:116
msgid "Zoom Out"
msgstr "Yenza ingxolo enkulu ngaphandle"

#: imagewindow.cpp:117
msgid "Flip Horizontally"
msgstr ""

#: imagewindow.cpp:118
msgid "Flip Vertically"
msgstr ""

#: imagewindow.cpp:119 imagewindow.cpp:690
msgid "Rotate 90 degrees"
msgstr ""

#: imagewindow.cpp:120 imagewindow.cpp:692
msgid "Rotate 180 degrees"
msgstr ""

#: imagewindow.cpp:121 imagewindow.cpp:694
msgid "Rotate 270 degrees"
msgstr ""

#: imagewindow.cpp:122
msgid "Maximize"
msgstr "Yandisa"

#: imagewindow.cpp:123
msgid "Restore original size"
msgstr ""

#: imagewindow.cpp:124
msgid "More Brightness"
msgstr ""

#: imagewindow.cpp:125
msgid "Less Brightness"
msgstr ""

#: imagewindow.cpp:126
msgid "More Contrast"
msgstr ""

#: imagewindow.cpp:127
msgid "Less Contrast"
msgstr ""

#: imagewindow.cpp:128
msgid "More Gamma"
msgstr ""

#: imagewindow.cpp:129
msgid "Less Gamma"
msgstr ""

#: imagewindow.cpp:130
msgid "Toggle Fullscreen mode"
msgstr ""

#: imagewindow.cpp:210
msgid "Filename (Imagewidth x Imageheight)"
msgstr ""

#: imagewindow.cpp:670 imagewindow.cpp:676 imagewindow.cpp:682
msgid "+"
msgstr "+"

#: imagewindow.cpp:672 imagewindow.cpp:678 imagewindow.cpp:683
msgid "-"
msgstr ""

#: imagewindow.cpp:685
msgid "Zoom in"
msgstr "Yenza ingxolo enkulu ngaphakathi"

#: imagewindow.cpp:687
msgid "Zoom out"
msgstr "Ingxolo enkulu ngaphandle"

#: imagewindow.cpp:706 kuickshow.cpp:162
msgid "Print image..."
msgstr ""

#: imagewindow.cpp:801
msgid "KuickShow: Couldn't print image."
msgstr ""

#: imagewindow.cpp:836
msgid "Couldn't save the file,\n"
msgstr ""

#: imagewindow.cpp:837
msgid "maybe this disk is full, or you don't\n"
msgstr ""

#: imagewindow.cpp:838
msgid "have write permissions to the file."
msgstr ""

#: imagewindow.cpp:839
msgid "File saving failed"
msgstr ""

#: kuickshow.cpp:166
msgid "Delete File"
msgstr "Cima Ifayile"

#: kuickshow.cpp:172
msgid "Slideshow"
msgstr "Umboniso wotyibiliko"

#: kuickshow.cpp:175
msgid "About KuickShow"
msgstr ""

#: kuickshow.cpp:179
msgid "Open only one image window"
msgstr ""

#: kuickshow.cpp:186
msgid "Show Image"
msgstr ""

#: kuickshow.cpp:189
msgid "Show Image in active window"
msgstr ""

#: kuickshow.cpp:194
msgid "Properties..."
msgstr "Iimpahla..."

#: kuickshow.cpp:454
msgid "You can only drop local files\n"
msgstr ""

#: kuickshow.cpp:455
msgid ""
"onto the imageviewer!\n"
"\n"
msgstr ""

#: kuickshow.cpp:456
msgid "KuickShow Drop Error"
msgstr ""

#: kuickshow.cpp:727
msgid ""
"Sorry, I can't load the image\n"
"\""
msgstr ""

#: kuickshow.cpp:729
msgid ""
"\"\n"
"Maybe it has a wrong format or your Imlib\n"
msgstr ""

#: kuickshow.cpp:730
msgid "is not installed properly."
msgstr ""

#: kuickshow.cpp:731
msgid "Image Error"
msgstr ""

#: kuickshow.cpp:756
msgid "Can't initialize \"Imlib\".\n"
msgstr ""

#: kuickshow.cpp:757
msgid "Start kuickshow on the commandline and look\n"
msgstr ""

#: kuickshow.cpp:758
msgid "out for some error messages.\n"
msgstr ""

#: kuickshow.cpp:759
msgid "I will quit now."
msgstr ""

#: kuickshow.cpp:760
msgid "Fatal Imlib error"
msgstr ""

#: kuickio.cpp:26
msgid ""
"Really delete the file\n"
"\n"
msgstr ""

#: kuickio.cpp:28
msgid " ?\n"
msgstr ""

#: kuickio.cpp:30
msgid "Delete file?"
msgstr ""

#: kuickio.cpp:42
msgid ""
"Sorry, I can't delete the file\n"
"\n"
msgstr ""

#: kuickio.cpp:44
msgid "Delete failed"
msgstr ""

