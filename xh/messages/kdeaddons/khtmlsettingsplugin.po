# K Desktop Environment - kdeaddons
# Copyright (C) 2001 translate.org.za
# Antoinette Dekeni <antoinette@transalate.org.za>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: kateinsertcommand\n"
"POT-Creation-Date: 2001-06-16 13:50+0200\n"
"PO-Revision-Date: 2001-09-20 15:37SAST\n"
"Last-Translator: Antoinette Dekeni <antoinette@translate.org.za>\n"
"Language-Team: Xhosa <xhosa@translate.org.za>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: settingsplugin.cpp:41
msgid "HTML Settings"
msgstr "Izicwangciso ze HTML"

#: settingsplugin.cpp:46
msgid "Java&script"
msgstr "Okushicilelweyo &kwe Java"

#: settingsplugin.cpp:51
msgid "&Java"
msgstr "&Java"

#: settingsplugin.cpp:56
msgid "&Cookies"
msgstr "&Cookies"

#: settingsplugin.cpp:61
msgid "&Plugins"
msgstr "&Iiplagi ezifakiweyo"

#: settingsplugin.cpp:66
msgid "Autoload &Images"
msgstr "Ulayisho oluzenzekelayo &lwemifanekiso"

#: settingsplugin.cpp:71
msgid "Enable Cache"
msgstr "Yenza indawo yokugcina efihlakeleyo"

#: settingsplugin.cpp:76
msgid "Cache Policy"
msgstr "Inkqubo yendawo efihlakeleyo yokugcina"

#: settingsplugin.cpp:80
msgid "&Keep Cache In Sync"
msgstr "&Gcina indawo efihlakeleyo yokugcina kwi Sync"

#: settingsplugin.cpp:81
msgid "&Use Cache If Possible"
msgstr "&Sebenzisa indawo efihlakeleyo yokugcina ukuba kuyenzeka"

#: settingsplugin.cpp:82
msgid "&Offline Browsing Mode"
msgstr "&Indlela yokukhangela ngapha kwelayini"

#: settingsplugin.cpp:168
msgid ""
"I can't enable cookies, because the\n"
"cookie daemon could not be started."
msgstr ""
"Andinakwenza cookies, kuba icookie\n"
"daemon ayinakuqalwa."

#: settingsplugin.cpp:170
msgid "Cookies disabled"
msgstr "Zikhubazekile i cookies"
