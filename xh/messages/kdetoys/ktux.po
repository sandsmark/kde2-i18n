# K Desktop Environment - kdetoys
# Copyright (C) 2001 translate.org.za
# Antoinette Dekeni <antoinette@transalate.org.za>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: ktux\n"
"POT-Creation-Date: 2001-03-18 01:31+0100\n"
"PO-Revision-Date: 2001-09-18 08:54SAST\n"
"Last-Translator: Antoinette Dekeni <antoinette@translate.org.za>\n"
"Language-Team: Xhosa <xhosa@translate.org.za>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: sprite.cpp:34
msgid "Tux Screensaver"
msgstr "Umgcini wekhusi we Tux"

#: sprite.cpp:57
msgid "Setup KTux"
msgstr "Cwangcisa i KTux"

#: sprite.cpp:65
msgid "Speed:"
msgstr "Isantya:"

#: sprite.cpp:134
msgid "About KTux"
msgstr "Malunga ne KTux"

#: sprite.cpp:135
msgid ""
"KTux Version 1.0\n"
"\n"
"written by Martin R. Jones 1999\n"
"mjones@kde.org"
msgstr ""
"KTux Uguqululo kolunye ulwimi 1.0\n"
"\n"
"ibhalwe ngu Martin R. Jones 1999\n"
"mjones@kde.org"
