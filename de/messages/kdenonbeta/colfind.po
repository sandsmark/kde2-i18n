msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-05-24 12:49+0200\n"
"PO-Revision-Date: 2000-06-15 16:14MET\n"
"Last-Translator: Matthias Kiefer <matthias.kiefer@gmx.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.5\n"

#: toplevel.cpp:55
msgid "Search Files"
msgstr "Dateien suchen"

#: toplevel.cpp:68
msgid "Where:"
msgstr "Wo:"

#: toplevel.cpp:75
msgid "Search in subdirectories"
msgstr "Suche in Unterverzeichnissen"

#: toplevel.cpp:79
msgid "Descend into mounted file systems"
msgstr "Gemountete Dateisysteme einbeziehen"

#: toplevel.cpp:83
msgid "&Search"
msgstr "&Suchen"

#: toplevel.cpp:101
msgid "More criteria"
msgstr "Mehr Kriterien"

#: toplevel.cpp:102
msgid "Fewer criteria"
msgstr "Weniger Kriterien"

#: toplevel.cpp:110
msgid "Found:"
msgstr "Gefunden:"

#: toplevel.cpp:170
msgid "Found %1 matches"
msgstr "%1 Einträge gefunden"

#: toplevel.cpp:230
msgid "No matches found"
msgstr "Keine Einträge gefunden"

#: toplevel.cpp:231
msgid "Syntax error in pattern"
msgstr "Syntaxfehler im Suchmuster"

#: toplevel.cpp:232
msgid "Ready"
msgstr "Fertig"

#: criterion.cpp:36 criterion.cpp:260 criterion.cpp:279 criterion.cpp:304
#: criterion.cpp:331
msgid "is"
msgstr "ist"

#: criterion.cpp:37 criterion.cpp:63 criterion.cpp:261 criterion.cpp:281
#: criterion.cpp:305 criterion.cpp:333
msgid "is not"
msgstr "ist nicht"

#: criterion.cpp:85 criterion.cpp:122
msgid "today"
msgstr "heute"

#: criterion.cpp:86 criterion.cpp:124
msgid "during the last hour"
msgstr "während der letzten Stunde"

#: criterion.cpp:87 criterion.cpp:126
msgid "during the last day"
msgstr "während des letzten Tages"

#: criterion.cpp:88 criterion.cpp:128
msgid "during the last week"
msgstr "während der letzten Woche"

#: criterion.cpp:89 criterion.cpp:130
msgid "during the last month"
msgstr "während des letzten Monats"

#: criterion.cpp:90 criterion.cpp:132
msgid "during the last year"
msgstr "während des letzten Jahres"

#: criterion.cpp:91 criterion.cpp:134 criterion.cpp:152
msgid "since"
msgstr "seit"

#: criterion.cpp:92 criterion.cpp:139 criterion.cpp:153
msgid "before"
msgstr "vor"

#: criterion.cpp:165
msgid "is (in kB)"
msgstr "ist (in kB)"

#: criterion.cpp:166 criterion.cpp:191
msgid "is smaller than (in kB)"
msgstr "ist kleiner als (in kB)"

#: criterion.cpp:167 criterion.cpp:193
msgid "is greater than (in kB)"
msgstr "ist größer als (in kB)"

#: criterion.cpp:211 criterion.cpp:234
msgid "is a regular file"
msgstr "ist eine reguläre Datei"

#: criterion.cpp:212 criterion.cpp:236
msgid "is a symbolic link"
msgstr "ist eine symbolische Verknüpfung"

#: criterion.cpp:213 criterion.cpp:238
msgid "is an executable"
msgstr "ist eine ausführbare Datei"

#: criterion.cpp:214 criterion.cpp:240
msgid "is an suid executable"
msgstr "ist eine mit root-Privilegien (suid) ausführbare Datei"

#: criterion.cpp:262 criterion.cpp:283 criterion.cpp:292 criterion.cpp:308
#: criterion.cpp:335 criterion.cpp:345
msgid "is invalid"
msgstr "ist ungültig"

#: criterion.cpp:306
msgid "has GID"
msgstr "hat GID"

#: criterion.cpp:307
msgid "does not have GID"
msgstr "hat keine GID"

#: criterion.cpp:359
msgid "Name"
msgstr "Name"

#: criterion.cpp:360
msgid "Last Modified"
msgstr "Zuletzt bearbeitet"

#: criterion.cpp:361
msgid "Size"
msgstr "Größe"

#: criterion.cpp:362
msgid "Type"
msgstr "Typ"

#: criterion.cpp:363
msgid "Owner"
msgstr "Eigentümer"

#: criterion.cpp:364
msgid "Owning group"
msgstr "Gruppenzugehörigkeit"

#: main.cpp:31
msgid "Columbo Find."
msgstr "Columbo Suche."

#: main.cpp:34
msgid "Columbo Find"
msgstr "Columbo Suche"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""
