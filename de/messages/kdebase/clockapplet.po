# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-01-20 01:24+0100\n"
"PO-Revision-Date: 2001-01-18 10:41+MET\n"
"Last-Translator: Thomas Diehl <thd@kde.org>\n"
"Language-Team: Deutsch <kde-i18n-de@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8pre\n"

#: clock.cpp:677
msgid ""
"_: hour\n"
"one"
msgstr "eins"

#: clock.cpp:677
msgid ""
"_: hour\n"
"two"
msgstr "zwei"

#: clock.cpp:678
msgid ""
"_: hour\n"
"three"
msgstr "drei"

#: clock.cpp:678
msgid ""
"_: hour\n"
"four"
msgstr "vier"

#: clock.cpp:678
msgid ""
"_: hour\n"
"five"
msgstr "fünf"

#: clock.cpp:679
msgid ""
"_: hour\n"
"six"
msgstr "sechs"

#: clock.cpp:679
msgid ""
"_: hour\n"
"seven"
msgstr "sieben"

#: clock.cpp:679
msgid ""
"_: hour\n"
"eight"
msgstr "acht"

#: clock.cpp:680
msgid ""
"_: hour\n"
"nine"
msgstr "neun"

#: clock.cpp:680
msgid ""
"_: hour\n"
"ten"
msgstr "zehn"

#: clock.cpp:680
msgid ""
"_: hour\n"
"eleven"
msgstr "elf"

#: clock.cpp:681
msgid ""
"_: hour\n"
"twelve"
msgstr "zwölf"

#: clock.cpp:684
#, no-c-format
msgid "%0 o'clock"
msgstr "%0 Uhr"

#: clock.cpp:685
#, no-c-format
msgid "five past %0"
msgstr "fünf nach %0"

#: clock.cpp:686
#, no-c-format
msgid "ten past %0"
msgstr "zehn nach %0"

#: clock.cpp:687
#, no-c-format
msgid "quarter past %0"
msgstr "viertel nach %0"

#: clock.cpp:688
#, no-c-format
msgid "twenty past %0"
msgstr "zwanzig nach %0"

#: clock.cpp:689
#, no-c-format
msgid "twenty five past %0"
msgstr "fünf vor halb %1"

#: clock.cpp:690
#, no-c-format
msgid "half past %0"
msgstr "halb %1"

#: clock.cpp:691
#, no-c-format
msgid "twenty five to %1"
msgstr "fünf nach halb %1"

#: clock.cpp:692
#, no-c-format
msgid "twenty to %1"
msgstr "zwanzig vor %1"

#: clock.cpp:693
#, no-c-format
msgid "quarter to %1"
msgstr "viertel vor %1"

#: clock.cpp:694
#, no-c-format
msgid "ten to %1"
msgstr "zehn vor %1"

#: clock.cpp:695
#, no-c-format
msgid "five to %1"
msgstr "fünf vor %1"

#: clock.cpp:696
#, no-c-format
msgid "%1 o'clock"
msgstr "%1 Uhr"

#: clock.cpp:698
#, no-c-format
msgid ""
"_: one\n"
"%0 o'clock"
msgstr "%0 Uhr"

#: clock.cpp:699
#, no-c-format
msgid ""
"_: one\n"
"five past %0"
msgstr "fünf nach %0"

#: clock.cpp:700
#, no-c-format
msgid ""
"_: one\n"
"ten past %0"
msgstr "zehn nach %0"

#: clock.cpp:701
#, no-c-format
msgid ""
"_: one\n"
"quarter past %0"
msgstr "viertel nach %0"

#: clock.cpp:702
#, no-c-format
msgid ""
"_: one\n"
"twenty past %0"
msgstr "zwanzig nach %0"

#: clock.cpp:703
#, no-c-format
msgid ""
"_: one\n"
"twenty five past %0"
msgstr "fünf vor halb %0"

#: clock.cpp:704
#, no-c-format
msgid ""
"_: one\n"
"half past %0"
msgstr "halb %1"

#: clock.cpp:705
#, no-c-format
msgid ""
"_: one\n"
"twenty five to %1"
msgstr "fünf nach halb %1"

#: clock.cpp:706
#, no-c-format
msgid ""
"_: one\n"
"twenty to %1"
msgstr "zwanzig vor %1"

#: clock.cpp:707
#, no-c-format
msgid ""
"_: one\n"
"quarter to %1"
msgstr "viertel vor %1"

#: clock.cpp:708
#, no-c-format
msgid ""
"_: one\n"
"ten to %1"
msgstr "zehn vor %1"

#: clock.cpp:709
#, no-c-format
msgid ""
"_: one\n"
"five to %1"
msgstr "fünf vor %1"

#: clock.cpp:710
#, no-c-format
msgid ""
"_: one\n"
"%1 o'clock"
msgstr "%1 Uhr"

#: clock.cpp:747
msgid "Night"
msgstr "Nacht"

#: clock.cpp:748
msgid "Early morning"
msgstr "Früher Morgen"

#: clock.cpp:748
msgid "Morning"
msgstr "Morgen"

#: clock.cpp:748
msgid "Almost noon"
msgstr "Fast Mittag"

#: clock.cpp:749
msgid "Noon"
msgstr "Mittag"

#: clock.cpp:749
msgid "Afternoon"
msgstr "Nachmittag"

#: clock.cpp:749
msgid "Evening"
msgstr "Abend"

#: clock.cpp:750
msgid "Late evening"
msgstr "Später Abend"

#: clock.cpp:756
msgid "Start of week"
msgstr "Wochenanfang"

#: clock.cpp:758
msgid "Middle of week"
msgstr "Mitte der Woche"

#: clock.cpp:760
msgid "End of week"
msgstr "Ende der Woche"

#: clock.cpp:762
msgid "Weekend!"
msgstr "Wochenende"

#: clock.cpp:1023
msgid "Clock"
msgstr "Uhr"

#: clock.cpp:1026
msgid "&Digital"
msgstr "&Digital"

#: clock.cpp:1027
msgid "&Analog"
msgstr "&Analog"

#: clock.cpp:1028
msgid "&Fuzzy"
msgstr "&Umgangssprachlich"

#: clock.cpp:1037
msgid "&Type"
msgstr "&Typ"

#: clock.cpp:1039
msgid "&Adjust Date && Time..."
msgstr "D&atum && Zeit einstellen..."

#: clock.cpp:1040
msgid "Date && Time &Format..."
msgstr "&Format für Datum && Zeit..."

#: rc.cpp:1
msgid "Clock Preferences"
msgstr "Einrichtung der Uhr"

#: rc.cpp:2
msgid "Type"
msgstr "Typ"

#: rc.cpp:3
msgid ""
"Here you can select the clock type.<br> digital clock: a LCD-style clock<br> "
"analog clock: a traditional clock with clock face and hands"
msgstr ""
"Hier können Sie den Grundtyp der Uhr festlegen.<br> Digitale Uhr: eine LCD-U"
"hr<br> Analoge Uhr: eine traditionelle Uhr mit Zifferblatt und Zeigern"

#: rc.cpp:4
msgid "&digital clock"
msgstr "&Digitale Uhr"

#: rc.cpp:5
msgid "analo&g clock"
msgstr "Analo&ge Uhr"

#: rc.cpp:6
msgid "fuzz&y clock"
msgstr "&Umgangssprachliche Uhrzeit"

#: rc.cpp:7
msgid "Here you can set various options for both clock styles."
msgstr "Hier können Sie verschiedene Einstellungen für beide Uhren-Typen vornehmen."

#: rc.cpp:8
msgid "D&igital Clock"
msgstr "D&igitale Uhr"

#: rc.cpp:10 rc.cpp:22 rc.cpp:33
msgid "s&how date"
msgstr "Datum &anzeigen"

#: rc.cpp:11 rc.cpp:23
msgid "show s&econds"
msgstr "Se&kunden anzeigen"

#: rc.cpp:12
msgid "blinking do&ts"
msgstr "Bl&inkende Punkte"

#: rc.cpp:13 rc.cpp:24 rc.cpp:37
msgid "Colors"
msgstr "Farben"

#: rc.cpp:14 rc.cpp:25
msgid "You can choose either the predefined LCD look or define a custom color set."
msgstr ""
"Sie können entweder die vorgegebene LCD-Optik übernehmen oder benutzerdefin"
"ierte Farben verwenden."

#: rc.cpp:15 rc.cpp:30
msgid "&LCD look"
msgstr "&LCD-Optik"

#: rc.cpp:16 rc.cpp:26 rc.cpp:39
msgid "&Background Color:"
msgstr "&Hintergrundfarbe:"

#: rc.cpp:17 rc.cpp:27
msgid "&Shadow Color:"
msgstr "&Schattenfarbe:"

#: rc.cpp:18 rc.cpp:28 rc.cpp:38
msgid "&Foreground Color:"
msgstr "&Vordergrundfarbe:"

#: rc.cpp:19 rc.cpp:29
msgid "&Custom colors"
msgstr "&Benutzerfarben"

#: rc.cpp:20
msgid "A&nalog Clock"
msgstr "A&naloge Uhr"

#: rc.cpp:31
msgid "F&uzzy Clock"
msgstr "&Umgangssprachliche Uhrzeit"

#: rc.cpp:34
msgid "Fu&zziness:"
msgstr "Un&schärfe:"

#: rc.cpp:35
msgid "Low"
msgstr "Gering"

#: rc.cpp:36
msgid "High"
msgstr "Hoch"

#: rc.cpp:40
msgid "Choose Fon&t"
msgstr "Schrif&t wählen"

#: rc.cpp:41
msgid "Font:"
msgstr "Schriftart:"

#: rc.cpp:43
msgid "&Apply"
msgstr "&Anwenden"
