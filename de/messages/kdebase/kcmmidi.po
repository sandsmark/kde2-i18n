msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 02:05+0200\n"
"PO-Revision-Date: 2002-04-25 16:47+MET\n"
"Last-Translator: Thomas Diehl <thd@kde.org>\n"
"Language-Team: Deutsch <kde-i18n-de@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.6\n"

#: midi.cpp:42
msgid "Select the midi device you want to use :"
msgstr "Wählen Sie das Midi-Gerät, das Sie verwenden möchten:"

#: midi.cpp:60
msgid "Use Midi Mapper"
msgstr "Midi-Mapper benutzen"

