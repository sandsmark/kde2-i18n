# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-06-10 23:15+MET\n"
"Last-Translator: Gregor Zumstein <zumstein@ssd.ethz.ch>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: index.docbook:11
msgid "The &knotes; Handbook"
msgstr "Das &knotes;-Handbuch"

#: index.docbook:14
msgid ""
"<firstname>Fabian</firstname> <surname>Dal Santo</surname> <affiliation> "
"<address><email>linuxgnu@yahoo.com.au</email></address> </affiliation>"
msgstr ""
"<firstname>Fabian</firstname> <surname>Dal Santo</surname> <affiliation> "
"<address><email>linuxgnu@yahoo.com.au</email></address> </affiliation>"

#: index.docbook:22
msgid ""
"<firstname>Greg</firstname> <othername>M.</othername> "
"<surname>Holmes</surname>"
msgstr ""
"<firstname>Greg</firstname> <othername>M.</othername> "
"<surname>Holmes</surname>"

#: index.docbook:31
msgid "Reviewer"
msgstr "Durchsicht"

#: index.docbook:33
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit "
"role=\"translator\"><firstname>Gregor</firstname><surname>Zumstein</surname>"
"<affiliation><address><email>zumstein@ssd.ethz.ch</email></address>"
"</affiliation><contrib>Übersetzung von</contrib></othercredit>"

#: index.docbook:39
msgid "Greg M. Holmes"
msgstr "Greg M. Holmes"

#: index.docbook:43
msgid "Fabian Del Santo"
msgstr "Fabian Dal Santo"

#: index.docbook:51
msgid "&knotes; is a sticky notes application for the desktop."
msgstr "&knotes; sind die Post-It's&reg; für die Arbeitsfläche."

#: index.docbook:55
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:56
msgid "Knotes"
msgstr "Knotes"

#: index.docbook:57
msgid "kdeutils"
msgstr "kdeutils"

#: index.docbook:58
msgid "notes"
msgstr "notizen"

#: index.docbook:59
msgid "popup"
msgstr "popup"

#: index.docbook:60
msgid "pop-up"
msgstr "popup"

#: index.docbook:61
msgid "knotes"
msgstr "knotes"

#: index.docbook:68
msgid "Introduction"
msgstr "Einleitung"

#: index.docbook:70
msgid ""
"&knotes; is a program that lets you write the computer equivalent of sticky "
"notes. The notes are saved automatically when you exit the program, and they "
"display when you open the program."
msgstr ""
"&knotes; erlaubt das Anbringen virtueller Haftnotizen (\"Post-it\") auf dem "
"Bildschirm. Die Notizen werden beim Beenden des Programms automatisch "
"gespeichert und beim Neustart wieder geladen und angezeigt."

#: index.docbook:74
msgid ""
"You may print and mail your notes if you configure &knotes; to use helper "
"applications."
msgstr ""
"Wenn die entsprechenden Einstellungen vorgenommen sind, können die Notizen "
"auch gedruckt oder per E-Mail versendet werden."

#: index.docbook:77
msgid ""
"Display features of notes such as color and font may be customized for each "
"note. You may also customize the defaults."
msgstr ""
"Die Notizen verwenden frei einstellbare Farben und Schriftarten. Es können "
"sowohl die Standardeinstellungen wie auch die Einstellungen für einzelne "
"Notizen verändert werden."

#: index.docbook:83
msgid "Configuration"
msgstr "Einstellen"

#: index.docbook:86
msgid "Configuring the Display in &knotes;"
msgstr "Die &knotes;-Anzeige einstellen"

#: index.docbook:88
msgid "To configure the display:"
msgstr "Um die Anzeige einzustellen, ist das Vorgehen wie folgt:"

#: index.docbook:90 index.docbook:133 index.docbook:487
msgid "<mousebutton>Right</mousebutton> click on the panel icon."
msgstr ""
"Mit der <mousebutton>rechten</mousebutton> Maustaste auf die Titelleiste "
"klicken."

#: index.docbook:90 index.docbook:133 index.docbook:169
msgid "Select <guimenuitem>Preferences...</guimenuitem>"
msgstr "<guimenuitem>Notizeinstellungen...</guimenuitem> aus dem Menü wählen."

#: index.docbook:90 index.docbook:133 index.docbook:169
msgid "The &knotes; <guilabel>Defaults - KNotes</guilabel> dialog will open."
msgstr ""
"Daraufhin öffnet sich der Dialog <guilabel>Lokale Einstellungen - "
"KNotes</guilabel>."

#: index.docbook:90 index.docbook:372
msgid "Choose the <guiicon>Display</guiicon> icon from the three icons at the left."
msgstr "Das Symbol <guiicon>Anzeige</guiicon> auf der linken Seite anwählen."

#: index.docbook:99 index.docbook:143 index.docbook:178 index.docbook:382
#: index.docbook:423
msgid "The following choices are available:"
msgstr "Folgendes steht zur Auswahl:"

#: index.docbook:101 index.docbook:384
msgid "Text Color"
msgstr "Textfarbe"

#: index.docbook:101 index.docbook:384
msgid ""
"The color square shows the current text color. By clicking this color square "
"you open the standard &kde; color selection dialog."
msgstr ""
"Die aktuell eingestellte Farbe wird rechts mit einem farbigen Rechteck "
"angezeigt. Ein Klick auf dieses Rechteck öffnet den Standard-&kde;Dialog "
"zur Farbauswahl."

#: index.docbook:101 index.docbook:384
msgid "Background Color"
msgstr "Hintergrundfarbe"

#: index.docbook:101
msgid ""
"The color square shows the current background color. By clicking this color "
"square you open the standard &kde; color selection dialog."
msgstr ""
"Die aktuell eingestellte Farbe wird rechts mit einem farbigen Rechteck "
"angezeigt. Ein Klick auf dieses Rechteck öffnet den Standard-&kde;-Dialog "
"zur Farbauswahl."

#: index.docbook:101 index.docbook:384
msgid "Note Width"
msgstr "Breite der Notiz"

#: index.docbook:101 index.docbook:384
msgid "The width of the note in pixels. Edit this number as desired."
msgstr "Die Breite der Notiz in Pixeln. Diese kann nach Herzenslust geändert werden."

#: index.docbook:101 index.docbook:384
msgid "Note Height"
msgstr "Höhe der Notiz"

#: index.docbook:101 index.docbook:384
msgid "The height of the note in pixels. Edit this number as desired."
msgstr "Die Höhe der Notiz in Pixeln. Diese kann nach Herzenslust geändert werden."

#: index.docbook:131
msgid "Configuring the Editor in &knotes;"
msgstr "Die Texteingabe in &knotes; einstellen"

#: index.docbook:132
msgid "To customize the editor:"
msgstr "Um die Texteingabe einzustellen, ist das Vorgehen wie folgt:"

#: index.docbook:133 index.docbook:413
msgid "Choose the <guiicon>Editor</guiicon> icon from the three icons at the left."
msgstr "Das Symbol <guiicon>Texteingabe</guiicon> auf der linken Seite wählen."

#: index.docbook:145 index.docbook:425
msgid "Tab Size"
msgstr "Tabulatorbreite"

#: index.docbook:145 index.docbook:425
msgid "This is the size of the tab in spaces. Edit this number as desired."
msgstr ""
"Gibt an, wie vielen Leerzeichen ein Tabulator entspricht. Diese Zahl kann "
"wie gewünscht geändert werden."

#: index.docbook:145 index.docbook:425
msgid "Auto Indent"
msgstr "Automatisch einrücken"

#: index.docbook:145 index.docbook:425
#, fuzzy
msgid "This is a check box. If selected, auto-indenting is on."
msgstr ""
"Wenn dieses Ankreuzfeld angewählt ist, so wird eine neue Zeile automatisch "
"so weit wie die vorhergehende eingerückt."

#: index.docbook:145 index.docbook:425
msgid "Current Font: Click to Change"
msgstr "Aktuelle Schriftart: Zum Ändern klicken"

#: index.docbook:145 index.docbook:425
msgid "Click this button to open the standard &kde; font selection dialog."
msgstr ""
"Ein Klick auf diesen Knopf öffnet den Standard-&kde;-Dialog zur "
"Schriftauswahl."

#: index.docbook:166
msgid "Configuring Actions in &knotes;"
msgstr "Aktionen in &knotes; einstellen"

#: index.docbook:168
msgid "To configure actions in &knotes;:"
msgstr "Um die Aktionen in &knotes; einzustellen, ist das Vorgehen wie folgt:"

#: index.docbook:169 index.docbook:335
msgid "Right-click on the &knotes; panel icon."
msgstr ""
"Mit der <mousebutton>rechten</mousebutton>"
" Maustaste auf das &knotes;-Symbol in der Kontrollleiste klicken."

#: index.docbook:169
msgid "Choose the <guiicon>Actions</guiicon> icon from the three icons at the left."
msgstr "Das Symbol <guiicon>Aktionen</guiicon> auf der linken Seite wählen."

#: index.docbook:180 index.docbook:461
msgid "Mail Action"
msgstr "Versenden"

#: index.docbook:180 index.docbook:461
msgid "Type a mail command and any required command line switches in this box."
msgstr ""
"Hier muss ein E-Mail-Programm inklusive aller Befehlszeilenparameter "
"angegeben werden."

#: index.docbook:180 index.docbook:461
#, fuzzy
msgid ""
"By using <token>%f</token> in the command line you can pass the filename of "
"the note body to the mail command."
msgstr ""
"Der Platzhalter <option>\"%f\"</option> auf der Befehlszeile steht für den "
"Text der Notiz."

#: index.docbook:180
msgid "Print"
msgstr "Drucken"

#: index.docbook:180 index.docbook:461
msgid "Type a printing command and any required command line switches in this box."
msgstr ""
"Hier muss ein Druckbefehl inklusive aller Befehlszeilenparameter angegeben "
"werden."

#: index.docbook:180 index.docbook:461
msgid "Use the following variables in the command line:"
msgstr "Die folgenden Platzhalter können verwendet werden:"

#: index.docbook:180 index.docbook:461
#, fuzzy
msgid "<token>%p</token> is the printer name."
msgstr "<option>\"%p\"</option> ist der Druckername."

#: index.docbook:180 index.docbook:461
#, fuzzy
msgid "<token>%t</token> is the title."
msgstr "<option>\"%t\"</option> ist der Titel der Notiz."

#: index.docbook:180 index.docbook:461
#, fuzzy
msgid "<token>%f</token> is the filename of the note body."
msgstr "<option>\"%f\"</option> ist der Dateiname des Notiztextes."

#: index.docbook:207
msgid "Using &knotes;"
msgstr "Wie benutze ich &knotes;"

#: index.docbook:210
msgid "Creating a New Note"
msgstr "Erstellen einer neuen Notiz"

#: index.docbook:212
msgid "To create a new note:"
msgstr "Um eine neue Notiz zu erstellen, ist das Vorgehen wie folgt:"

#: index.docbook:213
msgid "<mousebutton>Right</mousebutton> click on the &knotes; panel icon."
msgstr ""
"Mit der <mousebutton>rechten</mousebutton> Maustaste auf die Titelleiste "
"klicken."

#: index.docbook:213
msgid "Select New Note."
msgstr "<guimenuitem>Neue Notiz</guimenuitem> aus dem Menü wählen."

#: index.docbook:222
msgid "Writing Your Note"
msgstr "Eine Notiz schreiben"

#: index.docbook:224
msgid ""
"To write your note, simply type the note in the space provided. Normal "
"keyboard and mouse editing functions are supported."
msgstr ""
"Um eine Notiz zu machen, schreibt man einfach im freien Bereich. Die "
"gewöhnliche Tastatur- und Mausfunktionalität ist vorhanden."

#: index.docbook:227
msgid ""
"<mousebutton>Right</mousebutton> clicking in the editing space provides the "
"following menu options:"
msgstr ""
"Ein Klick mit der <mousebutton>rechten</mousebutton> Maustaste im "
"Textbereich öffnet ein Menü mit den Menüpunkten:"

#: index.docbook:230
msgid "Undo"
msgstr "Rückgängig"

#: index.docbook:230
msgid "Redo"
msgstr "Wiederherstellen"

#: index.docbook:230
msgid "<guimenuitem>Cut</guimenuitem>"
msgstr "Ausschneiden"

#: index.docbook:230
msgid "Copy"
msgstr "Kopieren"

#: index.docbook:230
msgid "Paste"
msgstr "Einfügen"

#: index.docbook:230
msgid "Paste special..."
msgstr "Einfügen spezial..."

#: index.docbook:230
msgid "Clear"
msgstr "Löschen"

#: index.docbook:230
msgid "Select All"
msgstr "Alles auswählen"

#: index.docbook:241
msgid ""
"Text may be selected by holding down the <mousebutton>left</mousebutton> "
"mouse button and moving the mouse, or by holding down the "
"<keycap>Shift</keycap> key and using the <keycap>arrow</keycap> keys."
msgstr ""
"Text wird ausgewählt indem entweder die <mousebutton>linke</mousebutton> "
"Maustaste gedrückt und die Maus bewegt wird oder die "
"<keycap>Umschalt</keycap>-Taste gedrückt wird und die "
"<keycap>Pfeil</keycap>-Tasten verwendet werden."

#: index.docbook:248
msgid "Inserting the Date"
msgstr "Datum einfügen"

#: index.docbook:250
msgid "To insert the current date in the Note:"
msgstr "Um das aktuelle Datum in die Notiz einzufügen, ist das Vorgehen wie folgt:"

#: index.docbook:251 index.docbook:348 index.docbook:356 index.docbook:413
#: index.docbook:449
msgid "<mousebutton>Right</mousebutton> click on the title bar of the note."
msgstr ""
"Mit der <mousebutton>rechten</mousebutton> Maustaste auf die Titelleiste "
"klicken."

#: index.docbook:251
msgid "Select <guimenuitem>Insert Date</guimenuitem>."
msgstr "<guimenuitem>Datum einfügen</guimenuitem> aus dem Menü wählen."

#: index.docbook:256
msgid ""
"The current date and time will be inserted at the cursor position in the "
"text of the note."
msgstr "Daraufhin wird das aktuelle Datum an der Cursorposition im Text eingefügt."

#: index.docbook:262
msgid "Renaming a Note"
msgstr "Eine Notiz umbenennen"

#: index.docbook:264
msgid "To rename a note:"
msgstr "Um eine Notiz umzubenennen, ist das Vorgehen wie folgt:"

#: index.docbook:265 index.docbook:282 index.docbook:298 index.docbook:313
#, fuzzy
msgid "<mousebutton>Right</mousebutton> click on the note title bar."
msgstr ""
"Mit der <mousebutton>rechten</mousebutton> Maustaste auf die Titelleiste "
"klicken."

#: index.docbook:265
msgid "Select <guimenuitem>Rename</guimenuitem>."
msgstr "<guimenuitem>Umbenennen</guimenuitem> aus dem Menü wählen."

#: index.docbook:271
msgid ""
"Type the new name of the note in the dialog that appears. To accept the new "
"name, press the <guibutton>OK</guibutton>"
" button. To exit the dialog without renaming the note, press the "
"<guibutton>Cancel</guibutton>"
" button. To clear what you have typed and start over, click the "
"<guibutton>Clear</guibutton> button."
msgstr ""
"Im sich öffnenden Dialog muss der neue Name eingegeben werden. "
"<guibutton>OK</guibutton> ändert den Namen, "
"<guibutton>Abbrechen</guibutton> kehrt zur Notiz zurück, ohne etwas zu "
"ändern und <guibutton>Löschen</guibutton>"
" löscht den Text im Eingabefeld, so dass von vorne begonnen werden kann."

#: index.docbook:279
msgid "Mailing a Note"
msgstr "Eine Notiz versenden"

#: index.docbook:281
msgid "To mail a note:"
msgstr "Um eine Notiz zu versenden, ist das Vorgehen wie folgt:"

#: index.docbook:282
msgid "Select <guimenuitem>Mail</guimenuitem>."
msgstr "<guimenuitem>Versenden</guimenuitem> aus dem Menü wählen."

#: index.docbook:288
msgid ""
"What happens next depends on how you configured the Mail action in the "
"<guilabel>Preferences...</guilabel> dialog."
msgstr ""
"Das weitere Vorgehen hängt vom Befehl ab, der im "
"<guilabel>Notizeinstellungen...</guilabel>-Dialog unter Aktionen angegeben "
"wurde."

#: index.docbook:294
msgid "Printing a Note"
msgstr "Eine Notiz drucken"

#: index.docbook:296
msgid "To print a note:"
msgstr "Um eine Notiz zu drucken, ist das Vorgehen wie folgt:"

#: index.docbook:298
msgid "Select <guimenuitem>Print</guimenuitem>."
msgstr "<guimenuitem>Drucken</guimenuitem> aus dem Menü wählen."

#: index.docbook:304
msgid ""
"What happens next depends on how you configured the Print action in the "
"<guilabel>Preferences...</guilabel> dialog."
msgstr ""
"Das weitere Vorgehen hängt vom Befehl ab, der im "
"<guilabel>Notizeinstellungen...</guilabel>-Dialog unter Aktionen angegeben "
"wurde."

#: index.docbook:311
msgid "Deleting a Note"
msgstr "Eine Notiz löschen"

#: index.docbook:312
msgid "To delete a note:"
msgstr "Um eine Notiz zu löschen, ist das Vorgehen wie folgt:"

#: index.docbook:313
msgid "Select <guimenuitem>Delete Note</guimenuitem>."
msgstr "<guimenuitem>Notiz löschen</guimenuitem> aus dem Menü wählen."

#: index.docbook:321
msgid "Hiding a Note"
msgstr "Eine Notiz verstecken"

#: index.docbook:323
msgid ""
"To hide a note, click the <guiicon>X</guiicon> in the upper right corner of "
"the title bar of the note. The note will no longer be displayed on the "
"screen. The note itself will not be deleted."
msgstr ""
"Um eine Notiz zu verstecken, genügt ein Klick auf das <guiicon>X</guiicon> "
"in der oberen rechten Ecke. Die Notiz erscheint nicht weiter auf dem "
"Bildschirm, wird aber nicht gelöscht."

#: index.docbook:330
msgid "Displaying Notes"
msgstr "Notizen anzeigen"

#: index.docbook:332
msgid ""
"When you start &knotes;, all notes will display on the screen. If you hide a "
"note and later want to display it:"
msgstr ""
"Beim Start von &knotes; werden alle Notizen auf dem Bildschirm angezeigt. "
"Wenn aber eine versteckte Notiz wieder angezeigt werden soll, ist das "
"Vorgehen wie folgt:"

#: index.docbook:335
msgid "Select <guimenuitem>Notes</guimenuitem>."
msgstr "<guimenuitem>Notiz</guimenuitem> aus dem Menü wählen."

#: index.docbook:335
msgid "Choose the note you want to display."
msgstr "Die gewünschte Notiz auswählen."

#: index.docbook:344
msgid "Desktop Functions"
msgstr "Funktionen für die Arbeitsflächen"

#: index.docbook:346
msgid "To send a note to a specific desktop:"
msgstr ""
"Um eine Notiz auf eine bestimmte Arbeitsfläche zu verschieben, ist das "
"Vorgehen wie folgt:"

#: index.docbook:348
msgid "Select <guimenuitem>To Desktop</guimenuitem>"
msgstr "<guimenuitem>Auf Arbeitsfläche</guimenuitem> aus dem Menü wählen."

#: index.docbook:348
msgid ""
"Choose the desktop desired, or alternatively, <guimenuitem>All "
"Desktops</guimenuitem>"
msgstr ""
"Die gewünschte Arbeitsfläche wählen oder aber <guimenuitem>Alle "
"Arbeitsflächen</guimenuitem>"
", um &knotes; auf allen Arbeitsflächen auf dem Bildschirm anzuzeigen."

#: index.docbook:355
msgid "To make the note remain on top of other windows:"
msgstr ""
"Um eine Notiz immer zuoberst, über allen anderen Fenstern, anzuzeigen, ist "
"das Vorgehen wie folgt:"

#: index.docbook:356
msgid "Select <guimenuitem>Always on Top</guimenuitem>."
msgstr "<guimenuitem>Immer im Vordergrund</guimenuitem> aus dem Menü wählen."

#: index.docbook:362
msgid ""
"To return the note to more normal window behavior, simply repeat this "
"process."
msgstr ""
"Um zur normalen Einstellung zurückzukehren, muss dieser Vorgang einfach "
"nochmals durchgeführt werden."

#: index.docbook:368
#, fuzzy
msgid "Customizing a Note Display"
msgstr "Die Anzeige einer einzelnen Notiz einstellen"

#: index.docbook:370
#, fuzzy
msgid "To customize a note display:"
msgstr "Um die Anzeige einer Notiz zu verändern, ist das Vorgehen wie folgt:"

#: index.docbook:372
#, fuzzy
msgid "<guimenuitem>Right</guimenuitem> click on the note title bar."
msgstr ""
"Mit der <guimenuitem>rechten</guimenuitem>"
" Maustaste auf die Titelleiste der Notiz klicken."

#: index.docbook:372
msgid "Select <guimenuitem>Note preferences...</guimenuitem>."
msgstr "<guimenuitem>Notizeinstellungen...</guimenuitem> aus dem Menü wählen."

#: index.docbook:372 index.docbook:413 index.docbook:449
msgid "The <guilabel>Local Settings - Knotes</guilabel> dialog will open."
msgstr ""
"Daraufhin öffnet sich der Dialog <guilabel>Lokale Einstellungen - "
"KNotes</guilabel>."

#: index.docbook:384
msgid "The color square shows the current text color."
msgstr "Das farbige Rechteck rechts zeigt die aktuelle Hintergrundfarbe an."

#: index.docbook:410
#, fuzzy
msgid "Customizing a Note Editor"
msgstr "Die Texteingabe einstellen"

#: index.docbook:412
#, fuzzy
msgid "To customize a note editor:"
msgstr "Um die Texteingabe einzustellen, ist das Vorgehen wie folgt:"

#: index.docbook:413 index.docbook:449
msgid "Select <guimenuitem>Note preferences...</guimenuitem>"
msgstr "<guimenuitem>Notizeinstellungen...</guimenuitem> aus dem Menü wählen."

#: index.docbook:446
#, fuzzy
msgid "Customizing a Note Actions"
msgstr "Aktionen für eine einzelne Notiz einstellen"

#: index.docbook:447
#, fuzzy
msgid "To customize a note actions:"
msgstr "Um die Aktionen für eine Notiz einzustellen, ist das Vorgehen wie folgt:"

#: index.docbook:449
msgid "Choose the <guiicon>Action</guiicon> icon from the three icons at the left."
msgstr "Das Symbol <guiicon>Aktionen</guiicon> auf der linken Seite wählen."

#: index.docbook:459
msgid "The following choices are available"
msgstr "Die folgenden Einstellungen stehen zur Auswahl:"

#: index.docbook:461
msgid "Print Action"
msgstr "Drucken"

#: index.docbook:485
msgid "Quitting &knotes;"
msgstr "&knotes; beenden"

#: index.docbook:486
msgid "To quit &knotes;:"
msgstr "Um &knotes; zu beenden, ist das Vorgehen wie folgt:"

#: index.docbook:487
msgid "Select <guimenuitem>Quit</guimenuitem>."
msgstr "<guimenuitem>Beenden</guimenuitem> aus dem Menü wählen."

#: index.docbook:498
msgid "Credits and License"
msgstr "Danksagung und Lizenz"

#: index.docbook:500
msgid "&knotes;"
msgstr "&knotes;"

#: index.docbook:502
msgid "Program copyright 1997 Bernd Wuebben <email>wuebben@kde.org</email>"
msgstr "Copyright für das Programm 1997 Bernd Wuebben <email>wuebben@kde.org</email>"

#: index.docbook:504
msgid "Contributors:"
msgstr "Beiträge von:"

#: index.docbook:505
msgid "Wynn Wilkes<email>wynnw@calderasystems.com</email>"
msgstr "Wynn Wilkes<email>wynnw@calderasystems.com</email>"

#: index.docbook:510
msgid ""
"Documentation copyright 2000 Greg M. Holmes "
"<email>holmegm@earthlink.net</email>"
msgstr ""
"Copyright für die Dokumentation 2000 Greg M. Holmes "
"<email>holmegm@earthlink.net</email>"

#: index.docbook:513
msgid ""
"Documentation updated 2001 by Fabian Del Santo "
"<email>linuxgnu@yahoo.com.au</email>."
msgstr "Überarbeitet 2001 von Fabian Dal Santo <email>linuxgnu@yahoo.com.au</email>."

#: index.docbook:516
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<para>Deutsche &Uuml;bersetzung von Gregor Zumstein "
"<email>zumstein@ssd.ethz.ch</email></para>"

#: index.docbook:524
msgid "Installation"
msgstr "Installation"

#: index.docbook:527
msgid "How to obtain &knotes;"
msgstr "Woher Sie &knotes; erhalten"

#: index.docbook:529
msgid ""
"&knotes; is part of the &kde; project <ulink "
"url=\"http://www.kde.org\">http://www.kde.org</ulink>"
". &knotes; can be found in the kdeutils package on <ulink "
"url=\"ftp://ftp.kde.org/pub/kde/\">ftp://ftp.kde.org/pub/kde/</ulink>, the "
"main ftp site of the &kde; project."
msgstr ""
"&knotes; ist Teil des kdeutils-Pakets und sollte kompiliert werden, wie es "
"im Hauptverzeichnis des Pakets beschrieben ist. Neue Versionen von kdeutils "
"finden Sie auf der Seite <ulink "
"url=\"ftp://ftp.kde.org/pub/kde/\">ftp://ftp.kde.org/pub/kde/</ulink>."

#: index.docbook:538
msgid "Requirements"
msgstr "Voraussetzungen"

#: index.docbook:540
#, fuzzy
msgid ""
"In order to print or mail notes, you need to have helper applications "
"installed, such as <command>a2ps</command> and &kmail;. You will need to "
"configure &knotes; to use these helper applications."
msgstr ""
"Um Notizen zu drucken oder per E-Mail zu versenden, werden externe Programme "
"wie <application>a2ps</application> oder &kmail; benötigt. Um diese "
"Programme zu verwenden, muss &knotes; entsprechend eingerichtet werden."

#: index.docbook:548
msgid "Compilation and Installation"
msgstr "Kompilierung und Installation"

#: index.docbook:550
msgid ""
"In order to compile and install &knotes; on your system, type the following "
"in the base directory of the kdeutils distribution:"
msgstr ""
"Um &knotes; auf Ihrem System zu kompilieren und installieren, geben Sie "
"Folgendes im obersten Verzeichnis des kdeutils-Pakets ein:"

#: index.docbook:553
msgid ""
"<prompt>%</prompt><userinput><command>./configure</command></userinput>\n"
"<prompt>%</prompt><userinput><command>make</command></userinput>\n"
"<prompt>%</prompt><userinput><command>make</command> "
"<option>install</option></userinput>"
msgstr ""
"<prompt>%</prompt><userinput><command>./configure</command></userinput>\n"
"<prompt>%</prompt><userinput><command>make</command></userinput>\n"
"<prompt>%</prompt><userinput><command>make install</command></userinput>"

#: index.docbook:559
#, fuzzy
msgid ""
"Since &knotes; uses <command>autoconf</command> and "
"<command>automake</command> you should have no trouble compiling it. Should "
"you run into problems please report them to the &kde; mailing lists."
msgstr ""
"Da &knotes; <application>autoconf</application> und "
"<application>automake</application> benutzt, sollten Sie beim Kompilieren "
"keine Probleme haben. Falls doch, teilen Sie bitte Ihr Problem auf den "
"&kde;-Mailinglisten mit."

#~ msgid ""
#~ "The color square shows the current text color. By clicking this color "
#~ "square you open the standard KDE color selection dialog."
#~ msgstr ""
#~ "Das farbige Rechteck rechts zeigt die aktuelle Textfarbe an. Ein Klick "
#~ "auf dieses Rechteck öffnet den Standard &kde;-Dialog zur Farbauswahl."

#~ msgid "<mousebutton>Right</mousebutton> click on the titlebar of the note."
#~ msgstr ""
#~ "Mit der <mousebutton>rechten</mousebutton> Maustaste auf die Titelleiste "
#~ "der Notiz klicken."
