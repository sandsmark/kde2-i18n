msgid ""
msgstr ""
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-08-01 11:43GMT\n"
"Last-Translator: Frank Schütte <F.Schuette@t-online.de>\n"
"Language-Team: Deutsch <kde-i18n-doc-de@kde.org>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.4\n"

#: index.docbook:5
msgid "<firstname>Mike</firstname> <surname>McBride</surname>"
msgstr "<firstname>Mike</firstname> <surname>McBride</surname>"

#: index.docbook:9
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit "
"role=\"translator\"><firstname>Stefan</firstname><surname>Winter</surname>"
"<affiliation><address><email>kickdown@online.de</email></address>"
"</affiliation><contrib>Deutsche Übersetzung</contrib></othercredit>"

#: index.docbook:16
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:17
msgid "KControl"
msgstr "Kontrollzentrum"

#: index.docbook:18
msgid "files association"
msgstr "Dateizuordnung"

#: index.docbook:19
msgid "association"
msgstr "Zuordnung"

#: index.docbook:22
msgid "File Associations"
msgstr "Dateizuordnungen"

#: index.docbook:23
msgid "Introduction"
msgstr "Einleitung"

#: index.docbook:25
msgid ""
"One of the most convenient aspects of &kde;, is its ability to automatically "
"match a data file, with its application. As an example, when you click on "
"your favorite &kword; document in &konqueror;, &kde; automatically starts "
"&kword;, and automatically loads that file into &kword; so you can begin "
"working on it."
msgstr ""
"Eine der bequemsten Eigenschaften von &kde; ist die Fähigkeit eine "
"Zuordnung von einer Datendatei zur zugehörigen Anwendung herzustellen. Wenn "
"man zum Beispiel in &konqueror; auf ein &kword;-Dokument klickt, startet "
"&kde; automatisch &kword; und lädt dieses Dokument in &kword;."

#: index.docbook:31
msgid ""
"In the example above, the &kword; Data file, is "
"<emphasis>associated</emphasis> with &kword; (the application). These file "
"associations are crucial to the functioning of &kde;."
msgstr ""
"Im obigen Beispiel ist die &kword;-Datei dem Programm &kword; "
"<emphasis>zugeordnet</emphasis>"
". Dies Dateizuordnungen sind grundlegend für die Funktion von &kde;."

#: index.docbook:35
msgid ""
"When &kde; is installed, it automatically creates hundreds of file "
"associations to many of the most common data types. These initial "
"associations are based on the most commonly included software, and the most "
"common user preferences."
msgstr ""
"Bei der Installation erstellt &kde; automatisch hunderte von "
"Dateizuordnungen für die gebräuchlisten Dateitypen. Diese Zuordnungen "
"verbinden diese Dateitypen mit den üblicherweise zu &kde; gehörenden "
"Programmen und den allgemeinen Benutzereinstellungen."

#: index.docbook:40
msgid "Unfortunately, &kde; can not:"
msgstr "Aber &kde; kann nicht:"

#: index.docbook:42
msgid "predict every possible combination of software and data files"
msgstr "jede mögliche Kombination von Programmen und Dateitypen vorhersehen"

#: index.docbook:42
msgid "prepare for file formats not yet invented"
msgstr "auf noch nicht erfundene Dateiformate vorbereitet sein"

#: index.docbook:42
msgid "or predict everyone's favorite application for certain file formats"
msgstr "jedermanns bevorzugtes Programm für einen bestimmten Dateityp erraten"

#: index.docbook:48
msgid ""
"You can change your current file associations or add new file associations "
"using this module."
msgstr ""
"In diesem Modul können die augenblicklichen Zuordnungen verändert und neue "
"Zuordnungen hinzugefügt werden."

#: index.docbook:51
msgid ""
"Each file association is recorded as a mime type. <acronym>MIME</acronym> "
"stands for <quote>Multipurpose Internet Mail Extensions</quote>"
". It allows a computer to determine the type of file, without opening and "
"analyzing the format of each and every file."
msgstr ""
"Jede Zuordnung wird als Mime-Typ (<acronym>MIME</acronym> steht für "
"<quote>Multipurpose Internet Mail Extensions</quote>) gespeichert. Mit "
"diesen Typen kann der Computer den Dateityp feststellen, ohne jede einzelne "
"Datei zu öffnen und das Datenformat explizit zu analysieren."

#: index.docbook:59
msgid "How to use this module"
msgstr "Benutzung dieses Moduls"

#: index.docbook:61
msgid "The file associations are organized into 7 categories:"
msgstr "Die Dateizuordnungen sind in 7 Kategorien gegliedert:"

#: index.docbook:63
msgid "Applications"
msgstr "Applications (Anwendungen)"

#: index.docbook:63
msgid "Audio"
msgstr "Audio"

#: index.docbook:63
msgid "Images"
msgstr "Images (Bilder)"

#: index.docbook:63
msgid "Inode"
msgstr "Inode"

#: index.docbook:63
msgid "Messages"
msgstr "Messages (Nachrichten)"

#: index.docbook:63
msgid "Text"
msgstr "Text"

#: index.docbook:63
msgid "Videos"
msgstr "Videos"

#: index.docbook:73
msgid "All of the file associations are sorted into one of these categories."
msgstr "Jede Dateizuordnung wird einer dieser Kategorien zugeordnet."

#: index.docbook:76
msgid ""
"There is also an overview category <quote>All</quote>, which displays all "
"the file types in one list, without categorizing them."
msgstr ""
"Die Kategorie <quote>All</quote>"
" (Alles) enthält alle Zuordnungen und dient dem Überblick."

#: index.docbook:80
msgid ""
"There is no functional difference between any of the categories. These "
"categories are designed to help organize your file associations, but they do "
"not alter the associations in any way."
msgstr ""
"Diese Kategorien haben keine funktionale Bedeutung. Sie helfen bei der "
"Organisation der Dateizuordnungen, unterscheiden sich aber nicht in der "
"Funktion."

#: index.docbook:85
msgid ""
"The categories are listed in the box labeled <guilabel>Known "
"Types</guilabel>."
msgstr ""
"Die Kategorien sind aufgelistet in dem Listenfeld <guilabel>Bekannte "
"Typen</guilabel>."

#: index.docbook:88
msgid ""
"You can explore each of these categories, and see the file associations "
"contained within each one, by simply double clicking on the category name. "
"You will be presented with a list of the associated mime-types under that "
"category."
msgstr ""
"Klickt man auf einen Kategorienamen, so werden darunter alle Mime-Typen "
"angezeigt, die dieser Kategorie zugerechnet worden sind."

#: index.docbook:93
msgid ""
"You can also search for a particular mime-type by using the search box. The "
"search box is labeled <guilabel>Find filename pattern</guilabel> and is "
"located above the category list."
msgstr ""
"Man kann nach einem bestimmten Mime-Typ mit Hilfe des oberhalb der Liste "
"vorhandenen Textfeldes <guilabel>Dateimuster suchen</guilabel> suchen."

#: index.docbook:93
msgid ""
"Simply type the first letter of the mime-type you are interested in. The "
"categories are automatically expanded, and only the mime-types that include "
"that letter are displayed."
msgstr ""
"Sobald man den ersten Buchstaben in das Textfeld eintippt, werden die "
"Mime-Typen angezeigt, die diesen Buchstaben enthatlen."

#: index.docbook:93
msgid ""
"You can then enter a second character and the mime-types will be further "
"limited to mime types containing those two characters."
msgstr ""
"Tippt man den zweiten Buchstaben ein, werden nur die Mime-Typen angezeigt, "
"die beide Buchstaben enthalten."

#: index.docbook:106
msgid "Adding a new mime type."
msgstr "Einen neuen Mime-Typ hinzufügen."

#: index.docbook:108
msgid ""
"If you want to add a new mime type to your file associations, you can click "
"on the <guibutton>Add</guibutton> button. A small dialog box will appear. "
"You select the category from the drop down box, and type the mime name in "
"the blank labeled <guilabel>Type name</guilabel>. Click "
"<guibutton>OK</guibutton> to add the new mime type, or click "
"<guibutton>Cancel</guibutton> to not add any new mime-types."
msgstr ""
"Um einen neuen Mime-Typ für eine Dateizuordnung zu erstellen, klickt man "
"auf den Knopf <guibutton>Hinzufügen</guibutton>. Ein Dialog erscheint, in "
"welchem man die gewünschte Kateogorie im Listenfeld "
"<guilabel>Gruppe</guilabel> und den Typnamen im Textfeld "
"<guilabel>Name</guilabel> festlegen kann. Ist alles eingegeben, wird der "
"Mime-Typ durch Klicken auf <guibutton>OK</guibutton>"
" erstellt, durch Klicken auf <guibutton>Abbrechen</guibutton> werden die "
"Eingaben verworfen und der neue Typ nicht erstellt."

#: index.docbook:119
msgid "Removing a mime type."
msgstr "Einen Mime-Typ entfernen."

#: index.docbook:121
msgid ""
"If you want to remove a mime type, simply select the mime type you want to "
"delete by clicking once with the mouse on the mime type name. Then click the "
"button labeled <guibutton>Remove</guibutton>. The mime type will be deleted "
"immediately."
msgstr ""
"Man wählt den gewünschten Mime-Typ durch einen Klick auf den Typnamen. "
"Dann klickt man auf den Knopf <guibutton>Entfernen</guibutton>. Damit wird "
"der Mime-Typ sofort gelöscht."

#: index.docbook:129
msgid "Editing a mime types properties."
msgstr "Die Eigenschaften eines Mime-Typen verändern."

#: index.docbook:131
msgid ""
"Before you can edit a mime types property, you must first specify which mime "
"type. Simply browse through the categories until you find the mime type you "
"want to edit, then click once on it with the mouse."
msgstr ""
"Bevor man Veränderungen vornehmen kann, muss man zunächst den gewünschten "
"Mime-Typ in der Liste der Bekannten Typen durch einen Mausklick auswählen."

#: index.docbook:136
msgid ""
"As soon as you have selected the mime type, the current values of the mime "
"type will appear in the module window."
msgstr ""
"Daraufhin werden die aktuellen Einstellungen dieses Mime-Typs im "
"Modulfenster angezeigt."

#: index.docbook:139
msgid ""
"You will notice the current values are split into two tabs: "
"<guilabel>General</guilabel> and <guilabel>Embedding</guilabel>"
msgstr ""
"Die aktuellen Werte werden auf den beiden Karteikarten "
"<guilabel>Allgemein</guilabel> und <guilabel>Einbetten</guilabel> angezeigt."

#: index.docbook:142
msgid "General"
msgstr "Allgemein"

#: index.docbook:142
msgid "There are 4 properties for each mime type in this tab:"
msgstr "Jeder Mime-Typ hat vier Eigenschaften auf dieser Karteikarte:"

#: index.docbook:142
msgid ""
"<guilabel>Mime Type Icon</guilabel> is the icon th7at will be visible when "
"using &konqueror; as a file manager."
msgstr ""
"<guilabel>Mime-Typ-Symbol</guilabel>"
" ist das für diesen Typ im Dateimanager &konqueror; angezeigte Symbol."

#: index.docbook:142
msgid ""
"<guilabel>Filename Patterns</guilabel> is a search pattern which &kde; will "
"use to determine the mime type."
msgstr ""
"<guilabel>Dateimuster</guilabel> ist ein Suchmuster, mit dessen Hilfe &kde; "
"Dateien dieses Mime-Typs findet."

#: index.docbook:142
msgid ""
"<guilabel>Description</guilabel> is a short description of the file type. "
"This is for your benefit only."
msgstr ""
"<guilabel>Beschreibung</guilabel> beschreibt den Dateityp kurz. Dieses Feld "
"hat keine weitere Funktion."

#: index.docbook:142
msgid ""
"<guilabel>Application Preference Order</guilabel> determines which "
"applications will be associated with the specified mime-type."
msgstr ""
"<guilabel>Bevorzugte Anordnung von Programmen</guilabel> legt fest, welche "
"Programme diesem Mime-Typ zugeordnet werden."

#: index.docbook:142
msgid "Embedding Tab"
msgstr "Karteikarte Einbetten"

#: index.docbook:142
msgid ""
"The Embedding tab allows you to determine if an file will be viewed within a "
"&konqueror; window, or by starting the application."
msgstr ""
"Auf dieser Karteikarte wird festgelegt, ob dieser Dateityp innerhalb von "
"&konqueror; oder durch Starten der Anwendung angezeigt wird."

#: index.docbook:176
msgid "Changing the Icon"
msgstr "Ändern des Symbols"

#: index.docbook:178
msgid ""
"To change the icon, simply click on the Icon button. A dialog box will "
"appear, which will show you all available icons. Simply click once with the "
"mouse on the icon of your choice, and click <guibutton>OK</guibutton>."
msgstr ""
"Um das Symbol zu verändern, klickt man auf das Symbol. Es erscheint ein "
"Dialog, der alle verfügbaren Symbole anzeigt. Man wählt ein neues Symbol "
"durch Klicken aus und bestätigt mit <guibutton>OK</guibutton>."

#: index.docbook:186
msgid "Editing the mime-type patterns"
msgstr "Ändern der Mime-Typ-Dateimuster"

#: index.docbook:188
msgid ""
"The box labeled <guilabel>Filename Patterns</guilabel>, determines what "
"files will be included within this mime-type."
msgstr ""
"Das Feld <guilabel>Dateimuster</guilabel>"
" bestimmt, welche Dateien zu diesem Mime-Typ gehören."

#: index.docbook:191
msgid ""
"Usually, files are selected based on their suffix. (Examples: Files that end "
"with <literal role=\"extension\">.wav</literal> are sound files, using the "
"WAV format and files that end in <literal role=\"extension\">.c</literal> "
"are program files written in C)."
msgstr ""
"Normalerweise wird die Dateiendung für die Bestimmung des Mime-Typs "
"verwendet. (Beispiele: Dateien, die auf <literal "
"role=\"extension\">.wav</literal>"
" enden, sind Sounddateien im WAV-Format und Dateien, die auf <literal "
"role=\"extension\">.c</literal> enden, sind Quelltexte zu Programmen in der "
"Programmiersprache C)."

#: index.docbook:196
msgid "You should enter your filename mask in this combo box."
msgstr "Man tippt das gewünschte Dateimuster in dieses Listenfeld ein."

#: index.docbook:198
msgid ""
"The asterisk (*) is a wildcard character that will be used with nearly every "
"mime type mask. A complete discussion of wildcards is beyond the scope of "
"this manual, but it is important to understand that the asterisk (in this "
"context), <quote>matches</quote> any number of characters. As an example: "
"<userinput>*.pdf</userinput> will match <filename>Datafile.pdf</filename>, "
"<filename>Graphics.pdf</filename> and <filename>User.pdf</filename>"
", but not <filename>PDF</filename>, <filename>Datafile.PDF</filename>, or "
"<filename>.pdf</filename>."
msgstr ""
"Das Sternchen (*) ist ein Spezialzeichen, das wohl in jedem Mime-Typ "
"auftaucht. Eine vollständige Diskussion von Spezialzeichen geht über "
"diesen Hilfetext hinaus. Wichtig ist, dass das Sternchen (in diesem "
"Zusammenhang) für eine beliebige Anzahl <quote>beliebiger</quote> Zeichen "
"steht. Zum Beispiel: Das Dateimuster <userinput>*.pdf</userinput> enthält "
"<filename>Datendatei.pdf</filename>, <filename>Graphik.pdf</filename> und "
"<filename>Benutzer.pdf</filename>, aber nicht <filename>PDF</filename>, "
"<filename>Datendatei.PDF</filename> oder <filename>.pdf</filename>"

#: index.docbook:207
msgid ""
"It is very beneficial to have multiple masks. One for lower case, one for "
"upper case, etc. This will help ensure that &kde; can determine the file "
"type more accurately."
msgstr ""
"Es ist günstig, mehrere Dateimuster zu haben; mindestens eines für Klein- "
"und eines für Großbuchstaben. Das stellt sicher, dass &kde; den Mime-Typ "
"einer Datei bestimmen kann."

#: index.docbook:214
msgid "Editing a mime types description."
msgstr "Ändern der Mime-Typ-Beschreibung."

#: index.docbook:216
msgid ""
"You can type a short description of the mime type in the text box labeled "
"<guilabel>Description</guilabel>. This lable is to help you, it does not "
"affect the function of the mime type."
msgstr ""
"Man kann eine kurze Beschreibung des Mime-Typs in dem Feld "
"<guilabel>Beschreibung</guilabel> eintragen. Diese Beschreibung hat keine "
"Funktion. Sie dient lediglich dem Benutzer zur Identifkation des Mime-Typs."

#: index.docbook:223
msgid "Editing the application associations"
msgstr "Ändern der zugeordneten Anwendungen"

#: index.docbook:225
msgid ""
"There are four buttons (<guibutton>Move Up</guibutton>, <guibutton>Move "
"Down</guibutton>, <guibutton>Add</guibutton> and "
"<guibutton>Remove</guibutton>) and a combo box (which lists the "
"applications) which are used to configure the applications."
msgstr ""
"Für die Konfiguration der zugeordneten Anwendungen gibt es vier Knöpfe "
"(<guibutton>Nach oben</guibutton>, <guibutton>Nach unten</guibutton>, "
"<guibutton>Hinzufügen</guibutton> und <guibutton>Entfernen</guibutton>) "
"sowie ein Listenfeld (es listet die Anwendungen auf)."

#: index.docbook:230
msgid ""
"The combo box lists all of the applications associated with a specific "
"mime-type. The list is in a specific order. The top application is the first "
"application tried. The next application down the list is the second, etc."
msgstr ""
"Das Listenfeld enthält alle dem Mime-Typ zugeordneten Anwendungen. Die "
"Reihenfolge ist dabei wichtig. Es wird zuerst die Anwendung probiert, die "
"oben steht, danach die Zweite und so weiter."

#: index.docbook:235
msgid ""
"What do you mean there is more than one application per mime-type? Why is "
"this necessary?"
msgstr "Warum gibt es mehr als eine Anwendung pro Mime-Typ? Wozu ist das notwendig?"

#: index.docbook:235
msgid ""
"We started out by saying that &kde; comes preconfigured with hundreds of "
"file associations. The reality is, each system that &kde; is installed on "
"has a different selection of applications. By allowing multiple associations "
"per mime-type, KDE can continue to operate when a certain application is not "
"installed on the system."
msgstr ""
"Anfangs wurde bereits gesagt, das &kde; mit hunderten von vorkonfigurierten "
"Dateizuordnungen ausgeliefert wird. Jedes System, auf dem &kde; installiert "
"ist, unterscheidet sich darin, welche Anwendungen installiert sind. Da "
"mehrere Anwendungen pro Mime-Typ zur Auswahl stehen, kann &kde; "
"funktionieren, auch wenn eine bestimmte Anwendung nicht installiert ist."

#: index.docbook:235
msgid "As an example:"
msgstr "Zum Beispiel:"

#: index.docbook:235
msgid ""
"For the mime type <literal>pdf</literal>, there are two applications "
"associated with this file type. The first program is called <application>PS "
"Viewer</application>. If your system does not have <application>PS "
"Viewer</application> installed, then &kde; automatically starts the second "
"application <application>Adobe Acrobat Reader</application>"
". As you can see, this will help keep &kde; running strong as you add and "
"subtract applications."
msgstr ""
"Für den Mime-Typ <literal>pdf</literal> gibt es zwei zugeordnete "
"Anwendungen. Die erste ist der <application>PS/PDF-Betrachter</application>"
". Falls der <application>PS/PDF-Betrachter</application> nicht installiert "
"ist, startet &kde; automatisch die zweite Anwendung, den <application>Adobe "
"Acrobat Reader</application>"
". Damit bleibt &kde; in Funktion obwohl sich die installierten Programme "
"verändern."

#: index.docbook:253
msgid ""
"We have established that the order is important. You can change the order of "
"the applications by clicking once with the mouse on the application you want "
"to move, and then clicking either <guibutton>Move Up</guibutton> or "
"<guibutton>Move Down</guibutton>. This will shift the currently selected "
"application up or down the list of applications."
msgstr ""
"Wir erwähnten bereits, dass die Reihenfolge der Anwendungen wichtig ist. "
"Die Reihenfolge kann geändert werden, indem man auf die Anwendung klickt, "
"deren Position verändert werden soll, und dann je nach Wunsch auf die "
"Knöpfe <guibutton>Nach oben</guibutton> und <guibutton>Nach "
"unten</guibutton>"
". Jeder Klick verschiebt die ausgewählte Anwendung um eine Position in der "
"Liste."

#: index.docbook:260
msgid ""
"You can add new applications to the list by clicking the button labeled "
"<guibutton>Add</guibutton>. A dialog box will appear. Using the dialog box, "
"you can select the application you want to use for this mime type. Click "
"<guibutton>OK</guibutton> when you are done, and the application will be "
"added to the current list."
msgstr ""
"Um neue Anwendungen hinzuzufügen, klickt man auf "
"<guibutton>Hinzufügen</guibutton>. Es erscheint ein Dialog, mit dessen "
"Hilfe man die gewünschte Anwendung auswählen kann. Klickt man zur "
"Bestätigung auf <guibutton>OK</guibutton>, so wird diese Anwendung zur "
"Liste der Anwendungen für diesen Mime-Typ hinzugefügt."

#: index.docbook:266
msgid ""
"You can remove an application (thereby ensuring that the application will "
"never run with this mime-type by clicking once on the name of the "
"application, and clicking the <guibutton>Remove</guibutton> button."
msgstr ""
"Um zu verhindern, dass eine bestimmte Anwendung mit diesem Mime-Typ "
"ausgeführt wird, klickt man auf die gewünschte Anwendung um sie "
"auszuwählen. Ein Klick auf <guibutton>Entfernen</guibutton> löscht die "
"Anwendung aus der Liste."

#: index.docbook:271
msgid ""
"It is a good idea to use the <guibutton>Move Up</guibutton> and "
"<guibutton>Move Down</guibutton> buttons to adjust the unwanted application "
"to a lower position in the list, rather than deleting the application from "
"the list entirely. Once you have deleted an application, if your preferred "
"application should become compromised, there will not be an application to "
"view the data document."
msgstr ""
"Normalerweise ist es sinnvoller, eine ungewünschte Anwendung mit Hilfe der "
"Knöpfe <guibutton>Nach oben</guibutton> und <guibutton>Nach "
"unten</guibutton> an das Ende der Liste der Anwendungen zu stellen anstatt "
"sie ganz zu löschen. Wenn die gewünschte Anwendung für diesen Typ einmal "
"unerreichbar werden sollte, hat man so immer noch wenigstens eine Anwendung, "
"um diesen Mime-Typ zu betrachten."

#: index.docbook:281
msgid "Embedding"
msgstr "Einbetten"

#: index.docbook:282
msgid ""
"By clicking on the <guilabel>Embedding</guilabel> tab, you are presented "
"with:"
msgstr "Klickt man auf <guilabel>Einbetten</guilabel> erscheint folgende Karteikarte:"

#: index.docbook:285
msgid ""
"Three radio buttons labeled <guilabel>Left click action</guilabel>. This "
"determines how &konqueror; views the selected mimetype."
msgstr ""
"Eine Gruppe von drei Auswahlknöpfen mit dem Titel "
"<guibutton>Links-Klick-Aktion</guibutton>. Hier kann man festlegen, wie "
"&konqueror; den Mime-Typ anzeigt."

#: index.docbook:289
msgid "Show file in embedded viewer"
msgstr "Datei in eingebettetem Betrachter anzeigen"

#: index.docbook:289
msgid ""
"If this is selected, the file will be shown <emphasis>within</emphasis> the "
"&konqueror; window."
msgstr ""
"Wenn diese Option gewählt ist, wird die Datei "
"<emphasis>innerhalb</emphasis> des &konqueror;-Fensters angezeigt."

#: index.docbook:289
msgid "Show file in seperate viewer"
msgstr "Datei in extra Betrachter anzeigen"

#: index.docbook:289
msgid "This will cause a seperate window to be created when showing this mime-type."
msgstr ""
"Bei dieser Option wird zum Anzeigen dieses Mime-Typs ein eigenes Fenster "
"erstellt."

#: index.docbook:289
msgid "Use group settings."
msgstr "Einstellungen der Gruppe \"...\" verwenden."

#: index.docbook:289
msgid ""
"This will cause the mime-type to use the settings for the mime-type group. "
"(if you are editing an audio mime type, then the settings for the audio "
"group are used)."
msgstr ""
"Diese Option sorgt dafür, dass die Einstellungen der jeweiligen Gruppe "
"verwendet werden. (Wenn man einen Audio-Mime-Typ editiert, werden die "
"Einstellungen der Audio-Gruppe verwendet)."

#: index.docbook:299
msgid ""
"Below this, is a listbox labled <guilabel>Services Preference "
"Order</guilabel>"
msgstr ""
"Darunter befindet sich das Listenfeld <guilabel>Bevorzugte Anordnung der "
"ausführenden Programme</guilabel>"

#: index.docbook:302
msgid ""
"When you are in &konqueror;, you can <mousebutton>right</mousebutton> mouse "
"click, and a menu will with an entry labled <guimenu>Preview "
"with...</guimenu> will appear. This box lists the applications that will "
"appear, in the order they will appear, under this menu."
msgstr ""
"In &konqueror; kann man durch klicken der <mousebutton>rechten "
"Maustaste</mousebutton> ein Kontextmenü öffnen. Dieses enthält das "
"Untermenü <guimenu>Vorschau in...</guimenu>. Das Listenfeld zeigt die "
"Anwendungen in der Reihenfolge an, wie sie unter dem Menü dann erscheinen."

#: index.docbook:308
msgid ""
"You can use the <guibutton>Move Up</guibutton> and <guibutton>Move "
"Down</guibutton> buttons to change the order."
msgstr ""
"Die Reihenfolge kann mit den Knöpfen <guibutton>Nach oben</guibutton> und "
"<guibutton>Nach unten</guibutton> verändert werden."

#: index.docbook:314
msgid "Making changes permanent"
msgstr "Änderungen speichern"

#: index.docbook:316
msgid ""
"When you are done making any changes to mime types, you can click "
"<guibutton>Apply</guibutton>"
" to make your changes permanent, but keep you in this module."
msgstr ""
"Um die gemachten Änderungen zu übernehmen und im Modul zu bleiben, klickt "
"man auf <guibutton>Anwenden</guibutton>."

#: index.docbook:320
msgid ""
"You can click <guibutton>OK</guibutton> to make your changes permanent and "
"return you to the main screen of the control center."
msgstr ""
"Um die Änderungen zu speichern und zum Hauptfenster des Kontrollzentrums "
"zurückzukehren, klickt man auf <guibutton>OK</guibutton>."

#: index.docbook:324
msgid ""
"You can click <guibutton>Cancel</guibutton> to abort any changes and return "
"you to the main screen of the control center."
msgstr ""
"Um die Änderungen zu verwerfen und zum Hauptfenster zurückzukehren, klickt "
"man auf <guibutton>Abbrechen</guibutton>."

#: index.docbook:331
msgid "Section Author"
msgstr "Abschnittsautor"

#: index.docbook:332
msgid "Mike McBride <email>mpmcbride7@yahoo.com</email>"
msgstr "Mike McBride <email>mpmcbride7@yahoo.com</email>"

#: index.docbook:333
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<para>Deutsche Übersetzung: Stefan Winter "
"<email>kickdown@online.de</email></para>"
