# Ratoûrnaedje è walon des messaedjes di KDE.
#
# Lorint Hendschel <LorintHendschel@skynet.be>, 1999
# Pablo Saratxaga <pablo@mandrakesoft.com>, 2001
#
msgid ""
msgstr ""
"Project-Id-Version: kcmbell 0.4\n"
"POT-Creation-Date: 2001-06-10 16:05+0200\n"
"PO-Revision-Date: 1999-03-18 23:11+0100\n"
"Last-Translator: Lorint Hendschel <LorintHendschel@skynet.be>\n"
"Language-Team: <linux-wa@chanae.alphanet.ch>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: bell.cpp:77
msgid "Bell Settings"
msgstr "Apontiaedjes pol xhîlete"

#: bell.cpp:88
msgid "&Use System Bell instead of System Notification"
msgstr "&Eployî li xhîlete do sistinme purade kel notifiaedje do sistinme"

#: bell.cpp:89
msgid ""
"You can use the standard system bell (PC speaker) or a more sophisticated "
"system notification, see the \"System Notifications\" control module for the "
"\"Something Special Happened in the Program\" event."
msgstr ""
"Vos ploz eployî li xhîlete standard del copiutrece (PC speaker) oudobén "
"des pus sûtis notifiaedjes, voeyoz li module «Notifiaedjes do sistinme» "
"do  cente di contrôle po l' evenmint «Yåk di speciål est arivé avou "
"l' programe»."

#: bell.cpp:98
msgid "&Volume:"
msgstr "&Volume:"

#: bell.cpp:103
msgid ""
"Here you can customize the volume of the system bell. For further "
"customization of the bell, see the \"Accessibility\" control module."
msgstr ""
"Chal vos ploz apontyî li foice del xhîlete. Po des pus spepieus "
"apontiaedjes voeyoz li module «Accessibilité» do cente di contrôle."

#: bell.cpp:107
msgid "&Pitch:"
msgstr "&Ton:"

#: bell.cpp:109
msgid "Hz"
msgstr "Hz"

#: bell.cpp:112
msgid ""
"Here you can customize the pitch of the system bell. For further "
"customization of the bell, see the \"Accessibility\" control module."
msgstr ""
"Chal vos ploz apontyî li hôteu del xhîlete. Po des pus spepieus "
"apontiaedjes voeyoz li module «Accessibilité» do cente di contrôle."

#: bell.cpp:116
msgid "&Duration:"
msgstr "&Longueu:"

#: bell.cpp:118
msgid "ms"
msgstr "ms"

#: bell.cpp:121
msgid ""
"Here you can customize the duration of the system bell. For further "
"customization of the bell, see the \"Accessibility\" control module."
msgstr ""
"Chal vos ploz apontyî li tins kel xhîlete xhufele. Po des pus spepieus "
"apontiaedjes voeyoz li module «Accessibilité» do cente di contrôle."

#: bell.cpp:125
msgid "&Test"
msgstr "&Sayî"

#: bell.cpp:129
msgid ""
"Click \"Test\" to hear how the system bell will sound using your changed "
"settings."
msgstr ""
"Clitchoz sol boton «Sayî» po schoûter li son del xhîlete avou les "
"apontiaedjes ki vos avoz fwait."

#: bell.cpp:228
msgid ""
"<h1>System Bell</h1>"
" Here you can customize the sound of the standard system bell, i.e. the "
"\"beep\" you always hear when there's something wrong. Note that you can "
"further customize this sound using the \"Accessibility\" control module: for "
"example you can choose a sound file to be played instead of the standard "
"bell."
msgstr ""
"<h1>Xhîlete do sistinme</h1> Chal vos ploz apontyî l' son fwait pal "
"xhîlete standard do sistinme, dj' ô bén, li «biiip» ki vos oyoz a tots "
"côps cwand  gn a yåk ki n' va nén. Notez ki vos ploz fé des pus "
"spepieus apontiaedjes viè l' module «Accessibilité» do cente di "
"contrôle; par egzimpe vos ploz tchwezi on fitchî son a djower purade kel "
"simpe xhuflaedje del xhîlete."

#~ msgid "&Bell"
#~ msgstr "&Xhîlete"

#~ msgid "usage: kcmbell [-init | bell]\n"
#~ msgstr "po s' è siervi: kcmbell [-init | bell]\n"

#~ msgid "milliseconds"
#~ msgstr "milisegondes"
