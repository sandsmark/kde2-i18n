# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-25 14:49+0200\n"
"PO-Revision-Date: 2001-09-09 16:58GMT\n"
"Last-Translator: �ystein Skadsem <oskadsem@start.no>\n"
"Language-Team: no\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: cupsdconf.cpp:596 cupsdconf.cpp:618 cupsdconf.cpp:634
msgid "_: Base\nRoot"
msgstr ""
"_: Base\n"
"Topp"

#: cupsdconf.cpp:596 cupsdconf.cpp:616 cupsdconf.cpp:636
msgid "All printers"
msgstr "Alle skriverne"

#: cupsdconf.cpp:596 cupsdconf.cpp:617 cupsdconf.cpp:637
msgid "All classes"
msgstr "Alle klasser"

#: cupsdconf.cpp:597 cupsdconf.cpp:615 cupsdconf.cpp:635
msgid "Administration"
msgstr "Administrasjon"

#: cupsdconf.cpp:598 cupsdconf.cpp:624 cupsdconf.cpp:627 cupsdconf.cpp:647
msgid "Class"
msgstr "Klasse"

#: cupsdconf.cpp:599 cupsdconf.cpp:619 cupsdconf.cpp:622 cupsdconf.cpp:641
msgid "Printer"
msgstr "Skriver"

#: cupsdconf.cpp:638
msgid "Root"
msgstr "Topp"

#: cupsddialog.cpp:100
msgid "CUPS server configuration"
msgstr "CUPS tjener-oppsett"

#: cupsddialog.cpp:185
msgid "Error while loading configuration file !"
msgstr "Feil under lasting av oppsettfilen !"

#: cupsddialog.cpp:185 cupsddialog.cpp:194 cupsddialog.cpp:237
#: cupsddialog.cpp:268
msgid "CUPS configuration error"
msgstr "CUPS oppsett-feil"

#: cupsddialog.cpp:212
msgid "Unable to find a running CUPS server"
msgstr "Finner ikke en kj�rende CUPS-tjener"

#: cupsddialog.cpp:217
msgid "Unable to restart CUPS server (pid = %1)"
msgstr "Kan ikke omstarte CUPS-tjener (pid = %1)"

#: cupsddialog.cpp:229
msgid "File \"%1\" doesn't exist !"
msgstr "Filen \"%1\" finnes ikke !"

#: cupsddialog.cpp:231
msgid "Can't open file \"%1\" !\nCheck file permissions."
msgstr ""
"Kan ikke �pne filen \"%1\" !\n"
"Sjekk filrettighetene."

#: cupsddialog.cpp:233
msgid "You are not allowed to modify file \"%1\" !\nCheck file permissions or contact system administrator."
msgstr ""
"Du har ikke lov til � forandre filen \"%1\" !\n"
"Sjekk filrettighetene eller kontakt systemadministratoren."

#: cupsddialog.cpp:262
#, c-format
msgid "Unable to write configuration file %1"
msgstr "Kan ikke skrive oppsettfilen %1"

#: cupsdoption.cpp:34
msgid "Toggle default value"
msgstr "Forandre standardverdi"

#: cupsdpage.cpp:29
msgid "CUPS print system"
msgstr "Uskriftsystemet CUPS"

#: cupsdserverdirpage.cpp:36 cupsdserverencryptpage.cpp:34
#: cupsdserverhttppage.cpp:36 cupsdserveridentitypage.cpp:34
#: cupsdserverlogpage.cpp:36 cupsdservermiscpage.cpp:38
msgid "Server"
msgstr "Tjener"

#: cupsdserverdirpage.cpp:37
msgid "Directories"
msgstr "Kataloger"

#: cupsdserverdirpage.cpp:38
msgid "Server directories configuration"
msgstr "Konfigurasjon av serverkataloger"

#: cupsdserverdirpage.cpp:50
msgid "Executables:"
msgstr "Eksekverbare:"

#: cupsdserverdirpage.cpp:51
msgid "Configuration:"
msgstr "Oppsett:"

#: cupsdserverdirpage.cpp:52
msgid "Data:"
msgstr "Data:"

#: cupsdserverdirpage.cpp:53
msgid "Temporary files:"
msgstr "Midlertidige filer:"

#: cupsdserverdirpage.cpp:54
msgid "Temporary requests:"
msgstr "Tempor�re foresp�rsler:"

#: cupsdserverdirpage.cpp:55
msgid "Font path:"
msgstr "Sti til skrifttype:"

#: cupsdserverhttppage.cpp:37
msgid "HTTP"
msgstr "HTTP"

#: cupsdserverhttppage.cpp:38
msgid "Server HTTP configuration"
msgstr "Innstilling av HTTP tjener"

#: cupsdserverhttppage.cpp:47
msgid "Document directory:"
msgstr "Dokument katalog:"

#: cupsdserverhttppage.cpp:48
msgid "Default language:"
msgstr "Standard spr�k:"

#: cupsdserverhttppage.cpp:49
msgid "Default charset:"
msgstr "Standard tegnsett:"

#: cupsdserveridentitypage.cpp:36
msgid "Server general configuration"
msgstr "Generell server konfigurasjon"

#: cupsdserveridentitypage.cpp:47
msgid "Server name:"
msgstr "Tjenernavn"

#: cupsdserveridentitypage.cpp:48
msgid "Administrator's email:"
msgstr "Administrators epostadresse"

#: cupsdserveridentitypage.cpp:49
msgid "Server user:"
msgstr "Tjener-bruker:"

#: cupsdserveridentitypage.cpp:50
msgid "Server group:"
msgstr "Tjener-gruppe:"

#: cupsdserveridentitypage.cpp:51
msgid "Remote user name:"
msgstr "Eksternt brukernavn:"

#: cupsdserverlogpage.cpp:37
msgid "Log"
msgstr "Logg"

#: cupsdserverlogpage.cpp:38
msgid "Server logging configuration"
msgstr "Innstillinger av tjener-logging"

#: cupsdserverlogpage.cpp:50
msgid "Complete debug (everything)"
msgstr "Fullstendig avlusing (alt)"

#: cupsdserverlogpage.cpp:51
msgid "Partial debug (almost everything)"
msgstr "Delvis avlusing (nesten alt)"

#: cupsdserverlogpage.cpp:52
msgid "Info (normal)"
msgstr "Info (normal)"

#: cupsdserverlogpage.cpp:53
msgid "Warning (errors and warnings)"
msgstr "Advarsler (feil og advarsler)"

#: cupsdserverlogpage.cpp:54
msgid "Error (errors only)"
msgstr "Feil (kun feil)"

#: cupsdserverlogpage.cpp:55 cupslocationgeneral.cpp:43
msgid "None"
msgstr "Ingen"

#: cupsdserverlogpage.cpp:58
msgid "Access log file:"
msgstr "Aksess loggfil:"

#: cupsdserverlogpage.cpp:59
msgid "Error log file:"
msgstr "Feil loggfil:"

#: cupsdserverlogpage.cpp:60
msgid "Page log file:"
msgstr "Side loggfil:"

#: cupsdserverlogpage.cpp:61
msgid "Log level:"
msgstr "Logge-niv�:"

#: cupsdserverlogpage.cpp:62
msgid "Max log file size:"
msgstr "Maksimal logg st�rrelse:"

#: cupsdserverlogpage.cpp:127
msgid "Max log size: wrong argument"
msgstr "Maksimal log st�rrelse: feil argument"

#: cupsdservermiscpage.cpp:39
msgid "Misc"
msgstr "Forskjellig"

#: cupsdservermiscpage.cpp:40
msgid "Server miscellaneous configuration"
msgstr "Forskjellige tjener parametere"

#: cupsdservermiscpage.cpp:45
msgid "Preserve job history (after completion)"
msgstr "Ta vare p� jobbhistorie (etter fullf�ring)"

#: cupsdservermiscpage.cpp:46
msgid "Preserve job file (after completion)"
msgstr "Ta vare p� jobbfil (etter fullf�ring)"

#: cupsdservermiscpage.cpp:55
msgid "Printcap file:"
msgstr "Printcap-fil:"

#: cupsdservermiscpage.cpp:56
msgid "RIP cache:"
msgstr "RIP hurtigbuffer:"

#: cupsdservermiscpage.cpp:57 cupsdservermiscpage.cpp:122
msgid "Filter limit:"
msgstr "Filtergrense:"

#: cupsdbrowsingconnpage.cpp:109 cupsdnetworkclientspage.cpp:97
#: cupsdnetworkclientspage.cpp:107 cupsdnetworkgeneralpage.cpp:109
#: cupsdnetworkgeneralpage.cpp:121 cupsdnetworkgeneralpage.cpp:131
#: cupsdservermiscpage.cpp:122
msgid "%1 wrong argument"
msgstr "%1 feil argument"

#: cupsdbrowsingconnpage.cpp:36 cupsdbrowsinggeneralpage.cpp:34
#: cupsdbrowsingmaskspage.cpp:35 cupsdbrowsingrelaypage.cpp:34
#: cupsdbrowsingtimeoutpage.cpp:34
msgid "Browsing"
msgstr "Utforske"

#: cupsdbrowsinggeneralpage.cpp:36
msgid "Browsing general configuration"
msgstr "Generelle utforskningsinnstillinger"

#: cupsdbrowsinggeneralpage.cpp:41
msgid "Enable browsing"
msgstr "Muliggj�r utforsker"

#: cupsdbrowsinggeneralpage.cpp:42
msgid "Use short names when possible"
msgstr "Bruk korte navn n�r mulig"

#: cupsdbrowsinggeneralpage.cpp:43
msgid "Use implicit classes"
msgstr "Bruk implisitte klasser"

#: cupsdbrowsingtimeoutpage.cpp:35
msgid "Timeouts"
msgstr "Tidsavbrudd"

#: cupsdbrowsingtimeoutpage.cpp:36
msgid "Browsing timeouts configuration"
msgstr "Tidsavbrudd for utforskning"

#: cupsdbrowsingtimeoutpage.cpp:44 cupsdbrowsingtimeoutpage.cpp:87
msgid "Browse interval:"
msgstr "Utforskningsintervall:"

#: cupsdbrowsingtimeoutpage.cpp:45 cupsdbrowsingtimeoutpage.cpp:97
msgid "Browse timeout:"
msgstr "Tidsavbrudd for utforskning:"

#: cupsdbrowsingtimeoutpage.cpp:87 cupsdbrowsingtimeoutpage.cpp:97
msgid "%1 wrong argument !"
msgstr "%1 feil argument!"

#: cupsdbrowsingtimeoutpage.cpp:103
msgid "Browse timeout value must be greater than browse interval"
msgstr "Utforsknings-tidsavbrudd m� v�re st�rre enn utforskningsintervall"

#: qinputbox.cpp:133
msgid "Input value:"
msgstr "Innverdi:"

#: cupslist.cpp:50
msgid "Enter new value:"
msgstr "Skriv inn ny verdi:"

#: cupsdserversecuritypage.cpp:70 cupslist.cpp:95
msgid "Add..."
msgstr "Legg til..."

#: cupsdsplash.cpp:34
msgid "Welcome to the CUPS server configuration tool"
msgstr "Velkommen til CUPS verkt�yet for tjenerinnstillinger"

#: cupsdsplash.cpp:50
msgid "<p>This tool will help you to configure graphically the server of the CUPS printing system. The available options are classified in a hierarchical way and can be accessed quickly through the tree view located on the left. If this tree view is not visible, simply click (or double-click) on the first item in the view.</p><br><p>Each option has a default value. If you want the server to use this value, simply check the box located on the right side of the corresponding option.</p>"
msgstr ""
"<p>Dette verkt�yet hjelper deg til � konfigurere CUPS printersystemet "
"grafisk. De mulige valgene er klassifisert p� en hierakisk m�te og kan "
"aksesseres p� en hurtig m�te gjennom treet til venstre. Dersom treet ikke er "
"synlig, klikk (eller dobbeltklikk) p� det f�rste elementet.</p><br><p>Hvert "
"element har en standard verdi. Dersom du vil bruke denne verdien for "
"tjeneren - klikk boksen til h�yre for valget.</p>"

#: cupsdbrowsingmaskspage.cpp:36
msgid "Masks"
msgstr "Masker"

#: cupsdbrowsingmaskspage.cpp:37
msgid "Browsing masks configuration"
msgstr "Maksimal utforsknings innstilling"

#: cupsdbrowsingmaskspage.cpp:45 cupslocationaccess.cpp:37
msgid "Allow, Deny"
msgstr "Tillat, Nekt"

#: cupsdbrowsingmaskspage.cpp:46 cupslocationaccess.cpp:38
msgid "Deny, Allow"
msgstr "Nekt, Tillat"

#: cupsdbrowsingmaskspage.cpp:48
msgid "Browse allow:"
msgstr "Tillat utforskning:"

#: cupsdbrowsingmaskspage.cpp:49
msgid "Browse deny:"
msgstr "Ikke tillat utforskning:"

#: cupsdbrowsingmaskspage.cpp:50
msgid "Browse order:"
msgstr "Utforsker rekkef�lge:"

#: cupsdbrowsingconnpage.cpp:37
msgid "Connection"
msgstr "Forbindelse"

#: cupsdbrowsingconnpage.cpp:38
msgid "Browsing connection configuration"
msgstr "Bla i gjennom oppsett av oppkobling"

#: cupsdbrowsingconnpage.cpp:49
msgid "Broadcast addresses:"
msgstr "Kringkastningsadresser:"

#: cupsdbrowsingconnpage.cpp:50
msgid "Broadcast port:"
msgstr "Kringkastningsport:"

#: cupsdbrowsingconnpage.cpp:51
msgid "Poll addresses:"
msgstr "?Pulje adresser?"

#: cupsdbrowsingconnpage.cpp:109
msgid "Browse port:"
msgstr "Bla igjennom port:"

#: cupsdnetworkclientspage.cpp:36 cupsdnetworkgeneralpage.cpp:36
msgid "Network"
msgstr "Nettverk"

#: cupsdnetworkgeneralpage.cpp:37
msgid "Network general configuration"
msgstr "Generelle nettverks-innstillinger"

#: cupsdnetworkgeneralpage.cpp:43
msgid "Look for hostname on IP addresses"
msgstr "Sl� opp navn p� IP-adresser"

#: cupsdnetworkgeneralpage.cpp:47 cupsdnetworkgeneralpage.cpp:109
msgid "Port:"
msgstr "Port:"

#: cupsdnetworkgeneralpage.cpp:48 cupsdnetworkgeneralpage.cpp:121
msgid "Max request size:"
msgstr "Maksimal st�rrelse p� foresp�rsel:"

#: cupsdnetworkgeneralpage.cpp:49 cupsdnetworkgeneralpage.cpp:131
msgid "Timeout:"
msgstr "Tidsavbrudd:"

#: cupsrelay.cpp:27 cupsrelay.cpp:29
msgid "From"
msgstr "Fra"

#: cupsrelay.cpp:28 cupsrelay.cpp:30
msgid "To"
msgstr "Til"

#: cupsdbrowsingrelaypage.cpp:35
msgid "Relay"
msgstr "Over"

#: cupsdbrowsingrelaypage.cpp:36
msgid "Browsing relay configuration"
msgstr "Bla i gjennom oppsett av over"

#: cupsdbrowsingrelaypage.cpp:45
msgid "Browser packets relay:"
msgstr "Bla i gjennom pakker over:"

#: cupsdnetworkclientspage.cpp:37
msgid "Clients"
msgstr "Klienter"

#: cupsdnetworkclientspage.cpp:38
msgid "Network clients configuration"
msgstr "Innstilling av nettverksklienter"

#: cupsdnetworkclientspage.cpp:43
msgid "Accept \"Keep Alive\" requests"
msgstr "Tillat \"Keep Alive\" foresp�rsler"

#: cupsdnetworkclientspage.cpp:47 cupsdnetworkclientspage.cpp:97
msgid "Keep alive timeout:"
msgstr "Hold i livet tidsavbrudd:"

#: cupsdnetworkclientspage.cpp:48 cupsdnetworkclientspage.cpp:107
msgid "Max number of clients:"
msgstr "Maksimalt antall klienter:"

#: cupsdserversecuritypage.cpp:41
msgid "Security"
msgstr "Sikkerhet"

#: cupsdserversecuritypage.cpp:42
msgid "Security configuration"
msgstr "Innstilling av sikkerhet"

#: cupsdserversecuritypage.cpp:51
msgid "System group:"
msgstr "Systemgruppe:"

#: cupsdserversecuritypage.cpp:64
msgid "Resource"
msgstr "Ressurs"

#: cupsdserversecuritypage.cpp:65
msgid "Path"
msgstr "Sti"

#: cupsdserversecuritypage.cpp:68
msgid "Resources:"
msgstr "Ressurser:"

#: cupsdserversecuritypage.cpp:99
msgid "<Unmatched resource>"
msgstr "<Ukjent ressurs>"

#: cupsdserversecuritypage.cpp:173
msgid "Really remove resource \"%1\" ?"
msgstr "Virkelig fjerne ressurs \"%1\" ?"

#: cupslocationdialog.cpp:36
msgid "General"
msgstr "Generelt"

#: cupslocationdialog.cpp:40
msgid "Access"
msgstr "Tilgang"

#: cupslocationdialog.cpp:46 mydialogbase.cpp:95
msgid "Short help..."
msgstr "Kort hjelp..."

#: cupslocationdialog.cpp:49
msgid "Add resource"
msgstr "Legg til ressurs"

#: cupslocationdialog.cpp:61
msgid "Resource \"%1\""
msgstr "Ressurs \"%1\""

#: cupslocationdialog.cpp:67 cupslocationdialog.cpp:81
msgid "You must specify a resource name !"
msgstr "Du m� angi et navn for ressursen!"

#: cupslocationgeneral.cpp:44
msgid "Basic"
msgstr "Grunnleggende"

#: cupslocationgeneral.cpp:45
msgid "Digest"
msgstr "Sammendrag"

#: cupslocationgeneral.cpp:49
msgid "Anonymous"
msgstr "Anonym"

#: cupslocationgeneral.cpp:50
msgid "User"
msgstr "Bruker"

#: cupslocationgeneral.cpp:51
msgid "System"
msgstr "System"

#: cupslocationgeneral.cpp:52
msgid "Group"
msgstr "Gruppe"

#: cupslocationgeneral.cpp:59
msgid "Always"
msgstr "Alltid"

#: cupslocationgeneral.cpp:60
msgid "Never"
msgstr "Aldri"

#: cupslocationgeneral.cpp:61
msgid "Required"
msgstr "P�krevd"

#: cupslocationgeneral.cpp:62
msgid "If requested"
msgstr "Om foresp�rt"

#: cupslocationgeneral.cpp:65
msgid "Resource:"
msgstr "Ressurs:"

#: cupslocationgeneral.cpp:66
msgid "Authorization type:"
msgstr "Autoriseringstype:"

#: cupslocationgeneral.cpp:67
msgid "Authorization class:"
msgstr "Autoriserings-klasse:"

#: cupslocationgeneral.cpp:68
msgid "Authorization group:"
msgstr "Autoriserings-gruppe:"

#: cupslocationgeneral.cpp:69
msgid "Encryption type:"
msgstr "Krypterings-type:"

#: cupslocationaccess.cpp:41
msgid "Allow from:"
msgstr "Tillat fra:"

#: cupslocationaccess.cpp:42
msgid "Deny from:"
msgstr "Nekt fra:"

#: cupslocationaccess.cpp:43
msgid "Order:"
msgstr "Rekkef�lge:"

#: cupsdserverencryptpage.cpp:35
msgid "Encryption"
msgstr "Kryptering"

#: cupsdserverencryptpage.cpp:36
msgid "Server encryption support configuration"
msgstr "Innstillinger av tjener-kryptering:"

#: cupsdserverencryptpage.cpp:44
msgid "Server certificate:"
msgstr "Tjener sertifikat:"

#: cupsdserverencryptpage.cpp:45
msgid "Server key:"
msgstr "Tjener-n�kkel:"

#: main.cpp:29
msgid "Configuration file to load"
msgstr "Oppsettfil � laste"

#: main.cpp:35
msgid "A CUPS configuration tool"
msgstr "Et CUPS konfigurasjons-verkt�y"
