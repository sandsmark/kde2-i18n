# Norwegina trnslations of koffice/koshell
# Copyright (C) 2000 Hans Petter Bieker.
# Hans Petter Bieker <bieker@kde.org>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: KDE KOFFICE/koshell\n"
"POT-Creation-Date: 2001-06-05 02:55+0200\n"
"PO-Revision-Date: 2001-07-28 15:12+GMT+1\n"
"Last-Translator: Rune Nordvik <rune@linuxnorge.com>\n"
"Language-Team: bokmål\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"<rune@linuxnorge.com>\n"
"X-Generator: KBabel 0.8\n"

#: koshell_main.cc:28 koshell_main.cc:33
msgid "KOffice Workspace"
msgstr "KOffice Arbeidsområde"

#: koshell_shell.cc:57
msgid "Parts"
msgstr "Deler"

#: koshell_shell.cc:84
msgid "Documents"
msgstr "Dokumenter"

#: koshell_shell.cc:115
msgid "KOffice Documents"
msgstr "KOffice-dokumenter"

#: koshell_shell.cc:203
msgid "No name"
msgstr "Intet navn"

#: koshell_shell.cc:342
msgid "Open document"
msgstr "Åpne dokument"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Rune Nordvik"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "rune@linuxnorge.com"
