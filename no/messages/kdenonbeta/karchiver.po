# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-08-17 10:28+0200\n"
"PO-Revision-Date: 2001-12-04 12:23-0000\n"
"Last-Translator: Andreas D. Landmark <andreas.landmark@skolelinux.no>\n"
"Language-Team: no <i18n-nb@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: caddfiles.cpp:48
msgid "targz archives"
msgstr "targz arkiver"

#: caddfiles.cpp:50
msgid "tarbz2 archives"
msgstr "tarbz2 arkiver"

#: caddfiles.cpp:52
#: cchoixfichier.cpp:29
msgid "lha archives"
msgstr "lha arkiver"

#: caddfiles.cpp:54
#: cchoixfichier.cpp:31
msgid "arj archives"
msgstr "arj arkiver"

#: caddfiles.cpp:56
#: cchoixfichier.cpp:33
msgid "rar archives"
msgstr "rar arkiver"

#: caddfiles.cpp:58
msgid "pure gzip archives"
msgstr "rene gzip arkiver"

#: caddfiles.cpp:60
msgid "pure bzip2 archives"
msgstr "rene bzip2 arkiver"

#: caddfiles.cpp:62
#: cchoixfichier.cpp:39
msgid "zip archives"
msgstr "zip arkiver"

#: caddfiles.cpp:64
msgid "pure tar archives"
msgstr "rene tar arkiver"

#: caddfilesdata.cpp:19
msgid "Would you like to:"
msgstr "Vil du:"

#: caddfilesdata.cpp:24
msgid "Add those files to the current archive"
msgstr "Legge til filene i det gjeldende arkivet"

#: caddfilesdata.cpp:30
msgid "Add those files to this archive:"
msgstr "Legge til filene i dette arkivet"

#: caddfilesdata.cpp:39
#: caddfilesdata.cpp:50
msgid "Browse"
msgstr "Bla gjennom"

#: caddfilesdata.cpp:44
msgid ""
"The file extension will\n"
" choose the compressor"
msgstr ""
"Filutvidelsen vil\n"
" velge komprimeringstype"

#: caddfilesdata.cpp:45
msgid "Create an archive stored as:"
msgstr "Lag et arkiv lagret som:"

#: caddfilesdata.cpp:60
msgid "This file is an archive. Open it !"
msgstr "Denne fila er et arkiv. Åpne den!"

#: cajoutfichiers.cpp:69
msgid "Choose files:"
msgstr "Velg filer:"

#: cajoutfichiers.cpp:90
msgid "Kind of operation:"
msgstr "Type handling:"

#: cajoutfichiers.cpp:103
#: karchiveur.cpp:2331
msgid "Add files"
msgstr "Legg til filer"

#: cajoutfichiers.cpp:115
msgid "Update files"
msgstr "Oppdater filer"

#: cajoutfichiers.cpp:123
msgid "Use wilcards"
msgstr "Bruk jokere"

#: cajoutfichiers.cpp:132
msgid "Delete source files"
msgstr "Slett kildefiler"

#: carchive.cpp:57
#: cinfosdata.cpp:58
#: cinfosdata.cpp:100
#: karchiveur.cpp:1463
msgid "bytes"
msgstr "byte"

#: carchive.cpp:149
#: carj.cpp:171
#: cbz2.cpp:95
#: cgz.cpp:94
#: clha.cpp:166
#: crar.cpp:202
#: ctar.cpp:187
#: czip.cpp:202
#: karchiveur.cpp:1092
msgid "Running compressor..."
msgstr "Kjør komprimering..."

#: carj.cpp:30
#: clha.cpp:30
#: crar.cpp:31
#: ctar.cpp:29
#: karchiveurpart.cpp:267
#: karchiveurview.cpp:37
msgid "Owner"
msgstr "Eier"

#: carj.cpp:31
#: clha.cpp:31
#: crar.cpp:32
#: ctar.cpp:30
#: karchiveurpart.cpp:268
#: karchiveurview.cpp:38
msgid "Permissions"
msgstr "Rettigheter"

#: cchoixfichier.cpp:27
msgid "All valid archives"
msgstr "Alle gyldige arkiver"

#: cchoixfichier.cpp:35
msgid "gzip archives"
msgstr "gzip arkiver"

#: cchoixfichier.cpp:37
msgid "bzip2 archives"
msgstr "bzip2 arkiver"

#: cchoixfichier.cpp:41
msgid "tar archives"
msgstr "tar arkiver"

#: cextraction.cpp:41
msgid "Extract:"
msgstr "Pakk ut:"

#: cextraction.cpp:64
msgid "Where to extract:"
msgstr "Pakk ut i:"

#: cextraction.cpp:82
msgid "Extract To:"
msgstr "Pakk ut til:"

#: cextraction.cpp:103
msgid "All files"
msgstr "Alle filer"

#: cextraction.cpp:114
msgid "Selected files"
msgstr "Valgte filer"

#: cextraction.cpp:120
msgid "Extract"
msgstr "Pakk ut"

#: cfinddata.cpp:15
msgid "Find"
msgstr "Finn"

#: cfinddata.cpp:20
msgid "Find in this archive:"
msgstr "Finn i dette arkivet:"

#: cfinddata.cpp:28
msgid "Next"
msgstr "Neste"

#: cfinddata.cpp:29
msgid "Done"
msgstr "Ferdig"

#: cinfos.cpp:37
msgid "Archive information"
msgstr "Arkivinformasjon"

#: cinfosdata.cpp:20
msgid "In this archive, there are:"
msgstr "I dette arkivet er det:"

#: cinfosdata.cpp:29
msgid "file(s)"
msgstr "fil(er)"

#: cinfosdata.cpp:35
msgid "They will occupy"
msgstr "De vil oppta"

#: cinfosdata.cpp:44
#: cproprietesdata.cpp:64
msgid "bytes on disk"
msgstr "bytes på disken"

#: cinfosdata.cpp:49
msgid "Archive size is"
msgstr "Arkivstørrelsen er"

#: cinfosdata.cpp:63
msgid "The compress rate is :"
msgstr "Komprimeringsgraden er:"

#: cinfosdata.cpp:72
msgid "Medium file size is:"
msgstr "Medium filstørrelse er:"

#: cinfosdata.cpp:81
#, fuzzy
msgid "the scattering"
msgstr "spredningen"

#: cinfosdata.cpp:82
msgid "Standard deviation"
msgstr "Standardavvik"

#: cinfosdata.cpp:95
#, c-format
msgid "%"
msgstr "%"

#: cinfosdata.cpp:109
#: cpreferencesdata.cpp:170
#: cproprietesdata.cpp:69
msgid "Ok"
msgstr "Ok"

#: cnavigateur.cpp:30
msgid "Archive browser"
msgstr "Arkivleser"

#: cnavigateurdata.cpp:19
msgid "Path:"
msgstr "Sti:"

#: cnavigateurdata.cpp:28
msgid "Double click to load archive !"
msgstr "Dobbelklikk for å laste arkivet"

#: cpreferences.cpp:293
#: cpreferences.cpp:295
#: cpreferences.cpp:297
#: cpreferences.cpp:299
#: cpreferences.cpp:301
#: cpreferences.cpp:303
#: cpreferences.cpp:305
#: cpreferences.cpp:307
msgid "is NOT avaible"
msgstr "er IKKE tilgjengelig"

#: cpreferences.cpp:321
msgid "Gzip"
msgstr "Gzip"

#: cpreferences.cpp:321
#: cpreferencesdata.cpp:22
msgid "Gzip and Bzip2 options"
msgstr "Gzip of Bzip2 muligheter"

#: cpreferences.cpp:329
#: cpreferencesdata.cpp:27
msgid "Compress rate"
msgstr "Komprimeringsgrad"

#: cpreferences.cpp:332
#: cpreferencesdata.cpp:45
msgid "9 = max"
msgstr "9 = max"

#: cpreferences.cpp:335
#: cpreferencesdata.cpp:50
msgid "1 = min"
msgstr "1 = min"

#: cpreferences.cpp:339
#: cpreferencesdata.cpp:40
msgid "1"
msgstr "1"

#: cpreferences.cpp:340
#: cpreferencesdata.cpp:39
msgid "2"
msgstr "2"

#: cpreferences.cpp:341
#: cpreferencesdata.cpp:38
msgid "3"
msgstr "3"

#: cpreferences.cpp:342
#: cpreferencesdata.cpp:37
msgid "4"
msgstr "4"

#: cpreferences.cpp:343
#: cpreferencesdata.cpp:36
msgid "5"
msgstr "5"

#: cpreferences.cpp:344
#: cpreferencesdata.cpp:35
msgid "6"
msgstr "6"

#: cpreferences.cpp:345
#: cpreferencesdata.cpp:34
msgid "7"
msgstr "7"

#: cpreferences.cpp:346
#: cpreferencesdata.cpp:33
msgid "8"
msgstr "8"

#: cpreferences.cpp:347
#: cpreferencesdata.cpp:32
msgid "9"
msgstr "9"

#: cpreferences.cpp:351
#: cpreferences.cpp:382
#: cpreferencesdata.cpp:55
#: cpreferencesdata.cpp:84
msgid "Overwrite files when extracting"
msgstr "Overskriv filer ved utpakking"

#: cpreferences.cpp:360
msgid "Tar"
msgstr "Tar"

#: cpreferences.cpp:360
#: cpreferencesdata.cpp:62
msgid "Tar options"
msgstr "Tar muligheter"

#: cpreferences.cpp:368
#: cpreferencesdata.cpp:67
msgid ""
"Store files as /home/toto/files\n"
"instead of home/toto/files"
msgstr ""
"Lagre filer som /home/toto/filer\n"
"istedenfor home/toto/filer"

#: cpreferences.cpp:369
#: cpreferencesdata.cpp:68
msgid "Ablolute path: add / to the paths of files"
msgstr "Absolutt-sti: legg til / til banen på filene"

#: cpreferences.cpp:373
#: cpreferencesdata.cpp:73
#, fuzzy
msgid ""
"If checked, the base path of the dropped dir will be\n"
" home/toto/dropped_dir instead of drop_dir"
msgstr ""
"Hvis markert, så vil stien til den sluppne katalogen bli\n"
" /home/brukernavn/sluppet_mappet istedet for sluppet_mappe"

#: cpreferences.cpp:374
#: cpreferencesdata.cpp:74
msgid "Add the base path when dropping"
msgstr "Legg til full sti ved slipp"

#: cpreferences.cpp:378
#: cpreferencesdata.cpp:79
msgid "Recurse folders when dropping"
msgstr "Rekursive kataloger på slipp"

#: cpreferences.cpp:391
msgid "Directories"
msgstr "Kataloger"

#: cpreferences.cpp:391
#: cpreferencesdata.cpp:98
msgid "Default directories"
msgstr "Standardkataloger"

#: cpreferences.cpp:397
#: cpreferencesdata.cpp:103
msgid "For extracting"
msgstr "For utpakking"

#: cpreferences.cpp:403
#: cpreferences.cpp:421
#: cpreferencesdata.cpp:108
#: cpreferencesdata.cpp:128
msgid "Last dir"
msgstr "Siste katalog"

#: cpreferences.cpp:407
#: cpreferences.cpp:425
#: cpreferencesdata.cpp:113
#: cpreferencesdata.cpp:133
msgid "Home dir (~/)"
msgstr "Hjemmekatalog (~/)"

#: cpreferences.cpp:411
#: cpreferences.cpp:429
#: cpreferencesdata.cpp:118
#: cpreferencesdata.cpp:138
msgid "Shell's one"
msgstr "Skallets"

#: cpreferences.cpp:415
#: cpreferencesdata.cpp:123
msgid "For opening"
msgstr "For å åpne"

#: cpreferences.cpp:436
msgid "Icons"
msgstr "Ikoner"

#: cpreferences.cpp:436
#: cpreferencesdata.cpp:145
msgid "Icon size"
msgstr "Ikonstørrelse"

#: cpreferences.cpp:442
#: cpreferencesdata.cpp:150
msgid "of files in archive"
msgstr "filer i arkivet"

#: cpreferences.cpp:449
#: cpreferencesdata.cpp:155
msgid "Small"
msgstr "Liten"

#: cpreferences.cpp:453
#: cpreferencesdata.cpp:160
msgid "Medium"
msgstr "Medium"

#: cpreferences.cpp:457
#: cpreferencesdata.cpp:165
msgid "Large"
msgstr "Stor"

#: cpreferences.cpp:464
msgid "Reading"
msgstr "Leser"

#: cpreferences.cpp:464
msgid "Method for reading archive"
msgstr "Metode for å lese arkiv"

#: cpreferences.cpp:470
msgid "Read:"
msgstr "Les:"

#: cpreferences.cpp:476
msgid "Fastest but don't use icons"
msgstr "Raskeste men ikke bruk ikoner"

#: cpreferences.cpp:480
msgid "Faster but freeze kar's interface"
msgstr "Raskere men frys kars grensesnitt"

#: cpreferences.cpp:484
msgid "Normaly, but the reading can be interupted"
msgstr "Normal, men lesing kan bli forstyrret"

#: cpreferences.cpp:491
#, fuzzy
msgid "Compressors"
msgstr "Komprimeringsverktøy"

#: cpreferences.cpp:491
#: cpreferencesdata.cpp:205
msgid "Avaible compressors"
msgstr "Tilgjengelige komprimeringsverktøy"

#: cpreferences.cpp:497
#: cpreferences.cpp:509
#: cpreferences.cpp:529
#: cpreferences.cpp:533
#: cpreferences.cpp:537
#: cpreferences.cpp:541
#: cpreferences.cpp:549
#: cpreferences.cpp:557
#: cpreferencesdata.cpp:210
#: cpreferencesdata.cpp:225
#: cpreferencesdata.cpp:250
#: cpreferencesdata.cpp:255
#: cpreferencesdata.cpp:260
#: cpreferencesdata.cpp:265
#: cpreferencesdata.cpp:275
#: cpreferencesdata.cpp:285
msgid "is avaible"
msgstr "er tilgjengelige"

#: cpreferences.cpp:501
#: cpreferencesdata.cpp:215
msgid "The Zip compressor..."
msgstr "Zip pakker..."

#: cpreferences.cpp:505
#: cpreferencesdata.cpp:220
msgid "The TAR compressor..."
msgstr "Tar pakker..."

#: cpreferences.cpp:513
#: cpreferencesdata.cpp:230
msgid "The GZIP compressor..."
msgstr "GZIP pakker..."

#: cpreferences.cpp:517
#: cpreferencesdata.cpp:235
msgid "The BZIP2 compressor..."
msgstr "BZIP2 pakker..."

#: cpreferences.cpp:521
#: cpreferencesdata.cpp:240
msgid "The RAR compressor..."
msgstr "RAR pakker..."

#: cpreferences.cpp:525
#: cpreferencesdata.cpp:245
msgid "The LHA compressor..."
msgstr "LHA pakker..."

#: cpreferences.cpp:545
#: cpreferencesdata.cpp:270
msgid "The Zip reader..."
msgstr "Zip-leser..."

#: cpreferences.cpp:553
#: cpreferencesdata.cpp:280
msgid "The Arj reader..."
msgstr "Arj-leser..."

#: cpreferences.cpp:603
#, fuzzy
msgid "List font"
msgstr "Skrifttype for lister"

#: cpreferences.cpp:606
msgid "font"
msgstr "font"

#: cpreferences.cpp:608
msgid "karchiveur.cpp\t80Ko 18:00:12"
msgstr "karchiveur.cpp\t80Ko 18:00:12"

#: cpreferences.cpp:619
#: karchiveurpart.cpp:266
#: karchiveurview.cpp:36
msgid "Date"
msgstr "Dato"

#: cpreferences.cpp:619
msgid "Display date"
msgstr "Vis dato"

#: cpreferences.cpp:630
#, fuzzy
msgid "Compressors's one"
msgstr "Komprimeringsverktøyets første"

#: cpreferences.cpp:634
msgid "Localised: 1/1/2000"
msgstr "Lokalisert: 1/1/2000"

#: cpreferences.cpp:638
msgid "Localised: Saturday May 2000"
msgstr "Lokalisert: Lørdag Mai 2000"

#: cpreferences.cpp:659
msgid "Tips"
msgstr "Tips"

#: cpreferences.cpp:659
msgid "Tip of the day"
msgstr "Dagens tips"

#: cpreferences.cpp:667
msgid "Display the tip of the day at startup"
msgstr "Vis dagens tips ved oppstart"

#: cpreferences.cpp:681
msgid "Misc"
msgstr "Annet"

#: cpreferences.cpp:681
msgid "Misc options"
msgstr "Forskjellige valg"

#: cpreferences.cpp:687
#, fuzzy
msgid "Selection of list's items"
msgstr "Valg av listeinnhold"

#: cpreferences.cpp:693
msgid "Click to add to selection"
msgstr "Klikk for å legge til et valg"

#: cpreferences.cpp:697
msgid "Ctrl+click to add to selection"
msgstr "Ctrl+klikk for å legge til et valg"

#: cpreferencesdata.cpp:91
msgid "Default viewer"
msgstr "Standard leser"

#: cpreferencesdata.cpp:187
msgid "Method for reading archives"
msgstr "Metode for å lese arkiv"

#: cpreferencesdata.cpp:192
msgid "Fastest but freeze kar's interface"
msgstr "Raskest men fryser kars grensesnitt"

#: cpreferencesdata.cpp:197
msgid "Slower, but the reading can be interupted"
msgstr "Saktere, men lesinga kan bli forstyrret"

# antar det er det og ikke proprieties, ettersom det ikke er noe spesielt vanlig ord
#: cproprietes.cpp:23
#: karchiveur.cpp:2337
msgid "Proprieties"
msgstr "Egenskaper"

#: cproprietesdata.cpp:19
msgid "The file:"
msgstr "Fila:"

#: cproprietesdata.cpp:25
msgid "Filename"
msgstr "Filnavn"

#: cproprietesdata.cpp:30
msgid "Will be extracted in:"
msgstr "Vil bli pakka ut i:"

#: cproprietesdata.cpp:36
msgid "this/path/"
msgstr "denne/sti/"

#: cproprietesdata.cpp:41
msgid "It has those permissions:"
msgstr "Den har disse tilgangene:"

#: cproprietesdata.cpp:48
msgid "rwxrwx---"
msgstr "rwxrwx---"

#: cproprietesdata.cpp:53
msgid "It will occupy:"
msgstr "Den vil oppta:"

msgid "0"
msgstr "0"

#: crecherche.cpp:41
msgid "Where to search:"
msgstr "Start søket i:"

#: crecherche.cpp:74
msgid "Search in:"
msgstr "Søk i:"

#: crecherche.cpp:99
msgid "Search"
msgstr "Søk"

#: cwizardstep1.cpp:25
msgid "KArchiver wizard step 1"
msgstr "KArchiverhjelper steg 1"

#: cwizardstep1conversion.cpp:26
msgid "Conversion wizard step 2"
msgstr "Konverteringshjelper steg 2"

#: cwizardstep1conversiondata.cpp:27
msgid "Converting archive format"
msgstr "Konvertere arkivformat"

#: cwizardstep1conversiondata.cpp:32
msgid "This wizard will help you to convert"
msgstr "Denne hjelperen vil hjelpe deg til å endre"

#: cwizardstep1conversiondata.cpp:37
msgid "the format of your archive, so as to "
msgstr "formatet på arkivet ditt, for å"

#: cwizardstep1conversiondata.cpp:42
msgid "improve compress rate or portability"
msgstr "øke komprimeringsgraden"

#: cwizardstep1conversiondata.cpp:47
#: cwizardstep1cuttdata.cpp:45
#: cwizardstep1data.cpp:53
#: cwizardstep1installationdata.cpp:68
#: cwizardstep1patchdata.cpp:66
#: cwizardstep2conversiondata.cpp:77
#: cwizardstep2cuttdata.cpp:46
#: cwizardstep2data.cpp:92
#: cwizardstep2installdata.cpp:80
msgid "Next >>"
msgstr "Neste >>"

#: cwizardstep1conversiondata.cpp:52
#: cwizardstep1cuttdata.cpp:50
#: cwizardstep1installationdata.cpp:73
#: cwizardstep1patchdata.cpp:61
#: cwizardstep2conversiondata.cpp:82
#: cwizardstep2cuttdata.cpp:51
#: cwizardstep2data.cpp:97
#: cwizardstep2installdata.cpp:85
msgid "<< Previous"
msgstr "<< Forrige"

#: cwizardstep1cutt.cpp:25
msgid "KArchiver cutt step 2"
msgstr "KArchiver del steg2"

#: cwizardstep1cuttdata.cpp:25
msgid "Split / Unsplit the archive"
msgstr "Del / Legg sammen arkivet"

#: cwizardstep1cuttdata.cpp:30
msgid "This wizard let you to split or unsplit"
msgstr "Dene hjelperen lar deg dele eller legge"

#: cwizardstep1cuttdata.cpp:35
msgid "the archive, so as is can fit into a"
msgstr "sammen arkivet, slik at det kan passe"

#: cwizardstep1cuttdata.cpp:40
msgid "floppy or anything else."
msgstr "på en floppy."

#: cwizardstep1data.cpp:23
msgid "This wizard will help you to make some"
msgstr "Dene hjelperen vil hjelpe deg å gjøre "

#: cwizardstep1data.cpp:28
msgid "useful things with your archive:"
msgstr "noen nyttige ting med arkivet ditt:"

#: cwizardstep1data.cpp:33
msgid "* extract current file and use it to patch"
msgstr "* pakk ut gjeldende fil og bruk den for å patche"

#: cwizardstep1data.cpp:38
msgid "a directory (Linux kernel ...)"
msgstr "en katalog (Linux kjerne ...)"

#: cwizardstep1data.cpp:43
msgid "* install the archive: extract all files, run"
msgstr "* innstaller arkivet: pakk ut alle filene, kjør"

#: cwizardstep1data.cpp:48
msgid "configure, make, make install (as root)"
msgstr "konfigurer, lag, lag install (som rot)"

#: cwizardstep1data.cpp:63
msgid "Time to choose !"
msgstr "På tide å velge!"

#: cwizardstep1data.cpp:68
#: cwizardstep1patchdata.cpp:31
msgid "Apply a patch"
msgstr "Påfør en patch"

#: cwizardstep1data.cpp:73
msgid "Install the archive"
msgstr "Innstaller arkivet"

#: cwizardstep1data.cpp:79
msgid "Convert archive format"
msgstr "Konverter arkivformat"

#: cwizardstep1data.cpp:84
msgid "Split archive"
msgstr "Del opp arkivet"

#: cwizardstep1installationdata.cpp:22
#: cwizardstep1patchdata.cpp:20
msgid "Installation wizard step 2"
msgstr "Innstallasjonshjelper steg 2"

#: cwizardstep1installationdata.cpp:33
msgid "Install a software"
msgstr "Innstaller softvare"

#: cwizardstep1installationdata.cpp:38
msgid "Most GNU software have to be"
msgstr "Det meste av GNU software må"

#: cwizardstep1installationdata.cpp:43
msgid "compiled before you can use"
msgstr "kompileres før du kan bruke"

#: cwizardstep1installationdata.cpp:48
msgid "them. Then, the installation is"
msgstr "det. Innstalleringa gjøres"

#: cwizardstep1installationdata.cpp:53
msgid "done using 'root' privilege,"
msgstr "med rot-privilegier,"

#: cwizardstep1installationdata.cpp:58
msgid "but you must be sure what you"
msgstr "du må være helt sikker "

#: cwizardstep1installationdata.cpp:63
msgid "are doing !"
msgstr "på hva du gjør!"

#: cwizardstep1patchdata.cpp:36
msgid "   A patch is a text file that apply some"
msgstr "   En patch er en tekst fil som påfører noen"

#: cwizardstep1patchdata.cpp:41
msgid "modifications to files (ex: the patches"
msgstr "modifikasjoner til filer (f. eks. patchene til"

#: cwizardstep1patchdata.cpp:46
msgid "of the Linux kernel). We will promt you"
msgstr "Linux-kjernen). Vi spørr etter"

#: cwizardstep1patchdata.cpp:51
msgid "to select the directory containing the"
msgstr "katalogen som inneholder"

#: cwizardstep1patchdata.cpp:56
msgid "software to be patched (ex: ~/karchiveur)"
msgstr "programvaren som skal patches (f. eks. ~/karchiveur)"

#: cwizardstep2.cpp:26
msgid "Installation wizard step 3"
msgstr "Innstalleringshjelper steg 3"

#: cwizardstep2conversion.cpp:26
msgid "Conversion wizard step 3"
msgstr "Konverteringshjelper steg 3"

#: cwizardstep2conversion.cpp:57
msgid "Choose the directory where to store archive:"
msgstr "Velg katalog for å lagre arkivet:"

#: cwizardstep2conversiondata.cpp:26
msgid "Convert to format:"
msgstr "Konverter til format:"

#: cwizardstep2conversiondata.cpp:31
msgid ".tar"
msgstr ".tar"

#: cwizardstep2conversiondata.cpp:37
msgid ".tar.gz"
msgstr ".tar.gz"

#: cwizardstep2conversiondata.cpp:43
msgid ".tar.bz2"
msgstr ".tar.bz2"

#: cwizardstep2conversiondata.cpp:50
msgid ".bz2"
msgstr ".bz2"

#: cwizardstep2conversiondata.cpp:56
msgid ".gz"
msgstr ".gz"

#: cwizardstep2conversiondata.cpp:62
msgid ".zip"
msgstr ".zip"

#: cwizardstep2conversiondata.cpp:67
msgid ".rar"
msgstr ".rar"

#: cwizardstep2conversiondata.cpp:72
msgid ".lha"
msgstr ".lha"

#: cwizardstep2conversiondata.cpp:87
msgid "Next step: please choose a valid"
msgstr "Neste steg: velg en gyldig"

#: cwizardstep2conversiondata.cpp:92
msgid "directory"
msgstr "katalog"

#: cwizardstep2cutt.cpp:25
msgid "KArchiver cutt step 3"
msgstr "Karchiver del steg 3"

#: cwizardstep2cutt.cpp:44
msgid "Next Step: please choose where"
msgstr "Neste steg: velg hvor"

#: cwizardstep2cutt.cpp:45
msgid "to store the splited files"
msgstr "de delte filene skal lagres"

#: cwizardstep2cutt.cpp:49
msgid "Next Step: please choose the first"
msgstr "Neste steg: velg hvor den første"

#: cwizardstep2cutt.cpp:50
msgid "splited file (ends with .01)"
msgstr "delen av fila (ender med .01)"

#: cwizardstep2cutt.cpp:59
msgid "Where to store splited files ?"
msgstr "Hvor skal fildelene lagres?"

#: cwizardstep2cuttdata.cpp:25
msgid "Split / Unsplit an archive"
msgstr "Del / legg sammen et arkiv"

#: cwizardstep2cuttdata.cpp:30
msgid "I want to"
msgstr "Jeg vil"

#: cwizardstep2cuttdata.cpp:35
msgid "Split my current archive"
msgstr "Dele gjeldende arkiv"

#: cwizardstep2cuttdata.cpp:41
msgid "Unsplit and open an archive"
msgstr "Legge sammen og åpne et arkiv"

#: cwizardstep2data.cpp:25
msgid "First run:"
msgstr "Kjør først:"

#: cwizardstep2data.cpp:30
msgid "configure"
msgstr "konfigurer"

#: cwizardstep2data.cpp:35
msgid "With the following arguments:"
msgstr "Med følgende argumenter"

#: cwizardstep2data.cpp:45
msgid "Then"
msgstr "Så"

#: cwizardstep2data.cpp:50
msgid "make"
msgstr "make"

#: cwizardstep2data.cpp:55
msgid "Installation"
msgstr "Installasjon"

#: cwizardstep2data.cpp:60
msgid "I will install"
msgstr "Jeg vil innstallere"

#: cwizardstep2data.cpp:65
msgid "As root"
msgstr "Som rot"

#: cwizardstep2data.cpp:71
msgid "As current user"
msgstr "Som gkjeldende bruker"

#: cwizardstep2data.cpp:76
msgid "Don't install"
msgstr "Ike innstaller"

#: cwizardstep2data.cpp:82
msgid "install"
msgstr "installer"

#: cwizardstep2data.cpp:87
msgid "and I will run make"
msgstr "og jeg vil kjøre make"

#: cwizardstep2installdata.cpp:20
msgid "Installation wizard step 4"
msgstr "Inntalleringshjelper steg 4"

#: cwizardstep2installdata.cpp:30
msgid "Installing software"
msgstr "Innstallere software"

#: cwizardstep2installdata.cpp:35
msgid "You are now ready to start install."
msgstr "Du er nå klar til å innstallere"

#: cwizardstep2installdata.cpp:40
msgid "Take a look at the output to ensure"
msgstr "Se på skjerm-meldingene for å forsikre"

#: cwizardstep2installdata.cpp:45
msgid "everything is OK. Don't forget you must"
msgstr "deg om at alt er OK. Ikke glem at du må"

#: cwizardstep2installdata.cpp:50
msgid "be 'root' to install the software."
msgstr "være 'rot' for å innstallere softvare."

#: cwizardstep2installdata.cpp:55
#, fuzzy
msgid "Perhaps you wish to compile only"
msgstr "Kanskje du bare ønsker å kompilere"

#: cwizardstep2installdata.cpp:60
msgid "throught this wizard, and before quiting"
msgstr "gjennom denne veilederen, og før du"

#: cwizardstep2installdata.cpp:65
msgid "it, go back to the shell, log in as root"
msgstr "går ut av den, gå tilbake i skallet, logg inn som root"

#: cwizardstep2installdata.cpp:70
msgid "and 'make install' in /tmp/karchiveurtmp.???"
msgstr "og «make install» i /tmp/karchiveurtmp.???"

#: cwizardstep3.cpp:25
msgid "KArchiver wizard's last step"
msgstr "KArchiverhjelperens siste steg"

#: cwizardstep3.cpp:99
msgid ""
"Compilation is now achieved\n"
"Check the output NOW for errors\n"
"Do you still want to install as root?"
msgstr ""
"Kompileringen er gjennomført\n"
"Sjekk for feil i skjerm-meldingene NÅ\n"
"Vi du fortsatt innstallere som rot?"

#: cwizardstep3.cpp:142
#, fuzzy
msgid "Choose the directory to patch"
msgstr "Velg katalogen som skal patches"

#: cwizardstep3data.cpp:22
msgid "KDE2's wizard"
msgstr "KDE2s hjelper"

#: cwizardstep3data.cpp:27
msgid "Output of running command :"
msgstr "Melding fra kommando"

#: cwizardstep3data.cpp:32
msgid "You can see here the output of currently running command"
msgstr "Her kan du se meldinger fra den aktive kommandoen"

#: cwizardstep3data.cpp:37
msgid "Abort"
msgstr "Avbryt"

#: cwizardstep3data.cpp:42
msgid "Start"
msgstr "Start"

#: czip.cpp:30
msgid "Packed"
msgstr "Pakket"

#: czip.cpp:31
msgid "Ratio"
msgstr "Forhold"

#: karchiveur.cpp:123
#: karchiveur.cpp:133
#: karchiveur.cpp:142
msgid "Failed to create temporary attachment directory '%2': %1"
msgstr "Klarte ikke å opprette midlertidig vedleggskatalog '%2': %1"

#: karchiveur.cpp:358
msgid "Opening a new application window..."
msgstr "Åpner et nytt aplikasjons-vindu..."

#: karchiveur.cpp:368
msgid "Creating new document..."
msgstr "Lager nytt dokument..."

#: karchiveur.cpp:383
msgid "Opening archive..."
msgstr "Åpner arkiv..."

#: karchiveur.cpp:420
msgid "Convert archive format..."
msgstr "Konverter arkivformat..."

#: karchiveur.cpp:443
#: karchiveur.cpp:542
msgid "First open an archive !"
msgstr "Du må åpne et arkiv først!"

#: karchiveur.cpp:452
msgid "Closing file..."
msgstr "Lukker fil..."

#: karchiveur.cpp:460
msgid "Printing..."
msgstr "Skriver ut..."

#: karchiveur.cpp:474
msgid "Exiting..."
msgstr "Avslutter..."

#: karchiveur.cpp:481
msgid "Toggle the toolbar..."
msgstr "Slå av/på verktøylinje..."

#: karchiveur.cpp:498
msgid "Toggle the statusbar..."
msgstr "Skrur av/på statuslinjen..."

#: karchiveur.cpp:547
msgid "Extract files..."
msgstr "Pakk ut filer..."

#: karchiveur.cpp:567
msgid "Add files..."
msgstr "Legg til filer..."

#: karchiveur.cpp:592
msgid "Create archive"
msgstr "Lage arkiv"

#: karchiveur.cpp:600
#: karchiveur.cpp:941
#: karchiveur.cpp:1790
#: karchiveur.cpp:2266
#: karchiveur.cpp:2443
msgid "Unknown archive type"
msgstr "Ukjent arkivtype"

#: karchiveur.cpp:624
msgid ""
"Files can be added only in\n"
".tar, .tar.gz, .tar.bz2"
msgstr ""
"Filer kan bare bli lagt til i\n"
".tar, .tar.gz, .tar.bz2"

#: karchiveur.cpp:639
msgid "View this file..."
msgstr "Se på denne fila..."

#: karchiveur.cpp:651
#: karchiveur.cpp:699
#: karchiveur.cpp:1918
#: karchiveur.cpp:2063
msgid "First select a file"
msgstr "Velg ei fil først"

#: karchiveur.cpp:686
msgid "View all selected files..."
msgstr "Se på alle valgte filer..."

#: karchiveur.cpp:733
#: karchiveur.cpp:2051
#: karchiveur.cpp:2127
#: karchiveur.cpp:2156
msgid "Deleting selected files..."
msgstr "Slett valgte filer..."

#: karchiveur.cpp:765
msgid "Do you really want to delete these files?"
msgstr "Vil du slette disse filene?"

#: karchiveur.cpp:767
#: karchiveur.cpp:891
#: karchiveur.cpp:1426
#: karchiveur.cpp:1594
#: karchiveur.cpp:1599
#: main.cpp:82
msgid "KArchiver"
msgstr "KArchiver"

#: karchiveur.cpp:877
msgid "Choose an archive to cutt"
msgstr "Velg et arkiv å dele opp"

#: karchiveur.cpp:890
msgid "archive split in %1.01..."
msgstr "arkivet er delt i %1.01..."

#: karchiveur.cpp:901
msgid "Choose an archive to uncutt"
msgstr "Velg et arkiv å sette sammen"

#: karchiveur.cpp:1023
msgid "Running wizard"
msgstr "Kjører hjelper"

#: karchiveur.cpp:1371
msgid ""
"Archives can only be converted in\n"
".tar, .tar.gz, .tar.bz2, .zip, .rar, .lha"
msgstr ""
"Arkiv kan bare konverteres til\n"
".tar, .tar.gz, .tar.bz2, .zip, .rar, .lha"

#: karchiveur.cpp:1388
msgid "Delete : %1 ?"
msgstr "Slett: %1 ?"

#: karchiveur.cpp:1425
msgid "archive splitted in %1.01..."
msgstr "arkiv delt i %1.01..."

#: karchiveur.cpp:1441
msgid "Bad url !"
msgstr "Feil url!"

#: karchiveur.cpp:1449
msgid "Cannot download file!"
msgstr "Kan ikke laste ned fil!"

#: karchiveur.cpp:1594
msgid "Stop opening of current archive ?"
msgstr "Stopp åpninga av gjeldende arkiv?"

#: karchiveur.cpp:1599
msgid "Stop extracting current archive ?"
msgstr "Stopp utpakkinga av gjeldende arkiv?"

#: karchiveur.cpp:1770
msgid ""
"Files can be added only in\n"
".tar, .tar.gz, .tar.bz2 .zip .rar .lha"
msgstr ""
"Filer kan bare bli lagt til i\n"
".tar, .tar.gz, .tar.bz2 .zip .rar .lha"

#: karchiveur.cpp:1782
msgid "Archive not created"
msgstr "Arkiv ikke laget"

#: karchiveur.cpp:1847
msgid ""
"Files can be added only in\n"
" .tar, .tar.gz, .tar.bz2"
msgstr ""
"Filer kan bare bli lagt til i\n"
".tar, .tar.gz, .tar.bz2"

#: karchiveur.cpp:1905
msgid "Move all selected files to trash..."
msgstr "Flytt alle valgte filer til søppelkurv"

#: karchiveur.cpp:1997
msgid "Paste those files in any application NOW !"
msgstr "Lim inn de filene i ethvert program nå!"

#: karchiveur.cpp:2032
#, fuzzy
msgid "No more occurrences found!"
msgstr "Ingen flere tilslag funnet"

#: karchiveur.cpp:2102
msgid "Nothing to undo !"
msgstr "Ingenting å angre!"

#: karchiveur.cpp:2116
msgid "Nothing to redo !"
msgstr "Ingenting å gjøre omigjen!"

#: karchiveur.cpp:2249
msgid "Choose new archive name"
msgstr "Velg et nytt navn til arkivet"

#: karchiveur.cpp:2251
msgid "Convert archive"
msgstr "Konverter arkiv"

#: karchiveur.cpp:2317
msgid "New &Window"
msgstr "Nytt &vindu"

#: karchiveur.cpp:2318
msgid "&New"
msgstr "&Ny"

#: karchiveur.cpp:2320
msgid "Search for archives"
msgstr "Søk etter arkiver"

#: karchiveur.cpp:2322
msgid "Convert &as"
msgstr "Konverter &som"

#: karchiveur.cpp:2323
msgid "View archive infos"
msgstr "Se på arkivinfo"

#: karchiveur.cpp:2325
msgid "C&lose Window"
msgstr "&Lukk vindu"

#: karchiveur.cpp:2330
msgid "Extract To"
msgstr "Pakk ut til"

#: karchiveur.cpp:2332
msgid "View file"
msgstr "Se på fil"

#: karchiveur.cpp:2333
msgid "View selected"
msgstr "Se på valgte"

#: karchiveur.cpp:2334
msgid "Delete Files"
msgstr "Slett filer"

#: karchiveur.cpp:2335
msgid "Move to trash"
msgstr "Flytt til søppelkurv"

#: karchiveur.cpp:2336
msgid "Launch the wizard"
msgstr "Start veileder"

#: karchiveur.cpp:2347
msgid "Select none"
msgstr "Velg ingen"

#: karchiveur.cpp:2348
msgid "Find..."
msgstr "Finn..."

#: karchiveur.cpp:2352
msgid "Split"
msgstr "Del opp"

#: karchiveur.cpp:2353
msgid "Unsplit"
msgstr "Legg sammen"

#: karchiveur.cpp:2357
msgid "&Toolbar"
msgstr "&Verktøylinje"

#: karchiveur.cpp:2358
msgid "&Statusbar"
msgstr "&Statuslinje"

#: karchiveur.cpp:2359
msgid "&File browser"
msgstr "&Filleser"

#: karchiveur.cpp:2360
msgid "preferences"
msgstr "innstillinger"

#: karchiveur.cpp:2363
msgid "Opens a new application window"
msgstr "Åpner et nytt applikasjons-vindu"

#: karchiveur.cpp:2364
#: karchiveur.cpp:2365
msgid "Creates a new archive"
msgstr "Lager et nytt arkiv"

#: karchiveur.cpp:2366
msgid "Search for existing archive"
msgstr "Søk etter et eksisterende arkiv"

#: karchiveur.cpp:2367
msgid "Convert current archive format to..."
msgstr "Konverter gjeldende arkiv til..."

#: karchiveur.cpp:2368
msgid "Delete selected files"
msgstr "Slett valgte filer"

#: karchiveur.cpp:2369
msgid "Extract to..."
msgstr "Pakk ut til..."

#: karchiveur.cpp:2370
msgid "Add files to this archive"
msgstr "Legg filer til dette arkivet"

#: karchiveur.cpp:2371
msgid "View the current file"
msgstr "Se på gjeldende fil"

#: karchiveur.cpp:2372
msgid "View all selected files"
msgstr "Se på alle valgte filer"

#: karchiveur.cpp:2373
msgid "Display info on current archive"
msgstr "Vis info om gjeldende arkiv"

#: karchiveur.cpp:2374
msgid "Launch a wizard to configure & compile this archive..."
msgstr "Start en veileder for å konfigurere og lage dette arkivet..."

#: karchiveur.cpp:2375
msgid "Split archive in 1.4Mb blocks"
msgstr "Del opp arkivet i blokker á 1,4Mb"

#: karchiveur.cpp:2376
msgid "UnSplit archive"
msgstr "Legg sammen arkiv"

#: karchiveur.cpp:2377
msgid "Enables / disables the file navigator"
msgstr "Skrur av / på filnavigatoren"

#: karchiveur.cpp:2378
msgid "Modify options of KArchiver"
msgstr "Endre muligheter i KArchiver"

#: karchiveur.cpp:2379
msgid "move the files that were copied to the clipboard"
msgstr "flytt filene som ble kopiert til utklippstavla"

#: karchiveur.cpp:2380
msgid "Extract selected files and copy them to the clipboard"
msgstr "Pakk ut valgte filer og kopier dem til utklippstavla"

#: karchiveur.cpp:2381
msgid "Add the files that were copied to the clipboard"
msgstr "Legg til filene som ble kopiert til utklippstavla"

#: karchiveur.cpp:2382
msgid "Find a filename in this archive"
msgstr "Finn et filnavn i dette arkivet"

#: karchiveur.cpp:2383
msgid "Undo the last operation"
msgstr "Angre den siste operasjonen"

#: karchiveur.cpp:2384
msgid "Redo the last operation"
msgstr "Gjør den siste operasjonen om igjen"

#: karchiveur.cpp:2579
msgid ""
"Has gziped ark files:\n"
msgstr ""
"Har gzippete ark filer:\n"

#: karchiveur.cpp:2579
msgid "KArchiver setup"
msgstr "Oppsett for KArchiver"

#: karchiveurpart.cpp:30
msgid ""
"KArchiver for KDE2 KPart\n"
"An easy tool to manage all your compressed files!\n"
"Released under GNU license"
msgstr ""
"Karchiver for KDE2 KPart\n"
"Et enkelt verktøy for å behandle dine komprimerte filer!\n"
"Utgitt under GNU-lisens"

#: karchiveurpart.cpp:104
msgid "&Extract"
msgstr "&Pakk ut"

#: karchiveurpart.cpp:164
msgid "Failed to create temporary attachment directory '%1'"
msgstr "Klarte ikke å opprette midlertidig vedleggskatalog '%1'"

#: karchiveurpart.cpp:172
#, c-format
msgid "Failed to create temporary attachment directory '%1"
msgstr "Klarte ikke å opprette midlertidig vedleggskatalog '%1"

#: karchiveurpart.cpp:263
#: karchiveurview.cpp:33
msgid "Name"
msgstr "Navn"

#: karchiveurpart.cpp:264
#: karchiveurview.cpp:34
msgid "Size"
msgstr "Størrelse"

#: karchiveurpart.cpp:265
#: karchiveurview.cpp:35
msgid "Time"
msgstr "Tid"

#: karchiveurpart.cpp:269
#: karchiveurview.cpp:39
msgid "Path"
msgstr "Sti"

#: ktipofday.cpp:33
msgid "Tip of the Day"
msgstr "Dagens tips"

#: ktipofday.cpp:74
msgid "Did you know...?"
msgstr "Visste du at...?"

#: ktipofday.cpp:99
msgid "show Tip of the day on next start"
msgstr "vis neste Dagens tips ved neste oppstart"

#: ktipofday.cpp:113
msgid "Next Tip"
msgstr "Neste tips"

#: ktipofday.cpp:157
msgid "Tip database not found! Please check your installation."
msgstr "Tipsdatabasen ble ikke funnet! Sjekk innstallasjonen din."

#: main.cpp:62
msgid ""
"KArchiver for KDE2\n"
"An easy tool to manage all your compressed files!\n"
"Released under GNU license"
msgstr ""
"Karchiver for KDE2\n"
"Et enkelt verktøy for å behandle dine komprimerte filer!\n"
"Utgitt under GNU-lisens"

#: main.cpp:67
msgid "file to open"
msgstr "fil å åpne"

#: main.cpp:69
msgid "Extract to the specified directory"
msgstr "Pakk ut i den spesifiserte katalogen"

#: main.cpp:71
msgid "Extract and choose directory in a dialog box"
msgstr "Pakk ut og velg en katalog i en dialogboks"

#: main.cpp:73
msgid "Extract to current directory"
msgstr "Pakk ut i gjeldende katalog"

#: main.cpp:75
msgid "Extract to the directory where the archive is"
msgstr "Pakk ut i katalogen der arkivet ligger"

#: main.cpp:85
msgid "Developer and Maintainer"
msgstr "Utvikler og vedlikeholder"

#: main.cpp:86
msgid "Bug track and icon designer"
msgstr "Feilretter og ikondesigner"

#: main.cpp:87
#, fuzzy
msgid "Code improvement"
msgstr "Kodeforbedringer"

#: rc.cpp:1
#: rc.cpp:3
msgid "&Archive"
msgstr "&Arkiv"

#: rc.cpp:5
msgid "&Disk"
msgstr "&Disk"

#: rc.cpp:6
msgid "&Configuration"
msgstr "&Konfigurasjon"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Andreas Landmark"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "andreas.landmark@skolelinux.no"

#: i18n_tips.cpp:1
msgid "The Karchiveur Team wishes you a nice day !"
msgstr "Karchiveur-laget ønsker deg en god dag!"

#: i18n_tips.cpp:2
msgid ""
"Karchiveur provides a wizard, to make complex operations step by step just "
"with a mouse !"
msgstr ""
"Karchiveur kommer med en hjelper, for å gjøre komplekse operasjoner steg "
"for steg med ei mus!"

#: i18n_tips.cpp:3
msgid "... that karchiveur can translate a .tar.gz in a .zip, or any other format ?"
msgstr ""
"... at karchiveur kan oversette en .tar.gz-fil til en -zip, eller et hvilket "
"som helst annet format?"

#: i18n_tips.cpp:4
msgid ""
"... that you can extract files just by dropping them to konqueror, the file "
"manager ?"
msgstr "... at du kan pakke ut filer ved å dra dem til konqueror, filbehandleren?"

#: i18n_tips.cpp:5
msgid ""
"... that you can add files or create an archive by dropping folders or files "
"to karchiveur ?"
msgstr ""
"... at du kan legge til filer eller lage et arkiv ved å dra mapper eller "
"filer til karchiveur?"

#: i18n_tips.cpp:6
msgid ""
"... that karchiveur's wizard can configure, compile and install a GNU "
"software without typing any command ?"
msgstr ""
"at karchiveur-hjelperen kan konfigurere, kompilere og innstallere et "
"GNU-program uten at du taster inn en kommando?"

#: i18n_tips.cpp:7
msgid "... that a right mouse button clic makes a contextual menu appear ?"
msgstr "... at ved et klikk med høyre museknapp dukker en meny opp?"

#: i18n_tips.cpp:8
msgid "... that you can view any file of the archive with its associated software ?"
msgstr "... at du kan se på enhver av filene i et arkiv med passende program?"

