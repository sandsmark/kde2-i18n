# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-08-04 12:46+0200\n"
"PO-Revision-Date: 2001-08-05 10:37CEST\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@telia.com>\n"
"Language-Team: Svenska <sv@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: channellistwindow.cpp:42
msgid "Update"
msgstr "Uppdatera"

#: channellistwindow.cpp:52 napigatordlg.cpp:55
msgid "Name"
msgstr "Namn"

#: channellistwindow.cpp:54 napigatordlg.cpp:57
msgid "Users"
msgstr "Användare"

#: channellistwindow.cpp:57
msgid "Subject"
msgstr "Ämne"

#: channellistwindow.cpp:90
msgid "No channels loaded"
msgstr "Inga kanaler laddade"

#: channellistwindow.cpp:105
msgid "%1 Channels present"
msgstr "%1 tillgängliga kanaler"

#: chatpanel.cpp:45
msgid "Console"
msgstr "Konsol"

#: chatpanel.cpp:50
msgid "Channel List"
msgstr "Kanallista"

#: chatpanel.cpp:64
msgid "Friends"
msgstr "Användare"

#: chatpanel.cpp:97 chatpanel.cpp:104 chatpanel.cpp:115
msgid "%1 said: %2"
msgstr "%1 sa: %2"

#: chatpanel.cpp:157
msgid ""
"_: <user> message\n"
"<%1> %2"
msgstr "<%1> %2"

#: chatpanel.cpp:230
msgid "Join Channel"
msgstr "Gå med i kanal"

#: chatpanel.cpp:232
msgid "Part Channel"
msgstr "Lämna kanal"

#: chatpanel.cpp:324
#, c-format
msgid "Talk: %1"
msgstr "Prata: %1"

#: chatwindow.cpp:60 downloadpanel.cpp:70 friendswindow.cpp:227
#: uploadpanel.cpp:62
msgid "Ping user"
msgstr "Pinga användare"

#: chatwindow.cpp:62 downloadpanel.cpp:72 friendswindow.cpp:228
#: uploadpanel.cpp:63
msgid "Whois user"
msgstr "Vem är användare"

#: chatwindow.cpp:64
msgid "Private message user"
msgstr "Privat meddelande till användare"

#: chatwindow.cpp:66 searchpanel.cpp:250
msgid "Add user to friend list"
msgstr "Lägg till användare till användarlistan"

#: chatwindow.cpp:119
msgid "User %1 has joined."
msgstr "Användare %1 har gått med."

#: chatwindow.cpp:130
msgid "User %1 has left."
msgstr "Användare %1 har lämnat kanalen."

#: chatwindow.cpp:158 talkwindow.cpp:105
msgid "Commands are: "
msgstr "Kommandon är:"

#: chatwindow.cpp:159 talkwindow.cpp:106
msgid " /clear : clear the console"
msgstr " /clear : rensa konsolen"

#: chatwindow.cpp:160
msgid " /version : get the server version"
msgstr " /version : ange serverversion"

#: chatwindow.cpp:162 talkwindow.cpp:108
msgid " /msg <user> <message>: send a message to a user"
msgstr " /msg <användare><meddelande>: skicka ett meddelande till en användare"

#: chatwindow.cpp:164
msgid " /ping <user> : ping a user"
msgstr " /ping <användare> : pinga en användare"

#: chatwindow.cpp:165
msgid " /whois <user> : get info about a user"
msgstr " /whois <användare> : hämta information om en användare"

#: chatwindow.cpp:167
msgid " /list        : list the available channels"
msgstr " /list        : lista de tillgängliga kanalerna"

#: chatwindow.cpp:168
msgid " /join <chan> : join the channel"
msgstr " /join <kanal> : gå med i kanalen"

#: chatwindow.cpp:169
msgid " /part        : leave this channel"
msgstr " /part        : lämna den här kanalen"

#: chatwindow.cpp:170
msgid " /chan <chan> <msg> : send a message to a channel"
msgstr " /chan <kanal><meddelande> : skicka ett meddelande till en kanal"

#: chatwindow.cpp:171
msgid " /users <chan> : list the users on channel <chan>"
msgstr " /users <kanal> : lista användare på kanalen <kanal>"

#: chatwindow.cpp:173 talkwindow.cpp:115
msgid " /add username : add user 'username' to friend list"
msgstr ""
" /add användarnamn : lägg till användaren 'användarnamn' till "
"användarlistan"

#: chatwindow.cpp:174 talkwindow.cpp:116
msgid " /del username : remove user 'username' from friend list"
msgstr ""
" /del användarnamn : ta bort användaren 'användarnamn' från "
"användarlistan"

#: chatwindow.cpp:176
msgid "/talk user     : open up a private talk window for user"
msgstr ""
"/talk användare : öppna ett privat fönster för att prata med en "
"användare"

#: chatwindow.cpp:178 talkwindow.cpp:118
msgid " /save        : prompt to save contents of the window"
msgstr " /save         : fråga om att spara innehållet i fönstret"

#: chatwindow.cpp:181
msgid "/kill username : kill username"
msgstr "/kill användarnamn: ta bort användare permanent"

#: chatwindow.cpp:182
msgid "/kick username : kick username off this channel"
msgstr "/kick användare : ta bort användare från den här kanalen"

#: chatwindow.cpp:186
msgid "Anything else not starting with / is sent directly to the channel"
msgstr "Allt annat som inte börjar med / skickas direkt till kanalen"

#: chatwindow.cpp:214 talkwindow.cpp:150
#, c-format
msgid "You said to %1"
msgstr "Du sa till %1"

#: chatwindow.cpp:242 knapster_cmd.cpp:223 talkwindow.cpp:163
#, c-format
msgid "Unknown cmd given: %1"
msgstr "Felaktigt kommando: %1"

#: downloadlistitem.cpp:42 downloadpanel.cpp:260 napigatoritem.cpp:32
#: napigatoritem.cpp:35 napigatoritem.cpp:38 propertiesdlg.cpp:137
#: support_funcs.cpp:71 uploadlistitem.cpp:62
msgid "Unknown"
msgstr "Okänd"

#: downloadpanel.cpp:44 searchpanel.cpp:244
msgid "Download file"
msgstr "Ladda ner fil"

#: downloadpanel.cpp:46
msgid "Read/size"
msgstr "Läs/storlek"

#: downloadpanel.cpp:50 uploadpanel.cpp:51
msgid "Rate/s"
msgstr "Takt/s"

#: downloadpanel.cpp:52 friendswindow.cpp:45 friendswindow.cpp:52
#: searchpanel.cpp:81 searchpanel.cpp:226 uploadpanel.cpp:53
msgid "User"
msgstr "Användare"

#: downloadpanel.cpp:54 uploadpanel.cpp:55
msgid "Status"
msgstr "Status"

#: downloadpanel.cpp:57
msgid "Progress"
msgstr "Förlopp"

#: downloadpanel.cpp:60
msgid "Time left"
msgstr "Tid kvar"

#: downloadpanel.cpp:67 librarypanel.cpp:84
msgid "Play"
msgstr "Spela"

#: downloadpanel.cpp:75
msgid "Terminate download"
msgstr "Avsluta nerladdning"

#: downloadpanel.cpp:78
msgid "Terminate download and delete file"
msgstr "Avsluta nerladdning och ta bort fil"

#: downloadpanel.cpp:233 uploadlistitem.cpp:36
msgid "Finished"
msgstr "Färdig"

#: downloadpanel.cpp:236 uploadlistitem.cpp:39
msgid "Initializing"
msgstr "Initierar"

#: downloadpanel.cpp:242
msgid "Downloading"
msgstr "Laddar ner..."

#: downloadpanel.cpp:245 uploadlistitem.cpp:48
msgid "No connection"
msgstr "Ingen anslutning"

#: downloadpanel.cpp:248 uploadlistitem.cpp:51
msgid "Terminated"
msgstr "Avbruten"

#: downloadpanel.cpp:251 uploadlistitem.cpp:54
msgid "No file"
msgstr "Ingen fil"

#: downloadpanel.cpp:254
msgid "Timed out"
msgstr "Tidsgräns överskriden"

#: downloadpanel.cpp:257 uploadlistitem.cpp:57
msgid "Queued"
msgstr "Köad"

#: friendswindow.cpp:229
msgid "Talk to user"
msgstr "Chatta med användare"

#: friendswindow.cpp:232 friendswindow.cpp:239
msgid "Remove from friend list"
msgstr "Ta bort från användarlistan"

#: knapster_cmd.cpp:132
msgid "Unable to run command - Not connected"
msgstr "Kan inte utföra kommandot - inte uppkopplad"

#: knapster_cmd.cpp:188
msgid "You said: %1 to %2"
msgstr "Du sa: %1 till %2"

#: knapster_cmd.cpp:192
msgid "Listing users..."
msgstr "Letar efter användare..."

#: knapster_cmd.cpp:213
#, c-format
msgid "** sending %1"
msgstr "** skickar %1"

#: knapster_cmd.cpp:356
#, c-format
msgid "Username:   %1"
msgstr "Användarnamn:  %1"

#: knapster_cmd.cpp:359
#, c-format
msgid " Class:      %1"
msgstr " Klass:      %1"

#: knapster_cmd.cpp:371
#, c-format
msgid " Last logged on: %1"
msgstr " Senaste anslutning: %1"

#: knapster_cmd.cpp:374
msgid " Status:     Offline"
msgstr " Status:      Nerkopplad"

#: knapster_cmd.cpp:485
msgid "Pingtime for %1 was %2 ms"
msgstr "Pingtid för %1 var %2 ms"

#: knapster_cmd.cpp:499
#, c-format
msgid "Pinging user %1"
msgstr "Pingar användaren %1"

#: knapster_cmd.cpp:506
#, c-format
msgid "Whois user... %1"
msgstr "Vem är användare... %1"

#: knapster_connect.cpp:189
msgid "Starting download of %1 from %2"
msgstr "Startar nerladdning av %1 från %2"

#: knapster.cpp:1031 knapster_connect.cpp:265
#, c-format
msgid "Warning: someone tried to download unshared song %1"
msgstr "Varning, någon försökte ladda upp låt %1 som inte är delad"

#: knapster_connect.cpp:292
msgid "%1 has started upload of %2"
msgstr "%1 har startat uppladdning av %2"

#: knapster.cpp:125 knapster.cpp:127 knapster.cpp:507 knapster.cpp:623
msgid "Not connected"
msgstr "Nerkopplad"

#: knapster.cpp:126
msgid "0 Songs in 0 Libraries. Total 0 GB."
msgstr "0 låtar i 0 kataloger. Totalt 0 Gb."

#: knapster.cpp:189
msgid "Console/Chat"
msgstr "Konsol/Chat"

#: knapster.cpp:206 searchpanel.cpp:101
msgid "Search"
msgstr "Sök"

#: knapster.cpp:237
msgid "Uploads/Downloads"
msgstr "Uppladdning / Nerladdning"

#: knapster.cpp:245
msgid "MP3 Library"
msgstr "MP3-katalog"

#: knapster.cpp:254
msgid "Recent"
msgstr "Senaste"

#: knapster.cpp:258
msgid "&LogOn/Off"
msgstr "&Logga på/Logga av"

#: knapster.cpp:260
msgid "&Relogin"
msgstr "Logga in &igen"

#: knapster.cpp:263
msgid "&Connect via &Napigator..."
msgstr "&Anslut via &Napigator..."

#: knapster.cpp:267
msgid "&Clear Console"
msgstr "&Rensa konsol"

#: knapster.cpp:289
msgid "No listener could be created - some downloads will not work"
msgstr "Ingen lyssnare kunde skapas - vissa nerladdningar kommer inte att fungera"

#: knapster.cpp:308
msgid ""
"You are trying to connect to a new server while you are still connected to a "
"server.\n"
"\n"
"Do you want to close the connection and connect to the new server?"
msgstr ""
"Du försöker ansluta till en ny server medan du fortfarande är uppkopplad "
"till en server.\n"
"\n"
"Vill du koppla ner anslutningen och ansluta till den nya servern?"

#: knapster.cpp:316 knapster.cpp:882 knapster.cpp:1477
msgid "&Yes"
msgstr "&Ja"

#: knapster.cpp:330
msgid "Connected"
msgstr "Uppkopplad"

#: knapster.cpp:336
msgid "Not Connected"
msgstr "Nerkopplad"

#: knapster.cpp:352
msgid "Cannot connect to invalid IP/port combination \"%1\"."
msgstr "Kan inte ansluta till ogiltig IP/port kombination \"%1\""

#: knapster.cpp:384
msgid "You have to connect to a server before you can start a search."
msgstr "Du måste ansluta till en server innan du kan starta en sökning."

#: knapster.cpp:394
msgid "Searching..."
msgstr "Söker..."

#: knapster.cpp:447 knapster.cpp:531
msgid "To logon, the username and password must be given in the preferences."
msgstr ""
"För att logga in måste användarnamnet och  lösenordet anges i "
"inställningarna."

#: knapster.cpp:457
msgid "Trying to connect..."
msgstr "Försöker ansluta..."

#: knapster.cpp:464
msgid "No Napster server has been specified in the preferences."
msgstr "Ingen Napster server har specifierats i inställningarna."

#: knapster.cpp:480 knapster.cpp:600
msgid ""
"%1\n"
"\n"
"Do you want to try to login as a new user?"
msgstr ""
"%1\n"
"\n"
"Vill du försöka logga på som en ny användare?"

#: knapster.cpp:486 knapster.cpp:606
msgid ""
"%1\n"
"\n"
"Please try to login under a different name."
msgstr ""
"%1\n"
"\n"
"Var god försök logga på med ett annat namn."

#: knapster.cpp:491
#, c-format
msgid "Could not connect to best host: %1"
msgstr "Kunde inte ansluta till den bästa värddatorn:  %1"

#: knapster.cpp:494
#, c-format
msgid "Could not connect to server: %1"
msgstr "Kunde inte ansluta till server: %1"

#: knapster.cpp:501 knapster.cpp:618
#, c-format
msgid "Connected to %1"
msgstr "Uppkopplad till %1"

#: knapster.cpp:551
msgid "Invalid IP/port obtained from Napigator"
msgstr "Ogiltig IP/port erhållen från napigator"

#: knapster.cpp:587
msgid "Trying to relogin..."
msgstr "Försöker logga in igen..."

#: knapster.cpp:611
#, c-format
msgid ""
"Could not connect to best host:\n"
"\n"
"%1"
msgstr ""
"Kunde inte ansluta till den bästa värddatorn:\n"
"\n"
"%1"

#: knapster.cpp:649
msgid "** read [%1] size: %2=%3"
msgstr "** läst [%1] storlek: %2=%3"

#: knapster.cpp:694
#, c-format
msgid "Channel: %1"
msgstr "Kanal: %1"

#: knapster.cpp:698
msgid "End of channel list"
msgstr "Slut på kanallista"

#: knapster.cpp:722
#, c-format
msgid "Joined channel %1"
msgstr "Gick med i kanal %1"

#: knapster.cpp:727
#, c-format
msgid "Parted channel %1"
msgstr "Lämnade kanal %1"

#: knapster.cpp:748
msgid "Search finished"
msgstr "Sökning klar"

#: knapster.cpp:757
#, c-format
msgid "User remote queue full: %1"
msgstr "Användarens fjärrkö full: %1"

#: knapster.cpp:767
#, c-format
msgid "Napster server error downloading %1"
msgstr "Napster serverfel vid nerladdning %1"

#: knapster.cpp:776
msgid "Server has received another login attempt by a user of the same name"
msgstr ""
"Servern har tagit emot ett annat loginförsök från en användare med samma "
"namn"

#: knapster.cpp:784
msgid "The Napster server reported the following:"
msgstr "Napsterservern rapporterade följande:"

#: knapster.cpp:791
msgid "Friend %1 has logged on."
msgstr "Användaren %1 har TBD."

#: knapster.cpp:797
msgid "User %1 has logged off."
msgstr "Användaren %1 har loggat av."

#: knapster.cpp:802
msgid "Couldn't add %1 to hotlist"
msgstr "Kunde inte lägga till %1 till användarlistan"

#: knapster.cpp:813
msgid "Unknown blk [%1] size: %2=%3"
msgstr "Okänt block [%1] storlek: %2 = %3"

#: knapster.cpp:825
msgid "Napster connection has been closed"
msgstr "Napster anslutningen har stängts"

#: knapster.cpp:838
msgid "%1 Songs in %2 Libraries. Total %3 GB."
msgstr "%1 låtar i %2 kataloger. Totalt %3 Gb."

#: knapster.cpp:879
#, c-format
msgid ""
"Terminate and delete the file:\n"
"%1"
msgstr ""
"Avsluta och ta bort filen:\n"
"%1"

#: knapster.cpp:894
#, c-format
msgid "Unable to delete file: %1"
msgstr "Kunde inte radera fil: %1"

#: knapster.cpp:993
msgid "Couldn't download %1 from %2 because their download queue is full"
msgstr "Kunde inte ladda ner %1 från %2 eftersom deras nerladdningskö är full"

#: knapster.cpp:1071
msgid "Can't download %1 from %2 as there is no listener connection."
msgstr ""
"Kan inte ladda ner %1 från %2 eftersom det inte finns någon "
"lyssningsanslutning."

#: knapster.cpp:1078
msgid "Can't download %1 since both persons are behind a firewall"
msgstr "Kan inte ladda ner %1 eftersom båda personerna är bakom en brandvägg"

#: knapster.cpp:1096
msgid ""
"You are about to download file\n"
"%1\n"
"over an existing file.\n"
"This file can be overwritten or\n"
"a resume can be attempted.\n"
" - Note that the bitrate/frequency\n"
"should match for the song to\n"
"resume properly."
msgstr ""
"Du försöker spara filen\n"
"%1\n"
"över en existerande fil.\n"
"Den här filen kan skrivas över eller\n"
"så kan den försöka återupptas.\n"
" - Observera att takt/frekvens\n"
"måste stämma för att kunna\n"
"återuppta nerladdningen."

#: knapster.cpp:1105
msgid "Resume"
msgstr "Återuppta"

#: knapster.cpp:1106
msgid "Overwrite"
msgstr "Skriv över"

#: knapster.cpp:1145
msgid "Couldn't create download to start download of %1 from %2"
msgstr "Kunde inte ansluta för att starta nerladdning av %1 från %2"

#: knapster.cpp:1157
msgid "Listing channels..."
msgstr "Letar efter kanaler..."

#: knapster.cpp:1211
#, c-format
msgid "User terminated download of %1"
msgstr "Användaren avbröt nerladdning av %1"

#: knapster.cpp:1227
msgid "Couldn't connect to %1 to start download of %2"
msgstr "Kunde inte ansluta till %1 för att starta nerladdning av %2"

#: knapster.cpp:1245
msgid "Couldn't open local file to save %1 from %2"
msgstr "Kunde inte öppna lokal fil för att spara %1 från %2"

#: knapster.cpp:1262
msgid "Time out occurred trying to download %1 from %2"
msgstr "Tillgänglig tid överskreds under nerladdning av %1 från %2"

#: knapster.cpp:1279
msgid "Error occurred during download of %1 from %2 (%3 of %4 bytes read)."
msgstr "Fel inträffade under nerladdning av %1 från %2 (%3 av %4 byte lästa)."

#: knapster.cpp:1334
msgid "Finished upload of %1 to %2"
msgstr "Färdig med uppladdning av %1 till %2"

#: knapster.cpp:1352
#, c-format
msgid "User terminated upload of %1"
msgstr "Användaren avbröt uppladdning av %1"

#: knapster.cpp:1367
msgid "Error occurred during upload %1 from %2"
msgstr "Fel inträffade under uppladdning av %1 till %2"

#: knapster.cpp:1400
msgid "Waiting for %1 to connect..."
msgstr "Väntar på att %1 skall ansluta..."

#: knapster.cpp:1419
msgid "Loading local MP3's from preferences..."
msgstr "Laddar lokala MP3 filer från inställningarna..."

#: knapster.cpp:1437 napigatordlg.cpp:140
msgid "Done"
msgstr "Klar"

#: knapster.cpp:1473
msgid ""
"You are about to quit the application. This will stop all files currently "
"being transfered.\n"
"\n"
"Do you really want to continue?"
msgstr ""
"Du ska precis avsluta programmet. Det här kommer att stoppa alla filer som "
"håller på att överföras.\n"
"\n"
"Vill du verkligen fortsätta?"

#: librarypanel.cpp:54
msgid "Rescan"
msgstr "Omsökning"

#: librarypanel.cpp:67 searchpanel.cpp:219
msgid "Bitrate"
msgstr "Takt"

#: librarypanel.cpp:70 searchpanel.cpp:213
msgid "Frequency"
msgstr "Frekvens"

#: librarypanel.cpp:73 searchpanel.cpp:210 uploadpanel.cpp:49
msgid "Size"
msgstr "Storlek"

#: librarypanel.cpp:76 propertiesdlg.cpp:322
msgid "Shared"
msgstr "Utdelad"

#: librarypanel.cpp:151
msgid "Total: %1 Songs, %2 Shared, %3 GB."
msgstr "Totalt: %1 låtar, %2 delade, %3 Gb"

#: main.cpp:26
msgid "KOpenNap lets users search, download and upload music"
msgstr "KOpenNap låter användare söka efter, ladda ner och ladda upp musik"

#: main.cpp:32
msgid "Napster Music Downloader"
msgstr "Napster musiknerladdning"

#: main.cpp:37
msgid "KDE 2 support"
msgstr "Stöd för KDE 2"

#: main.cpp:39
msgid "Initial release"
msgstr "Ursprunglig utgåva"

#: napigatordlg.cpp:37
msgid "Connect"
msgstr "Koppla upp"

#: napigatordlg.cpp:51
msgid "Server"
msgstr "Server"

#: napigatordlg.cpp:53
msgid "Port"
msgstr "Port"

#: napigatordlg.cpp:63
msgid "Gigabytes"
msgstr "Gigabyte"

#: napigatordlg.cpp:132
msgid "Connecting to Napigator..."
msgstr "Ansluter till Napigator..."

#: napigatordlg.cpp:135
msgid "Parsing Napigator data..."
msgstr "Tolkar data från Napigator..."

#: napigatordlg.cpp:147
msgid "Error: Cannot connect to Napigator"
msgstr "Fel: Kan inte ansluta till Napigator"

#: napigatordlg.cpp:151
msgid "Error while getting Napigator data"
msgstr "Kunde inte hämta data från Napigator"

#: napigatordlg.cpp:155
msgid "Error: invalid data received from Napigator."
msgstr "Fel: ogiltig data mottagen från Napigator."

#: napigatordlg.cpp:159
#, c-format
msgid "Error from server: %1"
msgstr "Fel från server: %1"

#: napsterconnection.cpp:79
msgid "Timeout"
msgstr "Tidsgräns överskriden"

#: napsterconnection.cpp:80
msgid "Bad port number"
msgstr "Fel portnummer"

#: napsterconnection.cpp:81
msgid "Bad IP address"
msgstr "Felaktig IP-adress"

#: napsterconnection.cpp:82
msgid "Cannot connect to host"
msgstr "Kan inte ansluta till värddator"

#: napsterconnection.cpp:83
msgid "Read error"
msgstr "Läsfel"

#: napsterconnection.cpp:84
msgid "Write error"
msgstr "Skrivfel"

#: napsterconnection.cpp:85
msgid "Server is busy"
msgstr "Server är upptagen"

#: napsterconnection.cpp:86
msgid "Could not connect to server to get best host"
msgstr "Kunde inte ansluta till servern för att hämta den bästa värddatorn"

#: napsterconnection.cpp:87
msgid "Bad data received"
msgstr "Felaktig data mottagen"

#: napsterconnection.cpp:88
msgid "Trying to send bad data"
msgstr "Försökte skicka felaktig data"

#: napsterconnection.cpp:89
msgid "Could not login - incorrect user/password?"
msgstr "Kunde inte logga in - felaktig användare/lösenord?"

#: napsterconnection.cpp:90
msgid "Could not download"
msgstr "Kunde inte ladda ner"

#: napsterconnection.cpp:91
msgid "Could not open socket"
msgstr "Kunde inte öppna socket"

#: napsterconnection.cpp:92
msgid "Could not create bound listener socket"
msgstr "Kunde inte skapa bunden lyssnarsocket"

#: napsterconnection.cpp:93
msgid "Could not create a new user - perhaps this name is already in use"
msgstr "Kunde inte skapa ny användare - kanske används det här namnet redan"

#: napsterconnection.cpp:95
msgid "Invalid username/password for new user - use a different name/password."
msgstr ""
"Ogiltigt användarnamn/lösenord för ny användare - använd ett annat "
"namn/lösenord"

#: napsterconnection.cpp:97
msgid "Unknown error"
msgstr "Okänt fel"

#: propertiesdlg.cpp:73
msgid "&User"
msgstr "&Användare"

#: propertiesdlg.cpp:77
msgid "Username "
msgstr "Användarnamn"

#: propertiesdlg.cpp:85
msgid "Password "
msgstr "Lösenord"

#: propertiesdlg.cpp:96
msgid "Email "
msgstr "E-post"

#: propertiesdlg.cpp:111
msgid "&Connection"
msgstr "&Anslutning"

#: propertiesdlg.cpp:115
msgid "Napster Server "
msgstr "Napster server "

#: propertiesdlg.cpp:124
msgid "Download port "
msgstr "Nerladdningsport"

#: propertiesdlg.cpp:132
msgid "Connection speed "
msgstr "Anslutningshastighet"

#: propertiesdlg.cpp:138 support_funcs.cpp:72
msgid "14.4K"
msgstr "14.4K"

#: propertiesdlg.cpp:139 support_funcs.cpp:73
msgid "28.8K"
msgstr "28.8K"

#: propertiesdlg.cpp:140 support_funcs.cpp:74
msgid "33.6K"
msgstr "33.6K"

#: propertiesdlg.cpp:141 support_funcs.cpp:75
msgid "57.6K"
msgstr "57.6K"

#: propertiesdlg.cpp:142 support_funcs.cpp:76
msgid "64K ISDN"
msgstr "64K ISDN"

#: propertiesdlg.cpp:143 support_funcs.cpp:77
msgid "128K ISDN"
msgstr "128K ISDN"

#: propertiesdlg.cpp:144
msgid "Cable"
msgstr "Kabel"

#: propertiesdlg.cpp:145 support_funcs.cpp:79
msgid "DSL"
msgstr "DSL"

#: propertiesdlg.cpp:146 support_funcs.cpp:80
msgid "T1"
msgstr "T1"

#: propertiesdlg.cpp:147 support_funcs.cpp:81
msgid "T3+"
msgstr "T3+"

#: propertiesdlg.cpp:171
msgid "Up/Down&loads"
msgstr "Upp/Ner&laddningar"

#: propertiesdlg.cpp:175
msgid "Download directory "
msgstr "Nerladdningskatalog"

#: propertiesdlg.cpp:187
msgid "Max download connections "
msgstr "Max. nedladdningar "

#: propertiesdlg.cpp:195
msgid "Download timeout (mins) "
msgstr "Max. nedladdningstid (min)"

#: propertiesdlg.cpp:203
msgid "Beep on download finish"
msgstr "Ljudsignal vid färdig nedladdning"

#: propertiesdlg.cpp:207
msgid "Max upload connections "
msgstr "Max. uppladdningar "

#: propertiesdlg.cpp:228
msgid "&Console Colors"
msgstr "&Konsolfärger"

#: propertiesdlg.cpp:232
msgid "Normal Text"
msgstr "Normal text"

#: propertiesdlg.cpp:236
msgid "Info Text"
msgstr "Informationstext"

#: propertiesdlg.cpp:240
msgid "Message Text"
msgstr "Meddelandetext"

#: propertiesdlg.cpp:244
msgid "Error Text"
msgstr "Felmeddelanden"

#: propertiesdlg.cpp:251
msgid "Chat Channel Text"
msgstr "Kanaltext"

#: propertiesdlg.cpp:293 whoisdlg.cpp:52
msgid "Misc"
msgstr "Diverse"

#: propertiesdlg.cpp:298
msgid "Clear searchlist on new search"
msgstr "Rensa söklistan vid ny sökning"

#: propertiesdlg.cpp:302
msgid "Show full file path in search list"
msgstr "Visa full filsökväg i söklistan"

#: propertiesdlg.cpp:306
msgid "Allow multiselect in search list"
msgstr "Tillåt flerval i söklistan"

#: propertiesdlg.cpp:313
msgid "Library paths"
msgstr "Katalogsökvägar"

#: propertiesdlg.cpp:321
msgid "Path"
msgstr "Sökväg"

#: propertiesdlg.cpp:323
msgid "Recurse depth"
msgstr "Katalogdjup"

#: propertiesdlg.cpp:325
msgid "Path "
msgstr "Sökväg"

#: propertiesdlg.cpp:337
msgid "Shared "
msgstr "Utdelad"

#: propertiesdlg.cpp:349
msgid "Directory depth "
msgstr "Katalogdjup"

#: rc.cpp:1
msgid "&Connect"
msgstr "&Koppla upp"

#: searchpanel.cpp:75
msgid "Search for:"
msgstr "Sök efter:"

#: searchpanel.cpp:80
msgid "Text in title"
msgstr "Text i titel"

#: searchpanel.cpp:107 whoisdlg.cpp:62
msgid "Speed:"
msgstr "Hastighet:"

#: searchpanel.cpp:125
msgid "Bitrate:"
msgstr "Takt:"

#: searchpanel.cpp:137
msgid "64"
msgstr "64"

#: searchpanel.cpp:138
msgid "74"
msgstr "74"

#: searchpanel.cpp:139
msgid "96"
msgstr "96"

#: searchpanel.cpp:140
msgid "112"
msgstr "112"

#: searchpanel.cpp:141
msgid "128"
msgstr "128"

#: searchpanel.cpp:142
msgid "160"
msgstr "160"

#: searchpanel.cpp:143
msgid "192"
msgstr "192"

#: searchpanel.cpp:144
msgid "256"
msgstr "256"

#: searchpanel.cpp:148
msgid "Frequency:"
msgstr "Frekvens:"

#: searchpanel.cpp:162
msgid "11,025"
msgstr "11.025"

#: searchpanel.cpp:163
msgid "22,050"
msgstr "22.050"

#: searchpanel.cpp:164
msgid "44,100"
msgstr "44.100"

#: searchpanel.cpp:165
msgid "48,800"
msgstr "48.800"

#: searchpanel.cpp:169
msgid "Max results:"
msgstr "Max resultat:"

#: searchpanel.cpp:176
msgid "100"
msgstr "100"

#: searchpanel.cpp:179
msgid "Optimizing level:"
msgstr "Optimeringsnivå:"

#: searchpanel.cpp:184
msgid "Disabled"
msgstr "Avstängd"

#: searchpanel.cpp:185
#, c-format
msgid "5 %"
msgstr "5 %"

#: searchpanel.cpp:186
#, c-format
msgid "10 %"
msgstr "10 %"

#: searchpanel.cpp:187
#, c-format
msgid "15 %"
msgstr "15 %"

#: searchpanel.cpp:188
#, c-format
msgid "20 %"
msgstr "20 %"

#: searchpanel.cpp:189
#, c-format
msgid "30 %"
msgstr "30 %"

#: searchpanel.cpp:190
#, c-format
msgid "40 %"
msgstr "40 %"

#: searchpanel.cpp:191
#, c-format
msgid "50 %"
msgstr "50 %"

#: searchpanel.cpp:200 searchpanel.cpp:433 searchpanel.cpp:554
#: searchpanel.cpp:572
msgid "No results"
msgstr "Inga resultat"

#: searchpanel.cpp:216
msgid "Speed"
msgstr "Hastighet"

#: searchpanel.cpp:222
msgid "Time"
msgstr "Tid"

#: searchpanel.cpp:247
msgid "Ping User"
msgstr "Pinga användare"

#: searchpanel.cpp:248
msgid "Whois this User"
msgstr "Vem är den här användaren"

#: searchpanel.cpp:254
msgid "Clear search list"
msgstr "Rensa söklistan"

#: searchpanel.cpp:258
msgid "Show Path"
msgstr "Visa sökväg"

#: searchpanel.cpp:274
msgid ""
"<qt>"
"Enter the words you want to find here. If a word is prepended by a minus "
"symbol it will match only if that word is not found. If you are searching "
"for users you should enter the username only.</qt>"
msgstr ""
"<qt>Ange orden du vill hitta här. Om ett ord föregås av ett minustecken "
"kommer en träff att ske bara om det ordet inte hittas. Om du letar efter "
"användare ska du bara ange användarnamnet.</qt>"

#: searchpanel.cpp:280
msgid "<qt>This will start the search.</qt>"
msgstr "<qt>Det här startar sökningen.</qt>"

#: searchpanel.cpp:435
msgid "One Match found"
msgstr "En träff hittad"

#: searchpanel.cpp:437 searchpanel.cpp:551
msgid "%1 Matches found"
msgstr "%1 träffar hittade"

#: searchpanel.cpp:750
msgid "Don't care"
msgstr "Vilken som helst"

#: searchpanel.cpp:751
msgid "At least"
msgstr "Som minst"

#: searchpanel.cpp:752
msgid "At best"
msgstr "Som bäst"

#: searchpanel.cpp:753
msgid "Equal to"
msgstr "Lika med"

#: support_funcs.cpp:47
msgid "%1 GB"
msgstr "%1 Gb"

#: support_funcs.cpp:51
msgid "%1 MB"
msgstr "%1 Mb"

#: support_funcs.cpp:55
msgid "%1 KB"
msgstr "%1 Kb"

#: support_funcs.cpp:60
msgid "1 Byte"
msgstr "1 Byte"

#: support_funcs.cpp:61
msgid "%1 Bytes"
msgstr "%1 byte"

#: support_funcs.cpp:78
msgid "CABLE"
msgstr "KABEL"

#: talkwindow.cpp:39
msgid "Talk"
msgstr "Prata"

#: talkwindow.cpp:110
msgid " /ping  : ping  this user"
msgstr " /ping  : pinga den här användaren"

#: talkwindow.cpp:111
msgid " /whois : get info about this user"
msgstr " /whois : visa information om den här användaren"

#: talkwindow.cpp:112
msgid " /talk username: open a new chat window to username"
msgstr " /talk användarnamn: öppna ett nytt chattfönster till användare"

#: talkwindow.cpp:113
msgid " /part        : close this chat window"
msgstr " /part        : stäng det här chattfönstret"

#: talkwindow.cpp:121
msgid "/kill username : kill this user"
msgstr "/kill användarnamn : ta bort den här användaren permanent"

#: talkwindow.cpp:123
msgid "Anything else not starting with / is sent directly to the person"
msgstr "Allt annat som inte börjar med / skickas direkt till personen"

#: talkwindow.cpp:167
#, c-format
msgid "You said %1"
msgstr "Du sa %1"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Stefan Asserhäll"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "stefan.asserhall@telia.com"

#: uploadlistitem.cpp:45
msgid "Uploading"
msgstr "Laddar upp"

#: uploadlistitem.cpp:59
msgid "Remote Error"
msgstr "Fjärrfel"

#: uploadpanel.cpp:45
msgid "Upload file"
msgstr "Ladda upp fil"

#: uploadpanel.cpp:47
msgid "Read"
msgstr "Läs"

#: uploadpanel.cpp:66
msgid "Terminate upload"
msgstr "Avsluta uppladdning"

#: whoisdlg.cpp:34
msgid "General"
msgstr "Allmänt"

#: whoisdlg.cpp:36
msgid "Username:"
msgstr "Användarnamn:"

#: whoisdlg.cpp:39
msgid "Class:"
msgstr "Klass:"

#: whoisdlg.cpp:42
msgid "Logged on for:"
msgstr "Ansluten i:"

#: whoisdlg.cpp:45
msgid "Channels:"
msgstr "Kanaler:"

#: whoisdlg.cpp:48
msgid "Status:"
msgstr "Status:"

#: whoisdlg.cpp:53
msgid "Shared:"
msgstr "Utdelad:"

#: whoisdlg.cpp:56
msgid "Downloads:"
msgstr "Nedladdningar:"

#: whoisdlg.cpp:59
msgid "Uploads:"
msgstr "Uppladdningar:"

#: whoisdlg.cpp:65
msgid "Client:"
msgstr "Klient:"

#: whoisdlg.cpp:83
msgid "%1 seconds"
msgstr "%1 sekunder"

#: whoisdlg.cpp:94
msgid "Who is %1?"
msgstr "Vem är %1?"
