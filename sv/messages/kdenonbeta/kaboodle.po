# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-07-11 22:27+0200\n"
"PO-Revision-Date: 2001-07-12 22:21CEST\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@telia.com>\n"
"Language-Team: Svenska <sv@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: rc.cpp:1 rc.cpp:2
msgid "Kaboodle Toolbar"
msgstr "Kaboodle verktygsrad"

#: conf.cpp:38
msgid "Start playing automatically"
msgstr "Börja spela automatiskt"

#: conf.cpp:41
msgid "Quit after finished playing"
msgstr "Avsluta efter att ha spelat klart"

#: engine.cpp:79
msgid "There was an error communicating to the aRts daemon."
msgstr "Det uppstod ett fel vid kommunikation med aRts-demonen."

#: engine.cpp:79
msgid "Danger, Will Robinson!"
msgstr "Farligt, Will Robinson!"

#: engine.cpp:279
msgid "*.%1|%2 Files"
msgstr "*.%1|%2 filer"

#: engine.cpp:286
msgid "All Supported Files"
msgstr "Alla filer som stöds"

#: engine.cpp:287
msgid "All Files"
msgstr "Alla filer"

#: kaboodle_factory.cpp:67
msgid "Kaboodle"
msgstr "Kaboodle"

#: kaboodle_factory.cpp:67
msgid "The Lean KDE Media Player"
msgstr "Den lätta KDE mediaspelaren"

#: kaboodle_factory.cpp:68
msgid "Maintainer"
msgstr "Underhållare"

#: kaboodle_factory.cpp:69
msgid "Konqueror Embedding"
msgstr "Inbäddning i Konqueror"

#: kaboodle_factory.cpp:70
msgid "Original Noatun Developer"
msgstr "Noatun-originalutvecklare"

#: kaboodle_part.cpp:82
msgid "Play"
msgstr "Spela"

#: kaboodle_part.cpp:83
msgid "Pause"
msgstr "Paus"

#: kaboodle_part.cpp:100
msgid "&Play"
msgstr "S&pela"

#: kaboodle_part.cpp:101
msgid "&Pause"
msgstr "&Paus"

#: kaboodle_part.cpp:102
msgid "&Stop"
msgstr "&Stopp"

#: kaboodle_part.cpp:103
msgid "&Looping"
msgstr "&Repetera"

#: kaboodle_part.cpp:277
msgid "Playing %1 - %2"
msgstr "Spelar %1 - %2"

#: main.cpp:38
msgid "URL to Open."
msgstr "Webbadress att öppna."

#: main.cpp:40
msgid "Turn on Qt Debug output"
msgstr "Aktivera Qt-avlusningsutdata"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Stefan Asserhäll"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "stefan.asserhall@telia.com"

#: userinterface.cpp:97
msgid "Select a file to Play"
msgstr "Välj en fil att spela"
