# GPL Copyright (C) 2001 Johan Thelmén
#
msgid ""
msgstr ""
"Project-Id-Version: khelp help 1\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-07-28 00:12GMT\n"
"Last-Translator: Mattias Newzella <newzella@linux.nu>\n"
"Language-Team: Swedish <swedoc-request@lists.lysator.liu.se>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: index.docbook:6
msgid "KDE Help System User Manual"
msgstr "Handbok för KDE:s hjälpsystem"

#: index.docbook:10
msgid "KDE Help System"
msgstr "KDE:s hjälpsystem"

#: index.docbook:12
msgid ""
"The &kde; help system is designed to make accessing the common &UNIX; help "
"systems (<application>man</application> and <application>info</application>"
") simple, as well as the native &kde; documentation (&XML;)."
msgstr ""
"&kde;:s hjälpsystem är utformat för att enkelt komma åt &UNIX;:s "
"hjälpsystem (<application>man</application> och "
"<application>info</application>), lika väl som &kde;:s egna dokumentation "
"(XML)."

#: index.docbook:18
msgid ""
"All base &kde; applications come fully documented, thanks to the efforts of "
"the Documentation team. If you would like to help, please write to the "
"Documentation coordinator, Lauri Watts, at <email>lauri@kde.org</email> for "
"information. No experience is required, just enthusiasm and patience."
msgstr ""
"Alla &kde;:s basprogram är fullständigt dokumenterade tack vare "
"dokumentationsgruppens arbete. Om du vill hjälpa till så kan du skriva "
"till Lauri Watts <email>lauri@kde.org</email>, som är samordnare för "
"dokumenteringen. Du behöver inte ha någon erfarenhet, utan det räcker med "
"entusiasm och tålamod."

#: index.docbook:26
msgid ""
"If you would like to help translating &kde; Documentation to your native "
"language, the Translation coordinator is Thomas Diehl, "
"<email>thd@kde.org</email>, and he would also welcome the help. More "
"information, including the coordinators for each language team, can be found "
"on the <ulink url=\"http://i18n.kde.org\">Internationalization web "
"site</ulink>, and in the <link linkend=\"contact\">Contact</link>"
" section of this document."
msgstr ""
"Om du skulle vilja hjälpa till med att översätta &kde;:s dokumentation "
"till ditt modersmål så kan du kontakta Thomas Diehl "
"<email>thd@kde.org</email>, som är samordnare för översättningarna. Mer "
"information, inklusive samordnarna för översättningen av de olika "
"språken, kan du hitta på <ulink url=\"http://i18n.kde.org\">Webbplatsen "
"för internationalisering</ulink>, samt i <link "
"linkend=\"contact\">kontakt</link>-sektionen av det här dokumentet."

#: index.docbook:37
msgid "Installation"
msgstr "Installation"

#: index.docbook:39
msgid ""
"&khelpcenter; is an integral part of the &kde; Base installation, and is "
"installed with every copy of &kde;. It can be found in the kdebase package, "
"and is available from the <ulink url=\"ftp://ftp.kde.org/\">KDE &FTP; "
"site</ulink> or will be found in your operating system kdebase package."
msgstr ""
"&khelpcenter; är en integrerad del av &kde;:s basinstallation, och "
"installeras med varje kopia av &kde; från paketet kdebase."

#: index.docbook:51
msgid "Invoking Help"
msgstr "Starta hjälpcentralen"

#: index.docbook:53
msgid "&khelpcenter; can be called in several ways:"
msgstr "&khelpcenter; kan startas på flera sätt:"

#: index.docbook:57
msgid "From the <guimenu>Help</guimenu> menu"
msgstr "Från <guimenu>Hjälp</guimenu>menyn"

#: index.docbook:57
msgid ""
"The most common will probably be from within an application. Choose "
"<menuchoice><guimenu>Help</guimenu> "
"<guimenuitem>Contents</guimenuitem></menuchoice> to open that applications "
"help file, at the contents page."
msgstr ""
"Det vanligaste sättet är förmodligen från något program. Välj "
"<menuchoice><guimenu>Hjälp</guimenu> "
"<guimenuitem>Innehåll</guimenuitem></menuchoice> för att öppna "
"programmets hjälpfil. Du startar då på innehållssidan."

#: index.docbook:57
msgid "From the <guimenu>K</guimenu> menu"
msgstr "Från <guimenu>K</guimenu>-menyn"

#: index.docbook:57
msgid ""
"Choose the big <guiicon>K</guiicon> in your panel, and select "
"<guimenuitem>Help</guimenuitem> to open &khelpcenter;, starting at the "
"default welcome page."
msgstr ""
"Klicka på det stora <guiicon>K</guiicon>:et i panelen och välj "
"<guimenuitem>Hjälp</guimenuitem> för att öppna &khelpcenter;. Du startar "
"då på välkomstsidan."

#: index.docbook:57
msgid "From the panel"
msgstr "Från panelen"

#: index.docbook:57
msgid ""
"The &kicker; panel contains by default an icon to call &khelpcenter;. Again, "
"the default welcome page is displayed."
msgstr ""
"&kicker; innehåller som standard en ikon som heter &khelpcenter;. "
"Välkomstsidan kommer att visas igen."

#: index.docbook:57
msgid "From the command line"
msgstr "Från kommandoraden"

#: index.docbook:57
msgid ""
"&khelpcenter; may be started using a <abbrev>URL</abbrev>"
" to display a file. <abbrev>URL</abbrev>s have been added for "
"<command>info</command> and <command>man</command> pages also. You can use "
"them as follows:"
msgstr ""
"&khelpcenter; kan startas genom att använda en webbadress för att visa en "
"fil. Webbadresser har även lagts till för <command>info</command>- och "
"<command>man</command>-sidor. Du kan använda dem på följande sätt:"

#: index.docbook:57
msgid "An application help file"
msgstr "Hjälpfil till ett program"

#: index.docbook:57
msgid ""
"<command>khelpcenter</command> "
"<option>help:/<replaceable>kedit</replaceable></option>"
msgstr ""
"<command>khelpcenter</command> "
"<option>help:/<replaceable>kedit</replaceable></option>"

#: index.docbook:57
msgid "Opens the &kedit; help file, at the contents page."
msgstr "Öppnar hjälpcentralen för &kedit; på innehållssidan."

#: index.docbook:57
msgid "A local url"
msgstr "En lokal webbadress"

#: index.docbook:57
msgid ""
"<command>khelpcenter</command> <option>file:/ "
"<replaceable>usr/local/src/qt/html/index.html</replaceable></option>"
msgstr ""
"<command>khelpcenter</command> <option>file:/ "
"<replaceable>usr/local/src/qt/html/index.html</replaceable></option>"

#: index.docbook:57
msgid "A Man page"
msgstr "En manualsida"

#: index.docbook:57
msgid ""
"<command>khelpcenter</command> "
"<option>man:/<replaceable>strcpy</replaceable></option>"
msgstr ""
"<command>khelpcenter</command> "
"<option>man:/<replaceable>strcpy</replaceable></option>"

#: index.docbook:57
msgid "An Info page"
msgstr "En infosida"

#: index.docbook:57
msgid ""
"<command>khelpcenter</command> "
"<option>info:/<replaceable>gcc</replaceable></option>"
msgstr ""
"<command>khelpcenter</command> "
"<option>info:/<replaceable>gcc</replaceable></option>"

#: index.docbook:57
msgid ""
"Invoking <command>khelpcenter</command>"
" with no parameters opens the default welcome page."
msgstr ""
"Då <command>khelpcenter</command> körs utan några parametrar så öppnas "
"välkomstsidan."

#: index.docbook:158
msgid "The &khelpcenter; interface"
msgstr "Gränssnittet i &khelpcenter;"

#: index.docbook:160
msgid "The &khelpcenter; interface consists of two panes of information."
msgstr "Gränssnittet i &khelpcenter; består av två avdelningar med information."

#: index.docbook:164
msgid ""
"The toolbar and menus are explained further in section<xref "
"linkend=\"menu-and-toolbar-ref\"/>."
msgstr ""
"Verktygsfältet och menyerna förklaras närmare i sektionen <xref "
"linkend=\"menu-and-toolbar-ref\"/>."

#: index.docbook:169
msgid ""
"Documents contain their own navigation tools, enabling you to move either "
"sequentially through a document, using <guilabel>Next</guilabel>, "
"<guilabel>Previous</guilabel>, <guilabel>Up</guilabel> and "
"<guilabel>Home</guilabel> links, or to move around in a less structured "
"manner, using hyperlinks."
msgstr ""
"De flesta dokumenten innehåller sina egna navigeringsverktyg som gör det "
"möjligt att läsa dokumentets delar i ordning med länkarna "
"<guilabel>Nästa</guilabel> och <guilabel>Föregående</guilabel>, "
"alternativt att förflytta sig mindre strukturerat med hjälp av länkarna."

#: index.docbook:177
msgid ""
"Links can take you to other parts of the same document, or to a different "
"document, and you can use the <guiicon>Back</guiicon> (Left pointing arrow) "
"or <guiicon>Forward</guiicon>"
" (Right pointing arrow) icons on the toolbar to move through the documents "
"you have viewed in this session."
msgstr ""
"Länkar kan förflytta dig till andra delar av samma dokument eller till ett "
"annat dokument. Du kan använda ikonerna <guiicon>Bakåt</guiicon> (en pil "
"åt vänster) eller <guiicon>Framåt</guiicon> (en pil åt höger) i "
"verktygsraden för att förflytta dig genom de dokument som du har läst "
"tidigare."

#: index.docbook:185
msgid ""
"The two panes display the contents of the help system, and the help files "
"themselves, on the left and right respectively."
msgstr ""
"I den vänstra avdelningen visas hjälpsystemets innehåll, och i den högra "
"visas själva hjälpfilerna."

#: index.docbook:191
msgid "The <guilabel>Contents</guilabel> pane"
msgstr "Avdelningen <guilabel>Innehåll</guilabel>"

#: index.docbook:193
msgid ""
"The <guilabel>Contents</guilabel> pane in &khelpcenter; is displayed on the "
"left hand side of the window. As you might expect, you can move the splitter "
"bar, to make sure you can comfortably read the contents of either pane."
msgstr ""
"Avdelningen <guilabel>Innehåll</guilabel> i &khelpcenter; visas i den "
"vänstra sidan av fönstret. Som du kanske har gissat så kan du flytta på "
"avgränsningskanten så att du på ett bekvämt sätt kan läsa innehållet "
"i de båda avdelningarna."

#: index.docbook:200
msgid ""
"The <guilabel>Contents</guilabel> pane is further divided into three tabs, "
"one containing a <link linkend=\"contents-menu\">menu</link>"
" showing all the help information &khelpcenter; is aware of, and the next "
"enabling you to <link linkend=\"search\">Search</link> for specific "
"information, and the third contains the &kde; <link "
"linkend=\"kde-glossary\">glossary</link> of terms."
msgstr ""
"Avdelningen <guilabel>Innehåll</guilabel> är dessutom uppdelad i tre "
"flikar, en som innehåller en <link linkend=\"contents-menu\">meny</link> "
"som visar all hjälpinformation som &khelpcenter; känner till, samt en "
"annan flik som ger dig möjlighet att <link linkend=\"search\">Söka</link> "
"efter specifik information. En tredje flik innehåller <link "
"linkend=\"kde-glossary\">ordförklaringar</link> med &kde; och datatermer."

#: index.docbook:210
msgid "The <guilabel>Contents</guilabel> Menu"
msgstr "<guilabel>Innehålls</guilabel>menyn"

#: index.docbook:212
msgid "The <guilabel>Contents</guilabel> contains the following default entries:"
msgstr "<guilabel>Innehåll</guilabel> har följande poster som standard:"

#: index.docbook:217
msgid "Introduction"
msgstr "Introduktion"

#: index.docbook:217
msgid "Welcome to &kde; - an introduction to the K Desktop Environment."
msgstr "Välkommen till &kde; - en introduktion till K skrivbordsmiljö."

#: index.docbook:217
msgid "Introduction to &kde;"
msgstr "Introduktion till &kde;"

#: index.docbook:217
msgid ""
"The &kde; Quickstart guide. Contains a tour of the &kde; Interface and "
"specific help and tips on how to work smarter with &kde;."
msgstr ""
"Snabbstartsvägledningen i &kde; innehåller en rundvisning av gränssnitt, "
"specifik hjälp och tips för hur du kan arbeta smartare med &kde;."

#: index.docbook:217
msgid "&kde; User's manual"
msgstr "&kde;:s bruksanvisning"

#: index.docbook:217
msgid ""
"The &kde; User's manual is an in depth exploration of &kde;, including "
"installation, configuration and customization, and use."
msgstr ""
"Användarhandboken är en ingående genomgång av &kde;, som tar upp "
"inställningar, anpassning och användning."

#: index.docbook:217
msgid "Application manuals"
msgstr "Program manualer"

#: index.docbook:217
msgid ""
"Native &kde; application documentation. All &kde; applications have "
"documentation in &XML; format, which are converted to HTML when you view "
"them. This section lists all the &kde; applications with a brief description "
"and a link to the full application documentation."
msgstr ""
"Dokumentation för program som ingår i &kde;. Alla &kde;-program har "
"dokumentation i XML-format som konverteras till HTML när de visas. Den här "
"sektionen innehåller en lista med alla &kde;-program med kortfattade "
"beskrivningar och länkar till den fullständiga dokumentationen."

#: index.docbook:217
msgid ""
"The applications are displayed in a tree structure that echoes the default "
"structure of the <guimenu>K</guimenu> menu, making it easy to find the "
"application you are looking for."
msgstr ""
"Programmen visas i en trädstruktur som motsvarar den förinställda "
"strukturen i <guimenu>K</guimenu>-menyn, vilket gör det enkelt att hitta "
"programmet som du letar efter."

#: index.docbook:217
msgid "Unix manual pages"
msgstr "Manualsidor för Unix"

#: index.docbook:217
msgid ""
"&UNIX; man pages are the traditional on-line documentation format for unix "
"systems. Most programs on your system will have a man page. In addition, man "
"pages exist for programming functions and file formats."
msgstr ""
"&UNIX; manualsidor är det traditionella formatet på dokumentationen för "
"UNIX-system. De flesta program i ditt system har en manualsida. Dessutom "
"finns det manualsidor för programmeringsfunktioner och filformat."

#: index.docbook:217
msgid "Browse info pages"
msgstr "Bläddra bland informationssidor"

#: index.docbook:217
msgid ""
"TeXinfo documentation is used by many &GNU; applications, including "
"<command>gcc</command> (the C/C++ compiler), <command>emacs</command>, and "
"many others."
msgstr ""
"TeXinfo-dokumentation används av många &GNU;-program, bland annat "
"<application>gcc</application> (C/C++-kompilatorn), "
"<application>emacs</application> och många andra."

#: index.docbook:217
msgid "Tutorials"
msgstr "Snabbstartsguide"

#: index.docbook:217
msgid "Short, task based or informational tutorials."
msgstr "Enkel, steg för steg baserad vägledning."

#: index.docbook:217
msgid "The KDE FAQ"
msgstr "Vanliga frågor om KDE"

#: index.docbook:217
msgid "Frequently asked questions about &kde;, and their answers."
msgstr "Vanliga frågor om &kde; och svaren till dem."

#: index.docbook:217
msgid "&kde; on the web"
msgstr "&kde; på Internet"

#: index.docbook:217
msgid ""
"Links to &kde; on the web, both the official &kde; website, and other useful "
"sites."
msgstr ""
"Länkar till &kde; på webben, KDE:s officiella webbplats och andra "
"användbara platser."

#: index.docbook:217
msgid "Contact Information"
msgstr "Kontaktinformation"

#: index.docbook:217
msgid ""
"Information on how to contact &kde; developers, and how to join the &kde; "
"mailing lists."
msgstr ""
"Information om hur du kan kontakta &kde;-utvecklare, och hur du går med i "
"&kde;:s e-postlistor."

#: index.docbook:217
msgid "Supporting &kde;"
msgstr "Att stödja &kde;"

#: index.docbook:217
msgid "How to help, and how to get involved."
msgstr "Hur du kan hjälpa till, och hur du gör för att engagera dig."

#: index.docbook:335
msgid "The <guilabel>Search</guilabel> tab"
msgstr "Fliken <guilabel>Sök</guilabel>"

#: index.docbook:337
msgid ""
"Searching requires you have the <application>ht://Dig</application> "
"application installed. Information on installing and configuring the search "
"index is available in the document. Configuration of the search index is "
"performed in the &kcontrol;, by choosing "
"<menuchoice><guisubmenu>Help</guisubmenu><guimenuitem>Index</guimenuitem>"
"</menuchoice>, and detailed help is available from this module."
msgstr ""
"För att kunna söka måste du ha programmet "
"<application>ht://Dig</application> installerat. Information om hur du "
"installerar och ställer in sökindexet finns tillgänglig i dokumentet. "
"Inställningen av sökindexet görs i <application>&kcontrol;</application> "
"genom att välja <menuchoice><guisubmenu>Hjälp</guisubmenu> "
"<guimenuitem>Index</guimenuitem></menuchoice>, och detaljerad hjälp finns "
"tillgänglig i den modulen."

#: index.docbook:346
msgid ""
"For the purposes of this document, we'll assume you already have this set up "
"and configured."
msgstr ""
"För ändamålet med det här dokumentet så antar vi att du redan har "
"installerat och ställt in detta."

#: index.docbook:351
msgid ""
"Searching the help files is fairly intuitive, enter the word(s) you wish to "
"search for in the text box, choose your options (if any), and press "
"<guibutton>Search</guibutton>"
". The results display in the viewer pane to the right."
msgstr ""
"Att söka bland hjälpfilerna är ganska enkelt, skriv sökordet i "
"textrutan, välj eventuella alternativ och klicka på "
"<guibutton>Sök</guibutton>. Resultaten visas i avdelningen till höger."

#: index.docbook:358
msgid "The options available are:"
msgstr "Alternativen är:"

#: index.docbook:360
msgid "Method"
msgstr "Metod"

#: index.docbook:360
msgid ""
"Choose how to search for multiple words. If you choose "
"<guilabel>and</guilabel>"
", results are returned only if all your search terms are included in the "
"page. <guilabel>or</guilabel> returns results if <emphasis>any</emphasis>"
" of the search terms are found, and <guilabel>boolean</guilabel> lets you "
"search using a combination."
msgstr ""
"Väljer hur den ska söka när flera sökord angivits. "
"<guilabel>och</guilabel> visar resultat som innehåller alla sökorden du "
"angav på samma sida. <guilabel>eller</guilabel> returnerar träffar som "
"innehåller <emphasis>endera</emphasis> av sökorden. "
"<guilabel>boolesk</guilabel> tillåter dig att kombinera sökorden."

#: index.docbook:360
msgid ""
"Boolean syntax lets you use the operators <literal>AND</literal>, "
"<literal>OR</literal> and <literal>NOT</literal>"
" to create complex searches. Some examples:"
msgstr ""
"Boolesk syntax gör att du kan använda operatorer som "
"<literal>AND</literal>, <literal>OR</literal> och <literal>NOT</literal> "
"för att skapa komplicerade sökningar. Några exempel:"

#: index.docbook:360
msgid "cat and dog"
msgstr "katt and hund"

#: index.docbook:360
msgid ""
"Searches for pages which have both the words <userinput>cat</userinput> and "
"<userinput>dog</userinput> in them. Pages with only one or the other will "
"not be returned."
msgstr ""
"Söker efter sidor som har båda orden <userinput>katt</userinput> och "
"<userinput>hund</userinput> i sig. Sidor som bara ett av orden kommer inte "
"med i resultatet."

#: index.docbook:360
msgid "cat not dog"
msgstr "katt not hund"

#: index.docbook:360
msgid ""
"Searches for pages with <userinput>cat</userinput>"
" in them, but only returns the ones that don't have the word "
"<userinput>dog</userinput> in them."
msgstr ""
"Söker efter sidor som innehåller <userinput>katt</userinput> men de får "
"samtidigt inte heller innehålla ordet <userinput>hund</userinput>."

#: index.docbook:360
msgid "cat or (dog not nose)"
msgstr "katt or (hund not nos)"

#: index.docbook:360
msgid ""
"Searches for pages which contain <userinput>cat</userinput>, and for pages "
"which contain <userinput>dog</userinput> but don't contain "
"<userinput>nose</userinput>. Pages which contain both "
"<userinput>cat</userinput> and <userinput>nose</userinput> would be "
"returned, pages containing all three words would not."
msgstr ""
"Söker efter sidor som innehåller <userinput>katt</userinput>, samt för "
"sidor som innehåller <userinput>hund</userinput> men inte "
"<userinput>nos</userinput>. Sidor som innehåller både "
"<userinput>katt</userinput> och <userinput>nos</userinput> kommer med, men "
"sidorna som innehåller alla tre ord utelämnas."

#: index.docbook:360
msgid ""
"If your searches are not returning the results you expect, check carefully "
"you haven't excluded the wrong search term with an errand "
"<literal>NOT</literal> or a stray brace."
msgstr ""
"Returnerar dina söksvar inte vad du hade tänkt dig, bör du kontrollera "
"att du har rätt sökmetod och inte missat ett <literal>NOT</literal> eller "
"bortsprungen parentes."

#: index.docbook:360
msgid "Max. results"
msgstr "Max. resultat"

#: index.docbook:360
msgid "Determines the maximum number of results returned from your search."
msgstr "Begränsar antalet svar som din sökning kan returnera."

#: index.docbook:360
msgid "Format"
msgstr "Format"

#: index.docbook:360
msgid ""
"Decide if you want just a short link to the page containing your search "
"terms, or do you want a longer summary."
msgstr "Visar en kort eller längre del av svaren som sökningen gav."

#: index.docbook:360
msgid "Sort"
msgstr "Sortera"

#: index.docbook:360
msgid ""
"Sort the results in order of <guilabel>Score</guilabel> (how closely your "
"search terms were matched,) alphabetically by <guilabel>Title</guilabel> or "
"by <guilabel>Date</guilabel>. Selecting the <guilabel>Reverse "
"order</guilabel>"
" check box, naturally enough, reverses the sort order of the results."
msgstr ""
"Sorterar resultaten i <guilabel>Poäng</guilabel>"
" hur bra sidan passade till sökningen, alfabetisk ordning på "
"<guilabel>Titel</guilabel> eller i <guilabel>Datum</guilabel> ordning. "
"Väljer man <guilabel>Omvänd ordning</guilabel>, så visas svaren "
"naturligtvis i omvänd ordning."

#: index.docbook:360
msgid "Update index"
msgstr "Uppdatera index"

#: index.docbook:360
msgid ""
"Update the search index, to incorporate new documents, or if you think your "
"database is incomplete or damaged. This may take some time."
msgstr ""
"Uppdaterar sökindexet för att man ska kunna söka på nytillkomna "
"dokument, eller om databasen kanske är gammal, tom eller trasig. "
"Uppdateringar kan ibland ta lång tid."

#: index.docbook:431
msgid ""
"The <application>Man</application> and <application>Info</application> "
"sections"
msgstr "<application>Man</application> och <application>Info</application> sektioner"

#: index.docbook:434
msgid ""
"Man pages are the standard unix manual pages, and have been in use for many "
"years on many operating systems. They are extremely thorough, and are the "
"very best place to get information about most linux commands and "
"applications. When people say <quote>RTFM</quote>, the Manual they're "
"referring to is very often the man page."
msgstr ""
"Man sidor är standard UNIX manualsidor, dessa har används i många "
"operativsystem och år. De är mycket detaljerade och det bästa källan "
"till information för de flesta Linux-kommandon och program. När man säger "
"engelska <quote>RTFM</quote>, är det ofta dessa manualer man hänvisar till."

#: index.docbook:440
msgid ""
"The man pages are not perfect. They tend to be in depth, but also extremely "
"technical, often written by developers, and for developers. In some cases "
"this makes them somewhat unfriendly, if not downright impossible for many "
"users to understand. They are however, the best source of solid information "
"on most commandline applications, and very often the only source."
msgstr ""
"Man sidor är inte helt perfekta. De är mycket detaljerade men också "
"mycket tekniska, oftast skriva av utvecklare för utvecklare. I några fall "
"något ogästvänliga om inte omöjliga för flera att förstå."

#: index.docbook:447
msgid ""
"If you've ever wondered what the number is when people write things like "
"man(1) it means which section of the manual the item is in. You'll see "
"&khelpcenter; uses the numbers to divide the very many man pages into their "
"own sections, making it easier for you to find the information you're "
"looking for, if you're just browsing."
msgstr ""
"Har du någon gång funderat över vad det är för nummer som skrivs vid "
"manualer som tex man(1). Numret talar om vilken sektion manualen tillhör. "
"&khelpcenter; använder också dessa sektioner för att gruppera alla "
"manualer så att det ska bli enklare att hitta den information du letar "
"efter."

#: index.docbook:453
msgid ""
"Also available are the Info pages, intended to be a replacement for the man "
"pages. The maintainer of some applications no longer update the man pages, "
"so if there is both a man page and an info page available, the info page is "
"probably the most recent. Most applications have one or the other though. If "
"the application you are looking for help on is a &GNU; utility, you will "
"most likely find it has an info page, not a man page."
msgstr ""
"Tillgängliga är även alla info sidor som var tänktes vara en ersättning "
"till man sidor. Vissa utvecklare uppdaterar inte längre man sidorna och "
"finns det både en man och info sida är det troligt att info sidan är den "
"mest aktuella. Vanligast är att man har en man eller en info sida. Många "
"&GNU;-program har oftast en info sida."

#: index.docbook:462
msgid "Navigating inside the <application>Info</application> pages"
msgstr "Navigera bland <application>Info</application> sidor"

#: index.docbook:464
msgid ""
"Info documents are arranged hierarchically with each page called a node. All "
"info documents have a <guilabel>Top</guilabel> node, <abbrev>i.e.</abbrev> "
"the opening page. You can return to the <guilabel>Top</guilabel> of an info "
"document by pressing <guilabel>Top</guilabel>."
msgstr ""
"Info sidorna är arrangerade i en hierarki där varje sida kallas en nod. "
"Alla info dokument har en <guilabel>topp</guilabel>nod, "
"<abbrev>d.v.s.</abbrev> första sida. Man kan återgå till "
"<guilabel>topp</guilabel>en av ett info dokument genom att trycka på "
"<guilabel>topp</guilabel>."

#: index.docbook:470
msgid ""
"<guibutton>Prev</guibutton> &amp; <guibutton>Next</guibutton> are used to "
"move to the previous/next page at the current level of the hierarchy."
msgstr ""
"<guibutton>Föregående</guibutton> &amp; <guibutton>Nästa</guibutton> "
"används till att förflytta sig föregående/nästa nod på den aktuell "
"nivå i hierarkin."

#: index.docbook:473
msgid ""
"Clicking on a menu item within a document moves you to a lower level in the "
"hierarchy. You may move up the hierarchy by pressing "
"<guibutton>Up</guibutton>."
msgstr ""
"Genom att klicka på en länk i dokumentet förflyttas man till en lägre "
"nivå i hierarkin. För att komma upp ur hierarkin trycker man "
"<guibutton>Upp</guibutton>."

#: index.docbook:477
msgid ""
"Man is treated similarly to info, with the section index being the Top node "
"and each man page on the level below. Man entries are one page long."
msgstr ""
"Man fungerar nästan som info, med ett index som toppnod och varje man sida "
"på nivån under. Man sidor har bara en lång sida."

#: index.docbook:484
msgid "The &kde; glossary"
msgstr "&kde;:s ordförklaringar"

#: index.docbook:486
msgid ""
"The glossary provides a quick reference point, where you can look up the "
"definitions of words that may be unfamiliar to you. These range from &kde; "
"specific applications and technologies, through to general &UNIX; computing "
"terms."
msgstr ""
"Ordförklaringar tillför en snabbreferens där man kan slå upp ord och "
"beteckningar som du kanske inte känner till. De är en hel skala av &kde; "
"specifika program och teknologier till vanliga &UNIX;-termer."

#: index.docbook:493
msgid ""
"In the left hand pane you will see a tree view, with two choices: "
"<guilabel>Alphabetically</guilabel> or <guilabel>By topic</guilabel>. Both "
"contain the same entries, sorted differently, to allow you to quickly find "
"the item of interest."
msgstr ""
"I den vänstra avdelningen finns det ett trädvy med två val: "
"<guilabel>Alfabetiskt</guilabel> och <guilabel>Efter ämne</guilabel>"
". Båda har samma innehåll, men är sorterade olika, för att du enkelt ska "
"hitta det du söker."

#: index.docbook:500
msgid ""
"Navigate down the tree views to the left, and items you select will be "
"displayed on the right."
msgstr ""
"Man väljer i trädvyn till vänster och innehållet man valt visas till "
"höger."

#: index.docbook:508
msgid "The menus and toolbar"
msgstr "Menyer och verktygsrad."

#: index.docbook:510
msgid ""
"&khelpcenter; has a very minimal interface, allowing you to concentrate on "
"getting help rather than learning how to use the help browser."
msgstr ""
"&khelpcenter; har inte så många funktioner, vilket gör att man kan "
"koncentrera sig mer på att leta efter hjälpen än att lära sig hur man "
"letar."

#: index.docbook:515
msgid "The icons available to you in the toolbar are as follows:"
msgstr "Följande ikoner finns på verktygsmenyn:"

#: index.docbook:519
msgid "Toolbar Icons"
msgstr "Verktygsikoner"

#: index.docbook:519
msgid "Print"
msgstr "Skriv ut ram"

#: index.docbook:519 index.docbook:565
msgid "Print the contents of the currently visible page."
msgstr "Skriv ut den aktuella sidan."

#: index.docbook:519
msgid "Find"
msgstr "Sök"

#: index.docbook:519
msgid "Find a word or words within the currently visible page."
msgstr "Sök på denna sida."

#: index.docbook:519
msgid "Increase Font"
msgstr "Öka teckenstorlek"

#: index.docbook:519
msgid "Increase the size of the text in the viewer pane."
msgstr "Ökar storleken på texten i avdelningen till höger."

#: index.docbook:519
msgid "Decrease Font"
msgstr "Minska teckenstorlek"

#: index.docbook:519
msgid ""
"Decrease the size of the text in the viewer pane. This icon is only enabled "
"if you have previously enlarged the text."
msgstr ""
"Minskar storleken på texten. Detta är bara möjligt om du innan har "
"förstorat texten."

#: index.docbook:561
msgid "The menus contain the following entries:"
msgstr "Menyn innehåller följande:"

#: index.docbook:565
msgid "File"
msgstr "Arkiv"

#: index.docbook:565
msgid "<guimenu><accel>F</accel>ile</guimenu> <guimenuitem>Print Frame</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>rkiv</guimenu> <guimenuitem>Skriv ut "
"ram</guimenuitem>"

#: index.docbook:565
msgid ""
"<shortcut> <keycombo action=\"simul\">&Ctrl;<keycap>W</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>C</accel>lose</guimenuitem>"
msgstr ""
"<shortcut> <keycombo action=\"simul\">&Ctrl;<keycap>W</keycap></keycombo> "
"</shortcut> <guimenu><accel>A</accel>rkiv</guimenu> "
"<guimenuitem>Stän<accel>g</accel></guimenuitem>"

#: index.docbook:565
msgid "Close and exit &khelpcenter;"
msgstr "Stäng och avsluta &khelpcenter;"

#: index.docbook:565
msgid "Edit"
msgstr "Redigera"

#: index.docbook:565
msgid ""
"<shortcut> <keycombo "
"action=\"simul\">&Ctrl;<keycap>A</keycap></keycombo></shortcut> "
"<guimenu><accel>E</accel>dit</guimenu> <guimenuitem>Select "
"<accel>A</accel>ll</guimenuitem>"
msgstr ""
"<shortcut> <keycombo "
"action=\"simul\">&Ctrl;<keycap>A</keycap></keycombo></shortcut> "
"<guimenu><accel>R</accel>edigera</guimenu> "
"<guimenuitem><accel>M</accel>arkera alla</guimenuitem>"

#: index.docbook:565
msgid "Select all the text in the current page."
msgstr "Markera all text i aktuell avdelning."

#: index.docbook:565
msgid ""
"<shortcut> <keycombo "
"action=\"simul\">&Ctrl;<keycap>F</keycap></keycombo></shortcut> "
"<guimenu><accel>E</accel>dit</guimenu> "
"<guimenuitem><accel>F</accel>ind</guimenuitem>"
msgstr ""
"<shortcut> <keycombo "
"action=\"simul\">&Ctrl;<keycap>F</keycap></keycombo></shortcut> "
"<guimenu><accel>R</accel>edigera</guimenu> "
"<guimenuitem><accel>S</accel>ök</guimenuitem>"

#: index.docbook:565
msgid "Find a word or words in the currently visible page."
msgstr "Sök på aktuell sida."

#: index.docbook:565
msgid "View"
msgstr "Visa"

#: index.docbook:565
msgid ""
"<guimenu><accel>V</accel>iew</guimenu> <guimenuitem>View Document "
"Source</guimenuitem>"
msgstr ""
"<guimenu><accel>V</accel>isa</guimenu> <guimenuitem>Visa dokumentets "
"källa</guimenuitem>"

#: index.docbook:565
msgid ""
"View the <acronym>HTML</acronym> source of the page you are currently "
"viewing."
msgstr "Visar <acronym>HTML</acronym>-källan från aktuell sida."

#: index.docbook:565
msgid ""
"<guimenu><accel>V</accel>iew</guimenu> <guisubmenu>Set "
"<accel>E</accel>ncoding</guisubmenu>"
msgstr ""
"<guimenu><accel>V</accel>isa</guimenu> <guisubmenu>Ang<accel>e</accel> "
"kodning</guisubmenu>"

#: index.docbook:565
msgid ""
"Change the encoding of the current page. Normally, the default setting of "
"<guimenuitem>Auto</guimenuitem> should be sufficient, but if you are having "
"problems viewing pages written in languages other than English, you may need "
"to choose a specific encoding in this menu."
msgstr ""
"Ändrar teckenöversättningen för aktuell sida. I normala fall behöver "
"man inte ändra från <guimenuitem>Auto</guimenuitem>"
" men om du får problem med något språk så kan du prova att ändra här."

#: index.docbook:682
msgid "Credits and Licenses"
msgstr "Tack till och licens"

#: index.docbook:684
msgid "&khelpcenter;"
msgstr "&khelpcenter;"

#: index.docbook:686
msgid "Originally developed by Matthias Elter <email>me@kde.org</email>"
msgstr "Ursprungligen utvecklad av by Matthias Elter <email>me@kde.org</email>"

#: index.docbook:687
msgid "Current maintainer Matthias Hoelzer-Kluepfel <email>mhk@kde.org</email>"
msgstr "Nuvarande underhållare Matthias Hoelzer-Kluepfel <email>mhk@kde.org</email>"
