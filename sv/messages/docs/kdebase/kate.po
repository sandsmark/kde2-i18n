# Swedish translation of docs KDE base, kate.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Johan Thelmén, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: KDE Base/kate\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-07-27 14:33GMT\n"
"Last-Translator: Mattias Newzella <newzella@linux.nu>\n"
"Language-Team: Swedish <swedoc-request@lists.lysator.liu.se>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: index.docbook:13
msgid "The &kate; Handbook"
msgstr "Handbok &kate;"

#: index.docbook:17
msgid ""
"<firstname>Seth</firstname> <surname>Rothberg</surname> <affiliation> "
"<address><email>sethmr@bellatlantic.net</email></address> </affiliation>"
msgstr ""
"<firstname>Seth</firstname> <surname>Rothberg</surname> "
"<affiliation><address><email>sethmr@bellatlantic.net</email></address>"
"</affiliation>"

#: index.docbook:26
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"> <firstname>Johan</firstname> "
"<surname>Thelmén</surname> "
"<affiliation><address><email>jth@home.se</email></address></affiliation> "
"<contrib>Översättare</contrib></othercredit>"

#: index.docbook:31
msgid "Seth M. Rothberg"
msgstr "Seth M. Rothberg"

#: index.docbook:49
msgid "&kate; is a programmer's text editor for KDE 2.2 and above."
msgstr "&kate; är en editor för programmerare. Den kräver KDE 2.2 eller senare."

#: index.docbook:51
msgid "This handbook documents &kate; Version 1.0"
msgstr "Denna handbok gäller för &kate; version 1.0"

#: index.docbook:59
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:60
msgid "kdebase"
msgstr "kdebase"

#: index.docbook:61
msgid "Kate"
msgstr "Kate"

#: index.docbook:62
msgid "text"
msgstr "text"

#: index.docbook:63
msgid "editor"
msgstr "editor"

#: index.docbook:64
msgid "programmer"
msgstr "programmerare"

#: index.docbook:65
msgid "programming"
msgstr "programmering"

#: index.docbook:66
msgid "projects"
msgstr "projekt"

#: index.docbook:67
msgid "<keyword>MDI</keyword>"
msgstr "<keyword>MDI</keyword>"

#: index.docbook:68
msgid "Multi"
msgstr "Multi"

#: index.docbook:69
msgid "Document"
msgstr "Dokument"

#: index.docbook:70
msgid "Interface"
msgstr "Gränssnitt"

#: index.docbook:71
msgid "terminial"
msgstr "terminal"

#: index.docbook:72
msgid "console"
msgstr "konsoll"

#: index.docbook:78
msgid "Introduction"
msgstr "Introduktion"

#: index.docbook:80
msgid ""
"Welcome to &kate;, a programmer's text editor for &kde; version 2.2 and "
"above. Some of &kate;'s many features include configurable syntax "
"highlighting for languages ranging from C and C++ to <acronym>HTML</acronym>"
" to bash scripts, the ability to create and maintain projects, a multiple "
"document interface (<acronym>MDI</acronym>), and a self-contained terminal "
"emulator."
msgstr ""
"Välkommen till &kate;, en texteditor för programmerare till &kde; 2.2 "
"eller senare. Några av finesserna med &kate; är inställbar "
"syntaxbelysning för programmeringsspråk som C, C++, "
"<acronym>HTML</acronym>"
" och bashskript, möjlighet att skapa och underhålla projekt, hantera flera "
"öppna dokument (multiple document interface) (<acronym>MDI</acronym>) och "
"en inbyggd terminalemulator."

#: index.docbook:89
msgid ""
"But &kate; is more than a programmer's editor. It's ability to open several "
"files at once makes it ideal for editing &UNIX;'s many configuration files. "
"This document was written in &kate;."
msgstr ""
"Men &kate; är mer än en texteditor för programmerare. Möjligheten att "
"öppna flera filer samtidigt gör den lämplig för redigering av "
"inställningsfiler i tex &UNIX;-system.  Denna text skrevs ursprungligen med "
"&kate;."

#: index.docbook:97
msgid "The Fundamentals"
msgstr "Grunderna"

#: index.docbook:99
msgid ""
"If you have ever used a text editor you will have no problem using &kate;. "
"In the next two sections, <link linkend=\"starting-kate\">Starting &kate; "
"</link> and in <link linkend=\"working-with-kate\">Working with "
"&kate;</link> we'll show you everything you need to get up and running "
"quickly."
msgstr ""
"Har du använt en ordbehandlare tidigare kommer du inte få några problem "
"med &kate;. I nästa två avsnitt, <link linkend=\"starting-kate\">Starta "
"&kate; </link> och <link linkend=\"working-with-kate\">Arbeta med "
"&kate;</link> kommer vi visa dig allt du behöver för att snabbt komma "
"igång."

#: index.docbook:108
msgid "Starting &kate;"
msgstr "Starta &kate;"

#: index.docbook:110
msgid ""
"You can start &kate; from the <guimenu>K menu</guimenu> or from the command "
"line."
msgstr ""
"Du kan starta &kate; från <guimenu>KDE:s programmeny</guimenu> eller från "
"kommandoraden."

#: index.docbook:116
msgid "From the Menu"
msgstr "Från menyn"

#: index.docbook:117
msgid ""
"Open the <acronym>KDE</acronym> program menu by clicking on the <guiicon>"
"big K</guiicon> icon on the toolbar at the bottom left of your screen. This "
"will raise the <guimenu>program menu</guimenu>"
". Move your cursor up the menu to the <guimenu>Editors</guimenu> menu item. "
"A list of available editors will appear. Choose "
"<guimenuitem>&kate;</guimenuitem>."
msgstr ""
"Öppna <acronym>KDE</acronym>:s programmeny genom att klicka på den "
"<guiicon>stora K</guiicon>-ikonen längst ner till vänster på skärmen. "
"Då kommer <guimenu>programmenyn</guimenu> att dyka upp. Flytta pekaren "
"uppåt till undermenyn <guimenu>Editorer</guimenu>. Det kommer då fram en "
"lista med editorer. Välj sedan <guimenuitem>&kate;</guimenuitem>."

#: index.docbook:126
msgid ""
"Unless you configure &kate; not to, it will load the last files you edited. "
"See <link linkend=\"configure\">Configuring &kate;</link> to learn how to "
"toggle this feature on and off."
msgstr ""
"De filer som var öppna när &kate; avslutades kommer att automatiskt "
"öppnas om du inte har ändrat den inställningen. Se <link "
"linkend=\"configure\">Anpassa &kate;</link> för att se hur man använder "
"denna funktion."

#: index.docbook:135
msgid "From the Command Line"
msgstr "Från kommandoraden"

#: index.docbook:137
msgid ""
"You can start &kate; by typing its name on the command line. If you give it "
"a file name, as in the example below, it will open or create that file."
msgstr ""
"Du kan starta &kate; genom att skriva namnet på kommandoraden. Lägger man "
"till ett filnamn som argument som i exemplet nedan kommer den filen att "
"öppnas eller skapas."

#: index.docbook:143
msgid ""
"<prompt>%</prompt><userinput><command>kate</command><option><replaceable>"
"myfile.txt</replaceable></option></userinput>"
msgstr ""
"<prompt>%</prompt><userinput><command>kate</command> "
"<option><replaceable>minfil.txt</replaceable></option></userinput>"

#: index.docbook:149
msgid ""
"If you have an active connection, and permission, you can take advantage of "
"&kde;'s network transparency to open files on the internet."
msgstr ""
"Har du en aktiv Internetanslutning och rättigheter, kan du utnyttja det "
"transparenta nätverksstödet i &kde; för att öppna filer på Internet."

#: index.docbook:154
msgid ""
"<prompt>%</prompt><userinput><command>kate</command><option><replaceable>"
"ftp://ftp.kde.org/pub/kde/README_FIRST</replaceable></option></userinput>"
msgstr ""
"<prompt>%</prompt><userinput><command>kate</command> "
"<option><replaceable>ftp://ftp.kde.org/pub/kde/README_FIRST</replaceable>"
"</option></userinput>"

#: index.docbook:161
msgid "Command Line Options"
msgstr "Argument på kommandoraden"

#: index.docbook:162
msgid "&kate; accepts following command line options:"
msgstr "&kate; godtar följande argument på kommandoraden:"

#: index.docbook:164
msgid "<command>kate</command> <option>--help</option>"
msgstr "<command>kate</command> <option>--help</option>"

#: index.docbook:164
msgid "This lists the most basic options available at the command line."
msgstr "Visar de vanligaste argumenten."

#: index.docbook:164
msgid "<command>kate</command> <option>--help-qt</option>"
msgstr "<command>kate</command> <option>--help-qt</option>"

#: index.docbook:164
msgid ""
"This lists the options available for changing the way &kate; interacts with "
"&Qt;."
msgstr "Ändrar hur &kate; fungerar med &Qt;."

#: index.docbook:164
msgid "<command>kate</command> <option>--help-kde</option>"
msgstr "<command>kate</command> <option>--help-kde</option>"

#: index.docbook:164
msgid ""
"This lists the options available for changing the way &kate; interacts with "
"&kde;."
msgstr "Ändrar hur &kate; fungerar med &kde;."

#: index.docbook:164
msgid "<command>kate</command> <option>--help-all</option>"
msgstr "<command>kate</command> <option>--help-all</option>"

#: index.docbook:164
msgid "This lists all of the command line options."
msgstr "Alla argument för kommandoraden."

#: index.docbook:164
msgid "<command>kate</command> <option>--author</option>"
msgstr "<command>kate</command> <option>--author</option>"

#: index.docbook:164
msgid "Lists &kate;'s authors in the terminal window."
msgstr "Visar upphovsmännen till &kate; i terminalfönstret."

#: index.docbook:164
msgid "<command>kate</command> <option>--version</option>"
msgstr "<command>kate</command> <option>--version</option>"

#: index.docbook:164
msgid ""
"Lists version information for &Qt;, &kde;, and &kate;. Also available "
"through <userinput><command>kate</command> <option>-V</option></userinput>"
msgstr ""
"Visar versioner av &Qt;, &kde; och &kate;. Samma gäller "
"<userinput><command>kate</command> <option>-V</option></userinput>"

#: index.docbook:164
msgid "<command>kate</command> <option>--licence</option>"
msgstr "<command>kate</command> <option>--licence</option>"

#: index.docbook:164
msgid "Shows license information."
msgstr "Visar licensen."

#: index.docbook:241
msgid "Drag and Drop"
msgstr "Drag och släpp"

#: index.docbook:242
msgid ""
"&kate; uses the &kde; Drag and Drop protocol. Files may be dragged and "
"dropped onto &kate; from the Desktop, &konqueror; or some remote ftp site "
"opened in one of &konqueror;'s windows."
msgstr ""
"&kate; använder &kde;:s protokoll för drag och släpp. Man kan dra och "
"släppa filer på &kate; från skrivbordet, &konqueror; eller någon "
"ftp-plats som är öppnad i något av &konqueror;s fönster."

#: index.docbook:251
msgid "Working with &kate;"
msgstr "Arbeta med &kate;"

#: index.docbook:252
msgid ""
"<link linkend=\"quick-start\">Quick Start</link>"
" will show you how to toggle four simple options that will let you configure "
"some of &kate;'s more powerful features right away. <link "
"linkend=\"keystroke-commands\"> Keystroke Commands</link> lays out some of "
"the default keystroke shortcuts for those who can't or don't want to use a "
"mouse."
msgstr ""
"<link linkend=\"quick-start\">Snabbstart</link> visar dig hur enkelt du kan "
"göra inställningar med fyra enkla alternativ ställa in flera kraftfulla "
"specialiteter i &kate;.<link linkend=\"keystroke-commands\"> "
"Snabbtangenter</link> visar några förvalda snabbtangentgenvägar för de "
"som inte kan eller vill använda musen."

#: index.docbook:261
msgid "Quick Start"
msgstr "Snabbstart"

#: index.docbook:263
msgid ""
"This section will describe some of the items on the "
"<guimenu>Settings</guimenu>"
" menu so that you can quickly configure &kate; to work the way you want it."
msgstr ""
"Avsnittet beskriver menyvalen i <guimenu>Inställningsmenyn</guimenu>. Då "
"kan du snabbt göra inställningarna så att &kate; fungerar precis som du "
"vill."

#: index.docbook:269
msgid ""
"When you start &kate; for the first time you'll see two windows with white "
"backgrounds. Above the two windows is a toolbar with the usual labeled "
"icons. And above that, a menu bar."
msgstr ""
"När du startat &kate; för första gången ser man två fönster. Ovanför "
"finns en verktygsrad med standardikoner. Samt menyraden ovanför denna."

#: index.docbook:274
msgid ""
"The left-hand window is a dockable side bar. It combines the Filelist and "
"Fileselector windows. Switch between the two by clicking on the tabs at the "
"top of the window."
msgstr ""
"Fönstret till vänster är ett verktygsfält som är flyttbart. Det "
"kombinerar fillistan och filväljaren i ett fönster. För att välja mellan "
"de två klickar man på flikarna överst i fönstret."

#: index.docbook:279
msgid ""
"If you've started &kate; with a file, the right-hand window will show the "
"file you are editing and the Filelist on the side bar will show the name of "
"the file. Use the Fileselector window to open files."
msgstr ""
"Har du startat &kate; med en fil, kommer det högra fönstret visa denna fil "
"för redigering och i fillistan kommer filnamnet att visas. Via "
"filväljarfönstret kan man öppna fler filer."

#: index.docbook:285
msgid ""
"You can toggle the Filelist and Fileselector window on and off in "
"<guimenu>Settings</guimenu> menu. This menu offers you your first glimpse "
"into &kate;'s power and flexibility. In this section we'll look at four "
"items:"
msgstr ""
"Fillist- och filväljarfönstret kan stängas och öppnas via menyn "
"<guimenu>Inställningar</guimenu>. Denna meny kommer visa dig kraften och "
"flexibiliteten i &kate;. Nu kommer vi att titta på fyra menyval."

#: index.docbook:292
msgid "Show <accel>T</accel>oolbar"
msgstr "Visa <accel>V</accel>erktygsrad"

#: index.docbook:292
msgid "Toggles the toolbar on and off."
msgstr "Visar eller gömmer verktygsraden. "

#: index.docbook:292
msgid "Show Filelist"
msgstr "Visa fillistan"

#: index.docbook:292
msgid ""
"Toggles the Filelist on and off. If the Filelist/Fileselector window is not "
"open, &kate; launches the side bar as a separate, undocked, window. To dock "
"the window grab the two thin parallel lines above the tabs by clicking on "
"them with your &LMB; and holding the button down. Drag the the window into "
"&kate;'s editing window and release the &LMB; when you have positioned the "
"Filelist/Fileselector window as you prefer."
msgstr ""
"Visar eller gömmer fillistan. Då fönstret för fillistan eller "
"filväljaren inte är öppet kommer &kate; öppna verktygsfönstret som ett "
"separat odockat fönster. För att docka fönstret klicka på de två övre "
"tunna parallella linjerna och genom att hålla ner vänster musknapp. Drag "
"till editor fönstret i &kate; och släpp vänster musknapp när du tycker "
"att det ligger bra till."

#: index.docbook:292
msgid ""
"If you have grabbed the two parallel lines successfully your mouse pointer "
"will turn into two crossed arrows as you drag."
msgstr ""
"När du tagit tag i de parallella linjerna riktigt kommer muspekaren att "
"bytas mot ett kors av fyra pilar medan du drar verktygsfältet."

#: index.docbook:292
msgid "Show Fileselector"
msgstr "Visa filväljare"

#: index.docbook:292
msgid ""
"Toggles the Fileselector on and off. This menu item is the same as "
"<guimenuitem>Show Filelist</guimenuitem>"
" with one difference. Toggling it on launches the window with the "
"Fileselector on top."
msgstr ""
"Visar eller gömmer filväljaren. Detta menyval är samma som "
"<guimenuitem>Visa fillista</guimenuitem> förutom att filväljarfliken är "
"överst."

#: index.docbook:292
msgid "Show <accel>C</accel>onsole"
msgstr "Visa <accel>K</accel>onsoll"

#: index.docbook:292
msgid ""
"Toggles a console emulator on and off at the bottom of &kate;'s window. In "
"other words, it gives you a command line within the application."
msgstr ""
"Visar eller gömmer en konsoll i  nedre delen av &kate;. Med andra ord får "
"du en kommandorad inbäddad i programmet."

#: index.docbook:356
msgid "Keystroke Commands"
msgstr "Snabbtangentkommandon"

#: index.docbook:358
msgid ""
"Many of &kate;'s keystroke commands (shortcuts) are configurable by way of "
"the <link linkend=\"settings\">Settings</link> menu. By default &kate; "
"honors the following key bindings."
msgstr ""
"Många av snabbtangenterna i &kate; är konfigurerbara i menyn <link "
"linkend=\"settings\">Inställningar</link>. Vanligtvis har &kate; följande "
"snabbtangenter."

#: index.docbook:366
msgid "Insert"
msgstr "Infoga (Insert)"

#: index.docbook:366
msgid ""
"Toggle between Insert and Overwrite mode. When in insert mode the editor "
"will add any typed characters to the text and push any previously typed data "
"to the right of the text cursor. Overwrite mode causes the entry of each "
"character to eliminate the character immediately to the right of the text "
"cursor."
msgstr ""
"Skiftar mellan lägena Infoga och Skriv över. I läget Infoga kommer "
"skrivna tecken infogas och skjuta efterföljande text till höger om "
"markören. När man skriver ett tecken i läget Skriv över kommer tecknet "
"alldeles till höger om markören ersättas av det du skrev."

#: index.docbook:366
msgid "Left Arrow"
msgstr "Pil vänster"

#: index.docbook:366
msgid "Move the cursor one character to the left"
msgstr "Flyttar markören ett tecken till vänster"

#: index.docbook:366
msgid "Right Arrow"
msgstr "Pil höger"

#: index.docbook:366
msgid "Move the cursor one character to the right"
msgstr "Flyttar markören ett tecken till höger"

#: index.docbook:366
msgid "Up Arrow"
msgstr "Pil upp"

#: index.docbook:366
msgid "Move the cursor up one line"
msgstr "Flyttar markören en rad upp"

#: index.docbook:366
msgid "Down Arrow"
msgstr "Pil ner"

#: index.docbook:366
msgid "Move the cursor down one line"
msgstr "Flyttar markören en rad ner"

#: index.docbook:366
msgid "Page Up"
msgstr "Sida upp (Page Up)"

#: index.docbook:366
msgid "Move the cursor up one page"
msgstr "Flyttar markören upp en sida"

#: index.docbook:366
msgid "Page Down"
msgstr "Sida ner (Page Down)"

#: index.docbook:366
msgid "Move the cursor down one page"
msgstr "Flyttar markören ned en sida"

#: index.docbook:366
msgid "Backspace"
msgstr "Backsteg (Backspace)"

#: index.docbook:366
msgid "Delete the character to the left of the cursor"
msgstr "Tar bort tecknet till vänster om markören"

#: index.docbook:366
msgid "Home"
msgstr "Hem (Home)"

#: index.docbook:366
msgid "Move the cursor to the beginning of the line"
msgstr "Flyttar markören till början av raden"

#: index.docbook:366
msgid "<keycap>End</keycap>"
msgstr "<keycap>End</keycap>"

#: index.docbook:366
msgid "Move the cursor to the end of the line"
msgstr "Flyttar markören till slutet på raden"

#: index.docbook:366
msgid "Delete"
msgstr "Ta bort (Delete)"

#: index.docbook:366
msgid "Delete the character to the right of the cursor (or any selected text)"
msgstr "Tar bort tecknet till höger om markören eller markerad text."

#: index.docbook:366
msgid "<keycap>Shift</keycap><keycap>Left Arrow</keycap>"
msgstr "<keycap>Skift</keycap><keycap>Pil vänster</keycap>"

#: index.docbook:366
msgid "Mark text one character to the left"
msgstr "Markerar ett tecken till vänster"

#: index.docbook:366
msgid "<keycap>Shift</keycap><keycap>Right Arrow</keycap>"
msgstr "<keycap>Skift</keycap><keycap>Pil höger</keycap>"

#: index.docbook:366
msgid "Mark text one character to the right"
msgstr "Markerar ett tecken till höger"

#: index.docbook:366
msgid "<keycap>F1</keycap>"
msgstr "<keycap>F1</keycap>"

#: index.docbook:366
msgid "Help"
msgstr "Hjälp"

#: index.docbook:366
msgid "<keycap>Shift</keycap><keycap>F1</keycap>"
msgstr "<keycap>Skift</keycap><keycap>F1</keycap>"

#: index.docbook:366
msgid "What's this?"
msgstr "Vad är det här? "

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>F</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>F</keycap>"

#: index.docbook:366
msgid "Find"
msgstr "Sök"

#: index.docbook:366
msgid "<keycap>F3</keycap>"
msgstr "<keycap>F3</keycap>"

#: index.docbook:366
msgid "Find again"
msgstr "Sök igen"

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>B</keycap>"
msgstr "<keycap> Ctrl</keycap><keycap>B</keycap>"

#: index.docbook:366
msgid "Set a Bookmark"
msgstr "Skapar ett bokmärke"

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>C</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>C</keycap>"

#: index.docbook:366
msgid "Copy the marked text to the clipboard."
msgstr "Kopierar den markerade texten till klippbordet."

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>N</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>N</keycap>"

#: index.docbook:366
msgid "<link linkend=\"new\">New</link> document"
msgstr "<link linkend=\"new\">Nytt</link> dokument"

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>P</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>P</keycap>"

#: index.docbook:366
msgid "Print"
msgstr "Skriv ut"

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>Q</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>Q</keycap>"

#: index.docbook:366
msgid "Quit - close active copy of editor"
msgstr "Avslutar - Stänger aktuell editor"

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>R</keycap>"
msgstr "<keycap> Ctrl</keycap><keycap>R</keycap>"

#: index.docbook:366
msgid "Replace"
msgstr "Ersätt"

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>S</keycap>"
msgstr "<keycap> Ctrl</keycap><keycap>S</keycap>"

#: index.docbook:366
msgid "Save your file."
msgstr "Sparar filen"

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>V</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>V</keycap>"

#: index.docbook:366
msgid "Paste."
msgstr "Klistrar in "

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>X</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>X</keycap>"

#: index.docbook:366
msgid "Delete the marked text and copy it to the clipboard."
msgstr "Tar bort markerad text och kopierar den till klippbordet."

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>Z</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>Z</keycap>"

#: index.docbook:366
msgid "Undo"
msgstr "Ångrar"

#: index.docbook:366
msgid "<keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>Z</keycap>"
msgstr "<keycap>Ctrl</keycap><keycap>Skift</keycap><keycap>Z</keycap>"

#: index.docbook:366
msgid "Redo"
msgstr "Gör om"

#: index.docbook:497
msgid "Menu Entries"
msgstr "Menyposter"

#: index.docbook:499
msgid "The <guimenu>File</guimenu> Menu"
msgstr "Menyn <guimenu>Arkiv</guimenu>"

#: index.docbook:501
msgid ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>N</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>N</accel>ew</guimenuitem>"
msgstr ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>N</keycap></keycombo> "
"</shortcut> <guimenu><accel>A</accel>rkiv</guimenu> "
"<guimenuitem><accel>N</accel>y</guimenuitem>"

#: index.docbook:501
msgid ""
"This command <action>starts a new document</action> in the editing window. "
"In the <guibutton>Filelist</guibutton> on the left the new file is named "
"<emphasis>Untitled</emphasis>."
msgstr ""
"<action>Skapar ett nytt dokument</action> i redigeringsfönstret. Det nya "
"filnamnet <emphasis>Namnlös</emphasis> visas i "
"<guibutton>fillistan</guibutton> till vänster."

#: index.docbook:501
msgid ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>O</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>O</accel>pen</guimenuitem>"
msgstr ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>O</keycap></keycombo> "
"</shortcut> <guimenu><accel>A</accel>rkiv</guimenu> "
"<guimenuitem>Ö<accel>p</accel>pna</guimenuitem>"

#: index.docbook:501
msgid ""
"This command does not <action>open a file</action>"
". It launches &kde;'s open file dialog box which waits for you to select the "
"files you want to open."
msgstr ""
"<action>Öppna</action>r filväljaren i &kde; där du kan välja vilken fil "
"du ska öppna."

#: index.docbook:501
msgid ""
"The open file dialog box works like a simple version of &konqueror;. Use "
"your &LMB; to click on a file name to select it. Double-click on a file name "
"to open that file. Once you've selected a file name, you can also press the "
"<guibutton>OK</guibutton> button to open the file."
msgstr ""
"Fildialogfönstret Öppna kan man också använda som en enkel variant av "
"&konqueror;. Markera filer genom att klicka på filnamnet med vänster "
"musknapp. Dubbelklicka på ett filnamn för att öppna denna fil direkt. "
"När du markerat ett filnamn kan du trycka på <guibutton>OK</guibutton> "
"för att öppna denna fil."

#: index.docbook:501
msgid ""
"Select multiple files by holding down the <keycap>Ctrl</keycap> or the "
"<keycap>Shift</keycap> key along with the &LMB;. <keycombo action=\"simul\">"
" <keycap>Ctrl</keycap><mousebutton>Left</mousebutton> </keycombo> click "
"selects one file at a time. <keycombo action=\"simul\"> "
"<keycap>Shift</keycap><mousebutton>Left</mousebutton> </keycombo> click "
"selects a contiguous set of files."
msgstr ""
"Markera flera filer genom att hålla ned <keycap>Ctrl</keycap> eller "
"<keycap>Skift</keycap> knappen samtidigt som vänster musknapp . <keycombo "
"action=\"simul\"> <keycap>Ctrl</keycap><mousebutton>Vänster</mousebutton> "
"</keycombo> klick markerar en fil i taget. <keycombo action=\"simul\"> "
"<keycap>Skift</keycap><mousebutton>Vänster</mousebutton> </keycombo> klick "
"markerar filerna mellan markeringarna."

#: index.docbook:501
msgid ""
"Clicking on a directory name in the file selection window opens that "
"directory and displays its contents. Clicking on a file name shows a "
"thumbnail view of the file in the preview window to the right of the file "
"system window."
msgstr ""
"Klickar man på en katalog öppnas den och innehållet visas. Markerar man "
"en fil kommer den förhandsgranskas i fönstret till höger."

#: index.docbook:501
msgid ""
"Use the buttons and combo box on the toolbar above the file selection window "
"to move through the file system or to adjust the properties of the open file "
"dialog box."
msgstr ""
"Använd knapparna och listboxen i verktygsraden ovanför fildialogfönstret "
"för att navigera i filsystemet eller ändra egenskaperna för dialogrutan."

#: index.docbook:501
msgid ""
"Below the file selection window is the Location combo box. Type the name of "
"the file you want to edit here. If you click the arrow on the right of the "
"drop down box, you can choose from recently used files. Open several files "
"at once by quoting each file name."
msgstr ""
"Under filvalsfönstret finns Platsfältet. Där kan du skriva filnamnet på "
"filen du vill redigera. Du kan välja från de senaste använda filerna "
"genom att klicka på pilen till höger i fältet. För att öppna flera "
"filer samtidigt måste man \"citera\" varje filnamn."

#: index.docbook:501
msgid ""
"Below the Location combo box is the Filter combo box. Enter file masks here "
"to filter the kinds of files shown in the selection window. For example, "
"typing the filter <literal role=\"extension\">*.txt</literal> and pressing "
"<keycap>Enter</keycap> will limit the display to files with a <literal "
"role=\"extension\">.txt</literal> extension. The Filter combo contains a "
"list of your most recently used filters."
msgstr ""
"Under platsfältet finns filterfältet. Här kan du skriva in en söksträng "
"för vilka filer som ska visas i filvalsfönstret. Till exempel skriv "
"<literal role=\"extension\">*.txt</literal> och tryck <keycap>Enter</keycap>"
" då kommer bara filer som bara slutar på <literal "
"role=\"extension\">.txt</literal> att visas. Du kan välja bland de senaste "
"söksträngarna genom att trycka på pilen till höger i filterfältet."

#: index.docbook:501
msgid ""
"<guimenu><accel>F</accel>ile</guimenu> <guimenuitem>Open "
"<accel>R</accel>ecent</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>rkiv</guimenu> <guimenuitem>Öppna "
"s<accel>e</accel>naste</guimenuitem>"

#: index.docbook:501
msgid ""
"This command allows you to <action>open a file</action> from a submenu that "
"contains a list of recently edited files."
msgstr ""
"Öppnar en undermeny med senast redigerade filer. Väljer du en fil kommer "
"den att <action>öppnas</action>."

#: index.docbook:501
msgid ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>S</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>S</accel>ave</guimenuitem>"
msgstr ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>S</keycap></keycombo> "
"</shortcut> <guimenu><accel>A</accel>rkiv</guimenu> "
"<guimenuitem><accel>S</accel>para</guimenuitem>"

#: index.docbook:501
msgid ""
"This command <action>saves your file</action>. Use it often. If the file is "
"<emphasis>Untitled</emphasis> then <guimenuitem>Save</guimenuitem> becomes "
"<guimenuitem>Save As</guimenuitem>."
msgstr ""
"<action>Sparar din fil</action>. Viktigt kommando, använd det ofta. Är "
"filen ny och <emphasis>Namnlös</emphasis> kommer istället för "
"<guimenuitem>Spara</guimenuitem> att <guimenuitem>Spara som</guimenuitem> "
"att användas."

#: index.docbook:501
msgid ""
"<guimenu><accel>F</accel>ile</guimenu> <guimenuitem>Save "
"<accel>A</accel>s</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>rkiv</guimenu> <guimenuitem>Spara "
"s<accel>o</accel>m</guimenuitem>"

#: index.docbook:501
msgid ""
"<action>Name and rename files</action> with this command. It launches the "
"save file dialog box. This dialog works just as the open file dialog box "
"does. You can use it to navigate through your file system, preview existing "
"files, or filter your file view with file masks."
msgstr ""
"Detta öppnar dialogrutan för att spara filen. Här kan du också "
"<action>ge filen ett nytt namn</action>. Dialogrutan Spara som fungerar "
"precis som dialogrutan Öppna. Du kan navigera i filsystemet, "
"förhandsgranska och filtrera med söksträngar."

#: index.docbook:501
msgid ""
"Type the name you want to give the file you are saving in the Location combo "
"box and press the <guibutton>OK</guibutton> button."
msgstr ""
"Skriv filnamnet för filen du ska spara i platsfältet och tryck på knappen "
"<guibutton>OK</guibutton>."

#: index.docbook:501
msgid ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>L</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> <guimenuitem>Save "
"A<accel>l</accel>l</guimenuitem>"
msgstr ""
"<shortcut> <keycombo action=\"simul\"><keycap>Ctrl</keycap> "
"<keycap>L</keycap></keycombo> </shortcut> "
"<guimenu><accel>A</accel>rkiv</guimenu> <guimenuitem>Spara "
"A<accel>l</accel>la</guimenuitem>"

#: index.docbook:501
msgid "This command <action>saves all open files</action>."
msgstr "<action>Sparar alla öppna filer</action>."

#: index.docbook:501
msgid ""
"<shortcut> <keycombo action=\"simul\"><keycap>F5</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>R</accel>eload</guimenuitem>"
msgstr ""
"<shortcut> <keycombo action=\"simul\"><keycap>F5</keycap></keycombo> "
"</shortcut> <guimenu><accel>A</accel>rkiv</guimenu> "
"<guimenuitem><accel>Å</accel>terinladda (Uppdatera)</guimenuitem>"

#: index.docbook:501
msgid ""
"<action>Reloads the active file from disk</action>. This command is useful "
"if another program or process has changed the file while you have it open in "
"&kate;"
msgstr ""
"<action>Återinladdar aktuell fil genom att öppna den från disk "
"igen</action>. Detta är användbart om du har haft filen öppen i &kate; "
"länge och något annat program har ändrat innehållet."

#: index.docbook:501
msgid ""
"<guimenu><accel>F</accel>ile</guimenu> <guimenuitem>O<accel>p</accel>en "
"with</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>rkiv</guimenu> <guimenuitem>Ö<accel>p</accel>pna "
"med</guimenuitem>"

#: index.docbook:501
msgid ""
"This command launches the open with dialog box that allows you to "
"<action>select another application to open the active file</action>. Your "
"file will still be open in &kate;."
msgstr ""
"Öppnar aktuell fil i annat program. I undermenyn <action>väljer du vilket "
"program du vill öppna filen i</action>. Aktuell fil kommer fortfarande att "
"vara öppen i &kate;."

#: index.docbook:501
msgid ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>P</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>P</accel>rint</guimenuitem>"
msgstr ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>P</keycap></keycombo> "
"</shortcut> <guimenu><accel>A</accel>rkiv</guimenu> <guimenuitem>Skriv "
"<accel>u</accel>t...</guimenuitem>"

#: index.docbook:501
msgid "<action>Print the active file</action>."
msgstr "<action>Skriver ut aktuell fil</action>."

#: index.docbook:501
msgid ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>W</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>C</accel>lose</guimenuitem>"
msgstr ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>W</keycap></keycombo> "
"</shortcut> <guimenu><accel>A</accel>rkiv</guimenu> "
"<guimenuitem>Stän<accel>g</accel></guimenuitem>"

#: index.docbook:501
msgid ""
"<action>Close the active file</action> with this command. If you have made "
"unsaved changes, you will be prompted to save the file before &kate; closes "
"it."
msgstr ""
"<action>Stänger aktuell fil</action>. Har du gjort förändringar och inte "
"sparat dessa, kommer du att få en fråga om du vill spara ändringarna "
"innan &kate; stänger filen."

#: index.docbook:501
msgid ""
"<guimenu><accel>F</accel>ile</guimenu> <guimenuitem><accel>C</accel>lose "
"All</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>rkiv</guimenu> <guimenuitem><accel>S</accel>täng "
"alla</guimenuitem>"

#: index.docbook:501
msgid "This command <action>closes all the files you have open</action> in &kate;."
msgstr "<action>Stänger alla filer som du har öppna</action> i &kate;."

#: index.docbook:501
msgid ""
"<guimenu><accel>F</accel>ile</guimenu> <guimenuitem>New "
"<accel>W</accel>indow</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>rkiv</guimenu> <guimenuitem>Nytt "
"<accel>f</accel>önster</guimenuitem>"

#: index.docbook:501
msgid ""
"<action>Opens a another instance of &kate;</action>. The new instance will "
"be identical to your previous instance."
msgstr ""
"<action>Öppnar ytterligare en kopia av &kate; fönstret</action>. Den nya "
"kopian är identiskt med original kate."

#: index.docbook:501
msgid ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>Q</keycap></keycombo> "
"</shortcut> <guimenu><accel>F</accel>ile</guimenu> "
"<guimenuitem><accel>Q</accel>uit</guimenuitem>"
msgstr ""
"<shortcut> <keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>Q</keycap></keycombo> "
"</shortcut> <guimenu><accel>A</accel>rkiv</guimenu> "
"<guimenuitem><accel>A</accel>vsluta</guimenuitem>"

#: index.docbook:501
msgid ""
"This command <action>closes &kate;</action> and any files you were editing. "
"If you have made unsaved changes to any of the files you were editing, you "
"will be prompted to save them."
msgstr ""
"<action>Avslutar &kate;</action> och alla filer som redigerades. Har du "
"osparade ändringar, får du frågor om att spara dessa."

#: index.docbook:806
msgid "Configuring Kate"
msgstr "Anpassa Kate"

#: index.docbook:807
msgid "This chapter will show you all the options for configuring &kate;."
msgstr "Detta avsnitt kommer att visa alla inställningar till &kate;."

#: index.docbook:830
msgid "Credits and License"
msgstr "Tack till och Licens"

#: index.docbook:832
msgid "&kate; Copyright 2000, 2001, Christoph Cullmann."
msgstr "&kate; Copyright 2000, 2001, Christoph Cullmann."

#: index.docbook:858
msgid ""
"Documentation copyright 2000,2001 Seth Rothberg, "
"<email>sethmr@bellatlantic.net</email>"
msgstr ""
"Dokumentation copyright 2000,2001 Seth Rothberg, "
"<email>sethmr@bellatlantic.net</email>"

#: index.docbook:863
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<para>Översättning Johan Thelmén <email>jth@linux.nu</email> "
"2001-07-13</para>"
