# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ksokoban-0.2.2\n"
"POT-Creation-Date: 2001-06-10 17:19+0200\n"
"PO-Revision-Date: 2001-04-19 20:27+0200\n"
"Last-Translator: Claudiu Costin <claudiuc@geocities.com>\n"
"Language-Team: Romanian <ro-kde@egroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9alpha\n"

#: InternalCollections.C:45
msgid "Sasquatch"
msgstr "Sasquatch"

#: InternalCollections.C:49
msgid "Mas Sasquatch"
msgstr "Mas Sasquatch"

#: InternalCollections.C:53
msgid "Sasquatch III"
msgstr "Sasquatch III"

#: InternalCollections.C:57
msgid "Microban (easy)"
msgstr "Microban (uşor)"

#: main.C:30
msgid "The japanese warehouse keeper game"
msgstr "Un joc extrem de jucat în Japonia"

#: main.C:37
msgid "Level collection file to load"
msgstr "Colecţii de nivele de încărcat"

#: main.C:45
msgid "KSokoban"
msgstr "KSokoban"

#: main.C:53
msgid "For contributing the Sokoban levels included in this game"
msgstr "Pentru contribuţia la nivelele Sokoban incluse în acest joc"

#: MainWindow.C:88
msgid "&Load levels..."
msgstr "Î&ncarcă nivele..."

#: MainWindow.C:90
msgid "&Next level"
msgstr "Nivelul &următor"

#: MainWindow.C:92
msgid "&Previous level"
msgstr "Nivelul &precedent"

#: MainWindow.C:94
msgid "Re&start level"
msgstr "R&eporneşte nivelul"

#: MainWindow.C:97
msgid "&Level collection"
msgstr "&Colecţii nivele"

#: MainWindow.C:112
msgid "&Slow"
msgstr "&Lentă"

#: MainWindow.C:113
msgid "&Medium"
msgstr "&Medie"

#: MainWindow.C:114
msgid "&Fast"
msgstr "&Rapidă"

#: MainWindow.C:115
msgid "&Off"
msgstr "&Oprită"

#: MainWindow.C:118
msgid "&Animation"
msgstr "A&nimaţie"

#: MainWindow.C:123 MainWindow.C:125 MainWindow.C:127 MainWindow.C:129
#: MainWindow.C:131 MainWindow.C:133 MainWindow.C:135 MainWindow.C:137
#: MainWindow.C:139 MainWindow.C:141 MainWindow.C:148 MainWindow.C:150
#: MainWindow.C:152 MainWindow.C:154 MainWindow.C:156 MainWindow.C:158
#: MainWindow.C:160 MainWindow.C:162 MainWindow.C:164 MainWindow.C:166
msgid "(unused)"
msgstr "(neutilizat)"

#: MainWindow.C:144
msgid "&Set bookmark"
msgstr "&Adaugă semn de carte"

#: MainWindow.C:169
msgid "&Go to bookmark"
msgstr "&Du-te la semnul de carte"

#: MainWindow.C:243
msgid "(invalid)"
msgstr "(eronat)"

#: MainWindow.C:299
msgid "Load levels from file"
msgstr "Încarcă nivele din fişier"

#: MainWindow.C:326
msgid "No levels found in file"
msgstr "Nu am găsit nivele în fişier"

#: PlayField.C:50
msgid "Level"
msgstr "Nivel"

#: PlayField.C:50
msgid "Steps"
msgstr "Paşi"

#: PlayField.C:51
msgid "Pushes"
msgstr "Împingeri"

#: PlayField.C:283
msgid "Level completed"
msgstr "Nivel terminat"

#: PlayField.C:564
msgid ""
"This is the last level in\n"
"the current collection."
msgstr ""
"Acesta este ultimul nivel din\n"
"colecţia curentă."

#: PlayField.C:570
msgid ""
"You have not completed\n"
"this level yet."
msgstr ""
"Nu aţi completat încă\n"
"acest nivel."

#: PlayField.C:584
msgid ""
"This is the first level in\n"
"the current collection."
msgstr ""
"Acesta este primul nivel din\n"
"colecţia curentă."

#: PlayField.C:725
msgid ""
"Sorry, bookmarks for external levels\n"
"is not implemented yet."
msgstr ""
"Nu sînt implementate încă semne\n"
"de carte pentru nivele externe."

#: PlayField.C:748
msgid "This level is broken"
msgstr "Acest nivel este eronat"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Claudiu Costin"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "claudiuc@geocities.com"
