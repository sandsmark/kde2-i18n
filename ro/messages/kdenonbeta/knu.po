# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: knu-0.5.1\n"
"POT-Creation-Date: 2001-07-29 19:34+0200\n"
"PO-Revision-Date: 2001-01-09 00:59+0200\n"
"Last-Translator: Claudiu Costin <claudiuc@geocities.com>\n"
"Language-Team: Romanian <ro-kde@egroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8alpha\n"

#: knu.cpp:242 knu.cpp:437
msgid "&Ping"
msgstr "&Ping"

#: knu.cpp:251 knu.cpp:443
msgid "&Traceroute"
msgstr "&Traceroute"

#: knu.cpp:260 knu.cpp:449
msgid "Host &resolution"
msgstr "&DNS"

#: knu.cpp:273
msgid "F&inger"
msgstr "F&inger"

#: knu.cpp:282 knu.cpp:461
msgid "&mtr"
msgstr "&Mtr"

#: knu.cpp:291 knu.cpp:467
msgid "&Whois"
msgstr "&Whois"

#: knu.cpp:300 knu.cpp:473
msgid "Netstat"
msgstr "Netstat"

#: knu.cpp:310
msgid ""
"There is no command enabled\n"
"in the configuration file.\n"
"\n"
"So I can't do anything...\n"
msgstr ""
"Nu este nici o comandă activată\n"
"în fişierul de configurare.\n"
"\n"
"Nu pot face nimic în aceste condiţii...\n"

#: knu.cpp:357
msgid "&New window"
msgstr "Fereastră &nouă"

#: knu.cpp:360
msgid "&Close window"
msgstr "Î&nchide fereastra"

#: knu.cpp:371
msgid "Select &all"
msgstr "Selectează &tot"

#: knu.cpp:374
msgid "C&lear output window"
msgstr "Ş&terge fereastra de mesaje"

#: knu.cpp:377
msgid "P&references..."
msgstr "&Opţiuni..."

#: knu.cpp:383
msgid ""
"Knu\n"
"Version %1\n"
"\n"
"Bertrand Leconte\n"
"<B.Leconte@mail.dotcom.fr>\n"
msgstr ""
"Knu\n"
"Versiunea %1\n"
"\n"
"Bertrand Leconte\n"
"<B.Leconte@mail.dotcom.fr>\n"

#: knu.cpp:393
#, c-format
msgid "&About %1"
msgstr "&Despre %1"

#: knu.cpp:395
msgid "About &Qt"
msgstr "Despre &Qt"

#: knu.cpp:455
msgid "&Finger"
msgstr "&Finger"

#: knu.cpp:690 knu.cpp:692
msgid "KDE Network Utilities"
msgstr "Utilitare KDE de reţea"

#: knu.cpp:703
msgid "Network utilities"
msgstr "Utilitare reţea"

#: CommandDlg.cpp:206
msgid "H&ost:"
msgstr "&Gazda:"

#: CommandDlg.cpp:215
msgid "&Go!"
msgstr "&Execută!"

#: CommandDlg.cpp:225
msgid "&Stop"
msgstr "&Opreşte"

#: CommandDlg.cpp:241
msgid ""
"This command binary was not found. \n"
"You can give its path in the Edit->Preferences... menu."
msgstr ""
"Acest program nu a fost găsit. \n"
"Puteţi să îi precizaţi calea în meniul \"Editare->Opţiuni...\"."

#: CommandDlg.cpp:434 NetstatDlg.cpp:425
msgid ""
"\n"
"You have a problem in the\n"
"configuration file of '%1'.\n"
"In the [%2] group,\n"
"I can't find a valid \"path=\" entry.\n"
"\n"
"Please use Edit->Preferences... menu\n"
"to configure it again.\n"
msgstr ""
"\n"
"Aveţi o problemă în\n"
"fişierul de configurare al '%1'.\n"
"În grupul [%2],\n"
"nu găsesc o înregistrare validă \"path=\".\n"
"\n"
"Utilizaţi meniul \"Editare->Opţiuni...\"\n"
"pentru a o configura din nou.\n"

#: CommandDlg.cpp:442 NetstatDlg.cpp:433
msgid "Error in pathname"
msgstr "Eroare în numele de cale"

#: CommandDlg.cpp:599
msgid "Binary"
msgstr "Program"

#: CommandDlg.cpp:608
msgid "Path&name:"
msgstr "Nume &cale:"

#: CommandDlg.cpp:618
msgid "Additional &arguments:"
msgstr "&Argumente adiţionale:"

#: CommandDlg.cpp:643
msgid "This command had been disabled in the configuration file."
msgstr "Această comandă a fost dezactivată în fişierul de configurare."

#: CommandDlg.cpp:776
#, fuzzy
msgid "Only local files are currently supported."
msgstr "Deocamdată sînt suportate numai fişiere locale."

#: OptionsDlg.cpp:77
msgid " - configuration"
msgstr "- Opţiuni"

#: MtrDlg.cpp:118
msgid "#"
msgstr "#"

#: MtrDlg.cpp:119
msgid "Hostname"
msgstr "Nume gazdă"

#: MtrDlg.cpp:120
msgid "Loss"
msgstr "Pierdute"

#: MtrDlg.cpp:122
msgid "Rcv"
msgstr "Recepţionate"

#: MtrDlg.cpp:124
msgid "Snt"
msgstr "Trimise"

#: MtrDlg.cpp:126
msgid "Best"
msgstr "Bune"

#: MtrDlg.cpp:128
msgid "Avg"
msgstr "Mediu"

#: MtrDlg.cpp:130
msgid "Worst"
msgstr "Slab"

#: MtrDlg.cpp:173 NetstatDlg.cpp:120 PingDlg.cpp:114 TracerouteDlg.cpp:118
msgid "Make host &name resolution"
msgstr "Transformă numele gazdei în adresă IP"

#: FingerDlg.cpp:149 HostDlg.cpp:194 WhoisDlg.cpp:107
msgid "Ser&ver:"
msgstr "Ser&ver:"

#: WhoisDlg.cpp:238
msgid "&whois"
msgstr "&whois"

#: WhoisDlg.cpp:242
msgid "&fwhois"
msgstr "&fwhois"

#: WhoisDlg.cpp:247
msgid "Servers to ask"
msgstr "Servere de interogat"

#: HostDlg.cpp:380 NetstatDlg.cpp:499 PingDlg.cpp:241
msgid "hos&t"
msgstr "ga&zda"

#: HostDlg.cpp:384 NetstatDlg.cpp:503 PingDlg.cpp:245
msgid "ns&lookup"
msgstr "ns&lookup"

#: TracerouteDlg.cpp:135
msgid "&Maximum number of hops:"
msgstr "Numărul &maxim de salturi:"

#: HostDlg.cpp:149
msgid "address (A)"
msgstr "adresa (A)"

#: HostDlg.cpp:150
msgid "name (PTR)"
msgstr "nume (PTR)"

#: HostDlg.cpp:151
msgid "name server (NS)"
msgstr "server de nume (NS)"

#: HostDlg.cpp:152
msgid "mail exchanger (MX)"
msgstr "agentul de mail (MX)"

#: HostDlg.cpp:153
msgid "alias (CNAME)"
msgstr "poreclă (CNAME)"

#: HostDlg.cpp:154
msgid "start of authority (SOA)"
msgstr "începutul autorităţii (SOA)"

#: HostDlg.cpp:155
msgid "any record (ANY)"
msgstr "orice înregistrare (ANY)"

#: HostDlg.cpp:160
msgid "Se&arch for:"
msgstr "&Caută:"

#: HostDlg.cpp:172 HostDlg.cpp:336
msgid "default server"
msgstr "server implicit"

#: FingerDlg.cpp:112
msgid "&User:"
msgstr "&Utilizator:"

#: NetstatDlg.cpp:87
msgid "Proto"
msgstr "Proto"

#: NetstatDlg.cpp:88
msgid "Local Address"
msgstr "Adresă locală"

#: NetstatDlg.cpp:89 NetstatDlg.cpp:92
msgid "Port"
msgstr "Port"

#: NetstatDlg.cpp:91
msgid "Remote Address"
msgstr "Adresă distantă"

#: NetstatDlg.cpp:94
msgid "State"
msgstr "Stare"

#: NetstatDlg.cpp:113
msgid "Mas&queraded connections"
msgstr "Conexiuni &masquerade"

#: NetstatDlg.cpp:127
msgid "Filter out &Localhost"
msgstr "Elimină informaţii &localhost"

#: mylistmanager.cpp:109
msgid "+"
msgstr "+"

#: mylistmanager.cpp:122
msgid "-"
msgstr "-"

#: mylistmanager.cpp:135
msgid "Up"
msgstr "Sus"

#: mylistmanager.cpp:143
msgid "Down"
msgstr "Jos"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Claudiu Costin"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "claudiuc@geocities.com"
