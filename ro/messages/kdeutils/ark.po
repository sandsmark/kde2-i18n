# KTranslator Generated File
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ark-0.5\n"
"POT-Creation-Date: 2001-07-31 11:25+0200\n"
"PO-Revision-Date: 2001-06-24 15:28+0200\n"
"Last-Translator: Claudiu Costin <claudiuc@geocities.com>\n"
"Language-Team: Romanian <ro-kde@egroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"claudiuc@geocities.com\n"
"X-Generator: KBabel 0.9.2\n"

#: arch.cpp:169 compressedfile.cc:178
msgid ""
"You probably don't have sufficient permissions.\n"
"Please check the file owner and the integrity\n"
"of the archive."
msgstr ""
"Probabil nu aveţi suficiente drepturi.\n"
"Vă rog să verificaţi proprietarul fişierului şi\n"
"integritatea arhivei."

#: arch.cpp:181
msgid "Deletion failed"
msgstr "Şterge eşuată"

#: arch.cpp:210
msgid ""
"Sorry, the extract operation failed.\n"
"Do you wish to view the shell output?"
msgstr ""
"Operaţia de extragere a eşuat.\n"
"Doriţi să vă uitaţi la rezultatele\n"
"operaţiei?"

#: arch.cpp:242 zip.cc:345
msgid ""
"You probably don't have sufficient permissions\n"
"Please check the file owner and the integrity\n"
"of the archive."
msgstr ""
"Probabil nu aveţi suficiente drepturi.\n"
"Vă rog să verificaţi proprietarul fişierului şi\n"
"integritatea arhivei."

#: arch.cpp:253
msgid ""
"Sorry, the add operation failed.\n"
"Do you wish to view the shell output?"
msgstr ""
"Operaţia de adăugare a eşuat.\n"
"Doriţi să vă uitaţi la rezultatele\n"
"operaţiei?"

#: ark_part.cpp:37
msgid "Ark KParts Component"
msgstr "Componentă KParts \"Arhivator\""

#: ark_part.cpp:106 main.cc:72
msgid "ark"
msgstr "Ark"

#: ark_part.cpp:138
msgid "&Extract"
msgstr "&Extrage"

#: arksettings.cpp:511
msgid ""
"*|All Files\n"
"*.zip *.xpi *.tar.gz *.tar.Z *.tar.lzo *.tgz *.taz *.tzo *.tar.bz2 *.tar.bz "
"*.tar *.lzh *.gz *.lzo *.Z *.bz *.bz2 *.zoo *.rar *.a|All valid archives "
"with extensions\n"
" *.tar.gz *.tar.Z *.tgz *.taz *.tzo *.tar.bz2 *.tar.bz *.tar.lzo *.tar |Tar "
"archives (*.tar, *.tar.gz, *.tar.Z, *.tar.bz2, etc.)\n"
"*.gz *.bz *.bz2 *.lzo *.Z|Compressed Files (*.gz *.bz *.bz2 *.lzo *.Z)\n"
"*.zip *.xpi|Zip archives (*.zip, *.xpi)\n"
"*.lzh|Lha archives with extension lzh\n"
"*.zoo|Zoo archives (*.zoo)\n"
"*.rar|Rar archives with extension rar\n"
"*.a|Ar archives with extension a\n"
msgstr ""
"*|Toate fişierele\n"
"*.zip *.tar.gz *.tar.Z *.tar.lzo *.tgz *.taz *.tzo *.tar.bz2 *.tar.bz *.tar "
"*.lzh *.gz *.lzo *.Z *.bz *.bz2 *.zoo *.rar *.a|Toate fişierele arhivă\n"
"*.tar.gz *.tar.Z *.tgz *.taz *.tzo *.tar.bz2 *.tar.bz *.tar.lzo *.tar "
"|Arhive TAR (*.tar, *.tar.gz, *.tar.Z, *.tar.bz2, etc.)\n"
"*.gz *.bz *.bz2 *.lzo *.Z|Fişiere comprimate (*.gz *.bz *.bz2 *.lzo *.Z)\n"
"*.zip|Arhive ZIP (*.zip)\n"
"*.lzh|Arhive LHA (*.lzh)\n"
"*.zoo|Arhive ZOO (*.zoo)\n"
"*.rar|Arhive RAR (*.rar)\n"
"*.a|Arhive AR (*.a)\n"

#: arkwidget.cc:153 arkwidgetpart.cpp:100
msgid "Sorry, you've run out of disk space."
msgstr "Nu mai există spaţiu pe disk."

#: arkwidget.cc:505 arkwidgetpart.cpp:163
msgid "The archive %1 does not exist."
msgstr "Arhiva %1 nu există."

#: arkwidget.cc:509 arkwidgetpart.cpp:167
#, c-format
msgid "Can't access the archive %1"
msgstr "Nu pot accesa arhiva %1"

#: arkwidgetpart.cpp:170
msgid "Unknown error."
msgstr "Eroare necunoscută."

#: arkwidgetpart.cpp:192
msgid "You don't have permission to access that archive"
msgstr "Nu aveţi permisiuni de a accesa această arhivă"

#: arkwidget.cc:843 arkwidgetpart.cpp:257
msgid ""
"This archive is read-only. If you want to save it under\n"
"a new name, go to the File menu and select Save As."
msgstr ""
"Această arhivă poate fi numai citită. Dacă doriţi să o\n"
"salvaţi sub un nume nou, atunci selectaţi \"Salvează ca\"."

#: arkwidget.cc:1641 arkwidgetpart.cpp:388
msgid ""
"%1 will not be extracted because it will overwrite an existing file.\n"
"Go back to Extract Dialog?"
msgstr ""
"%1 nu a fost etras deoarece va suprascrie un fişier existent.\n"
"Revin la dialogul de extragere?"

#: arkwidget.cc:2195 arkwidgetpart.cpp:627
msgid "Sorry, bzip doesn't like filename extensions that use capital letters."
msgstr "BZIP nu suportă extensii de fişiere care utilizează majuscule."

#: arkwidget.cc:2197 arkwidgetpart.cpp:629
msgid "Sorry, bzip doesn't like filename extensions that aren't exactly \".bz\"."
msgstr "BZIP nu suportă extensii de fişiere care nu sînt exact \".bz\"."

#: arkwidget.cc:2199 arkwidgetpart.cpp:631
msgid "Sorry, bzip2 doesn't like filename extensions that use capital letters."
msgstr "BZIP2 nu suportă extensii de fişiere care utilizează majuscule."

#: arkwidget.cc:2201 arkwidgetpart.cpp:633
msgid "Sorry, bzip2 doesn't like filename extensions that aren't exactly \".bz2\"."
msgstr "BZIP2 nu suportă extensii de fişiere care nu sînt exact \".bz2\"."

#: arkwidgetpart.cpp:668
msgid "Gzip archives need to have an extension `gz'."
msgstr "Arhivele GZIP trebuie să aibă extensia '.gz'."

#: arkwidgetpart.cpp:673
msgid "Zoo archives need to have an extension `zoo'."
msgstr "Arhivele ZOO trebuie să aibă extensia '.zoo'."

#: arkwidget.cc:2223 arkwidget.cc:2282 arkwidgetpart.cpp:678
msgid "Unknown archive format or corrupted archive"
msgstr "Format de arhivă necunoscut sau arhivă coruptă"

#: arkwidget.cc:2229 arkwidget.cc:2293 arkwidgetpart.cpp:690
msgid ""
"Sorry, the utility %1 is not in your PATH.\n"
"Please install it or contact your system administrator."
msgstr ""
"Utilitarul %1 nu este accesibil prin variabila de mediu PATH.\n"
"Vă rog să îl instalaţi sau contactaţi administratorul dumneavoastră\n"
"de sistem."

#: common_texts.cpp:1
msgid " Filename "
msgstr " Nume fişier "

#: common_texts.cpp:2
msgid " Permissions "
msgstr " Permisiuni "

#: common_texts.cpp:3
msgid " Owner/Group "
msgstr " Propr./Grup "

#: arkwidget.cc:2082 common_texts.cpp:4
msgid " Size "
msgstr " Mărime "

#: common_texts.cpp:5
msgid " Timestamp "
msgstr " Data "

#: common_texts.cpp:6
msgid " Link "
msgstr " Legătura "

#: common_texts.cpp:7
msgid " Size Now "
msgstr " Mărime actuală "

#: common_texts.cpp:8
msgid " Ratio "
msgstr " Rata "

#: common_texts.cpp:9
msgid ""
"_: acronym for Cyclic Redundancy Check\n"
" CRC "
msgstr " CRC "

#: common_texts.cpp:10
msgid " Method "
msgstr " Metoda "

#: common_texts.cpp:11
msgid " Version "
msgstr " Versiunea "

#: common_texts.cpp:12
msgid " Owner "
msgstr " Proprietar "

#: common_texts.cpp:13
msgid " Group "
msgstr " Grup "

#: common_texts.cpp:14
msgid ""
"_: (used as part of a sentence)\n"
"start-up directory"
msgstr "director de pornire"

#: common_texts.cpp:15
msgid ""
"_: directory for opening files (used as part of a sentence)\n"
"open directory"
msgstr "director deschidere"

#: common_texts.cpp:16
msgid ""
"_: directory for extracting files (used as part of a sentence)\n"
"extract directory"
msgstr "director de extragere"

#: common_texts.cpp:17
msgid ""
"_: directory for adding files (used as part of a sentence)\n"
"add directory"
msgstr "director de adăugare"

#: common_texts.cpp:19
msgid "Settings"
msgstr "Opţiuni"

#: common_texts.cpp:20
msgid "&Adding"
msgstr "&Adăugare"

#: common_texts.cpp:21
msgid "&Extracting"
msgstr "&Extragere"

#: common_texts.cpp:22
msgid "&Directories"
msgstr "&Directoare"

#: common_texts.cpp:23
msgid "Add Settings"
msgstr "Opţiuni adăugare"

#: common_texts.cpp:24
msgid "Extract Settings"
msgstr "Opţiuni extragere"

#: common_texts.cpp:25
msgid "Replace &old files only with newer files"
msgstr "Înlocuieşte fişierele &vechi numai cu fişiere noi"

#: common_texts.cpp:26
msgid "Keep entries &generic (Lha)"
msgstr "Păstrează intrările &generice (LHA)"

#: common_texts.cpp:27
msgid "Force &MS-DOS short filenames (Zip)"
msgstr "Forţează nume de fişiere scurte &MS-DOS (ZIP)"

#: common_texts.cpp:28
msgid "Tranlate LF to DOS &CRLF (Zip)"
msgstr "Translatează LF la &CRLF de MS-DOS (ZIP)"

#: common_texts.cpp:29
msgid "&Recursively add subdirectories (Zip, Rar)"
msgstr "Adaugă &recursiv subdirectoare (ZIP, RAR)"

#: common_texts.cpp:30
msgid "&Store symlinks as links (Zip, Rar)"
msgstr "&Arhivează legăturile simbolice ca link-uri (ZIP, RAR)"

#: common_texts.cpp:31
msgid "O&verwrite files (Zip, Tar, Zoo, Rar)"
msgstr "&Suprascrie fişiere (ZIP, TAR, ZOO, RAR)"

#: common_texts.cpp:32
msgid "&Preserve permissions (Tar)"
msgstr "&Păstrează permisiunile (TAR)"

#: common_texts.cpp:33
msgid "&Ignore directory names (Zip)"
msgstr "&Ignoră numele de directoare (ZIP)"

#: common_texts.cpp:34
msgid "Convert filenames to &lowercase (Zip, Rar)"
msgstr "Converteşte numele de fişiere la litere &mici (ZIP, RAR)"

#: common_texts.cpp:35
msgid "Convert filenames to &uppercase (Rar)"
msgstr "Converteşte numele de fişiere la litere &mare (RAR)"

#: deleteDlg.cpp:50
msgid "What do you want to delete?"
msgstr "Ce doriţi să ştergeţi?"

#: deleteDlg.cpp:60
msgid "Selected files"
msgstr "Fişierele selectate"

#: deleteDlg.cpp:69
msgid "Files: "
msgstr "Fişiere: "

#: dirDlg.cpp:84
msgid "Directories:"
msgstr "Directoare: "

#: dirDlg.cpp:88
msgid "Start-up directory"
msgstr "Directorul de pornire"

#: dirDlg.cpp:89
msgid ""
"_: directory for opening files\n"
"Open directory"
msgstr "Director deschidere"

#: dirDlg.cpp:90
msgid ""
"_: directory for extracting files\n"
"Extract directory"
msgstr "Director extragere"

#: dirDlg.cpp:91
msgid ""
"_: directory for adding files\n"
"Add directory"
msgstr "Director adăugare"

#: dirDlg.cpp:132 dirDlg.cpp:263
msgid "Favorite directory"
msgstr "Directorul favorit"

#: dirDlg.cpp:143
msgid "Last start-up directory"
msgstr "Ultimul director pentru pornire"

#: dirDlg.cpp:144
msgid "Last open directory"
msgstr "Ultimul director pentru deschidere"

#: dirDlg.cpp:145
msgid "Last extract directory"
msgstr "Ultimul director pentru extragere"

#: dirDlg.cpp:146
msgid "Last add directory"
msgstr "Ultimul director pentru adăugare"

#: dirDlg.cpp:147
msgid "Fixed start-up directory"
msgstr "Director de pornire fixat"

#: dirDlg.cpp:148
msgid "Fixed open directory"
msgstr "Director de deschidere fixat"

#: dirDlg.cpp:149
msgid "Fixed extract directory"
msgstr "Director de extragere fixat"

#: dirDlg.cpp:150
msgid "Fixed add directory"
msgstr "Directoru de adăugare fixat"

#: dirDlg.cpp:282
msgid "Fixed directory"
msgstr "Director fixat"

#: dirDlg.cpp:294
msgid "The directory specified as your favorite does not exist."
msgstr "Directorul specificat ca favorit nu există."

#: dirDlg.cpp:306
msgid "The fixed directory specified for your %1 does not exist."
msgstr "Directorul specificat ca fix pentru %1 nu există."

#: filelistview.cpp:129
msgid ""
"This area is for displaying information about the files contained within an "
"archive."
msgstr ""
"Această zonă este utilizată pentru afişarea informaţiilor despre "
"fişierele aflate într-o arhivă."

#: kdirselect.cpp:70
msgid "Directory"
msgstr "Director"

#: kdirselectdialog.cpp:35
msgid "Directories"
msgstr "Directoare"

#: rc.cpp:1
msgid "&Archive"
msgstr "&Arhivă"

#: rc.cpp:4
msgid "&Action"
msgstr "&Acţiune"

#: selectDlg.cpp:49
msgid "Selection"
msgstr "Selecţie"

#: selectDlg.cpp:58
msgid "Select files:"
msgstr "Fişiere selectate:"

#: shellOutputDlg.cpp:50
msgid "Shell Output"
msgstr "Rezultate de ieşire"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Claudiu Costin"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "claudiuc@geocities.com"

#: waitDlg.cpp:45
msgid "ark - Extracting..."
msgstr "Extragere"

#: waitDlg.cpp:48
msgid "Please wait..."
msgstr "Vă rog să aşteptaţi..."

#: adddlg.cc:56
msgid "Select Directory to Add"
msgstr "Selectaţi un director de adăugat"

#: adddlg.cc:62
msgid "Select Files to Add"
msgstr "Selectaţi fişiere de adăugat"

#: ar.cc:118 ar.cc:137 ar.cc:197 ar.cc:252 ar.cc:286 compressedfile.cc:160
#: compressedfile.cc:278 lha.cc:162 lha.cc:267 lha.cc:315 lha.cc:349
#: rar.cc:147 rar.cc:253 rar.cc:320 rar.cc:354 tar.cc:278 tar.cc:648
#: tar.cc:726 tar.cc:767 zip.cc:127 zip.cc:231 zip.cc:292 zip.cc:327
#: zoo.cc:128 zoo.cc:226 zoo.cc:288 zoo.cc:322
msgid "Couldn't start a subprocess."
msgstr "Nu pot porni un subproces."

#: arkwidget.cc:136
#, c-format
msgid "You don't have permission to write to the directory %1"
msgstr "Nu aveţi permisiuni să scrieţi în directorul %1"

#: arkwidget.cc:224
msgid "New &Window"
msgstr "&Fereastră nouă"

#: arkwidget.cc:233
msgid "Re&load"
msgstr "&Reîncarcă"

#: arkwidget.cc:240
msgid "&Close Archive"
msgstr "În&chide arhiva"

#: arkwidget.cc:252
msgid "&View shell output"
msgstr "Re&zultate de ieşire"

#: arkwidget.cc:258
msgid "Add &File..."
msgstr "Adaugă &fişier..."

#: arkwidget.cc:262
msgid "Add &Directory..."
msgstr "Adaugă &director..."

#: arkwidget.cc:266
msgid "E&xtract..."
msgstr "&Extrage..."

#: arkwidget.cc:270
msgid "De&lete"
msgstr "Ş&terge"

#: arkwidget.cc:274 arkwidget.cc:278
msgid ""
"_: to view something\n"
"&View"
msgstr "&Vizualizează"

#: arkwidget.cc:282 arkwidget.cc:286
msgid "&Open with..."
msgstr "&Deschide cu..."

#: arkwidget.cc:291 arkwidget.cc:295
msgid "Edit &with..."
msgstr "&Editează cu..."

#: arkwidget.cc:299
msgid "&Select..."
msgstr "&Selectează..."

#: arkwidget.cc:306
msgid "&Deselect All"
msgstr "&Deselectează tot"

#: arkwidget.cc:310
msgid "&Invert Selection"
msgstr "&Inversează selecţia"

#: arkwidget.cc:365
msgid ""
"The statusbar shows you how many files you have and how many you have "
"selected. It also shows you total sizes for these groups of files."
msgstr ""

#: arkwidget.cc:370 arkwidget.cc:1896
msgid "0 Files Selected"
msgstr "0 fişiere selectate"

#: arkwidget.cc:375
msgid "Total: 0 Files"
msgstr "Total: 0 fişiere"

#: arkwidget.cc:435
#, fuzzy
msgid ""
"_n: %n File  %1\n"
"%n Files  %1"
msgstr ""
"%n fişier  %1\n"
"%n fişiere  %1"

#: arkwidget.cc:457
msgid "Save Archive As"
msgstr "Salvează arhiva ca"

#: arkwidget.cc:470
msgid ""
"Please save your archive in the same format as the original.\n"
"Hint: Use the same extension."
msgstr ""

#: arkwidget.cc:512
msgid "Unknown error"
msgstr "Eroare necunoscută"

#: arkwidget.cc:535
msgid "You don't have permission to access that archive."
msgstr "Nu aveţi permisiuni de a accesa această arhivă."

#: arkwidget.cc:560
msgid ""
"The archive %1 is already open and has been raised.\n"
"Note: if the filename does not match, it only means that one of the two is a "
"symbolic link."
msgstr ""

#: arkwidget.cc:662
msgid "Archive already exists. Do you wish to overwrite it?"
msgstr ""

#: arkwidget.cc:662
msgid "Archive already exists"
msgstr "Arhiva există deja"

#: arkwidget.cc:685
msgid ""
"Sorry, you need to create an archive, not a new\n"
"compressed file. Please try again."
msgstr ""

#: arkwidget.cc:698
msgid ""
"Your file is missing an extension to indicate the archive type.\n"
"Is it OK to create a file of the default type (%1)?"
msgstr ""

#: arkwidget.cc:720 arkwidget.cc:1324
msgid "Create a New Archive"
msgstr "Creează o arhivă nouă"

#: arkwidget.cc:758
msgid ""
"Sorry, ark cannot create an archive of that type.\n"
"\n"
"  [Hint:  The filename should have an extension such as `.zip' to\n"
"  indicate the type of the archive. Please see the help pages for\n"
"more information on supported archive formats.]"
msgstr ""

#: arkwidget.cc:916
msgid "Edit With:"
msgstr "Editează cu:"

#: arkwidget.cc:929
msgid "Trouble editing the file..."
msgstr ""

#: arkwidget.cc:944
msgid "Open With:"
msgstr "Deschide cu:"

#: arkwidget.cc:1320
msgid ""
"You are currently working with a simple compressed file.\n"
"Would you like to make it into an archive so that it can contain multiple "
"files?\n"
"If so, you must choose a name for your new archive."
msgstr ""

#: arkwidget.cc:1513
msgid ""
"If you delete a directory in a Tar archive, all the files in that\n"
"directory will also be deleted. Are you sure you wish to proceed?"
msgstr ""

#: arkwidget.cc:1523
msgid "Do you really want to delete the selected items?"
msgstr ""

#: arkwidget.cc:1900
msgid "%1 Files Selected  %2"
msgstr "%1 fişiere selectate  %2"

#: arkwidget.cc:1906
#, c-format
msgid "1 File Selected  %2"
msgstr "1 fişier selectat  %2"

#: arkwidget.cc:2003
msgid "Do you wish to add this to the current archive or open it as a new archive?"
msgstr ""

#: arkwidget.cc:2051
msgid ""
"There is no archive currently open. Do you wish to create one now for these "
"files?"
msgstr ""

#: arkwidget.cc:2052
msgid ""
"There is no archive currently open. Do you wish to create one now for this "
"file?"
msgstr ""

#: arkwidget.cc:2081
msgid " File "
msgstr " fişier "

#: arkwidget.cc:2089
#, fuzzy
msgid "Archive directory does not exist."
msgstr "Arhiva %1 nu există."

#: arkwidget.cc:2272
#, fuzzy
msgid "Gzip archives need to have the extension `gz'."
msgstr "Arhivele GZIP trebuie să aibă extensia '.gz'."

#: arkwidget.cc:2277
#, fuzzy
msgid "Zoo archives need to have the extension `zoo'."
msgstr "Arhivele ZOO trebuie să aibă extensia '.zoo'."

#: compressedfile.cc:186
msgid "Open failed"
msgstr "Deschidere eşuată"

#: compressedfile.cc:293 tar.cc:162 tar.cc:177
msgid "Trouble writing to the archive..."
msgstr ""

#: extractdlg.cc:86
#, fuzzy
msgid "Extract to:"
msgstr "&Extrage"

#: extractdlg.cc:119
msgid "Files to be extracted"
msgstr "Fişiere de extras"

#: extractdlg.cc:133
msgid "Current"
msgstr "Curent"

#: extractdlg.cc:137
msgid "All"
msgstr "Tot"

#: extractdlg.cc:141
msgid "Selected Files"
msgstr "Fişiere selectate"

#: extractdlg.cc:149
msgid "Pattern"
msgstr "Model"

#: extractdlg.cc:164
msgid "&Preferences"
msgstr "&Preferinţe"

#: extractdlg.cc:211
msgid ""
"Ark cannot extract files to a remote location. Please provide a local valid "
"directory"
msgstr ""

#: extractdlg.cc:217
#, fuzzy
msgid "Please provide a valid directory"
msgstr "Ultimul director pentru adăugare"

#: extractdlg.cc:230
msgid "Please provide a pattern"
msgstr ""

#: extractdlg.cc:249
#, fuzzy
msgid "Select an Extract Directory"
msgstr "Ultimul director pentru extragere"

#: extractdlg.cc:286
msgid "Failure to Extract"
msgstr ""

#: extractdlg.cc:288
msgid ""
"Some files already exist in your destination directory.\n"
"The following files will not be extracted if you continue: "
msgstr ""

#: main.cc:59
msgid "KDE Archiving tool"
msgstr ""

#: main.cc:65
msgid "Open extract dialog, quit when finished."
msgstr ""

#: main.cc:66
msgid "Open 'archive'"
msgstr ""

#: main.cc:74
msgid "(c) 1997-2001, The Various Ark Developers"
msgstr ""

#: tar.cc:448
msgid "I can't fork a decompressor"
msgstr ""

#: tar.cc:469
msgid "Trouble writing to the tempfile..."
msgstr ""

#: zip.cc:351
msgid "Test of integrity failed"
msgstr ""
