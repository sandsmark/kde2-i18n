# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-01 12:33+0200\n"
"PO-Revision-Date: 2000-10-24 01:04+0200\n"
"Last-Translator: Claudiu Costin <claudiuc@geocities.com>\n"
"Language-Team: Romanian <ro-kde@egroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.6\n"

#: mixconfig.cpp:55
msgid "Default Volumes"
msgstr "Volume implicite"

#: mixconfig.cpp:63
msgid "Save current volumes"
msgstr "Salvează volumele curente"

#: mixconfig.cpp:68
msgid "Load volumes"
msgstr "Încarcă volumele"

#: mixconfig.cpp:73
msgid "Load volumes on login"
msgstr "Încarcă volumele la logare"

#: mixconfig.cpp:78
msgid "Hardware Settings"
msgstr "Setări hardware"

#: mixconfig.cpp:85
msgid "Maximum number of probed mixers"
msgstr "Numărul maxim de mixere încercate"

#: mixconfig.cpp:89
msgid ""
"Change this value to optimize the startup time of kmix.\n"
"High values mean that kmix probes for many soundcards. If you have more "
"mixers installed than kmix detects, increase this value."
msgstr ""
"Modificaţi această valoare pentru a optimiza pornirea kmix. Valori mari "
"înseamnă că programul 'kmix' încearcă multe plăci de sunet. Dacă "
"aveţi mai multe plăci de sunet instalate decît detectează 'kmix', atunci "
"creşteţi această valoare."

#: mixconfig.cpp:97
msgid "Maximum number of probed devices per mixer"
msgstr "Numărul maxim de dispozitive încercate per mixer"

#: mixconfig.cpp:102
#, fuzzy
msgid ""
"Change this value to optimize the startup time of kmix. High values mean "
"that kmix probes for many devices per soundcard driver.\n"
"If there are more mixer sub devices in a driver than kmix detects, increase "
"this value"
msgstr ""
"Modificaţi această valoare pentru a optimiza pornirea kmix. Valori mari "
"înseamnă că programul 'kmix' încearcă multe dispozitive pentru fiecare "
"driver de placă de sunet. Dacă există mai multe subdispozitive mixer "
"decît detectează 'kmix', atunci creşteţi această valoare."

#: mixconfig.cpp:122
msgid "Restoring default volumes"
msgstr "Refac volumele implicite"

#: mixconfig.cpp:129 mixconfig.cpp:156
msgid "The kmixctrl executable can't be found."
msgstr "Nu am găsit executabilul 'kmixctrl'."

#: mixconfig.cpp:149
msgid "Saving default volumes"
msgstr "Salvez volumele implicite"
