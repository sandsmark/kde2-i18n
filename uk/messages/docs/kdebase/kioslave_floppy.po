# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-10-02 15:14PDT\n"
"Last-Translator: Andriy Rysin <arysin@yahoo.com>\n"
"Language-Team: Ukrainian <linux@linux.org.ua>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: index.docbook:2
msgid "Floppy"
msgstr "Floppy"

#: index.docbook:4
msgid ""
"The floppy ioslave gives you easy access to the floppy disk drives installed "
"on your system."
msgstr ""
"Підлеглий floppy надає можливість "
"звертатись до приводів гнучких дисків, "
"встановлених на вашому комп'ютері."

#: index.docbook:9
msgid ""
"The drive letter becomes the first subdirectory in the floppy &URL;. Let's "
"say there is a file <filename>logo.png</filename> on your floppy disk in "
"drive A, then the &URL; will be "
"<userinput><command>floppy:</command><replaceable>/a/logo.png</replaceable>"
"</userinput>"
msgstr ""
"Назва диску - це перший каталог в адресі. "
"Наприклад, файл <filename>logo.png</filename>"
" у приводі A, має "
"адресу<userinput><command>floppy:</command><replaceable>"
"/a/logo.png</replaceable></userinput>"

#: index.docbook:15
msgid ""
"If you want to access drive B, "
"<userinput><command>floppy:/b</command></userinput> will do it. "
"<command>floppy:/</command> is a shortcut for <command>floppy:/a</command>."
msgstr ""
"Щоб звернутись до приводу B введіть "
"<userinput><command>floppy:/b</command></userinput>. "
"<command>floppy:/</command> це скорочення для "
"<command>floppy:/a</command>."

#: index.docbook:20
msgid ""
"Note that <command>floppy:/logo.png</command> means you have a disk drive "
"named <filename>logo.png</filename>."
msgstr ""
"Примітка, <command>floppy:/logo.png</command> передбачає, "
"що у приводі є дискета та на дискеті є "
"файл з ім'ям <filename>logo.png</filename>."

#: index.docbook:23
msgid ""
"To use it you need to have the mtools package installed, and the floppy "
"ioslave supports everything the various mtools command line utilities "
"support. You don't have to mount your floppy disks, simply enter "
"<userinput>floppy:/</userinput> in any &kde; 2.x app and you are able to "
"read and write from your floppy drive."
msgstr ""
"Щоб скористатись цим підлеглим пакет "
"програм mtools повинен бути встановлений на "
"вашому комп'ютері, підлеглий floppy "
"підтримує всі команди mtools. Вам не "
"потрібно монтувати ваші гнучкі дискети, "
"просто введіть <userinput>floppy:/</userinput> у будь "
"якій програмі &kde; 2.x та Ви зможете читати "
"та записувати файли на диску."

#: index.docbook:30
msgid ""
"According to the mtools documentation ZIP and JAZ drives are also supported, "
"you could try <command>floppy:/z</command> and <command>floppy:/j</command> "
"to access them. Due to missing hardware this is not tested."
msgstr ""
"У документації до mtools написано, що також "
"підтримуються приводи ZIP та JAZ, Ви можете "
"спробувати <command>floppy:/z</command> або "
"<command>floppy:/j</command>, щоб звернутися до них. Ці "
"команди не було перевірено за браком "
"таких приводів."

#: index.docbook:35
msgid ""
"Currently it is only possible to access one floppy drive exactly once at a "
"moment, it is not possible to read and write at the same time on the same "
"floppy."
msgstr ""
"На цей час підтримується тільки одне "
"одночасне звернення до приводу гнучких "
"дисків, не можливо одночасно читати та "
"писати на одну й туж дискету."

#: index.docbook:38
msgid "Author: Alexander Neundorf <email>neundorf@kde.org</email>"
msgstr "Автор: Alexander Neundorf <email>neundorf@kde.org</email>"
