# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-19 10:48+0200\n"
"PO-Revision-Date: 2001-06-30 11:59PST\n"
"Last-Translator: Andriy Rysin <arysin@yahoo.com>\n"
"Language-Team: Ukrainian <linux@linux.org.ua>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2\n"

#: kolourpicker.cpp:66
msgid "Color Picker"
msgstr "Вибір кольору"

#: kolourpicker.cpp:68
msgid "An applet to pick color values from anywhere on the screen"
msgstr ""
"Аплет датчику коліру для будь-якої точки "
"екрана."

#: kolourpicker.cpp:71
msgid "Original Author"
msgstr "Перший автор"

#: kolourpicker.cpp:84
msgid "Pick a color"
msgstr "Проба кольору"

#: kolourpicker.cpp:93 kolourpicker.cpp:128
msgid "History"
msgstr "Історія"

#: kolourpicker.cpp:115
msgid "I'm an unimplemented Help System"
msgstr "Невпроваджена система довідки"

#: kolourpicker.cpp:142
msgid "&Clear History"
msgstr "&Очистити історію"

#: kolourpicker.cpp:276
msgid "Copy color value"
msgstr "Скопіювати значення кольору"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Andriy Rysin"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "arysin@yahoo.com"
