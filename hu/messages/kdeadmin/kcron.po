# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Tamas Szanto <tszanto@mol.hu>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: KDE 2.2\n"
"POT-Creation-Date: 2001-06-01 12:38+0200\n"
"PO-Revision-Date: 2001-06-05 15:15+0100\n"
"Last-Translator: Tamas Szanto <tszanto@mol.hu>\n"
"Language-Team: Hungarian <kde-lista@sophia.jpte.hu>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: ctcron.cpp:61
msgid "(System Crontab)"
msgstr "(Rendszerfeladatok)"

#: ctmonth.cpp:34
msgid "every month "
msgstr "minden hónap"

#: ctmonth.cpp:51
msgid "January"
msgstr "január"

#: ctmonth.cpp:51
msgid "February"
msgstr "február"

#: ctmonth.cpp:52
msgid "March"
msgstr "március"

#: ctmonth.cpp:52
msgid "April"
msgstr "április"

#: ctmonth.cpp:53
msgid ""
"_: May long\n"
"May"
msgstr "május"

#: ctmonth.cpp:53
msgid "June"
msgstr "június"

#: ctmonth.cpp:54
msgid "July"
msgstr "július"

#: ctmonth.cpp:54
msgid "August"
msgstr "augusztus"

#: ctmonth.cpp:55
msgid "September"
msgstr "szeptember"

#: ctmonth.cpp:55
msgid "October"
msgstr "október"

#: ctmonth.cpp:56
msgid "November"
msgstr "november"

#: ctmonth.cpp:56
msgid "December"
msgstr "december"

#: ctdom.cpp:35 ctdow.cpp:60 cttask.cpp:287
msgid "every day "
msgstr "mindennap "

#: ctdom.cpp:52
msgid "1st"
msgstr "1"

#: ctdom.cpp:52
msgid "2nd"
msgstr "2"

#: ctdom.cpp:53
msgid "3rd"
msgstr "3"

#: ctdom.cpp:53
msgid "4th"
msgstr "4"

#: ctdom.cpp:54
msgid "5th"
msgstr "5"

#: ctdom.cpp:54
msgid "6th"
msgstr "6"

#: ctdom.cpp:55
msgid "7th"
msgstr "7"

#: ctdom.cpp:55
msgid "8th"
msgstr "8"

#: ctdom.cpp:56
msgid "9th"
msgstr "9"

#: ctdom.cpp:56
msgid "10th"
msgstr "10"

#: ctdom.cpp:57
msgid "11th"
msgstr "11"

#: ctdom.cpp:57
msgid "12th"
msgstr "12"

#: ctdom.cpp:58
msgid "13th"
msgstr "13"

#: ctdom.cpp:58
msgid "14th"
msgstr "14"

#: ctdom.cpp:59
msgid "15th"
msgstr "15"

#: ctdom.cpp:59
msgid "16th"
msgstr "16"

#: ctdom.cpp:60
msgid "17th"
msgstr "17"

#: ctdom.cpp:60
msgid "18th"
msgstr "18"

#: ctdom.cpp:61
msgid "19th"
msgstr "19"

#: ctdom.cpp:61
msgid "20th"
msgstr "20"

#: ctdom.cpp:62
msgid "21st"
msgstr "21"

#: ctdom.cpp:62
msgid "22nd"
msgstr "22"

#: ctdom.cpp:63
msgid "23rd"
msgstr "23"

#: ctdom.cpp:63
msgid "24th"
msgstr "24"

#: ctdom.cpp:64
msgid "25th"
msgstr "25"

#: ctdom.cpp:64
msgid "26th"
msgstr "26"

#: ctdom.cpp:65
msgid "27th"
msgstr "27"

#: ctdom.cpp:65
msgid "28th"
msgstr "28"

#: ctdom.cpp:66
msgid "29th"
msgstr "29"

#: ctdom.cpp:66
msgid "30th"
msgstr "30"

#: ctdom.cpp:67
msgid "31st"
msgstr "31"

#: ctdow.cpp:62
msgid "weekday "
msgstr "a hét napja "

#: ctdow.cpp:80
msgid "Mon"
msgstr "H"

#: ctdow.cpp:80
msgid "Tue"
msgstr "K"

#: ctdow.cpp:81
msgid "Wed"
msgstr "Sze"

#: ctdow.cpp:81
msgid "Thu"
msgstr "Cs"

#: ctdow.cpp:82
msgid "Fri"
msgstr "P"

#: ctdow.cpp:82
msgid "Sat"
msgstr "Szo"

#: ctdow.cpp:83
msgid "Sun"
msgstr "V"

#: cttask.cpp:220
msgid "Translators: See README.translators!"
msgstr ""
"OK, megnéztem! :-) Az alábbi kifejezések a magyar idő- és "
"dátumformákat tartalmazzák."

#: cttask.cpp:222
msgid "%H:%M"
msgstr "%H:%M"

#: cttask.cpp:223
msgid "DAYS_OF_MONTH of MONTHS"
msgstr "MONTHS DAYS_OF_MONTH"

#: cttask.cpp:224
msgid "every DAYS_OF_WEEK"
msgstr "minden DAYS_OF_WEEK"

#: cttask.cpp:225
msgid "DOM_FORMAT as well as DOW_FORMAT"
msgstr "DOM_FORMAT és DOW_FORMAT"

#: cttask.cpp:226
msgid "At TIME"
msgstr "TIME-kor"

#: cttask.cpp:227
msgid "TIME_FORMAT, DATE_FORMAT"
msgstr "DATE_FORMAT TIME_FORMAT"

#: cttask.cpp:273
msgid ", and "
msgstr ", és "

#: cttask.cpp:275
msgid " and "
msgstr " és "

#: cttask.cpp:277
msgid ", "
msgstr ", "

#: ktapp.cpp:52 ktapp.cpp:451
msgid "Task Scheduler"
msgstr "Időzített feladatok"

#: ktapp.cpp:90
msgid ""
"You can use this application to schedule programs to run in the background.\n"
"To schedule a new task now, click on the Tasks folder and select Edit/New "
"from the menu."
msgstr ""
"Ezzel az alkalmazással megadott időpontokban lehet háttérfeladatokat "
"indítani.\n"
"Új időzített feladat létrehozásához kattintson a Feladatok mappára "
"és válassza a Szerkesztés/Új menüpontot."

#: ktapp.cpp:90
msgid "Welcome to the Task Scheduler"
msgstr "Üdvözli Önt a Feladatidőzítő"

#: ktapp.cpp:135
msgid "&Print"
msgstr "&Nyomtatás"

#: ktapp.cpp:141
msgid "Cu&t"
msgstr "Ki&vágás"

#: ktapp.cpp:145
msgid "&New..."
msgstr "Ú&j..."

#: ktapp.cpp:146
msgid "M&odify..."
msgstr "Mó&dosítás..."

#: ktapp.cpp:149 kttask.cpp:104 ktvariable.cpp:73
msgid "&Enabled"
msgstr "Be&kapcsolva"

#: ktapp.cpp:151
msgid "&Run Now"
msgstr "&Futtatás most"

#: ktapp.cpp:198 ktapp.cpp:335 ktapp.cpp:342 ktapp.cpp:361 ktapp.cpp:368
#: ktapp.cpp:375 ktapp.cpp:382 ktapp.cpp:389 ktapp.cpp:396 ktapp.cpp:413
#: ktapp.cpp:420 ktapp.cpp:432 ktapp.cpp:444
msgid "Ready."
msgstr "Kész."

#: ktapp.cpp:299
msgid ""
"Scheduled tasks have been modified.\n"
"Do you want to save changes?"
msgstr ""
"Az időzített feladatok beállításai módosultak.\n"
"Kívánja menteni a változásokat?"

#: ktapp.cpp:333
msgid "Saving..."
msgstr "Mentés..."

#: ktapp.cpp:340
msgid "Printing..."
msgstr "A nyomtatás folyamatban..."

#: ktapp.cpp:358
msgid "Cutting to clipboard..."
msgstr "Kivágás..."

#: ktapp.cpp:366
msgid "Copying to clipboard..."
msgstr "Másolás..."

#: ktapp.cpp:373
msgid "Pasting from clipboard..."
msgstr "Beillesztés..."

#: ktapp.cpp:380
msgid "Adding new entry..."
msgstr "Új bejegyzés hozzáadása..."

#: ktapp.cpp:387
msgid "Modifying entry..."
msgstr "Bejegyzés módosítása"

#: ktapp.cpp:394
msgid "Deleting entry..."
msgstr "Bejegyzés törlése"

#: ktapp.cpp:403
msgid "Disabling entry..."
msgstr "Bejegyzés kikapcsolása"

#: ktapp.cpp:409
msgid "Enabling entry..."
msgstr "Bejegyzés bekapcsolása"

#: ktapp.cpp:418
msgid "Running command..."
msgstr "Program indítása..."

#: ktapp.cpp:514
msgid "Save tasks and variables."
msgstr "A feladatok és a változók mentése"

#: ktapp.cpp:517
msgid "Print all or current crontab(s)."
msgstr "Az összes vagy az aktuális crontab kinyomtatása."

#: ktapp.cpp:520
#, c-format
msgid "Exit %1."
msgstr "Kilépés: %1."

#: ktapp.cpp:524
msgid "Cut the selected task or variable and put on the clipboard."
msgstr "A kijelölt feladat vagy változó kivágása."

#: ktapp.cpp:527
msgid "Copy the selected task or variable to the clipboard."
msgstr "A kijelölt feladat vagy változó másolása."

#: ktapp.cpp:530
msgid "Paste task or variable from the clipboard."
msgstr "Feladat vagy változó beillesztése."

#: ktapp.cpp:533
msgid "Create a new task or variable."
msgstr "Új feladat vagy változó létrehozása."

#: ktapp.cpp:536
msgid "Edit the selected task or variable."
msgstr "A kijelölt feladat vagy változó módosítása."

#: ktapp.cpp:539
msgid "Delete the selected task or variable."
msgstr "A kijelölt feladat vagy változó törlése."

#: ktapp.cpp:542
msgid "Enable/disable the selected task or variable."
msgstr "A kijelölt feladat vagy változó ki- vagy bekapcsolása."

#: ktapp.cpp:545
msgid "Run the selected task now."
msgstr "A kijelölt feladat indítása most."

#: ktapp.cpp:549
msgid "Enable/disable the tool bar."
msgstr "Az eszköztár ki- és bekapcsolása."

#: ktapp.cpp:552
msgid "Enable/disable the status bar."
msgstr "Az állapotsor ki- vagy bekapcsolása."

#: ktview.cpp:82
msgid "Users/Tasks/Variables"
msgstr "Felhasználók/feladatok/változók"

#: ktview.cpp:84
msgid "Tasks/Variables"
msgstr "Feladatok/változók"

#: ktview.cpp:86
msgid "Value"
msgstr "Érték"

#: ktview.cpp:87
msgid "Description"
msgstr "Leírás"

#: ktview.cpp:253
msgid ""
"_: user on host\n"
"%1 <%2> on %3"
msgstr "%1 <%2> - %3"

#: ktview.cpp:261
msgid "Scheduled Tasks"
msgstr "Időzített feladatok"

#: ktlisttask.cpp:45 ktlistvar.cpp:45
msgid "Disabled"
msgstr "Kikapcsolva"

#: ktlisttask.cpp:59 ktlistvar.cpp:68
msgid "Disabled."
msgstr "Kikapcsolva."

#: ktlisttask.cpp:64
msgid "Modify Task"
msgstr "Feladat módosítása"

#: ktlisttasks.cpp:42
msgid "Tasks"
msgstr "Feladatok"

#: ktlisttasks.cpp:48
msgid "Edit Task"
msgstr "Feladat szerkesztése"

#: ktlisttasks.cpp:75
msgid "Task Name:"
msgstr "A feladat neve:"

#: ktlisttasks.cpp:76
msgid "Program:"
msgstr "Program:"

#: ktlisttasks.cpp:77 ktlistvars.cpp:72
msgid "Description:"
msgstr "Leírás:"

#: ktlisttasks.cpp:86
msgid "No tasks..."
msgstr "Nincsenek feladatok..."

#: ktlistvar.cpp:74
msgid "Modify Variable"
msgstr "Változó módosítása"

#: ktlistvars.cpp:43
msgid "Edit Variable"
msgstr "Változószerkesztés"

#: ktlistvars.cpp:70
msgid "Variable:"
msgstr "Változó:"

#: ktlistvars.cpp:71
msgid "Value:"
msgstr "Érték:"

#: ktlistvars.cpp:81
msgid "No variables..."
msgstr "Nincsenek változók..."

#: ktlistvars.cpp:98
msgid "Variables"
msgstr "Változók"

#: kttask.cpp:47
msgid "&Run as:"
msgstr "&Felh.név:"

#: kttask.cpp:73
msgid "&Comment:"
msgstr "Megje&gyzés:"

#: kttask.cpp:85
msgid "&Program:"
msgstr "&Programnév:"

#: kttask.cpp:100
msgid "&Browse..."
msgstr "&Tallózás"

#: kttask.cpp:109
msgid "&Silent"
msgstr "&Néma üzemmód"

#: kttask.cpp:118
msgid "Months"
msgstr "Hónapok"

#: kttask.cpp:137
msgid "Days of the Month"
msgstr "A hónap napjai"

#: kttask.cpp:165
msgid "Days of the Week"
msgstr "A hét napjai"

#: kttask.cpp:184
msgid "Daily"
msgstr "Futtatás naponta"

#: kttask.cpp:190
msgid "Run Every Day"
msgstr "Mindennap"

#: kttask.cpp:195
msgid "Hours"
msgstr "Óra"

#: kttask.cpp:200
msgid "AM"
msgstr "de."

#: kttask.cpp:223
msgid "PM"
msgstr "du."

#: kttask.cpp:236
msgid "Minutes"
msgstr "Perc"

#: kttask.cpp:402
msgid ""
"Please enter the following to schedule the task:\n"
msgstr ""
"A következőket kell megadni egy időzített feladat létrehozásához:\n"

#: kttask.cpp:408
msgid "the program to run"
msgstr "a program nevét"

#: kttask.cpp:420
msgid "the months"
msgstr "a hónapokat"

#: kttask.cpp:441
msgid "either the days of the month or the days of the week"
msgstr "a hónap vagy a hét napjait"

#: kttask.cpp:457
msgid "the hours"
msgstr "az órákat"

#: kttask.cpp:473
msgid "the minutes"
msgstr "a perceket"

#: kttask.cpp:501
msgid "Cannot locate program.  Please re-enter."
msgstr "A program nem található. Adja meg újból a nevét."

#: kttask.cpp:509
msgid "Program is not an executable file.  Please re-enter."
msgstr "A program nem futtatható. Adja meg újból a nevét."

#: kttask.cpp:577
msgid "Only local or mounted files can be executed by crontab."
msgstr "Csak helyi vagy már csatlakoztatott fájlrendszeri fájlok futtathatóak."

#: ktvariable.cpp:37
msgid "&Variable:"
msgstr "&Változó:"

#: ktvariable.cpp:55
msgid "Va&lue:"
msgstr "É&rték:"

#: ktvariable.cpp:64
msgid "Co&mment:"
msgstr "&Megjegyzés:"

#: ktvariable.cpp:106
msgid "Override default home directory."
msgstr "Az alapértelmezett saját könyvtár felülbírálása."

#: ktvariable.cpp:111
msgid "Email output to specified account."
msgstr "A kimenet elküldése e-mail-ben a megadott címre."

#: ktvariable.cpp:116
msgid "Override default shell."
msgstr "Az alapértelmezett parancsértelmező felülbírálása."

#: ktvariable.cpp:121
msgid "Directories to search for program files."
msgstr "A futtatható programok elérési útja."

#: ktvariable.cpp:133
msgid "Please enter the variable name."
msgstr "Adja meg a változó nevét:"

#: ktvariable.cpp:140
msgid "Please enter the variable value."
msgstr "Adja meg a változó értékét."

#: ktprintopt.cpp:27
msgid "Cron Options"
msgstr "A KCron beállításai"

#: ktprintopt.cpp:31
msgid "Print Cron&tab"
msgstr "A cron&tab nyomtatása"

#: ktprintopt.cpp:34
msgid "Print &All Users"
msgstr "Az összes &felhasználót"

#: main.cpp:26
msgid "KDE Task Scheduler"
msgstr "KDE feladatidőzítő"

#: main.cpp:28
msgid "KCron"
msgstr "KCron"

#: main.cpp:53
msgid "KCron fatal error: Unable to read or write file."
msgstr "KCron: hiba történt. Egy írási vagy olvasási művelet nem sikerült."

#: main.cpp:58
msgid "KCron fatal error: Unknown."
msgstr "KCron: hiba történt. A hiba oka pontosabban nem határozható meg."

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Szántó Tamás"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "tszanto@mol.hu"
