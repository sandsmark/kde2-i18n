# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Tamas Szanto <tszanto@mol.hu>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: KDE 2.2\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-06-06 11:11+0100\n"
"Last-Translator: Tamas Szanto <tszanto@mol.hu>\n"
"Language-Team: Hungarian <kde-lista@sophia.jpte.hu>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: imap4.cc:701
msgid "Unable to close mailbox."
msgstr "Nem sikerült bezárni a postaládát."

#: imap4.cc:1207
msgid "The server does not support TLS."
msgstr "A kiszolgáló nem támogatja a TLS-t."

#: imap4.cc:1231
msgid "Starting TLS failed."
msgstr "A TLS indítása nem sikerült."

#: imap4.cc:1242
msgid "Authentication method %1 not supported."
msgstr "A(z) '%1' felhasználóazonosítási módszer nem támogatott."

#: imap4.cc:1255
msgid "Username and password for your IMAP account:"
msgstr "Az IMAP azonosítóhoz tartozó név és jelszó:"
