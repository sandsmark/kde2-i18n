# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 17:44+0200\n"
"PO-Revision-Date: 2001-06-17 19:50CET\n"
"Last-Translator: Federico Cozzi <federico.cozzi@sns.it>\n"
"Language-Team: Italiano <kde-i18n-it@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.1\n"

#: aktionConf.cpp:26
msgid "aKtion! Setup"
msgstr "Impostazione di aKtion!"

#: aktionConf.cpp:30
msgid "Audio"
msgstr "Audio"

#: aktionConf.cpp:31
msgid "Color"
msgstr "Colore"

#: aktionConf.cpp:32
msgid "Scaling"
msgstr "Riscala"

#: aktionConf.cpp:33
msgid "Capture"
msgstr "Cattura"

#: aktionConf.cpp:34
msgid "Others"
msgstr "Altro"

#: aktionConf.cpp:51
msgid "Audio enable"
msgstr "Abilita audio"

#: aktionConf.cpp:57
msgid "Skip frames for audio sync"
msgstr "Salta fotogrammi per sincronizzazione audio"

#: aktionConf.cpp:63
msgid "Show volume slider"
msgstr "Mostra barra volume"

#: aktionConf.cpp:69
msgid "Initial volume (0-100):"
msgstr "Volume iniziale (0-100):"

#: aktionConf.cpp:97
msgid "Color mapping (non TrueColor displays)"
msgstr "Mappatura colore (display non TrueColor)"

#: aktionConf.cpp:102
msgid "TrueColor to 322 (static color)"
msgstr "TrueColor a 322 (colore statico)"

#: aktionConf.cpp:108
msgid "Color lookup table"
msgstr "Tabella consultazione colore"

#: aktionConf.cpp:113
msgid "Numer of frames to look ahead:"
msgstr "Numero di fotogrammi da vedere in anticipo:"

#: aktionConf.cpp:130
msgid "TrueColor to grayscale"
msgstr "Da TrueColor a scala di grigio"

#: aktionConf.cpp:135
msgid "None"
msgstr "Nessuno"

#: aktionConf.cpp:159
msgid "Gamma of display:"
msgstr "Gamma del display:"

#: aktionConf.cpp:182
msgid "Display scale"
msgstr "Scala display"

#: aktionConf.cpp:187 principal.cpp:63
msgid "Original size"
msgstr "Dimensione originale"

#: aktionConf.cpp:188 principal.cpp:65
msgid "Half size"
msgstr "Dimensione dimezzata"

#: aktionConf.cpp:189 principal.cpp:67
msgid "Double size"
msgstr "Dimensione doppia"

#: aktionConf.cpp:190 principal.cpp:69
msgid "Maximized"
msgstr "Massimizzato"

#: aktionConf.cpp:191 aktionConf.cpp:197 principal.cpp:71
msgid "Full screen"
msgstr "Schermo intero"

#: aktionConf.cpp:203
msgid "Use XFree86-VidModeExtensions (if available)"
msgstr "Usa (se disponibili) le estensioni XFree86-VidMode"

#: aktionConf.cpp:209
msgid "Grab the mouse"
msgstr "Cattura il mouse"

#: aktionConf.cpp:230
msgid "Output file format:"
msgstr "Formato del file di output:"

#: aktionConf.cpp:250
msgid "Output file directory:"
msgstr "Directory del file di output:"

#: aktionConf.cpp:269
msgid "Press 'c' during playback to capture a single frame."
msgstr "Premi \"c\" durante la riproduzione per catturare un fotogramma singolo."

#: aktionConf.cpp:287
msgid "Video loading"
msgstr "Caricamento video"

#: aktionConf.cpp:294
msgid "Preload animation into memory"
msgstr "Precarica l'animazione in memoria"

#: aktionConf.cpp:299
msgid "Preload and uncompress animation"
msgstr "Precarica e decomprimi l'animazione"

#: aktionConf.cpp:304
msgid "Read each sector only when needed"
msgstr "Leggi ogni settore solo quando richiesto"

#: aktionConf.cpp:317
msgid "Use X11 shared memory"
msgstr "Usa la memoria condivisa di X11"

#: aktionConf.cpp:325
msgid "Use X11 multi buffering"
msgstr "Usa il multi buffering di X11"

#: aktionConf.cpp:331
msgid "Use Pixmap instead of Image in X11"
msgstr "Usa le Pixmap invece delle Immagini in X11"

#: aktionConf.cpp:351
msgid "Initial directory:"
msgstr "Directory iniziale:"

#: aktionConf.cpp:370
msgid "Loop animation"
msgstr "Animazione ciclica"

#: aktionConf.cpp:376
msgid "Automatically start playing video"
msgstr "Inizia automaticamente la riproduzione del video"

#: aktionConf.cpp:382
msgid "Extra XAnim parameters:"
msgstr "Parametri extra di XAnim:"

#: aktionConf.cpp:392 aktionConf.cpp:485
msgid "xanim executable:"
msgstr "Eseguibile di xanim:"

#: aktionConf.cpp:492
msgid "Only local executables allowed."
msgstr "Sono permessi solo file eseguibili locali."

#: aktion_part.cpp:35
msgid "KDE Video Player"
msgstr "Lettore video di KDE"

#: aktion_part.cpp:90
msgid "aKtion"
msgstr "aKtion"

#: aktion_part.cpp:114 principal.cpp:115
msgid "Play"
msgstr "Riproduci"

#: aktion_part.cpp:119
msgid "Pause"
msgstr "Pausa"

#: aktion_part.cpp:129 principal.cpp:129
msgid "Backward"
msgstr "Indietro"

#: aktion_part.cpp:135 principal.cpp:124
msgid "Forward"
msgstr "Avanti"

#: capture.cpp:20
#, c-format
msgid "Can't save image to file:/n%1"
msgstr "Impossibile salvare l'immagine sul file:/n%1"

#: kxanim.cpp:418
msgid "%1: wrong file name."
msgstr "%1: nome file sbagliato."

#: kxanim.cpp:419
msgid "Error reading file info."
msgstr "Errore nella lettura delle informazioni del file."

#: kxanim.cpp:420
msgid "Unsupported video codec."
msgstr "Codec video non supportato."

#: kxanim.cpp:421
msgid "The video is active!"
msgstr "Il video è attivo!"

#: kxanim.cpp:422
#, c-format
msgid ""
"Can't find the xanim executable:\n"
"%1"
msgstr ""
"Impossibile trovare l'eseguibile di xanim:\n"
"%1"

#: main.cpp:10
msgid "File to open."
msgstr "File da aprire."

#: principal.cpp:55
msgid "Video information"
msgstr "Informazioni video"

#: principal.cpp:58
msgid "Open file..."
msgstr "Apri file..."

#: principal.cpp:60
msgid "Setup..."
msgstr "Impostazioni..."

#: principal.cpp:102
msgid "Volume"
msgstr "Volume"

#: principal.cpp:111
msgid "Open file"
msgstr "Apri file"

#: principal.cpp:137
msgid "Setup"
msgstr "Impostazioni"

#: principal.cpp:167
msgid "Invalid initial directory"
msgstr "Directory iniziale non valida"

#: principal.cpp:168
msgid "aktion error!"
msgstr "Errore di aKtion!"

#: principal.cpp:323
msgid ""
"*.avi *.AVI *.mov *.MOV *.mpg *.MPG *.mpeg *.MPEG *.flc *.FLC *.fli "
"*.FLI|All video formats\n"
"*.avi *.AVI|AVI files (*.avi)\n"
"*.mov *.MOV|QuickTime files (*.mov)\n"
"*.mpg *.MPG *.mpeg *.MPEG|MPG Files (*.mpg)\n"
"*.fli *.FLI *.flc *.FLC|FLI/FLC Files (*.fli *.flc)\n"
"*|All Files (*)\n"
msgstr ""
"*.avi *.AVI *.mov *.MOV *.mpg *.MPG *.mpeg *.MPEG *.flc *.FLC *.fli "
"*.FLI|Tutti i formati video\n"
"*.avi *.AVI|File AVI (*.avi)\n"
"*.mov *.MOV|File QuickTime (*.mov)\n"
"*.mpg *.MPG *.mpeg *.MPEG|File MPG (*.mpg)\n"
"*.fli *.FLI *.flc *.FLC|File FLI/FLC (*.fli *.flc)\n"
"*|Tutti i file (*)\n"

#: principal.cpp:329
msgid "Select a movie"
msgstr "Seleziona un film"

#: principal.cpp:343
msgid "Only local files are currently supported."
msgstr "Attualmente sono supportati solo i file locali."

#: principal.cpp:625
msgid ""
"File: %1\n"
"%2\n"
"%3x%4\n"
"%5 frames - %6 fps"
msgstr ""
"File: %1\n"
"%2\n"
"%3x%4\n"
"%5 fotogrammi - %6 fps"

#: principal.cpp:752
msgid ""
"You must be root to use the XFree86-VidMode extensions\n"
"Switching to normal full-screen..."
msgstr ""
"Devi essere root per usare le estensioni XFree86-VidMode\n"
"Passo in modalità a tutto schermo normale..."

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Andrea Rizzi,Federico Cozzi"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "rizzi@kde.org,federico.cozzi@sns.it"
