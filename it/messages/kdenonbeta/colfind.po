msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-05-24 12:49+0200\n"
"PO-Revision-Date: 2000-06-20 15:18CET\n"
"Last-Translator: Federico Cozzi <Federico.Cozzi@sns.it>\n"
"Language-Team: Italiano <kde-i18n-it@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.5\n"

#: toplevel.cpp:55
msgid "Search Files"
msgstr "Cerca file"

#: toplevel.cpp:68
msgid "Where:"
msgstr "Dove:"

#: toplevel.cpp:75
msgid "Search in subdirectories"
msgstr "Cerca nelle sottocartelle"

#: toplevel.cpp:79
msgid "Descend into mounted file systems"
msgstr "Entra nei filesystem montati"

#: toplevel.cpp:83
msgid "&Search"
msgstr "&Cerca"

#: toplevel.cpp:101
msgid "More criteria"
msgstr "Più criteri"

#: toplevel.cpp:102
msgid "Fewer criteria"
msgstr "Meno criteri"

#: toplevel.cpp:110
msgid "Found:"
msgstr "Trovato:"

#: toplevel.cpp:170
msgid "Found %1 matches"
msgstr "Trovate %1 corrispondenze"

#: toplevel.cpp:230
msgid "No matches found"
msgstr "Nessuna corrispondenza trovata"

#: toplevel.cpp:231
msgid "Syntax error in pattern"
msgstr "Errore di sintassi nel pattern"

#: toplevel.cpp:232
msgid "Ready"
msgstr "Pronto"

#: criterion.cpp:36 criterion.cpp:260 criterion.cpp:279 criterion.cpp:304
#: criterion.cpp:331
msgid "is"
msgstr "è"

#: criterion.cpp:37 criterion.cpp:63 criterion.cpp:261 criterion.cpp:281
#: criterion.cpp:305 criterion.cpp:333
msgid "is not"
msgstr "non è"

#: criterion.cpp:85 criterion.cpp:122
msgid "today"
msgstr "oggi"

#: criterion.cpp:86 criterion.cpp:124
msgid "during the last hour"
msgstr "nell'ultima ora"

#: criterion.cpp:87 criterion.cpp:126
msgid "during the last day"
msgstr "nell'ultimo giorno"

#: criterion.cpp:88 criterion.cpp:128
msgid "during the last week"
msgstr "nell'ultima settimana"

#: criterion.cpp:89 criterion.cpp:130
msgid "during the last month"
msgstr "nell'ultimo mese"

#: criterion.cpp:90 criterion.cpp:132
msgid "during the last year"
msgstr "nell'ultimo anno"

#: criterion.cpp:91 criterion.cpp:134 criterion.cpp:152
msgid "since"
msgstr "da"

#: criterion.cpp:92 criterion.cpp:139 criterion.cpp:153
msgid "before"
msgstr "prima di"

#: criterion.cpp:165
msgid "is (in kB)"
msgstr "è (in KByte)"

#: criterion.cpp:166 criterion.cpp:191
msgid "is smaller than (in kB)"
msgstr "è più piccolo di (in KByte)"

#: criterion.cpp:167 criterion.cpp:193
msgid "is greater than (in kB)"
msgstr "è più grande di (in KByte)"

#: criterion.cpp:211 criterion.cpp:234
msgid "is a regular file"
msgstr "è un file regolare"

#: criterion.cpp:212 criterion.cpp:236
msgid "is a symbolic link"
msgstr "è un collegamento simbolico"

#: criterion.cpp:213 criterion.cpp:238
msgid "is an executable"
msgstr "è un eseguibile"

#: criterion.cpp:214 criterion.cpp:240
msgid "is an suid executable"
msgstr "è un eseguibile suid"

#: criterion.cpp:262 criterion.cpp:283 criterion.cpp:292 criterion.cpp:308
#: criterion.cpp:335 criterion.cpp:345
msgid "is invalid"
msgstr "non è valido"

#: criterion.cpp:306
msgid "has GID"
msgstr "ha GID"

#: criterion.cpp:307
msgid "does not have GID"
msgstr "non ha GID"

#: criterion.cpp:359
msgid "Name"
msgstr "Nome"

#: criterion.cpp:360
msgid "Last Modified"
msgstr "Modificato"

#: criterion.cpp:361
msgid "Size"
msgstr "Dimensione"

#: criterion.cpp:362
msgid "Type"
msgstr "Tipo"

#: criterion.cpp:363
msgid "Owner"
msgstr "Proprietario"

#: criterion.cpp:364
msgid "Owning group"
msgstr "Gruppo"

#: main.cpp:31
msgid "Columbo Find."
msgstr "Ricerca con Colombo."

#: main.cpp:34
msgid "Columbo Find"
msgstr "Ricerca con Colombo"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""
