msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-28 17:26+0200\n"
"PO-Revision-Date: 2001-07-27 10:31CET\n"
"Last-Translator: Andrea Rizzi <rizzi@kde.org>\n"
"Language-Team: Italiano <kde-i18n-it@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9\n"

#: core/container_panel.cpp:363
msgid "Scroll Left"
msgstr "Scorri verso sinistra"

#: core/container_panel.cpp:364
msgid "Scroll Right"
msgstr "Scorri verso destra"

#: core/container_panel.cpp:370
msgid "Scroll Up"
msgstr "Scorri in alto"

#: core/container_panel.cpp:371
msgid "Scroll Down"
msgstr "Scorri in basso"

#: core/container_panel.cpp:390 core/container_panel.cpp:391
msgid "Show Panel"
msgstr "Mostra pannello"

#: core/container_panel.cpp:393 core/container_panel.cpp:394
msgid "Hide Panel"
msgstr "Nascondi pannello"

#: core/kickerbindings.cpp:9 ui/appletop_mnu.cpp:46 ui/extensionop_mnu.cpp:34
msgid "Panel"
msgstr "Pannello"

#: core/kickerbindings.cpp:11
msgid "Popup Launch Menu"
msgstr "Mostra menu di esecuzione"

#: core/kickerbindings.cpp:15
msgid "Toggle showing Desktop"
msgstr "Mostra/nascondi il desktop"

#: core/main.cpp:176
msgid "KDE Panel"
msgstr "Pannello di KDE"

#: core/main.cpp:178
msgid "The KDE desktop panel."
msgstr "Il pannello del desktop di KDE."

#: core/panelbutton.cpp:300
msgid "The file %1 doesn't exist"
msgstr "Il file %1 non esiste"

#: core/panelbutton.cpp:318 core/panelbutton.cpp:387
#, c-format
msgid "Browse: %1"
msgstr "Sfoglia: %1"

#: core/panelbutton.cpp:443 core/panelbutton.cpp:444
msgid "Start Application"
msgstr "Avvia applicazione"

#: core/panelbutton.cpp:541 core/panelbutton.cpp:542
msgid "Show Desktop"
msgstr "Mostra il desktop"

#: core/panelbutton.cpp:753 core/panelbutton.cpp:779
msgid "Cannot execute non-KDE application!"
msgstr "Impossibile eseguire un'applicazione non KDE!"

#: core/panelbutton.cpp:754 core/panelbutton.cpp:780
msgid "Kicker Error!"
msgstr "Errore di Kicker!"

#: core/panelbutton.cpp:802 core/panelbutton.cpp:804
msgid "Window List"
msgstr "Lista delle finestre"

#: core/panelbutton.cpp:853 core/panelbutton.cpp:854
#: ui/addcontainer_mnu.cpp:50
msgid "Bookmarks"
msgstr "Segnalibri"

#: core/panelbutton.cpp:900 core/panelbutton.cpp:901
#: ui/addcontainer_mnu.cpp:51 ui/k_mnu.cpp:104
msgid "Recent Documents"
msgstr "Documenti recenti"

#: core/panelbutton.cpp:945
msgid "Terminal-Emulation"
msgstr "Emulazione terminale"

#: core/panelbutton.cpp:946
msgid "Terminal Session"
msgstr "Sessione terminale"

#: ui/addcontainer_mnu.cpp:44
msgid "Button"
msgstr "Pulsante"

#: ui/addcontainer_mnu.cpp:45
msgid "Applet"
msgstr "Applet"

#: ui/addcontainer_mnu.cpp:46
msgid "Extension"
msgstr "Estensione"

#: ui/addcontainer_mnu.cpp:48 ui/k_mnu.cpp:54
msgid "K Menu"
msgstr "Menu K"

#: ui/addcontainer_mnu.cpp:49
msgid "Windowlist"
msgstr "Lista finestre"

#: ui/addcontainer_mnu.cpp:52
msgid "Desktop Access"
msgstr "Accesso al desktop"

#: ui/addcontainer_mnu.cpp:53 ui/k_mnu.cpp:113
msgid "Quick Browser"
msgstr "Browser veloce"

#: ui/addcontainer_mnu.cpp:54
msgid "Non-KDE Application"
msgstr "Applicazione non KDE"

#: ui/addcontainer_mnu.cpp:55
msgid "Terminal Sessions"
msgstr "Sessioni di terminale"

#: ui/addcontainer_mnu.cpp:105 ui/addcontainer_mnu.cpp:117
msgid "Select an executable"
msgstr "Scegli un eseguibile"

#: ui/addcontainer_mnu.cpp:112
msgid ""
"The selected file is not executable.\n"
"Do you want to select another file?"
msgstr ""
"Il file selezionato non è eseguibile.\n"
"Vuoi selezionare un altro file?"

#: ui/appletop_mnu.cpp:49 ui/extensionop_mnu.cpp:41
msgid "&Move"
msgstr "&Sposta"

#: ui/appletop_mnu.cpp:58 ui/extensionop_mnu.cpp:50
msgid "Report &Bug..."
msgstr "Segnala un &bug..."

#: ui/browser_dlg.cpp:38
msgid "Quick Browser Configuration"
msgstr "Configurazione del browser veloce"

#: ui/browser_dlg.cpp:46
msgid "Button Icon:"
msgstr "Icona pulsante:"

#: ui/browser_dlg.cpp:55
msgid "Path:"
msgstr "Percorso:"

#: ui/browser_dlg.cpp:60
msgid "&Browse"
msgstr "&Sfoglia"

#: ui/browser_dlg.cpp:79
msgid "Select a directory"
msgstr "Scegli una directory"

#: ui/browser_mnu.cpp:83 ui/browser_mnu.cpp:92
msgid "Failed to read directory."
msgstr "Impossibile leggere la directory."

#: ui/browser_mnu.cpp:232
msgid "More..."
msgstr "Altri..."

#: ui/browser_mnu.cpp:246
msgid "Open in File Manager"
msgstr "Apri nel file manager"

#: ui/browser_mnu.cpp:247
msgid "Open in Terminal"
msgstr "Apri in un terminale"

#: ui/dirdrop_mnu.cpp:32
msgid "Add as &file manager URL"
msgstr "Aggiungi come URL del &file manager"

#: ui/dirdrop_mnu.cpp:34
msgid "Add as Quick&Browser"
msgstr "Aggiungi come &browser veloce"

#: ui/exe_dlg.cpp:40
msgid "Non-KDE application configuration"
msgstr "Configurazione di una applicazione non KDE"

#: ui/exe_dlg.cpp:42
msgid "Filename: "
msgstr "Nome file: "

#: ui/exe_dlg.cpp:43
msgid "Optional command line arguments:"
msgstr "Argomenti opzionali riga di comando:"

#: ui/exe_dlg.cpp:45
msgid "Run in terminal."
msgstr "Esegui in un terminale."

#: ui/extensionop_mnu.cpp:37
msgid "&Shade"
msgstr "&Arrotola"

#: ui/extensionop_mnu.cpp:66
msgid "&Help..."
msgstr "&Aiuto..."

#: ui/k_mnu.cpp:138
msgid "Run Command..."
msgstr "Esegui comando..."

#: ui/k_mnu.cpp:143
msgid "Configure Panel"
msgstr "Configura pannello"

#: ui/k_mnu.cpp:147
msgid "Lock Screen"
msgstr "Blocca lo schermo"

#: ui/k_mnu.cpp:148
msgid "Logout"
msgstr "Termina la sessione"

#: ui/konsole_mnu.cpp:85
#, c-format
msgid ""
"_: Screen is a program controlling screens!\n"
"Screen at %1"
msgstr "Screen a %1"

#: ui/panelop_mnu.cpp:48
msgid "Tiny"
msgstr "Piccolissimo"

#: ui/panelop_mnu.cpp:49
msgid "Small"
msgstr "Piccolo"

#: ui/panelop_mnu.cpp:50
msgid "Normal"
msgstr "Normale"

#: ui/panelop_mnu.cpp:51
msgid "Large"
msgstr "Grande"

#: ui/panelop_mnu.cpp:60
msgid "&Add"
msgstr "&Aggiungi"

#: ui/panelop_mnu.cpp:62
msgid "Si&ze"
msgstr "Dimen&sione"

#: ui/panelop_mnu.cpp:65
msgid "&Menu Editor..."
msgstr "Editor &menu..."

#: ui/quickbrowser_mnu.cpp:42
msgid "&Home Directory"
msgstr "&Directory home"

#: ui/quickbrowser_mnu.cpp:44
msgid "&Root Directory"
msgstr "Directory &root"

#: ui/quickbrowser_mnu.cpp:46
msgid "System &Configuration"
msgstr "&Configurazione sistema"

#: ui/recent_mnu.cpp:49
msgid "Clear History"
msgstr "Ripulisci la cronologia"

#: ui/recent_mnu.cpp:55 ui/service_mnu.cpp:189
msgid "No entries"
msgstr "Nessuna voce"

#: ui/service_mnu.cpp:196
msgid "Add this menu"
msgstr "Aggiungi questo menu"
