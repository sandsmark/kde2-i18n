# Galician translation of clockapplet.
# Copyright (C) 2000 Jesús Bravo Álvarez.
# Jesús Bravo Álvarez <jba@pobox.com>, 2000.
#
# Proxecto Trasno - Adaptación do software libre á lingua galega:  Se desexas
# colaborar connosco, podes atopar máis información en http://trasno.gpul.org
#
# First Version: 2000-09-25 21:35+0200
#
msgid ""
msgstr ""
"Project-Id-Version: clockapplet\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2000-11-26 23:26+0100\n"
"Last-Translator: Jesús Bravo Álvarez <jba@pobox.com>\n"
"Language-Team: Galician <trasno@ceu.fi.udc.es>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: clock.cpp:879
msgid ""
"_: hour\n"
"one"
msgstr "a unha"

#: clock.cpp:879
msgid ""
"_: hour\n"
"two"
msgstr "as dúas"

#: clock.cpp:880
msgid ""
"_: hour\n"
"three"
msgstr "as tres"

#: clock.cpp:880
msgid ""
"_: hour\n"
"four"
msgstr "as catro"

#: clock.cpp:880
msgid ""
"_: hour\n"
"five"
msgstr "as cinco"

#: clock.cpp:881
msgid ""
"_: hour\n"
"six"
msgstr "as seis"

#: clock.cpp:881
msgid ""
"_: hour\n"
"seven"
msgstr "as sete"

#: clock.cpp:881
msgid ""
"_: hour\n"
"eight"
msgstr "as oito"

#: clock.cpp:882
msgid ""
"_: hour\n"
"nine"
msgstr "as nove"

#: clock.cpp:882
msgid ""
"_: hour\n"
"ten"
msgstr "as dez"

#: clock.cpp:882
msgid ""
"_: hour\n"
"eleven"
msgstr "as once"

#: clock.cpp:883
msgid ""
"_: hour\n"
"twelve"
msgstr "as doce"

#: clock.cpp:886
#, no-c-format
msgid "%0 o'clock"
msgstr "%0 en punto"

#: clock.cpp:887
#, no-c-format
msgid "five past %0"
msgstr "%0 e cinco"

#: clock.cpp:888
#, no-c-format
msgid "ten past %0"
msgstr "%0 e dez"

#: clock.cpp:889
#, no-c-format
msgid "quarter past %0"
msgstr "%0 e cuarto"

#: clock.cpp:890
#, no-c-format
msgid "twenty past %0"
msgstr "%0 e vinte"

#: clock.cpp:891
#, no-c-format
msgid "twenty five past %0"
msgstr "%0 e vintecinco"

#: clock.cpp:892
#, fuzzy, no-c-format
msgid "half past %0"
msgstr "%0 e cinco"

#: clock.cpp:893
#, no-c-format
msgid "twenty five to %1"
msgstr "%1 menos vintecinco"

#: clock.cpp:894
#, no-c-format
msgid "twenty to %1"
msgstr "%1 menos vinte"

#: clock.cpp:895
#, no-c-format
msgid "quarter to %1"
msgstr "%1 menos cuarto"

#: clock.cpp:896
#, no-c-format
msgid "ten to %1"
msgstr "%1 menos dez"

#: clock.cpp:897
#, no-c-format
msgid "five to %1"
msgstr "%1 menos cinco"

#: clock.cpp:898
#, no-c-format
msgid "%1 o'clock"
msgstr "%1 en punto"

#: clock.cpp:900
#, no-c-format
msgid ""
"_: one\n"
"%0 o'clock"
msgstr "%0 en punto"

#: clock.cpp:901
#, no-c-format
msgid ""
"_: one\n"
"five past %0"
msgstr "%0 e cinco"

#: clock.cpp:902
#, no-c-format
msgid ""
"_: one\n"
"ten past %0"
msgstr "%0 e dez"

#: clock.cpp:903
#, no-c-format
msgid ""
"_: one\n"
"quarter past %0"
msgstr "%0 e cuarto"

#: clock.cpp:904
#, no-c-format
msgid ""
"_: one\n"
"twenty past %0"
msgstr "%0 e vinte"

#: clock.cpp:905
#, no-c-format
msgid ""
"_: one\n"
"twenty five past %0"
msgstr "%0 e vintecinco"

#: clock.cpp:906
#, fuzzy, no-c-format
msgid ""
"_: one\n"
"half past %0"
msgstr "%0 e media"

#: clock.cpp:907
#, no-c-format
msgid ""
"_: one\n"
"twenty five to %1"
msgstr "%1 menos vintecinco"

#: clock.cpp:908
#, no-c-format
msgid ""
"_: one\n"
"twenty to %1"
msgstr "%1 menos vinte"

#: clock.cpp:909
#, no-c-format
msgid ""
"_: one\n"
"quarter to %1"
msgstr "%1 menos cuarto"

#: clock.cpp:910
#, no-c-format
msgid ""
"_: one\n"
"ten to %1"
msgstr "%1 menos dez"

#: clock.cpp:911
#, no-c-format
msgid ""
"_: one\n"
"five to %1"
msgstr "%1 menos cinco"

#: clock.cpp:912
#, no-c-format
msgid ""
"_: one\n"
"%1 o'clock"
msgstr "%1 en punto"

#: clock.cpp:948
msgid "Night"
msgstr "Noite"

#: clock.cpp:949
msgid "Early morning"
msgstr "Mañanciña"

#: clock.cpp:949
msgid "Morning"
msgstr "Mañá"

#: clock.cpp:949
msgid "Almost noon"
msgstr "Mañá"

#: clock.cpp:950
msgid "Noon"
msgstr "Mediodía"

#: clock.cpp:950
msgid "Afternoon"
msgstr "Tarde"

#: clock.cpp:950
msgid "Evening"
msgstr "Serán"

#: clock.cpp:951
msgid "Late evening"
msgstr "Madrugada"

#: clock.cpp:957
msgid "Start of week"
msgstr "Inicio da semana"

#: clock.cpp:959
msgid "Middle of week"
msgstr "Metade da semana"

#: clock.cpp:961
msgid "End of week"
msgstr "Final da semana"

#: clock.cpp:963
msgid "Weekend!"
msgstr "¡Fin de semana!"

#: clock.cpp:1203
msgid "Clock"
msgstr ""

#: clock.cpp:1206
msgid "&Plain"
msgstr ""

#: clock.cpp:1207
msgid "&Digital"
msgstr "&Dixital"

#: clock.cpp:1208
msgid "&Analog"
msgstr "&Analóxico"

#: clock.cpp:1209
msgid "&Fuzzy"
msgstr "&Difuso"

#: clock.cpp:1212
msgid "&Type"
msgstr "&Tipo"

#: clock.cpp:1214
msgid "&Adjust Date && Time..."
msgstr "&Axustar Data e Hora..."

#: clock.cpp:1215
msgid "Date && Time &Format..."
msgstr "&Formato da Data e da Hora..."

#: rc.cpp:1
msgid "Clock Preferences"
msgstr "Preferencias do Reloxo"

#: rc.cpp:2
msgid "&General"
msgstr ""

#: rc.cpp:3
msgid "Clock &Type:"
msgstr ""

#: rc.cpp:4
msgid "Plain Clock"
msgstr ""

#: rc.cpp:5
#, fuzzy
msgid "Digital Clock"
msgstr "Reloxo D&ixital"

#: rc.cpp:6
#, fuzzy
msgid "Analog Clock"
msgstr "Reloxo A&nalóxico"

#: rc.cpp:7
#, fuzzy
msgid "Fuzzy Clock"
msgstr "Reloxo Di&fuso"

#: rc.cpp:8
msgid "Date"
msgstr ""

#: rc.cpp:9 rc.cpp:21 rc.cpp:59
#, fuzzy
msgid "C&hoose Font"
msgstr "Escolla a Fon&te"

#: rc.cpp:10
#, fuzzy
msgid "&Use custom color"
msgstr "Cores Per&sonalizadas"

#: rc.cpp:11 rc.cpp:22 rc.cpp:56
msgid "Font:"
msgstr "Fonte:"

#: rc.cpp:12 rc.cpp:19 rc.cpp:32 rc.cpp:45 rc.cpp:57
msgid "&Foreground Color:"
msgstr "Cor de &Primeiro Plano:"

#: rc.cpp:13
#, fuzzy
msgid "&Plain Clock"
msgstr "Reloxo A&nalóxico"

#: rc.cpp:15 rc.cpp:25 rc.cpp:38 rc.cpp:50
#, fuzzy
msgid "Sho&w date"
msgstr "amosar da&ta"

#: rc.cpp:16 rc.cpp:26 rc.cpp:39
#, fuzzy
msgid "Show s&econds"
msgstr "amosar &segundos"

#: rc.cpp:18 rc.cpp:33 rc.cpp:42 rc.cpp:55
#, fuzzy
msgid "&Use custom colors"
msgstr "Cores Per&sonalizadas"

#: rc.cpp:20 rc.cpp:30 rc.cpp:43 rc.cpp:58
msgid "&Background Color:"
msgstr "Cor do &Fondo:"

#: rc.cpp:23
#, fuzzy
msgid "&Digital Clock"
msgstr "Reloxo D&ixital"

#: rc.cpp:27
#, fuzzy
msgid "Blinking do&ts"
msgstr "pu&ntos a pestanexar"

#: rc.cpp:29 rc.cpp:41
msgid "You can choose either the predefined LCD look or define a custom color set."
msgstr ""
"Pode escoller o aspecto predeterminado de LCD ou definir cores "
"personalizadas."

#: rc.cpp:31 rc.cpp:44
#, fuzzy
msgid "S&hadow Color:"
msgstr "Cor da &Sombra:"

#: rc.cpp:34 rc.cpp:46
msgid "&LCD look"
msgstr "Aspecto &LCD"

#: rc.cpp:35 rc.cpp:47
msgid "U&se plain colors"
msgstr ""

#: rc.cpp:36
msgid "A&nalog Clock"
msgstr "Reloxo A&nalóxico"

#: rc.cpp:48
#, fuzzy
msgid "Fu&zzy Clock"
msgstr "Reloxo Di&fuso"

#: rc.cpp:51
#, fuzzy
msgid "Fuzzine&ss:"
msgstr "&Difusidade:"

#: rc.cpp:52
msgid "Low"
msgstr "Baixa"

#: rc.cpp:53
msgid "High"
msgstr "Alta"

#~ msgid "&Apply"
#~ msgstr "&Aplicar"

#~ msgid "Type"
#~ msgstr "Tipo"

#~ msgid ""
#~ "Here you can select the clock type.<br> digital clock: a LCD-style "
#~ "clock<br> analog clock: a traditional clock with clock face and hands"
#~ msgstr ""
#~ "Aquí pode selecciona-lo tipo de reloxo.<br> reloxo dixital: un reloxo "
#~ "estilo LCD<br>reloxo analóxico: un reloxo tradicional con esfera e agullas"

#~ msgid "&digital clock"
#~ msgstr "reloxo &dixital"

#~ msgid "analo&g clock"
#~ msgstr "reloxo analó&xico"

#~ msgid "fuzz&y clock"
#~ msgstr "reloxo di&fuso"

#~ msgid "Here you can set various options for both clock styles."
#~ msgstr "Aquí pode establecer diversas opcións para ambos estilos de reloxo."

#~ msgid "Colors"
#~ msgstr "Cores"

#~ msgid "half %0"
#~ msgstr "%0 e media"
