# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-07-23 12:16+0200\n"
"PO-Revision-Date: 2001-07-23 20:07GMT\n"
"Last-Translator: Ramon Casha <ramon.casha@linux.org.mt>\n"
"Language-Team: Maltese <mt@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2\n"

#: img_saver.cpp:63
msgid "Kooka save assistant"
msgstr "Assistent tal-ktib Kooka"

#: img_saver.cpp:81
msgid "<B>Save Assistant</B><P>Select an image format to save the scanned image."
msgstr ""
"<B>Assistent tal-Ktib</B><P>Agħżel format ta' stampa li trid tuża biex "
"tikteb l-istampa skannjata."

#: img_saver.cpp:99
msgid "Available image formats:"
msgstr "Formati ta' stampi disponibbli:"

#: img_saver.cpp:118
msgid "-No format selected-"
msgstr "-Ebda format magħżul-"

#: img_saver.cpp:124
msgid "Select the image sub-format"
msgstr "Agħżel is-sotto-format tal-istampa"

#: img_saver.cpp:134
msgid "Remember this format for this image type"
msgstr "Ftakar dan il-format għal din it-tip ta' stampa"

#: img_saver.cpp:137
msgid "Don't ask again for this format"
msgstr "Terġax tistaqsi għal dan il-format"

#: img_saver.cpp:170
msgid "-no hint available-"
msgstr "-ebda ħjiel disponibbli-"

#: img_saver.cpp:299
msgid ""
"The directory\n"
"%1\n"
" does not exist and could not be created !\n"
"Please check the permissions."
msgstr ""
"Id-direttorju\n"
"%1\n"
" ma jeżistix u ma jistax jinħoloq !\n"
"Jekk jogħġbok iċċekkja l-permessi."

#: img_saver.cpp:306
msgid ""
"The directory\n"
"%1\n"
" is not writeable.\n"
"Please check the permissions."
msgstr ""
"Id-direttorju\n"
"%1\n"
" ma jistax jinkiteb !\n"
"Jekk jogħġbok iċċekkja l-permessi."

#: img_saver.cpp:502
msgid "Skipped"
msgstr "Maqbuż"

#: img_saver.cpp:656
msgid " image save OK      "
msgstr " ktib tal-istampa OK      "

#: img_saver.cpp:657
msgid " permission error   "
msgstr " problema bil-permessi   "

#: img_saver.cpp:658
msgid " bad filename       "
msgstr " isem tal-fajl ħażin       "

#: img_saver.cpp:659
msgid " no space on device "
msgstr " ma fadalx biżżejjed spazju "

#: img_saver.cpp:660
msgid " could not write image format "
msgstr " ma stajtx nikteb il-format tal-istampa"

#: img_saver.cpp:661
msgid " can not write file using that protocol "
msgstr " ma nistax nikteb fajl b'dak il-protokoll"

#: img_saver.cpp:662
msgid " user canceled saving "
msgstr " ktib kanċellat mill-user "

#: img_saver.cpp:663
msgid " unknown error      "
msgstr " problema mhux magħrufa      "

#: img_saver.cpp:664
msgid " parameter wrong    "
msgstr " parametru ħażin    "

#: kocrfindia.cpp:49
msgid "Optical Character Recognition finished"
msgstr "Għarfien Ottiku tal-Karattri (OCR) lest"

#: kocrfindia.cpp:50
msgid "Open in Kate"
msgstr "Iftaħ bil-Kate"

#: kocrfindia.cpp:61
msgid "<B>Optical Character Recognition Results</B>"
msgstr "<B>Riżultati tal-Għarfien Ottiku tal-Karattri (OCR)</B>"

#: kocrstartdia.cpp:47
msgid "Optical Character Recognition"
msgstr "Għarfien Ottiku tal-Karattri (OCR)"

#: kocrstartdia.cpp:48
msgid "Start OCR"
msgstr "Ibda' OCR"

#: kocrstartdia.cpp:61
msgid ""
"<B>Starting Optical Character Recognition</B><P>Kooka uses <I>gocr</I> for "
"optical character recognition, an Open Source Project<P>Author of gocr is "
"<B>Joerg Schulenburg</B><BR>For more information about gocr see <A "
"HREF=http://jocr.sourceforge.net>http://jocr.sourceforge.net</A>"
msgstr ""
"<B>Bidu tal-Għarfien Ottiku tal-Karattri (OCR)</B><P>Kooka tuża "
"<I>gocr</I> - proġett Sors Miftuħ - għall-għarfien ottiku "
"tal-karattri.<P> L-awtur ta' gocr huwa <B>Joerg Schulenburg</B><BR>Għal "
"iżjed tagħrif dwar gocr irrikorri għand <A "
"HREF=http://jocr.sourceforge.net>http://jocr.sourceforge.net</A>"

#: kocrstartdia.cpp:77
msgid "Path to 'gocr' binary: "
msgstr "Fejn qiegħed il-programm 'gocr': "

#: kocrstartdia.cpp:92
msgid "Enter the path to gocr, the optical-character-recognition command line tool."
msgstr ""
"Daħħal l-isem tal-fajl tal-programm gocr, l-għodda tal-għarfien ottiku "
"tal-karattri."

#: kocrstartdia.cpp:107
msgid "Gray level"
msgstr "Livell ta' griż"

#: kocrstartdia.cpp:112
msgid ""
"The numeric value gray pixels are \n"
"considered to be black.\n"
"\n"
"Default is 160"
msgstr ""
"L-iskurezza ta' griż fejn jibda' jitqies\n"
"bħala iswed.\n"
"Il-valur pridefinit huwa 160"

#: kocrstartdia.cpp:114
msgid "Dust size"
msgstr "Daqs tat-trab"

#: kocrstartdia.cpp:119
msgid ""
"Clusters smaller than this values\n"
"will be considered to be dust and \n"
"removed from the image.\n"
"\n"
"Default is 10"
msgstr ""
"Tikek iżgħar minn dan l-ammont\n"
"jitqiesu bħala ħmieġ u jitneħħew \n"
"mill-istampa.\n"
"\n"
"Il-valur pridefinit huwa 10"

#: kocrstartdia.cpp:121
msgid "Space width"
msgstr "Wisa' ta' spazju"

#: kocrstartdia.cpp:125
msgid ""
"Spacing between characters.\n"
"\n"
"Default is 0 what means autodetection"
msgstr ""
"Spazzjar bejn l-ittri\n"
"\n"
"Il-valur pridefinit huwa 0 li jfisser jiġi kkalkulat awtomatikament"

#: kocrstartdia.cpp:172
msgid ""
"The path does not lead to the gocr-binary.\n"
"Please check your installation and/or install gocr."
msgstr ""
"Dan m'huwiex il-fajl programm 'gocr'.\n"
"Jekk jogħġbok iċċekkja l-installazzjoni u/jew installa l-gocr."

#: kocrstartdia.cpp:174
msgid "OCR software not found"
msgstr "Programm OCR ma nstabx"

#: kocrstartdia.cpp:183
msgid ""
"The gocr exists, but is not executable.\n"
"Please check your installation and/or install gocr properly."
msgstr ""
"Il-fajl gocr jeżisti, imma m'hux eżekutibbli.\n"
"Jekk jogħġbok iċċekkja l-installazzjoni u/jew installa l-gocr sew."

#: kocrstartdia.cpp:185
msgid "OCR software not executable"
msgstr "Programm tal-OCR mhux eżekutibbli"

#: kooka.cpp:64
msgid "KDE2 Scanning"
msgstr "Skannjar KDE2"

#: kooka.cpp:120
msgid "Show scan &parameter"
msgstr "Uri l-&parametru tal-iskannjar"

#: kooka.cpp:125
msgid "Show scan pre&view"
msgstr "Uri pre&viżjoni tal-iskannjar"

#: kooka.cpp:136
msgid "&OCR image..."
msgstr "&OCR stampa..."

#: kooka.cpp:140
msgid "O&CR on selection..."
msgstr "O&CR fuq l-għażla"

#: kooka.cpp:144
msgid "Scale to W&idth"
msgstr "Skala għall-&wisa'"

#: kooka.cpp:148
msgid "Scale to &Height"
msgstr "Skala għall-&għoli"

#: kooka.cpp:152
msgid "Original &Size"
msgstr "&Daqs oriġinali"

#: kooka.cpp:156
msgid "Create from selectio&n"
msgstr "Oħloq &mill-għażla"

#: kooka.cpp:160
msgid "Mirror image &vertically"
msgstr "Aqleb l-istampa &vertikament"

#: kooka.cpp:164
msgid "&Mirror image horizontally"
msgstr "Aqleb l-istampa &orizzontalment"

#: kooka.cpp:168
msgid "Mirror image &both directions"
msgstr "Aqleb l-istampa fiż-&żewġ direzzjonijiet"

#: kooka.cpp:172
msgid "Open image in &graphic application"
msgstr "Iftaħ l-istampa fi programm tal-&grafika"

#: kooka.cpp:176
msgid "&Rotate image clockwise"
msgstr "Dawwar stampa &favur l-arloġġ"

#: kooka.cpp:180
msgid "Rotate image counter-clock&wise"
msgstr "Dawwar stampa &kontra l-arloġġ"

#: kooka.cpp:183
msgid "Rotate image 180 &degrees"
msgstr "Dawwar stampa &180 grad"

#: kooka.cpp:187
msgid "Save Scanparameters"
msgstr "Ikteb il-parametri tal-iskannjar"

#: kookapref.cpp:31
msgid "Preferences"
msgstr "Preferenzi"

#: kookapref.cpp:49
msgid "Startup"
msgstr "Bidu"

#: kookapref.cpp:49
msgid "Kooka Startup Preferences"
msgstr "Preferenzi tal-bidu ta' Kooka"

#: kookapref.cpp:52
msgid "Note that changing this options will affect Kooka's next start!"
msgstr "Innota li bdil f'dawn l-għażliet ikollhom effett id-darba li jmiss!"

#: kookapref.cpp:54
msgid "Query network for available scanners"
msgstr "Fittex in-network għal scanners disponibbli"

#: kookapref.cpp:58
msgid "Show the scanner selection box on next startup"
msgstr "Uri l-kaxxa biex tagħżel skanner meta terġa' tibda"

#: kookaview.cpp:73
msgid "&Images"
msgstr "&Stampi"

#: kookaview.cpp:74
msgid "&Preview"
msgstr "&Previżjoni"

#: kookaview.cpp:197
msgid "Could not find a suitable HTML component"
msgstr "Ma stajtx insib komponent HTML adegwat"

#: kookaview.cpp:327
msgid "Starting OCR on selection"
msgstr "Qed nibda OCR fuq l-għażla"

#: kookaview.cpp:341
msgid "Starting OCR on the entire image"
msgstr "Qed nibda OCR fuq l-istampa kollha"

#: kookaview.cpp:359
msgid ""
"Could not start OCR-Process,\n"
"Probably there is already one running."
msgstr ""
"Ma stajtx jibda' l-proċess OCR.\n"
"aktarx diġa hemm ieħor għaddej."

#: kookaview.cpp:370
msgid "Create new image from selection"
msgstr "Oħloq stampa ġdida mill-għażla"

#: kookaview.cpp:395
msgid "rotate image 90 degrees"
msgstr "dawwar l-istampa 90 grad"

#: kookaview.cpp:399
msgid "rotate image 180 degrees"
msgstr "dawwar l-istampa 180 grad"

#: kookaview.cpp:405
msgid "rotate image -90 degrees"
msgstr "dawwar l-istampa -90 grad"

#: kookaview.cpp:441
msgid "Mirroring image vertically"
msgstr "Aqleb l-istampa vertikament"

#: kookaview.cpp:445
msgid "Mirroring image horizontally"
msgstr "Aqleb l-istampa orizzontalment"

#: kookaview.cpp:449
msgid "Mirroring image in both directions"
msgstr "Aqleb l-istampa fiż-żewġ direzzjonijiet"

#: kookaview.cpp:478
msgid "Storing image changes"
msgstr "Qed nikteb il-bidliet fl-istampa"

#: main.cpp:50
msgid "Kooka"
msgstr "Kooka"

#: miscwidgets.cpp:94
msgid "Select image zoom:"
msgstr "Agħżel tkabbir tal-istampa:"

#: miscwidgets.cpp:100
#, c-format
msgid "25 %"
msgstr "25 %"

#: miscwidgets.cpp:106
#, c-format
msgid "50 %"
msgstr "50 %"

#: miscwidgets.cpp:112
#, c-format
msgid "75 %"
msgstr "75 %"

#: miscwidgets.cpp:118
#, c-format
msgid "100 %"
msgstr "100 %"

#: miscwidgets.cpp:124
#, c-format
msgid "150 %"
msgstr "150 %"

#: miscwidgets.cpp:130
#, c-format
msgid "200 %"
msgstr "200 %"

#: miscwidgets.cpp:136
#, c-format
msgid "300 %"
msgstr "300 %"

#: miscwidgets.cpp:142
#, c-format
msgid "400 %"
msgstr "400 %"

#: miscwidgets.cpp:149
msgid "Custom scale factor:"
msgstr "Skapa personalizzata:"

#: packageritem.cpp:267
msgid "1 item"
msgstr "oġġett wieħed"

#: packageritem.cpp:269
msgid "%1 items"
msgstr "%1 oġġetti"

#: rc.cpp:1
msgid "&ImageCanvas"
msgstr "&TilaStampa"

#: rc.cpp:3
msgid "Image Viewer Toolbar"
msgstr "Toolbar tal-wiri tal-istampa"

#: scanpackager.cpp:73
msgid "Scan packages"
msgstr ""

#: scanpackager.cpp:74
msgid "Size"
msgstr "Daqs"

#: scanpackager.cpp:75
msgid "Colors"
msgstr "Kuluri"

#: scanpackager.cpp:76
msgid "Format"
msgstr "Format"

#: scanpackager.cpp:88
msgid "Image collection"
msgstr "Kollezzjoni ta' stampi"

#: scanpackager.cpp:116
msgid "incoming"
msgstr "deħlin"

#: scanpackager.cpp:187
msgid ""
"You entered a file extension that differs from the existing one.\n"
"That is not yet possible, converting 'on the fly' is planned for a future "
"release.\n"
"\n"
"Kooka corrects the extension."
msgstr ""
"Daħħalt estensjoni tal-fajl li huwa differenti minn dak eżistenti.\n"
"Dan għadu m'hux possibbli - il-konverżjoni immedjata hija ppjanata għal "
"ħarġa fil-futur.\n"
"\n"
"Kooka ikkoreġa l-estensjoni"

#: scanpackager.cpp:189
msgid "On the fly conversion"
msgstr "Konverżjoni immedjata"

#: scanpackager.cpp:282 scanpackager.cpp:338
msgid ""
"Cannot write this image format.\n"
"Image will not be saved!"
msgstr ""
"Ma nistax jikteb f'dan il-format.\n"
"L-istampa mhux se tinkiteb!"

#: scanpackager.cpp:283 scanpackager.cpp:288 scanpackager.cpp:295
#: scanpackager.cpp:339 scanpackager.cpp:344
msgid "Save error"
msgstr "Problema fil-ktib"

#: scanpackager.cpp:287 scanpackager.cpp:343
msgid ""
"Image file is write protected.\n"
"Image will not be saved!"
msgstr ""
"Fajl tal-istampa ma jistax jinkiteb.\n"
"L-istampa mhux se tinkiteb!"

#: scanpackager.cpp:293
msgid ""
"Can not save the image, because the file is local.\n"
"Kooka will support other protocols later."
msgstr ""
"Ma nistax nikteb l-istampa, għax il-fajl huwa lokali.\n"
"Kooka taċċetta protokolli oħra fil-ġejjieni."

#: scanpackager.cpp:366
msgid "%1 images"
msgstr "%1 stampi"

#: scanpackager.cpp:368 scanpackager.cpp:978
#, c-format
msgid "image %1"
msgstr "stampa %1"

#: scanpackager.cpp:447
msgid "Unload image"
msgstr ""

#: scanpackager.cpp:448
msgid "Export image"
msgstr "Esporta stampa"

#: scanpackager.cpp:451 scanpackager.cpp:871
msgid "Delete image"
msgstr "Ħassar stampa"

#: scanpackager.cpp:455
msgid "Create new folder"
msgstr "Oħloq direttorju ġdid"

#: scanpackager.cpp:764
msgid "Canceled by user"
msgstr "Kanċellat mill-user"

#: scanpackager.cpp:837
msgid ""
"Do you really want to delete the folder %1\n"
"and all the images inside ?"
msgstr ""
"Żgur li trid tħassar il-folder %1\n"
"u l-istampi kollha li hemm fih ?"

#: scanpackager.cpp:838
msgid "Delete collection item"
msgstr "Ħassar oġġett mill-kollezzjoni"

#: scanpackager.cpp:870
msgid ""
"Do you really want to delete this image ?\n"
"It can't be restored !"
msgstr ""
"Żgur li trid tħassar din l-istampa?\n"
"Ma tkunx tista' terġa' ġġibha lura."

#: scanpackager.cpp:893
msgid "New Folder"
msgstr "Direttorju Ġdid"

#: scanpackager.cpp:893
msgid "<B>Please enter a name for the new folder:</B>"
msgstr "<B>Jekk jogħġbok daħħal isem għad-direttorju l-ġdid:</B>"

#: scanpackager.cpp:953
msgid "%1 MB"
msgstr "%1 MB"

#: scanpackager.cpp:957
msgid "%1 kB"
msgstr "%1 kB"

#: scanpackager.cpp:961
msgid "%1 bytes"
msgstr "%1 byte"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Ramon Casha"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "ramon.casha@linux.org.mt"
