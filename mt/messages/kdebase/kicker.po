# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-28 17:26+0200\n"
"PO-Revision-Date: 2001-07-01 19:54CET\n"
"Last-Translator: Ramon Casha <ramon.casha@linux.org.mt>\n"
"Language-Team: Maltese <mt@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.1\n"

#: core/container_panel.cpp:363
msgid "Scroll Left"
msgstr "Mexxi lejn ix-xellug"

#: core/container_panel.cpp:364
msgid "Scroll Right"
msgstr "Mexxi lejn il-lemin"

#: core/container_panel.cpp:370
msgid "Scroll Up"
msgstr "Mexxi 'l fuq"

#: core/container_panel.cpp:371
msgid "Scroll Down"
msgstr "Mexxi 'l isfel"

#: core/container_panel.cpp:390 core/container_panel.cpp:391
msgid "Show Panel"
msgstr "Uri l-pannell"

#: core/container_panel.cpp:393 core/container_panel.cpp:394
msgid "Hide Panel"
msgstr "Aħbi l-pannell"

#: core/kickerbindings.cpp:9 ui/appletop_mnu.cpp:46 ui/extensionop_mnu.cpp:34
msgid "Panel"
msgstr "Pannell"

#: core/kickerbindings.cpp:11
msgid "Popup Launch Menu"
msgstr "Menu tal-eżekuzzjoni"

#: core/kickerbindings.cpp:15
msgid "Toggle showing Desktop"
msgstr "Uri/aħbi desktop"

#: core/main.cpp:176
msgid "KDE Panel"
msgstr "Pannell KDE"

#: core/main.cpp:178
msgid "The KDE desktop panel."
msgstr "Il-pannell tad-desktop KDE"

#: core/panelbutton.cpp:300
msgid "The file %1 doesn't exist"
msgstr "Il-fajl %1 ma jeżistix"

#: core/panelbutton.cpp:318 core/panelbutton.cpp:387
#, c-format
msgid "Browse: %1"
msgstr "Browse: %1"

#: core/panelbutton.cpp:443 core/panelbutton.cpp:444
msgid "Start Application"
msgstr "Ħaddem programm"

#: core/panelbutton.cpp:541 core/panelbutton.cpp:542
msgid "Show Desktop"
msgstr "Uri desktop"

#: core/panelbutton.cpp:753 core/panelbutton.cpp:779
msgid "Cannot execute non-KDE application!"
msgstr "Ma jistax jitħaddem dal-programm mhux tal-KDE"

#: core/panelbutton.cpp:754 core/panelbutton.cpp:780
msgid "Kicker Error!"
msgstr "Problema tal-Kicker"

#: core/panelbutton.cpp:802 core/panelbutton.cpp:804
msgid "Window List"
msgstr "Lista ta' windows"

#: core/panelbutton.cpp:853 core/panelbutton.cpp:854
#: ui/addcontainer_mnu.cpp:50
msgid "Bookmarks"
msgstr "Bookmarks"

#: core/panelbutton.cpp:900 core/panelbutton.cpp:901
#: ui/addcontainer_mnu.cpp:51 ui/k_mnu.cpp:104
msgid "Recent Documents"
msgstr "Dokumenti riċenti"

#: core/panelbutton.cpp:945
msgid "Terminal-Emulation"
msgstr "Terminal"

#: core/panelbutton.cpp:946
msgid "Terminal Session"
msgstr "Sessjoni terminal"

#: ui/addcontainer_mnu.cpp:44
msgid "Button"
msgstr "Buttuna"

#: ui/addcontainer_mnu.cpp:45
msgid "Applet"
msgstr "Applet"

#: ui/addcontainer_mnu.cpp:46
msgid "Extension"
msgstr "Estensjoni"

#: ui/addcontainer_mnu.cpp:48 ui/k_mnu.cpp:54
msgid "K Menu"
msgstr "Menu \"K\""

#: ui/addcontainer_mnu.cpp:49
msgid "Windowlist"
msgstr "Lista ta' windows"

#: ui/addcontainer_mnu.cpp:52
msgid "Desktop Access"
msgstr "Aċċess għad-desktop"

#: ui/addcontainer_mnu.cpp:53 ui/k_mnu.cpp:113
msgid "Quick Browser"
msgstr "Browser ta' malajr"

#: ui/addcontainer_mnu.cpp:54
msgid "Non-KDE Application"
msgstr "Programm non-KDE"

#: ui/addcontainer_mnu.cpp:55
msgid "Terminal Sessions"
msgstr "Sessjonijiet tat-terminal"

#: ui/addcontainer_mnu.cpp:105 ui/addcontainer_mnu.cpp:117
msgid "Select an executable"
msgstr "Agħżel programm"

#: ui/addcontainer_mnu.cpp:112
msgid ""
"The selected file is not executable.\n"
"Do you want to select another file?"
msgstr ""
"Il-fajl li għażilt m'huwiex eżekutibbli.\n"
"Trid tagħżel ieħor?"

#: ui/appletop_mnu.cpp:49 ui/extensionop_mnu.cpp:41
msgid "&Move"
msgstr "&Mexxi"

#: ui/appletop_mnu.cpp:58 ui/extensionop_mnu.cpp:50
msgid "Report &Bug..."
msgstr "Irrapporta &Problema..."

#: ui/browser_dlg.cpp:38
msgid "Quick Browser Configuration"
msgstr "Konfigurazzjoni tal-Browser ta' Malajr"

#: ui/browser_dlg.cpp:46
msgid "Button Icon:"
msgstr "Stampa tal-buttuna:"

#: ui/browser_dlg.cpp:55
msgid "Path:"
msgstr "Fejn:"

#: ui/browser_dlg.cpp:60
msgid "&Browse"
msgstr "&Browse"

#: ui/browser_dlg.cpp:79
msgid "Select a directory"
msgstr "Agħżel direttorju"

#: ui/browser_mnu.cpp:83 ui/browser_mnu.cpp:92
msgid "Failed to read directory."
msgstr "Ma stajtx naqra d-direttorju."

#: ui/browser_mnu.cpp:232
msgid "More..."
msgstr "Iżjed..."

#: ui/browser_mnu.cpp:246
msgid "Open in File Manager"
msgstr "Iftaħ fil-fajl manager"

#: ui/browser_mnu.cpp:247
msgid "Open in Terminal"
msgstr "Iftaħ ġo terminal"

#: ui/dirdrop_mnu.cpp:32
msgid "Add as &file manager URL"
msgstr "Żid bħala URL tal-&fajl manager"

#: ui/dirdrop_mnu.cpp:34
msgid "Add as Quick&Browser"
msgstr "Żid bħala Browser ta' &Malajr"

#: ui/exe_dlg.cpp:40
msgid "Non-KDE application configuration"
msgstr "Konfigurazzjoni ta' programmi mhux tal-KDE"

#: ui/exe_dlg.cpp:42
msgid "Filename: "
msgstr "Isem tal-fajl: "

#: ui/exe_dlg.cpp:43
msgid "Optional command line arguments:"
msgstr "Parametri tal-linja ta' kmand:"

#: ui/exe_dlg.cpp:45
msgid "Run in terminal."
msgstr "Ħaddem ġo terminal."

#: ui/extensionop_mnu.cpp:37
msgid "&Shade"
msgstr "&Raqqaq"

#: ui/extensionop_mnu.cpp:66
msgid "&Help..."
msgstr "&Għajnuna..."

#: ui/k_mnu.cpp:138
msgid "Run Command..."
msgstr "Ħaddem kmand..."

#: ui/k_mnu.cpp:143
msgid "Configure Panel"
msgstr "Issettja l-pannell"

#: ui/k_mnu.cpp:147
msgid "Lock Screen"
msgstr "Sakkar l-iskrin"

#: ui/k_mnu.cpp:148
msgid "Logout"
msgstr "Temm is-sessjoni"

#: ui/konsole_mnu.cpp:85
#, c-format
msgid ""
"_: Screen is a program controlling screens!\n"
"Screen at %1"
msgstr "Screen fuq %1"

#: ui/panelop_mnu.cpp:48
msgid "Tiny"
msgstr "Ċkejken"

#: ui/panelop_mnu.cpp:49
msgid "Small"
msgstr "Żgħir"

#: ui/panelop_mnu.cpp:50
msgid "Normal"
msgstr "Normali"

#: ui/panelop_mnu.cpp:51
msgid "Large"
msgstr "Kbir"

#: ui/panelop_mnu.cpp:60
msgid "&Add"
msgstr "&Żid"

#: ui/panelop_mnu.cpp:62
msgid "Si&ze"
msgstr "&Daqs"

#: ui/panelop_mnu.cpp:65
msgid "&Menu Editor..."
msgstr "&Editur tal-Menus..."

#: ui/quickbrowser_mnu.cpp:42
msgid "&Home Directory"
msgstr "Direttorju tal-&Bidu"

#: ui/quickbrowser_mnu.cpp:44
msgid "&Root Directory"
msgstr "Direttorju &root"

#: ui/quickbrowser_mnu.cpp:46
msgid "System &Configuration"
msgstr "&Konfigurazzjoni tas-Sistema"

#: ui/recent_mnu.cpp:49
msgid "Clear History"
msgstr "Żvojta l-kronoloġija"

#: ui/recent_mnu.cpp:55 ui/service_mnu.cpp:189
msgid "No entries"
msgstr "Ebda elementi"

#: ui/service_mnu.cpp:196
msgid "Add this menu"
msgstr "Żid dan il-menu"
