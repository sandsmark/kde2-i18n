# WEB-Translator generated file. UTF-8 test:äëïöü
# Copyright (C) 2001 Free Software Foundation, Inc.
# Frikkie Thirion <frix@engineer.com>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: kicker VERSION\n"
"POT-Creation-Date: 2001-06-28 17:26+0200\n"
"PO-Revision-Date: 2001-10-08 17:34+0200\n"
"Last-Translator: WEB-Translator <http://kde.af.org.za>\n"
"Language-Team: AFRIKAANS <AF@lia.org.za>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: core/container_panel.cpp:363
msgid "Scroll Left"
msgstr "rol links"

#: core/container_panel.cpp:364
msgid "Scroll Right"
msgstr "rol regterkant"

#: core/container_panel.cpp:370
msgid "Scroll Up"
msgstr "rol begin"

#: core/container_panel.cpp:371
msgid "Scroll Down"
msgstr "rol ondertoe"

#: core/container_panel.cpp:390 core/container_panel.cpp:391
msgid "Show Panel"
msgstr "vertoon paneel"

#: core/container_panel.cpp:393 core/container_panel.cpp:394
msgid "Hide Panel"
msgstr "steek weg paneel"

#: core/kickerbindings.cpp:9 ui/appletop_mnu.cpp:46 ui/extensionop_mnu.cpp:34
msgid "Panel"
msgstr "paneel"

#: core/kickerbindings.cpp:11
msgid "Popup Launch Menu"
msgstr "opspring lanseer kieslys"

#: core/kickerbindings.cpp:15
msgid "Toggle showing Desktop"
msgstr "wissel te wys werkskerm"

#: core/main.cpp:176
msgid "KDE Panel"
msgstr "kde paneel"

#: core/main.cpp:178
msgid "The KDE desktop panel."
msgstr "die kde werkskerm paneel."

#: core/panelbutton.cpp:300
msgid "The file %1 doesn't exist"
msgstr "die lêer %1 nie bestaan"

#: core/panelbutton.cpp:318 core/panelbutton.cpp:387
#, c-format
msgid "Browse: %1"
msgstr "blaai: %1"

#: core/panelbutton.cpp:443 core/panelbutton.cpp:444
msgid "Start Application"
msgstr "begin aansoek"

#: core/panelbutton.cpp:541 core/panelbutton.cpp:542
msgid "Show Desktop"
msgstr "vertoon werkskerm"

#: core/panelbutton.cpp:753 core/panelbutton.cpp:779
msgid "Cannot execute non-KDE application!"
msgstr "kan nie uitvoer nie-kde aansoek!"

#: core/panelbutton.cpp:754 core/panelbutton.cpp:780
msgid "Kicker Error!"
msgstr "kicker fout!"

#: core/panelbutton.cpp:802 core/panelbutton.cpp:804
msgid "Window List"
msgstr "venster lys"

#: core/panelbutton.cpp:853 core/panelbutton.cpp:854
#: ui/addcontainer_mnu.cpp:50
msgid "Bookmarks"
msgstr "boekmerke"

#: core/panelbutton.cpp:900 core/panelbutton.cpp:901
#: ui/addcontainer_mnu.cpp:51 ui/k_mnu.cpp:104
msgid "Recent Documents"
msgstr "onlangse dokumente"

#: core/panelbutton.cpp:945
msgid "Terminal-Emulation"
msgstr "Terminal-Emulation"

#: core/panelbutton.cpp:946
msgid "Terminal Session"
msgstr "terminaal sessie"

#: ui/addcontainer_mnu.cpp:44
msgid "Button"
msgstr "knoppie"

#: ui/addcontainer_mnu.cpp:45
msgid "Applet"
msgstr "miniprogram"

#: ui/addcontainer_mnu.cpp:46
msgid "Extension"
msgstr "uitbreiding"

#: ui/addcontainer_mnu.cpp:48 ui/k_mnu.cpp:54
msgid "K Menu"
msgstr "k kieslys"

#: ui/addcontainer_mnu.cpp:49
msgid "Windowlist"
msgstr "venster lys"

#: ui/addcontainer_mnu.cpp:52
msgid "Desktop Access"
msgstr "werkskerm toegang verkry"

#: ui/addcontainer_mnu.cpp:53 ui/k_mnu.cpp:113
msgid "Quick Browser"
msgstr "vinnige blaaier"

#: ui/addcontainer_mnu.cpp:54
msgid "Non-KDE Application"
msgstr "nie-kde aansoek"

#: ui/addcontainer_mnu.cpp:55
msgid "Terminal Sessions"
msgstr "terminaal sessies"

#: ui/addcontainer_mnu.cpp:105 ui/addcontainer_mnu.cpp:117
msgid "Select an executable"
msgstr "kies 'n uitvoerbare"

#: ui/addcontainer_mnu.cpp:112
msgid ""
"The selected file is not executable.\n"
"Do you want to select another file?"
msgstr ""
"die gekose lêer is nie uitvoerbare.\n"
"doen jy wil hê aan kies nog 'n lêer?"

#: ui/appletop_mnu.cpp:49 ui/extensionop_mnu.cpp:41
msgid "&Move"
msgstr "beweeg"

#: ui/appletop_mnu.cpp:58 ui/extensionop_mnu.cpp:50
msgid "Report &Bug..."
msgstr "raporteer fout..."

#: ui/browser_dlg.cpp:38
msgid "Quick Browser Configuration"
msgstr "vinnige blaaier opstelling"

#: ui/browser_dlg.cpp:46
msgid "Button Icon:"
msgstr "knoppie ikoon:"

#: ui/browser_dlg.cpp:55
msgid "Path:"
msgstr "gids soeklys:"

#: ui/browser_dlg.cpp:60
msgid "&Browse"
msgstr "blaai"

#: ui/browser_dlg.cpp:79
msgid "Select a directory"
msgstr "kies 'n gids"

#: ui/browser_mnu.cpp:83 ui/browser_mnu.cpp:92
msgid "Failed to read directory."
msgstr "gevaal aan lees gids."

#: ui/browser_mnu.cpp:232
msgid "More..."
msgstr "meer..."

#: ui/browser_mnu.cpp:246
msgid "Open in File Manager"
msgstr "open in lêer bestuurder"

#: ui/browser_mnu.cpp:247
msgid "Open in Terminal"
msgstr "open in terminaal"

#: ui/dirdrop_mnu.cpp:32
msgid "Add as &file manager URL"
msgstr "voeg by as lêer bestuurder url"

#: ui/dirdrop_mnu.cpp:34
msgid "Add as Quick&Browser"
msgstr "voeg by as vinnige blaaier"

#: ui/exe_dlg.cpp:40
msgid "Non-KDE application configuration"
msgstr "nie-kde aansoek opstelling"

#: ui/exe_dlg.cpp:42
msgid "Filename: "
msgstr "lêernaam: "

#: ui/exe_dlg.cpp:43
msgid "Optional command line arguments:"
msgstr "opsionele opdrag lyn argumente:"

#: ui/exe_dlg.cpp:45
msgid "Run in terminal."
msgstr "hardloop in terminaal."

#: ui/extensionop_mnu.cpp:37
msgid "&Shade"
msgstr "skadu"

#: ui/extensionop_mnu.cpp:66
msgid "&Help..."
msgstr "hulp..."

#: ui/k_mnu.cpp:138
msgid "Run Command..."
msgstr "hardloop opdrag..."

#: ui/k_mnu.cpp:143
msgid "Configure Panel"
msgstr "konfigureer paneel"

#: ui/k_mnu.cpp:147
msgid "Lock Screen"
msgstr "sluit skerm"

#: ui/k_mnu.cpp:148
msgid "Logout"
msgstr "teken af"

#: ui/konsole_mnu.cpp:85
#, c-format
msgid ""
"_: Screen is a program controlling screens!\n"
"Screen at %1"
msgstr "skerm na %1"

#: ui/panelop_mnu.cpp:48
msgid "Tiny"
msgstr "baie klein"

#: ui/panelop_mnu.cpp:49
msgid "Small"
msgstr "klein"

#: ui/panelop_mnu.cpp:50
msgid "Normal"
msgstr "normale"

#: ui/panelop_mnu.cpp:51
msgid "Large"
msgstr "groot"

#: ui/panelop_mnu.cpp:60
msgid "&Add"
msgstr "voeg by"

#: ui/panelop_mnu.cpp:62
msgid "Si&ze"
msgstr "grootte"

#: ui/panelop_mnu.cpp:65
msgid "&Menu Editor..."
msgstr "kieslys redigeerder..."

#: ui/quickbrowser_mnu.cpp:42
msgid "&Home Directory"
msgstr "huis gids"

#: ui/quickbrowser_mnu.cpp:44
msgid "&Root Directory"
msgstr "root gids"

#: ui/quickbrowser_mnu.cpp:46
msgid "System &Configuration"
msgstr "stelsel opstelling"

#: ui/recent_mnu.cpp:49
msgid "Clear History"
msgstr "maak skoon geskiedenis"

#: ui/recent_mnu.cpp:55 ui/service_mnu.cpp:189
msgid "No entries"
msgstr "nee inskrywings"

#: ui/service_mnu.cpp:196
msgid "Add this menu"
msgstr "voeg by hierdie kieslys"
