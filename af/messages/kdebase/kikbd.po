# WEB-Translator generated file. UTF-8 test:äëïöü
# Copyright (C) 2001 Free Software Foundation, Inc.
# Frikkie Thirion <frix@engineer.com>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: kikbd VERSION\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2001-10-08 17:34+0200\n"
"Last-Translator: WEB-Translator <http://kde.af.org.za>\n"
"Language-Team: AFRIKAANS <AF@lia.org.za>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: main.cpp:16
msgid "International Keyboard Selector"
msgstr "internasionaal sleutelbord kiesser"

#: main.cpp:17
msgid "Run time selector for keyboard layouton the desktop or on individual windows"
msgstr "hardloop tyd kiesser vir sleutelbord uitleg op die werkskerm of op individuele vensters"

#: main.cpp:21
msgid "Current maintainer"
msgstr "huidige onderhouer"

#: main.cpp:23
msgid "Original author"
msgstr "oorspronklike outeur"

#: main.cpp:24
msgid "Bug fixing"
msgstr "fout regmaak"

#: options.cpp:169
msgid "X default codes"
msgstr "x verstek kodes"

#: options.cpp:243
msgid ""
"%1\n"
"Do you want to configure now?"
msgstr ""
"%1\n"
"doen jy wil hê aan konfigureer nou?"

#: options.cpp:252
msgid ""
"You're using \"International Keyboard\" for the first time.\n"
"I installed a number of default settings."
msgstr ""
"jy is te gebruik \"internasionaal sleutelbord\" vir die eerste tyd.\n"
"i geïnstalleer 'n nommer van verstek instellings."

#: kikbd.cpp:172
msgid ""
"Only one instance of the international keyboard configuration\n"
"can run at a given moment."
msgstr ""
"slegs een voorbeeld van die internasionaal sleutelbord opstelling\n"
"kan hardloop na 'n gegewe oomblik."

#: kikbd.cpp:174
msgid "Already running"
msgstr "alreeds wat loop"

#: kikbd.cpp:239
msgid ""
"X codes \"%1\" file does not exist.\n"
"Using previously default X codes."
msgstr ""
"x kodes \"%1\" lêer doen nie bestaan.\n"
"te gebruik vorige verstek x kodes."

#: kikbd.cpp:266
msgid ""
"Keyboard map \"%1\" does not exists.\n"
"Skipping this map."
msgstr ""
"sleutelbord kaart \"%1\" doen nie bestaan.\n"
"ignoreer hierdie kaart."

#: kikbd.cpp:282
msgid "Setup"
msgstr "opstelling"

#: kikbd.cpp:304 kikbd.cpp:317
msgid "Can not remove %1 from Modifiers.\n"
msgstr "kan nie verwyder %1 van aanpassers.\n"

#: kikbd.cpp:307
msgid "Can not add %1 to Modifier number 3.\n"
msgstr "kan nie voeg by %1 aan modifiseerder nommer 3.\n"

#: kikbd.cpp:329
msgid ""
"Can not set Mode Switch as %1 for keyboard %2.\n"
"Alt symbols disabled"
msgstr ""
"kan nie stel modus wissel as %1 vir sleutelbord %2.\n"
"alt simbole gestremde"

#: kikbd.cpp:351
msgid ""
"Can't remove Caps Lock from Modifiers.\n"
"Caps Lock Emulation disabled"
msgstr ""
"kan nie verwyder caps sluit van aanpassers.\n"
"caps sluit emulering gestremde"

#: kikbd.cpp:1008
msgid ""
"You're about to quit the \"International Keyboard\" tool.\n"
" Do you want to have it autostarted at your next login?"
msgstr ""
"jy is aangaande aan verlaat die \"internasionaal sleutelbord\" program.\n"
" doen jy wil hê aan het dit outo begin na jou volgende aanteken?"

#: kikbd.cpp:1011
msgid "Exiting International Keyboard"
msgstr "besig om toe te maak internasionaal sleutelbord"

#: keysyms.cpp:18
msgid "KeyMap: can't allocate memory"
msgstr "sleutelbord kaart: kan nie toeken geheue"

#: keysyms.cpp:73
msgid ""
"Can't find symbol \"%1\".\n"
"Maybe your default X codes do not match.\n"
"You can preset X codes from section \"Advanced\"\n"
"in the configuration program."
msgstr ""
"kan nie soek simbool \"%1\".\n"
"miskien jou verstek x kodes doen nie ooreenstem.\n"
"jy kan vooraf stel x kodes van seksie \"gevorderde\"\n"
"in die opstelling program."

#: keysyms.cpp:78
msgid "KeyMap: can't find symbol \"%1\"."
msgstr "sleutelbord kaart: kan nie soek simbool \"%1\"."

#: keysyms.cpp:88
msgid "KeyMap: can't read keycode \"%1\"."
msgstr "sleutelbord kaart: kan nie lees sleutelkode \"%1\"."

#: keysyms.cpp:94
msgid "KeyMap: keycode \"%1\" is out of range."
msgstr "sleutelbord kaart: sleutelkode \"%1\" is uit van omvang."