# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Chih-Wei Huang <cwhuang@linux.org.tw, 2000
#
msgid ""
msgstr ""
"Project-Id-Version: krn\n"
"POT-Creation-Date: 2001-08-04 12:46+0200\n"
"PO-Revision-Date: 2001-10-13 17:00+CST\n"
"Last-Translator: Chih-Wei Huang <cwhuang@linux.org.tw>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: main.cpp:265 main.cpp:278 main.cpp:292 main.cpp:310 main.cpp:325
msgid "KRN-Missing Configuration info"
msgstr "KRN - 沒有找到配置資訊"

#: main.cpp:266
msgid "Please enter your email adress"
msgstr "請輸入你的電子郵件地址"

#: main.cpp:279
msgid "Please enter your real name"
msgstr "請輸入你的真實姓名"

#: main.cpp:293
msgid "Please enter your organization's name"
msgstr "請輸入你所在的組織名稱"

#: main.cpp:311
msgid "Please enter your NNTP server name"
msgstr "請輸入你使用的 NNTP 伺服器的名字"

#: main.cpp:326
msgid "Please enter your SMTP server name"
msgstr "請輸入你使用的 SMTP 伺服器的名字"

#: NNTP.cpp:132
msgid "Received %.2f KB"
msgstr "接收到 %.2f KB"

#: NNTP.cpp:549
#, c-format
msgid ""
"error getting group list\n"
"Server said %s\n"
msgstr ""
"無法取得討論群組列表\n"
"伺服器回應：%s\n"

#: NNTP.cpp:667
msgid "Getting Article"
msgstr "正在取得文章"

#: NNTP.cpp:815
msgid "Article Received"
msgstr "文章已收到"

#: groupdlg.cpp:190
msgid "KRN - Group List"
msgstr "KRN - 群組列表"

#: groupdlg.cpp:195
msgid "Connect to Server"
msgstr "連接到伺服器"

#: groupdlg.cpp:196
msgid "Disconnect From Server"
msgstr "從伺服器斷開"

#: groupdlg.cpp:197
msgid "Get Active File"
msgstr "取得活動文件"

#: artdlg.cpp:203 findArtDlg.cpp:77 groupdlg.cpp:198
msgid "Find"
msgstr ""

#: groupdlg.cpp:200
msgid "Post Queued Messages"
msgstr "發表排隊的信件"

#: groupdlg.cpp:202
msgid "Exit"
msgstr "退出"

#: groupdlg.cpp:207 groupdlg.cpp:214 groupdlg.cpp:227
msgid "Get Subjects"
msgstr "取得標題"

#: groupdlg.cpp:208 groupdlg.cpp:215 groupdlg.cpp:228
msgid "Get Articles"
msgstr "取得文章"

#: groupdlg.cpp:209 groupdlg.cpp:216 groupdlg.cpp:229
msgid "Get Tagged Articles"
msgstr "取得標記文章"

#: groupdlg.cpp:210 groupdlg.cpp:219 groupdlg.cpp:226
msgid "Catchup"
msgstr "全部標示為已讀"

#: groupdlg.cpp:217 groupdlg.cpp:224 groupdlg.cpp:279
msgid "(Un)Subscribe"
msgstr "(取消)訂閱"

#: groupdlg.cpp:218
msgid "Untag"
msgstr "取消標記"

#: groupdlg.cpp:225
msgid "(Un)Tag"
msgstr "(取消)標記"

#: artdlg.cpp:207 artdlg.cpp:266 groupdlg.cpp:231 groupdlg.cpp:284
msgid "Post New Article"
msgstr "發表新文章"

#: groupdlg.cpp:233
msgid "&Subscribed"
msgstr "訂閱[&S]"

#: groupdlg.cpp:234
msgid "&Tagged"
msgstr "標記的[&T]"

#: groupdlg.cpp:239
msgid "Identity..."
msgstr "身份..."

#: groupdlg.cpp:240
msgid "NNTP Options..."
msgstr "NNTP 選項..."

#: groupdlg.cpp:241
msgid "Expire Options..."
msgstr "過期選項..."

#: groupdlg.cpp:242
msgid "Printing Options..."
msgstr "打印選項..."

#: groupdlg.cpp:249
msgid "About KRN..."
msgstr "關於 KRN..."

#: groupdlg.cpp:261
msgid "&Newsgroup"
msgstr "新聞組[&N]"

#: groupdlg.cpp:268
msgid "Connect to server"
msgstr "連接到伺服器"

#: groupdlg.cpp:272
msgid "Disconnect from server"
msgstr "從伺服器斷開"

#: groupdlg.cpp:275
msgid "Get list of active groups"
msgstr "取得活動群組列表"

#: groupdlg.cpp:276
msgid "Find group"
msgstr "查找討論組"

#: groupdlg.cpp:282
msgid "Check for Unread Articles"
msgstr "檢查尚未閱讀文章"

#: groupdlg.cpp:421 groupdlg.cpp:695 groupdlg.cpp:1117
msgid "All Newsgroups."
msgstr "所有新聞群組."

#: groupdlg.cpp:537 groupdlg.cpp:679
msgid "Subscribed Newsgroups."
msgstr "訂閱的新聞群組."

#: groupdlg.cpp:613
msgid "Disconnected"
msgstr "切斷連線"

#: groupdlg.cpp:623
#, c-format
msgid "Connecting to server %s"
msgstr "正在連結伺服器 %s"

#: groupdlg.cpp:633
msgid "Connected to server - Posting not allowed"
msgstr "已連結到伺服器 - 不允許發表文章"

#: groupdlg.cpp:635
msgid "Connected to server - Posting allowed"
msgstr "已連結到伺服器 - 允許發表文章"

#: groupdlg.cpp:641 groupdlg.cpp:653
msgid "Can't connect to server"
msgstr "不能連結到伺服器"

#: groupdlg.cpp:643 groupdlg.cpp:655
msgid "Connection to server failed"
msgstr "到伺服器的連結失敗"

#: groupdlg.cpp:662
msgid "Authentication Failed"
msgstr "身份驗証失敗"

#: groupdlg.cpp:665
msgid "Connection to server failed: Authentication problem"
msgstr "到伺服器的連結失敗: 身份驗証有問題"

#: groupdlg.cpp:714
msgid ""
"The operation you requested needs a connection to the news server\n"
"Should I attempt one?"
msgstr ""
"你請求的操作需要連接到新聞服務器上\n"
"要嘗試連結嗎?"

#: groupdlg.cpp:977
msgid ""
"You don't have an active groups list.\n"
" get it from server?"
msgstr ""
"你沒有活動討論群組列表.\n"
"從伺服器上取得 ?"

#: groupdlg.cpp:994
msgid "Listing active newsgroups"
msgstr "列出正在活動新聞群組"

#: groupdlg.cpp:1191
msgid "I can't find the group you want!"
msgstr "找不到你要的討論群組 !"

#: groupdlg.cpp:1287 groupdlg.cpp:1300
msgid "Getting messages in "
msgstr "取得訊息 "

#: groupdlg.cpp:1313
msgid "Getting list of messages in "
msgstr "取得訊息列表 "

#: groupdlg.cpp:1363
msgid ""
"Lost the connection to the server\n"
"Want to reconnect?\n"
msgstr ""

#: artdlg.cpp:188 artdlg.cpp:202 artdlg.cpp:329
msgid "Download"
msgstr "下載"

#: artdlg.cpp:190 artdlg.cpp:213
msgid "Decode"
msgstr "解碼"

#: artdlg.cpp:191 artdlg.cpp:216 artdlg.cpp:312
msgid "UnTag"
msgstr "取消標記"

#: artdlg.cpp:192
msgid "Mark as read"
msgstr "標記為已讀"

#: artdlg.cpp:193
msgid "Mark as unread"
msgstr "標記為未讀"

#: artdlg.cpp:194 artdlg.cpp:217 artdlg.cpp:318
msgid "Don't expire"
msgstr "不過期"

#: artdlg.cpp:195 artdlg.cpp:218 artdlg.cpp:319
msgid "Allow expire"
msgstr "允許過期"

#: artdlg.cpp:204
msgid "Lookup in Altavista"
msgstr "在 Altavista 中查找"

#: artdlg.cpp:208 artdlg.cpp:299
msgid "Reply by Mail"
msgstr "用郵件回覆"

#: artdlg.cpp:209
msgid "Post Followup"
msgstr "發表討論"

#: artdlg.cpp:210
msgid "Post & Reply"
msgstr "發表和回覆"

#: artdlg.cpp:211 artdlg.cpp:305
msgid "Forward"
msgstr ""

#: artdlg.cpp:214 artdlgbehav.cpp:95
msgid "Toggle Tag"
msgstr ""

#: artdlg.cpp:215 artdlg.cpp:311
msgid "Tag"
msgstr "標記"

#: artdlg.cpp:220 artdlg.cpp:288
msgid "Tagged"
msgstr "已標記的"

#: artdlg.cpp:230
msgid "Show Only Unread Messages"
msgstr "隻顯示尚未讀過的訊息"

#: artdlg.cpp:232
msgid "Show Only Cached Messages"
msgstr "只顯示快取中的訊息"

#: artdlg.cpp:234
msgid "Show Locked Messages"
msgstr "顯示鎖住的訊息"

#: artdlg.cpp:236
msgid "Expunge"
msgstr "刪除"

#: artdlg.cpp:237
msgid "Appearance..."
msgstr "外觀..."

#: artdlg.cpp:238
msgid "Sorting..."
msgstr "排序中..."

#: artdlg.cpp:239
msgid "Behavior..."
msgstr ""

#: artdlg.cpp:243
msgid "Edit Rules"
msgstr "編輯規則"

#: artdlg.cpp:244
msgid "Update"
msgstr "更新"

#: artdlg.cpp:250
msgid "&Article"
msgstr "文章[&A]"

#: artdlg.cpp:252
msgid "&Scoring"
msgstr "記分[&S]"

#: artdlg.cpp:259
msgid "Previous Message"
msgstr "前一個訊息"

#: artdlg.cpp:261
msgid "Next Message"
msgstr "下一個訊息"

#: artdlg.cpp:263
msgid "Find Article"
msgstr "查找文章"

#: artdlg.cpp:268
msgid "Get Article List"
msgstr "取得文章列表"

#: artdlg.cpp:270
msgid "Refresh Display"
msgstr "更新顯示"

#: artdlg.cpp:287
msgid "Current"
msgstr "現在的"

#: artdlg.cpp:289
msgid "UnTagged"
msgstr "未標記的"

#: artdlg.cpp:290 fontsDlg.cpp:52
msgid "All"
msgstr "全部"

#: artdlg.cpp:291
msgid "Read"
msgstr "已讀"

#: artdlg.cpp:292
msgid "UnRead"
msgstr "未讀"

#: artdlg.cpp:295
msgid "Save Article"
msgstr "文章存檔"

#: artdlg.cpp:297
msgid "Print Article"
msgstr "列印文章"

#: artdlg.cpp:301
msgid "Post a Followup"
msgstr "發表討論"

#: artdlg.cpp:303
msgid "Post & Mail"
msgstr "發表和寄信[&M]"

#: artdlg.cpp:309
msgid "Tag Article"
msgstr "標記文章"

#: artdlg.cpp:316
msgid "Don't expire (keep in cache)"
msgstr "鎖定 (保存在快取中)"

#: artdlg.cpp:323
msgid "Decode Article"
msgstr "對文章進行解碼"

#: artdlg.cpp:325 artdlgbehav.cpp:98
msgid "Mark Read"
msgstr "標記為已讀"

#: artdlg.cpp:327
msgid "Mark UnRead"
msgstr "標記為未讀"

#: artdlg.cpp:518
msgid "Threading..."
msgstr "正在串接 ..."

#: artdlg.cpp:1407
msgid "KRN - Existing File"
msgstr "KRN - 已經存在的檔案"

#: artdlg.cpp:1408
msgid "The file you selected already exists"
msgstr "你所選中的檔案已經存在"

#: artdlg.cpp:1414
msgid "OverWrite"
msgstr "覆蓋"

#: artdlg.cpp:1416
msgid "Append"
msgstr "加到結尾"

#: artdlg.cpp:1449 artdlg.cpp:1509
msgid ""
"From: KRN\n"
"To: You\n"
"\n"
"Error getting article.\n"
"Server said:\n"
msgstr ""
"從: Krn\n"
"到: 你\n"
"\n"
"取文章時發生錯誤.\n"
"伺服器訊息:\n"

#: artdlg.cpp:1573
msgid "Getting Article List"
msgstr "正在取得文章列表"

#: identDlg.cpp:41
msgid "KRN-Identity Configuration"
msgstr "KRN - 身份配置"

#: identDlg.cpp:50
msgid "Here you should enter your personal information"
msgstr "你應該在這兒輸入你的個人資訊"

#: identDlg.cpp:52
msgid "You should know what these are!"
msgstr "你應該知道這些是什麼!"

#: identDlg.cpp:61
msgid "Real Name"
msgstr "真實姓名"

#: identDlg.cpp:67
msgid "Email Address"
msgstr "電子郵件地址"

#: identDlg.cpp:73
msgid "Organization"
msgstr "組織"

#: NNTPConfigDlg.cpp:42
msgid "KRN-NNTP configuration"
msgstr "KRN - NNTP 配置"

#: NNTPConfigDlg.cpp:47
msgid "Servers"
msgstr "伺服器"

#: NNTPConfigDlg.cpp:49
msgid "SMTP server"
msgstr "SMTP 伺服器"

#: NNTPConfigDlg.cpp:54
msgid "NNTP Server"
msgstr "NNTP 伺服器"

#: NNTPConfigDlg.cpp:78
msgid "Connect on Startup"
msgstr "在啟動時進行連結"

#: NNTPConfigDlg.cpp:82
msgid "Connect without asking"
msgstr "不需問就進行連結"

#: NNTPConfigDlg.cpp:86
msgid "Authenticate"
msgstr "身份驗証"

#: NNTPConfigDlg.cpp:91
msgid "User Name"
msgstr "使用者名稱"

#: NNTPConfigDlg.cpp:95
msgid "Password"
msgstr "密碼"

#: NNTPConfigDlg.cpp:102
msgid "Ask before downloading more than"
msgstr ""

#: NNTPConfigDlg.cpp:105
msgid "articles"
msgstr "文章"

#: aboutDlg.cpp:28
msgid "About KRN"
msgstr "關於 KRN"

#: aboutDlg.cpp:39
msgid "Part of the KDE project."
msgstr "KDE 計劃的一部分."

#: aboutDlg.cpp:42
msgid "Credits & Thanks"
msgstr "致謝"

#: aboutDlg.cpp:66
msgid "And many thanks to:"
msgstr "非常感謝:"

#: aboutDlg.cpp:72
msgid "All KDE developers"
msgstr "所有 KDE 發展成員"

#: aboutDlg.cpp:74
msgid "Chuck, for helping in time of need"
msgstr ""

#: aboutDlg.cpp:75
msgid "All testers"
msgstr "所有測試者"

#: fontsDlg.cpp:39
msgid "Message Display Settings"
msgstr "信件顯示設定"

#: fontsDlg.cpp:46
msgid "Header Style"
msgstr "標題風格"

#: fontsDlg.cpp:48
msgid "Fancy"
msgstr "非常花"

#: fontsDlg.cpp:49
msgid "Brief"
msgstr "簡潔"

#: fontsDlg.cpp:50
msgid "Standard"
msgstr "標準"

#: fontsDlg.cpp:51
msgid "Long"
msgstr "長"

#: fontsDlg.cpp:56
msgid "Attachment Style"
msgstr "附件檔案"

#: fontsDlg.cpp:58
msgid "Iconic"
msgstr "圖式"

#: fontsDlg.cpp:59
msgid "Smart"
msgstr "時髦"

#: fontsDlg.cpp:60
msgid "Inline"
msgstr "內嵌"

#: fontsDlg.cpp:67
msgid "Font Size:"
msgstr "字型大小:"

#: fontsDlg.cpp:70
msgid "Small"
msgstr "小"

#: fontsDlg.cpp:71
msgid "Normal"
msgstr "普通"

#: fontsDlg.cpp:72
msgid "Large"
msgstr "大"

#: fontsDlg.cpp:73
msgid "Huge"
msgstr "非常大"

#: fontsDlg.cpp:78
msgid "Standard Font"
msgstr "標準字體"

#: fontsDlg.cpp:84
msgid "Standard Font Test"
msgstr "標準字體測試"

#: fontsDlg.cpp:86
msgid "Fixed Font"
msgstr "等寬字體"

#: fontsDlg.cpp:91
msgid "Fixed Font Test"
msgstr "等寬字體測試"

#: fontsDlg.cpp:94
msgid "Background Color"
msgstr "背景顏色"

#: fontsDlg.cpp:97
msgid "Foreground Color"
msgstr "前景顏色"

#: fontsDlg.cpp:100
msgid "Link Color"
msgstr "連結顏色"

#: fontsDlg.cpp:103
msgid "Followed Color"
msgstr "過的顏色"

#: fontsDlg.cpp:107
msgid "Use only one window"
msgstr "只使用一個視窗"

#: fontsDlg.cpp:109
msgid "Split window vertically"
msgstr "垂直分割視窗"

#: kmmessage.cpp:1338
msgid "On %D, you wrote:"
msgstr ""

#: kmmessage.cpp:1339
msgid "On %D, %F wrote:"
msgstr ""

#: kmmessage.cpp:1340
msgid "Forwarded Message"
msgstr "轉寄訊息"

#: kmcomposewin.cpp:138
msgid "KMail Composer"
msgstr "KMail 編寫器"

#: kmcomposewin.cpp:149
msgid "F"
msgstr "F"

#: kmcomposewin.cpp:150
msgid "Name"
msgstr "姓名"

#: kmcomposewin.cpp:151
msgid "Size"
msgstr "大小"

#: kmcomposewin.cpp:152
msgid "Encoding"
msgstr "編碼"

#: kmcomposewin.cpp:153
msgid "Type"
msgstr "型態"

#: kmcomposewin.cpp:358
msgid "&From:"
msgstr "來自[&F]："

#: kmcomposewin.cpp:360
msgid "&Reply to:"
msgstr "回信到[&R]："

#: kmcomposewin.cpp:362
msgid "&To:"
msgstr "至[&T]："

#: kmcomposewin.cpp:364
msgid "&Cc:"
msgstr "副本受件人[&C]："

#: kmcomposewin.cpp:366
msgid "&Bcc:"
msgstr "隱藏副本受件人[&B]："

#: kmcomposewin.cpp:368
msgid "&Subject:"
msgstr "主旨[&S]："

#: kmcomposewin.cpp:371
msgid "&Newsgroups:"
msgstr "新聞群組[&N]："

#: kmcomposewin.cpp:373
msgid "&Followup-To:"
msgstr "回應至[&F]："

#: kmcomposewin.cpp:375
msgid "&Extra:"
msgstr ""

#: kmcomposewin.cpp:457
msgid "&Send"
msgstr "送出[&S]"

#: kmcomposewin.cpp:460
msgid "S&end later"
msgstr "稍後送出[&E]"

#: kmcomposewin.cpp:462
msgid "S&end now"
msgstr "現在送出[&e]"

#: kmcomposewin.cpp:464
msgid "&Insert File..."
msgstr "插入[&I]檔案"

#: kmcomposewin.cpp:468
msgid "&Addressbook..."
msgstr "通訊錄[&A]"

#: kmcomposewin.cpp:473
msgid "&New Composer..."
msgstr ""

#: kmcomposewin.cpp:476
msgid "New Mailreader"
msgstr "新的新聞閱讀器"

#: kmcomposewin.cpp:496 kmcomposewin.cpp:719 kmcomposewin.cpp:1889
msgid "Mark all"
msgstr "標記全部"

#: kmcomposewin.cpp:499 kmcomposewin.cpp:721
msgid "Find..."
msgstr "搜尋..."

#: kmcomposewin.cpp:501 kmcomposewin.cpp:722
msgid "Replace..."
msgstr "置換..."

#: kmcomposewin.cpp:504
msgid "&Spellcheck..."
msgstr "拼字檢查[&S]"

#: kmcomposewin.cpp:511
msgid "&Urgent"
msgstr "緊急[&U]"

#: kmcomposewin.cpp:513
msgid "&Confirm delivery"
msgstr ""

#: kmcomposewin.cpp:515
msgid "&Confirm read"
msgstr ""

#: kmcomposewin.cpp:518
msgid "&Spellchecker..."
msgstr ""

#: kmcomposewin.cpp:521
msgid "&Charsets..."
msgstr ""

#: kmcomposewin.cpp:534
msgid "&All Fields"
msgstr ""

#: kmcomposewin.cpp:536
msgid "&From"
msgstr "來自[&F]："

#: kmcomposewin.cpp:537
msgid "&Reply to"
msgstr "回信到[&R]："

#: kmcomposewin.cpp:538
msgid "&To"
msgstr "至[&T]："

#: kmcomposewin.cpp:539
msgid "&Cc"
msgstr "副本受件人[&C]："

#: kmcomposewin.cpp:540
msgid "&Bcc"
msgstr "隱藏副本受件人[&B]："

#: kmcomposewin.cpp:541
msgid "&Subject"
msgstr "主旨[&S]"

#: kmcomposewin.cpp:543
msgid "&Newsgroups"
msgstr "新聞群組[&N]"

#: kmcomposewin.cpp:544
msgid "&Followup-To"
msgstr "回應至[&F]"

#: kmcomposewin.cpp:545
msgid "&Extra"
msgstr "額外[&E]"

#: kmcomposewin.cpp:551
msgid "Append S&ignature"
msgstr ""

#: kmcomposewin.cpp:553
msgid "&Insert File"
msgstr "插入[&I]檔案"

#: kmcomposewin.cpp:555
msgid "Insert My &Public Key"
msgstr "加入我的 &Public Key"

#: kmcomposewin.cpp:560
msgid "&Attach..."
msgstr "附上[&A]"

#: kmcomposewin.cpp:563
msgid "&Save..."
msgstr "存檔[&S]"

#: kmcomposewin.cpp:564
msgid "Pr&operties..."
msgstr "屬性[&o]"

#: kmcomposewin.cpp:566
msgid "&Attach"
msgstr "附上[&A]"

#: kmcomposewin.cpp:585
msgid "Send message"
msgstr "送出信件"

#: kmcomposewin.cpp:590
msgid "Compose new message"
msgstr "編輯信件"

#: kmcomposewin.cpp:595
msgid "Save message to file"
msgstr "信件另存新檔"

#: kmcomposewin.cpp:600
msgid "Print message"
msgstr "列印信件"

#: kmcomposewin.cpp:609
msgid "Cut selection"
msgstr "剪下選項"

#: kmcomposewin.cpp:612
msgid "Copy selection"
msgstr "複製選項"

#: kmcomposewin.cpp:615
msgid "Paste clipboard contents"
msgstr "貼入剪貼簿目錄"

#: kmcomposewin.cpp:620
msgid "Attach file"
msgstr "附件檔案"

#: kmcomposewin.cpp:624
msgid "Open addressbook..."
msgstr "開啟通訊錄"

#: kmcomposewin.cpp:631
msgid "sign message"
msgstr "簽名內容"

#: kmcomposewin.cpp:636
msgid "encrypt message"
msgstr "內容編碼"

#: kmcomposewin.cpp:653 kmcomposewin.cpp:666
msgid "Line"
msgstr "行"

#: kmcomposewin.cpp:654 kmcomposewin.cpp:668
msgid "Column"
msgstr "列"

#: kmcomposewin.cpp:952
msgid ""
"Close and discard\n"
"edited message?"
msgstr ""
"關閉然後放棄\n"
"目前正在編輯的內容？"

#: kmcomposewin.cpp:1225
msgid "Attach File"
msgstr "附件檔案"

#: kmcomposewin.cpp:1247
msgid "Include File"
msgstr "引入檔"

#: kmcomposewin.cpp:1282 kmreaderwin.cpp:762
msgid "View..."
msgstr "瀏覽..."

#: kmcomposewin.cpp:1283
msgid "Save..."
msgstr "存檔..."

#: kmcomposewin.cpp:1284 kmreaderwin.cpp:765
msgid "Properties..."
msgstr "屬性..."

#: kmcomposewin.cpp:1288
msgid "Attach..."
msgstr "附件..."

#: kmcomposewin.cpp:1330 kmreaderwin.cpp:807
msgid "View Attachment: "
msgstr "檢視附件檔："

#: kmcomposewin.cpp:1501 kpgp.cpp:543
msgid ""
"Sorry, but this feature\n"
"is still missing"
msgstr ""
"抱歉 , 這個特色還沒有\n"
"實現"

#: kmcomposewin.cpp:1509
msgid "unnamed"
msgstr ""

#: kmcomposewin.cpp:1524
msgid "To:"
msgstr "至："

#: kmcomposewin.cpp:1528
msgid "Subject:"
msgstr "主旨："

#: kmcomposewin.cpp:1531
msgid "Date:"
msgstr "日期："

#: kmcomposewin.cpp:1572
msgid "Message sent or queued OK"
msgstr "信件已經送出或者放在待寄區內了"

#: kmcomposewin.cpp:1579
msgid "Error while sending or queuing message!"
msgstr "送出或者是放在待寄區內時發生錯誤!"

#: kmcomposewin.cpp:1617
msgid "Choose Signature File"
msgstr "選擇簽名檔"

#: kmcomposewin.cpp:2071
msgid "Spellcheck - KMail"
msgstr "拼字檢查—KMail"

#: kmcomposewin.cpp:2097
msgid "Error starting KSpell. Please make sure you have ISpell properly configured."
msgstr ""

#: kmsender.cpp:116
msgid ""
"Please specify a send\n"
"method in the settings\n"
"and try again."
msgstr ""
"請在設定中 \n"
"指定傳送方式 \n"
"並且再試一次"

#: kmsender.cpp:122
msgid ""
"Please set the required fields in the\n"
"identity settings:\n"
"user-name and email-address"
msgstr ""

#: kmsender.cpp:147
msgid "You must specify a receiver"
msgstr "你必須指定受件人"

#: kmsender.cpp:158
msgid "Cannot add message to outbox folder"
msgstr "無法新增信件至outbox檔案夾"

#: kmsender.cpp:189
msgid "Sending still in progress"
msgstr ""

#: kmsender.cpp:245
msgid "Initiating sender process..."
msgstr "初始化外送行程..."

#: kmsender.cpp:261
msgid "Sending message: "
msgstr "送出信件："

#: kmsender.cpp:289
msgid "Done sending messages."
msgstr "已送出信件 "

#: kmsender.cpp:308
msgid "Sending failed:"
msgstr "送出失敗："

#: kmsender.cpp:461
msgid ""
"Please specify a mailer program\n"
"in the settings."
msgstr "請指定郵件程式"

#: kmsender.cpp:501
#, c-format
msgid "Failed to execute mailer program %s"
msgstr "執行郵件程式 %s 失敗"

#: kmsender.cpp:589
msgid "connecting to server"
msgstr "正在連線到伺服器"

#: kmsender.cpp:594
#, c-format
msgid ""
"Cannot open SMTP connection to\n"
"host %s for sending:\n"
"%s"
msgstr ""
"無法開啟SMTP連線到 \n"
"主機 %s 以送出： \n"
"%s"

#: kmsender.cpp:623
msgid "Cannot close SMTP connection."
msgstr "無法結束SMTP連線。"

#: kmsender.cpp:673
msgid "transmitting message"
msgstr "正在傳送信件 "

#: kmsender.cpp:707
msgid "network error"
msgstr "網路發生錯誤"

#: kmsender.cpp:709
msgid ""
"a SMTP error occurred.\n"
"Command: %s\n"
"Response: %s\n"
"Return code: %d"
msgstr ""
"SMTP 發生錯誤。 \n"
"命令： %s\n"
"回應： %s\n"
"傳回碼： %d"

#: kmsender.cpp:724
#, c-format
msgid "Sending SMTP command: %s"
msgstr "送出 smtp 指令: %s"

#: findArtDlg.cpp:31 findArtDlg.cpp:34
msgid "KRN - Article Searcher"
msgstr "KRN - 文章搜尋者"

#: findArtDlg.cpp:41
msgid "Find Articles With:"
msgstr "尋找帶...的文章:"

#: findArtDlg.cpp:46 rulesDlg.cpp:107
msgid "In Field:"
msgstr "在域:"

#: findArtDlg.cpp:65 rulesDlg.cpp:132
msgid "Case Sensitive"
msgstr ""

#: findArtDlg.cpp:68 rulesDlg.cpp:133
msgid "Wildcard Mode"
msgstr "通配字元模式"

#: findArtDlg.cpp:78
msgid "Find All"
msgstr "尋找全部"

#: kmidentity.cpp:162
#, c-format
msgid ""
"Failed to execute signature script\n"
"%s\n"
"%s"
msgstr ""
"執行簽名命令稿失敗 \n"
"%s\n"
"%s"

#: kfileio.cpp:28 kmaddrbook.cpp:162
msgid "File I/O Error"
msgstr "檔案 I/O 錯誤"

#: kfileio.cpp:48
#, c-format
msgid ""
"The specified file does not exist:\n"
"%s"
msgstr ""
"這個檔案尚未建立： \n"
"%s"

#: kfileio.cpp:55
#, c-format
msgid ""
"This is a directory and not a file:\n"
"%s"
msgstr ""
"這是目錄不是檔案： \n"
"%s"

#: kfileio.cpp:62
msgid ""
"You do not have read permissions to the file:\n"
"%s"
msgstr "你無權讀此檔: %s"

#: kfileio.cpp:73 kmaddrbook.cpp:152
msgid ""
"Could not read file:\n"
"%s"
msgstr "無法讀入檔案：\n"
"%s"

#: kfileio.cpp:76 kmaddrbook.cpp:155
msgid ""
"Could not open file:\n"
"%s"
msgstr "無法開檔：\n"
"%s"

#: kfileio.cpp:79
msgid ""
"Error while reading file:\n"
"%s"
msgstr "讀入檔案時發生錯誤：\n"
"%s"

#: kfileio.cpp:96
#, c-format
msgid "Could only read %u bytes of %u."
msgstr "只能讀到 %u 位元組屬於 %u"

#: kfileio.cpp:125
#, c-format
msgid ""
"File %s exists.\n"
"Do you want to replace it?"
msgstr ""

#: kfileio.cpp:145
#, c-format
msgid ""
"Failed to make a backup copy of %s.\n"
"Continue anyway?"
msgstr ""

#: kfileio.cpp:157 kfileio.cpp:175
msgid ""
"Could not write to file:\n"
"%s"
msgstr "無法寫入檔案：\n"
"%s"

#: kfileio.cpp:160
msgid ""
"Could not open file for writing:\n"
"%s"
msgstr "無法開檔寫入：\n"
"%s"

#: kfileio.cpp:164 kmaddrbook.cpp:158
msgid ""
"Error while writing file:\n"
"%s"
msgstr ""
"寫入檔案發生錯誤：\n"
"%s"

#: kfileio.cpp:181
#, c-format
msgid "Could only write %d bytes of %d."
msgstr "共 %d 只能夠寫入 %d 位元組內容。"

#: kmmsgpartdlg.cpp:42
msgid "Message Part Properties"
msgstr ""

#: kmmsgpartdlg.cpp:63
msgid "Name:"
msgstr "名稱："

#: kmmsgpartdlg.cpp:74
msgid "Description:"
msgstr "描述："

#: kmmsgpartdlg.cpp:84
msgid "Encoding:"
msgstr "編碼："

#: kmmsgpartdlg.cpp:90
msgid "none (8-bit)"
msgstr "無 （8位元）"

#: kmmsgpartdlg.cpp:91
msgid "base 64"
msgstr "base64 編碼"

#: kmmsgpartdlg.cpp:92
msgid "quoted printable"
msgstr "qp 編碼"

#: kmreaderwin.cpp:320 kmreaderwin.cpp:340 kmreaderwin.cpp:367
#: kmreaderwin.cpp:396
msgid "Cc: "
msgstr "副本收件人"

#: kmreaderwin.cpp:325 kmreaderwin.cpp:347 kmreaderwin.cpp:376
#: kmreaderwin.cpp:406
msgid "Newsgroups: "
msgstr "新聞群組："

#: kmreaderwin.cpp:327 kmreaderwin.cpp:349 kmreaderwin.cpp:378
#: kmreaderwin.cpp:408
msgid "Followup-To: "
msgstr "回應至："

#: kmreaderwin.cpp:334 kmreaderwin.cpp:361 kmreaderwin.cpp:390
msgid "From: "
msgstr "來自："

#: kmreaderwin.cpp:337 kmreaderwin.cpp:364 kmreaderwin.cpp:393
msgid "To: "
msgstr "至："

#: kmreaderwin.cpp:343 kmreaderwin.cpp:372 kmreaderwin.cpp:402
msgid "Reply to: "
msgstr "回覆至："

#: kmreaderwin.cpp:351 kmreaderwin.cpp:380 kmreaderwin.cpp:410
msgid "References: "
msgstr "參照："

#: kmreaderwin.cpp:369 kmreaderwin.cpp:389
msgid "Date: "
msgstr "日期："

#: kmreaderwin.cpp:399
msgid "Bcc: "
msgstr "隱藏副本收件人："

#: kmreaderwin.cpp:450
msgid "Encrypted message"
msgstr "加密信件"

#: kmreaderwin.cpp:456
msgid "Cannot decrypt message:"
msgstr "無法解密"

#: kmreaderwin.cpp:465
msgid "Message was signed by"
msgstr "信件簽名是"

#: kmreaderwin.cpp:466
msgid "Warning: Bad signature from"
msgstr "警告：錯誤的簽名來自"

#: kmreaderwin.cpp:476
msgid "unknown key ID"
msgstr "未知的 key ID"

#: kmreaderwin.cpp:726
msgid "Attachment: "
msgstr "附件檔案:"

#: kmreaderwin.cpp:838
msgid "Could not create temporary file"
msgstr "無法建立暫存檔"

#: kmreaderwin.cpp:859
#, c-format
msgid "Could not save temporary file %s"
msgstr "無法回存暫存檔 %s"

#: kmreaderwin.cpp:888
msgid "Could not save file"
msgstr "無法儲存檔案"

#: kpgp.cpp:152 kpgp.cpp:194
msgid ""
"Could not find PGP executable.\n"
"Please check your PATH is set correctly."
msgstr ""
"找不到 PGP 執行檔程式。\n"
"請確認您的 PATH 設定正確。"

#: kpgp.cpp:208
msgid "The pass phrase is missing."
msgstr "通行碼不見"

#: kpgp.cpp:288
msgid ""
"Could not find the public keys for the\n"
"recipients of this mail.\n"
"Message will not be encrypted."
msgstr ""
"沒找到這封信的公開金鑰， \n"
"這封信將不會加密。"

#: kpgp.cpp:295
msgid ""
"Could not find the public keys for\n"
msgstr ""
"無法找到公開金鑰給\n"

#: kpgp.cpp:306
msgid ""
"These persons will not be able to\n"
msgstr ""
"這些人不能夠\n"

#: kpgp.cpp:308
msgid ""
"This person will not be able to\n"
msgstr ""
"這個人不能夠\n"

#: kpgp.cpp:310
msgid "decrypt the message."
msgstr "將信件編碼"

#: kpgp.cpp:324
msgid ""
"You just entered an invalid passphrase.\n"
"Do you want to try again, continue and\n"
"leave the message unsigned, or cancel sending the message?"
msgstr ""
"您所指定的 passphrase 不正確。\n"
"您要再嘗試一次嗎，要讓信件沒簽名，\n"
"或者是取消寄信的動作？"

#: kpgp.cpp:328
msgid "Retry"
msgstr "重試"

#: kpgp.cpp:339
msgid ""
"Do you want to encrypt anyway, leave the\n"
"message as is, or cancel the message?"
msgstr ""

#: kpgp.cpp:343
msgid "Encrypt"
msgstr "加密"

#: kpgp.cpp:356
msgid ""
"Do you want to leave the message as is,\n"
"or cancel the message?"
msgstr ""

#: kpgp.cpp:714
msgid ""
"Please enter your\n"
"PGP passphrase"
msgstr ""
"請輸入您的 PGP passphrase"

#: kpgp.cpp:741
msgid "PGP Security Check"
msgstr "PGP安全檢查"

#: kpgp.cpp:762
msgid "Identity"
msgstr "身份"

#: kpgp.cpp:768
msgid "PGP User Identity:"
msgstr "PGP用戶身份："

#: kpgp.cpp:779
msgid "Store passphrase"
msgstr "儲存通行碼"

#: kpgp.cpp:785
msgid "Always encrypt to self"
msgstr ""

#: krnsender.cpp:46 krnsender.cpp:118 krnsender.cpp:169
msgid ""
"You are posting an empty article\n"
"That sucks. Are you forcing me to post it anyway?"
msgstr ""

#: krnsender.cpp:67
msgid "This server is read only, you can't post here!"
msgstr "這個伺服器是唯讀的 , 你不能在這兒發表文章 !"

#: kmaddrbookdlg.cpp:21
msgid "Addressbook"
msgstr "通訊錄"

#: kmaddrbookdlg.cpp:96
msgid "Addressbook Manager"
msgstr "通訊錄總管"

#: kmaddrbookdlg.cpp:179
msgid "Storing addressbook failed"
msgstr "儲存通訊錄失敗"

#: kmaddrbook.cpp:26
msgid ""
"Storing the addressbook failed!\n"
msgstr ""
"儲存通訊錄失敗!\n"

#: expireDlg.cpp:21 expireDlg.cpp:26
msgid "KRN - Expire Settings"
msgstr "KRN - 過期設定"

#: expireDlg.cpp:35
msgid "For how long should we keep things (in days)?"
msgstr "信件保留期限(天) ?"

#: expireDlg.cpp:45
msgid "Read:"
msgstr "已讀:"

#: expireDlg.cpp:46
msgid "Unread:"
msgstr "未讀:"

#: expireDlg.cpp:49
msgid "Article Bodies:"
msgstr "文章主體:"

#: expireDlg.cpp:54
msgid "Article Headers:"
msgstr "文章標題:"

#: printdlg.cpp:21
msgid "KRN - Print Settings"
msgstr "KRN - 列印配置"

#: printdlg.cpp:26
msgid "KRN - Printing Settings"
msgstr "KRN - 列印配置"

#: printdlg.cpp:33
msgid "General"
msgstr "一般"

#: printdlg.cpp:35
msgid "Printing System"
msgstr "列印系統"

#: printdlg.cpp:37
msgid "Use HTML Printing"
msgstr "使用 HTML 列印"

#: printdlg.cpp:40
msgid "Use Direct Printing"
msgstr "使用直接列印"

#: printdlg.cpp:45
msgid "Direct Printing"
msgstr "直接列印"

#: printdlg.cpp:48
msgid "Print using:"
msgstr "列印時用:"

#: printdlg.cpp:52
msgid "Feed it with:"
msgstr "送給:"

#: printdlg.cpp:54
msgid "Postscript"
msgstr "Postscript"

#: printdlg.cpp:55
msgid "HTML"
msgstr "HTML"

#: printdlg.cpp:56
msgid "Text"
msgstr "文本"

#: rulesDlg.cpp:39
msgid "KRN - Scoring Rules Editor"
msgstr "KRN - 記分規則編輯器"

#: rulesDlg.cpp:46
msgid "Global Rules"
msgstr "全局規則"

#: rulesDlg.cpp:48
msgid "All Rules"
msgstr "所有規則"

#: rulesDlg.cpp:64
msgid "Active Rules"
msgstr "活動規則"

#: rulesDlg.cpp:71
msgid "Rule Editor"
msgstr "規則編輯器"

#: rulesDlg.cpp:88
msgid "Save as"
msgstr "另存新檔"

#: rulesDlg.cpp:102
msgid "Match Articles With:"
msgstr "用 ... 匹配文章:"

#: rulesDlg.cpp:125
msgid "Weight:"
msgstr ""

#: rulesDlg.cpp:127
msgid "Negative"
msgstr ""

#: rulesDlg.cpp:145
msgid "Done"
msgstr "完成"

#: rulesDlg.cpp:191
msgid "Rule name:"
msgstr "規則名稱:"

#: sortDlg.cpp:27
msgid "KRN - Header Sorting"
msgstr "KRN - 標題排序"

#: artlist.cpp:28 sortDlg.cpp:36
msgid "Subject"
msgstr "主旨"

#: artlist.cpp:34 sortDlg.cpp:37
msgid "Score"
msgstr "得分"

#: artlist.cpp:38 sortDlg.cpp:38
msgid "Sender"
msgstr "發信人"

#: artlist.cpp:30 sortDlg.cpp:39
msgid "Date"
msgstr "日期"

#: artlist.cpp:32 sortDlg.cpp:40
msgid "Lines"
msgstr "行"

#: sortDlg.cpp:41
msgid "None"
msgstr "無"

#: sortDlg.cpp:43
msgid "Sort by:"
msgstr "排序按照:"

#: sortDlg.cpp:60
msgid "Use Threading"
msgstr "使用串連"

#: charsetsDlg.cpp:36
msgid "KRN - Charset Settings"
msgstr "KRN - 字元集配置"

#: charsetsDlg.cpp:38
msgid "KMail - Charset Settings"
msgstr "KMail - 字元集配置"

#: charsetsDlg.cpp:47
msgid "Message charset"
msgstr "信件字元集"

#: charsetsDlg.cpp:65
msgid "Composer charset"
msgstr "編輯器編碼"

#: charsetsDlg.cpp:83
msgid "Set as &default"
msgstr "設定成預設 [&D]"

#: charsetsDlg.cpp:88
msgid "&7-bit is ASCII"
msgstr "7-bit 是 ASSCII [&7]"

#: charsetsDlg.cpp:92
msgid "&Quote unknown characters"
msgstr ""

#: expirestatusdlg.cpp:35
msgid "Please wait a minute, I am expiring!"
msgstr "請稍待，正在處理過期文章..."

#: kpgpbase.cpp:317
msgid "Bad passphrase; couldn't sign"
msgstr "不正確的 passphrase，無法簽名"

#: kpgpbase.cpp:356
msgid "error running PGP"
msgstr "PGP 執行失敗"

#: kpgpbase.cpp:373 kpgpbase.cpp:664
msgid "Bad passphrase; couldn't decrypt"
msgstr "不正確的 passphrase，無法解密"

#: kpgpbase.cpp:384 kpgpbase.cpp:675
msgid "Do not have the secret key for this message"
msgstr "沒有這個信件的秘密金鑰"

#: kpgpbase.cpp:412
msgid "unknown key ID "
msgstr ""

#: kpgpbase.cpp:432
msgid "??? (file ~/.pgp/pubring.pgp not found)"
msgstr ""

#: kpgpbase.cpp:587
msgid "The passphrase you entered is invalid."
msgstr "你輸入錯誤的通行碼"

#: kpgpbase.cpp:613
msgid ""
"The key(s) you want to encrypt your message\n"
"to are not trusted. No encryption done."
msgstr ""
"您目前要用來加密信件的鑰匙是不被信任的\n"
"沒有加密處理完成。"

#: kpgpbase.cpp:616
msgid ""
"The following key(s) are not trusted:\n"
"%s\n"
"They will not be able to decrypt the message"
msgstr ""
"下面這些鑰匙是不被信任的： \n"
"%s\n"
"它們將不能解密信件。"

#: TooManydlg.cpp:23
#, c-format
msgid "This group has %d articles."
msgstr ""

#: TooManydlg.cpp:35
msgid "KRN-Confirmation"
msgstr "KRN - 確認"

#: TooManydlg.cpp:42
msgid "Read how many articles?"
msgstr "讀進幾篇文章？"

#: TooManydlg.cpp:48
msgid "Get them from"
msgstr "從取得它們"

#: TooManydlg.cpp:49
msgid "Oldest articles"
msgstr "最舊的文章"

#: TooManydlg.cpp:51
msgid "Newest articles"
msgstr "新進文章"

#: TooManydlg.cpp:55
msgid "Make this the new threshold"
msgstr "在此製造新的開端"

#: secondgrlist.cpp:14
msgid "Search results"
msgstr ""

#: secondgrlist.cpp:19
msgid ""
"\n"
"Enter group name (you can use wildcards)\n"
msgstr ""

#: secondgrlist.cpp:34
msgid "&Search"
msgstr ""

#: secondgrlist.cpp:46
msgid ""
"\n"
"Search results (subscribed first)\n"
msgstr ""

#: artlist.cpp:36
msgid "Status"
msgstr ""

#: artdlgbehav.cpp:94
msgid "Open Article"
msgstr "取得文章"

#: artdlgbehav.cpp:96
msgid "Do Nothing"
msgstr "什麼事都沒做"

#: artdlgbehav.cpp:97
msgid "Toggle Read"
msgstr "標記為讀取"

#: artdlgbehav.cpp:99
msgid "Mark Unread"
msgstr "標記為未讀"

#: artdlgbehav.cpp:111
msgid "KRN - Behavior"
msgstr "KRN - 行為"

#: artdlgbehav.cpp:116
msgid ""
"\n"
"Left Click\n"
msgstr ""

#: artdlgbehav.cpp:123 artdlgbehav.cpp:148 artdlgbehav.cpp:173
msgid "Online:"
msgstr "上線:"

#: artdlgbehav.cpp:127 artdlgbehav.cpp:152 artdlgbehav.cpp:177
msgid "Offline:"
msgstr "離線:"

#: artdlgbehav.cpp:141
msgid ""
"\n"
"Double Click\n"
msgstr ""
"\n"
"雙擊\n"

#: artdlgbehav.cpp:166
msgid ""
"\n"
"Right Click\n"
msgstr ""
"\n"
"右鍵點選\n"

#~ msgid "About Krn..."
#~ msgstr "關於 Krn..."

#~ msgid ""
#~ "Couldn't find PGP executable.\n"
#~ "Please check your PATH is set correctly."
#~ msgstr "無法發現可執行的PGP請檢查你的路徑是否正確"
