# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ksystrayproxy\n"
"POT-Creation-Date: 2001-08-22 18:54+0200\n"
"PO-Revision-Date: 2001-09-03 16:00+0800\n"
"Last-Translator: 李政諭 <znyuili@ms1.hinet.net>\n"
"Language-Team: zh-l10n <zh-l10n@linux.org.tw>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ksystrayproxy.cpp:62
msgid ""
"No window matching pattern '%1' and no command specified.\n"
msgstr ""
"沒有視窗符合樣式 '%1' 而且沒有指定指令.\n"

#: ksystrayproxy.cpp:69
msgid "KSysTrayCmd: KShellProcess can't find a shell."
msgstr "KSysTrayCmd: KShellProcess無法找到指令輸入器(shell)"

#: ksystrayproxy.cpp:189 main.cpp:41
msgid "KSysTrayCmd"
msgstr "KSysTrayCmd"

#: ksystrayproxy.cpp:190
msgid "&Hide"
msgstr "隱藏(&H)"

#: ksystrayproxy.cpp:190
msgid "&Restore"
msgstr "回復(&R)"

#: main.cpp:23
msgid "Command to execute."
msgstr "執行指令."

#: main.cpp:25
msgid ""
"A regular expression matching the window title.\n"
"If you do not specify one, then the very first window\n"
"to appear will be taken. Not recommended!"
msgstr ""
"一個標準表示法符合視窗標題.\n"
"如果您沒有指定其中一個,則顯示在最前頭之視窗將會被選"
"擇.\n"
"不建議此種情況!"

#: main.cpp:28
msgid "Hide the window to the tray on startup"
msgstr "啟動時將視窗縮小至狀態列"

#: main.cpp:29
msgid ""
"Wait until we're told to show the window before\n"
"executing the command"
msgstr "等待至被告之在執行指令前顯示視窗"

#: main.cpp:31
msgid "Sets the initial tooltip for the tray icon"
msgstr "設定狀態列圖示之初始工具提示"

#: main.cpp:32
msgid ""
"Keep the tray icon even if the client exits. This option\n"
"has no effect unless startonshow is specified."
msgstr ""
"當客戶端離開時仍保持狀態列圖示。\n"
"除非啟動顯示被指定否則此設定無效。"

#: main.cpp:43
msgid "Allows any application to be kept in the system tray."
msgstr "允許任何應用程式置於系統狀態列."

#: main.cpp:62
msgid "No command or window title specified"
msgstr "未指定指令或視窗標題"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "李政諭"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "znyuili@ms1.hinet.net"
