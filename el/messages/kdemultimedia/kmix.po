msgid ""
msgstr ""
"Project-Id-Version: kmix\n"
"POT-Creation-Date: 2001-06-10 23:22+0200\n"
"PO-Revision-Date: 1999-02-10 18:00+0200\n"
"Last-Translator: Yiannis Belias <orion@hellug.gr>\n"
"Language-Team: Greek <linux-howto@hellug.gr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: kmixapplet.cpp:169
msgid "Select mixer"
msgstr "Επιλογή Μείκτη"

#: kmix.cpp:448 kmixapplet.cpp:225
msgid "Mixers"
msgstr "Μείκτες"

#: kmix.cpp:449 kmixapplet.cpp:225
msgid "Available mixers"
msgstr "Διαθέσιμοι μείκτες"

#: kmix.cpp:456 kmixapplet.cpp:231
msgid "Invalid mixer entered."
msgstr ""

#: kmix.cpp:133
msgid "&New mixer tab"
msgstr ""

#: kmix.cpp:135
msgid "&Close mixer tab"
msgstr ""

#: kmix.cpp:137
msgid "&Restore default volumes"
msgstr ""

#: kmix.cpp:139
msgid "&Save current volumes as default"
msgstr ""

#: kmix.cpp:461
msgid "Description"
msgstr "Περιγραφή"

#: kmixctrl.cpp:35
msgid "kmixctrl - kmix volume save/restore utility"
msgstr ""

#: kmixctrl.cpp:40
msgid "Save current volumes as default"
msgstr ""

#: kmixctrl.cpp:42
msgid "Restore default volumes"
msgstr ""

#: kmixctrl.cpp:50
msgid "KMixCtrl"
msgstr "KMixCtrl"

#: kmixdockwidget.cpp:77
msgid "Volume at %1%"
msgstr ""

#: kmixerwidget.cpp:70 mixdevicewidget.cpp:80
#, fuzzy
msgid "Show &all"
msgstr "&Εμφάνιση όλων"

#: kmixerwidget.cpp:80
msgid "Invalid mixer"
msgstr ""

#: kmixerwidget.cpp:136
msgid "Left/Right balancing"
msgstr ""

#: kmixerwidget.cpp:198 mixdevicewidget.cpp:542
msgid "Device settings"
msgstr "Ρυθμίσεις Συσκευής"

#: kmixprefdlg.cpp:41
msgid "KMix Preferences"
msgstr "Προτιμήσεις KMix"

#: kmixprefdlg.cpp:49
msgid "&Dock into panel"
msgstr "Ενσωμάτωση στο Πάνελ"

#: kmixprefdlg.cpp:51
#, fuzzy
msgid "Docks the mixer into the KDE panel"
msgstr "Ενσωμάτωση στο Πάνελ"

#: kmixprefdlg.cpp:53
msgid "Enable System Tray &volume control"
msgstr ""

#: kmixprefdlg.cpp:61
msgid "Show &tickmarks"
msgstr "Εμφάνιση Κλίμακας"

#: kmixprefdlg.cpp:63
msgid "Enable/disable tickmark scales on the sliders"
msgstr ""

#: kmixprefdlg.cpp:65
#, fuzzy
msgid "Show &labels"
msgstr "Εμφάνιση"

#: kmixprefdlg.cpp:67
msgid "Enables/disables description labels above the sliders"
msgstr ""

#: kmixprefdlg.cpp:69
#, fuzzy
msgid "&General"
msgstr "&Γενικά"

#: main.cpp:32
msgid "KMix - KDE's full featured mini mixer"
msgstr ""

#: main.cpp:42
msgid "KMix"
msgstr "KMix"

#: main.cpp:44
msgid "(c) 2000 by Stefan Schimanski"
msgstr "(c) 2000 Stefan Schimanski"

#: main.cpp:48
msgid "Solaris port"
msgstr ""

#: main.cpp:49
msgid "SGI Port"
msgstr ""

#: main.cpp:50 main.cpp:51
msgid "*BSD fixes"
msgstr ""

#: main.cpp:52
msgid "ALSA port"
msgstr ""

#: main.cpp:53
msgid "HP/UX port"
msgstr ""

#: main.cpp:54
msgid "NAS port"
msgstr ""

#: mixdevicewidget.cpp:71
msgid "&Split channels"
msgstr "&Διαχωρισμός Καναλιών"

#: mixdevicewidget.cpp:74
msgid "&Hide"
msgstr "&Απόκρυψη"

#: mixdevicewidget.cpp:76
#, fuzzy
msgid "&Muted"
msgstr "&Κλείσιμο ήχου"

#: mixdevicewidget.cpp:84
msgid "Set &record source"
msgstr ""

#: mixdevicewidget.cpp:88
msgid "Define &keys"
msgstr ""

#: mixdevicewidget.cpp:99
msgid "Increase volume"
msgstr ""

#: mixdevicewidget.cpp:100
msgid "Decrease volume"
msgstr ""

#: mixdevicewidget.cpp:101
msgid "Toggle mute"
msgstr ""

#: mixdevicewidget.cpp:140
msgid "Muting"
msgstr ""

#: mixdevicewidget.cpp:179
msgid "Recording"
msgstr ""

#: mixer.cpp:38 mixer_oss.cpp:61 mixer_oss.cpp:62
msgid "unknown"
msgstr ""

#: mixer.cpp:335
msgid ""
"kmix:You do not have permission to access the mixer device.\n"
"Please check your operating systems manual to allow the access."
msgstr ""

#: mixer.cpp:339
msgid "kmix: Could not write to mixer."
msgstr ""

#: mixer.cpp:342
msgid "kmix: Could not read from mixer."
msgstr ""

#: mixer.cpp:345
msgid "kmix: Your mixer does not control any devices."
msgstr ""

#: mixer.cpp:348
msgid ""
"kmix: Mixer does not support your platform. See mixer.cpp for porting hints "
"(PORTING)."
msgstr ""

#: mixer.cpp:351
msgid "kmix: Not enough memory."
msgstr ""

#: mixer.cpp:354
msgid ""
"kmix: Mixer cannot be found.\n"
"Please check that the soundcard is installed and that\n"
"the soundcard driver is loaded.\n"
msgstr ""

#: mixer.cpp:359
msgid ""
"kmix: Initial set is incompatible.\n"
"Using a default set.\n"
msgstr ""

#: mixer.cpp:363
msgid "kmix: Unknown error. Please report how you produced this error."
msgstr ""

#: mixer_oss.cpp:52
msgid "Volume"
msgstr ""

#: mixer_oss.cpp:52
msgid "Bass"
msgstr ""

#: mixer_oss.cpp:52
msgid "Treble"
msgstr ""

#: mixer_oss.cpp:53
msgid "Synth"
msgstr ""

#: mixer_oss.cpp:53
msgid "Pcm"
msgstr ""

#: mixer_oss.cpp:53
msgid "Speaker"
msgstr ""

#: mixer_oss.cpp:54
msgid "Line"
msgstr ""

#: mixer_oss.cpp:54 mixer_sun.cpp:70
msgid "Microphone"
msgstr ""

#: mixer_oss.cpp:54 mixer_sun.cpp:72
msgid "CD"
msgstr ""

#: mixer_oss.cpp:55
#, fuzzy
msgid "Mix"
msgstr "KMix"

#: mixer_oss.cpp:55
msgid "Pcm2"
msgstr ""

#: mixer_oss.cpp:55
msgid "RecMon"
msgstr ""

#: mixer_oss.cpp:56
msgid "IGain"
msgstr ""

#: mixer_oss.cpp:56
msgid "OGain"
msgstr ""

#: mixer_oss.cpp:56
msgid "Line1"
msgstr ""

#: mixer_oss.cpp:57
msgid "Line2"
msgstr ""

#: mixer_oss.cpp:57
msgid "Line3"
msgstr ""

#: mixer_oss.cpp:57
msgid "Digital1"
msgstr ""

#: mixer_oss.cpp:58
msgid "Digital2"
msgstr ""

#: mixer_oss.cpp:58
msgid "Digital3"
msgstr ""

#: mixer_oss.cpp:58
msgid "PhoneIn"
msgstr ""

#: mixer_oss.cpp:59
msgid "PhoneOut"
msgstr ""

#: mixer_oss.cpp:59
msgid "Video"
msgstr ""

#: mixer_oss.cpp:59
msgid "Radio"
msgstr ""

#: mixer_oss.cpp:60
msgid "Monitor"
msgstr ""

#: mixer_oss.cpp:60
msgid "3D-depth"
msgstr ""

#: mixer_oss.cpp:60
#, fuzzy
msgid "3D-center"
msgstr "&Κέντρο"

#: mixer_oss.cpp:62
msgid "unused"
msgstr ""

#: mixer_oss.cpp:196
msgid ""
"kmix: You do not have permission to access the mixer device.\n"
"Login as root and do a 'chmod a+rw /dev/mixer*' to allow the access."
msgstr ""

#: mixer_oss.cpp:200
msgid ""
"kmix: Mixer cannot be found.\n"
"Please check that the soundcard is installed and the\n"
"soundcard driver is loaded.\n"
"On Linux you might need to use 'insmod' to load the driver.\n"
"Use 'soundon' when using commercial OSS."
msgstr ""

#: mixer_sun.cpp:65
msgid "Master Volume"
msgstr ""

#: mixer_sun.cpp:66
msgid "Internal Speaker"
msgstr ""

#: mixer_sun.cpp:67
msgid "Headphone"
msgstr ""

#: mixer_sun.cpp:68
msgid "Line Out"
msgstr ""

#: mixer_sun.cpp:69
msgid "Record Monitor"
msgstr ""

#: mixer_sun.cpp:71
msgid "Line In"
msgstr ""

#: mixer_sun.cpp:233
msgid ""
"kmix: You do not have permission to access the mixer device.\n"
"Ask your system administrator to fix /dev/audioctl to allow access."
msgstr ""

#: rc.cpp:3
#, fuzzy
msgid "Preferences"
msgstr "Προτιμήσεις KMix"

#: rc.cpp:4
msgid "Colors"
msgstr ""

#: rc.cpp:5
msgid "&Custom colors"
msgstr ""

#: rc.cpp:6
msgid "&Default look"
msgstr ""

#: rc.cpp:7 rc.cpp:9
msgid "Background:"
msgstr ""

#: rc.cpp:8 rc.cpp:13
msgid "Silent:"
msgstr ""

#: rc.cpp:10 rc.cpp:12
msgid "Loud:"
msgstr ""

#: rc.cpp:11
msgid "Active"
msgstr ""

#: rc.cpp:14
#, fuzzy
msgid "Muted"
msgstr "&Κλείσιμο ήχου"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#~ msgid "&Apply"
#~ msgstr "&Εφαρμογή"

#, fuzzy
#~ msgid "Hardware Settings"
#~ msgstr "Ρυθμίσεις Εκκίνησης"

#, fuzzy
#~ msgid "Allow &docking"
#~ msgstr "Ενεργοποίηση ενσωμάτωσης"

#, fuzzy
#~ msgid "&Show all"
#~ msgstr "Δ&είξε Γραμμή Επιλογών"

#, fuzzy
#~ msgid "Minimize"
#~ msgstr "Ελαχιστοποίηση"

#, fuzzy
#~ msgid ""
#~ "\n"
#~ "(C) 1997-2000 by Christian Esken (esken@kde.org)\n"
#~ "\n"
#~ "Sound mixer panel for the KDE Desktop Environment.\n"
#~ "This program is in the GPL.\n"
#~ "GUI by Stefan Schimanski (1Stein@gmx.de)\n"
#~ "SGI Port done by Paul Kendall (paul@orion.co.nz).\n"
#~ "*BSD fixes by Sebestyen Zoltan (szoli@digo.inf.elte.hu)\n"
#~ "and Lennart Augustsson (augustss@cs.chalmers.se).\n"
#~ "ALSA port by Nick Lopez (kimo_sabe@usa.net).\n"
#~ "HP/UX port by Helge Deller (deller@gmx.de)."
#~ msgstr ""
#~ "\n"
#~ "(C) 1997-1998 από τον Christian Esken (esken@kde.org).\n"
#~ "\n"
#~ "Μείκτης ήχου για το περιβάλλον εργασίας KDE.\n"
#~ "Αυτό το πρόγραμμα βρίσκεται υπό το GPL.\n"
#~ "SGI Port από τον Paul Kendall (paul@orion.co.nz).\n"
#~ "Διορθώσεις *BSD από τους Sebestyen Zoltan (szoli@digo.inf.elte.hu)\n"
#~ "και Lennart Augustsson (augustss@cs.chalmers.se).\n"
#~ "ALSA port από τον Nick Lopez (kimo_sabe@usa.net)."

#~ msgid "Mixer channel setup"
#~ msgstr "Ρυθμίσεις καναλιών μείκτη"

#~ msgid "Device"
#~ msgstr "Συσκευή"

#~ msgid "Split"
#~ msgstr "Διαχωρισμός"

#~ msgid "&Hide Menubar"
#~ msgstr "&Απόκρυψη Γραμμής Επιλογών"

#~ msgid "&Tickmarks On/Off"
#~ msgstr "&Κλίμακα On/Off"

#~ msgid "&Options..."
#~ msgstr "&Επιλογές..."

#~ msgid "&Left"
#~ msgstr "&Αριστερά"

#~ msgid "&Right"
#~ msgstr "&Δεξιά"

#~ msgid "Un&mute"
#~ msgstr "&Επαναφορά ήχου"

#~ msgid "&Split"
#~ msgstr "&Διαχωρισμός"

#~ msgid "Un&split"
#~ msgstr "'&Ενωση"

#, fuzzy
#~ msgid "&Menubar"
#~ msgstr "Γραμμή Επιλογών"
