# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-08-29 09:51+0200\n"
"PO-Revision-Date: 2001-12-12 14:32GMT\n"
"Last-Translator: Thibaut Cousin <cousin@kde.org>\n"
"Language-Team: Français <kde-francophone@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: imgallerydialog.cpp:38
msgid "Create Image Gallery"
msgstr "Créer une gallerie d'images"

#: imgallerydialog.cpp:43
msgid "Page &Look"
msgstr "Appa&rence de la page"

#: imgallerydialog.cpp:47
msgid "Page Title:"
msgstr "Titre de la page :"

#: imgallerydialog.cpp:49
#, c-format
msgid "Image Gallery for %1"
msgstr "Gallerie d'image pour %1"

#: imgallerydialog.cpp:55
msgid "Images per Row :"
msgstr "Nombre d'images par ligne :"

#: imgallerydialog.cpp:65
msgid "Add Image Name"
msgstr "Ajouter le nom d'image"

#: imgallerydialog.cpp:69
msgid "Add Image File Size"
msgstr "Ajouter la taille du fichier image"

#: imgallerydialog.cpp:73
msgid "Add Image Dimension"
msgstr "Ajouter la dimension de l'image"

#: imgallerydialog.cpp:85
msgid "Font Name"
msgstr "Nom de la police"

#: imgallerydialog.cpp:105
msgid "Foreground Color:"
msgstr "Couleur du texte :"

#: imgallerydialog.cpp:115
msgid "Background Color:"
msgstr "Couleur du fond :"

#: imgallerydialog.cpp:121
msgid "&Directories"
msgstr "&Dossiers"

#: imgallerydialog.cpp:125
msgid "Save to:"
msgstr "Enregistrer dans :"

#: imgallerydialog.cpp:130
msgid "Copy &Original Files"
msgstr "Copier les fichiers &originaux"

#: imgallerydialog.cpp:134
msgid "Use comment file"
msgstr "Utiliser un fichier de commentaires"

#: imgallerydialog.cpp:139
msgid "Comments File:"
msgstr "Fichier de commentaire :"

#: imgalleryplugin.cpp:44
msgid "&Create Image Gallery"
msgstr "Créer une &galerie d'images"

#: imgalleryplugin.cpp:52
msgid "KImGalleryPlugin::slotCreateHtml: Program error, please report a bug."
msgstr ""
"KImGalleryPlugin::slotCreateHtml: erreur du programme. Veuillez signaler ce "
"bogue."

#: imgalleryplugin.cpp:56
msgid "Creating an image gallery works only on local directories."
msgstr ""
"La création d'une galerie d'images n'est possible qu'avec des dossiers "
"locaux."

#: imgalleryplugin.cpp:77
msgid "Creating thumbnails"
msgstr "Création des aperçus"

#: imgalleryplugin.cpp:108 imgalleryplugin.cpp:122
#, c-format
msgid "Couldn't create directory: %1"
msgstr "Impossible de créer le dossier «%1»"

#: imgalleryplugin.cpp:166
#, c-format
msgid "<i>Number of images</i>: %1"
msgstr "<i>Nombre d'images</i> : %1"

#: imgalleryplugin.cpp:167
#, c-format
msgid "<i>Created on</i>: %1"
msgstr "<i>Créé le</i> : %1"

#: imgalleryplugin.cpp:217
#, c-format
msgid ""
"Created thumbnail for: \n"
"%1"
msgstr ""
"Aperçus créés pour :\n"
"%1"

#: imgalleryplugin.cpp:219
msgid ""
"Creating thumbnail for: \n"
"%1\n"
" failed"
msgstr ""
"La création des aperçus pour :\n"
"%1\n"
"a échoué."

#: imgalleryplugin.cpp:257 imgalleryplugin.cpp:305
#, c-format
msgid "Couldn't open file: %1"
msgstr "Impossible d'ouvrir le fichier «%1»"

