msgid ""
msgstr ""
"POT-Creation-Date: 2001-06-23 20:39+0200\n"
"PO-Revision-Date: 2001-12-12 14:46GMT\n"
"Last-Translator: Thibaut Cousin <cousin@kde.org>\n"
"Language-Team: Français <kde-francophone@kde.org>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: kdm-appear.cpp:79
msgid "&Greeting:"
msgstr "&Bienvenue :"

#: kdm-appear.cpp:84
msgid ""
"This is the string KDM will display in the login window. You may want to put "
"here some nice greeting or information about the operating system.<p> KDM "
"will replace the string [HOSTNAME] with the actual host name of the computer "
"running the X server. Especially in networks this is a good idea."
msgstr ""
"Ceci est le texte que KDE affichera dans la fenêtre de connexion. Vous "
"pouvez mettre un message d'accueil chaleureux ou des informations sur le "
"système d'exploitation.<p> KDM remplacera la chaîne de caractères "
"[HOSTNAME] par le nom de l'ordinateur exécutant le serveur X. Ceci est "
"particulièrement adapté dans un réseau."

#: kdm-appear.cpp:98
msgid "Logo area:"
msgstr "Zone de logo :"

#: kdm-appear.cpp:103 kdm-conv.cpp:83 kdm-users.cpp:161
msgid "&None"
msgstr "&Aucun"

#: kdm-appear.cpp:104
msgid "Show cloc&k"
msgstr "Afficher l'horlog&e"

#: kdm-appear.cpp:105
msgid "Sho&w logo"
msgstr "Afficher le &logo"

#: kdm-appear.cpp:119
msgid ""
"You can choose to display a custom logo (see below), a clock or no logo at "
"all."
msgstr ""
"Vous pouvez afficher un logo personnalisé (voir ci-dessous), une horloge ou "
"rien du tout."

#: kdm-appear.cpp:125
msgid "&Logo:"
msgstr "L&ogo :"

#: kdm-appear.cpp:138
msgid ""
"Click here to choose an image that KDM will display. You can also drag and "
"drop an image onto this button (e.g. from Konqueror)."
msgstr ""
"Cliquez ici pour sélectionner l'image que KDM affichera. Vous pouvez aussi "
"glisser-déposer une image sur ce bouton (à partir de Konqueror par "
"exemple)."

#: kdm-appear.cpp:151
msgid "Position:"
msgstr "Position"

#: kdm-appear.cpp:156
msgid "Cente&red"
msgstr "Cent&rée"

#: kdm-appear.cpp:157
msgid "Spec&ify"
msgstr "Spécif&ier"

#: kdm-appear.cpp:169
msgid ""
"You can choose whether the login dialog should be centered or placed at "
"specified coordinates."
msgstr ""
"Vous pouvez choisir si la fenêtre de connexion au système doit être "
"centrée ou placée à un endroit précis."

#: kdm-appear.cpp:178
msgid "&X"
msgstr "&X"

#: kdm-appear.cpp:184
msgid "&Y"
msgstr "&Y"

#: kdm-appear.cpp:190
msgid "Here you specify the coordinates of the login dialog's <em>center</em>."
msgstr ""
"Indiquez ici les coordonnées du <em>centre</em> de la fenêtre de "
"connexion. "

#: kdm-appear.cpp:204
msgid "GUI S&tyle:"
msgstr "St&yle :"

#: kdm-appear.cpp:212
msgid "You can choose a basic GUI style here that will be used by KDM only."
msgstr ""
"Vous pouvez sélectionner un style d'affichage qui sera utilisé uniquement "
"par KDM."

#: kdm-appear.cpp:217
msgid "Echo &mode:"
msgstr "Mode d&'affichage :"

#: kdm-appear.cpp:220
msgid "No echo"
msgstr "Aucun"

#: kdm-appear.cpp:221
msgid "One Star"
msgstr "Une étoile"

#: kdm-appear.cpp:222
msgid "Three Stars"
msgstr "Trois étoiles"

#: kdm-appear.cpp:226
msgid "You can choose whether and how KDM shows your password when you type it."
msgstr ""
"Vous pouvez choisir si et comment KDM affiche votre mot de passe quand vous "
"le saisissez."

#: kdm-appear.cpp:232
msgid "Locale"
msgstr "Langue"

#: kdm-appear.cpp:238
msgid "Languag&e:"
msgstr "&Langue :"

#: kdm-appear.cpp:247
msgid ""
"Here you can choose the language used by KDM. This setting doesn't affect a "
"user's personal settings that will take effect after login."
msgstr ""
"Vous pouvez sélectionner ici la langue utilisée par KDM. Cette valeur ne "
"modifie pas les paramètres personnels qui seront pris en compte après la "
"connexion."

#: kdm-appear.cpp:291
msgid "without name"
msgstr "sans nom"

#: kdm-appear.cpp:386 kdm-users.cpp:281
msgid ""
"There was an error loading the image:\n"
"%1\n"
"It will not be saved..."
msgstr ""
"Un problème est survenu pendant le chargement de l'image :\n"
"%1\n"
"Elle ne sera pas enregistrée..."

#: kdm-appear.cpp:494
msgid ""
"<h1>KDM - Appearance</h1>"
" Here you can configure the basic appearance of the KDM login manager, i.e. "
"a greeting string, an icon etc.<p> For further refinement of KDM's "
"appearance, see the \"Font\" and \"Background\"  tabs."
msgstr ""
"<h1>Apparence de KDM</h1> Vous pouvez configurer ici l'apparence de base du "
"gestionnaire graphique de connexion KDM, c'est-à-dire le message d'accueil, "
"l'icône, etc.<p> Pour affiner l'apparence de KDM, consultez les onglets "
"«Polices» et «Fond d'écran»."

#: kdm-font.cpp:41
msgid "Select fonts"
msgstr "Sélection des polices"

#: kdm-font.cpp:42
msgid "Example"
msgstr "Exemple"

#: kdm-font.cpp:44
msgid "Shows a preview of the selected font."
msgstr "Affiche un aperçu de la police sélectionnée."

#: kdm-font.cpp:46
msgid "C&hange font..."
msgstr "Changer la p&olice"

#: kdm-font.cpp:50
msgid "Click here to change the selected font."
msgstr "Cliquez ici pour changer la police sélectionnée."

#: kdm-font.cpp:54
msgid "Greeting"
msgstr "Accueil"

#: kdm-font.cpp:55
msgid "Fail"
msgstr "Problème"

#: kdm-font.cpp:56
msgid "Standard"
msgstr "Standard"

#: kdm-font.cpp:59
msgid ""
"Here you can select the font you want to change. KDM knows three fonts: "
"<ul><li><em>Greeting:</em> used to display KDM's greeting string (see "
"\"Appearance\" tab)</li><li><em>Fail:</em> used to display a message when a "
"person fails to login</li><li><em>Standard:</em> used for the rest of the "
"text</li></ul>"
msgstr ""
"Vous pouvez sélectionner ici la police que vous voulez changer. KDM utilise "
"trois polices : <ul> <li><em>Accueil :</em>"
" utilisée pour afficher le texte de bienvenue de KDM (voir l'onglet "
"«Apparence»)</li> <li><em>Problème :</em> utilisée pour afficher un "
"message lorsque la connexion d'un utilisateur échoue</li> <li><em>Standard "
":</em> utilisée pour les autres textes</li> </ul>"

#: kdm-font.cpp:172
msgid "Greeting font"
msgstr "Police du message d'accueil"

#: kdm-font.cpp:176
msgid "Fail font"
msgstr "Police en cas de problème"

#: kdm-font.cpp:180
msgid "Standard font"
msgstr "Police standard"

#: kdm-sess.cpp:50
msgid "Allow shutdown"
msgstr "Autoriser l'arrêt du système"

#: kdm-sess.cpp:53
msgid "Co&nsole:"
msgstr "Cons&ole :"

#: kdm-sess.cpp:54 kdm-sess.cpp:60
msgid "Everybody"
msgstr "Tout le monde"

#: kdm-sess.cpp:55 kdm-sess.cpp:61
msgid "Only root"
msgstr "Uniquement le super-utilisateur"

#: kdm-sess.cpp:56 kdm-sess.cpp:62
msgid "Nobody"
msgstr "Personne"

#: kdm-sess.cpp:59
msgid "Re&mote:"
msgstr "Distan&t :"

#: kdm-sess.cpp:64
msgid ""
"Here you can select who is allowed to shutdown the computer using KDM. You "
"can specify different values for local (console) and remote displays. "
"Possible values are:<ul> <li><em>Everybody:</em> everybody can shutdown the "
"computer using KDM</li> <li><em>Only root:</em>"
" KDM will only allow shutdown after the user has entered the root "
"password</li> <li><em>Nobody:</em> nobody can shutdown the computer using "
"KDM</li></ul>"
msgstr ""
"Vous pouvez déterminer ici qui est autorisé à éteindre l'ordinateur en "
"utilisant KDM. Les valeurs possibles sont : <ul> <li><em>Tout le monde "
":</em> tout le monde peut arrêter l'ordinateur en utilisant KDM</li> "
"<li><em>Console seulement :</em> seuls les utilisateurs travaillant sur cet "
"ordinateur peuvent arrêter l'ordinateur en utilisant KDM</li> "
"<li><em>Uniquement le super-utilisateur :</em> KDM n'autorisera l'arrêt de "
"l'ordinateur qu'après que le mot de passe du super-utilisateur ait été "
"saisi</li> <li><em>Personne :</em>"
" personne ne peut arrêter l'ordinateur en utilisant KDM</li> </ul>"

#: kdm-sess.cpp:72
msgid "Commands"
msgstr "Commandes"

#: kdm-sess.cpp:75
msgid "Ha&lt"
msgstr "Arrêt&er"

#: kdm-sess.cpp:78
msgid "Command to initiate the system halt. Typical value: /sbin/halt"
msgstr "Commande pour arrêter le système. Exemple : /sbin/halt"

#: kdm-sess.cpp:83
msgid "&Reboot"
msgstr "&Redémarrer"

#: kdm-sess.cpp:86
msgid "Command to initiate the system reboot. Typical value: /sbin/reboot"
msgstr "Commande pour redémarrer le système. Exemple : /sbin/reboot"

#: kdm-sess.cpp:91
msgid "Lilo"
msgstr "Lilo"

#: kdm-sess.cpp:93
msgid "Show boot opt&ions"
msgstr "Afficher &les options de démarrage"

#: kdm-sess.cpp:98
msgid "Enable Lilo boot options in the \"Shutdown ...\" dialog."
msgstr ""
"Activer les options de démarrage de Lilo dans la boîte de dialogue "
"«Arrêter»."

#: kdm-sess.cpp:102
msgid "Lilo command"
msgstr "Commande de Lilo"

#: kdm-sess.cpp:105
msgid "Command to run Lilo. Typical value: /sbin/lilo"
msgstr "Commande pour lancer Lilo. Exemple : /sbin/lilo"

#: kdm-sess.cpp:110
msgid "Lilo map file"
msgstr "Carte de démarrage de Lilo"

#: kdm-sess.cpp:113
msgid "Position of Lilo's map file. Typical value: /boot/map"
msgstr "Emplacement de la carte de démarrage de Lilo, habituellement «/boot/map»."

#: kdm-sess.cpp:118
msgid "Session types"
msgstr "Type de session"

#: kdm-sess.cpp:121
msgid "New t&ype"
msgstr "Nouveau t&ype"

#: kdm-sess.cpp:128
msgid ""
"To create a new session type, enter its name here and click on <em>Add "
"new</em>"
msgstr ""
"Pour créer un nouveau type de session, saisissez ici son nom et cliquez sur "
"<em>Ajouter</em>"

#: kdm-sess.cpp:132
msgid "Add ne&w"
msgstr "A&jouter"

#: kdm-sess.cpp:136
msgid ""
"Click here to add the new session type entered in the <em>New type</em> "
"field to the list of available sessions."
msgstr ""
"Cliquez ici pour ajouter le nouveau type de session saisi dans le champ "
"<em>Nouveau type</em> dans la liste des sessions disponibles."

#: kdm-sess.cpp:139
msgid "Available &types"
msgstr "Types disponi&bles"

#: kdm-sess.cpp:142
msgid ""
"This box lists the available session types that will be presented to the "
"user. Names other than \"default\" and \"failsafe\" are usually treated as "
"program names, but it depends on your Xsession script what the session type "
"means."
msgstr ""
"Cette liste contient les types de session actuellement définis qui seront "
"proposés à l'utilisateur. Les noms autres que «default» et «failsafe» "
"sont en général des noms de programme, mais cela peut dépendre de la "
"façon dont est conçu votre script Xsession."

#: kdm-sess.cpp:148
msgid "R&emove"
msgstr "&Supprimer"

#: kdm-sess.cpp:152
msgid "Click here to remove the currently selected session type"
msgstr "Cliquez ici pour supprimer le type de session sélectionné."

#: kdm-sess.cpp:163
msgid ""
"With these two arrow buttons, you can change the order in which the "
"available session types are presented to the user"
msgstr ""
"Avec ces flèches, vous pouvez changer l'ordre dans lequel les types de "
"session sont affichés."

#: backgnd.cpp:112
msgid "abcdefgh"
msgstr "abcdefgh"

#: backgnd.cpp:126
msgid ""
"Here you can see a preview of how KDM's background will look like using the "
"current settings. You can even set a background picture by dragging it onto "
"the preview (e.g. from Konqueror)."
msgstr ""
"Vous avez ici un aperçu de l'apparence du fond d'écran de KDM avec la "
"configuration courante. Vous pouvez aussi définir une image de fond en la "
"glissant-déposant (par exemple depuis Konqueror) sur l'aperçu."

#: backgnd.cpp:136
msgid "Back&ground"
msgstr "F&ond d'écran"

#: backgnd.cpp:145 backgnd.cpp:211 bgdialogs.cpp:677
msgid "&Mode:"
msgstr "&Mode :"

#: backgnd.cpp:154
msgid ""
"Here you can change the way colors are applied to KDM's background. Apart of "
"just a plain color you can choose gradients, custom patterns or a background "
"program (e.g. xearth)."
msgstr ""
"Vous pouvez changer ici la configuration des couleurs du fond de KDM. Vous "
"pouvez choisir parmi une couleur unie, des dégradés ou des motifs "
"personnalisés, ou même un programme de fond d'écran (xearth, par exemple)."

#: backgnd.cpp:160
msgid "Color &1:"
msgstr "Couleur &1 :"

#: backgnd.cpp:170
msgid ""
"By clicking on these buttons you can choose the colors KDM will use to paint "
"the background. If you selected a gradient or a pattern, you can choose two "
"colors."
msgstr ""
"Vous pouvez choisir ici les couleurs que KDM utilisera pour dessiner le fond "
"d'écran. Si vous avez choisi un dégradé ou un motif, vous pouvez choisir "
"deux couleurs."

#: backgnd.cpp:176
msgid "Color &2:"
msgstr "Couleur &2 :"

#: backgnd.cpp:191 backgnd.cpp:258
msgid "S&etup"
msgstr "Confi&gurer"

#: backgnd.cpp:197
msgid "Click here to setup a pattern or a background program."
msgstr "Cliquez ici pour configurer un motif ou un programme de fond d'écran."

#: backgnd.cpp:202
msgid "Wa&llpaper"
msgstr "Papier p&eint"

#: backgnd.cpp:220
msgid ""
"Here you can choose the way the selected picture will be used to cover KDM's "
"background. If you select \"No Wallpaper\", the colors settings will be used."
msgstr ""
"Vous pouvez choisir ici la façon dont l'image sélectionnée sera utilisée "
"pour couvrir le fond d'écran de KDM. Si vous sélectionnez «Pas de papier "
"peint», les paramètres de couleurs seront utilisés."

#: backgnd.cpp:225
msgid "&Wallpaper"
msgstr "Papier pein&t"

#: backgnd.cpp:235
msgid ""
"Here you can choose from several wallpaper pictures, i.e. pictures that have "
"been installed in the system's or your wallpaper directory. If you want to "
"use other pictures, you can either click on the \"Browse\" button or drag a "
"picture (e.g. from Konqueror) onto the preview."
msgstr ""
"Vous pouvez choisir ici parmi plusieurs images de papier peint, "
"c'est-à-dire qui ont été installées sont le système ou dans le dossier "
"des papiers peints. Si vous voulez utiliser d'autres images, vous pouvez "
"cliquer sur le bouton «Parcourir» ou glisser-déposer une image (par "
"exemple depuis Konqueror) sur l'aperçu."

#: backgnd.cpp:244
msgid "B&rowse"
msgstr "&Parcourir"

#: backgnd.cpp:250
msgid "Click here to choose a wallpaper using a file dialog."
msgstr ""
"Cliquez ici pour choisir un papier peint en utilisant le sélecteur de "
"fichier."

#: backgnd.cpp:252
msgid "Mul&tiple:"
msgstr "M&ultiple :"

#: backgnd.cpp:303
msgid "Flat"
msgstr "Unie"

#: backgnd.cpp:304 bgdialogs.cpp:361
msgid "Pattern"
msgstr "Motif"

#: backgnd.cpp:305
msgid "Background Program"
msgstr "Programme de fond"

#: backgnd.cpp:306
msgid "Horizontal Gradient"
msgstr "Dégradé horizontal"

#: backgnd.cpp:307
msgid "Vertical Gradient"
msgstr "Dégradé vertical"

#: backgnd.cpp:308
msgid "Pyramid Gradient"
msgstr "Dégradé pyramidal"

#: backgnd.cpp:309
msgid "Pipecross Gradient"
msgstr "Dégradé en croix"

#: backgnd.cpp:310
msgid "Elliptic Gradient"
msgstr "Dégradé elliptique"

#: backgnd.cpp:322
msgid "No Wallpaper"
msgstr "Pas de papier peint"

#: backgnd.cpp:323
msgid "Centred"
msgstr "Centré"

#: backgnd.cpp:324
msgid "Tiled"
msgstr "Mosaïque"

#: backgnd.cpp:325
msgid "Center Tiled"
msgstr "Mosaïque centrée"

#: backgnd.cpp:326
msgid "Centred Maxpect"
msgstr "Centré avec aspect max"

#: backgnd.cpp:327
msgid "Scaled"
msgstr "Adapté"

#: backgnd.cpp:591
msgid "Currently are only local wallpapers allowed."
msgstr "Seuls les papiers peints locaux peuvent être gérés pour le moment."

#: kdm-users.cpp:51
msgid "<default>"
msgstr "<par défaut>"

#: kdm-users.cpp:57
msgid "&Remaining users"
msgstr "Autres u&tilisateurs"

#: kdm-users.cpp:60
msgid "S&elected users"
msgstr "Utilisateurs sé&lectionnés"

#: kdm-users.cpp:63
msgid "No-sho&w users"
msgstr "Utilisateurs &à ne pas afficher"

#: kdm-users.cpp:75
msgid ""
"Click here to add the highlighted user on the left to the list of selected "
"users on the right, i.e. users that should in any case be shown in KDM's "
"user list."
msgstr ""
"Cliquez ici pour ajouter l'utilisateur en surbrillance à gauche dans la "
"liste des utilisateurs sélectionnés à droite, c'est-à-dire les "
"utilisateurs qui doivent toujours être affichés dans KDM."

#: kdm-conv.cpp:149 kdm-users.cpp:83
msgid "Click here to remove the highlighted user from the list of selected users."
msgstr ""
"Cliquez ici pour supprimer l'utilisateur en surbrillance de la liste des "
"utilisateurs sélectionnés."

#: kdm-users.cpp:93
msgid ""
"Click here to add the highlighted user on the left to the list of users "
"explicitly not shown by KDM."
msgstr ""
"Cliquez ici pour ajouter l'utilisateur en surbrillance à gauche dans la "
"liste des utilisateurs à ne pas afficher par KDM."

#: kdm-users.cpp:101
msgid ""
"Click here to remove the highlighted user from the list of users explicitly "
"not shown by KDM."
msgstr ""
"Cliquez ici pour supprimer l'utilisateur en surbrillance de la liste des "
"utilisateurs à ne pas afficher par KDM."

#: kdm-users.cpp:109
msgid ""
"This is the list of users for which no explicit show policy has been set, "
"i.e. they will only be shown by KDM if \"Show users\" is \"all but no-show\"."
msgstr ""
"Ceci est la liste des utilisateurs pour lesquels aucune règle explicite n'a "
"été définie, c'est-à-dire qu'ils seront affichés par KDM seulement si "
"l'option «Tous sauf ceux à ne pas afficher» a été cochée."

#: kdm-users.cpp:117
msgid "This is the list of users KDM will show in its login dialog in any case."
msgstr ""
"Ceci est la liste des utilisateurs que KDM affichera toujours dans la "
"fenêtre de connexion."

#: kdm-users.cpp:124
msgid ""
"This is the list of users KDM will not show in its login dialog. Users "
"(except for root) with a user ID less than the one specified in the \"Hide "
"UIDs below\" field will not be shown, too."
msgstr ""
"Ceci est la liste d'utilisateurs que KDM n'affichera pas dans la fenêtre de "
"connexion. Les utilisateurs (sauf le super-utilisateur) dont le numéro est "
"inférieur à celui spécifié dans le champ «Cacher les numéro "
"d'utilisateur inférieurs à» ne seront pas affichés non plus."

#: kdm-users.cpp:150
msgid "Click or drop an image here"
msgstr "Cliquez ou déposez une image ici"

#: kdm-users.cpp:152
msgid ""
"Here you can see the username of the currently selected user and the image "
"assigned to this user. Click on the image button to select from a list of "
"images or drag and drop your own image onto the button (e.g. from Konqueror)."
msgstr ""
"Vous voyez ici le nom de l'utilisateur sélectionné et l'image qui lui est "
"associée. Cliquez sur le bouton d'image pour sélectionner une autre image "
"dans une liste ou glissez-déposez un fichier d'image (par exemple à partir "
"de Konqueror) sur le bouton."

#: kdm-users.cpp:158
msgid "Show users"
msgstr "Afficher les utilisateurs"

#: kdm-users.cpp:162
msgid ""
"If this option is selected, KDM will not show any users. If one of the "
"alternative radio buttons is selected, KDM will show a list of users in its "
"login dialog, so users can click on their name and image rather than typing "
"in their login."
msgstr ""
"Si cette option est cochée, KDM n'affichera aucun utilisateur. Si un des "
"boutons radio est coché, KDM affichera une liste d'utilisateurs dans la "
"fenêtre de connexion, de telle manière que les utilisateurs peuvent "
"cliquer sur leur nom et leur image au lieu de saisir leur nom au clavier."

#: kdm-users.cpp:166
msgid "Selected onl&y"
msgstr "Sélecti&onnés seulement"

#: kdm-users.cpp:167
msgid ""
"If this option is selected, KDM will only show the users listed in the "
"\"selected users\" listbox in its login dialog."
msgstr ""
"Si cette option est cochée, la fenêtre de connexion de KDM contiendra "
"seulement les utilisateurs définis dans la zone «Utilisateurs "
"sélectionnés»."

#: kdm-users.cpp:169
msgid "A&ll but no-show"
msgstr "Tous sauf &ceux à ne pas afficher"

#: kdm-users.cpp:170
msgid ""
"If this option is selected, KDM will show all users but those who are listed "
"in the \"no-show users\" listbox or have user ID less then the one specified "
"in the \"Hide UIDs below\" field (except root)."
msgstr ""
"Si cette option est cochée, KDM affichera tous les utilisateurs, sauf ceux "
"qui sont explicitement définis dans la zone «Utilisateurs à ne pas "
"afficher » ou qui ont.un numéro d'utilisateur plus petit que le nombre "
"spécifié dans le champ «Cacher les numéro d'utilisateur inférieurs à» "
"(sauf le super-utilisateur)."

#: kdm-users.cpp:173
msgid "Sor&t users"
msgstr "Tri&er les utilisateurs"

#: kdm-users.cpp:175
msgid ""
"This option tells KDM to alphabetically sort the users shown in its login "
"dialog."
msgstr ""
"Cette option demande à KDM de trier alphabétiquement les utilisateurs "
"affichés dans la fenêtre de connexion."

#: kdm-users.cpp:179
msgid "Hide U&IDs below"
msgstr "Cacher les nu&méros inférieurs à"

#: kdm-users.cpp:180
msgid ""
"In \"All but no-show\"-mode all users with a user ID less than this number "
"are hidden. Note, that the root user is not affected by this and must be "
"explicitly listed in \"No-show users\" to be hidden."
msgstr ""
"Dans le mode «Tous sauf ceux à ne pas afficher», tous les utilisateurs "
"dont le numéro est plus petit que ce nombre sont cachés. Notez que le "
"super-utilisateur n'est pas concerné par ceci et doit être explicitement "
"listé dans «Utilisateurs à ne pas afficher» pour être caché."

#: kdm-users.cpp:227
msgid "Save image as default image?"
msgstr "Enregistrer l'image comme image pas défaut ?"

#: kdm-users.cpp:237
msgid ""
"There was an error saving the image:\n"
"%1\n"
msgstr ""
"Un problème est survenu pendant l'enregistrement de l'image :\n"
"%1\n"

#: kdm-conv.cpp:51
msgid "Automatic login"
msgstr "Connexion automatique"

#: kdm-conv.cpp:53
msgid "Enable au&to-login"
msgstr "Activer la &connexion automatique"

#: kdm-conv.cpp:54
msgid ""
"Turn on the auto-login feature. This applies only to KDM's graphical login. "
"Think twice before enabling this!"
msgstr ""
"Active la fonctionnalité de connexion automatique. Ceci ne s'applique qu'au "
"gestionnaire de connexion graphique KDM. Réfléchissez bien avant d'activer "
"ceci !"

#: kdm-conv.cpp:60
msgid "Truly automatic lo&gin"
msgstr "Connexion vraimen&t automatique"

#: kdm-conv.cpp:61
msgid ""
"When this option is on, the auto-login will be carried out immediately when "
"KDM starts (i.e., when your computer comes up). When this is off, you will "
"need to initiate the auto-login by crashing the X server (by pressing "
"alt-ctrl-backspace)."
msgstr ""
"Quand cette option est cochée, la connexion automatique sera active "
"immédiatement quand KDM démarrera (c'est-à-dire quand votre ordinateur "
"démarre). Quand ceci est inactif, vous devrez fermer fermer brutalement le "
"serveur X (en appuyant sur Ctrl-Alt-Correction) pour démarrer la connexion "
"automatique."

#: kdm-conv.cpp:69
msgid "Use&r:"
msgstr "Utilisat&eur :"

#: kdm-conv.cpp:71
msgid "Select the user to be logged in automatically from this list."
msgstr ""
"Sélectionnez l'utilisateur qui doit se connecter automatiquement dans cette "
"liste."

#: kdm-conv.cpp:80
msgid "Preselect User"
msgstr "Présélectionner un utilisateur"

#: kdm-conv.cpp:84
msgid "Prev&ious"
msgstr "Le dernier c&onnecté"

#: kdm-conv.cpp:85
msgid "Specif&y"
msgstr "Un utilisateur particu&lier"

#: kdm-conv.cpp:88
msgid "Us&er:"
msgstr "Utilisat&eur :"

#: kdm-conv.cpp:90
msgid "Select the user to be preselected for login from this list."
msgstr "Sélectionnez l'utilisateur qui sera présélectionné dans cette liste."

#: kdm-conv.cpp:97
msgid "Focus pass&word"
msgstr "Se p&lacer dans le champ du mot de passe"

#: kdm-conv.cpp:98
msgid ""
"When this option is on, KDM will place the cursor in the password field "
"instead of the login field after preselecting a user. This will save one key "
"press per login, if the user name is very seldom changed."
msgstr ""
"Lorsque cette option est cochée, KDM placera le curseur dans la zone de "
"saisie du mot de passe après avoir présélectionné un utilisateur. Cela "
"économise un appui sur une touche, ce qui peut être agréable si le nom "
"d'utilisateur change rarement."

#: kdm-conv.cpp:104
msgid "Password-less login"
msgstr "Connexion sans mot de passe"

#: kdm-conv.cpp:108
msgid "Enable password-&less logins"
msgstr "Acti&ver les connexions sans mot de passe"

#: kdm-conv.cpp:109
msgid ""
"When this option is checked, the users from the right list will be allowed "
"to log in without entering their password. This applies only to KDM's "
"graphical login. Think twice before enabling this!"
msgstr ""
"Quand cette option est cochée, les utilisateurs de la liste de droite "
"seront autorisés à se connecter sans saisir de mot de passe. Ceci ne "
"s'applique qu'au gestionnaire de connexion graphique KDM. Réfléchissez-y "
"à deux fois avant d'activer ceci !"

#: kdm-conv.cpp:117
msgid "Password re&quired"
msgstr "Mot de passe re&quis"

#: kdm-conv.cpp:122
msgid "This is the list of users which need to type their password to log in."
msgstr ""
"Ceci est la liste des utilisateurs qui ont besoin de saisir leur mot de "
"passe pour se connecter."

#: kdm-conv.cpp:126
msgid "S&kip password check"
msgstr "Ne pas vérifier le &mot de passe"

#: kdm-conv.cpp:131
msgid "This is the list of users which are allowed in without typing their password."
msgstr ""
"Ceci est la liste des utilisateurs qui sont autorisés à se connecter sans "
"saisir de mot de passe."

#: kdm-conv.cpp:141
msgid ""
"Click here to add the highlighted user on the left to the list of selected "
"users on the right, i.e. users that are allowed in without entering their "
"password."
msgstr ""
"Cliquez ici pour ajouter l'utilisateur en surbrillance à gauche dans la "
"liste des utilisateurs sélectionnés à droite, c'est-à-dire les "
"utilisateurs qui sont autorisés à se connecter sans saisir leur mot de "
"passe."

#: kdm-conv.cpp:157
msgid "Automatically log in again after &X server crash"
msgstr "Rétablir la connexion automatiquement après un planta&ge du serveur X"

#: kdm-conv.cpp:158
msgid ""
"When this option is on, a user will be logged in again automatically, when "
"his session is interrupted by an X server crash."
msgstr ""
"Quand cette option est cochée, un utilisateur sera reconnecté "
"automatiquement quand sa session est interrompue par un plantage du serveur "
"X."

#: main.cpp:62
msgid ""
"Sorry, but %1\n"
"does not seem to be an image file\n"
"Please use files with these extensions:\n"
"%2"
msgstr ""
"Désolé mais %1\n"
"ne semble pas être une image\n"
"Utilisez des fichiers ayant ces extensions :\n"
"%2"

#: main.cpp:87
msgid "A&ppearance"
msgstr "Appare&nce"

#: main.cpp:91
msgid "&Font"
msgstr "&Polices"

#: main.cpp:95
msgid "&Background"
msgstr "Fond d'&écran"

#: main.cpp:99
msgid "&Sessions"
msgstr "&Sessions"

#: main.cpp:103
msgid "&Users"
msgstr "&Utilisateurs"

#: main.cpp:108
msgid "Con&venience"
msgstr "Commo&dités"

#: main.cpp:123
msgid ""
"<h1>Login Manager</h1> In this module you can configure the various aspects "
"of the KDE Login Manager.  This includes the look and feel as well as the "
"users that can be selected for login. Note that you can only make changes if "
"you run the module with superuser rights. If you haven't started the KDE "
"Control Center with superuser rights (which is the completely right thing to "
"do, by the way), click on the <em>Modify</em> button to acquire superuser "
"rights. You will be asked for the superuser password.<h2>Appearance</h2> On "
"this tab page, you can configure how the Login Manager should look, which "
"language it should use, and which GUI style it should use. The language "
"settings made here have no influence on the user's language "
"settings.<h2>Font</h2>Here you can choose the fonts that the Login Manager "
"should use for various purposes like greetings and user names. "
"<h2>Background</h2>If you want to set a special background for the login "
"screen, this is where to do it.<h2>Sessions</h2> Here you can specify which "
"types of sessions the Login Manager should offer you for logging in and who "
"is allowed to shutdown/reboot the machine.<h2>Users</h2>On this tab page, "
"you can select which users the Login Manager will offer you for logging "
"in.<h2>Convenience</h2> Here you can specify a user to be logged in "
"automatically, users not needing to provide a password to log in, and other "
"features ideal for lazy people. ;-)"
msgstr ""
"<h1>Gestionnaire de connexion</h1> Dans ce module, vous pouvez configurer "
"plusieurs aspects du gestionnaire de connexion de KDE. Cela inclut "
"l'apparence ainsi que les utilisateurs qui peuvent être sélectionnés à "
"la connexion. Note : vous ne pouvez effectuer des changements que si vous "
"exécutez ce module en tant que super-utilisateur (ce qui est de toute "
"façon la chose à faire). Si vous n'avez pas lancé le centre de "
"configuration de KDE avec des droits de super-utilisateur (ce qui est aussi "
"la bonne façon de procéder), cliquez sur le bouton <em>Modifier</em> pour "
"obtenir ces droits. Le mot de passe du super-utilisateur vous sera demandé. "
"<h2>Apparence</h2> Sous cet onglet, vous pouvez configurer l'apparence du "
"gestionnaire de connexion, la langue et la style graphique à utiliser. La "
"langue choisie ici n'a aucune incidence sur celle choisie par tel ou tel "
"utilisateur. <h2>Polices</h2> Vous pouvez y choisir les polices que le "
"gestionnaire de connexion doit utiliser pour différents messages comme les "
"noms des utilisateurs ou le message d'accueil. <h2>Fond d'écran</h2>Ceci "
"est l'endroit pour définir un fond d'écran spécifique au gestionnaire de "
"connexion.<h2>Sessions</h2>"
" Vous pouvez y spécifier quels types de sessions seront proposés pour la "
"connexion et qui est autorisé à arrêter ou redémarrer la "
"machine.<h2>Utilisateurs</h2> Sous cet onglet, vous pouvez sélectionner "
"quels utilisateurs seront affichés dans la fenêtre de "
"connexion.<h2>Commodités</h2> Vous pouvez y configurer une connexion "
"automatique sous l'identité d'un utilisateur de votre choix, ainsi que la "
"liste des utilisateurs qui peuvent se connecter sans avoir à saisir de mot "
"de passe... et quelques autres fonctionnalités pour les paresseux !"

#: bgdialogs.cpp:45
msgid "Select Background Program"
msgstr "Sélection du programme de fond"

#: bgdialogs.cpp:51
msgid "Select Background Program:"
msgstr "Sélection du programme de fond :"

#: bgdialogs.cpp:58
msgid "Program"
msgstr "Programme"

#: bgdialogs.cpp:59 bgdialogs.cpp:362
msgid "Comment"
msgstr "Commentaire"

#: bgdialogs.cpp:60
msgid "Refresh"
msgstr "Rafraîchir"

#: bgdialogs.cpp:83 bgdialogs.cpp:385 bgdialogs.cpp:700
msgid "&Add..."
msgstr "A&jouter"

#: bgdialogs.cpp:89 bgdialogs.cpp:391
msgid "&Modify..."
msgstr "M&odifier"

#: bgdialogs.cpp:135
msgid "%1 min."
msgstr "%1 min."

#: bgdialogs.cpp:170
msgid ""
"Unable to remove the program! The program is global\n"
"and can only be removed by the System Administrator.\n"
msgstr ""
"Impossible de supprimer ce programme ! Il fait partie du\n"
"système et ne peut être supprimé que par le super-utilisateur.\n"

#: bgdialogs.cpp:172
msgid "Cannot remove program"
msgstr "Impossible de supprimer le programme"

#: bgdialogs.cpp:176
msgid "Are you sure you want to remove the program `%1'?"
msgstr "Voulez-vous vraiment supprimer le programme «%1» ?"

#: bgdialogs.cpp:227
msgid "Configure Background Program"
msgstr "Configuration du programme de fond"

#: bgdialogs.cpp:235 bgdialogs.cpp:540
msgid "&Name:"
msgstr "&Nom :"

#: bgdialogs.cpp:241 bgdialogs.cpp:546
msgid "&Comment:"
msgstr "&Commentaire :"

#: bgdialogs.cpp:247
msgid "&Command:"
msgstr "&Commande :"

#: bgdialogs.cpp:253
msgid "&Preview cmd:"
msgstr "Commande d'a&perçu :"

#: bgdialogs.cpp:259
msgid "&Executable:"
msgstr "&Exécutable :"

#: bgdialogs.cpp:265
msgid "&Refresh time:"
msgstr "Délai de &rafraîchissement :"

#: bgdialogs.cpp:270 bgdialogs.cpp:673
msgid " minutes"
msgstr " minutes"

#: bgdialogs.cpp:277
msgid "New Command"
msgstr "Nouvelle commande"

#: bgdialogs.cpp:280
msgid "New Command <%1>"
msgstr "Nouvelle commande <%1>"

#: bgdialogs.cpp:307 bgdialogs.cpp:601
msgid ""
"You did not fill in the `Name' field.\n"
"This is a required field."
msgstr ""
"Vous n'avez pas rempli le champ «Nom».\n"
"C'est un champ obligatoire."

#: bgdialogs.cpp:315
msgid ""
"There is already a program with the name `%1'.\n"
"Do you want to overwrite it?"
msgstr ""
"Un programme possède déjà le nom «%1».\n"
"Voulez-vous l'écraser ?"

#: bgdialogs.cpp:322
msgid ""
"You did not fill in the `Executable' field.\n"
"This is a required field."
msgstr ""
"Vous n'avez pas rempli le champ «Exécutable».\n"
"C'est un champ obligatoire."

#: bgdialogs.cpp:327
msgid ""
"You did not fill in the `Command' field.\n"
"This is a required field."
msgstr ""
"Vous n'avez pas rempli le champ «Commande».\n"
"C'est un champ obligatoire."

#: bgdialogs.cpp:348
msgid "Select Background Pattern"
msgstr "Sélection du programme de fond"

#: bgdialogs.cpp:354
msgid "Select Pattern:"
msgstr "Sélection du motif :"

#: bgdialogs.cpp:363
msgid "Preview"
msgstr "Aperçu"

#: bgdialogs.cpp:480
msgid ""
"Unable to remove the pattern! The pattern is global\n"
"and can only be removed by the System Administrator.\n"
msgstr ""
"Impossible de supprimer le motif ! Il fait partie du système\n"
"et ne peut être supprimé que par le super-utilisateur.\n"

#: bgdialogs.cpp:482
msgid "Cannot remove pattern"
msgstr "Impossible de supprimer le motif"

#: bgdialogs.cpp:486
msgid "Are you sure you want to remove the pattern `%1'?"
msgstr "Voulez-vous vraiment supprimer le motif «%1» ?"

#: bgdialogs.cpp:533
msgid "Configure Background Pattern"
msgstr "Configuration du motif de fond"

#: bgdialogs.cpp:552
msgid "&Image:"
msgstr "&Image :"

#: bgdialogs.cpp:559
msgid "&Browse..."
msgstr "&Parcourir"

#: bgdialogs.cpp:565
msgid "New Pattern"
msgstr "Nouveau motif"

#: bgdialogs.cpp:568
msgid "New Pattern <%1>"
msgstr "Nouveau motif <%1>"

#: bgdialogs.cpp:609
msgid ""
"There is already a pattern with the name `%1'.\n"
"Do you want to overwrite it?"
msgstr ""
"Un motif possède déjà le nom «%1».\n"
"Voulez-vous l'écraser ?"

#: bgdialogs.cpp:616
msgid ""
"You did not fill in the `Image' field.\n"
"This is a required field."
msgstr ""
"Vous n'avez pas rempli le champ «Image».\n"
"C'est un champ obligatoire."

#: bgdialogs.cpp:656
msgid "Configure Wallpapers"
msgstr "Configuration du papier peint"

#: bgdialogs.cpp:668
msgid "&Interval:"
msgstr "&Intervalle :"

#: bgdialogs.cpp:679
msgid "In Order"
msgstr "Dans l'ordre"

#: bgdialogs.cpp:680
msgid "Random"
msgstr "Aléatoire"

#: bgdialogs.cpp:686
msgid "You can select files and directories below:"
msgstr "Vous pouvez sélectionner les fichiers et les dossiers ci-dessous :"

#: bgdialogs.cpp:711
msgid "Select"
msgstr "Sélection"

