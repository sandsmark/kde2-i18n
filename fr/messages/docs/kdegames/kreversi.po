# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-08-30 22:29GMT\n"
"Last-Translator: Gérard Delafond <gerard@delafond.org>\n"
"Language-Team: Français <kde-francophone@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.4\n"

#: index.docbook:10
msgid "The &kreversi; Handbook"
msgstr "Le manuel de &kreversi;"

#: index.docbook:14
msgid ""
"<firstname>Clay</firstname> <surname>Pradarits</surname> <affiliation> "
"<address><email>gasky@winfire.com</email></address> </affiliation>"
msgstr ""
"<firstname>Clay</firstname> <surname>Pradarits</surname> <affiliation> "
"<address><email>gasky@winfire.com</email></address> </affiliation>"

#: index.docbook:27
msgid "Developer"
msgstr "Développeur"

#: index.docbook:33
msgid "Reviewer"
msgstr "Relecture"

#: index.docbook:39
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"><firstname>Éric</firstname> "
"<surname>Bischoff</surname><affiliation><address><email>"
"e.bischoff@noos.fr</email></address></affiliation><contrib>Traduction "
"française</contrib></othercredit>"

#: index.docbook:46
msgid "&kreversi; is a simple one player strategy game."
msgstr "&kreversi; est un jeu de stratégie simple pour un joueur."

#: index.docbook:50
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:51
msgid "kdegames"
msgstr "kdegames"

#: index.docbook:52
msgid "game"
msgstr "jeu"

#: index.docbook:53
msgid "KReversi"
msgstr "Othello"

#: index.docbook:58
msgid "Introduction"
msgstr "Introduction"

#: index.docbook:61
msgid "What is &kreversi;?"
msgstr "Qu'est-ce que &kreversi;&nbsp;?"

#: index.docbook:63
msgid ""
"&kreversi; is a simple one player strategy game played against the computer. "
"The playing field is an 8 by 8 square board divided into 64 squares. The "
"game piece used is a colored stone - one side of it is red, the other blue. "
"If a piece is captured by an opposing player, that piece is turned to reveal "
"the color of that player. A winner is declared when one player has more "
"pieces of his own color on the board and if there are no more possible moves."
msgstr ""
"&kreversi; est un jeu de stratégie simple qui se joue seul contre "
"l'ordinateur. Le plateau de jeu se compose de 64 cases disposées en un "
"carré de 8 cases sur 8. Les pions sont des pièces dont un côté est rouge "
"et l'autre bleu. Si une pièce est capturée par l'adversaire, cette pièce "
"est retournée de manière à montrer la couleur de cet adversaire. Est "
"déclaré vainqueur celui qui a plus de pièces que son adversaire lorsque "
"plus aucun coup ne peut être joué."

#: index.docbook:74
msgid "Object of the game"
msgstr "But du jeu"

#: index.docbook:76
msgid "The object of the game is to control the majority of squares on the board."
msgstr "Le but du jeu est de contrôler la plupart des cases du plateau de jeu."

#: index.docbook:85
msgid "Playing"
msgstr "Déroulement de la partie"

#: index.docbook:88
msgid "Basics"
msgstr "Notions élémentaires"

#: index.docbook:90
msgid ""
"A move consists of outflanking your opponents disc(s), then flipping the "
"outflanked disc(s) to reveal your color. A move is performed by placing the "
"mouse pointer over the desired square then &LMB; click."
msgstr ""
"Un coup consiste à encadrer un ou plusieurs pions de votre adversaire, puis "
"de retourner ces pions pour qu'ils montrent votre couleur. Un coup se joue "
"en plaçant le pointeur de la souris par-dessus la case désirée puis à "
"cliquer avec le &BGS;."

#: index.docbook:94
msgid ""
"To outflank means to place a disc on the board so that your opponent's "
"row(s) of disc(s) is bordered at each end by a disc of your color. A row may "
"be made up of one or more discs."
msgstr ""
"Encadrer signifie placer un pion sur le plateau de manière à ce qu'un ou "
"plusieurs alignements de pions de votre adversaire soit flanqué à chaque "
"extrémité par un pion de votre couleur. Un tel alignement peut se composer "
"d'un ou plusieurs pions."

#: index.docbook:98
msgid ""
"The game starts with each player having two discs automatically placed in "
"the center four squares of the board in the following pattern:"
msgstr ""
"La partie démarre avec deux jetons pour chaque joueur placés sur le "
"plateau de jeu dans le carré de 4 cases central comme suit&nbsp;:"

#: index.docbook:101
msgid "&kreversi; opening position"
msgstr "Position de départ de &kreversi;"

#: index.docbook:101
msgid "Board Layout"
msgstr "Composition du plateau de jeu"

#: index.docbook:110
msgid "Menu Bar"
msgstr "Barre de menus"

#: index.docbook:110
msgid ""
"The <interface>Menu Bar</interface> contains four choices of drop-down "
"menus. These are <link linkend=\"file-menu\">File</link>, <link "
"linkend=\"game-menu\">Game</link>, <link "
"linkend=\"settings-menu\">Setting</link>, and <link "
"linkend=\"help-menu\">Help</link>. &LMB; click or <keycombo "
"action=\"simul\"><keycap>Alt</keycap><keycap>the underlined "
"letter</keycap></keycombo> to show the drop-down menu."
msgstr ""
"La <interface>barre de menus</interface> donne accès à quatre menus "
"déroulants. Il s'agit de <link linkend=\"file-menu\">Fichier</link>, <link "
"linkend=\"game-menu\">Jeu</link>, <link "
"linkend=\"settings-menu\">Configuration</link>, et <link "
"linkend=\"help-menu\">Aide</link>. Cliquez avec le &BGS; ou appuyez sur "
"<keycombo action=\"simul\"><keycap>Alt</keycap><keycap>la lettre "
"soulignée</keycap></keycombo> pour afficher le menu déroulant."

#: index.docbook:122
msgid "Tool Menu"
msgstr "Barre d'outils"

#: index.docbook:122
msgid ""
"Provides the player with icon shortcuts for commonly used actions. These are "
"<guiicon>Stop Thinking</guiicon>, <guiicon>Undo Move</guiicon>, "
"<guiicon>Shrink Board</guiicon>, <guiicon>Enlarge Board</guiicon>, "
"<guiicon>Get Hint</guiicon>, and <guiicon>Get Help</guiicon>. Place the "
"mouse pointer over any of these icons and &LMB; click to activate."
msgstr ""
"Donne accès au joueur aux actions les plus fréquentes. Il s'agit "
"d'<guiicon>Arrêter la réflexion</guiicon>, <guiicon>Annuler le "
"coup</guiicon>, <guiicon>Rétrécir</guiicon>, <guiicon>Agrandir</guiicon>, "
"<guiicon>Conseil</guiicon>, et <guiicon>Aide</guiicon>. Placez le pointeur "
"de la souris sur l'une de ces icônes et cliquez avec le &BGS; pour "
"déclencher l'action."

#: index.docbook:134
msgid "Game Board"
msgstr "Le plateau de jeu"

#: index.docbook:134
msgid "The game board consists of a 8 by 8 square board divided into 64 squares."
msgstr ""
"Le plateau de jeu consiste en une grille de 64 cases disposées pour former "
"un carré de 8 cases sur 8."

#: index.docbook:142
msgid "Status Bar"
msgstr "La barre d'état"

#: index.docbook:142
msgid ""
"The status bar contains useful information such as whose turn it is, whicn "
"color represents each player and the number of squares each player controls "
"on the board."
msgstr ""
"La barre d'état renferme des informations utiles, comme le joueur dont "
"c'est le tour, quelle couleur représente chaque joueur, et le nombre de "
"cases contrôlées par chaque joueur sur le plateau de jeu."

#: index.docbook:154
msgid "Rules"
msgstr "Règles du jeu"

#: index.docbook:156
msgid "Blue always moves first."
msgstr "Le joueur bleu joue toujours en premier."

#: index.docbook:158
msgid ""
"If on your turn you cannot outflank and flip at least one opposing disc, "
"your turn is forfeited and your opponent moves again. However, if a move is "
"available to you, you may not forfeit your turn."
msgstr ""
"Si, lorsque c'est votre tour, vous ne pouvez pas encadrer et retourner au "
"moins un pion de votre adversaire, vous perdez votre tour et votre "
"adversaire rejoue. Par contre, si vous pouvez jouer, alors vous ne pouvez "
"pas sauter votre tour."

#: index.docbook:162
msgid ""
"A disc may outflank any number of discs in one or more rows in any number of "
"directions at the same time - horizontally, vertically or diagonally. A row "
"is defined as one or more discs in a continuous straight line."
msgstr ""
"Un pion peut encadrer en même temps n'importe quel nombre de pions alignés "
"dans n'importe quel nombre de directions - horizontalement, verticalement ou "
"en diagonale. Un alignement est défini comme un ou plusieurs pions en ligne "
"droite sans interruption."

#: index.docbook:166
msgid "You may not skip over your own color disc to outflank an opposing disc."
msgstr ""
"Vous ne pouvez pas passer par-dessus un pion de votre propre couleur pour "
"encadrer un pion de l'adversaire."

#: index.docbook:169
msgid ""
"Discs may only be outflanked as a direct result of a move and must fall in "
"the direct line of the disc placed down."
msgstr ""
"Les pions ne peuvent être encadrés que directement suite à un coup et "
"doivent être alignés avec le pion déposé."

#: index.docbook:172
msgid ""
"All discs outflanked in any one move must be flipped, even if it is to the "
"player's advantage not to flip them."
msgstr ""
"Tous les pions encadrés lorsqu'un coup est joué doivent être retournés, "
"même si le joueur a intérêt à ce qu'ils ne le soient pas."

#: index.docbook:175
msgid ""
"Once a disc is placed on a square, it can never be moved to another square "
"later in the game."
msgstr ""
"Une fois qu'un pion a été déposé sur une case, il ne peut pas être "
"déplacé vers une autre case."

#: index.docbook:178
msgid ""
"When it is no longer possible for either player to move, the game is over. "
"Discs are then counted and the player with the majority of his or her color "
"discs on the board is declared the winner."
msgstr ""
"Quand plus aucun joueur ne peut jouer, la partie est finie. Les pions sont "
"alors comptés et le joueur qui a le plus de pions de sa couleur sur le "
"plateau de jeu est déclaré vainqueur."

#: index.docbook:182
msgid "It is possible for a game to end before all 64 squares are filled."
msgstr "Il se peut que le jeu s'arrête avant que les 64 cases ne soient remplies."

#: index.docbook:188
msgid "Tips"
msgstr "Conseils"

#: index.docbook:190
msgid ""
"Try to place pieces on the edges whenever possible. Your opponent cannot "
"outflank these pieces."
msgstr ""
"Essayez de placer des pions sur les coins quand c'est possible. Votre "
"adversaire ne pourra pas encadrer ces pièces."

#: index.docbook:193
msgid ""
"Avoid placing pieces on one of the three neighbor fields of a corner unless "
"you are absolutely sure that your opponent will not be able to put a piece "
"on a corner."
msgstr ""
"Évitez de placer des pions sur l'une des trois cases bordant un coin à "
"moins d'être absolument sûr que votre adversaire ne pourra pas placer un "
"pion dans ce coin."

#: index.docbook:197
msgid "Sometimes it is better to offer some pieces to your opponent"
msgstr "Il vaut parfois mieux offrir des pions à votre adversaire."

#: index.docbook:199
msgid "Try to put pieces on fields which prevent your opponent from moving."
msgstr ""
"Essayez de placer vos pièces sur des cases qui empêchent votre adversaire "
"de jouer."

#: index.docbook:201
msgid "Try to force your opponent to put a piece in a neighboring field of a corner."
msgstr ""
"Essayez de forcer votre adversaire à placer un pion dans une case "
"avoisinant un coin."

#: index.docbook:209
msgid "User Interface"
msgstr "Interface utilisateur"

#: index.docbook:212
msgid "<guimenu>File</guimenu> Menu"
msgstr "Menu <guimenu>Fichier</guimenu>"

#: index.docbook:214
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>N</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>F</accel>ile</guimenu><guimenuitem><accel>N</accel>ew "
"Game</guimenuitem>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>N</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>F</accel>ichier</guimenu><guimenuitem><accel>N</accel>"
"ouvelle partie</guimenuitem>"

#: index.docbook:214
msgid "Starts a new game."
msgstr "Démarre une nouvelle partie."

#: index.docbook:214
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>L</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>F</accel>ile</guimenu><guimenuitem><accel>L</accel>oad "
"Game</guimenuitem>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>L</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>F</accel>ichier</guimenu><guimenuitem><accel>C</accel>"
"harger une partie</guimenuitem>"

#: index.docbook:214
msgid "Loads a saved game."
msgstr "Charge une partie enregistrée au préalable."

#: index.docbook:214
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>S</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>F</accel>ile</guimenu><guimenuitem><accel>S</accel>ave "
"Game</guimenuitem>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>S</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>F</accel>ichier</guimenu><guimenuitem><accel>E</accel>"
"nregistrer la partie</guimenuitem>"

#: index.docbook:214
msgid "Saves the current game."
msgstr "Enregistre la partie en cours."

#: index.docbook:214
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>Q</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>F</accel>ile</guimenu><guimenuitem><accel>Q</accel>"
"uit</guimenuitem>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>Q</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>F</accel>ichier</guimenu><guimenuitem><accel>Q</accel>"
"uitter</guimenuitem>"

#: index.docbook:214
msgid "<action>Quits</action> &kreversi;."
msgstr "<action>Sort</action> de &kreversi;."

#: index.docbook:249
msgid "<guimenu>Game</guimenu> menu"
msgstr "Menu <guimenu>Jeu</guimenu>"

#: index.docbook:251
msgid ""
"<guimenu><accel>G</accel>ame</guimenu><guimenuitem>Get "
"<accel>h</accel>int</guimenuitem>"
msgstr ""
"<guimenu><accel>J</accel>eu</guimenu><guimenuitem>Donner un "
"<accel>c</accel>onseil</guimenuitem>"

#: index.docbook:251
msgid "The computer will provide a hint for your next move."
msgstr "L'ordinateur vous conseillera quant à votre prochain coup."

#: index.docbook:251
msgid ""
"<shortcut><keycap>Esc</keycap></shortcut> "
"<guimenu><accel>G</accel>ame</guimenu><guimenuitem><accel>S</accel>top "
"thinking</guimenuitem>"
msgstr ""
"<shortcut><keycap>Échap</keycap></shortcut> "
"<guimenu><accel>J</accel>eu</guimenu><guimenuitem>Arrê<accel>t</accel>"
"er la réflexion</guimenuitem>"

#: index.docbook:251
msgid "Stops the computer's depth search, then you choose the computer's next move."
msgstr ""
"Interrompt la recherche en profondeur de l'ordinateur, et vous offre de "
"choisir le prochain coup de l'ordinateur."

#: index.docbook:251
msgid ""
"<guimenu><accel>G</accel>ame</guimenu> "
"<guimenuitem><accel>C</accel>ontinue</guimenuitem>"
msgstr ""
"<guimenu><accel>J</accel>eu</guimenu> "
"<guimenuitem>Co<accel>n</accel>tinuer</guimenuitem>"

#: index.docbook:251
msgid "Continues play."
msgstr "Reprend la partie."

#: index.docbook:251
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>U</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>G</accel>ame</guimenu><guimenuitem><accel>U</accel>ndo "
"Move</guimenuitem>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>U</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>J</accel>eu</guimenu><guimenuitem>Annu<accel>l</accel>"
"er</guimenuitem>"

#: index.docbook:251
msgid "Removes your last move as well as the computer's last move from the board."
msgstr "Défait votre dernier coup ainsi que celui de l'ordinateur."

#: index.docbook:251
msgid ""
"<guimenu><accel>G</accel>ame</guimenu><guimenuitem>Switch "
"Si<accel>d</accel>es</guimenuitem>"
msgstr ""
"<guimenu><accel>J</accel>eu</guimenu><guimenuitem>Éc<accel>h</accel>anger "
"les couleurs</guimenuitem>"

#: index.docbook:251
msgid "Will make you the opposite color."
msgstr "Vous attribue la couleur opposée."

#: index.docbook:251
msgid ""
"<guimenu><accel>G</accel>ame</guimenu><guimenuitem>Hall of "
"<accel>F</accel>ame...</guimenuitem>"
msgstr ""
"<guimenu><accel>J</accel>eu</guimenu><guimenuitem><accel>P</accel>"
"anthéon...</guimenuitem>"

#: index.docbook:251
msgid "A new window will appear with a list of the top ten scores."
msgstr "Une nouvelle fenêtre avec la liste des dix meilleurs scores apparaît."

#: index.docbook:303
msgid "<guimenu>Settings</guimenu> Menu"
msgstr "Menu <guimenu>Configuration</guimenu>"

#: index.docbook:305
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>1-7</keycap></keycombo>"
"</shortcut> <guimenu><accel>S</accel>ettings</guimenu> "
"<guisubmenu>Skill</guisubmenu>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>1-7</keycap></keycombo>"
"</shortcut> <guimenu><accel>C</accel>onfiguration</guimenu> "
"<guisubmenu>Difficulté</guisubmenu>"

#: index.docbook:305
msgid ""
"<action>Sets the difficulty</action> from <guimenuitem>1</guimenuitem>, "
"being the easiest, to <guimenuitem>7</guimenuitem>, being the most difficult."
msgstr ""
"<action>Règle la difficulté</action> de <guimenuitem>1</guimenuitem>, le "
"plus facile, à <guimenuitem>7</guimenuitem>, le plus difficile."

#: index.docbook:305
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>-</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>S</accel>ettings</guimenu><guimenuitem><accel>S</accel>"
"hrink board</guimenuitem>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>-</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>C</accel>onfiguration</guimenu><guimenuitem><accel>R</accel>"
"étrécir</guimenuitem>"

#: index.docbook:305
msgid "Reduces the game board by twenty percent."
msgstr "Réduit le plateau de jeu de 20 pour cent."

#: index.docbook:305
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>+</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>S</accel>ettings</guimenu><guimenuitem><accel>E</accel>"
"nlarge board</guimenuitem>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Ctrl</keycap><keycap>+</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>C</accel>onfiguration</guimenu><guimenuitem>A<accel>g</accel>"
"randir</guimenuitem>"

#: index.docbook:305
msgid "Enlarges the game board by twenty percent."
msgstr "Agrandit le plateau de jeu de 20 pour cent."

#: index.docbook:305
msgid "<guimenu><accel>S</accel>ettings</guimenu> <guisubmenu>Set Size</guisubmenu>"
msgstr ""
"<guimenu><accel>C</accel>onfiguration</guimenu><guisubmenu>"
"Taille</guisubmenu>"

#: index.docbook:305
msgid "Allows you to shrink or enlarge the game board."
msgstr "Vous permet de rétrécir ou d'agrandir le plateau de jeu."

#: index.docbook:305
msgid ""
"<guimenu><accel>S</accel>ettings</guimenu> <guimenuitem>Select "
"<accel>b</accel>ackground color...</guimenuitem>"
msgstr ""
"<guimenu><accel>C</accel>onfiguration</guimenu><guimenuitem><accel>C</accel>"
"ouleur de fond</guimenuitem>"

#: index.docbook:305
msgid "Allows you to change the background color."
msgstr "Vous permet de changer la couleur de fond."

#: index.docbook:305
msgid ""
"<guimenu><accel>S</accel>ettings</guimenu> <guisubmenu>Select background "
"image</guisubmenu>"
msgstr ""
"<guimenu><accel>C</accel>onfiguration</guimenu><guisubmenu>Image de "
"fond</guisubmenu>"

#: index.docbook:305
msgid "Allows you to select a background image."
msgstr "Vous permet de choisir une image de fond."

#: index.docbook:305
msgid ""
"<guimenu><accel>S</accel>ettings</guimenu> "
"<guimenuitem><accel>G</accel>rayscale</guimenuitem>"
msgstr ""
"<guimenu><accel>C</accel>onfiguration</guimenu> "
"<guimenuitem><accel>N</accel>iveaux de gris</guimenuitem>"

#: index.docbook:305
msgid "Sets the color settings to grayscale."
msgstr "Convertit les réglages de couleur en niveaux de gris."

#: index.docbook:365
msgid ""
"<guimenu><accel>S</accel>ettings</guimenu> "
"<guimenuitem><accel>A</accel>nimations</guimenuitem>"
msgstr ""
"<guimenu><accel>C</accel>onfiguration</guimenu><guimenuitem><accel>A</accel>"
"nimations</guimenuitem>"

#: index.docbook:365
msgid "Animates the discs as their being flipped."
msgstr "Anime les pions lorsqu'ils sont retournés."

#: index.docbook:374
msgid ""
"<guimenu><accel>S</accel>ettings</guimenu> <guisubmenu>Animation "
"Speed</guisubmenu>"
msgstr ""
"<guimenu><accel>C</accel>onfiguration</guimenu><guisubmenu>Vitesse "
"d'animation</guisubmenu>"

#: index.docbook:374
msgid ""
"<action>Allows you to set the animation speed,</action> "
"<guimenuitem>1</guimenuitem> being the fastest and "
"<guimenuitem>10</guimenuitem> being the slowest."
msgstr ""
"<action>Vous permet de régler la vitesse d'animation,</action> de "
"<guimenuitem>1</guimenuitem> la plus rapide à <guimenuitem>10</guimenuitem>"
" la plus lente."

#: index.docbook:387
msgid "<guimenu>Help</guimenu> Menu"
msgstr "Menu <guimenu>Aide</guimenu>"

#: index.docbook:389
msgid ""
"<shortcut><keycap>F1</keycap></shortcut> "
"<guimenu><accel>H</accel>elp</guimenu> "
"<guimenuitem><accel>C</accel>ontents</guimenuitem>"
msgstr ""
"<shortcut><keycap>F1</keycap></shortcut> "
"<guimenu><accel>A</accel>ide</guimenu> "
"<guimenuitem>Conten<accel>u</accel></guimenuitem>"

#: index.docbook:389
msgid ""
"<action>Displays the help documentation</action> for &kreversi; (this "
"document.)"
msgstr ""
"<action>Affiche la documentation d'aide à l'usage</action> de &kreversi; "
"(ce document.)"

#: index.docbook:389
msgid ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Shift</keycap><keycap>F1</keycap></keycombo>"
"</shortcut> <guimenu><accel>H</accel>elp</guimenu><guimenuitem>What's "
"<accel>T</accel>his</guimenuitem>"
msgstr ""
"<shortcut><keycombo "
"action=\"simul\"><keycap>Shift</keycap><keycap>F1</keycap></keycombo>"
"</shortcut> "
"<guimenu><accel>A</accel>ide</guimenu><guimenuitem><accel>Q</accel>u'est-ce "
"que c'est&nbsp;?</guimenuitem>"

#: index.docbook:389
msgid ""
"When the mouse pointer is placed over an item and &LMB; clicked, a "
"<action>help window will open explaining the item's function.</action>"
msgstr ""
"Quand le pointeur de la souris est place sur un élément graphique et que "
"vous cliquez dessus avec le &BGS;, une <action>fenêtre d'aide s'ouvre pour "
"expliquer la fonction de cet élément.</action>"

#: index.docbook:389
msgid ""
"<guimenu><accel>H</accel>elp</guimenu> <guimenuitem><accel>R</accel>eport "
"Bug</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>ide</guimenu> <guimenuitem><accel>R</accel>apport "
"de bogue...</guimenuitem>"

#: index.docbook:389
msgid "Report a bug to the author."
msgstr "Permet d'établir un rapport de bogue à l'attention de l'auteur."

#: index.docbook:416
msgid ""
"<guimenu><accel>H</accel>elp</guimenu> <guimenuitem><accel>A</accel>bout "
"KReversi</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>ide</guimenu> <guimenuitem><accel>À</accel>"
" propos de KReversi</guimenuitem>"

#: index.docbook:416
msgid "Displays author and version information."
msgstr "Affiche des informations sur l'auteur et la version."

#: index.docbook:425
msgid ""
"<guimenu><accel>H</accel>elp</guimenu><guimenuitem>About "
"<accel>K</accel>DE</guimenuitem>"
msgstr ""
"<guimenu><accel>A</accel>ide</guimenu><guimenuitem>À propos de "
"<accel>K</accel>DE</guimenuitem>"

#: index.docbook:425
msgid "Displays information about the KDE version."
msgstr "Affiche des informations concernant la version de KDE."

#: index.docbook:439
msgid "Credits and License"
msgstr "Remerciements et licence"

#: index.docbook:441
msgid "Program Copyright 1998-2000 Mario Weilguni"
msgstr "Programme Copyright 1998-2000 Mario Weilguni"

#: index.docbook:442
msgid "Mats Luthman - Designer of the move engine."
msgstr "Mats Luthman - Concepteur du moteur de résolution de coups."

#: index.docbook:444
msgid "Original documentation by Mario Weilguni"
msgstr "Documentation originelle par Mario Weilguni"

#: index.docbook:445
msgid "Edited by Robert Williams"
msgstr "Mise en forme par Robert Williams"

#: index.docbook:447
msgid ""
"Documentation re-written and updated for KDE 2.0 by Clay Pradarits "
"<email>gasky@winfire.com</email>"
msgstr ""
"Documentation réécrite et mise à jour pour KDE 2.0 par Clay Pradarits "
"<email>gasky@winfire.com</email>"

#: index.docbook:450
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<para>Traduction française de la documentation par Éric Bischoff "
"<email>e.bischoff@noos.fr</email>.</para>"

#: index.docbook:457
msgid "Installation"
msgstr "Installation"

#: index.docbook:459
msgid ""
"&kreversi; is part of the KDE project <ulink "
"url=\"http://www.kde.org\">http://www.kde.org</ulink>. &kreversi; can be "
"found in the kdegames package on <ulink "
"url=\"ftp://ftp.kde.org/pub/kde/\">ftp://ftp.kde.org/pub/kde/</ulink>, the "
"main ftp site of the KDE project."
msgstr ""
"&kreversi; est un composant du projet KDE <ulink "
"url=\"http://www.kde.org\">http://www.kde.org</ulink>. &kreversi; se trouve "
"dans le paquetage kdegames à l'adresse <ulink "
"url=\"ftp://ftp.kde.org/pub/kde/\">ftp://ftp.kde.org/pub/kde/</ulink>, le "
"site FTP principal du projet KDE."

#: index.docbook:466
msgid "Compilation and Installation"
msgstr "Compilation et installation"

#: index.docbook:468
msgid ""
"To compile and install &kreversi; on your system, type the following in the "
"base directory of the KDE distribution:"
msgstr ""
"Pour compiler et installer &kreversi; sur votre système, saisissez les "
"commandes suivantes dans le dossier de base de la distribution de KDE&nbsp;:"

#: index.docbook:470
msgid ""
"<prompt>%</prompt> <userinput><command>./configure</command></userinput>\n"
"<prompt>%</prompt> <userinput><command>make</command></userinput>\n"
"<prompt>%</prompt> <userinput><command>make</command> "
"<option>install</option></userinput>"
msgstr ""
"<prompt>%</prompt> <userinput><command>./configure</command></userinput>\n"
"<prompt>%</prompt> <userinput><command>make</command></userinput>\n"
"<prompt>%</prompt> <userinput><command>make</command> "
"<option>install</option></userinput>"

#: index.docbook:475
msgid ""
"Since &kreversi; uses <application>autoconf</application> and "
"<application>automake</application>"
" you should have no trouble compiling it. Should you run into problems "
"please report them to the KDE mailing lists."
msgstr ""
"Comme &kreversi; utilise <application>autoconf</application> et "
"<application>automake</application>, vous ne devriez pas avoir de problème "
"à le compiler. Si c'était toutefois le cas, veuillez rapporter les "
"problèmes aux listes de diffusion de KDE."
