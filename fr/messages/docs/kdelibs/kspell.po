# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-07-01 20:16GMT\n"
"Last-Translator: Ludovic Grossard <ludovic.grossard@libertysurf.fr>\n"
"Language-Team: français <kde-francophone@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: index.docbook:11
msgid "The KSpell Handbook"
msgstr "Le manuel de KSpell"

#: index.docbook:13
msgid ""
"<firstname>David</firstname> <surname>Sweet</surname> <affiliation> "
"<address><email>dsweet@chaos.umd.edu</email></address> </affiliation>"
msgstr ""
"<firstname>David</firstname> <surname>Sweet</surname> <affiliation> "
"<address><email>dsweet@chaos.umd.edu</email></address> </affiliation>"

#: index.docbook:21
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit "
"role=\"translator\"><firstname>Yves</firstname><surname>Dessertine</surname>"
"<affiliation><address><email>yves.d@hrnet.fr</email></address></affiliation>"
"<contrib>Traduction française</contrib></othercredit>"

#: index.docbook:27
msgid ""
"KSpell is the spelling checker used by KDE applications such as KEdit, "
"KMail, KRN, and KWord. It is a GUI frontend to International ISpell and "
"ASpell."
msgstr ""
"KSpell est un vérificateur d'orthographe utilisé par les applications KDE, "
"telles que KEdit, KMail, KRN, et KWord. C'est une interface graphique pour "
"International ISpell et ASpell."

#: index.docbook:32
msgid "kspell"
msgstr "kspell"

#: index.docbook:33
msgid "ispell"
msgstr "ispell"

#: index.docbook:34
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:35
msgid "Linux"
msgstr "Linux"

#: index.docbook:36
msgid "spell"
msgstr "orthographe"

#: index.docbook:37
msgid "check"
msgstr "vérification"

#: index.docbook:42
msgid "Misspelled Word Dialog"
msgstr "Boîte de dialogue de mot mal orthographié"

#: index.docbook:45
msgid ""
"(If you do not have Ispell installed you can obtain it from the <ulink "
"url=\"http://fmg-www.cs.ucla.edu/geoff/ispell.html\">International ISpell "
"home page</ulink>. ASpell is available from the <ulink "
"url=\"http://metalab.unc.edu/kevina/aspell/\">ASpell home page</ulink>.)"
msgstr ""
"(Si vous n'avez Ispell installé sur votre système, vous pouvez vous le "
"procurer depuis <ulink "
"url=\"http://fmg-www.cs.ucla.edu/geoff/ispell.html\">la page web "
"d'International ISpell</ulink>. ASpell est disponible depuis <ulink "
"url=\"http://metalab.unc.edu/kevina/aspell/\">la page web d'ASpell</ulink>.)"

#: index.docbook:51
msgid "General Use"
msgstr "Utilisation générale"

#: index.docbook:53
msgid ""
"The top line in the dialog displays a misspelled word which was found in "
"your document. KSpell attempts to find an appropriate replacement word. One "
"or several may be found. The best guess is shown to the right of "
"<emphasis>Replacement:.</emphasis> To accept this replacement, click on "
"<emphasis>Replace.</emphasis> You may also choose a word from the list of "
"<emphasis>Suggestions</emphasis> then click <emphasis>Replace</emphasis> to "
"replace the misspelled word with the chosen word."
msgstr ""
"La première ligne dans la boîte de dialogue montre un mot mal "
"orthographié qui a été trouvé dans votre document. KSpell essaye de "
"trouver un mot de remplacement approprié. Un ou plusieurs mots peuvent "
"être trouvés. La meilleure suggestion est affichée juste à coté de "
"<emphasis>Remplacer par&nbsp;:</emphasis>. Pour accepter ce mot de "
"remplacement, cliquez sur <emphasis>Remplacer</emphasis>. Vous pouvez aussi "
"choisir un mot dans la liste de <emphasis>Suggestions&nbsp;:</emphasis>, "
"puis cliquez sur <emphasis>Remplacer</emphasis> pour remplacer le mot mal "
"orthographié par le mot choisi."

#: index.docbook:61
msgid "To keep your original spelling, click on <emphasis>Ignore.</emphasis>"
msgstr ""
"Pour conserver votre orthographe originale, cliquez sur "
"<emphasis>Ignorer</emphasis>."

#: index.docbook:63
msgid ""
"To stop the spell checking -- keeping the changes you've already made -- "
"click on <emphasis>Stop.</emphasis>"
msgstr ""
"Pour arrêter la vérification de l'orthographe tout en conservant les "
"changements déjà effectués, cliquez sur <emphasis>Arrêter</emphasis>."

#: index.docbook:66
msgid ""
"To stop the spell checking and cancel the changes you've already made, click "
"on <emphasis>Cancel.</emphasis>"
msgstr ""
"Pour arrêter la vérification de l'orthographe et annuler les changements "
"effectués, cliquez sur <emphasis>Annuler</emphasis>."

#: index.docbook:71
msgid "Other Functions"
msgstr "Autres fonctions"

#: index.docbook:73
msgid ""
"Clicking on <emphasis>Replace All</emphasis>"
" will initially perform the same function as clicking on "
"<emphasis>Replace,</emphasis> but will automatically replace the misspelled "
"word with the chosen replacement word if it appears again (at a later point) "
"in your document."
msgstr ""
"Cliquer sur <emphasis>Remplacer tout</emphasis> va effectuer la même "
"fonction que si vous cliquiez sur <emphasis>Remplacer</emphasis>, mais va "
"automatiquement remplacer le mot mal orthographié par le mot de "
"remplacement choisi s'il réapparaît (plus loin) dans votre document."

#: index.docbook:78
msgid ""
"The <emphasis>Ignore All</emphasis> button ignores this and all future "
"occurrences of the misspelled word."
msgstr ""
"Le bouton <emphasis>Ignorer toujours</emphasis> ignore cette occurrence et "
"toutes les occurrences futures du mot mal orthographié."

#: index.docbook:81
msgid ""
"Clicking on <emphasis>Add</emphasis> will add the misspelled word to your "
"personal dictionary (this is distinct from the original system dictionary, "
"so the additions you make will not be seen by other users)."
msgstr ""
"Le fait de cliquer sur <emphasis>Ajouter</emphasis> va ajouter le mot mal "
"orthographié à votre dictionnaire personnel (celui-ci est dissocié du "
"dictionnaire original du système, de sorte que les ajouts que vous "
"effectuez ne soient pas visibles par les autres utilisateurs)."

#: index.docbook:88
msgid "Configuration Dialog"
msgstr "Boîte de dialogue de configuration"

#: index.docbook:92
msgid "Dictionaries"
msgstr "Dictionnaire"

#: index.docbook:94
msgid ""
"You can choose the dictionary to use for spell checking from the list of "
"installed dictionaries."
msgstr ""
"Vous pouvez choisir le dictionnaire à utiliser pour la vérification "
"d'orthographe dans la liste des dictionnaires installés."

#: index.docbook:99
msgid "Encodings"
msgstr "Encodage"

#: index.docbook:101
msgid "The available character encodings are:"
msgstr "Les encodages de caractères disponibles sont&nbsp;:"

#: index.docbook:101
msgid "7-Bit/ASCII"
msgstr "7-Bit/ASCII"

#: index.docbook:101
msgid "This is the character set used for English text."
msgstr "Ceci est le jeu de caractères utilisé pour du texte anglais."

#: index.docbook:101
msgid "Latin1"
msgstr "Latin1"

#: index.docbook:101
msgid "This is used for Western European languages."
msgstr "Celui-ci est utilisé pour les langues de l'Europe de l'Ouest."

#: index.docbook:101
msgid "Latin2"
msgstr "Latin2"

#: index.docbook:101
msgid "This is used for Eastern European languages."
msgstr "Celui-ci est utilisé pour les langues de l'Europe de l'Est."

#: index.docbook:124
msgid ""
"You should select the one that matches the character set you are using. In "
"some cases, dictionaries will support more than one encoding. A dictionary "
"might, for example, accept accented characters when "
"<emphasis>Latin1</emphasis> is selected, but accept email-style character "
"combinations (like <emphasis>'a</emphasis> for an accented "
"<emphasis>a</emphasis>) when <emphasis>7-Bit/ASCII</emphasis> is selected. "
"Please see your dictionary's disctribution for more information."
msgstr ""
"Vous devriez sélectionner celui qui correspond au jeu de caractères que "
"vous utilisez. Dans certains cas, les dictionnaires supporteront plus d'un "
"encodage. Un dictionnaire pourra, par exemple, accepter les caractères "
"accentués lorsque <emphasis>Latin 1</emphasis> sera sélectionné, et "
"accepter les combinaisons de caractères du genre email (par exemple "
"<emphasis>'a</emphasis> pour un <emphasis>a</emphasis> accentué) lorsque "
"<emphasis>7-Bit/ASCII</emphasis> sera sélectionné. Veuillez consulter la "
"distribution de votre dictionnaire pour de plus amples informations."

#: index.docbook:133
msgid "Spell-checking client"
msgstr "Client"

#: index.docbook:135
msgid ""
"You may choose to use <emphasis>Ispell</emphasis> or "
"<emphasis>Aspell</emphasis> as the spell-checking backend for "
"<emphasis>KSpell</emphasis>. <emphasis>Ispell</emphasis> is more widely "
"available and may have better international support, but "
"<emphasis>Aspell</emphasis> is gaining popularity as it claims to give "
"better suggestions for word replacements."
msgstr ""
"Vous pouvez choisir d'utiliser <emphasis>ISpell</emphasis> ou "
"<emphasis>ASpell</emphasis> comme vérificateur d'orthographe pour "
"<emphasis>KSpell</emphasis>. <emphasis>ISpell</emphasis> est plus répandu "
"et offre un meilleur support international, mais <emphasis>ASpell</emphasis>"
" gagne en popularité parce qu'il prétend proposer de meilleures "
"suggestions pour les remplacements de mots."

#: index.docbook:142
msgid "Other"
msgstr "Autres"

#: index.docbook:144
msgid ""
"It is recommend that you do not change the first two options unless you have "
"read the Interational ISpell man page."
msgstr ""
"Il est recommandé de ne pas changer les deux premières options tant que "
"vous n'avez pas lu la page man d'International ISpell."

#: index.docbook:150
msgid "Contact Information"
msgstr "Contacts"

#: index.docbook:152
msgid ""
"For more information about KSpell, visit the <ulink "
"url=\"http://www.chaos.umd.edu/~dsweet/KDE/KSpell\">KSpell Home "
"Page</ulink>. In particular, you will find information about programmng the "
"KSpell C++ class."
msgstr ""
"Pour plus d'informations à propos de KSpell, visitez la <ulink "
"url=\"http://www.chaos.umd.edu/~dsweet/KDE/KSpell\">page web de "
"KSpell</ulink>"
". En particulier, vous trouverez des informations à propos de la "
"programmation des classes C++ de KSpell."

#: index.docbook:155
msgid ""
"You mail email the author/maintainer with questions and/or comments at "
"<ulink url=\"mailto:dsweet@kde.org\">dsweet@kde.org</ulink>."
msgstr ""
"Pour pouvez envoyer un email de questions et/ou de commentaires à "
"l'auteur/mainteneur à <ulink "
"url=\"mailto:dsweet@kde.org\">dsweet@kde.org</ulink>."

#: index.docbook:158
msgid "CREDIT_FOR_TRANSLATORS"
msgstr ""
"<para>Traduction française par Yves Dessertine "
"<email>yves.d@hrnet.fr</email></para>"
