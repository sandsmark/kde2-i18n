# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-12 14:34+0200\n"
"PO-Revision-Date: 2001-07-24 18:31GMT\n"
"Last-Translator: Thibaut Cousin <cousin@in2p3.fr>\n"
"Language-Team: Français <kde-francophone@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2\n"

#: kenddlg.cpp:72
msgid "Good bye"
msgstr "À la prochaine !"

#: kenddlg.cpp:84
msgid ""
"Thanks for playing !\n"
"\n"
"Come back soon\n"
"\n"
"\n"
"Sure you really wanna quit?"
msgstr ""
"Merci d'avoir joué.\n"
"\n"
"À bientôt\n"
"\n"
"\n"
"Voulez-vous vraiment quitter ?"

#: kenddlg.cpp:100
msgid "Stay here"
msgstr "Continuer"

#: kwin4aboutdlg.cpp:32
msgid "About Four Wins"
msgstr "À propos de Puissance 4"

#: kwin4aboutdlg.cpp:65
#, c-format
msgid ""
"Four Wins\n"
"\n"
"Version %1"
msgstr ""
"Puissance 4\n"
"\n"
"Version %1"

#: kwin4aboutdlg.cpp:72
msgid ""
"(c) 2000 Martin Heni\n"
"Email: martin@heni-online.de\n"
"\n"
"Gamefeatures:\n"
"  - Multiplayer network game\n"
"  - Up to two Computerplayer\n"
"\n"
"\n"
" Many thanks to Laura for betatesting!"
msgstr ""
"(c) 2000 Martin Heni\n"
"Adresse électronique: martin@heni-online.de\n"
"\n"
"Fonctionnalités du jeu :\n"
"  - Jeu en réseau multi-joueurs\n"
"  - Jusqu'à deux ordinateurs\n"
"\n"
"\n"
" Remerciements à Laura pour le betâ-test !"

#: kwin4.cpp:64
msgid "Four wins"
msgstr "Puissance 4"

#: kwin4.cpp:302 kwin4.cpp:303
msgid "Starting a new game..."
msgstr "Démarrage de la nouvelle partie..."

#: kwin4.cpp:305
msgid "&End Game"
msgstr "&Terminer la partie"

#: kwin4.cpp:307
msgid "Ending the current game..."
msgstr "Termine la partie en cours..."

#: kwin4.cpp:308
msgid "Aborts a currently played game. No winner will be declared."
msgstr ""
"Interrompt une partie actuellement en cours. Aucun vainqueur ne sera "
"déclaré."

#: kwin4.cpp:310
msgid "&Show Statistics..."
msgstr "&Afficher les statistiques"

#: kwin4.cpp:312
msgid "Show all time statistics..."
msgstr "Afficher les statistiques de tous les temps"

#: kwin4.cpp:313
msgid "Shows the all time statistics which is kept in all sessions."
msgstr ""
"Afficher les statistiques de tous les temps qui sont conservées dans toutes "
"les sessions."

#: kwin4.cpp:315
msgid "&Hint"
msgstr "&Conseil"

#: kwin4.cpp:317 kwin4.cpp:318
msgid "Shows a hint on how to move."
msgstr "Conseiller un coup"

#: kwin4.cpp:321
msgid "Send &Message..."
msgstr "Envoyer un &message"

#: kwin4.cpp:323
msgid "Sending message to remote player..."
msgstr "Envoi d'un message au joueur distant..."

#: kwin4.cpp:324
msgid "Allows you to talk with a remote player."
msgstr "Vous permet de parler avec un joueur distant."

#: kwin4.cpp:327
msgid "Exiting..."
msgstr "Arrêt..."

#: kwin4.cpp:328
msgid "Quits the program."
msgstr "Quitte le programme."

#: kwin4.cpp:331 kwin4.cpp:332
msgid "Undo last move."
msgstr "Annuler le dernier coup."

#: kwin4.cpp:335 kwin4.cpp:336
msgid "Redo last move."
msgstr "Refaire le dernier coup."

#: kwin4.cpp:338
msgid "&Show Statusbar"
msgstr "Afficher la ba&rre d'état"

#: kwin4.cpp:340 kwin4.cpp:341
msgid "Toggle the statusbar..."
msgstr "(Dés)Activer la barre d'état..."

#: kwin4.cpp:343
msgid "Startplayer"
msgstr "Premier joueur"

#: kwin4.cpp:345
msgid "Changing startplayer..."
msgstr "Changement du premier joueur..."

#: kwin4.cpp:346
msgid "Chooses which player begins the next game."
msgstr "Choisit quel joueur commence la prochaine partie."

#: kwin4.cpp:348
msgid "&Yellow"
msgstr "Jau&ne"

#: kwin4.cpp:349
msgid "&Red"
msgstr "&Rouge"

#: kwin4.cpp:352
msgid "Yellow played by"
msgstr "Les jaunes sont joués par"

#: kwin4.cpp:354 kwin4.cpp:355
msgid "Change who plays the yellow player..."
msgstr "Changer qui joue les jaunes..."

#: kwin4.cpp:357
msgid "&Player"
msgstr "&Joueur"

#: kwin4.cpp:358
msgid "&Computer"
msgstr "Or&dinateur"

#: kwin4.cpp:359
msgid "&Remote"
msgstr "Di&stant"

#: kwin4.cpp:361
msgid "Red played by"
msgstr "Les rouges sont joués par"

#: kwin4.cpp:363 kwin4.cpp:364
msgid "Change who plays the red player..."
msgstr "Changer qui joue les rouges..."

#: kwin4.cpp:367
msgid "&Level"
msgstr "&Niveau"

#: kwin4.cpp:369
msgid "Change level..."
msgstr "Changer le niveau..."

#: kwin4.cpp:370
msgid "Change the strength of the computer player."
msgstr "Changer le niveau de l'ordinateur."

#: kwin4.cpp:372
msgid "&1"
msgstr "&1"

#: kwin4.cpp:373
msgid "&2"
msgstr "&2"

#: kwin4.cpp:374
msgid "&3"
msgstr "&3"

#: kwin4.cpp:375
msgid "&4"
msgstr "&4"

#: kwin4.cpp:376
msgid "&5"
msgstr "&5"

#: kwin4.cpp:377
msgid "&6"
msgstr "&6"

#: kwin4.cpp:378
msgid "&7"
msgstr "&7"

#: kwin4.cpp:379
msgid "&8"
msgstr "&8"

#: kwin4.cpp:380
msgid "&9"
msgstr "&9"

#: kwin4.cpp:384
msgid "Change &Names..."
msgstr "C&hanger les noms..."

#: kwin4.cpp:386 kwin4.cpp:387
msgid "Configure player names..."
msgstr "Configurer le nom des joueurs"

#: kwin4.cpp:389
msgid "Network server"
msgstr "Serveur de réseau"

#: kwin4.cpp:391 kwin4.cpp:392
msgid "Try to act as network master..."
msgstr "Essayer de devenir serveur réseau..."

#: kwin4.cpp:394
msgid "&Animations"
msgstr "&Animations"

#: kwin4.cpp:396 kwin4.cpp:397
msgid "Display animations."
msgstr "Afficher des animations."

#: kwin4.cpp:411 kwin4.cpp:426
msgid "Ready"
msgstr "Prêt"

#: kwin4.cpp:424
msgid "This leaves space for the mover"
msgstr "Ceci laisse de la place pour le déplacement"

#: kwin4.cpp:425
msgid "23:45"
msgstr "23:45"

#: kwin4.cpp:429
msgid "(c) Martin Heni   "
msgstr "(c) Martin Heni   "

#: kwin4.cpp:430
#, c-format
msgid "Welcome to %1"
msgstr "Bienvenue à %1"

#: kwin4.cpp:665
msgid "Remote connection to %1:%2..."
msgstr "Connexion distante à %1:%2..."

#: kwin4.cpp:669
msgid "Offering remote connection on port %1 ..."
msgstr "Autorise la connexion distante sur le port %1..."

#: kwin4.cpp:671
msgid "Abort"
msgstr "Annuler"

#: kwin4.cpp:777
msgid ""
"It is not possible to start the yellow and red player.\n"
msgstr ""
"Il est impossible de démarrer les joueurs jaune et rouge.\n"

#: kwin4.cpp:778
msgid ""
"It is not possible to start the yellow player.\n"
msgstr ""
"Il est impossible de démarrer le joueur jaune.\n"

#: kwin4.cpp:779
msgid ""
"It is not possible to start the red player.\n"
msgstr ""
"Il est impossible de démarrer le joueur rouge.\n"

#: kwin4.cpp:781
msgid ""
"This is a serious error. If one player is played by\n"
"the computer check for the existance of the file '%1'.\n"
msgstr ""
"C'est un problème sérieux. Si un joueur est joué par\n"
"l'ordinateur, vérifiez l'existence du fichier «%1».\n"

#: kwin4.cpp:849
msgid " Game aborted. Please restart next round."
msgstr " Partie interrompue. Veuillez redémarrer une autre manche."

#: kwin4.cpp:901
msgid ""
"It is not possbile to start the hint process.\n"
"This is a serious error. Check for the existance of\n"
"the file '%1'.\n"
msgstr ""
"Il est impossible de démarrer le processus de conseils.\n"
"C'est un problème sérieux. Veuillez vérifier l'existence\n"
"du fichier «%1».\n"

#: kwin4.cpp:977
msgid ""
"It is not your turn. You cannot undo now.\n"
msgstr ""
"Ce n'est pas votre tour. Vous ne pouvez pas annuler maintenant.\n"

#: kwin4.cpp:1000
msgid ""
"It is not your turn. You cannot redo now.\n"
msgstr ""
"Ce n'est pas votre tour. Vous ne pouvez pas refaire maintenant.\n"

#: kwin4.cpp:1418
msgid "Waiting for the computer to move..."
msgstr "Attend le coup de l'ordinateur..."

#: kwin4.cpp:1432
msgid "Waiting for remote player..."
msgstr "Attend le joueur distant..."

#: kwin4.cpp:1438
msgid "Please make your move..."
msgstr "Veuillez jouer..."

#: kwin4.cpp:1464
msgid "Remote player ended game..."
msgstr "Le joueur distant a arrêté la partie..."

#: kwin4.cpp:1466
msgid "Remote player ended game."
msgstr "Le joueur distant a arrêté la partie."

#: kwin4.cpp:1486 kwin4.cpp:1529
msgid "Game started as remote slave"
msgstr "Le jeu a commencé comme esclave distant"

#: kwin4.cpp:1501 kwin4.cpp:1522 kwin4.cpp:1533
msgid "Game started as remote master"
msgstr "Le jeu a commencé comme maître distant"

#: kwin4.cpp:1550
msgid "Remote connection lost for yellow..."
msgstr "La connexion distante pour les jaunes a été perdue..."

#: kwin4.cpp:1557
msgid "Remote connection lost for red..."
msgstr "La connexion distante pour les rouges a été perdue..."

#: kwin4.cpp:1576
msgid "Received an error from the remote player"
msgstr "Une erreur du joueur distant a été reçue"

#: kwin4.cpp:1655
msgid ""
"Please wait... Computer is thinking.\n"
msgstr ""
"Veuillez patienter... L'ordinateur réfléchit.\n"

#: kwin4.cpp:1712
msgid ""
"This move is not possible.\n"
msgstr ""
"Ce coup n'est pas possible.\n"

#: kwin4.cpp:1720 kwin4.cpp:1731
msgid "%1 won the game. Please restart next round."
msgstr "%1 a gagné la partie. Veuillez démarrer la manche suivante."

#: kwin4.cpp:1723
msgid "%1 (Yellow) has won the game!"
msgstr "%1 (les jaunes) a gagné !"

#: kwin4.cpp:1734
msgid "%1 (Red) has won the game!"
msgstr "%1 (les rouges) a gagné !"

#: kwin4.cpp:1742
msgid "The game is drawn. Please restart next round."
msgstr "Il y a égalité. Veuillez recommencer une autre manche."

#: kwin4.cpp:1743
msgid ""
"The game ended remis!\n"
msgstr ""
"La partie est terminée.\n"

#: kwin4.cpp:1777
msgid "No game  "
msgstr "Pas de partie  "

#: kwin4.cpp:1781
msgid " - Yellow "
msgstr " - Jaune "

#: kwin4.cpp:1785
msgid " - Red "
msgstr " - Rouge "

#: kwin4.cpp:1789
msgid "Nobody  "
msgstr "Personne"

#: kwin4doc.cpp:746 namedlg.cpp:67 statdlg.cpp:71
msgid "Player 1"
msgstr "Joueur 1 "

#: kwin4doc.cpp:747 namedlg.cpp:82 namedlg.cpp:86 statdlg.cpp:77
msgid "Player 2"
msgstr "Joueur 2 "

#: kwin4view.cpp:146
msgid ""
"_: Four wins for KDE\n"
"Four wins\n"
"\n"
"\n"
"for\n"
"\n"
"K D E"
msgstr ""
"Puissance 4\n"
"\n"
"\n"
"pour\n"
"\n"
"KDE"

#: kwin4view.cpp:240
msgid ""
"_: versus\n"
"vs"
msgstr "contre"

#: kwin4view.cpp:243
msgid "Level:"
msgstr "Niveau : "

#: kwin4view.cpp:250
msgid "Move:"
msgstr "Coup : "

#: kwin4view.cpp:263
msgid "Chance:"
msgstr "Chance :"

#: kwin4view.cpp:267
msgid "Loser"
msgstr "Perdant"

#: kwin4view.cpp:268
msgid "Winner"
msgstr "Gagnant"

#: kwin4view.cpp:286 kwin4view.cpp:312
msgid "Computer"
msgstr "Ordinateur"

#: kwin4view.cpp:291 kwin4view.cpp:317
msgid " Human"
msgstr " Humain"

#: kwin4view.cpp:296 kwin4view.cpp:322
msgid "Remote"
msgstr "Distant"

#: kwin4view.cpp:360
msgid ""
"_: 1_letter_abbr_won\n"
"W"
msgstr "G"

#: kwin4view.cpp:362
msgid ""
"_: 1_letter_abbr_drawn\n"
"D"
msgstr "P"

#: kwin4view.cpp:364
msgid ""
"_: 1_letter_abbr_lost\n"
"L"
msgstr "P"

#: kwin4view.cpp:366
msgid ""
"_: 1-2_letter_abbr_number\n"
"No"
msgstr "N°"

#: kwin4view.cpp:368
msgid ""
"_: 1-2_letter_abbr_breaks/aborted\n"
"Bk"
msgstr "Ar"

#: kwin4view.cpp:461
msgid "Hold on..the other was not yet gone..."
msgstr "Du calme... votre adversaire n'a pas encore fini..."

#: kwin4view.cpp:464
msgid "Hold your horses..."
msgstr "Tenez les rennes..."

#: kwin4view.cpp:467
msgid "Ah ah ah...only one go at a time..."
msgstr "Ha Ha Ha... un seul à la fois..."

#: kwin4view.cpp:470
msgid "Please wait... it is not your turn."
msgstr "Veuillez patienter... ce n'est pas votre tour."

#: main.cpp:33
msgid "Enter debug level"
msgstr "Saisissez le niveau de déboguage"

#: main.cpp:41
msgid "KWin4"
msgstr "kWin4"

#: main.cpp:43
msgid "KWin4: Two player network game"
msgstr "kWin4 : jeu en réseau pour deux joueurs"

#: msgdlg.cpp:44
msgid "Send message to remote player..."
msgstr "Envoyer un message au joueur distant..."

#: msgdlg.cpp:50
msgid "Enter message"
msgstr "Saisissez le message"

#: msgdlg.cpp:62
msgid "Send"
msgstr "Envoyer"

#: namedlg.cpp:32
msgid "Configure names..."
msgstr "Changer les noms..."

#: namedlg.cpp:49
msgid "Player names"
msgstr "Noms des joueurs"

#: namedlg.cpp:72 namedlg.cpp:88
msgid "Enter a player's name"
msgstr "Saisissez le nom d'un joueur"

#: networkdlg.cpp:32
msgid "Configure network options..."
msgstr "Configuration du réseau"

#: networkdlg.cpp:46
msgid "Remote settings"
msgstr "Configuration distante"

#: networkdlg.cpp:81
msgid "Connect to a remote game"
msgstr "Connexion vers une partie distante"

#: networkdlg.cpp:97
msgid "Remote host"
msgstr "Hôte distant"

#: networkdlg.cpp:101
msgid "Port"
msgstr "Port"

#: statdlg.cpp:36
msgid "Statistics..."
msgstr "Statistiques"

#: statdlg.cpp:52
msgid "All time game statistics"
msgstr "Statistiques de toutes les parties"

#: statdlg.cpp:65
msgid "Name"
msgstr "Nom"

#: statdlg.cpp:88
msgid "Won"
msgstr "Gagné"

#: statdlg.cpp:111
msgid "Drawn"
msgstr "Égalité"

#: statdlg.cpp:134
msgid "Lost"
msgstr "Perdu"

#: statdlg.cpp:157
msgid "Aborted"
msgstr "Arrêté"

#: statdlg.cpp:180
msgid "Sum"
msgstr "Total"

#: statdlg.cpp:214
msgid "Clear all statistics"
msgstr "Effacer toutes les statistiques"

#: statdlg.cpp:263
msgid ""
"Do you really want to delete the all time \n"
"statistics?\n"
msgstr ""
"Voulez-vous vraiment effacer toutes les statistiques ?\n"

#: statdlg.cpp:265
msgid "Statistics warning"
msgstr "Avertissement des statistiques"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Gérard Delafond"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "g.delafond@medsyn.fr"
