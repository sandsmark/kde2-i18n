msgid ""
msgstr ""
"Project-Id-Version: KDISPLAY\n"
"POT-Creation-Date: 2001-07-02 17:48+0200\n"
"PO-Revision-Date: 2001-07-04 20:44GMT+0200\n"
"Last-Translator: Kim Enkovaara <kim.enkovaara@iki.fi>\n"
"Language-Team: Finnish <fi@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2\n"

#: general.cpp:155
msgid "Name"
msgstr "Nimi"

#: general.cpp:156
msgid "Description"
msgstr "Kuvaus"

#: general.cpp:196
msgid "No description available."
msgstr "Ei kuvausta:"

#: general.cpp:306
msgid "Widget style and theme"
msgstr "Tyyli ja teema"

#: general.cpp:313
msgid ""
"Here you can choose from a list of predefined widget styles (e.g. the way "
"buttons are drawn) which may or may not be combined with a theme (additional "
"information like a marble texture or a gradient)."
msgstr ""
"Tässä voit valita etukäteenmääritellyn tyylin (eli tyylin, jolla esim. "
"painikkeet piirretään). Asetus voidaan yhdistää myös teemaan "
"(lisätietoja kuten marmorikuviointi tai väriliuku)"

#: general.cpp:321
msgid "&Import GTK themes..."
msgstr "&Tuo GTK teemoja..."

#: general.cpp:323
msgid ""
"This launches KDE GTK style importer which allows you to convert legacy GTK "
"pixmap themes to KDE widget styles."
msgstr ""
"Tämä painike avaa KDE:n teemojen tuontityökalun, jonka avulla voit "
"muuttaa GTK teemoja KDE:n teemoiksi."

#: general.cpp:334
msgid "Other settings for drawing"
msgstr "Muut asetukset piirtämiseen"

#: general.cpp:340
msgid "&Menubar on top of the screen in the style of MacOS"
msgstr "&Valikkorivi ruudun ylälaidassa MacOS tyyliin"

#: general.cpp:342
msgid ""
"If this option is selected, applications won't have their menubar attached "
"to their own window anymore. Instead, there is one menu bar at the top of "
"the screen which shows the menu of the currently active application. Maybe "
"you know this behavior from MacOS."
msgstr ""
"Jos tämä asetus on valittuna sovelluksien valikko ei ole enää niiden "
"omassa ikkunassa. Asetus siirtää valikon ruudun ylälaitaan ja jakaa sen "
"eri sovellusten välillä ja aina aktiivisen sovelluksen valikko on "
"näkyvillä. Tämä käyttäytyminen on ehkä tuttua MacOS:ta."

#: general.cpp:351
msgid "&Apply fonts and colors to non-KDE apps"
msgstr "&Käytä kirjasimia ja värejä muissa sovelluksissa"

#: general.cpp:355
msgid ""
"If this option is selected, KDE will try to force your preferences regarding "
"colors and fonts even on non-KDE applications. While this works fine with "
"most applications, it <em>may</em> give strange results sometimes."
msgstr ""
"Jos tämä asetus on valittuna KDE yrittää pakottaa väri- ja "
"kirjasinasetukset myös muihin kuin KDE:n sovelluksiin. Vaikka tämä toimii "
"hyvin monien sovellusten kanssa, asetus <em>voi</em>"
" aiheuttaa joskus kummia tuloksia."

#: general.cpp:360
msgid "Style options for toolbars"
msgstr "Tyylin asetukset työkaluriveille"

#: general.cpp:370
msgid "&Icons only"
msgstr "Vain &kuvakkeet"

#: general.cpp:371
msgid "&Text only"
msgstr "Vain &teksti"

#: general.cpp:372
msgid "Text a&side icons"
msgstr "Teksti &ja kuvakkeet"

#: general.cpp:373
msgid "Text &under icons"
msgstr "Teksti kuvakkeiden &alla"

#: general.cpp:375
msgid "Shows only icons on toolbar buttons. Best option for low resolutions."
msgstr ""
"Näyttää vain kuvakkeet työkalurivillä. Paras valinta matalille "
"näytöntarkkuuksille."

#: general.cpp:376
msgid "Shows only text on toolbar buttons."
msgstr "Näyttää vain tekstin työkalupalkeissa."

#: general.cpp:377
msgid "Shows icons and text on toolbar buttons. Text is aligned aside the icon."
msgstr ""
"Näyttää kuvakkeet ja tekstin työkalurivin napeissa. Teksti kuvakkeen "
"vieressä."

#: general.cpp:378
msgid "Shows icons and text on toolbar buttons. Text is aligned below the icon."
msgstr ""
"Näyttää kuvakkeet ja tekstin työkalurivin napeissa. Teksti kuvakkeen "
"alla."

#: general.cpp:380
msgid "&Highlight buttons under mouse"
msgstr "&Korosta nappeja hiiren alla"

#: general.cpp:381
msgid "Tool&bars are transparent when moving"
msgstr "Työkalurivit &läpinäkyviä siirrettäessä"

#: general.cpp:384
msgid ""
"If this option is selected, toolbar buttons will change their color when the "
"mouse cursor is moved over them."
msgstr ""
"Jos tämä asetus on valittuna työkalupalkin napit vaihtavat väriä kun "
"hiiren kursori on niiden päällä."

#: general.cpp:386
msgid ""
"If this option is selected, only the skeleton of a toolbar will be shown "
"when moving that toolbar."
msgstr ""
"Jos tämä asetus on valittuna vain työkalupalkin reunus näytetään sitä "
"siirrettäessä."

#: general.cpp:407
msgid "Effect options"
msgstr "Tehosteen asetukset"

#: general.cpp:417
msgid "No m&enu effect"
msgstr "Ei &valikkotehostetta"

#: general.cpp:418
msgid "&Fade menus"
msgstr "&Häivytä valikot"

#: general.cpp:419
msgid "Anima&te menus"
msgstr "&Animoi valikot"

#: general.cpp:423
msgid "Animate &combo boxes"
msgstr "Animoi &rastipainikkeet"

#: general.cpp:425
msgid "If this option is selected, menus open immediately."
msgstr "Jos tämä kohta on valittuna, valikot avautuvat välittömästi."

#: general.cpp:426
msgid "If this option is selected, menus fade in slowly."
msgstr "Jos tämä kohta on valittuna, valikot avautuvat hitaasti."

#: general.cpp:427
msgid "If this option is selected, menus are animated to grow to their full size."
msgstr "Jos tämä kohta on valittuna, valikot kasvavat täyteen kokoon."

#: general.cpp:428
msgid ""
"If this option is selected, combo boxes are animated to grow to their full "
"size."
msgstr "Jos tämä kohta on valittuna, rastipainikkeet kasvavat täyteen kokoon."

#: general.cpp:447
msgid "Tooltips"
msgstr "Työkaluvihjeet"

#: general.cpp:454
msgid "&Disable tool tips"
msgstr "&Poista työkaluvihjeet"

#: general.cpp:455
msgid "&Plain tool tips"
msgstr "&Normaalit työkaluvihjeet"

#: general.cpp:456
msgid "Fade tool t&ips"
msgstr "&Häivytä työkaluvihjeet"

#: general.cpp:457
msgid "&Rollout tool t&ips"
msgstr "&Rullaa pois työkaluvihjeet"

#: general.cpp:458
msgid "If this option is selected, tooltips are disabled in all applications."
msgstr "Jos tämä kohta on valittuna, työkaluvihjeitä ei näytetä sovelluksissa."

#: general.cpp:459
msgid "If this option is selected, tooltips appear with no effect."
msgstr ""
"Jos tämä kohta on valittuna, työkaluvihjeet tulevat esiin ilman "
"erikoisefektejä."

#: general.cpp:460
msgid "If this option is selected, tooltips fade in slowly."
msgstr "Jos tämä kohta on valittuna, työkaluvihjeet sulkeutuvat hitaasti."

#: general.cpp:461
msgid "If this option is selected, tooltips roll out slowly."
msgstr "Jos tämä kohta on valittuna, työkaluvihjeet rullaavat pois hitaasti."

#: general.cpp:656
msgid ""
"<h1>Style</h1> In this module you can configure how your KDE applications "
"will look.<p>"
" Styles and themes affect the way buttons, menus etc. are drawn in KDE.  "
"Think of styles as plugins that provide a general look for KDE and of themes "
"as a way to fine-tune those styles. E.g. you might use the \"System\" style "
"but use a theme that provides a color gradient or a marble texture.<p>"
" Apart from styles and themes you can configure here the behavior of "
"menubars and toolbars."
msgstr ""
"<h1>Tyyli</h1> Tässä modulissa voit asettaa miltä KDE-sovellukset "
"näyttävät. <p>Tyylit ja teemat vaikuttavat siihen miten painikkeet, "
"valikot jne. piirretään KDE:ssa.  Voit ajatella tyylejä sovelmina jotka "
"määrittelevät KDE:n yleisen ulkonäön ja teemoja tyylien "
"hienosäätönä. Voit esim. haluta \"Järjestelmä\" -tyylin, mutta haluat "
"teeman joka käyttää väriliukuja tai marmorikuviota.<p>Tyylien lisäksi "
"voit asettaa myös valikko- ja työkalurivien käyttäytymistä."
