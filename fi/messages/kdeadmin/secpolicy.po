msgid ""
msgstr ""
"Project-Id-Version: SECPOLICY\n"
"POT-Creation-Date: 2001-05-24 02:12+0200\n"
"PO-Revision-Date: 2001-05-30 01:33GMT+0200\n"
"Last-Translator: Kim Enkovaara <kim.enkovaara@iki.fi>\n"
"Language-Team: Finnish <fi@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.1\n"

#: main.cpp:9
msgid "What does this do?"
msgstr "Mitä tämä tekee?"

#: pamview.cpp:27
msgid "Available Services"
msgstr "Olemassaolevat palvelut"

#: pamview.cpp:34
msgid "Category"
msgstr "Kategoria"

#: pamview.cpp:35
msgid "Level"
msgstr "Taso"

#: pamview.cpp:36
msgid "Module"
msgstr "Moduli"

#: pamview.cpp:58
msgid ""
"/etc/pam.d directory does not exist.\n"
"Either your system does not have PAM support\n"
"or there is some other configuration problem."
msgstr ""
"/etc/pam.d hakemistoa ei ole olemassa.\n"
"Joko järjestelmässäsi ei ole PAM tukea, tai\n"
"järjestelmässä on joku muu asetusongelma."
