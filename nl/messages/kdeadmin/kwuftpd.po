# Nederlandse vertaling van kwuftpd
# Copyright (C) 2000 Free Software Foundation, Inc.
# Rinse de Vries <rinse@kde.nl>
# KDE-vertaalgroep Nederlands <i18n@kde.nl), 2000
# Bijgewerkt op 23 December 2000 door Niels Reedijk
msgid ""
msgstr ""
"Project-Id-Version: 2.0\n"
"POT-Creation-Date: 2001-06-10 17:48+0200\n"
"PO-Revision-Date: 2001-07-16 23:41+GMT\n"
"Last-Translator: Rinse de Vries <Rinse@kde.nl>\n"
"Language-Team: Nederlands <i18n@kde.nl>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: AddDlFree.cpp:13
msgid "Add freely downloadable directory"
msgstr "Vrijelijk downloadbare map toevoegen"

#: AddDlFree.cpp:15
msgid "Add freely downloadable file"
msgstr "Vrijelijk downloadbaar bestand toevoegen"

#: AddDlFree.cpp:19 AddDlFree.cpp:21 AddNoRet.cpp:14
msgid "&Filename:"
msgstr "&Bestandsnaam:"

#: AddMessage.cpp:17
msgid "Add Readme"
msgstr "Readme toevoegen"

#: AddMessage.cpp:19
msgid "Add Message"
msgstr "Bericht toevoegen"

#: AddMessage.cpp:25
msgid "Display at &login"
msgstr "Tonen bij &login"

#: AddMessage.cpp:29
msgid ""
"Message will be displayed after a user logs in.\n"
"The file has to be relative to the chrooted directory for anonymous/guest "
"users."
msgstr ""
"De berichten worden getoond nadat een gebruiker ingelogd is.\n"
"Het bestand moet evenredig zijn aan de startmap van de anonieme of "
"gastgebruiker."

#: AddMessage.cpp:30
msgid "Display at &CWD"
msgstr "In a&ctuele map tonen"

#: AddMessage.cpp:34
msgid ""
"Message will be displayed when a user enters the specified directory.\n"
"Path has to be relative to chroot for anonymous/guest users."
msgstr ""
"De berichten worden getoont wanneer een gebruiker de gespecificeerde map "
"binnengaat.\n"
"Het zoekpad moet evenredig zijn aan de startmap voor anonieme of "
"gastgebruikers."

#: AddMessage.cpp:37
msgid "&Message file"
msgstr "&Berichtbestand"

#: AddMessage.cpp:39
msgid "Filename of the message file - remember to make this relative to chroot!"
msgstr ""
"Bestandsnaam van het berichtbestand. - onthoud dat deze evenredig moet zijn "
"aan de startmap!"

#: AddMessage.cpp:41
msgid "&Directory"
msgstr "&Map"

#: AddMessage.cpp:41 Directories.cpp:24
msgid "Sele&ct"
msgstr "Sele&cteren"

#: AddMessage.cpp:43
msgid ""
"Directory in which the message will be displayed.\n"
"May be globbed: For example * means show in every directory.\n"
"This is especially useful if you're using ratios."
msgstr ""
"De map waarin het bericht wordt getoond.\n"
"Dit mag worden afgekort, bijv. * betekent tonen in elke map.\n"
"Dit is in het bijzonder bruikbaar als u verhoudingen gebruikt."

#: AddMessage.cpp:47
msgid "Display for &any class"
msgstr "Aan elke &klasse tonen."

#: AddMessage.cpp:48
msgid "The message will be displayed to everyone"
msgstr "Het bericht wordt aan iedereen getoond"

#: AddMessage.cpp:52
msgid "Displa&y for selected classes:"
msgstr "Tonen aan gesele&cteerde klassen:"

#: AddMessage.cpp:53
msgid "The message will be displayed only to members of the classes below."
msgstr "Het bericht wordt alleen getoond aan leden van onderstaande klassen."

#: AddMessage.cpp:68
msgid "The message will be displayed to members of the classes selected here."
msgstr "Het bericht wordt alleen getoond aan leden van de hier geselecteerde klassen."

#: AddNoRet.cpp:11
msgid "Add non-retrievable file/directory"
msgstr "Nieuwe onleesbare map/bestand toevoegen"

#: AddNoRet.cpp:17
msgid ""
"End the filename with a / to indicate it is\n"
"an entire directory; no file in this directory\n"
"will be retrievable."
msgstr ""
"Laat de bestandsnaam eindigen met een / om\n"
" aan te geven dat het een gehele map is. geen enkel\n"
"bestand uit die map is dan bereikbaar."

#: AddRCPT.cpp:12
msgid "Add recipient for upload-notification"
msgstr "Geadresseerde voor upload-notificatie toevoegen"

#: AddRCPT.cpp:15
msgid "&Address:"
msgstr "&Adres:"

#: AddVHost.cpp:11
msgid "Add virtual host"
msgstr "Virtuele host toevoegen"

#: AddVHost.cpp:14
msgid "&IP:"
msgstr "&IP:"

#: AddVHost.cpp:17
msgid "Enter the IP address of the new virtual host here."
msgstr "Voer hier het IP-adres in voor de nieuw virtuele host."

#: Directories.cpp:12
msgid "Root directory for &anonymous users:"
msgstr "Hoofdmap voor &anonieme gebruikers:"

#: Directories.cpp:14
msgid ""
"Anonymous users (username <i>ftp</i> or <i>anonymous</i>) will see the "
"directory entered here as their / directory."
msgstr ""
"Anonieme gebruikers (gebruikersnaam <i>ftp</i> of <i>anonymous</i>) zullen "
"deze map zien als hun / -map."

#: Directories.cpp:16
msgid "Root directory for &guest users:"
msgstr "Hoofdmap voor &gastgebruikers:"

#: Directories.cpp:16
msgid "S&elect"
msgstr "S&electeren"

#: Directories.cpp:18
msgid ""
"Guest users (real users with restricted access) will see the directory "
"entered here as their / directory."
msgstr ""
"Gastgebruikers (bekende gebruikers met beperkte toegang) zullen deze map "
"zien als hun / -map."

#: Directories.cpp:20
msgid "&Passwd file:"
msgstr "&Wachtwoordbestand:"

#: Directories.cpp:20 EditUpload.cpp:23
msgid "Se&lect"
msgstr "Se&lecteren"

#: Directories.cpp:22
msgid ""
"Use the alternate passwd file specified here for authenticating users.<br>"
"If this is left blank, the system passwd file will be used.<br>This setting "
"can be overridden for virtual hosts."
msgstr ""
"Gebruikt dit alternatieve wachtwoordbestand voor de authenticatie van "
"gebruikers.<br>"
" Als dit leeg gelaten wordt, dan wordt het passwd-bestand van het systeem "
"gebruikt.<br> Deze instelling kan omzeilt worden voor virtuele hosts."

#: Directories.cpp:24
msgid "&Shadow passwd file:"
msgstr "&Shadow-passwd-bestand:"

#: Directories.cpp:26
msgid ""
"Use the alternate shadow password file specified here for authenticating "
"users.<br>If this is left blank, the system shadow file will be "
"used.<br>This setting can be overridden for virtual hosts."
msgstr ""
"Gebruik dit alternatieve shadow-passwd-bestand voor de authenticatie van "
"gebruikers. <br>Als dit veld leeg gelaten wordt, dan wordt het "
"shadow-bestand van het systeem gebruikt. <br> Deze instelling kan worden "
"omzeilt voor virtuele hosts. "

#: EditLimit.cpp:28
msgid "Edit limit for class"
msgstr "Limiet voor klasse bewerken"

#: EditLimit.cpp:30
msgid "Edit limit"
msgstr "Limiet bewerken"

#: EditLimit.cpp:33
msgid "Add limit for class"
msgstr "Limiet voor klasse toevoegen"

#: EditLimit.cpp:35
msgid "Add limit"
msgstr "Limiet toevoegen"

#: EditLimit.cpp:41
msgid "Limit &always in effect"
msgstr "Limiet is &altijd actief"

#: EditLimit.cpp:45
msgid "Limit in effect at the following &days/hours:"
msgstr "Limiet is actief op de volgende &dagen/uren:"

#: EditLimit.cpp:51
msgid "&Monday"
msgstr "&maandag"

#: EditLimit.cpp:53
msgid "&Tuesday"
msgstr "&dinsdag"

#: EditLimit.cpp:55
msgid "&Wednesday"
msgstr "&woensdag"

#: EditLimit.cpp:57
msgid "Thu&rsday"
msgstr "d&onderdag"

#: EditLimit.cpp:59
msgid "&Friday"
msgstr "&vrijdag"

#: EditLimit.cpp:61
msgid "&Saturday"
msgstr "&zaterdag"

#: EditLimit.cpp:63
msgid "S&unday"
msgstr "zo&ndag"

#: EditLimit.cpp:66
msgid "&Hours:"
msgstr "&Uren:"

#: EditLimit.cpp:71 EditLimit.cpp:72
msgid ""
"Use this to specify at which hours this limit is in effect.\n"
"Example:\n"
"0630-1310 -> 06:30 AM - 01:10 PM\n"
"Defaults to 0000-2400."
msgstr ""
"Gebruik dit om aan te geven op welke uren dit limiet actief is.\n"
"Bijvoorbeeld:\n"
"0630-1310 -> 06:30 AM - 01:10 PM.\n"
"Standaard is 0000-2400."

#: EditLimit.cpp:77
msgid "&Limit to"
msgstr "&Limiet op"

#: EditLimit.cpp:84
msgid "users"
msgstr "gebruikers"

#: EditLimit.cpp:87
msgid "&No limit"
msgstr "&Geen limiet"

#: EditLimit.cpp:91
msgid "M&essage at violation:"
msgstr "B&ericht bij overschrijding:"

#: EditLimit.cpp:157
msgid "Select message file"
msgstr "Berichtbestand selecteren"

#: EditLimit.cpp:161 InputFileCtl.cpp:69 kwuftpd.cpp:148
msgid "Only local files are currently supported."
msgstr "Vooralsnog worden alleen lokale bestanden ondersteund."

#: EditUpload.cpp:19
msgid "Edit upload"
msgstr "Upload bewerken"

#: EditUpload.cpp:20
msgid "For users with root d&irectory:"
msgstr "Voor gebru&ikers met hoofdmap:"

#: EditUpload.cpp:23
msgid "&Directory (may be globbed):"
msgstr "&Map (mag asterisken bevatten):"

#: EditUpload.cpp:26
msgid "&Permit uploads; permissions:"
msgstr "&Uploads toegestaan; toegangsrechten:"

#: EditUpload.cpp:32
msgid "&Change owner of uploaded files:"
msgstr "Ei&genaar van inkomende bestanden wijzigen:"

#: EditUpload.cpp:53
msgid "C&hange group of uploaded files:"
msgstr "Groep van inkomende bestanden wij&zigen:"

#: EditUpload.cpp:74
msgid "Permit creating new d&irectories"
msgstr "Aan&maak van nieuwe mappen toestaan"

#: InputFileCtl.cpp:18
msgid "&Select"
msgstr "&Selecteren"

#: InputFileCtl.cpp:57 InputFileCtl.cpp:65
msgid "Select file"
msgstr "Bestand selecteren"

#: kwuftpd.cpp:22
msgid "KDE tool to configure wuftpd"
msgstr "KDE-programma voor het configureren van wuftpd."

#: kwuftpd.cpp:38
msgid "&Load /etc/ftpaccess"
msgstr "/etc/ftpaccess &laden"

#: kwuftpd.cpp:39
msgid "Load &other file"
msgstr "Ander bestan&d laden"

#: kwuftpd.cpp:41
msgid "&Save /etc/ftpaccess"
msgstr "/etc/ftpaccess op&slaan"

#: kwuftpd.cpp:42
msgid "Save o&ther file"
msgstr "&Ander bestand opslaan"

#: kwuftpd.cpp:106
msgid "User &Classes"
msgstr "Gebruikers&klassen"

#: kwuftpd.cpp:107
msgid "&Directories"
msgstr "&Mappen"

#: kwuftpd.cpp:108
msgid "&Security"
msgstr "&Beveiliging"

#: kwuftpd.cpp:109
msgid "&Messages"
msgstr "B&erichten"

#: kwuftpd.cpp:110
msgid "&Logging"
msgstr "&Loggen"

#: kwuftpd.cpp:111
msgid "Rat&ios"
msgstr "Ver&houdingen"

#: kwuftpd.cpp:112
msgid "&Uploads"
msgstr "&Uploads"

#: kwuftpd.cpp:113
msgid "&Virtual Hosts"
msgstr "&Virtuele hosts"

#: kwuftpd.cpp:130 kwuftpd.cpp:146
msgid "Select ftpaccess file"
msgstr "ftpacces-bestand selecteren"

#: kwuftpd.cpp:132
msgid ""
"Only local files are supported yet.\n"
"/etc/ftpaccess used instead."
msgstr ""
"Vooralsnog worden alleen lokale bestanden ondersteund.\n"
"In plaats hiervan wordt /etc/ftpacces gebruikt."

#: kwuftpd.cpp:156
msgid "An error occured while trying to save."
msgstr "Er deed zich een fout voor tijdens het opslaan."

#: kwuftpd.cpp:175
msgid "KWuFTPd"
msgstr "KWuFTPd"

#: Logging.cpp:17
msgid "Log commands:"
msgstr "Commando's loggen:"

#: Logging.cpp:18
msgid ""
"Use these check boxes to control for which types of users all commands will "
"be logged."
msgstr ""
"Gebruik deze instellingen om te bepalen van welk type gebruikers alle "
"commando's zullen worden gelogd."

#: Logging.cpp:23
msgid "Log transfers:"
msgstr "Overdrachten loggen:"

#: Logging.cpp:24
msgid ""
"Use these check boxes to control for which types of users file transfers "
"will be logged."
msgstr ""
"Gebruik deze instellingen om te bepalen van welk type gebruiker de "
"bestandsoverdrachten zullen worden gelogd."

#: Logging.cpp:28
msgid "Inbound:"
msgstr "Inkomend:"

#: Logging.cpp:29
msgid "Log inbound transfers (uploads to this server)"
msgstr "Log de inkomende overdrachten. (uploads naar deze server)"

#: Logging.cpp:31
msgid "Outbound:"
msgstr "Uitgaand:"

#: Logging.cpp:32
msgid "Log outbound transfers (downloads from this server)"
msgstr "Log uitgaande overdrachten (downloads van deze server)"

#: Logging.cpp:33
msgid "Security log:"
msgstr "Beveiligingslog:"

#: Logging.cpp:34
msgid "Log security-related messages (failed logins etc.)"
msgstr "Aan beveiliging gerelateerde berichten (falende logins etc.) loggen."

#: Logging.cpp:48
msgid "A&nonymous"
msgstr "A&nomiem"

#: Logging.cpp:49
msgid ""
"Log commands given by anonymous (username <i>ftp</i> or <i>anonymous</i>) "
"users."
msgstr ""
"Log de commando's die gegeven worden door anomieme (gebruikersnaam "
"<i>ftp</i> of <i>anonymous</i>) gebruikers."

#: Logging.cpp:52 NewClass.cpp:26 UserClasses.cpp:31
msgid "&Guest"
msgstr "&Gast"

#: Logging.cpp:53
msgid ""
"Log commands given by guest users (users with a real account, but restricted "
"access)."
msgstr ""
"Log de commando's die gegeven worden door gastgebruikers (bezoekers met "
"volwaardige toegang, maar met beperkte toegangsrechten)."

#: Logging.cpp:56 NewClass.cpp:29 UserClasses.cpp:33
msgid "&Real"
msgstr "Be&kend"

#: Logging.cpp:57
msgid ""
"Log commands given by real users (users with an unrestricted account on the "
"server"
msgstr ""
"Log de commando's die gegeven worden door bekende gebruikers (gebruikers met "
"volwaardige toegang tot de server)."

#: Logging.cpp:60
msgid "An&onymous"
msgstr "An&oniem"

#: Logging.cpp:61
msgid ""
"Log inbound transfers (uploads to the server) by anonymous users (username "
"<i>ftp</i> or <i>anonymous</i>)"
msgstr ""
"Log de inkomende overdrachten (uploads naar de server) van anonieme "
"gebruikers (gebruikersnaam <i>ftp</i> of <i>anonymous</i>)."

#: Logging.cpp:63
msgid "G&uest"
msgstr "&Gast"

#: Logging.cpp:64
msgid ""
"Log inbound transfers (uploads to the server) by guest users (users with a "
"real account, but restricted access)"
msgstr ""
"Log de inkomende overdrachten (uploads naar de server) van gastgebruikers "
"(gebruikers met een volwaardig account, maar met beperkte toegang)."

#: Logging.cpp:66
msgid "R&eal"
msgstr "Be&kend"

#: Logging.cpp:67
msgid ""
"Log inbound transfers (uploads to the server) by real users (users with a "
"real account and without restrictions)"
msgstr ""
"Log de inkomende overdrachten (uploads naar de server) van bekende "
"gebruikers (gebruikers met een volwaardig account, zonder restricties)."

#: Logging.cpp:70
msgid "Anon&ymous"
msgstr "Anon&iem"

#: Logging.cpp:71
msgid ""
"Log outbound transfers (downloads from the server) by anonymous users "
"(username <i>ftp</i> or <i>anonymous</i>)"
msgstr ""
"Log de uitgaande overdrachten (downloads van server) van anonieme "
"gebruikers(gebruikersnaam <i>ftp</i> of <i>anonymous</i>)."

#: Logging.cpp:73
msgid "Gue&st"
msgstr "Ga&st"

#: Logging.cpp:74
msgid ""
"Log outbound transfers (downloads from the server) by guest users (users "
"with a real account, but restricted access)"
msgstr ""
"Log de uitgaande overdrachten (downloads van server) van gastgebruikers "
"(gebruikers met een volwaardig account, maar met beperkte toegang)."

#: Logging.cpp:76
msgid "Re&al"
msgstr "Beken&d"

#: Logging.cpp:77
msgid ""
"Log outbound transfers (downloads from the server) by real users (users with "
"a real account and without restrictions)"
msgstr ""
"Log de uitgaande overdrachten (downloads van server) van bekende gebruikers "
"(gebruikers met een volwaardig account, zonder verdere restricties)."

#: Logging.cpp:79
msgid "Anony&mous"
msgstr "Anonie&m"

#: Logging.cpp:81
msgid ""
"Keep a security log (failed login attempts etc.) for anonymous users "
"(username <i>anonymous</i> or <i>ftp</i>)"
msgstr ""
"Houd een beveiligingslog (falende loginpogingen etc.) bij van anonieme "
"gebruikers (gebruikersnaam <i>anonymous</i> of <i>ftp</i>)."

#: Logging.cpp:82
msgid "Gues&t"
msgstr "Gas&t"

#: Logging.cpp:84
msgid ""
"Keep a security log (failed login attempts etc.) for guest users (real users "
"with restricted access)"
msgstr ""
"Houd een beveiligingslog (falende loginpogingen etc.) bij van gastgebruikers "
"(volwaardige gebruikers met beperkte toegang)."

#: Logging.cpp:85
msgid "Rea&l"
msgstr "&Bekend"

#: Logging.cpp:87
msgid ""
"Keep a security log (failed login attempts etc.) for guest users (real users "
"with unrestricted access)"
msgstr ""
"Houd een beveiligingslog (falende loginpogingen etc.) bij van bekende "
"gebruikers (volwaardige gebruikers met onbeperkte toegang)."

#: Logging.cpp:88
msgid "Redirect lo&g to syslog"
msgstr "Lo&g omleiden naar syslog"

#: Logging.cpp:91
msgid "Log to syslog instead of logfiles"
msgstr "Log opslaan in syslog in plaats van in logbestanden."

#: Logging.cpp:93
msgid "E-&mail ftpadmin on uploads:"
msgstr "E-&mail sturen aan ftp-admin. bij uploads:"

#: Logging.cpp:94
msgid "Automatically notify the administrator of uploads from anonymous users"
msgstr ""
"Waarschuw de administrator automatisch bij inkomende overdrachten van "
"anonieme gebruikers."

#: Logging.cpp:98
msgid "&Send mail as"
msgstr "E-mail &versturen als"

#: Logging.cpp:100
msgid ""
"The address entered here will appear in the <i>From:</i> header of upload "
"notification e-mail messages sent by wu-ftpd."
msgstr ""
"Het adres dat hier wordt ingevuld verschijnt in het <i>From:</i>-kopschrift "
"van de e-mailberichten die verstuurd worden door wu-ftpd."

#: Logging.cpp:101
msgid "Mail ser&ver"
msgstr "E-mailse&rver"

#: Logging.cpp:103
msgid ""
"The mail server entered here will be used to transmit upload notification "
"e-mail messages sent by wu-ftpd. If left blank, localhost will be used."
msgstr ""
"Deze e-mailserver wordt gebruikt om de e-mailberichten van wu-ftpd te "
"verzenden. Als u niets invult wordt localhost gebruikt."

#: Logging.cpp:104
msgid "&Send mail to:"
msgstr "E-mail &sturen aan:"

#: Logging.cpp:105 Logging.cpp:107
msgid ""
"This list box shows the recipient(s) of upload notification e-mails. Use the "
"<b>Add</b> and <b>Remove</b> buttons below to modify the recipients."
msgstr ""
"Dit vak toont de geadresseerde aan wie de e-mailberichten verstuurd worden. "
"Gebruik de knoppen <b>Toevoegen</b> en <b>Verwijderen</b> om de "
"geadresseerde te wijzigen."

#: Logging.cpp:112 Ratios.cpp:91 Security.cpp:19 Uploads.cpp:25 Virtual.cpp:17
msgid "&Add"
msgstr "&Toevoegen"

#: Logging.cpp:114
msgid "Add a new recipient for upload notification e-mails"
msgstr ""
"Voeg een nieuwe geadresseerde toe voor e-mailberichten met "
"upload-notificaties."

#: Logging.cpp:118
msgid "Remove the currently selected recipient for upload notification e-mails"
msgstr "Verwijder de momenteel geselecteerde geadresseerde."

#: Messages.cpp:11
msgid "&Banner"
msgstr "Ba&nier"

#: Messages.cpp:14
msgid ""
"The file selected here will be displayed to\n"
"ftp users before login.\n"
"May break compatibility with truly ancient clients."
msgstr ""
"Het bestand dat u hier selecteert wordt getoont aan\n"
"ftp-gebruikers voor de login.\n"
"Dit kan voor compatibiliteitsproblemen zorgen bij antieke cliënten."

#: Messages.cpp:15
msgid "&Hostname (blank=default)"
msgstr "&Hostnaam (leeg=standaard)"

#: Messages.cpp:18
msgid ""
"The hostname given here will be shown at login and for\n"
"the %L cookie in message files.\n"
"If this is blank, the canonical hostname of your system will be used."
msgstr ""
"De hostnaam die u hier opgeeft wordt getoond tijdens de login en voor\n"
"de %L-cookie gebruikt in berichtbestanden.\n"
"Als dit leeg is, dan wordt de complete naam van uw systeem gebruikt."

#: Messages.cpp:19
#, c-format
msgid "E-Mail of &admin (for %E):"
msgstr "E-mailadres van &admin. (voor %E):"

#: Messages.cpp:21
#, c-format
msgid "E-Mail address returned with the %E cookie in message files."
msgstr "E-mailadres, dat voor de %E-cookie gebruikt wordt in berichtbestanden."

#: Messages.cpp:23
msgid "Show &messages every time"
msgstr "B&erichten telkens tonen"

#: Messages.cpp:24
msgid "If this is not checked, message files at CWD will be displayed only once."
msgstr ""
"Als dit niet is ingeschakeld, dan worden berichtbestanden in iedere map "
"slechts eenmaal getoond."

#: Messages.cpp:26
msgid "Show &readmes every time"
msgstr "&Readmes telkens tonen"

#: Messages.cpp:29
msgid "If this is not checked, hints on README files will be displayed only once."
msgstr ""
"Als dit niet is ingeschakeld, dan wordt de verwijzing naar README-bestanden "
"slechts eenmaal getoond."

#: Messages.cpp:30
msgid "Add M&essage"
msgstr "Beri&cht toevoegen"

#: Messages.cpp:31
msgid "Remo&ve Message"
msgstr "Bericht ver&wijderen"

#: Messages.cpp:32
msgid "Add Rea&dme"
msgstr "Rea&dme toevoegen"

#: Messages.cpp:33
msgid "Rem&ove Readme"
msgstr "Readme verwi&jderen"

#: Messages.cpp:34
msgid "&Messages:"
msgstr "&Berichten:"

#: Messages.cpp:36
msgid "&Readmes:"
msgstr "&Readmes:"

#: Messages.cpp:42 Messages.cpp:44 Messages.cpp:45 Messages.cpp:46
msgid ""
"Messages are files that are automatically displayed to the user after "
"logging in or after entering a directory. Use the <b>Add Message</b> and "
"<b>Remove Message</b> buttons to modify the messages."
msgstr ""
"Berichten zijn bestanden die automatisch aan de gebruiker worden getoond "
"nadat deze is ingelogd, of na het openen van een map. Gebruik de knoppen "
"<b>Bericht toevoegen</b> en <b>Bericht verwijderen</b> om de berichten te "
"bewerken."

#: Messages.cpp:53 Messages.cpp:54 Messages.cpp:55 Messages.cpp:56
msgid ""
"When you've defined README files, users changing to a directory containing "
"files matching the glob listed here will be pointed to these files with a "
"<i>Please read the file README, last modified x days ago</i> message.<br>"
"Use the <b>Add Readme</b> and <b>Remove Readme</b> buttons to modify the "
"list of README files."
msgstr ""
"Als u de README-bestanden hebt gedefiniëerd, dan worden de gebruikers op "
"dit bestand gewezen wanneer ze een map met gegevens openen. De melding ziet "
"er dan als volgt uit: <i>Please read the file README, last modified x days "
"ago</i> <br>Gebruik de knoppen <b>Readme toevoegen</b> en <b>Readme "
"verwijderen</b> om de lijst met README-bestanden te bewerken."

#: NewClass.cpp:12
msgid "Add new user class"
msgstr "Nieuwe gebruikersklasse toevoegen"

#: NewClass.cpp:15
msgid "New class &name:"
msgstr "Naam &nieuwe klasse:"

#: NewClass.cpp:17 NewClass.cpp:18
msgid ""
"The name of your new user class -\n"
"may be anything, but may not contain whitespaces."
msgstr ""
"De naam voor uw nieuwe gebruikersklasse -\n"
"dat mag van alles zijn, maar dan wel zonder spaties."

#: NewClass.cpp:23 UserClasses.cpp:29
msgid "&Anonymous"
msgstr "&Anoniem"

#: NewClass.cpp:25
msgid ""
"Anonymous users\n"
"(login=ftp or anonymous)"
msgstr ""
"Anonieme gebruikers\n"
"(login='ftp' of 'anonymous')"

#: NewClass.cpp:28
msgid ""
"Guest users\n"
"(chrooted real users)"
msgstr ""
"Gastgebruikers\n"
"(Bekende gebruikers met beperkte toegang)"

#: NewClass.cpp:31
msgid "Real users"
msgstr "Bekende gebruikers"

#: NewClass.cpp:32
msgid "A&ddresses"
msgstr "A&dressen"

#: NewClass.cpp:37 NewClass.cpp:38 UserClasses.cpp:40 UserClasses.cpp:41
msgid ""
"Specify the addresses for this user class here;\n"
"may be globbed:\n"
"* = any address\n"
"*.localdomain.com = any address in localdomain.com\n"
"127.0.0.* = IPs 127.0.0.1-127.0.0.255\n"
"..."
msgstr ""
"Specificeer hier de adressen voor deze gebruikersklassen.\n"
"U hebt o.a. de volgende mogelijkheden:\n"
"* = elk mogelijk adres\n"
"*.localdomain.com = elk mogelijk adres uit localdomain.com\n"
"127.0.0.* = IP-adressen 127.0.0.1-127.0.0.255\n"
"..."

#: NewClass.cpp:64
msgid "Class name may not be empty."
msgstr "De klassenaam mag niet leeg zijn."

#: NewClass.cpp:68
msgid "Class name may not contain whitespaces"
msgstr "De klassenaam mag geen spaties bevatten."

#: NewClass.cpp:72
msgid "You must check at least one of Anonymous, Guest or Real!"
msgstr ""
"U moet minstens één van de anonieme, gast- of bekende gebruikers "
"inschakelen!"

#: NewClass.cpp:77
msgid "Addresses can not be empty. Assume '*'?"
msgstr "Adressen kunnen niet leeg zijn, '*' aannemen?"

#: Permissions.cpp:9
msgid "Special:"
msgstr "Speciaal:"

#: Permissions.cpp:10
msgid "Owner:"
msgstr "Eigenaar:"

#: Permissions.cpp:11
msgid "Group:"
msgstr "Groep:"

#: Permissions.cpp:12
msgid "Other:"
msgstr "Overig:"

#: Permissions.cpp:13
msgid "Set&UID"
msgstr "&UID instellen"

#: Permissions.cpp:14
msgid "Set&GID"
msgstr "&GID instellen"

#: Permissions.cpp:15
msgid "Stick&y"
msgstr "Vast&geplakt"

#: Permissions.cpp:16
msgid "&Read"
msgstr "&Lezen"

#: Permissions.cpp:17
msgid "&Write"
msgstr "&Schrijven"

#: Permissions.cpp:18
msgid "&Execute"
msgstr "U&itvoeren"

#: Permissions.cpp:19
msgid "Re&ad"
msgstr "L&ezen"

#: Permissions.cpp:20
msgid "Wr&ite"
msgstr "S&chrijven"

#: Permissions.cpp:21
msgid "E&xecute"
msgstr "Uit&voeren"

#: Permissions.cpp:22
msgid "Rea&d"
msgstr "Leze&n"

#: Permissions.cpp:23
msgid "Wri&te"
msgstr "Schri&jven"

#: Permissions.cpp:24
msgid "Exe&cute"
msgstr "Uitv&oeren"

#: Ratios.cpp:14
msgid "&Upload/download ratio: 1:"
msgstr "&Upload-/downloadverhouding: 1:"

#: Ratios.cpp:20
msgid "for &anonymous users"
msgstr "voor &anonieme gebruikers"

#: Ratios.cpp:23
msgid "for &guest users"
msgstr "voor &gastgebruikers"

#: Ratios.cpp:25
msgid "If enabled, an anonymous user can download only [x] bytes per uploaded byte."
msgstr ""
"Indien ingeschakeld, kan een anonieme gebruiker maximaal [x] bytes "
"downloaden per uploaded byte."

#: Ratios.cpp:26
msgid "If enabled, a guest can download only [x] bytes per uploaded byte."
msgstr ""
"Indien ingeschakeld, kan een gastgebruiker maximaal [x] bytes downloaden per "
"uploaded byte."

#: Ratios.cpp:29
msgid "&Time limit:"
msgstr "&Tijdslimiet:"

#: Ratios.cpp:35
msgid "minutes"
msgstr "minuten"

#: Ratios.cpp:38
msgid "for a&nonymous users"
msgstr "voor a&nonieme gebruikers"

#: Ratios.cpp:41
msgid "for g&uest users"
msgstr "voor ga&stgebruikers"

#: Ratios.cpp:45
msgid "U&pload limit:"
msgstr "U&ploadlimiet:"

#: Ratios.cpp:51 Ratios.cpp:67
msgid "bytes"
msgstr "bytes"

#: Ratios.cpp:54
msgid "for an&onymous users"
msgstr "voor an&onieme gebruikers"

#: Ratios.cpp:57
msgid "for gu&est users"
msgstr "voor gastg&ebruikers"

#: Ratios.cpp:61
msgid "&Download limit:"
msgstr "&Downloadlimiet:"

#: Ratios.cpp:70
msgid "for anon&ymous users"
msgstr "voor anon&ime gebruikers"

#: Ratios.cpp:73
msgid "for gue&st users"
msgstr "voor ga&stgebruikers"

#: Ratios.cpp:77
msgid "Freely downloadable files:"
msgstr "Vrijelijk downloadbare bestanden:"

#: Ratios.cpp:80
msgid "Freely downloadable directories:"
msgstr "Vrijelijk downloadbare mapen:"

#: Ratios.cpp:98
msgid "A&dd"
msgstr "Toe&voegen"

#: Ratios.cpp:101
msgid "Remo&ve"
msgstr "&Verwijderen"

#: Security.cpp:15
msgid "&Noretrieves:"
msgstr "&Noretrieves:"

#: Security.cpp:18 Security.cpp:24 Security.cpp:30 Security.cpp:31
msgid ""
"Noretrieves are files and directories that can't be retrieved (downloaded) "
"by remote users. Use the <b>Add</b> and <b>Remove</b> buttons below to "
"control noretrieves."
msgstr ""
"Noretrieves zijn bestanden en mappen die niet kunnen worden opgehaald "
"(gedownload) door gebruikers op afstand. Gebruik de knoppen <b>Toevoegen</b>"
" en <b>Verwijderen</b> hieronder om de noretrieves te bepalen."

#: Security.cpp:32
msgid "Number of allo&wed failed logins:"
msgstr "Toegestaand aantal &falende logins:"

#: Security.cpp:34 Security.cpp:37
msgid ""
"A user may attempt to login with a wrong password this many times before the "
"connection is closed."
msgstr ""
"Een gebruiker mag zo vaak proberen in te loggen met een onjuist wachtwoord "
"voordat de verbinding wordt verbroken."

#: Security.cpp:46
msgid "Permit SITE &GROUP"
msgstr "SITE &GROUP toestaan"

#: Security.cpp:47
msgid ""
"Permit users to change their group membership using the ftp SITE GROUP "
"command"
msgstr ""
"Staat het toe dat gebruikers hun groepslidmaatschap wijzigen via het "
"ftp-commando SITE GROUP."

#: Security.cpp:53
msgid "Permit the following commands to users:"
msgstr "Geef gebruikers toestemming voor de volgende commando's:"

#: Security.cpp:58
msgid "Anonymous"
msgstr "Anoniem"

#: Security.cpp:61
msgid "Guest"
msgstr "Gast"

#: Security.cpp:64
msgid "Real"
msgstr "Bekend"

#: Security.cpp:142
msgid "Check password for anonymous:"
msgstr "Wachtwoord van anonymous controleren:"

#: Security.cpp:146
msgid "&No"
msgstr "&Nee"

#: Security.cpp:147
msgid "Don't check password given for anonymous/ftp at all"
msgstr "Controleer het wachtwoord van anonymous/ftp helemaal niet."

#: Security.cpp:151
msgid "&trivial"
msgstr "&trivial"

#: Security.cpp:152
msgid "Password for anonymous/ftp must contain an @ character"
msgstr "Wachtwoorden van anonymous/ftp moeten een @ bevatten."

#: Security.cpp:156
msgid "&RFC822"
msgstr "&RFC822"

#: Security.cpp:157
msgid ""
"Make sure password for anonymous/ftp is a valid e-mail address\n"
"according to RFC822"
msgstr ""
"Zorg er voor dat het wachtwoord van anonymous/ftp een geldig e-mailadres is\n"
"volgens RFC822"

#: Security.cpp:161
msgid "Enf&orce"
msgstr "A&fdwingen"

#: Security.cpp:162
msgid ""
"If this is checked, people using something other than an e-mail address\n"
"as anonymous password will not be allowed to log in.\n"
"\n"
"If this is not checked, they will just be warned and told to use their\n"
"e-mail address as password next time."
msgstr ""
"Als dit is ingeschakeld, dan krijgen gebruikers die iets anders invoeren dan "
"een\n"
"e-mailadres geen toestemming om in te loggen.\n"
"\n"
"Als dit niet is ingeschakeld, dan worden ze er alleen op gewezen\n"
"dat ze de volgende keer hun e-mailadres moeten opgeven."

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Rinse de Vries,Niels Reedijk"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "rinse@kde.nl,nielx@kde.nl"

#: UserClasses.cpp:27
msgid ""
"User classes can be used to control access from specific hosts/users. Use "
"the <b>Add class</b> and <b>Delete class</b> buttons on the right of this "
"box to add or remove classes. Use the other buttons to control the access "
"rights of users in the class selected here."
msgstr ""
"Gebruikersklassen kunnen worden gebruikt om de toegang van specifieke "
"hosts/gebruikters te bepalen. Gebruikt de knoppen \"<b>Klasse "
"toevoegen</b>\" en \"<b>Klasse verwijderen</b>\" rechts van dit vak om de "
"klassen toe te voegen of te verwijderen. Gebruik de overige knoppen voor het "
"bepalen van de toegangsrechten van de gebruikers in de geselecteerde klasse."

#: UserClasses.cpp:30
msgid "Anonymous users (login=ftp or anonymous) can belong to this class"
msgstr ""
"Anonieme gebruikers (login='ftp' of 'anonymous') kunnen tot deze klase "
"behoren. "

#: UserClasses.cpp:32
msgid "Guest users (restricted real users) can belong to this class"
msgstr ""
"Gastgebruikers (bekende gebruikers met beperkte toegangsrechten) kunnen "
"behoren tot deze klasse."

#: UserClasses.cpp:34
msgid "Real users can belong to this class"
msgstr "Bekende gebruikers kunnen behoren tot deze klasse."

#: UserClasses.cpp:38
msgid "A&ddresses:"
msgstr "A&dressen:"

#: UserClasses.cpp:45
msgid "Add &class"
msgstr "Klasse &Toevoegen"

#: UserClasses.cpp:47
msgid "Add a new user class"
msgstr "Voeg een nieuwe gebruikersklasse toe."

#: UserClasses.cpp:49
msgid "D&elete class"
msgstr "Klasse &verwijderen"

#: UserClasses.cpp:50
msgid "Delete the currently selected user class"
msgstr "Verwijder de momenteel geselecteerde gebruikersklasse."

#: UserClasses.cpp:54
msgid "Aut&ogroup to:"
msgstr "Aut&om. groep van:"

#: UserClasses.cpp:56 UserClasses.cpp:60
msgid ""
"Users from this class are automatically members\n"
"of the group selected here.\n"
"That way, you can (for example) grant local users\n"
"anonymous access to files remote users\n"
"can't download."
msgstr ""
"Gebruikers van deze klasse zijn automatisch leden\n"
"van de hier geselecteerde groep.\n"
"Op die manier kunt u (bijvoorbeeld) anonieme toegang\n"
"verlenen tot bestanden die gebruikers op afstand\n"
"niet kunnen downloaden."

#: UserClasses.cpp:69
msgid "L&imits:"
msgstr "L&imieten:"

#: UserClasses.cpp:70 UserClasses.cpp:74 UserClasses.cpp:87 UserClasses.cpp:88
#: UserClasses.cpp:89
msgid ""
"You can use limits to restrict the number of simultaneous connections of "
"users in this class at specific times. Use the <b>Add Limit</b>, <b>Edit "
"Limit</b> and <b>Delete Limit</b> buttons on the right to control limits."
msgstr ""
"U kunt gebruik maken van limieten om het aantal simultane verbindingen van "
"gebruikers uit deze klasse te beperken. Gebruik de knoppen <b>Limiet "
"toevoegen</b>, <b>Limiet bewerken</b> en <b>Limiet verwijderen</b>"
" rechts om de limieten te bepalen."

#: UserClasses.cpp:78
msgid "Add &Limit"
msgstr "Limiet toe&voegen"

#: UserClasses.cpp:81
msgid "Edi&t Limit"
msgstr "Limiet be&werken"

#: UserClasses.cpp:84
msgid "Delete Li&mit"
msgstr "Limiet verwij&deren"

#: UserClasses.cpp:134 UserClasses.cpp:144
msgid "Limits are specific to user classes - create a class first."
msgstr "Limieten zijn van toepassing op gebruikersklassen - maak er eerst een aan."

#: UserClasses.cpp:161 UserClasses.cpp:186
msgid "You must select a limit first."
msgstr "U moet eerst een limiet selecteren."

#: UserClasses.cpp:171
msgid "Select a class first."
msgstr "Selecteer eerst een klasse."

#: Virtual.cpp:16
msgid ""
"Select the virtual host you want to edit here. If this field is empty, click "
"the <b>Add</b> button below to create a virtual host."
msgstr ""
"Selecteer de virtuele host die u hier wilt bewerken. Als dit veld leeg is, "
"klik dan op de knop <b>Toevoegen</b> om een virtuele host aan te maken."

#: Virtual.cpp:19
msgid "Click here to add a new virtual host."
msgstr "Klik hier om een nieuwe virtuele host aan te maken."

#: Virtual.cpp:22
msgid "Remove the selected virtual host."
msgstr "Verwijder de geselecteerde virtuele host."

#: Virtual.cpp:28
msgid "Root directory:"
msgstr "Hoofdmap:"

#: Virtual.cpp:30
msgid ""
"Select the root directory of this virtual host.<br>"
"The root directory is the directory users logging in to this server will see "
"as /."
msgstr ""
"Selecteer de hoofdmap van deze virtuele host. <br>De hoofdmap is de map die "
"door inloggende gebruikers wordt gezien als /."

#: Virtual.cpp:31
msgid "Banner:"
msgstr "Banier:"

#: Virtual.cpp:33
msgid ""
"The file selected here will be displayed to the user before logging "
"in.<br><b>Note:</b> The file name is relative to the root directory "
"specified above."
msgstr ""
"Het hier opgegeven bestand zal voor het inloggen worden getoond aan de "
"gebruiker. <br><b>N.B.:</b>"
" De bestandsnaam is evenredig aan de hoofdmap die hierboven is opgegeven."

#: Virtual.cpp:34
msgid "Logfile:"
msgstr "Logbestand:"

#: Virtual.cpp:36
msgid "Transfers from this virtual server will be logged to the file specified here."
msgstr ""
"Overdrachten van deze virtuele server worden gelogd in het hier opgegeven "
"bestand."

#: Virtual.cpp:37
msgid "Passwd file:"
msgstr "Passwd-bestand:"

#: Virtual.cpp:39
msgid ""
"If you specify a file here, it will be used as an alternative passwd file "
"for authenticating users on this virtual server.\n"
"If you leave this blank, the system passwd file will be used."
msgstr ""
"Als u hier een bestand opgeeft, dan wordt dat gebruikt als alternatief "
"passwd-bestand voor het valideren van gebruikers op deze virtuele server.\n"
"Als u dit leeg laat, dan wordt het passwd-bestand van het systeem gebruikt."

#: Virtual.cpp:40
msgid "Shadow file:"
msgstr "Shadow-bestand:"

#: Virtual.cpp:42
msgid ""
"If you specify a file here, it will be used as an alternative shadow file "
"(storing passwords) for authenticating users on this virtual server.\n"
"If you leave this blank, the system shadow file will be used, unless you "
"specified a passwd file containing password entries above."
msgstr ""
"Als u hier een bestand opgeeft, dan wordt deze gebruikt als alternatief "
"shadow-bestand (met wachtwoorden) voor het valideren van gebruikers op deze "
"virtuele server.\n"
"Als u dit leeg laat, dan wordt het shadow-bestand van het systeem gebruikt, "
"tenzij u hierboven een passwd-bestand met wachtwoordingangen hebt opgegeven."

#: Virtual.cpp:44
msgid "Host name:"
msgstr "Hostnaam:"

#: Virtual.cpp:46
msgid ""
"Host name of this virtual host. The host name specified here will be "
"displayed to the user before login; it is also returned for the %L macros in "
"welcome messages."
msgstr ""
"De hostnaam voor deze virtuele host. De hostnaam die hier is opgegeven zal "
"worden getoond aan de gebruiker voordat deze inlogt. Het zal ook gebruikt "
"worden voor de %L macro's in welkomstberichten."

#: Virtual.cpp:47
msgid "Administrator E-Mail:"
msgstr "E-mailadres administrator:"

#: Virtual.cpp:49
#, c-format
msgid ""
"E-Mail of the administrator for this virtual domain. This is used solely for "
"the %E macro in welcome messages."
msgstr ""
"Het e-mailadres van de administrator van dit virtuele domein. Het wordt "
"uitsluitend gebruikt voor de %E-macro in welkomstberichten."

#: Virtual.cpp:50
msgid "Allow anonymous logins"
msgstr "Anonieme logins toelaten"

#: Virtual.cpp:52
msgid ""
"If checked, anonymous logins (username ftp or anonymous) are permitted for "
"this virtual server."
msgstr ""
"Als dit is ingeschakeld, dan zijn anonieme logins (gebruikersnaam ftp of "
"anonymous) toegestaan voor deze virtuele server."

#: Virtual.cpp:55
msgid "&Real user logins:"
msgstr "Logins van &bekende gebruikers:"

#: Virtual.cpp:56
msgid ""
"Use the buttons here to control which real users can log in to this virtual "
"server."
msgstr ""
"Gebruik deze knoppen om te bepalen welke bekende gebruikers kunnen inloggen "
"op deze virtuele server."

#: Virtual.cpp:57
msgid "&Deny all"
msgstr "Allen &verbieden"

#: Virtual.cpp:59
msgid "&Allow all"
msgstr "&Allen toelaten"

#: Virtual.cpp:61
msgid "Allow &specified:"
msgstr "G&especificeerden toelaten:"

#: Virtual.cpp:63
msgid "Den&y specified:"
msgstr "Gespecificeerden ver&bieden:"
