# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-07-30 03:11GMT+1\n"
"Last-Translator: Otto Bruggeman <otto.bruggeman@home.nl>\n"
"Language-Team: Nederlands (dutch) <nl@li.org, i18n@kde.nl>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.4\n"

#: index.docbook:2
msgid "rlogin"
msgstr "rlogin"

#: index.docbook:4
msgid ""
"Using &konqueror; you can statr up an <command>rlogin</command>"
" session with a server hosting the rlogin service."
msgstr ""
"Door van &konqueror; gebruik te maken kunt u een <command>rlogin</command> "
"sessie starten met een server die een rlogin service aanbiedt."

#: index.docbook:7
msgid ""
"To use this kioslave feature, in the &konqueror; <acronym>URL</acronym> bar "
"type <userinput>rlogin:/host_to_connect_to</userinput>"
msgstr ""
"Om de mogelijkheden van deze ioslaaf in &konqueror; te kunnen gebruiken "
"voert u <userinput>rlogin:/host_om_mee_te_verbinden</userinput> in de "
"<acronym>URL</acronym> balk."

#: index.docbook:11
msgid ""
"This will initialize &konsole; with an <command>rlogin</command> session, "
"prompting you for your password."
msgstr ""
"Dit zal een &konsole; starten met een <command>rlogin</command>"
" sessie dat u om een wachtwoord vraagt."

#: index.docbook:14
msgid ""
"The <command>rlogin</command> kioslave uses the username of the account you "
"are currently using in &kde;."
msgstr ""
"De <command>rlogin</command> ioslaaf gebruikt de gebruikersnaam van het "
"account dat u op dit moment in &kde; gebruikt."

#: index.docbook:17
msgid ""
"After you have successfully entered your password, you can begin your remote "
"session."
msgstr ""
"Nadat u uw wachtwoord succesvol heeft ingevoerd kunt u uw remote sessie "
"beginnen."
