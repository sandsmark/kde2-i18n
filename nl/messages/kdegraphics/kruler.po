# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-05-05 01:43+0200\n"
"PO-Revision-Date: 2001-01-28 15:47GMT\n"
"Last-Translator: Rinse de Vries <Rinse@kde.nl>\n"
"Language-Team: Nederlands <i18n@kde.nl>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.6\n"

#: klineal.cpp:78
msgid ""
"This is a tool to measure pixel distances and colors on the screen. It is "
"useful for working on layouts of dialogs, web pages etc."
msgstr ""
"Dit is een hulpmiddel om de afstanden tussen de pixels en kleuren op het "
"scherm te meten. Het is vooral bruikbaar voor het werken aan de opmaak van "
"dialoogvensters, webpagina's etc."

#: klineal.cpp:111
msgid "This is the current distance measured in pixels"
msgstr "Dit is de huidige afstand, gemeten in pixels"

#: klineal.cpp:120
msgid ""
"This is the current color in hexadecimal rgb representation as you may use "
"it in HTML or as a QColor name. The rectangles background shows the color of "
"the pixel inside the little square at the end of the line cursor."
msgstr ""
"Dit is de huidige kleur in hexadecimale rgb-prestentaties, zoals u mogelijk "
"gebruikt in html of als een QColor-naam. De rechthoekige achtergrond toont "
"de kleur van de pixel binnen het kleine vierkant aan het einde van de "
"regelcursor."

#: klineal.cpp:131
msgid "K-Ruler"
msgstr "K-Ruler"

#: klineal.cpp:133
msgid "North"
msgstr "Noord"

#: klineal.cpp:134
msgid "East"
msgstr "Oost"

#: klineal.cpp:135
msgid "South"
msgstr "Zuid"

#: klineal.cpp:136
msgid "West"
msgstr "West"

#: klineal.cpp:137
msgid "Turn Right"
msgstr "Naar rechts draaien"

#: klineal.cpp:138
msgid "Turn Left"
msgstr "Naar links draaien"

#: klineal.cpp:141
msgid "Short"
msgstr "Klein"

#: klineal.cpp:142
msgid "Medium"
msgstr "Middel"

#: klineal.cpp:143
msgid "Tall"
msgstr "Groot"

#: klineal.cpp:144
msgid "Full Screen Width"
msgstr "Volledig-scherm-breedte"

#: klineal.cpp:145
msgid "Length"
msgstr "Lengte"

#: klineal.cpp:146
msgid "Choose Color..."
msgstr "Kleur kiezen..."

#: main.cpp:31
msgid "KDE Screen Ruler"
msgstr "KDE Scherm Liniaal"

#: main.cpp:33
msgid "A screen ruler for the K Desktop Environment."
msgstr "Een schermliniaal voor de K Desktop Environment."

#: main.cpp:39
msgid "Programming"
msgstr "Programmering"

#: main.cpp:40
msgid "Initial port to KDE 2"
msgstr "Initiële overdracht naar KDE 2"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Rinse de Vries"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "rinse@kde.nl"

#~ msgid "Orientation"
#~ msgstr "Oriëntatie"
