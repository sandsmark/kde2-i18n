# KTranslator Generated File
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Otto Bruggeman <otto.bruggeman@home.nl>
# Gelezen, Rinse
msgid ""
msgstr ""
"Project-Id-Version: 2.0\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-01-10 00:29GMT\n"
"Last-Translator: Rinse de Vries <Rinse@kde.nl>\n"
"Language-Team: Nederlands <i18n@kde.nl>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.6\n"

#: config.cpp:46
msgid "KPager Settings"
msgstr "KPager - instellingen"

#: config.cpp:49
msgid "Enable Window Dragging"
msgstr "Vensterverslepen inschakelen"

#: config.cpp:57
msgid "Show Name"
msgstr "Naam tonen"

#: config.cpp:59
msgid "Show Number"
msgstr "Nummer tonen"

#: config.cpp:61
msgid "Show Background"
msgstr "Achtergrond tonen"

#: config.cpp:63
msgid "Show Windows"
msgstr "Vensters tonen"

#: config.cpp:66
msgid "Type of Window"
msgstr "Type venster"

#: config.cpp:71
msgid "Plain"
msgstr "Egaal"

#: config.cpp:72
msgid "Icon"
msgstr "Pictogram"

#: config.cpp:74
msgid "Pixmap"
msgstr "Pixmap"

#: config.cpp:84
msgid "Classical"
msgstr "Klassiek"

#: kpager.cpp:274 kpager.cpp:307
msgid "Configure Pager..."
msgstr "Pager instellen..."

#: kpager.cpp:295
msgid "Mi&nimize"
msgstr "Mi&nimaliseren"

#: kpager.cpp:296
msgid "Ma&ximize"
msgstr "Ma&ximaliseren"

#: kpager.cpp:301
msgid "&To desktop"
msgstr "&Naar bureaublad"

#: kpager.cpp:592
msgid "&All desktops"
msgstr "&Alle bureaubladen"

#: main.cpp:36
msgid "Create pager as a child of specified parent window"
msgstr "Pager aanmaken als dochter van een opgegeven vadervenster."

#: main.cpp:87
msgid "Desktop Pager"
msgstr "Bureaubladpager"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Otto Bruggeman"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "otto.bruggeman@home.nl"

#~ msgid "Horizontal"
#~ msgstr "Horizontaal"

#~ msgid "Vertical"
#~ msgstr "Verticaal"
