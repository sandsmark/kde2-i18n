# Norwegian (Nynorsk) KDE translation
# Copyright (C) 2001 Gaute Hvoslef Kvalnes.
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 1999-2001
#
msgid ""
msgstr ""
"Project-Id-Version: kdeutils/ktimer\n"
"POT-Creation-Date: 2001-06-10 17:32+0200\n"
"PO-Revision-Date: 2001-07-19 22:11+GMT+0200\n"
"Last-Translator: Gaute Hvoslef Kvalnes <gaute@verdsveven.com>\n"
"Language-Team: Norwegian (Nynorsk)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: main.cpp:28
msgid "KDE Timer"
msgstr "KDE Tidsur"

#: main.cpp:34
msgid "KTimer"
msgstr "KTimer"

#: rc.cpp:1
msgid "Timer Settings"
msgstr "Tidsurinnstillingar"

#: rc.cpp:2
msgid "Counter [s]"
msgstr "Teljar [s}"

#: rc.cpp:3
msgid "Delay [s]"
msgstr "Forseinking [s]"

#: rc.cpp:4 rc.cpp:15
msgid "State"
msgstr "Tilstand"

#: rc.cpp:5
msgid "Command"
msgstr "Kommando"

#: rc.cpp:6
msgid "&New"
msgstr "&Ny"

#: rc.cpp:8
msgid "Settings"
msgstr "Innstillingar"

#: rc.cpp:9
msgid "&Loop"
msgstr "&Løkkje"

#: rc.cpp:10
msgid "..."
msgstr " ..."

#: rc.cpp:11
msgid "Delay:"
msgstr "Forseinking:"

#: rc.cpp:12
msgid "Start only &one instance"
msgstr "Start berre &ein instans"

#: rc.cpp:13
msgid "Seconds"
msgstr "Sekund"

#: rc.cpp:14
msgid "Command line:"
msgstr "Kommandolinje:"

#: rc.cpp:16
msgid "||"
msgstr "||"

#: rc.cpp:17
msgid ">"
msgstr ">"

#: rc.cpp:18
msgid "="
msgstr "="

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Gaute Hvoslef Kvalnes"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "gaute@verdsveven.com"
