# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-08-22 19:00+0200\n"
"PO-Revision-Date: 2001-08-30 23:27CET\n"
"Last-Translator: Pablo de Vicente <pvicentea@nexo.es>\n"
"Language-Team: español <kde-es@kyb.uni-stuttgart.de>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: plugin_kateinsertcommand.cpp:92
msgid "Insert Command..."
msgstr "Insertar comando..."

#: plugin_kateinsertcommand.cpp:104
msgid "Sorry, a process is currently beeing executed:("
msgstr "Lo siento, se está ejecutando un proceso actualmente:("

#: plugin_kateinsertcommand.cpp:104 plugin_kateinsertcommand.cpp:186
#: plugin_kateinsertcommand.cpp:224
msgid "Oops!"
msgstr "¡Ups!"

#: plugin_kateinsertcommand.cpp:186
msgid "Could not kill command :o(("
msgstr "Imposible matar el comando :o(("

#: plugin_kateinsertcommand.cpp:192
msgid "Executing command:"
msgstr "Ejecutando comando:"

#: plugin_kateinsertcommand.cpp:192
msgid "Press Cancel to abort"
msgstr "Pulsar cancelar para abortar"

#: plugin_kateinsertcommand.cpp:224
#, c-format
msgid "Command exited with status %1"
msgstr "El comando finalizó con el estado %1"

#: plugin_kateinsertcommand.cpp:260
msgid "Insert Command"
msgstr "Insertar comando"

#: plugin_kateinsertcommand.cpp:267
msgid "Enter &Command"
msgstr "Introducir &comando"

#: plugin_kateinsertcommand.cpp:277
msgid "Choose &Working Directory"
msgstr "Elija el directorio de &trabajo"

#: plugin_kateinsertcommand.cpp:287
msgid "Insert Std&Err messages"
msgstr "Insertar mensajes Std&Err"

#: plugin_kateinsertcommand.cpp:290
msgid "&Print Command Name"
msgstr "&Imprimir nombre de comando"

#: plugin_kateinsertcommand.cpp:294
msgid ""
"Enter the shell command which' output you want inserted into your document. "
"Feel free to use a pipe or two if you're in that mood;-)"
msgstr ""
"Introduzca el comando del intérprete cuya salida desea insertar en su "
"documento. Sientase libre de usar una tubería o dos si está dispuesto a "
"ello;-)"

#: plugin_kateinsertcommand.cpp:295
msgid ""
"Sets the working directory of command. The command executed is 'cd <dir> && "
"<command>'"
msgstr ""
"Configura el directorio de trabajo de comando. El comando ejecutado es 'cd "
"<dir> && <command>'"

#: plugin_kateinsertcommand.cpp:296
msgid ""
"Check this if you want error output from <command> inserted too.\n"
"Some commands like locate prints everything to STDERR"
msgstr ""
"Seleccione esta opción si desea insertar la salida de error del <commando> "
"\n"
"Algunos comandos necesitan imprimir sobre STDERR"

#: plugin_kateinsertcommand.cpp:297
msgid ""
"If you check this I will print the command string followed by a newline "
"before the output."
msgstr ""
"Si selecciona esta opción se imprimirá una cadena de comando seguida de "
"una nueva línea antes de la salida."

#: plugin_kateinsertcommand.cpp:336
msgid "Remember"
msgstr "Recordar"

#: plugin_kateinsertcommand.cpp:338
msgid "&Commands"
msgstr "&Comandos"

#: plugin_kateinsertcommand.cpp:345
msgid "Start In"
msgstr "Iniciar"

#: plugin_kateinsertcommand.cpp:347
msgid "Application &Working Directory"
msgstr "Directorio de &trabajo de aplicación"

#: plugin_kateinsertcommand.cpp:348
msgid "&Document Directory"
msgstr "Directorio de &documento"

#: plugin_kateinsertcommand.cpp:349
msgid "&Latest Used Working Directory"
msgstr "Directorio de trabajo utilizado por ú&ltima vez"

#: plugin_kateinsertcommand.cpp:356
msgid ""
"Sets the number of commands to remember. The command history is saved over "
"sessions."
msgstr ""
"Configura el número de comandos a recordar. El historial de comandos se "
"guarda a lo largo de diferentes sesiones."

#: plugin_kateinsertcommand.cpp:357
msgid ""
"<qt><p>Decides what is suggested as <em>working directory</em> for the "
"command.</p><p><strong>Application Working Directory (default):</strong>"
" The directory from which you launched the application hosting the plugin, "
"usually your home directory.</p><p><strong>Document Directory:</strong> The "
"directory of the document. Used only for local documents.<p><strong>Latest "
"Working Directory:</strong> The directory used last time you used this "
"plugin.</p></qt>"
msgstr ""
"<qt><p>Decide lo sugerido como <em>directorio de trabajo</em> por el "
"comando.</p><p><strong>Directorio de trabajo de aplicación (por "
"omisión):</strong> El directorio desde el que lanzó la aplicación que "
"aloja el plugin, habitualmente su directorio "
"personal.</p><p><strong>Directorio de documento:</strong> El directorio del "
"documento. Usado sólo para documentos locales.<p><strong>Ultimo directorio "
"de trabajo:</strong> El directorio usada la última vez que usó este "
"plugin.</p></qt>"
