# kformula
# Copyright (C) 2001 Free Software Foundation, Inc.
# Mario Teijeiro Otero <mteijeiro@escomposlinux.org>, 2001.
# Juan Manuel García Molina <juanmagm@mail.com>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: KOffice 1.1\n"
"POT-Creation-Date: 2001-05-18 02:08+0200\n"
"PO-Revision-Date: 2001-07-24 20:30GMT\n"
"Last-Translator: Juan Manuel García Molina <juanmagm@mail.com>\n"
"Language-Team: Spanish <kde-es@kyb.uni-stuttgart.de>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: rc.cpp:2
msgid "E&lement"
msgstr "E&lemento"

#: rc.cpp:3
msgid "Add..."
msgstr "Añadir..."

#: rc.cpp:4
msgid "Vertical align"
msgstr "Alineación vertical"

#: rc.cpp:5
msgid "Horizontal align"
msgstr "Alineación horizontal"

#: rc.cpp:6
msgid "Fraction vertical distance"
msgstr "Distancia vertical de la división"

#: rc.cpp:7
msgid "Matrix"
msgstr "Matriz"

#: rc.cpp:9
msgid "Increase/Decrease options.."
msgstr "opciones de incrementar/decrementar..."

#: rc.cpp:14
msgid "Element"
msgstr "Elemento"

#: rc.cpp:15
msgid "Symbol"
msgstr "Símbolo"

#: main.cc:30
msgid "File To Open"
msgstr "Archivo a abrir"

#: matrixwidget.cc:34
msgid "KFormula - Matrix Element Setup"
msgstr "KFormula - Configuración de los elementos de la matriz"

#: matrixwidget.cc:37
msgid "General"
msgstr "General"

#: matrixwidget.cc:46
msgid "Size & Space:"
msgstr "Tamaño y espacio:"

#: matrixwidget.cc:51
msgid "Columns:"
msgstr "Columnas:"

#: matrixwidget.cc:58
msgid "Rows"
msgstr "Filas"

#: matrixwidget.cc:68
msgid "Space:"
msgstr "Espacio:"

#: matrixwidget.cc:81
msgid "Borders:"
msgstr "Bordes:"

#: matrixwidget.cc:84 matrixwidget.cc:90 matrixwidget.cc:96
#: matrixwidget.cc:102 matrixwidget.cc:108 matrixwidget.cc:114
msgid "No Border"
msgstr "Sin bordes"

#: matrixwidget.cc:85 matrixwidget.cc:91 matrixwidget.cc:97
#: matrixwidget.cc:103 matrixwidget.cc:109 matrixwidget.cc:115
msgid "Single Line"
msgstr "Línea sencilla"

#: matrixwidget.cc:86 matrixwidget.cc:92 matrixwidget.cc:98
#: matrixwidget.cc:104 matrixwidget.cc:110 matrixwidget.cc:116
msgid "Double Line"
msgstr "Línea doble"

#: matrixwidget.cc:121
msgid "Internal Horizontal"
msgstr "Horizontal interior"

#: matrixwidget.cc:125
msgid "Internal Vertical"
msgstr "Vertical interior"

#: matrixwidget.cc:156
msgid "Vertical:"
msgstr "Vertical:"

#: matrixwidget.cc:162
msgid "Fixed Row"
msgstr "Fila fija"

#: matrixwidget.cc:170
msgid "Half Matrix"
msgstr "Media matriz"

#: matrixwidget.cc:179
msgid "Row Midline"
msgstr "Linea del medio de la fila."

#: matrixwidget.cc:182
msgid "Over the row"
msgstr "Sobre la fila"

#: matrixwidget.cc:185
msgid "Under the row"
msgstr "Bajo la fila"

#: matrixwidget.cc:196
msgid "Horizontal:"
msgstr "Horizontal:"

#: matrixwidget.cc:199
msgid "Center "
msgstr "Centro "

#: matrixwidget.cc:200
msgid "Left side"
msgstr "Lado izquierdo"

#: matrixwidget.cc:201
msgid "Right side"
msgstr "Lado derecho"

#: kformula_aboutdata.h:27
msgid "KOffice Formula Editor"
msgstr "KOffice- Editor de fórmulas"

#: kformula_aboutdata.h:32
msgid "KFormula"
msgstr "KFormula"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Mario Teijeiro OteroJuan Manuel García Molina"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "mteijeiro@escomposlinux.orgjuanmagm@mail.com"
