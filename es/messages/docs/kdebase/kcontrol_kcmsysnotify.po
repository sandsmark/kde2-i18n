# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-02-09 01:25+0100\n"
"PO-Revision-Date: 2001-08-25 08:21CET\n"
"Last-Translator: Pablo de Vicente <pvicentea@nexo.es>\n"
"Language-Team: español <kde-es@kyb.uni-stuttgart.de>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Frst-Translator: Pablo de Vicente <pvicentea@nexo.es>\n"
"X-Generator: KBabel 0.8\n"

#: index.docbook:5
msgid "<firstname>Mike</firstname> <surname>McBride</surname>"
msgstr "<firstname>Mike</firstname> <surname>McBride</surname>"

#: index.docbook:9
msgid "ROLES_OF_TRANSLATORS"
msgstr ""
"<othercredit role=\"translator\"> <firstname>Pablo</firstname> <surname>de "
"Vicente</surname> "
"<affiliation><address><email>pvicentea@nexo.es</email></address>"
"</affiliation> <contrib>Traductor</contrib> </othercredit>"

#: index.docbook:16
msgid "<keyword>KDE</keyword>"
msgstr "<keyword>KDE</keyword>"

#: index.docbook:17
msgid "KControl"
msgstr "KControl"

#: index.docbook:18
msgid "system notification"
msgstr "notificación del sistema"

#: index.docbook:19
msgid "notification"
msgstr "notificación"

#: index.docbook:23
msgid "System Notification"
msgstr "Notificación del sistema"

#: index.docbook:25
msgid ""
"&kde;, like all applications, needs to inform the user when a problem "
"occurs, a task is completed, or something has happened. &kde; uses a set of "
"<quote>System Notifications</quote> to keep the user informed on what is "
"happening."
msgstr ""
"&kde;, como todas las aplicaciones, necesita informar al usuario cuando se "
"produce un problema, una tarea se completa, o algo ocurre. &kde; utiliza un "
"conjunto de <quote>Notificaciones del sistema</quote> para mantener "
"informado al usuario de lo que está ocurriendo."

#: index.docbook:30
msgid ""
"Using this module, you can determin what &kde; does to communicate each "
"event."
msgstr ""
"Usando este módulo, puede determinar lo que hace &kde; para comunicar tal "
"evento."

#: index.docbook:33
msgid ""
"The panel consists of a large list of specific events which need to be "
"communicated to the user. This list is organized into a tree, so that you "
"can rapidly find the notification you are looking for."
msgstr ""
"El panel consiste en una gran lista de eventos específicos que necesitan "
"ser comunicados al usuario. Esta lista está organizada en árbol, de modo "
"que usted pueda encontrar rápidamente la notificación que está buscando."

#: index.docbook:37
msgid ""
"To configure a notification, simply click on a group, which will open up a "
"subgroup. You can click on subgroups, which may lead to more subgroups, or "
"it may lead to a list of notifications."
msgstr ""
"Para configurar una notificación, simplemente pulse sobre un grupo, y se le "
"abrirá un subgrupo. Puede pulsar sobre subgrupos, lo que puede llevarle a "
"más subgrupos, o una lista de notificaciones."

#: index.docbook:41
msgid ""
"Once you have found the notification you are looking for, double-click on "
"the notification."
msgstr ""
"Una vez haya encontrado la notificación que busca, pulse dos veces sobre "
"ella."

#: index.docbook:44
msgid "You will be presented with 4 options:"
msgstr "Se le presentarán 4 opciones:"

#: index.docbook:46
msgid "Log to file"
msgstr "Registrar en archivo"

#: index.docbook:46
msgid ""
"This will tell &kde; to add the notification to the end of a file. Once you "
"place a mark in front of this option, you can enter a filename at the bottom "
"of the module. If you click on the <guiicon>folder</guiicon>"
" to the right of the blank, you can browse through your filesystem and "
"select the file you want."
msgstr ""
"Esto le dirá a &kde; que añada la notificación al final del archivo. Una "
"vez que haya puesto una marca en frente de esta opción, puede introducir un "
"nombre de archivo al final del módulo. Si pulsa sobre "
"<guiicon>carpeta</guiicon> a la derecha del espacio en blanco, podrá "
"navegar a través de su sistema de archivos y seleccionar el archivo que "
"desee."

#: index.docbook:46
msgid "Play sound"
msgstr "Reproducir sonido"

#: index.docbook:46
msgid ""
"When this notification is activated, &kde; will play a sound. Once you place "
"a mark in front ofthis option, you can enter a filename at the bottom of the "
"module."
msgstr ""
"Cuando se activa esta notificación &kde; reproducirá un sonido. Una vez "
"colocada una marca frente a esta opción, puede introducir un nombre de "
"archivo al final del módulo."

#: index.docbook:46
msgid ""
"If you click on the <guiicon>folder</guiicon>"
" to the right of the blank, you can browse through your filesystem and "
"select the file you want."
msgstr ""
"Si pulsa sobre <guiicon>carpeta</guiicon> a la derecha del espacio en "
"blanco, podrá navegar a través de su sistema de archivos y seleccionar el "
"archivo que desee."

#: index.docbook:46
msgid ""
"By clicking the arrow button to the right of the <guiicon>folder</guiicon> "
"button, you can hear the sound."
msgstr ""
"Pulsando sobre el botón de la flecha a la derecha del botón "
"<guiicon>carpeta</guiicon> podrá oir el sonido."

#: index.docbook:46
msgid ""
"Currently, you can only play <literal role=\"extension\">.wav</literal> "
"files. This will likely change in future releases."
msgstr ""
"En este momento, sólo puede reproducir archivos <literal "
"role=\"extension\">.wav</literal>. Esto cambiará en versiones futuras."

#: index.docbook:46
msgid ""
"Maybe you want to use a special media player to play sound files, &eg; "
"because you use sound files in a special format or you don't use the "
"<application>aRts</application> sound daemon. In that case, check the "
"<guilabel>Use external player</guilabel> option and enter the full path and "
"name of the program you want to use into the text field."
msgstr ""
"Quizá quiera utilizar un reproductor especial de medios para reproducir "
"archivos de sonido, &eg; porque utilice archivos de sonido en un formato "
"especial o no utilice el demonio de sonido <application>aRts</application>. "
"En dicho caso, seleccione la opción <guilabel>Usar reproductor "
"externo</guilabel> e introduzca la ruta completa y el nombre del programa "
"que desea usar en el campo de texto."

#: index.docbook:46
msgid "Show message box"
msgstr "Mostrar cuadro de mensaje"

#: index.docbook:46
msgid ""
"When this notification is activated, a message box appears in the middle of "
"the screen to inform the user of the message.."
msgstr ""
"Cuando esta notificación está activada, un cuadro de mensaje aparecerá en "
"medio de la pantalla para informar al usuario de mensaje."

#: index.docbook:46
msgid "Standard error output"
msgstr "Salida error estándar"

#: index.docbook:46
msgid ""
"When this notification is activated, the message is sent to the standard "
"output."
msgstr ""
"Cuando esta notificación está activada, el mensaje se envia a la salida "
"estándar."

#: index.docbook:87
msgid ""
"You are not limited to choosing one option, you can use any combination of "
"these four options for each notification."
msgstr ""
"Usted no se limita a elegir una opción, puede utilizar cualquier "
"combinación de estas cuatro opciones para cada notificación."

#: index.docbook:91
msgid "Section Author"
msgstr "Sección de autores"

#: index.docbook:93
msgid "This section written by Mike McBride <email>mpmcbride7@yahoo.com</email>"
msgstr "Sección escrita por Mike McBride <email>mpmcbride7@yahoo.com</email>"

#: index.docbook:95
msgid "CREDIT_FOR_TRANSLATORS"
msgstr "<para>Traducción por Pablo de Vicente<email>pvicentea@nexo.es</email></para>"
