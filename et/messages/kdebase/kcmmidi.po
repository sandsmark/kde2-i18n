# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 02:05+0200\n"
"PO-Revision-Date: 2000-09-23 13:11+0200\n"
"Last-Translator: Hasso Tepper <hasso@estpak.ee>\n"
"Language-Team: Estonian <linux-ee@eenet.ee>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.5.4\n"

#: midi.cpp:42
msgid "Select the midi device you want to use :"
msgstr "MIDI seade, mida soovid kasutada:"

#: midi.cpp:60
msgid "Use Midi Mapper"
msgstr "Kasutatakse MIDI mäpperit"
