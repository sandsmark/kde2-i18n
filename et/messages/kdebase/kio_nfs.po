# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-06-27 03:09+0200\n"
"Last-Translator: Hasso Tepper <hasso@estpak.ee>\n"
"Language-Team: Estonian <linux-ee@eenet.ee>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2\n"

#: kio_nfs.cpp:986
msgid "An RPC error occurred."
msgstr "Ilmnes RPC viga."

#: kio_nfs.cpp:1030
msgid "No space left on device"
msgstr "Seadmel pole enam vaba ruumi"

#: kio_nfs.cpp:1033
msgid "Read only file system"
msgstr "Failisüsteem on ainult lugemiseks"

#: kio_nfs.cpp:1036
msgid "Filename too long"
msgstr "Faili nimi on liiga pikk"

#: kio_nfs.cpp:1043
msgid "Disk quota exceeded"
msgstr "Ketta piirang ületatud"
