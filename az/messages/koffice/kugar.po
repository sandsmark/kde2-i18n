# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kugar\n"
"POT-Creation-Date: 2001-03-27 01:38+0200\n"
"PO-Revision-Date: 2001-05-08 18:07GMT+0200\n"
"Last-Translator: Vasif İsmayıloğlu MD <azerb_linux@hotmailşcom>\n"
"Language-Team: Azerbaijani Turkish <linuxaz@azerimail.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: lib/mreportviewer.cpp:146
msgid ""
"There are no pages in the\n"
"report to print."
msgstr ""
"Raportda çap ediləcək səhifə\n"
"yoxdur."

#: lib/mreportviewer.cpp:184
msgid "Printing report..."
msgstr "Çap etmə raportu..."

#: lib/mreportviewer.cpp:299
msgid "Creating report..."
msgstr "Raport yaradılır..."

#: part/kugar_part.cpp:82 shell/main.cpp:21
msgid "Kugar"
msgstr "Kugar"

#: part/kugar_part.cpp:145 shell/report.cpp:78
#, c-format
msgid "Invalid data file: %1"
msgstr "Hökmsüz verilən faylı: %1"

#: part/kugar_part.cpp:150 shell/report.cpp:83
#, c-format
msgid "Unable to open data file: %1"
msgstr "Verilən faylı açıla bilmir:%1"

#: part/kugar_part.cpp:195 shell/report.cpp:107
#, c-format
msgid "Unable to download template file: %1"
msgstr "Nümunə faylı endirilə bilmir: %1"

#: part/kugar_part.cpp:205 shell/report.cpp:117
#, c-format
msgid "Invalid template file: %1"
msgstr "Hökmsüz nümunə faylı: %1 "

#: part/kugar_part.cpp:210 shell/report.cpp:122
#, c-format
msgid "Unable to open template file: %1"
msgstr "Nümunə faylı açıla bilmir: %1"

#: shell/main.cpp:22
msgid "A template driven report viewer for XML data."
msgstr "XML verilən nümayişçisi."

#: shell/main.cpp:28
msgid ""
"Kugar merges XML data files with XML templates\n"
"to display and print high quality reports."
msgstr ""
"Kuqar, XML verilən fayllarını XML nümunələri\n"
"ilə birləşdirərək yüksək keyfiyyətli günlük raportlar yazır."

#: shell/main.cpp:35
msgid "The XML data file."
msgstr "XML verilən faylı."

#: shell/main.cpp:36
msgid "The XML template file URL."
msgstr "XML nümunə faylı URLsi."
