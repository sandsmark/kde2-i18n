# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-08-22 18:51+0200\n"
"PO-Revision-Date: 2001-06-12 17:54GMT+0200\n"
"Last-Translator: Vasif İsmayıloğlu <azerb_linux@hotmail.com>\n"
"Language-Team: Azerbaijani Turkish <linuxaz@azerimail.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: kio_lan.cpp:188 kio_lan.cpp:206 kio_lan.cpp:385
#, c-format
msgid "Received unexpected data from %1"
msgstr "%1 gözlənilməz verilən göndərdi"

#: kio_lan.cpp:642
msgid "No hosts allowed in rlan:/ url"
msgstr "rlan üçün heç qovşaq icazəsi yoxdur:/ url"

#~ msgid ""
#~ "\n"
#~ "Your LAN Browsing is not yet configured.\n"
#~ "Go to the KDE Control Center and open Network->LAN Browsing\n"
#~ "and configure the settings you will find there."
#~ msgstr ""
#~ "\n"
#~ "Sizin LAN Səyahətiniz hələ ki quraşdırılmayıbdır.\n"
#~ "KDE İdarə Mərkəzinə gedərək Şəbəkə->LAN Səyahətini\n"
#~ "seçin və lazımi sahələri dolduraraq quraşdırın."
