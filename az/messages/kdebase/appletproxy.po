# SOME DESCRIPTIVE TITLE.
# Vasif Ismailoglu MD <azerb_linux@hotmail.com>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: AppletProxy\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-04-13 23:37GMT\n"
"Last-Translator: \n"
"Language-Team:  <de@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: appletproxy.cpp:52
msgid "The applets desktop file."
msgstr "Proqramlar masa üstü faylı."

#: appletproxy.cpp:53
msgid "The config file to be used."
msgstr "Quraşdırma faylının işlədilməsi lazımdır."

#: appletproxy.cpp:54
msgid "DCOP callback id of the applet container."
msgstr "Proqram tə'minatı daşıyıcısının DCOP geri təpmə kimliyi."

#: appletproxy.cpp:60 appletproxy.cpp:62
msgid "Panel applet proxy."
msgstr "Panel proqram vəkili."

#: appletproxy.cpp:84
msgid "No desktop file specified"
msgstr "Heç masa üstü faylı seçilməyib"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Vasif Ismailoglu MD"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "azerb_linux@hotmail.com"
