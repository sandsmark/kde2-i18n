# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Vasif Ismailoglu MD <azerb_linux@hotmail.com>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: taskbarextension\n"
"POT-Creation-Date: 2001-04-30 01:36+0200\n"
"PO-Revision-Date: 2001-05-08 20:37GMT+0200\n"
"Last-Translator: Vasif İsmayıloğlu MD <azerb_linux@hotmailşcom>\n"
"Language-Team: Azerbaijani Turkish <linuxaz@azerimail.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: taskbarextension.cpp:510
msgid "modified"
msgstr "təkmilləşdirilmiş"

#: taskbarextension.cpp:586
msgid "&All Desktops"
msgstr "&Bütün Masa Üstləri"

#: taskbarextension.cpp:657
msgid "Mi&nimize"
msgstr "&Balacalaşdır"

#: taskbarextension.cpp:658
msgid "Ma&ximize"
msgstr "Bö&yüt"

#: taskbarextension.cpp:659
msgid "&Restore"
msgstr "Bər&pa Et"

#: taskbarextension.cpp:663
msgid "&Shade"
msgstr "&Kölgələ"

#: taskbarextension.cpp:664
msgid "&Always On Top"
msgstr "&Həmişə Üstdə"

#: taskbarextension.cpp:672
msgid "To &Desktop"
msgstr "Masa &Üstünə"

#: taskbarextension.cpp:673
msgid "&To Current Desktop"
msgstr "Ha&zırkı Masa Üstünə"

#: taskbarextension.cpp:688
#, c-format
msgid "Starting %1"
msgstr "%1 Başladılır"
