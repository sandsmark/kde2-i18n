# Vasif Ismailoglu MD <azerb_linux@hotmail.com>, 2001.
#
# Kcmcrypto.
msgid ""
msgstr ""
"Project-Id-Version: Kcmcrytpo\n"
"POT-Creation-Date: 2001-07-05 13:30+0200\n"
"PO-Revision-Date: 2001-07-26 17:38GMT+0200\n"
"Last-Translator: Vasif İsmayıloğlu <azerb_linux@hotmail.com>\n"
"Language-Team: Azerbaijani Turkish <linuxaz@azerimail.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: crypto.cpp:94
msgid "%1 (%2 of %3 bits)"
msgstr "%1 (%2 bitdən %3 'ü)"

#: crypto.cpp:201
msgid "Enable &TLS support if supported by the server."
msgstr "Vericinin dəstəklədiyi &TLS dəstəyini aç."

#: crypto.cpp:204
msgid ""
"TLS is the newest revision of the SSL protocol.  It integrates better with "
"other protocols and has replaced SSL in protocols such as POP3 and SMTP."
msgstr ""
"TSl SSL protokolunun yeni buraxılışıdır. Bu protokol POP3 və SMTP kimi "
"protokollarda daha yaxşı qaynaşma yaratdığı üçün çox dəbdədir."

#: crypto.cpp:209
msgid "Enable SSLv&2"
msgstr "SSLv&2'ni fəallaşdır"

#: crypto.cpp:212
msgid ""
"SSL v2 is the second revision of the SSL protocol. It is most common to "
"enable v2 and v3."
msgstr ""
"SSLv2 SSL protokolunun ikinci buraxılışıdır. Bir çox halda V2 və V3 "
"fəallaşdırılır."

#: crypto.cpp:216
msgid "Enable SSLv&3"
msgstr "SSLv&3'ü fəallaşdır"

#: crypto.cpp:219
msgid ""
"SSL v3 is the third revision of the SSL protocol. It is most common to "
"enable v2 and v3."
msgstr ""
"SSLv3 SSL protokolunun üçüncü buraxılışıdır. Bir çox halda V2 və "
"V3 fəallaşdırılır."

#: crypto.cpp:225
msgid "SSLv2 Ciphers To Use:"
msgstr "İşlədiləcək SSLv2 Şifrləməsi"

#: crypto.cpp:226
msgid ""
"Select the ciphers you wish to enable when using the SSL v2 protocol.  The "
"actual protocol used will be negotiated with the server at connection time."
msgstr ""
"SSL v2 protokolunu işlədirkən fəallaşdırılacaq şifrləmələri "
"seçiniz. İşlədiləcək həqiqi protokol bağlantı anında verici ilə "
"tapılacaqdır"

#: crypto.cpp:236
msgid ""
"SSL ciphers cannot be configured because this module was not linked with "
"OpenSSL."
msgstr ""
"SSL şifrləmələri quraşdırıla bilmir, çünkü bu modul OpenSSL ilə "
"bağlanmamışdır"

#: crypto.cpp:251
msgid "SSLv3 Ciphers To Use:"
msgstr "İşlədiləcək SSLv3 şifrləməsi"

#: crypto.cpp:252
msgid ""
"Select the ciphers you wish to enable when using the SSL v3 protocol.  The "
"actual protocol used will be negotiated with the server at connection time."
msgstr ""
"SSL v3 protokolunu işlədirkən fəallaşdırılacaq şifrləmələri "
"seçiniz. İşlədiləcək həqiqi protokol bağlantı anında verici ilə "
"tapılacaqdır"

#: crypto.cpp:266
msgid "Cipher Wizards..."
msgstr "Şifr Sehibazı..."

#: crypto.cpp:267
msgid "Most &Compatible"
msgstr "&Ən Uyğun"

#: crypto.cpp:268
msgid "&US Ciphers Only"
msgstr "Sadəcə olaraq &US Şifrləri"

#: crypto.cpp:269
msgid "E&xport Ciphers Only"
msgstr "&Sadəcə olaraq Şifrləri Al"

#: crypto.cpp:270
msgid "Enable &All"
msgstr "&Hamısını fəallaşdır"

#: crypto.cpp:276
msgid "Use these buttons to more easily configure the SSL encryption settings."
msgstr ""
"SSL şifrləmə qurğularını daha asand etmək üçün bu düymələri "
"işlədin."

#: crypto.cpp:278
msgid "Select the settings found to be most compatible."
msgstr "Ən uyğun tapılan qurğuları seç."

#: crypto.cpp:280
msgid "Select only the US strong (>= 128 bit) encryption ciphers."
msgstr "Güclü şifrləməni (>= 128 bit) seç."

#: crypto.cpp:282
msgid "Select only the weak ciphers (<= 56 bit)."
msgstr "Sadəcə olaraq zəif şifrləməni seç (<= 56 bit)."

#: crypto.cpp:284
msgid "Select all SSL ciphers and methods."
msgstr "Bütün SSL şifrləməni və metodları seç"

#: crypto.cpp:291
msgid "Use EGD"
msgstr "EGD işlət"

#: crypto.cpp:297
msgid "Path to EGD:"
msgstr "EGD cığırı:"

#: crypto.cpp:306
msgid ""
"If selected, OpenSSL will be asked to use the entropy gathering daemon (EGD) "
"for initializing the pseudo-random number generator."
msgstr ""
"Seçili olduğu vaxt OpenSSL yalancı rəqəm yaradıcısını başlatmaq "
"üçün entropi axtarış vasitəsini (EGD) açmaq istəyəcəkdir."

#: crypto.cpp:309
msgid "Enter the path to the socket created by the entropy gathering daemon here."
msgstr ""
"Entropi axtarış vasitəsinin yaratdığı soketə gedan cığırı burya "
"girin."

#: crypto.cpp:313
msgid "Click here to browse for the EGD socket file."
msgstr "EGD soket faylını tapmaq üçün buraya tıqlayın."

#: crypto.cpp:321
msgid "Warn on &entering SSL mode"
msgstr "SSL moduna keçildiyində &xəbərdar et"

#: crypto.cpp:324
msgid "If selected, you will be notified when entering an SSL enabled site"
msgstr ""
"Əgər seçilmişsə SSL işlədən sayta girdiyinizdə xəbərdar "
"ediləcəksiniz"

#: crypto.cpp:328
msgid "Warn on &leaving SSL mode"
msgstr "SSL modundan &ayrılırkən xəbərdar et"

#: crypto.cpp:331
msgid "If selected, you will be notified when leaving an SSL based site."
msgstr ""
"Əgər seçili isə SSL dəstəkli saytdan ayrılırkən xəbərdar "
"ediləcəksiniz"

#: crypto.cpp:336
msgid "Warn on sending &unencrypted data"
msgstr "Şifrlənməmiş &mə'lumat yollarkən xəbərdar et"

#: crypto.cpp:339
msgid ""
"If selected, you will be notified before sending unencrypted data via a web "
"browser."
msgstr ""
"Əgər seçili isə veb səyyahınızdan şifrlənməmiş mə'lumat "
"yollanırkən sizi xəbərdar edər"

#: crypto.cpp:343
msgid "Warn on &mixed SSL/non-SSL pages"
msgstr "SSL/SSLsiz &qarışıq səhifələrdə xəbərdar et"

#: crypto.cpp:346
msgid ""
"If selected, you will be notified if you view a page that has both encrypted "
"and non-encrypted parts."
msgstr ""
"Əgər seçili isə həm şifrli hemde şifrlənməmiş qisimləri olan "
"səhifəni izlərkən sizi xəbərdar edər"

#: crypto.cpp:359
msgid "Enter the path to your OpenSSL shared libraries:"
msgstr "OpenSSL bölüşülmüş kitabxanalarının cığırını gir:"

#: crypto.cpp:363
msgid "..."
msgstr "..."

#: crypto.cpp:365
msgid "&Test..."
msgstr "&Sına..."

#: crypto.cpp:385
msgid ""
"This list box shows which certificates of yours KDE knows about.  You can "
"easily manage them from here."
msgstr ""
"Bu siyahı qutusu KDE səyyahınızın hansı sertifikalarınızı bildiyini "
"göstərir. Onları buradan idarə edə bilərsiniz"

#: crypto.cpp:389 crypto.cpp:542
msgid "Common Name"
msgstr "Ümumi Ad"

#: crypto.cpp:390
msgid "Email Address"
msgstr "Poçt Ünvanı"

#: crypto.cpp:393 crypto.cpp:650
msgid "&Import..."
msgstr "&Daxil et..."

#: crypto.cpp:397 crypto.cpp:545
msgid "&Export..."
msgstr "İ&xraç et...."

#: crypto.cpp:402 crypto.cpp:501
msgid "Remo&ve"
msgstr "Çıxa&rt"

#: crypto.cpp:407
msgid "&Unlock"
msgstr "&Aç"

#: crypto.cpp:412 crypto.cpp:662
msgid "Verif&y..."
msgstr "&Təsdiq et...."

#: crypto.cpp:417
msgid "&Change Password..."
msgstr "Parol &Dəyişdir..."

#: crypto.cpp:427 crypto.cpp:575
msgid "This is the information known about the owner of the certificate."
msgstr "Bu, vəsiqə yiyəsi haqqında mə'lumatdır."

#: crypto.cpp:429 crypto.cpp:577
msgid "This is the information known about the issuer of the certificate."
msgstr "Bu, vəsiqə verən haqqında mə'lumatdır."

#: crypto.cpp:432 crypto.cpp:580
msgid "Valid From:"
msgstr "E'tibarlılıq Başlanğıcı:"

#: crypto.cpp:433 crypto.cpp:581
msgid "Valid Until:"
msgstr "E'tibarlılıq Sonu:"

#: crypto.cpp:438 crypto.cpp:586
msgid "The certificate is valid starting at this date."
msgstr "Bu vəsiqə bu tarixdən e'tibarən e'tibarlıdır."

#: crypto.cpp:440 crypto.cpp:588
msgid "The certificate is valid until this date."
msgstr "Bu vəsiqə bu tarixə qədər e'tibarlıdır."

#: crypto.cpp:444
msgid "On SSL Connection..."
msgstr "SSL Bağlantıda ....."

#: crypto.cpp:445
msgid "&Use default certificate"
msgstr "&Əsas vəsiqəni işlət...."

#: crypto.cpp:446
msgid "&List upon connection"
msgstr "&Bağlantı anında sırala "

#: crypto.cpp:447
msgid "&Do not use certificates"
msgstr "&Vəsiqə işlətmə"

#: crypto.cpp:451 crypto.cpp:520 crypto.cpp:626 crypto.cpp:667
msgid ""
"SSL certificates cannot be managed because this module was not linked with "
"OpenSSL."
msgstr ""
"SSL vəsiqələri işlədilə bilməz ,çünkü bu modul Open SSL ilə "
"bağlanmamışdır"

#: crypto.cpp:466
msgid "Default Authentication Certificate"
msgstr "Əsas Kimlik Vəsiqəsi"

#: crypto.cpp:467
msgid "Default Action..."
msgstr "Əsas Gediş..."

#: crypto.cpp:468
msgid "&Send"
msgstr "&Göndər"

#: crypto.cpp:469 crypto.cpp:614
msgid "&Prompt"
msgstr "Sə&tir"

#: crypto.cpp:470
msgid "D&on't Send"
msgstr "Yolla&ma"

#: crypto.cpp:472
msgid "Default Certificate:"
msgstr "Əsas Vəsiqə:"

#: crypto.cpp:479
msgid "Host Authentication"
msgstr "Qovşaq Tanıdılması"

#: crypto.cpp:482
msgid "Host"
msgstr "Qovşaq"

#: crypto.cpp:483
msgid "Certificate"
msgstr "Vəsiqə"

#: crypto.cpp:484 crypto.cpp:611
msgid "Policy"
msgstr "Siyasət"

#: crypto.cpp:486
msgid "Host:"
msgstr "Qovşaq:"

#: crypto.cpp:487
msgid "Certificate:"
msgstr "Vəsiqə:"

#: crypto.cpp:494
msgid "Action..."
msgstr "Gediş..."

#: crypto.cpp:495
msgid "Send"
msgstr "Yolla"

#: crypto.cpp:496
msgid "Prompt"
msgstr "Soruş"

#: crypto.cpp:497
msgid "Don't Send"
msgstr "Yollama"

#: crypto.cpp:500
msgid "Ne&w"
msgstr "&Yeni"

#: crypto.cpp:538
msgid ""
"This list box shows which site and person certificates KDE knows about.  You "
"can easily manage them from here."
msgstr ""
"Bu siyahı qutusu KDE səyyahınızın hansı sayt və adam vəsiqələri "
"bildiyini göstərir. Onları buradan idarə edə bilərsiniz"

#: crypto.cpp:541
msgid "Organization"
msgstr "Şirkət"

#: crypto.cpp:548
msgid ""
"This button allows you to export the selected certificate to a file of "
"various formats."
msgstr "Bu düymə vəsiqəni seçdiyiniz fayl çəkillərinə çevirər."

#: crypto.cpp:552 crypto.cpp:658
msgid "&Remove..."
msgstr "Çı&xar..."

#: crypto.cpp:555
msgid "This button removes the selected certificate from the certificate cache."
msgstr "Bu düymə seçili vəsiqəni ara yaddaşdan silər."

#: crypto.cpp:559
msgid "&Verify..."
msgstr "&Təsdiq et...."

#: crypto.cpp:562
msgid "This button tests the selected certificate for validity."
msgstr "Bu düymə seçili vəsiqəni slnayar."

#: crypto.cpp:591
msgid "Cache..."
msgstr "Ara yaddaş..."

#: crypto.cpp:592
msgid "Permanentl&y"
msgstr "Həmişə&lik"

#: crypto.cpp:593
msgid "&Until..."
msgstr "&Son..."

#: crypto.cpp:604
msgid "Select here to make the cache entry permanent."
msgstr "Ara yaddaşı daimi etmək üçün buranı seçin."

#: crypto.cpp:606
msgid "Select here to make the cache entry temporary."
msgstr "Ara yaddaşı müvəqqətiləşdirmək üçün buranı seçin."

#: crypto.cpp:608
msgid "The date and time until the certificate cache entry should expire."
msgstr ""
"Vəsiqə ara yaddaş sahəsinin vaxtının dolmayacağı tarix və vaxtı "
"seçin."

#: crypto.cpp:612
msgid "A&ccept"
msgstr "Qə&bul Et"

#: crypto.cpp:613
msgid "Re&ject"
msgstr "Rə&dd Et"

#: crypto.cpp:618
msgid "Select this to always accept this certificate."
msgstr "Bu vəsiqəni həmişə qəbul etmək üçün buranı seçin."

#: crypto.cpp:620
msgid "Select this to always reject this certificate."
msgstr "Bu vəsiqəni həmişə rədd etmək üçün buranı seçin."

#: crypto.cpp:622
msgid ""
"Select this if you wish to be prompted for action when receiving this "
"certificate."
msgstr "Bu vəsiqəni aldığınız vaxt xəbərdar edilməniz üçün bunu seçin."

#: crypto.cpp:643
msgid ""
"This list box shows which certificate authorities KDE knows about.  You can "
"easily manage them from here."
msgstr ""
"Bu siyahı qutusu KDE səyyahınızın hansı vəsiqə təsdiqləyicilərini "
"bildiyini göstərir. Onları buradan idarə edə bilərsiniz"

#: crypto.cpp:654
msgid "&View/Edit..."
msgstr "&Gör/Düzəlt..."

#: crypto.cpp:680
msgid "Warn on &self-signed certificates or unknown CA's"
msgstr ""
"&Şəxsi imzalanmış və ya namə'lum CA (Vəsiqə Avtoriteti) halında "
"xəbərdar et"

#: crypto.cpp:682
msgid "Warn on &expired certificates"
msgstr "&Keçmiş vəsiqələrdə xəbərdar et"

#: crypto.cpp:684
msgid "Warn on re&voked certificates"
msgstr "&Ləğv edilmiş vəsiqələrdə xəbərdar et"

#: crypto.cpp:694
msgid ""
"This list box shows which sites you have decided to accept a certificate "
"from even though the certificate might fail the validation procedure."
msgstr ""
"Bu siyahı qutusu hansı saytlardan təsdiqləmə əməliyyatı bacarılmasa "
"da vəsiqə qəbul etdiyinizi göstərir"

#: crypto.cpp:702
msgid "&Add"
msgstr "&Əlavə Et"

#: crypto.cpp:710
msgid "&Clear"
msgstr "&Təmizlə"

#: crypto.cpp:715
msgid ""
"These options are not configurable because this module was not linked with "
"OpenSSL."
msgstr ""
"Bu seçənəklər quraşdırıla bilmir çünkü bu modul Open SSL ilə "
"bağlanmamışdır"

#: certexport.cpp:94 certexport.cpp:109 certexport.cpp:117 crypto.cpp:725
#: crypto.cpp:901 crypto.cpp:1191 crypto.cpp:1216 crypto.cpp:1221
#: crypto.cpp:1223 crypto.cpp:1395 crypto.cpp:1413 crypto.cpp:1462
#: crypto.cpp:1488 crypto.cpp:1490
msgid "SSL"
msgstr "SSL"

#: crypto.cpp:727 crypto.cpp:1777 crypto.cpp:1785 crypto.cpp:1789
msgid "OpenSSL"
msgstr "OpenSSL"

#: crypto.cpp:729
msgid "Your Certificates"
msgstr "Sizin Vəsiqələriniz"

#: crypto.cpp:730
msgid "Authentication"
msgstr "Kimlik Yoxlaması"

#: crypto.cpp:731
msgid "Peer SSL Certificates"
msgstr "Digər SSL Vəsiqələri"

#: crypto.cpp:734
msgid "SSL C.A.s"
msgstr "SSL C.A (Vəsiqə Avtoriteti)"

#: crypto.cpp:735
msgid "Validation Options"
msgstr "Təsdiqləmə Seçənəkləri"

#: crypto.cpp:897
msgid ""
"If you don't select at least one SSL algorithm, either SSL will not work or "
"the application may be forced to choose a suitable default."
msgstr ""
"Əgər ən az bir SSL alqoritmi seçməzsəniz, ya SSl işləməyəcəkdir "
"ya da proqram əsas bir seçənəyi işlədəcəkdir."

#: crypto.cpp:948
msgid "If you don't select at least one cipher, SSLv2 will not work."
msgstr "Əgər en az bir şifrləmə seçməzsəniz SSLv2 işləməyəcəkdir"

#: crypto.cpp:950
msgid "SSLv2 Ciphers"
msgstr "SSLv2 Şifrləri"

#: crypto.cpp:965
msgid "If you don't select at least one cipher, SSLv3 will not work."
msgstr "Əgər en az bir şifrləmə seçməzsəniz SSLv3 işləməyəcəkdir"

#: crypto.cpp:967
msgid "SSLv3 Ciphers"
msgstr "SSLv3 Şifrləri"

#: crypto.cpp:1095
msgid ""
"<h1>crypto</h1> This module allows you to configure SSL for use with most "
"KDE applications, as well as manage your personal certificates and the known "
"certificate authorities."
msgstr ""
"<h1>crypto</h1> Bu modul SSL'in quraşdırılması ilə bir çox KDE "
"proqramının işlədilməsini, və şəxsi vəsiqələrinizlə bilinən "
"vəsiqə avtoritetlərinin idarəsinə imkan verər."

#: crypto.cpp:1190
msgid "Couldn't open the certificate."
msgstr "Vəsiqəni aça bilmirəm."

#: crypto.cpp:1216
msgid "Error obtaining the certificate."
msgstr "Vəsiqə alma xətası."

#: crypto.cpp:1221 crypto.cpp:1488
msgid "This certificate passed the verification tests successfully."
msgstr "Vəsiqə müvəffəqiyyətlə sınaqdan keçdi."

#: crypto.cpp:1223 crypto.cpp:1490
msgid "This certificate has failed the tests and should be considered invalid."
msgstr "Vəsiqə sınaqdan keçə bilmədi və e'tibarsız olduğuna qərar verildi."

#: crypto.cpp:1389
msgid "Certificate password"
msgstr "Vəsiqə parolu"

#: crypto.cpp:1395
msgid "The certificate file could not be loaded.  Try a different password?"
msgstr "Vəsiqə faylı yüklənə bilmədi.  Başqa parol sınayaq?"

#: crypto.cpp:1413
msgid ""
"A certificate with that name already exists.  Are you sure that you wish to "
"replace it?"
msgstr "Bu adda vəsiqə onsuz da mövcuddur.  Üstünə yazmaq istəyirsiniz?"

#: crypto.cpp:1444 crypto.cpp:1475 crypto.cpp:1517
msgid "Enter the certificate password:"
msgstr "Xahiş edirik, vəsiqə parolunu girin:"

#: crypto.cpp:1450 crypto.cpp:1481 crypto.cpp:1523 crypto.cpp:1617
msgid "Decoding failed.  Please try again:"
msgstr "Kod açılması bacarılmadı. Xahiş edirik, yenidən sınayın:"

#: crypto.cpp:1462
msgid "Export failed."
msgstr "İxrac bacarılmadı."

#: crypto.cpp:1612
msgid "Enter the OLD password for the certificate:"
msgstr "Vəsiqənin KÖHNƏ parolunu girin:"

#: crypto.cpp:1626
msgid "Enter the new certificate password"
msgstr "Yeni vəsiqə parolunu girin"

#: crypto.cpp:1775 crypto.cpp:1783
msgid "Failed to load OpenSSL."
msgstr "OpenSSL yüklənə bilmədi."

#: crypto.cpp:1776
msgid "libssl was not found or successfully loaded."
msgstr "libssl tapılmadı ya da müvəffəqiyyətlə yüklənə bilmədi."

#: crypto.cpp:1784
msgid "libcrypto was not found or successfully loaded."
msgstr "libcrypto tapılmadı ya da müvəffəqiyyətlə yüklənə bilmədi."

#: crypto.cpp:1789
msgid "OpenSSL was successfully loaded."
msgstr "OpenSSL müvəffəqiyyətlə yükləndi."

#: crypto.cpp:1805
msgid "Personal SSL"
msgstr "Şəxsi SSL"

#: crypto.cpp:1806
msgid "Server SSL"
msgstr "Verici SSL"

#: crypto.cpp:1807
msgid "S/MIME"
msgstr "S/MIME"

#: crypto.cpp:1808
msgid "PGP"
msgstr "PGP"

#: crypto.cpp:1809
msgid "GPG"
msgstr "GPG"

#: crypto.cpp:1810
msgid "SSL Personal Request"
msgstr "SSL Şəxsi Sorğulaması"

#: crypto.cpp:1811
msgid "SSL Server Request"
msgstr "SSL Verici Sorğulaması"

#: crypto.cpp:1812
msgid "Netscape SSL"
msgstr "Netscape SSL"

#: crypto.cpp:1813
msgid ""
"_: Server certificate authority\n"
"Server CA"
msgstr "Verici CA"

#: crypto.cpp:1814
msgid ""
"_: Personal certificate authority\n"
"Personal CA"
msgstr "Şəxsi CA"

#: crypto.cpp:1815
msgid ""
"_: Secure MIME certificate authority\n"
"S/MIME CA"
msgstr "S/MIME CA"

#: crypto.cpp:1897
msgid "None"
msgstr "Heç Biri"

#: certexport.cpp:45
msgid "X509 Certificate Export"
msgstr "X509 Vəsiqə İxracı"

#: certexport.cpp:47
msgid "Format"
msgstr "Şəkil"

#: certexport.cpp:48
msgid "&PEM"
msgstr "&PEM"

#: certexport.cpp:49
msgid "&Netscape"
msgstr "&Netscape"

#: certexport.cpp:50
msgid "&DER/ASN1"
msgstr "&DER/ASN1"

#: certexport.cpp:51
msgid "&Text"
msgstr "&Mətn"

#: certexport.cpp:55
msgid "Filename:"
msgstr "Fayl adı:"

#: certexport.cpp:66
msgid "&Export"
msgstr "İ&xraç et"

#: certexport.cpp:94
msgid "Internal error.  Please report to kfm-devel@kde.org."
msgstr "Daxili xəta. Haxeş edirik kfm-devel@kde.org ünvanına xəbər verin."

#: certexport.cpp:109
msgid "Error converting the certificate into the requested format."
msgstr "Vəsiqəni istənilən şəklə gətirmə xətası."

#: certexport.cpp:117
msgid "Error opening file for output."
msgstr "Yekunlaşdırma üçün fayl açıla bilmir."

#: certexport.cpp:133
msgid ""
"*.pem|Privacy Enhanced Mail format\n"
"*.der|DER/ASN1 format"
msgstr ""
"*.pem|Privacy Enhanced Mail formatı\n"
"*.der|DER/ASN1 formatı"

#: kdatetimedlg.cpp:41
msgid "Date and Time Selector"
msgstr "Tarix və Zaman Seçici"

#: kdatetimedlg.cpp:46
msgid "Hour:"
msgstr "Saat:"

#: kdatetimedlg.cpp:51
msgid "Minute:"
msgstr "Dəqiqə:"

#: kdatetimedlg.cpp:56
msgid "Second:"
msgstr "Saniyə:"
