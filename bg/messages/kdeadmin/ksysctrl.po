# KDE2 - ksysctrl.pot Russian translation.
# Copyright (C) 2000 KDE Team.
# Dmitri Ovechkin <d_ovechkin@hotmail.com>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: ksysctrl\n"
"POT-Creation-Date: 2001-07-19 00:28+0200\n"
"PO-Revision-Date: 2001-06-22 14:47+GMT\n"
"Last-Translator: nikolova vesselina <vesselina@galgenberg.net>\n"
"Language-Team: Bulgarisch\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: abouttab.cpp:33
msgid ""
"Copyright (C) 1999\n"
"Thorsten Westheider"
msgstr ""
"Copyright (C) 1999\n"
"Thorsten Westheider"

#: abouttab.cpp:36
msgid ""
"Version %1\n"
"\n"
msgstr ""
"Версия %1\n"
"\n"

#: abouttab.cpp:38
msgid ""
"This program is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation; either version 2 of the License, or (at your option) "
"any later version.\n"
msgstr ""

#: abouttab.cpp:47
msgid "The KDE System Control Tool"
msgstr ""
"Помощтна програма за настройка на "
"хардуера за KDE"

#: conflict.cpp:50
msgid "used by:"
msgstr "използва се от:"

#: conflictdisplay.cpp:47
msgid "No conflicts."
msgstr "Няма конфликт."

#: device.cpp:82
msgid "Unclassified devices"
msgstr "Неизвестно устройство"

#: device.cpp:83 device.cpp:98
msgid "Mass storage controllers"
msgstr "Дискови контролери"

#: device.cpp:84
msgid "Network controllers"
msgstr "Мрежови платки"

#: device.cpp:85
msgid "Display controllers"
msgstr "Видео контролери"

#: device.cpp:86
msgid "Audio, video and game controllers"
msgstr "Звукови, видео и игрови контролери"

#: device.cpp:87 pcidevice.cpp:94
msgid "Memory"
msgstr "Памет"

#: device.cpp:88 device.cpp:90 device.cpp:93
msgid "System components"
msgstr "Системни компоненти"

#: device.cpp:89
msgid "Communication controllers"
msgstr "Комуникационни контролери"

#: device.cpp:91
msgid "Input device controllers"
msgstr "Входни устройства"

#: device.cpp:92
msgid "Docking stations"
msgstr ""

#: device.cpp:94
msgid "Serial bus controllers"
msgstr "Контролери на серийни устройства"

#: device.cpp:96
msgid "Connectors"
msgstr "Конектори"

#: device.cpp:97
msgid "Drives"
msgstr "Драйвери"

#: device.cpp:99 scsidevice.cpp:48
msgid "Printer"
msgstr "Принтер"

#: device.cpp:100
msgid "CD-ROM"
msgstr ""

#: device.cpp:101
msgid "SCSI controllers"
msgstr "SCSI контролери"

#: device.cpp:102
msgid "Scanners"
msgstr "Скенери"

#: device.cpp:103
msgid "PC-Cards"
msgstr "PCMCIA карти"

#: device.cpp:104 scsidevice.cpp:49
msgid "Processor"
msgstr "Процесор"

#: device.cpp:106
msgid "Unknown device class"
msgstr "Клас неизвестни устройства"

#: devmantab.cpp:46
msgid "Device"
msgstr "Устройство"

#: devmantab.cpp:58
msgid "&Refresh"
msgstr "&Обнови"

#: devmantab.cpp:144
msgid ""
"You are about to change device resource\n"
"settings which may affect the stability\n"
"of your system.\n"
"Do you really want the changes to take\n"
"effect?"
msgstr ""
"Вие се готвите да промените параметрите "
"на устройствата,\n"
"това може да повлияе на устойчивата "
"работа на системата.\n"
"Действително ли искате \n"
"да ги промените?"

#: devmantab.cpp:150
msgid "Change resource settings"
msgstr "Промяна на настройките на ресурсите"

#: dmaport.cpp:42 dmaport.cpp:47
msgid "DMA"
msgstr ""

#: drvspecinfobox.cpp:36
msgid "%1 MB (%2 GB)"
msgstr "%1 Мб (%2 Гб)"

#: drvspecinfobox.cpp:41
msgid "Device path:"
msgstr "Път към устройство:"

#: drvspecinfobox.cpp:43
msgid "Storage capacity:"
msgstr "Капацитет на носителя:"

#: drvusginfobox.cpp:42 propdialog.cpp:53
msgid "Usage"
msgstr "Използване"

#: drvusginfobox.cpp:47
msgid "One partition"
msgstr "Един дял"

#: drvusginfobox.cpp:49
msgid "%1 partitions"
msgstr "%1 дяла"

#: generaltab.cpp:77
msgid "Computer:"
msgstr "Компютър:"

#: generaltab.cpp:132
msgid ""
"\n"
"Intel MMX(TM) Technology"
msgstr ""
"\n"
"Intel MMX(TM) технология"

#: generaltab.cpp:142
msgid " processors"
msgstr " процесори"

#: generaltab.cpp:172
msgid "%1 MB RAM"
msgstr "%1 Мб RAM"

#: generaltab.cpp:180 generaltab.cpp:227
msgid "Unknown"
msgstr "Неизвестно"

#: generaltab.cpp:186
msgid ""
"System:\n"
msgstr ""
"Система:\n"

#: geninfobox.cpp:38
msgid "Device type:"
msgstr "Тип устройство:"

#: geninfobox.cpp:40
msgid "Manufacturer:"
msgstr "Производител:"

#: idedrive.cpp:32 pccarddevice.cpp:64
msgid "Unknown vendor"
msgstr "Неизвестен доставчик"

#: idedrive.cpp:51
#, fuzzy
msgid "Hard disk drive"
msgstr "Хард диск устройство"

#: idedrive.cpp:52
msgid "Floppy drive"
msgstr "Флопи диск"

#: idedrive.cpp:53 scsidevice.cpp:51
msgid "CD-ROM drive"
msgstr "CD-ROM устройство"

#: idedrive.cpp:54 isapnpdevice.cpp:78 pccarddevice.cpp:65 pcidevice.cpp:137
#: pcidevice.cpp:281 scsidevice.cpp:57
msgid "Unknown device"
msgstr "Неизвестно устройство"

#: interrupt.cpp:42
msgid "interrupt"
msgstr "прекъсвания"

#: interrupt.cpp:47
msgid "Interrupt"
msgstr "Прекъсвания"

#: ioaddress.cpp:43 ioaddress.cpp:47
msgid "I/O area"
msgstr "Диапазон I/O"

#: isapnpdevice.cpp:75 pcidevice.cpp:82
msgid "Network controller"
msgstr "Мрежови контролер"

#: isapnpdevice.cpp:76 pcidevice.cpp:89
msgid "Multimedia audio controller"
msgstr "Аудио контролер"

#: isapnpdevice.cpp:77 pcidevice.cpp:119
msgid "Input device controller"
msgstr "Адаптер на устроство за въвеждане"

#: main.cpp:42
msgid "KDE System Control Tool"
msgstr ""
"Помощна програма за контрол на хардуер на "
"KDE "

#: main.cpp:56
msgid "KSysControl"
msgstr "KSysControl"

#: mainwindow.cpp:37
msgid "Properties of System"
msgstr "Свойства на системата"

#: mainwindow.cpp:48
msgid "Device Manager"
msgstr "Менаджер на устройства"

#: memaddress.cpp:31
msgid "memory area"
msgstr "област от паметта"

#: memaddress.cpp:36
msgid "Memory area"
msgstr "Област от паметта"

#: parport.cpp:43
msgid "Printer connector"
msgstr "Порт на принтер"

#: parport.cpp:44
msgid "ECP printer connector"
msgstr "Порт на принтер ECP"

#: parport.cpp:45
msgid "Unknown printer connector"
msgstr "Неизвестен порт на принтер"

#: pccarddevice.cpp:76
msgid "Multifunction card"
msgstr "Многофункционално устройство"

#: pccarddevice.cpp:77
msgid "Memory card"
msgstr "Карта за разширение на паметта"

#: pccarddevice.cpp:78
msgid "Serial/Modem card"
msgstr "Серийна/Модемна карта"

#: pccarddevice.cpp:79
msgid "Parallel card"
msgstr "Паралелен порт"

#: pccarddevice.cpp:80
msgid "Fixed disk"
msgstr "Твърд диск"

#: pccarddevice.cpp:81
msgid "Video card"
msgstr "Видеоплатка"

#: pccarddevice.cpp:82
msgid "Network adapter"
msgstr "Мрежова карта"

#: pccarddevice.cpp:83
msgid "AIMS card"
msgstr "AIMS карта"

#: pccarddevice.cpp:84
msgid "SCSI adapter"
msgstr "SCSI адаптер"

#: pccarddevice.cpp:86
msgid "unknown card type"
msgstr "неизвестна тип карта"

#: pccarddevice.cpp:94
msgid "Empty socket"
msgstr "Празно гнездо"

#: pcidevice.cpp:68
msgid "Non-VGA unclassified device"
msgstr "Неизвестно не VGA-съвместимо устройство"

#: pcidevice.cpp:69
msgid "VGA compatible unclassified device"
msgstr "Неизвестно VGA-съвместимо устройство"

#: pcidevice.cpp:71
msgid "SCSI storage controller"
msgstr "SCSI контролер"

#: pcidevice.cpp:72
msgid "IDE interface"
msgstr "IDE интерфейс"

#: pcidevice.cpp:73
msgid "Floppy disk controller"
msgstr "Флопи дисков контролер"

#: pcidevice.cpp:74
msgid "IPI bus controller"
msgstr "Контролер на IPI"

#: pcidevice.cpp:75
msgid "RAID bus controller"
msgstr "RAID Контролер"

#: pcidevice.cpp:76
msgid "Unknown mass storage controller"
msgstr "Неизвестен контролер"

#: pcidevice.cpp:78
msgid "Ethernet controller"
msgstr "Ethernet контролер"

#: pcidevice.cpp:79
msgid "Token ring network controller"
msgstr "Token ring контролер"

#: pcidevice.cpp:80
msgid "FDDI network controller"
msgstr "Мрежова платка за FDDI"

#: pcidevice.cpp:81
msgid "ATM network controller"
msgstr "Мрежова платка за ATM"

#: pcidevice.cpp:84
msgid "VGA compatible controller"
msgstr "VGA-съвместим контролер"

#: pcidevice.cpp:85
msgid "XGA compatible controller"
msgstr "XGA-съвместим видеоадаптер"

#: pcidevice.cpp:86
msgid "Display controller"
msgstr "Видеоадаптер"

#: pcidevice.cpp:88
msgid "Multimedia video controller"
msgstr "Мултимедиен видео контролер"

#: pcidevice.cpp:90
msgid "Multimedia controller"
msgstr "Мултимедиен контролер"

#: pcidevice.cpp:92
msgid "RAM memory"
msgstr "RAM памет"

#: pcidevice.cpp:93
msgid "FLASH memory"
msgstr "FLASH памет"

#: pcidevice.cpp:96
msgid "Host bridge"
msgstr "Системен мост"

#: pcidevice.cpp:97
msgid "ISA bridge"
msgstr "ISA мост"

#: pcidevice.cpp:98
msgid "EISA bridge"
msgstr "EISA мост"

#: pcidevice.cpp:99
msgid "MicroChannel bridge"
msgstr "MicroChannel мост"

#: pcidevice.cpp:100
msgid "PCI bridge"
msgstr "PCI мост"

#: pcidevice.cpp:101
msgid "PCMCIA bridge"
msgstr "PCMCIA мост"

#: pcidevice.cpp:102
msgid "NuBus bridge"
msgstr "NuBus мост"

#: pcidevice.cpp:103
msgid "CardBus bridge"
msgstr "CardBus мост"

#: pcidevice.cpp:104
msgid "Bridge"
msgstr "Мост"

#: pcidevice.cpp:106
msgid "Serial controller"
msgstr "Сериен контролер"

#: pcidevice.cpp:107
msgid "Parallel controller"
msgstr "Паралелен контролер"

#: pcidevice.cpp:108
msgid "Communication controller"
msgstr "Комуникационен контролер"

#: pcidevice.cpp:110
msgid "PIC"
msgstr "PIC"

#: pcidevice.cpp:111
msgid "DMA controller"
msgstr "DMA-Контролер "

#: pcidevice.cpp:112
msgid "Timer"
msgstr "Таймер"

#: pcidevice.cpp:113
msgid "Real time clock"
msgstr "Часовник на реално време"

#: pcidevice.cpp:114
msgid "System peripheral"
msgstr "Периферийни устройства"

#: pcidevice.cpp:116
msgid "Keyboard controller"
msgstr "Контролер на клавиатура"

#: pcidevice.cpp:117
msgid "Digitizer Pen"
msgstr "Перо на дигитайзер"

#: pcidevice.cpp:118
msgid "Mouse controller"
msgstr "Контролер на мишка"

#: pcidevice.cpp:121
msgid "Generic Docking Station"
msgstr ""

#: pcidevice.cpp:122
msgid "Docking Station"
msgstr ""

#: pcidevice.cpp:124
msgid "386"
msgstr "386"

#: pcidevice.cpp:125
msgid "486"
msgstr "486"

#: pcidevice.cpp:126
msgid "Pentium"
msgstr "Pentium"

#: pcidevice.cpp:127
msgid "Alpha"
msgstr "Alpha"

#: pcidevice.cpp:128
msgid "Power PC"
msgstr "Power PC"

#: pcidevice.cpp:129
msgid "Co-processor"
msgstr "Копроцесор"

#: pcidevice.cpp:131
msgid "FireWire (IEEE 1394)"
msgstr "FireWire (IEEE 1394)"

#: pcidevice.cpp:132
msgid "ACCESS Bus"
msgstr "Шина ACCESS"

#: pcidevice.cpp:133
msgid "SSA"
msgstr "SSA"

#: pcidevice.cpp:134
msgid "USB Controller"
msgstr "Контролер на USB"

#: pcidevice.cpp:135
msgid "Fiber Channel"
msgstr "Оптичен канал"

#: propdialog.cpp:31
msgid "Properties of "
msgstr "Свойства на"

#: propdialog.cpp:37
msgid "General"
msgstr "Общи"

#: propdialog.cpp:38
msgid "Driver"
msgstr "Драйвер"

#: propdialog.cpp:43
msgid "Resources"
msgstr "Ресурси"

#: propdialog.cpp:52
msgid "Settings"
msgstr "Настройки"

#: proprestab.cpp:58
msgid "Settings based on:"
msgstr "Настройки на основата на:"

#: proprestab.cpp:70
msgid "Resource type"
msgstr "Тип на ресурс"

#: proprestab.cpp:71
msgid "Setting"
msgstr "Значение"

#: proprestab.cpp:74
msgid "Change &setting..."
msgstr "Промени &значенията..."

#: proprestab.cpp:79
msgid "Device conflicts:"
msgstr "Конфликтни устройства:"

#: proprestab.cpp:166
msgid "Base configuration"
msgstr "Базова конфигурация"

#: reschangedlg.cpp:45
msgid "Enter the desired %1 for this device."
msgstr "Въведете %1 за това устройство:"

#: reschangedlg.cpp:47
#, c-format
msgid "Change %1"
msgstr "Промени %1"

#: reschangedlg.cpp:62
msgid "&Value:"
msgstr "З&начение:"

#: reschangedlg.cpp:70
msgid "Information (Conflict)"
msgstr "Информация (конфликт)"

#: reschangedlg.cpp:72
msgid "The chosen setting leads to conflict with the following devices:"
msgstr ""
"Избраните настройки влизат в конфликт "
"със следните устройства:"

#: resentry.cpp:63
msgid "DMA "
msgstr "DMA "

#: scsidevice.cpp:46
msgid "Direct access device"
msgstr "Устройство за пряк достъп"

#: scsidevice.cpp:47
msgid "Sequential access device"
msgstr "Устройство за последователен достъпа"

#: scsidevice.cpp:50
msgid "WORM"
msgstr "Устройство за еднократен запис (WORM)"

#: scsidevice.cpp:52
msgid "Scanner"
msgstr "Скенер"

#: scsidevice.cpp:53
msgid "Optical device"
msgstr "Оптично устройство"

#: scsidevice.cpp:54
msgid "Medium changer"
msgstr "Сменяемо устройство"

#: scsidevice.cpp:55
msgid "Communication device"
msgstr "Устройство за връзка"

#: scsidevice.cpp:56
msgid "Enclosure"
msgstr "Обкръжение"

#: scsiinfobox.cpp:44
msgid "Target ID:"
msgstr "Номер на устройство за SCSI Id"

#: scsiinfobox.cpp:46
msgid "Logical device unit:"
msgstr "Логически номер на устройство (LUN)"

#: scsiinfobox.cpp:48
msgid "Firmware revision:"
msgstr "Версия на микрокода:"

#: scsiscanner.cpp:165
msgid "Rescanning SCSI bus..."
msgstr "Пресканирам SCSI шината..."

#: scsiscanner.cpp:165
msgid "Abort scan"
msgstr "Сканирането прекъсна"

#: scsiscanner.cpp:169
msgid "SCSI bus rescan"
msgstr "Пресканира SCSI шина"

#: serport.cpp:42
msgid "Unknown serial connector"
msgstr "Неизвестен сериен порт"

#: serport.cpp:44
msgid "Serial connector "
msgstr "Сериен порт"

#: stateinfobox.cpp:33
msgid "Device state"
msgstr "Състояние на устройствата"

#: stateinfobox.cpp:39
msgid "This device is operational."
msgstr "Това устройството работи нормално."

#: stateinfobox.cpp:41
msgid "This socket is empty."
msgstr "Гнездото е празно."

#: stateinfobox.cpp:43
msgid "No status information available."
msgstr "Информация за статуса не е налична."

#: vendorinfobox.cpp:46
msgid "Vendor:"
msgstr "Доставчик:"

#: vendorinfobox.cpp:48
msgid "Date:"
msgstr "Дата:"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Борислав Александров"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "boby_a_@web.bg"

#~ msgid "&Properties"
#~ msgstr "&Свойства"
