# Hebrew Translation of KDE
# Copyright (C) 2000-2001 Free Software Foundation, Inc.
# Meni Livne <livne@kde.org>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 02:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Meni Livne <livne@kde.org>\n"
"Language-Team: Hebrew <kde-il@yahoogroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: connectionconfig.cpp:50
msgid "Add..."
msgstr "...ףסוה"

#: connectionconfig.cpp:55
msgid "Edit..."
msgstr "...הכירע"

#: kcmkdb.cpp:49
msgid "Plugins"
msgstr "םיפסות"

#: kcmkdb.cpp:53
msgid "Connections"
msgstr "םירוביח"

#: pluginconfig.cpp:53
msgid "Info..."
msgstr "...עדימ"

#: pluginconfig.cpp:57
msgid "&Configure..."
msgstr "...תורד&גה"
