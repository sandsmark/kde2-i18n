# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: klock\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-06-04 11:27GMT\n"
"Last-Translator: Malcolm Hunter <malcolm.hunter@gmx.co.uk>\n"
"Language-Team: UK English <en_GB>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: rc.cpp:1
msgid "Media Screensaver"
msgstr "Media Screensaver"

#: rc.cpp:3
msgid "&Down"
msgstr "&Down"

#: rc.cpp:4
msgid "&Up"
msgstr "&Up"

#: rc.cpp:5
msgid "&Add"
msgstr "&Add"

#: rc.cpp:7
msgid "Settings"
msgstr "Settings"

#: rc.cpp:8
msgid "Only show one randomly chosen medium"
msgstr "Only show one randomly chosen medium"

#: rc.cpp:9
msgid "Switch to another medium after a delay"
msgstr "Switch to another medium after a delay"

#: kdesavers/slideshow.cpp:888 rc.cpp:10
msgid "Delay:"
msgstr "Delay:"

#: rc.cpp:11
msgid "Choose next medium randomly"
msgstr "Choose next medium randomly"

#: rc.cpp:12
msgid "seconds"
msgstr "seconds"

#: random.cpp:34
msgid ""
"Usage: %1 [-setup] [args]\n"
msgstr ""
"Usage: %1 [-setup] [args]\n"

#: random.cpp:35
msgid ""
"Starts a random screensaver.\n"
msgstr ""
"Starts a random screensaver.\n"

#: random.cpp:36
msgid ""
"Any arguments (except -setup) are passed on to the screensaver.\n"
msgstr ""
"Any arguments (except -setup) are passed on to the screensaver.\n"

#: random.cpp:41
msgid "Start a random KDE screen saver"
msgstr "Start a random KDE screen saver"

#: random.cpp:48 xsavers/main.cpp:35
msgid "Run in the specified XWindow."
msgstr "Run in the specified XWindow."

#: random.cpp:49 xsavers/main.cpp:36
msgid "Run in the root XWindow."
msgstr "Run in the root XWindow."

#: kdesavers/banner.cpp:40
msgid "KBanner"
msgstr "KBanner"

#: kdesavers/banner.cpp:73
msgid "Setup Banner Screen Saver"
msgstr "Setup Banner Screen Saver"

#: kdesavers/banner.cpp:85
msgid "Family:"
msgstr "Family:"

#: kdesavers/banner.cpp:103 kdesavers/science.cpp:926
msgid "Size:"
msgstr "Size:"

#: kdesavers/banner.cpp:122
msgid "Bold"
msgstr "Bold"

#: kdesavers/banner.cpp:133 kdesavers/blankscrn.cpp:63
msgid "Color:"
msgstr "Colour:"

#: kdesavers/banner.cpp:141
msgid "Cycling color"
msgstr "Cycling colour"

#: kdesavers/banner.cpp:156 kdesavers/lines.cpp:150 kdesavers/lorenz.cpp:89
#: kdesavers/matrix.cpp:498 kdesavers/polygon.cpp:107
#: kdesavers/science.cpp:957 xsavers/bouboule.cpp:996 xsavers/morph3d.cpp:1052
#: xsavers/rock.cpp:547 xsavers/space.cpp:599 xsavers/swarm.cpp:334
msgid "Speed:"
msgstr "Speed:"

#: kdesavers/banner.cpp:171
msgid "Message:"
msgstr "Message:"

#: kdesavers/banner.cpp:180
msgid "Show current time"
msgstr "Show current time"

#: kdesavers/banner.cpp:338
msgid ""
"Banner Version 2.2.0\n"
"\n"
"written by Martin R. Jones 1996\n"
"mjones@kde.org\n"
"extended by Alexander Neundorf 2000\n"
"alexander.neundorf@rz.tu-ilmenau.de\n"
msgstr ""
"Banner Version 2.2.0\n"
"\n"
"written by Martin R. Jones 1996\n"
"mjones@kde.org\n"
"extended by Alexander Neundorf 2000\n"
"alexander.neundorf@rz.tu-ilmenau.de\n"

#: kdesavers/blankscrn.cpp:27
msgid "KBlankScreen"
msgstr "KBlankScreen"

#: kdesavers/blankscrn.cpp:54
msgid "Setup Blank Screen Saver"
msgstr "Setup Blank Screen Saver"

#: kdesavers/blob.cpp:54
msgid "KBlob"
msgstr "KBlob"

#: kdesavers/blob.cpp:75
msgid "Random Linear"
msgstr "Random Linear"

#: kdesavers/blob.cpp:76
msgid "Horizonal Sine"
msgstr "Horizonal Sine"

#: kdesavers/blob.cpp:77
msgid "Circular Bounce"
msgstr "Circular Bounce"

#: kdesavers/blob.cpp:78
msgid "Polar Coordinates"
msgstr "Polar Coordinates"

#: kdesavers/blob.cpp:79
msgid "Random"
msgstr "Random"

#: kdesavers/blob.cpp:93
msgid "Sorry. This screen saver requires a color display"
msgstr "Sorry. This screen saver requires a colour display"

#: kdesavers/blob.cpp:454
msgid "Setup Blob Screen Saver"
msgstr "Setup Blob Screen Saver"

#: kdesavers/blob.cpp:464
msgid "Frame Show (secs)"
msgstr "Frame Show (secs)"

#: kdesavers/blob.cpp:474
msgid "Algorithm"
msgstr "Algorithm"

#: kdesavers/blob.cpp:551
msgid ""
"Blobsaver Version 0.1\n"
"\n"
"written by Tiaan Wessels 1997\n"
"tiaan@netsys.co.za"
msgstr ""
"Blobsaver Version 0.1\n"
"\n"
"written by Tiaan Wessels 1997\n"
"tiaan@netsys.co.za"

#: kdesavers/kvm.cpp:69
msgid "Virtual Machine"
msgstr "Virtual Machine"

#: kdesavers/kvm.cpp:287
msgid "Setup Virtual Machine Screen Saver"
msgstr "Setup Virtual Machine Screen Saver"

#: kdesavers/kvm.cpp:299
msgid "Virtual machine speed"
msgstr "Virtual machine speed"

#: kdesavers/kvm.cpp:312
msgid "Display update speed"
msgstr "Display update speed"

#: kdesavers/kvm.cpp:400
msgid ""
"Virtual Machine Version 0.1\n"
"\n"
"Copyright (c) 2000 Artur Rataj <art@zeus.polsl.gliwice.pl>\n"
msgstr ""
"Virtual Machine Version 0.1\n"
"\n"
"Copyright (c) 2000 Artur Rataj <art@zeus.polsl.gliwice.pl>\n"

#: kdesavers/kvm.cpp:401
msgid "About The Virtual Machine"
msgstr "About The Virtual Machine"

#: kdesavers/lines.cpp:39
msgid "KLines"
msgstr "KLines"

#: kdesavers/lines.cpp:131
msgid "Setup Lines Screen Saver"
msgstr "Setup Lines Screen Saver"

#: kdesavers/lines.cpp:139 kdesavers/polygon.cpp:82
msgid "Length:"
msgstr "Length:"

#: kdesavers/lines.cpp:161
msgid "Beginning:"
msgstr "Beginning:"

#: kdesavers/lines.cpp:170
msgid "Middle:"
msgstr "Middle:"

#: kdesavers/lines.cpp:179
msgid "End:"
msgstr "End:"

#: kdesavers/lines.cpp:269
msgid ""
"Lines Version 2.2.0\n"
"\n"
"written by Dirk Staneker 1997\n"
"dirk.stanerker@student.uni-tuebingen.de"
msgstr ""
"Lines Version 2.2.0\n"
"\n"
"written by Dirk Staneker 1997\n"
"dirk.stanerker@student.uni-tuebingen.de"

#: kdesavers/lorenz.cpp:34
msgid "KLorenz"
msgstr "KLorenz"

#: kdesavers/lorenz.cpp:77
msgid "Setup Lorenz Attractor Screen Saver"
msgstr "Setup Lorenz Attractor Screen Saver"

#: kdesavers/lorenz.cpp:100
msgid "Epoch:"
msgstr "Epoch:"

#: kdesavers/lorenz.cpp:111
msgid "Color rate:"
msgstr "Colour rate:"

#: kdesavers/lorenz.cpp:122
msgid "Rotation Z:"
msgstr "Rotation Z:"

#: kdesavers/lorenz.cpp:133
msgid "Rotation Y:"
msgstr "Rotation Y:"

#: kdesavers/lorenz.cpp:144
msgid "Rotation X:"
msgstr "Rotation X:"

#: kdesavers/lorenz.cpp:246
msgid ""
"Lorenz Attractor screen saver for KDE\n"
"\n"
"Copyright (c) 2000 Nicolas Brodu"
msgstr ""
"Lorenz Attractor screen saver for KDE\n"
"\n"
"Copyright (c) 2000 Nicolas Brodu"

#: kdesavers/matrix.cpp:49
msgid "Kmatrix \"The Matrix\" movie style screen saver"
msgstr "Kmatrix \"The Matrix\" movie style screen saver"

#: kdesavers/matrix.cpp:462
msgid "Setup Matrix Screen Saver"
msgstr "Setup Matrix Screen Saver"

#: kdesavers/matrix.cpp:474
msgid "Density:"
msgstr "Density:"

#: kdesavers/matrix.cpp:521
msgid "Insert from:"
msgstr "Insert from:"

#: kdesavers/matrix.cpp:536
msgid "Both"
msgstr "Both"

#: kdesavers/matrix.cpp:657
msgid ""
"KMatrix\n"
"\n"
"based on xmatrix\n"
"Copyright (C) 1999 by Jamie Zawinski <jwz@jwz.org>\n"
"Ported to kscreensaver by Dmitry DELTA Malykhanov\n"
"<d.malykhanov@iname.com>\n"
"Ported to KDE 2.0 by Thorsten Westheider\n"
"<thorsten.westheider@teleos-web.de>"
msgstr ""
"KMatrix\n"
"\n"
"based on xmatrix\n"
"Copyright (C) 1999 by Jamie Zawinski <jwz@jwz.org>\n"
"Ported to kscreensaver by Dmitry DELTA Malykhanov\n"
"<d.malykhanov@iname.com>\n"
"Ported to KDE 2.0 by Thorsten Westheider\n"
"<thorsten.westheider@teleos-web.de>"

#: kdesavers/matrix.cpp:664
msgid "About KMatrix"
msgstr "About KMatrix"

#: kdesavers/polygon.cpp:40
msgid "KPolygon"
msgstr "KPolygon"

#: kdesavers/polygon.cpp:74
msgid "Setup Polygon Screen Saver"
msgstr "Setup Polygon Screen Saver"

#: kdesavers/polygon.cpp:94
msgid "Vertices:"
msgstr "Vertices:"

#: kdesavers/polygon.cpp:219
msgid ""
"Polygon Version 2.2.0\n"
"\n"
"written by Martin R. Jones 1996\n"
"mjones@kde.org"
msgstr ""
"Polygon Version 2.2.0\n"
"\n"
"written by Martin R. Jones 1996\n"
"mjones@kde.org"

#: kdesavers/science.cpp:62
msgid "Science Screensaver"
msgstr "Science Screensaver"

#: kdesavers/science.cpp:89
msgid "Whirl"
msgstr "Whirl"

#: kdesavers/science.cpp:92
msgid "Sphere"
msgstr "Sphere"

#: kdesavers/science.cpp:95
msgid "Exponential"
msgstr "Exponential"

#: kdesavers/science.cpp:98
msgid "Contraction"
msgstr "Contraction"

#: kdesavers/science.cpp:101
msgid "Wave"
msgstr "Wave"

#: kdesavers/science.cpp:104
msgid "Curvature"
msgstr "Curvature"

#: kdesavers/science.cpp:866
msgid "Setup Science Screen Saver"
msgstr "Setup Science Screen Saver"

#: kdesavers/science.cpp:884 xsavers/slidescreen.cpp:488
msgid "Mode:"
msgstr "Mode:"

#: kdesavers/science.cpp:898
msgid "Inverse"
msgstr "Inverse"

#: kdesavers/science.cpp:908
msgid "Gravity"
msgstr "Gravity"

#: kdesavers/science.cpp:917
msgid "Hide Background"
msgstr "Hide Background"

#: kdesavers/science.cpp:942
msgid "Intensity:"
msgstr "Intensity:"

#: kdesavers/science.cpp:970
msgid "Motion:"
msgstr "Motion:"

#: kdesavers/science.cpp:1187
msgid ""
"Science Version 0.26.5\n"
"\n"
"written by Rene Beutler (1998)\n"
"rbeutler@g26.ethz.ch"
msgstr ""
"Science Version 0.26.5\n"
"\n"
"written by Rene Beutler (1998)\n"
"rbeutler@g26.ethz.ch"

#: kdesavers/slideshow.cpp:44
msgid "KSlideshow"
msgstr "KSlideshow"

#: kdesavers/slideshow.cpp:724
msgid "No images found"
msgstr "No images found"

#: kdesavers/slideshow.cpp:798
msgid ""
"Failed to load image \"%1\"\n"
msgstr ""
"Failed to load image \"%1\"\n"

#: kdesavers/slideshow.cpp:864
msgid "Setup Slide Show Screen Saver"
msgstr "Setup Slide Show Screen Saver"

#: kdesavers/slideshow.cpp:875
msgid "Zoom pictures"
msgstr "Zoom pictures"

#: kdesavers/slideshow.cpp:879
msgid "Random play"
msgstr "Random play"

#: kdesavers/slideshow.cpp:883
msgid "Show names"
msgstr "Show names"

#: kdesavers/slideshow.cpp:911
msgid "Select..."
msgstr "Select..."

#: kdesavers/slideshow.cpp:952 kdesavers/slideshow.cpp:1022
msgid "Delay: %1 seconds"
msgstr "Delay: %1 seconds"

#: kdesavers/slideshow.cpp:1034
msgid "Choose Images Directory"
msgstr "Choose Images Directory"

#: kdesavers/slideshow.cpp:1055
msgid ""
"SlideShow Version 1.1\n"
"\n"
"Copyright (c) 1999 by\n"
"Stefan Taferner <taferner@kde.org>\n"
msgstr ""
"SlideShow Version 1.1\n"
"\n"
"Copyright (c) 1999 by\n"
"Stefan Taferner <taferner@kde.org>\n"

#: kpartsaver/kpartsaver.cpp:62
msgid "KPart Screensaver"
msgstr "KPart Screensaver"

#: kpartsaver/kpartsaver.cpp:122
msgid "The screensaver isn't configured yet"
msgstr "The screensaver isn't configured yet"

#: kpartsaver/kpartsaver.cpp:266
msgid "All of your files are unsupported"
msgstr "All of your files are unsupported"

#: kpartsaver/kpartsaver.cpp:349
msgid "Select media files"
msgstr "Select media files"

#: kxsconfig/kxsconfig.cpp:240
msgid "KDE X Screensaver Configuration tool"
msgstr "KDE X Screensaver Configuration tool"

#: kxsconfig/kxsconfig.cpp:246
msgid "Filename of the screensaver to configure."
msgstr "Filename of the screensaver to configure."

#: kxsconfig/kxsrun.cpp:45
msgid "KDE X Screensaver Launcher"
msgstr "KDE X Screensaver Launcher"

#: kxsconfig/kxsrun.cpp:51
msgid "Filename of the screensaver to start."
msgstr "Filename of the screensaver to start."

#: kxsconfig/kxsrun.cpp:52
msgid "Extra options to pass to the screensaver."
msgstr "Extra options to pass to the screensaver."

#: xsavers/bouboule.cpp:983
msgid "Setup Bouboule Screen Saver"
msgstr "Setup Bouboule Screen Saver"

#: xsavers/bouboule.cpp:1010
msgid "Number of points:"
msgstr "Number of points:"

#: xsavers/bouboule.cpp:1024
msgid "Point size:"
msgstr "Point size:"

#: xsavers/bouboule.cpp:1038
msgid "Color-change frequency:"
msgstr "Colour-change frequency:"

#: xsavers/bouboule.cpp:1054
msgid "3D mode"
msgstr "3D mode"

#: xsavers/bouboule.cpp:1176
msgid ""
"Bouboule v0.1 -- a glob of spheres twisting and changing size\n"
"\n"
"Copyright (c) 1996 by Jeremie PETIT\n"
"\n"
"Ported to kscreensave by Cedric Tefft"
msgstr ""
"Bouboule v0.1 -- a glob of spheres twisting and changing size\n"
"\n"
"Copyright (c) 1996 by Jeremie PETIT\n"
"\n"
"Ported to kscreensave by Cedric Tefft"

#: xsavers/main.cpp:28
msgid "KDE Screen Lock / Saver"
msgstr "KDE Screen Lock / Saver"

#: xsavers/main.cpp:34
msgid "Setup screen saver."
msgstr "Setup screen saver."

#: xsavers/main.cpp:37
msgid "Start screen saver in demo mode."
msgstr "Start screen saver in demo mode."

#: xsavers/morph3d.cpp:863 xsavers/pipes.cpp:261 xsavers/space.cpp:432
msgid ""
"GL can not render with root visual\n"
msgstr ""
"GL can not render with root visual\n"

#: xsavers/morph3d.cpp:1036
msgid "Setup Morph3D Screen Saver"
msgstr "Setup Morph3D Screen Saver"

#: xsavers/morph3d.cpp:1062
msgid "Object Type:"
msgstr "Object Type:"

#: xsavers/morph3d.cpp:1139
msgid ""
"Morph3D\n"
"\n"
"Copyright (c) 1997 by Marcelo F. Vianna\n"
"\n"
"Ported to kscreensave by Emanuel Pirker."
msgstr ""
"Morph3D\n"
"\n"
"Copyright (c) 1997 by Marcelo F. Vianna\n"
"\n"
"Ported to kscreensave by Emanuel Pirker."

#: xsavers/pipes.cpp:751
msgid "Setup Pipes Screen Saver"
msgstr "Setup Pipes Screen Saver"

#: xsavers/pipes.cpp:764
msgid "Number of Pipes"
msgstr "Number of Pipes"

#: xsavers/pipes.cpp:820
msgid ""
"KPipes\n"
"Copyright (c) 1998-2000\n"
"Lars Doelle <lars.doelle@on-line.de>"
msgstr ""
"KPipes\n"
"Copyright (c) 1998-2000\n"
"Lars Doelle <lars.doelle@on-line.de>"

#: xsavers/pyro.cpp:516
msgid "Setup Pyro Screen Saver"
msgstr "Setup Pyro Screen Saver"

#: xsavers/pyro.cpp:528 xsavers/rock.cpp:560 xsavers/slidescreen.cpp:474
msgid "Number:"
msgstr "Number:"

#: xsavers/pyro.cpp:541
msgid "Cloud"
msgstr "Cloud"

#: xsavers/pyro.cpp:622
msgid ""
"Pyro Version 3.4\n"
"\n"
"Copyright (c) 1991 by Patrick J. Naughton\n"
"\n"
"Ported to kscreensave by Martin Jones."
msgstr ""
"Pyro Version 3.4\n"
"\n"
"Copyright (c) 1991 by Patrick J. Naughton\n"
"\n"
"Ported to kscreensave by Martin Jones."

#: xsavers/rock.cpp:535
msgid "Setup Rock Screen Saver"
msgstr "Setup Rock Screen Saver"

#: xsavers/rock.cpp:579
msgid "Rotate"
msgstr "Rotate"

#: xsavers/rock.cpp:690
msgid ""
"Rock Version 3.3\n"
"\n"
"Copyright (c) 1992 by Jamie Zawinski\n"
"\n"
"Ported to kscreensave by Martin Jones."
msgstr ""
"Rock Version 3.3\n"
"\n"
"Copyright (c) 1992 by Jamie Zawinski\n"
"\n"
"Ported to kscreensave by Martin Jones."

#: xsavers/slidescreen.cpp:461
msgid "Setup Slide Screen Saver"
msgstr "Setup Slide Screen Saver"

#: xsavers/slidescreen.cpp:493
msgid "Balls"
msgstr "Balls"

#: xsavers/slidescreen.cpp:494
msgid "Lines"
msgstr "Lines"

#: xsavers/slidescreen.cpp:495
msgid "Polygons"
msgstr "Polygons"

#: xsavers/slidescreen.cpp:496
msgid "Tails"
msgstr "Tails"

#: xsavers/slidescreen.cpp:509
msgid "Glow"
msgstr "Glow"

#: xsavers/slidescreen.cpp:554
msgid ""
"SlideScreen Version 1.1\n"
"\n"
"Copyright (c) 1992-1997 by Jamie Zawinski <jwz@jwz.org>\n"
"\n"
"Ported to kscreensaver by:\n"
"\n"
"Tom Vijlbrief <tom.vijlbrief@knoware.nl> oct 1998"
msgstr ""
"SlideScreen Version 1.1\n"
"\n"
"Copyright (c) 1992-1997 by Jamie Zawinski <jwz@jwz.org>\n"
"\n"
"Ported to kscreensaver by:\n"
"\n"
"Tom Vijlbrief <tom.vijlbrief@knoware.nl> oct 1998"

#: xsavers/space.cpp:586
msgid "Setup Space Screen Saver"
msgstr "Setup Space Screen Saver"

#: xsavers/space.cpp:610
msgid "Warp Interval:"
msgstr "Warp Interval:"

#: xsavers/space.cpp:686
msgid ""
"KSpace\n"
"Copyright (c) 1998\n"
"Bernd Johannes Wuebben <wuebben@kde.org>"
msgstr ""
"KSpace\n"
"Copyright (c) 1998\n"
"Bernd Johannes Wuebben <wuebben@kde.org>"

#: xsavers/swarm.cpp:322
msgid "Setup Swarm Screen Saver"
msgstr "Setup Swarm Screen Saver"

#: xsavers/swarm.cpp:348
msgid "Number of Bees:"
msgstr "Number of Bees:"

#: xsavers/swarm.cpp:438
msgid ""
"Swarm\n"
"\n"
"Copyright (c) 1991 by Patrick J. Naughton\n"
"\n"
"Ported to kscreensave by Emanuel Pirker."
msgstr ""
"Swarm\n"
"\n"
"Copyright (c) 1991 by Patrick J. Naughton\n"
"\n"
"Ported to kscreensave by Emanuel Pirker."

#: xsavers/swarm.cpp:439
msgid "About Swarm"
msgstr "About Swarm"
