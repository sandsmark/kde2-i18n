msgid ""
msgstr ""
"POT-Creation-Date: 2001-08-27 10:55+0200\n"
"PO-Revision-Date: 2001-07-24 00:56BST\n"
"Last-Translator: Malcolm Hunter <malcolm.hunter@gmx.co.uk>\n"
"Language-Team: UK English <en_GB>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: aboutwidget.cpp:39 main.cpp:80
msgid "KDE Control Center"
msgstr "KDE Control Centre"

#: aboutwidget.cpp:41
msgid "Configure your desktop environment."
msgstr "Configure your desktop environment."

#: aboutwidget.cpp:43
msgid ""
"Welcome to the \"KDE Control Center\", a central place to configure your "
"desktop environment. Select an item from the index on the left to load a "
"configuration module."
msgstr ""
"Welcome to the \"KDE Control Centre\", a central place to configure your "
"desktop environment. Select an item from the index on the left to load a "
"configuration module."

#: aboutwidget.cpp:49
msgid ""
"Click on the \"<b>Help</b>\" tab on the left to view help for the active "
"control module. Use the \"<b>Search</b>\" tab if you are unsure where to "
"look for a particular configuration option."
msgstr ""
"Click on the \"<b>Help</b>\" tab on the left to view help for the active "
"control module. Use the \"<b>Search</b>\" tab if you are unsure where to "
"look for a particular configuration option."

#: aboutwidget.cpp:55
msgid "KDE version:"
msgstr "KDE version:"

#: aboutwidget.cpp:56
msgid "User:"
msgstr "User:"

#: aboutwidget.cpp:57
msgid "Hostname:"
msgstr "Hostname:"

#: aboutwidget.cpp:58
msgid "System:"
msgstr "System:"

#: aboutwidget.cpp:59
msgid "Release:"
msgstr "Release:"

#: aboutwidget.cpp:60
msgid "Machine:"
msgstr "Machine:"

#: dockcontainer.cpp:43 modules.cpp:149
msgid "<big>Loading ...</big>"
msgstr "<big>Loading ...</big>"

#: dockcontainer.cpp:79 toplevel.cpp:326
msgid ""
"There are unsaved changes in the active module.\n"
"Do you want to apply the changes before running\n"
"the new module or forget the changes?"
msgstr ""
"There are unsaved changes in the active module.\n"
"Do you want to apply the changes before running\n"
"the new module or forget the changes?"

#: dockcontainer.cpp:84 toplevel.cpp:331
msgid ""
"There are unsaved changes in the active module.\n"
"Do you want to apply the changes before exiting\n"
"the Control Center or forget the changes?"
msgstr ""
"There are unsaved changes in the active module.\n"
"Do you want to apply the changes before exiting\n"
"the Control Centre or forget the changes?"

#: dockcontainer.cpp:89 toplevel.cpp:336
msgid "Unsaved changes"
msgstr "Unsaved changes"

#: dockcontainer.cpp:91 toplevel.cpp:338
msgid "&Forget"
msgstr "&Forget"

#: helpwidget.cpp:58
msgid "<br><br>To read the full manual click <a href=\"%1\">here</a>."
msgstr "<br><br>To read the full manual click <a href=\"%1\">here</a>."

#: helpwidget.cpp:64
msgid ""
"<h1>KDE Control Center</h1>Sorry, there is no quick help available for the "
"active control module.<br><br>Click <a href = "
"\"kcontrol/index.html\">here</a> to read the general control center manual."
msgstr ""
"<h1>KDE Control Centre</h1>Sorry, there is no quick help available for the "
"active control module.<br><br>Click <a href = "
"\"kcontrol/index.html\">here</a> to read the general Control Centre manual."

#: kcdialog.cpp:37 kecdialog.cpp:40 proxywidget.cpp:152
msgid "Use &Defaults"
msgstr "Use &Defaults"

#: kcminit.cpp:35 kcmshell.cpp:51
msgid "Configuration module to open."
msgstr "Configuration module to open."

#: kcminit.cpp:42
msgid "KCMInit"
msgstr "KCMInit"

#: kcminit.cpp:44
msgid "KCMInit - runs startups initialization for Control Modules."
msgstr "KCMInit - runs startups initialisation for Control Modules."

#: kcminit.cpp:78 kcmshell.cpp:79
msgid "Module %1 not found!"
msgstr "Module %1 not found!"

#: kcmshell.cpp:50
msgid "List all possible modules"
msgstr "List all possible modules"

#: kcmshell.cpp:52
msgid "Window ID to embed into."
msgstr "Window ID to embed into."

#: kcmshell.cpp:130
msgid "KDE Control Module"
msgstr "KDE Control Module"

#: kcmshell.cpp:132
msgid "A tool to start single KDE control modules"
msgstr "A tool to start single KDE control modules"

#: kcmshell.cpp:246
#, c-format
msgid ""
"There was an error loading the module.\n"
"The diagnostics is:\n"
"%1"
msgstr ""
"There was an error loading the module.\n"
"The diagnostics is:\n"
"%1"

#: kcrootonly.cpp:31
msgid ""
"<big>You need super user privileges to run this control "
"module.</big><br>Click on the \"Modify\" button below."
msgstr ""
"<big>You need super user privileges to run this control "
"module.</big><br>Click on the \"Modify\" button below."

#: kecdialog.cpp:39
msgid "Settings"
msgstr "Settings"

#: kecdialog.cpp:93
msgid ""
"There was an error loading module\n"
"'%1'\n"
"The diagnostics is:\n"
"%2"
msgstr ""
"There was an error loading module\n"
"'%1'\n"
"The diagnostics are:\n"
"%2"

#: main.cpp:81
msgid "The KDE Control Center"
msgstr "The KDE Control Centre"

#: main.cpp:82
msgid "(c) 1998-2000, The KDE Control Center Developers"
msgstr "(c) 1998-2000, The KDE Control Centre Developers"

#: moduleiconview.cpp:133
msgid "Go up"
msgstr "Go up"

#: moduletreeview.cpp:55
msgid "The %1 configuration group. Click to open it."
msgstr "The %1 configuration group. Click to open it."

#: moduletreeview.cpp:57
msgid ""
"This treeview displays all available control modules. Click on one of the "
"modules to receive more detailed information."
msgstr ""
"This treeview displays all available control modules. Click on one of the "
"modules to receive more detailed information."

#: proxywidget.cpp:49
msgid "The currently loaded configuration module."
msgstr "The currently loaded configuration module."

#: proxywidget.cpp:151 toplevel.cpp:119
msgid "Hel&p"
msgstr "Hel&p"

#: proxywidget.cpp:154
msgid "&Reset"
msgstr "&Reset"

#: rc.cpp:2
msgid "&Mode"
msgstr "&Mode"

#: rc.cpp:3
msgid "Icon &size"
msgstr "Icon &size"

#: searchwidget.cpp:90
msgid "Se&arch:"
msgstr "Se&arch:"

#: searchwidget.cpp:97
msgid "&Keywords:"
msgstr "&Keywords:"

#: searchwidget.cpp:104
msgid "&Results:"
msgstr "&Results:"

#: toplevel.cpp:98
msgid "Choose between Index, Search and Quick Help"
msgstr "Choose between Index, Search and Quick Help"

#: toplevel.cpp:104
msgid "&Index"
msgstr "&Index"

#: toplevel.cpp:115
msgid "S&earch"
msgstr "S&earch"

#: toplevel.cpp:203
msgid "&Icon View"
msgstr "&Icon View"

#: toplevel.cpp:208
msgid "&Tree View"
msgstr "&Tree View"

#: toplevel.cpp:213
msgid "&Small"
msgstr "&Small"

#: toplevel.cpp:218
msgid "&Medium"
msgstr "&Medium"

#: toplevel.cpp:223
msgid "&Large"
msgstr "&Large"

#: toplevel.cpp:227 toplevel.cpp:413
msgid "About the current Module"
msgstr "About the current Module"

#: toplevel.cpp:235 toplevel.cpp:300
msgid "&Report Bug..."
msgstr "&Report Bug..."

#: toplevel.cpp:302
msgid "Report Bug on Module %1..."
msgstr "Report Bug on Module %1..."

#: toplevel.cpp:407
#, c-format
msgid ""
"_: Help menu->about <modulename>\n"
"About %1"
msgstr "About %1"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Aston Clulow"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "clulow@upnaway.com"
