# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-05-24 12:49+0200\n"
"PO-Revision-Date: 2001-07-24 17:01GMT\n"
"Last-Translator: Dwayne Bailey <dwayne@obsidian.co.za>\n"
"Language-Team: British English\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.4\n"

#: main.cpp:30
msgid "The KDE CDROM Tool"
msgstr "The KDE CDROM Tool"

#: main.cpp:36
msgid "KAutoRun"
msgstr "KAutoRun"

#: setupdlg.cpp:37
msgid "Setup CD AutoRun"
msgstr "Setup CD AutoRun"

#: setupdlg.cpp:42
msgid "AutoRun"
msgstr "AutoRun"

#: setupdlg.cpp:46
msgid "Audio CDs"
msgstr "Audio CDs"

#: setupdlg.cpp:52
msgid "Play on Insert"
msgstr "Play on Insert"

#: setupdlg.cpp:60
msgid "Data CDs"
msgstr "Data CDs"

#: setupdlg.cpp:67
msgid "Open on Insert"
msgstr "Open on Insert"

#: setupdlg.cpp:77
msgid "Execute autorun.sh"
msgstr "Execute autorun.sh"

#: setupdlg.cpp:82
msgid "Ask before execute"
msgstr "Ask before execute"

#: setupdlg.cpp:88
msgid "Mixed Mode CDs"
msgstr "Mixed Mode CDs"

#: setupdlg.cpp:92
msgid "Treat like Audio CD"
msgstr "Treat like Audio CD"

#: setupdlg.cpp:93
msgid "Treat like Data CD"
msgstr "Treat like Data CD"

#: setupdlg.cpp:94
msgid "Ask"
msgstr "Ask"

#: setupdlg.cpp:95 toplevel.cpp:185
msgid "Ignore"
msgstr "Ignore"

#: setupdlg.cpp:97
msgid "CD Device"
msgstr "CD Device"

#: toplevel.cpp:57
msgid "Setup..."
msgstr "Setup..."

#: toplevel.cpp:58
msgid "About..."
msgstr "About..."

#: toplevel.cpp:147
msgid "Do you want to execute '%1' on the CD?"
msgstr "Do you want to execute '%1' on the CD?"

#: toplevel.cpp:148
#, c-format
msgid "Execute %1"
msgstr "Execute %1"

#: toplevel.cpp:183
msgid "Mixed mode CD"
msgstr "Mixed mode CD"

#: toplevel.cpp:184
msgid "How do you want to treat this mixed mode CD?"
msgstr "How do you want to treat this mixed mode CD?"

#: toplevel.cpp:185
msgid "Audio"
msgstr "Audio"

#: toplevel.cpp:185
msgid "Data"
msgstr "Data"

#: toplevel.cpp:258 toplevel.cpp:259
msgid "KDE CD AutoRun"
msgstr "KDE CD AutoRun"

#: toplevel.cpp:260
msgid "Author:"
msgstr "Author:"

#: _translatorinfo.cpp:1
msgid "_: NAME OF TRANSLATORS\nYour names"
msgstr "Dwayne Bailey"

#: _translatorinfo.cpp:3
msgid "_: EMAIL OF TRANSLATORS\nYour emails"
msgstr "dwayne@obsidian.co.za"
