# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 17:27+0200\n"
"PO-Revision-Date: 2001-07-07 19:48GMT\n"
"Last-Translator: John Knight <anarchist_tomato@herzeleid.net>\n"
"Language-Team: English Great Britain\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9\n"

#: gotodialog.cpp:134
msgid "Go to page"
msgstr "Go to page"

#: gotodialog.cpp:143
msgid "Section:"
msgstr "Section:"

#: gotodialog.cpp:150
msgid "Page:"
msgstr "Page:"

#: gotodialog.cpp:176 gotodialog.cpp:178 kgv_miniwidget.cpp:1006
#, c-format
msgid "of %1"
msgstr "of %1"

#: gotodialog.cpp:214
msgid "You must enter section and page numbers first."
msgstr "You must enter section and page numbers first."

#: gotodialog.cpp:225
msgid "Please enter a valid number first."
msgstr "Please enter a valid number first."

#: infodialog.cpp:34
msgid "Document information"
msgstr "Document information"

#: infodialog.cpp:42
msgid "File name:"
msgstr "File name:"

#: infodialog.cpp:47
msgid "Document title:"
msgstr "Document title:"

#: infodialog.cpp:52
msgid "Publication date:"
msgstr "Publication date:"

#: kgvconfigdialog.cpp:52
msgid "General"
msgstr "General"

#: kgvconfigdialog.cpp:53
msgid "General settings"
msgstr "General settings"

#: kgvconfigdialog.cpp:55
msgid "Antialiasing"
msgstr "Antialiasing"

#: kgvconfigdialog.cpp:56
msgid "Platform fonts"
msgstr "Platform fonts"

#: kgvconfigdialog.cpp:57
msgid "Messages"
msgstr "Messages"

#: kgvconfigdialog.cpp:58
msgid "Palette"
msgstr "Palette"

#: kgvconfigdialog.cpp:60
msgid "Monochrome"
msgstr "Monochrome"

#: kgvconfigdialog.cpp:61
msgid "Grayscale"
msgstr "Greyscale"

#: kgvconfigdialog.cpp:62
msgid "Color"
msgstr "Colour"

#: kgvconfigdialog.cpp:63
msgid "Backing"
msgstr "Backing"

#: kgvconfigdialog.cpp:65
msgid "Pixmap"
msgstr "Pixmap"

#: kgvconfigdialog.cpp:66
msgid "Backing store"
msgstr "Backing store"

#: kgvconfigdialog.cpp:85
msgid "Ghostscript"
msgstr "Ghostscript"

#: kgvconfigdialog.cpp:86
msgid "Ghostscript configuration"
msgstr "Ghostscript configuration"

#: kgvconfigdialog.cpp:88
msgid "Configure..."
msgstr "Configure..."

#: kgvconfigdialog.cpp:91
msgid "Settings"
msgstr "Settings"

#: kgvconfigdialog.cpp:93
msgid "Interpreter: "
msgstr "Interpreter: "

#: kgvconfigdialog.cpp:95
msgid "Non-antialiasing arguments: "
msgstr "Non-antialiasing arguments: "

#: kgvconfigdialog.cpp:97
msgid "Antialiasing arguments: "
msgstr "Antialiasing arguments: "

#: kgv_miniwidget.cpp:119 kgv_miniwidget.cpp:211
#, c-format
msgid "Could not create temporary file: %1"
msgstr "Could not create temporary file: %1"

#: kgv_miniwidget.cpp:136
#, c-format
msgid "Could not open standard input stream: %1"
msgstr "Could not open standard input stream: %1"

#: kgv_miniwidget.cpp:147
msgid "File <nobr><strong>%1</strong></nobr> doesn't exist"
msgstr "File <nobr><strong>%1</strong></nobr> doesn't exist"

#: kgv_miniwidget.cpp:161
#, c-format
msgid "Could not create temporary file: %2"
msgstr "Could not create temporary file: %2"

#: kgv_miniwidget.cpp:171 kgv_miniwidget.cpp:192
msgid "Could not unzip <nobr><strong>%1</strong></nobr>"
msgstr "Could not unzip <nobr><strong>%1</strong></nobr>"

#: kgv_miniwidget.cpp:218 kgv_miniwidget.cpp:245
msgid "Error opening file <nobr><strong>%1</strong></nobr>: %2"
msgstr "Error opening file <nobr><strong>%1</strong></nobr>: %2"

#: kgv_miniwidget.cpp:233
msgid ""
"Could not open file <nobr><strong>%1</strong></nobr> which has type "
"<strong>%2</strong>. KGhostview can only load Postscript (.ps, .eps) and "
"Portable Document Format (.pdf) files."
msgstr ""
"Could not open file <nobr><strong>%1</strong></nobr> which has type "
"<strong>%2</strong>. KGhostview can only load Postscript (.ps, .eps) and "
"Portable Document Format (.pdf) files."

#: kgv_miniwidget.cpp:272
msgid "Not known"
msgstr "Not known"

#: kgv_miniwidget.cpp:505
msgid ""
"Printing failed because the list of\n"
"pages to be printed was empty.\n"
msgstr ""
"Printing failed because the list of\n"
"pages to be printed was empty.\n"

#: kgv_miniwidget.cpp:507
msgid "Error printing"
msgstr "Error printing"

#: kgv_miniwidget.cpp:1000
msgid "of 1    "
msgstr "of 1    "

#: kgv_miniwidget.cpp:1002
msgid "of %1    "
msgstr "of %1    "

#: kgv_miniwidget.cpp:1004
msgid "of %1  "
msgstr "of %1  "

#: kgv_miniwidget.cpp:1063
msgid "Could not create temporary file: "
msgstr "Could not create temporary file: "

#: kgvshell.cpp:44
msgid "&Fit To Page Width"
msgstr "&Fit To Page Width"

#: kgvshell.cpp:146
msgid ""
"*.ps *.ps.gz *.eps *.eps.gz *.pdf|All document files\n"
"*.ps *.ps.gz|Postscript files\n"
"*.eps *.eps.gz|Encapsulated Postscript files\n"
"*.pdf|Portable Document Format files"
msgstr ""
"*.ps *.ps.gz *.eps *.eps.gz *.pdf|All document files\n"
"*.ps *.ps.gz|Postscript files\n"
"*.eps *.eps.gz|Encapsulated Postscript files\n"
"*.pdf|Portable Document Format files"

#: kgv_view.cpp:143
msgid "Document &Info..."
msgstr "Document &Info..."

#: kgv_view.cpp:150
msgid "Auto"
msgstr "Auto"

#: kgv_view.cpp:153
msgid "Upside Down"
msgstr "Upside Down"

#: kgv_view.cpp:154 viewcontrol.cpp:75
msgid "Seascape"
msgstr "Seascape"

#: kgv_view.cpp:156 viewcontrol.cpp:105
msgid "&Orientation"
msgstr "&Orientation"

#: kgv_view.cpp:163
msgid "Paper &Size"
msgstr "Paper &Size"

#: kgv_view.cpp:180
msgid "Go to start"
msgstr "Go to start"

#: kgv_view.cpp:184
msgid "Go to end"
msgstr "Go to end"

#: kgv_view.cpp:188
msgid "Read up document"
msgstr "Read up document"

#: kgv_view.cpp:193
msgid "Read down document"
msgstr "Read down document"

#: kgv_view.cpp:215 marklist.cpp:43
msgid "Mark Current Page"
msgstr "Mark Current Page"

#: kgv_view.cpp:219 marklist.cpp:45
msgid "Mark &All Pages"
msgstr "Mark &All Pages"

#: kgv_view.cpp:223 marklist.cpp:47
msgid "Mark &Even Pages"
msgstr "Mark &Even Pages"

#: kgv_view.cpp:227 marklist.cpp:49
msgid "Mark &Odd Pages"
msgstr "Mark &Odd Pages"

#: kgv_view.cpp:231 marklist.cpp:51
msgid "&Toggle Page Marks"
msgstr "&Toggle Page Marks"

#: kgv_view.cpp:235 marklist.cpp:53
msgid "&Remove Page Marks"
msgstr "&Remove Page Marks"

#: kgv_view.cpp:244
msgid "Show &Scrollbars"
msgstr "Show &Scrollbars"

#: kgv_view.cpp:248
msgid "&Watch File"
msgstr "&Watch File"

#: kgv_view.cpp:252
msgid "Show &Page List"
msgstr "Show &Page List"

#: kgv_view.cpp:256
msgid "Show Page Names"
msgstr "Show Page Names"

#: kgv_view.cpp:261
msgid "&Configure KGhostView..."
msgstr "&Configure KGhostView..."

#: kgv_view.cpp:418
msgid "Auto "
msgstr "Auto "

#: main.cpp:14
msgid " Files to load"
msgstr " Files to load"

#: main.cpp:18
msgid "Viewer for Postscript (.ps, .eps) and Portable Document Format (.pdf) files."
msgstr "Viewer for Postscript (.ps, .eps) and Portable Document Format (.pdf) files."

#: main.cpp:23
msgid "KGhostView"
msgstr "KGhostView"

#: main.cpp:27
msgid ""
"KGhostView displays, prints, and saves Postscript and PDF files.\n"
"Based on original work by Tim Theisen."
msgstr ""
"KGhostView displays, prints, and saves Postscript and PDF files.\n"
"Based on original work by Tim Theisen."

#: main.cpp:31
msgid "Current maintainer"
msgstr "Current maintainer"

#: main.cpp:34
msgid "Maintainer 1999-2000"
msgstr "Maintainer 1999-2000"

#: main.cpp:38
msgid "Original author"
msgstr "Original author"

#: main.cpp:40
msgid "Basis for shell"
msgstr "Basis for shell"

#: main.cpp:42
msgid "Port to KParts"
msgstr "Port to KParts"

#: main.cpp:44
msgid "Dialog boxes"
msgstr "Dialogue boxes"

#: marklist.cpp:56
msgid "&Page Marks"
msgstr "&Page Marks"

#: messages.cpp:30
msgid "Ghostscript messages"
msgstr "Ghostscript messages"

#: messages.cpp:57
msgid "&Clear"
msgstr "&Clear"

#: messages.cpp:60
msgid "&Dismiss"
msgstr "&Dismiss"

#: rc.cpp:3
msgid "&PageMarks"
msgstr "&PageMarks"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "John Knight"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "anarchist_tomato@herzeleid.net"

#: viewcontrol.cpp:76
msgid "Upside down"
msgstr "Upside down"

#: viewcontrol.cpp:86
msgid "&Magnification"
msgstr "&Magnification"

#: viewcontrol.cpp:97
msgid "M&edia"
msgstr "M&edia"
