msgid ""
msgstr ""
"PO-Revision-Date: 2001-07-13 16:01GMT\n"
"Last-Translator: Dwayne Bailey <dwayne@obsidian.co.za>\n"
"Language-Team: British English\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"Date: 1998-05-01 20:02:19+0200\n"
"From: Martin Haefner <mh@jeff_clever>\n"
"Xgettext-Options: -C -ki18n\n"
"Files: appearance.cc configtab.cc exitwidget.cc kcmdlineargs.cc kfdird.cc "
"linkdirselect.cc main.cc mainwidget.cc mywidget.cc panelwindow.cc "
"sessiondata.cc startupwidget.cc\n"
"X-Generator: KBabel 0.9.2\n"

#: appearance.cc:37
msgid "Window"
msgstr "Window"

#: appearance.cc:45
msgid "Menus"
msgstr "Menus"

#: appearance.cc:52
msgid "Minimum Window"
msgstr "Minimum Window"

#: appearance.cc:60
msgid "Standard Window"
msgstr "Standard Window"

#: appearance.cc:68
msgid "Full Window"
msgstr "Full Window"

#: appearance.cc:76
msgid "Show Menubar"
msgstr "Show Menubar"

#: appearance.cc:84
msgid "Show Buttonbar"
msgstr "Show Buttonbar"

#: configtab.cc:31
msgid "Configure KTelnet"
msgstr "Configure KTelnet"

#: configtab.cc:38
msgid "Appearance"
msgstr "Appearance"

#: configtab.cc:39 startupwidget.cc:39
msgid "StartUp"
msgstr "StartUp"

#: configtab.cc:40
msgid "On Exit"
msgstr "On Exit"

#: configtab.cc:44 kfdird.cc:52 linkdirselect.cc:90 mainwidget.cc:154
msgid "Cancel"
msgstr "Cancel"

#: configtab.cc:45
msgid "Apply"
msgstr "Apply"

#: exitwidget.cc:38
msgid "Exit"
msgstr "Exit"

#: exitwidget.cc:45
msgid "Exit on Connect"
msgstr "Exit on Connect"

#: exitwidget.cc:53
msgid "AutoSave options on exit"
msgstr "AutoSave options on exit"

#: exitwidget.cc:59
msgid "AutoSave sessions on exit"
msgstr "AutoSave sessions on exit"

#: exitwidget.cc:67
msgid "X Hosts"
msgstr "X Hosts"

#: exitwidget.cc:72
msgid "Call xhost for remote"
msgstr "Call xhost for remote"

#: kfdird.cc:50
msgid "Directories:"
msgstr "Directories:"

#: kfdird.cc:51 linkdirselect.cc:82
msgid "OK"
msgstr "OK"

#: kfdird.cc:171 kfdird.cc:196 kfdird.cc:214
msgid "Sorry"
msgstr "Sorry"

#: kfdird.cc:172 kfdird.cc:197 kfdird.cc:215
msgid "Cannot open or read directory."
msgstr "Cannot open or read directory."

#: linkdirselect.cc:35
msgid "Create KDE Link"
msgstr "Create KDE Link"

#: linkdirselect.cc:42
msgid "Save to location"
msgstr "Save to location"

#: linkdirselect.cc:49
msgid "Desktop"
msgstr "Desktop"

#: linkdirselect.cc:57
msgid "Personal application link directory"
msgstr "Personal application link directory"

#: linkdirselect.cc:65
msgid "User defined directory"
msgstr "User defined directory"

#: linkdirselect.cc:73
msgid "Browse ..."
msgstr "Browse ..."

#: linkdirselect.cc:76
msgid "Click to select a directory"
msgstr "Click to select a directory"

#: main.cc:106 sessiondata.cc:38 sessiondata.cc:74 sessiondata.cc:488 sessiondata.cc:500 sessiondata.cc:512
msgid "Warning"
msgstr "Warning"

#: main.cc:107
msgid "Your sessions have changed but are not saved yet.\nSave now?"
msgstr ""
"Your sessions have changed but are not saved yet.\n"
"Save now?"

#: main.cc:108 mywidget.cc:150 sessiondata.cc:74
msgid "Yes"
msgstr "Yes"

#: main.cc:108 mywidget.cc:151 sessiondata.cc:74
msgid "No"
msgstr "No"

#: mainwidget.cc:35
msgid "Session:"
msgstr "Session:"

#: mainwidget.cc:36
msgid "Account:"
msgstr "Account:"

#: mainwidget.cc:37
msgid "Host:"
msgstr "Host:"

#: mainwidget.cc:40
msgid "Further Options:"
msgstr "Further Options:"

#: mainwidget.cc:60
msgid "Select session, right\nmousebutton to change name"
msgstr ""
"Select session, right\n"
"mousebutton to change name"

#: mainwidget.cc:65 mainwidget.cc:479
msgid "Enter new session name"
msgstr "Enter new session name"

#: mainwidget.cc:75
msgid "Enter remote host"
msgstr "Enter remote host"

#: mainwidget.cc:81
msgid "Enter account to log in"
msgstr "Enter account to log in"

#: mainwidget.cc:85
msgid "Terminal type:"
msgstr "Terminal type:"

#: mainwidget.cc:86
msgid "Connect/Port:"
msgstr "Connect/Port:"

#: mainwidget.cc:87
msgid "Textsize:"
msgstr "Textsize:"

#: mainwidget.cc:88
msgid "Background Color:"
msgstr "Background Colour:"

#: mainwidget.cc:89
msgid "Foreground Color:"
msgstr "Foreground Colour:"

#: mainwidget.cc:92
msgid "Toolbar Icon:"
msgstr "Toolbar Icon:"

#: mainwidget.cc:97
msgid "Change Terminal"
msgstr "Change Terminal"

#: mainwidget.cc:103
msgid "Tiny - 6x13"
msgstr "Tiny - 6x13"

#: mainwidget.cc:104
msgid "Small - 7x14"
msgstr "Small - 7x14"

#: mainwidget.cc:105
msgid "Medium - 8x13"
msgstr "Medium - 8x13"

#: mainwidget.cc:106
msgid "Large - 9x15"
msgstr "Large - 9x15"

#: mainwidget.cc:107
msgid "Huge - 10x20"
msgstr "Huge - 10x20"

#: mainwidget.cc:108
msgid "Change terminal textsize"
msgstr "Change terminal textsize"

#: mainwidget.cc:115
msgid "Change connection program"
msgstr "Change connection program"

#: mainwidget.cc:119
msgid "Set port number, Telnet default=23\nSecure Shell default=22\nSet to \"0\" if not shure"
msgstr ""
"Set port number, Telnet default=23\n"
"Secure Shell default=22\n"
"Set to \"0\" if not sure"

#: mainwidget.cc:123
msgid "Click to change background color"
msgstr "Click to change background colour"

#: mainwidget.cc:126
msgid "Click to change foreground color"
msgstr "Click to change foreground colour"

#: mainwidget.cc:133
msgid "Enter additional terminal options here"
msgstr "Enter additional terminal options here"

#: mainwidget.cc:141 mainwidget.cc:438 mainwidget.cc:581 mainwidget.cc:730 mainwidget.cc:737 mainwidget.cc:765
msgid "Add to toolbar"
msgstr "Add to toolbar"

#: mainwidget.cc:144
msgid "Click to add/remove\nsession to toolbar"
msgstr ""
"Click to add/remove\n"
"session to toolbar"

#: mainwidget.cc:147
msgid "Accept"
msgstr "Accept"

#: mainwidget.cc:149
msgid "Accept the new session"
msgstr "Accept the new session"

#: mainwidget.cc:157
msgid "Cancel entering a new session"
msgstr "Cancel entering a new session"

#: mainwidget.cc:164
msgid "Connect"
msgstr "Connect"

#: mainwidget.cc:167
msgid "Start the remote session"
msgstr "Start the remote session"

#: mainwidget.cc:179
msgid "Lower Window Size"
msgstr "Lower Window Size"

#: mainwidget.cc:180
msgid "Raise Window Size"
msgstr "Raise Window Size"

#: mainwidget.cc:459
msgid "Enter new session name.\nPress RETURN to finish."
msgstr ""
"Enter new session name.\n"
"Press RETURN to finish."

#: mainwidget.cc:580 mainwidget.cc:732
msgid "Remove"
msgstr "Remove"

#: mywidget.cc:140 sessiondata.cc:128
msgid "Error"
msgstr "Error"

#: mywidget.cc:141
msgid "You need at least 1 session!"
msgstr "You need at least 1 session!"

#: mywidget.cc:145
msgid "You attempt to delete session \""
msgstr "You attempt to delete session \""

#: mywidget.cc:147
msgid "\"!\n\nContinue deleting?"
msgstr ""
"\"!\n"
"\n"
"Continue deleting?"

#: mywidget.cc:149 mywidget.cc:355
msgid "Delete session"
msgstr "Delete session"

#: mywidget.cc:171
msgid "Show &menubar"
msgstr "Show &menubar"

#: mywidget.cc:186 mywidget.cc:396
msgid "Hide &menubar"
msgstr "Hide &menubar"

#: mywidget.cc:201
msgid "Show &toolbar"
msgstr "Show &toolbar"

#: mywidget.cc:216 mywidget.cc:397
msgid "Hide &toolbar"
msgstr "Hide &toolbar"

#: mywidget.cc:348
msgid "Exit the remote login manager"
msgstr "Exit the remote login manager"

#: mywidget.cc:352
msgid "Define new session"
msgstr "Define new session"

#: mywidget.cc:359
msgid "Save sessions"
msgstr "Save sessions"

#: mywidget.cc:364
msgid "Panelize the program"
msgstr "Panelise the program"

#: mywidget.cc:367
msgid "Get Help"
msgstr "Get Help"

#: mywidget.cc:399
msgid "On &panel"
msgstr "On &panel"

#: mywidget.cc:402 mywidget.cc:441
msgid "&Options"
msgstr "&Options"

#: mywidget.cc:403
msgid "Sa&ve options"
msgstr "Sa&ve options"

#: mywidget.cc:407
msgid "&New Session"
msgstr "&New Session"

#: mywidget.cc:408
msgid "&Delete Session"
msgstr "&Delete Session"

#: mywidget.cc:410
msgid "&Save Sessions"
msgstr "&Save Sessions"

#: mywidget.cc:413
msgid "&Create kdelnk"
msgstr "&Create kdelnk"

#: mywidget.cc:415
msgid "E&xit"
msgstr "E&xit"

#: mywidget.cc:424
msgid "     formerly Telly"
msgstr "     formerly Telly"

#: mywidget.cc:426
msgid "\n\n(c) 1997/98 by\nSpaghetti Code Computing"
msgstr ""
"\n"
"\n"
"(c) 1997/98 by\n"
"Spaghetti Code Computing"

#: mywidget.cc:428
msgid "\n\nWritten by Martin Häfner and Cristian Tibirna"
msgstr ""
"\n"
"\n"
"Written by Martin Häfner and Cristian Tibirna"

#: mywidget.cc:430
msgid "\n\nEmail: mh@ap-dec717c.physik.uni-karlsruhe.de"
msgstr ""
"\n"
"\n"
"Email: mh@ap-dec717c.physik.uni-karlsruhe.de"

#: mywidget.cc:431
msgid "\nHomepage: http://ap-dec717c.physik.uni-karlsruhe.de/~mh/ktelnet"
msgstr ""
"\n"
"Homepage: http://ap-dec717c.physik.uni-karlsruhe.de/~mh/ktelnet"

#: mywidget.cc:432
msgid "\n\nThanks to all translators!"
msgstr ""
"\n"
"\n"
"Thanks to all translators!"

#: mywidget.cc:440
msgid "&File"
msgstr "&File"

#: mywidget.cc:443
msgid "&Help"
msgstr "&Help"

#: panelwindow.cc:39
msgid "Click left to start session\nClick right to get mainwindow"
msgstr ""
"Click left to start session\n"
"Click right to get mainwindow"

#: sessiondata.cc:39
msgid "Something went wrong reading the global resource file! Could not find any terminals. Stop!"
msgstr ""
"Something went wrong reading the global resource file! Could not find any "
"terminals. Stop!"

#: sessiondata.cc:74
msgid "File already exists!\nOverwrite it?"
msgstr ""
"File already exists!\n"
"Overwrite it?"

#: sessiondata.cc:105
msgid "Connects to "
msgstr "Connects to "

#: sessiondata.cc:113
msgid "Opens Terminal"
msgstr "Opens Terminal"

#: sessiondata.cc:121
msgid "Information"
msgstr "Information"

#: sessiondata.cc:122
msgid "Saved file  "
msgstr "Saved file  "

#: sessiondata.cc:124
msgid "Perhaps you need to resfresh your desktop\nor panel to display the KDElnk icon."
msgstr ""
"Perhaps you need to resfresh your desktop\n"
"or panel to display the KDElnk icon."

#: sessiondata.cc:129
msgid "Could not open file!\nMaybe you don't have write permissions."
msgstr ""
"Could not open file!\n"
"Maybe you don't have write permissions."

#: sessiondata.cc:391
msgid "ordinaryTerminal"
msgstr "ordinaryTerminal"

#: sessiondata.cc:489
msgid "Nothing to append!"
msgstr "Nothing to append!"

#: sessiondata.cc:501
msgid "That doesn't seem to be a real account!"
msgstr "That doesn't seem to be a real account!"

#: sessiondata.cc:513
msgid "No 2 sessions with same name!"
msgstr "No 2 sessions with same name!"

#: startupwidget.cc:46
msgid "Start on panel"
msgstr "Start on panel"
