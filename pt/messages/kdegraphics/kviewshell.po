msgid ""
msgstr ""
"Project-Id-Version: kviewshell\n"
"POT-Creation-Date: 2001-06-12 14:37+0200\n"
"PO-Revision-Date: 2001-09-01 21:39+0100\n"
"Last-Translator: José Carlos Monteiro <jcm@netcabo.pt>\n"
"Language-Team: Portuguese <kde@poli.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: none\n"
"X-Spell-Extra: KGhostView KParts KViewShell\n"

#: gotodialog.cpp:36
msgid "Go to page"
msgstr "Ir para a página"

#: gotodialog.cpp:42
msgid "Enter page number:"
msgstr "Número da página:"

#: gotodialog.cpp:77
msgid "You must enter a page number first."
msgstr "Tens que indicar primeiro o número da página."

#: gotodialog.cpp:86
msgid "You must enter a valid number"
msgstr "Tens que indicar um número válido"

#: kmultipage.cpp:55
msgid "Save File As"
msgstr "Gravar Ficheiro Como"

#: kmultipage.cpp:66
msgid ""
"The file %1\n"
"exists. Shall I overwrite that file?"
msgstr ""
"O ficheiro %1 existe.\n"
"Devo escrever por cima do ficheiro?"

#: kmultipage.cpp:67
msgid "Overwrite file"
msgstr "Escrever por cima"

#: kviewpart.cpp:145
msgid "Show &Page List"
msgstr "Mostrar a Lista de &Páginas"

#: kviewpart.cpp:146
msgid "Show P&review"
msgstr "Mostra&r uma Antevisão"

#: kviewpart.cpp:147
msgid "&Watch file"
msgstr "&Vigiar ficheiro"

#: kviewpart.cpp:153
msgid "&Orientation"
msgstr "&Orientação"

#: kviewpart.cpp:157
msgid "Paper &Size"
msgstr "&Tamanho do Papel"

#: kviewpart.cpp:181
msgid "Read down document"
msgstr "Ler documento para baixo"

#: kviewpart.cpp:206
msgid "Scroll Up"
msgstr "Subir"

#: kviewpart.cpp:207
msgid "Scroll Down"
msgstr "Descer"

#: kviewpart.cpp:208
msgid "Scroll Left"
msgstr "Esquerda"

#: kviewpart.cpp:209
msgid "Scroll Right"
msgstr "Direita"

#: kviewpart.cpp:211
msgid "Scroll Up Page"
msgstr "Subir Página"

#: kviewpart.cpp:212
msgid "Scroll Down Page"
msgstr "Descer Página"

#: kviewpart.cpp:213
msgid "Scroll Left Page"
msgstr "Página para a Esquerda"

#: kviewpart.cpp:214
msgid "Scroll Right Page"
msgstr "Página para a Direita"

#: kviewpart.cpp:293
msgid "A3"
msgstr "A3"

#: kviewpart.cpp:294
msgid "A4"
msgstr "A4"

#: kviewpart.cpp:295
msgid "A5"
msgstr "A5"

#: kviewpart.cpp:296
msgid "Letter"
msgstr "Carta"

#: kviewpart.cpp:297
msgid "Legal"
msgstr "Legal"

#: kviewpart.cpp:298
msgid "Other..."
msgstr "Outro..."

#: kviewpart.cpp:831
msgid "Page %1 of %2"
msgstr "Página %1 de %2"

#: main.cpp:15
msgid "The part to use"
msgstr "A parte a usar"

#: main.cpp:16
msgid "Files to load"
msgstr "Ficheiro a ler"

#: main.cpp:21
msgid "Generic framework for viewer applications."
msgstr "Moldura genérica para aplicações de visualização."

#: main.cpp:26
msgid "KViewShell"
msgstr "KViewShell"

#: main.cpp:30
msgid "Displays various document formats. Based on original code from KGhostView."
msgstr ""
"Mostra vários formatos de documentos. Baseado em código original do "
"KGhostView."

#: main.cpp:32
msgid "Framework"
msgstr "Estrutura"

#: main.cpp:35
msgid "KGhostView Maintainer"
msgstr "Manutenção do KGhostView"

#: main.cpp:39
msgid "KGhostView Author"
msgstr "Autor do KGhostView"

#: main.cpp:41
msgid "Navigation widgets"
msgstr "Elementos de navegação"

#: main.cpp:43
msgid "Basis for shell"
msgstr "Base para a 'shell'"

#: main.cpp:45
msgid "Port to KParts"
msgstr "Porte para KParts"

#: main.cpp:47
msgid "Dialogs boxes"
msgstr "Caixas de diálogo"

#: marklist.cpp:42
msgid "Mark current page"
msgstr "Marcar página actual"

#: marklist.cpp:43
msgid "Mark all pages"
msgstr "Marcar todas as páginas"

#: marklist.cpp:44
msgid "Mark even pages"
msgstr "Marcar páginas pares"

#: marklist.cpp:45
msgid "Mark odd pages"
msgstr "Marcar páginas ímpares"

#: marklist.cpp:46
msgid "Toggle page marks"
msgstr "Trocar marcações de página"

#: marklist.cpp:47
msgid "Remove page marks"
msgstr "Remover marcações de página"

#: rc.cpp:3
msgid "&Go"
msgstr "&Ir"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""
"José Nuno Pires\n"
"José Carlos Monteiro"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""
"jncp@rnl.ist.utl.pt\n"
"jcm@netcabo.pt"
