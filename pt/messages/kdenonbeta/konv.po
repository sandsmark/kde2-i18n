msgid ""
msgstr ""
"Project-Id-Version: konv\n"
"POT-Creation-Date: 2001-07-29 19:34+0200\n"
"PO-Revision-Date: 2000-09-09 19:11+01:00\n"
"Last-Translator: Pedro Morais <morais@kde.org>\n"
"Language-Team: pt <kde-list@poli.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: none\n"

#: main.cpp:8
msgid "KDE Unit Converter"
msgstr "Conversor de Unidades"

#: main.cpp:13
msgid "Konv"
msgstr "Konv"

#: main.cpp:16
msgid "Developer"
msgstr "Desenvolvimento"

#: distances.cpp:4
msgid "Meters (m)"
msgstr "Metros (m)"

#: distances.cpp:5
msgid "Centimeters (cm)"
msgstr "Centímetros (cm)"

#: distances.cpp:6
msgid "Angstroms (A)"
msgstr "Angstroms (A)"

#: distances.cpp:7
msgid "Inches (in)"
msgstr "Polegadas (in)"

#: distances.cpp:8
msgid "Feet (ft)"
msgstr "Pés (ft)"

#: distances.cpp:9
msgid "Fermats (F)"
msgstr "Fermats (F)"

#: distances.cpp:10
msgid "Lightyears (ly)"
msgstr "Anos-luz (ly)"

#: distances.cpp:11
msgid "Parsecs (pc)"
msgstr "Parsecs (pc)"

#: distances.cpp:12
msgid "Astronomical Unit (au)"
msgstr "Unidade Astronómica (au)"

#: distances.cpp:13
msgid "Nautical Mile"
msgstr "Milha Náutica"

#: distances.cpp:14
msgid "Mile (mi)"
msgstr "Milha (mi)"

#: distances.cpp:18
msgid ""
"A meter is equivalent to roughly 1/10,000,000th the distance between the "
"equator and the North Pole, passing through Lyons, France.  A meter was "
"redefined as 1650763.73 times the wavelength of light emitted from "
"krypton-86.  1982 saw another update: A meter is now 1/299,692,458th the "
"distance light travels in a vacuum, in one second."
msgstr ""
"Um metro equivale a cerca de 1/10 000 000 da distância entre o equador e\n"
"o Pólo Norte, passando por Lyon, França. Um metro foi redefinido como\n"
"1650763.73 vezes o comprimento de onda da luz emitida do kripton-86. Houve\n"
"outra actualização em 1982: um metro é agora 1/299 692 458 da distância\n"
"que a luz percorre no vácuo num segundo."

#: distances.cpp:20
msgid "The 'A' is an 'A' with a small circle above.  It is commonly used in Swedish."
msgstr ""
"O 'A' é um 'A' com um pequeno círculo por cima. É frequentemente "
"utilizado na Suécia."

#: distances.cpp:24
msgid ""
"A lightyear is the distance light travels in a year.  The speed of light is "
"299,792,458 meters/second or 186,000 miles/second."
msgstr ""
"Um ano-luz é a distância que a luz percorre num ano. A velocidade da luz "
"é 299 792 458 Metro/Segundo ou 186 000 Milha/Segundo."

#: distances.cpp:26
msgid "An astronomical unit is the mean distance between the Earth and the Sun."
msgstr ""
"Uma medida astronómica que representa a distância média entre a Terra e o "
"Sol."

#: distances.cpp:27
msgid ""
"A nautical mile is often used at sea, defined as one minute of the mean "
"latitude on earth.  This makes perfect sense at sea, where it's possible to "
"measure latitude, but not distance."
msgstr ""
"Uma milha náutica é muito usada no mar, definida como um minuto da "
"latitude\n"
"média da Terra. Faz perfeito sentido no mar, onde é possível medir a "
"latitude,\n"
"mas não a distância."

#: distances.cpp:28
msgid ""
"Also known as the statute mile.  This word comes from the latin phrase "
"\"millia passuum\", or \"thousand paces\"-- the distance a Roman legion "
"could travel in a thousand paces (two steps)."
msgstr ""
"Também conhecida como a milha estadual. Esta palavra vem da frase latina\n"
"\"millia passuum\", ou \"mil paces\"-- a distância que uma legião Romana\n"
"podia viajar com mil \"paces\" (dois passos)."

#: konv.cpp:46
msgid "Common &Prefixes"
msgstr "&Prefixos Comuns"

#: konvtabview.cpp:13
msgid "&Go"
msgstr "&Converter"

#: konvtabview.cpp:20
msgid "Type"
msgstr "Tipo"

#: konvtabview.cpp:21 prefixes.cpp:11
msgid "Value"
msgstr "Valor"

#: konvtabview.cpp:118
msgid "&About Unit"
msgstr "&Acerca da Unidade"

#: konvview.cpp:23
msgid "&Distance"
msgstr "&Distância"

#: konvview.cpp:27
msgid "&Mass"
msgstr "&Massa"

#: konvview.cpp:31
msgid "&Velocity"
msgstr "&Velocidade"

#: konvview.cpp:35
msgid "&Time"
msgstr "&Tempo"

#: times.cpp:4
msgid "Picoseconds(ps)"
msgstr "Picosegundos (ps)"

#: times.cpp:5
msgid "Nanoseconds (ns)"
msgstr "Nanosegundos (ns)"

#: times.cpp:6
msgid "Microseconds (µs)"
msgstr "Microsegundos (µs)"

#: times.cpp:7
msgid "Milliseconds (ms)"
msgstr "Milisegundos (ms)"

#: times.cpp:8
msgid "Seconds (s)"
msgstr "Segundos (s)"

#: times.cpp:9
msgid "Minutes (m)"
msgstr "Minutos (m)"

#: times.cpp:10
msgid "Hours (h)"
msgstr "Horas (h)"

#: times.cpp:11
msgid "Days (d)"
msgstr "Dias (d)"

#: times.cpp:12
msgid "Weeks (w)"
msgstr "Semanas (w)"

#: times.cpp:13
msgid "Years (y)"
msgstr "Anos (y)"

#: times.cpp:26
msgid ""
"The Ancient Egyptians believed it was 360 days long, resulting in there "
"being 360 degrees in a circle.  For the sake of Konv, a day is considered "
"365.25637 days. (A sidereal year)"
msgstr ""
"O Antigos Egípcios acreditavam que tinha 360 dias, donde resultou a facto "
"do círculo ter 360 graus. Para o Konv, uma ano é considerado como "
"365.25637 dias (um ano sideral)"

#: mass.cpp:4
msgid "Atomic Mass Units (u)"
msgstr "Unidades de Massa Atómica (u)"

#: mass.cpp:5
msgid "Nanograms (ng)"
msgstr "Nanogramas (ng)"

#: mass.cpp:6
msgid "Micrograms (µg)"
msgstr "Microgramas (µg)"

#: mass.cpp:7
msgid "Milligrams (ms)"
msgstr "Miligramas (ms)"

#: mass.cpp:8
msgid "Grams (g)"
msgstr "Gramas (g)"

#: mass.cpp:9
msgid "Kilograms (kg)"
msgstr "Quilogramas (kg)"

#: mass.cpp:10
msgid "Newtons (N)"
msgstr "Newtons (N)"

#: mass.cpp:11
msgid "Slugs"
msgstr "Slugs"

#: mass.cpp:12
msgid "Pounds (lb)"
msgstr "Libras (lb)"

#: mass.cpp:13
msgid "Stone (st)"
msgstr "Pedras (st)"

#: mass.cpp:14
msgid "Tons (t)"
msgstr "Toneladas (t)"

#: mass.cpp:15
msgid "Metric Tons (t)"
msgstr "Toneladas Métricas (m)"

#: mass.cpp:16
msgid "Protons"
msgstr "Protões"

#: mass.cpp:17
msgid "Electrons"
msgstr "Electrões"

#: mass.cpp:22
msgid ""
"This is given as multiples of the mass of electrons, which are much less "
"massive than protons.  Electrons are negatively charged."
msgstr ""
"Dado como múltiplos da massa do electrão, que têm muito menos massa do "
"que os protões. Os electrões estão carregados negativamente."

#: mass.cpp:23
msgid ""
"An Atomic Mass Unit is equal to about 1/12th the mass of a Carbon-12 "
"nucleus, or about that of a proton."
msgstr ""
"Uma Unidade de Massa Atómica é igual a cerca de 1/12 da massa de um "
"núcleo de Carbono-12, ou aproximadamente a massa de um protão."

#: mass.cpp:24
msgid ""
"This result is given as multiples of the mass of a proton. A proton has a "
"significantly larger mass than an electron.  Protons are positively charged. "
"These are also very close to Atomic Mass Units."
msgstr ""
"Este resultado é dado em múltiplos da massa dum Protão. Um protão tem "
"uma\n"
"massa substancialmente maior que um electrão. Os protões são carregados\n"
"positivamente. É um valor bastante próximo das Unidades de Massa Atómica."

#: mass.cpp:30
msgid ""
"The weight of something under the effect of the gravity of Earth in "
"Washington DC, at sea level (9.801m/s^2)."
msgstr ""
"O peso de um corpo sob a gravidade da terra em Washington DC, ao nível do "
"mar (9.801m/s^2)."

#: mass.cpp:31
msgid ""
"Since pounds are a measurement of weight, not mass, this is assumed to be "
"the weight on Earth.  The full explanation is under Slugs."
msgstr ""
"Uma vez que as libras são uma medida de peso, não de massa, este é "
"assumido como o peso na Terra. A explicação completa está em \"Slugs\"."

#: mass.cpp:32
msgid ""
"A unit of measurement of weight, commonly used in Britain and not America.  "
"No 's' is appended for plural."
msgstr ""
"Uma unidade de medida do peso, vulgarmente usada na Grã-Bretanha e não na\n"
"América. Não é adicionado o s ao plural."

#: mass.cpp:33
msgid ""
"Slugs are actual units for mass in imperial units, not weight!  Pounds are a "
"measure of weight, not mass.  Pounds are to newtons as kilograms are to "
"slugs."
msgstr ""
"\"Slugs\" é uma unidade para massa em unidades Imperiais, não para peso! "
"As libras medem o peso, não a massa. As libras estão para os Newtons tal "
"como os Quilogramas estão para os slugs."

#: mass.cpp:35
msgid "Officially known as Short Tons"
msgstr "Oficialmente conhecida como \"Short Tons\""

#: velocity.cpp:4
msgid "Meters per second (m/s)"
msgstr "Metros por segundo (m/s)"

#: velocity.cpp:5
msgid "Miles per hour (mi/h)"
msgstr "Milhas por hora (mi/h)"

#: velocity.cpp:6
msgid "Kilometers per hour (km/h)"
msgstr "Quilómetros por hora (km/h)"

#: velocity.cpp:7
msgid "Knots"
msgstr "Nós"

#: velocity.cpp:14
msgid "A knot is a nautical mile per hour."
msgstr "Um nó é uma milha náutica por hora."

#: prefixes.cpp:6
msgid "Common Prefixes"
msgstr "Prefixos Comuns"

#: prefixes.cpp:9
msgid "Prefix"
msgstr "Prefixo"

#: prefixes.cpp:10
msgid "Power of Ten"
msgstr "Potência de Dez"

#: prefixes.cpp:17
msgid "1 septillion"
msgstr "1 septilião"

#: prefixes.cpp:18
msgid "1 sextillion"
msgstr "1 sextilião"

#: prefixes.cpp:19
msgid "1 quintillion"
msgstr "1 quintilião"

#: prefixes.cpp:20
msgid "1 quadrillion"
msgstr "1 quadrilião"

#: prefixes.cpp:21
msgid "1 trillion"
msgstr "1 trilião"

#: prefixes.cpp:22
msgid "1 billion"
msgstr "1 bilião"

#: prefixes.cpp:23
msgid "1 million"
msgstr "1 milhão"

#: prefixes.cpp:24
msgid "10 thousand"
msgstr "10 mil"

#: prefixes.cpp:25
msgid "1 thousand"
msgstr "1 milhar"

#: prefixes.cpp:26
msgid "1 hundred"
msgstr "1 centena"

#: prefixes.cpp:27
msgid "1 ten"
msgstr "1 dezena"

#: prefixes.cpp:28
msgid "1 tenth"
msgstr "1 décimo"

#: prefixes.cpp:29
msgid "1 hundredth"
msgstr "1 centésimo"

#: prefixes.cpp:30
msgid "1 thousandth"
msgstr "1 milionésimo"

#: prefixes.cpp:31
msgid "1 millionth"
msgstr "1 milionésimo"

#: prefixes.cpp:32
msgid "1 billionth"
msgstr "1 bilionésimo"

#: prefixes.cpp:33
msgid "1 trillionth"
msgstr "1 trilionésimo"

#: prefixes.cpp:34
msgid "1 quadrillionth"
msgstr "1 quadrilionésimo"

#: prefixes.cpp:35
msgid "1 quintillionth"
msgstr "1 quintilionésimo"

#: prefixes.cpp:36
msgid "1 sextillionth"
msgstr "1 sextilionésimo"

#: prefixes.cpp:37
msgid "1 septillionth"
msgstr "1 septilionésimo"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "José Nuno Coelho Sanarra Pires"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "jncp@rnl.ist.utl.pt"
