msgid ""
msgstr ""
"Project-Id-Version: ktalk 0.2.4\n"
"POT-Creation-Date: 2001-07-26 23:49+0200\n"
"PO-Revision-Date: 2001-08-05 00:15+01:00\n"
"Last-Translator: José Nuno Coelho Sanarra Pires <jncp@rnl.ist.utl.pt>\n"
"Language-Team: pt <kde@poli.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: none\n"

#: addrbook.cpp:25
msgid "Address Book"
msgstr "Agenda"

#: addrbook.cpp:27
msgid "Entries :"
msgstr "Registos :"

#: addrbook.cpp:42
msgid "Undelete"
msgstr "Recuperar"

#: addrbook.cpp:46
msgid "Address(es) :"
msgstr "Endereço(s) :"

#: addrbook.cpp:55
msgid "Reset"
msgstr "Reinicializar"

#: addrbook.cpp:194 addrbook.cpp:276
msgid "[delete] "
msgstr "[apagar] "

#: addrbook.cpp:334
msgid "Error: no name"
msgstr "Erro: sem nome"

#: addrbook.cpp:335
msgid ""
"You have to specify a name\n"
"for the address book entry!"
msgstr ""
"Tens de indicar um nome\n"
"para o registo na agenda!"

#: addrbook.cpp:338
msgid "Error: entry exists"
msgstr "Erro: a entrada existe"

#: addrbook.cpp:339
msgid ""
"An address book entry with this name already exists,\n"
"please use a different name!"
msgstr ""
"Já existe um registo na agenda com este nome,\n"
"por favor usa um nome diferente!"

#: addrbook.cpp:363
msgid "Change existing address book entry"
msgstr "Alterar registo na agenda existente"

#: addrbook.cpp:365
msgid "Add new entry to address book"
msgstr "Adicionar um registo novo à agenda"

#: addrbook.cpp:367
msgid "Entry name :"
msgstr "Nome do registo :"

#: addrbook.cpp:374
msgid "Addresses :"
msgstr "Endereços :"

#: addrbook.cpp:380
msgid "recently used addresses :"
msgstr "Endereços usados recentemente :"

#: announcing.cpp:35
msgid "Talk Request"
msgstr "Pedido de Talk"

#: announcing.cpp:40
msgid "Answer"
msgstr "Responder"

#: announcing.cpp:41
msgid "Ignore"
msgstr "Ignorar"

#: announcing.cpp:48
msgid ""
"Talk connection requested by\n"
msgstr ""
"Ligação de 'talk' pedida por\n"

#: announcing.cpp:188
msgid "Client on display \""
msgstr "Cliente no dispositivo \""

#: announcing.cpp:209
msgid "ktalk: Error"
msgstr "ktalk: Erro"

#: announcing.cpp:210
msgid ""
"The client doesn't respond!\n"
"Do you want to start a new client?"
msgstr ""
"O cliente não responde!\n"
"Queres iniciar um novo cliente?"

#: announcing.cpp:319
msgid ""
"There are already other ktalk clients running\n"
"on this machine for this user, but on other displays.\n"
"Please choose the client to use!"
msgstr ""
"Já existem outros clientes do ktalk a correr nesta\n"
"máquina para este cliente, mas noutros dispositivos.\n"
"Escolhe qual o cliente a usar!"

#: filetransfer.cpp:77
msgid ""
"<qt>The connection to your talk partner was closed, so we cannot start file "
"transfer any more!</qt>"
msgstr ""
"<qt>A ligação ao teu colega de talk foi fechada, por isso não podemos "
"iniciar a transferência do ficheiro!</qt>"

#: filetransfer.cpp:79 ktalk.cpp:570
msgid "No connection"
msgstr "Sem ligação"

#: filetransfer.cpp:176
msgid ""
"<qt>The transfer connection was broken.<br>Maybe your partner aborted the "
"transfer.</qt>"
msgstr ""
"<qt>A ligação de transferência foi quebrada.<br>"
"Talvez o teu colega tenha abortado a transferência.</qt>"

#: filetransfer.cpp:178 filetransfer.cpp:311
msgid "Connection broken"
msgstr "Ligação quebrada"

#: filetransfer.cpp:219
msgid ""
"<qt>The transfer connection was refused!<br>Maybe your partner aborted "
"already.</qt>"
msgstr ""
"<qt>A ligação de transferência foi recusada!<br>Talvez o teu colega já "
"tenha abortado.</qt>"

#: filetransfer.cpp:221
msgid "Connection refused"
msgstr "Ligação recusada"

#: filetransfer.cpp:224
msgid "<qt>We couldn't create a connection due to an unexpected error!</qt>"
msgstr "<qt>Não conseguimos criar uma ligação devido a um erro inesperado!</qt>"

#: filetransfer.cpp:225
msgid "Connection error"
msgstr "Erro de ligação"

#: filetransfer.cpp:243
msgid "<qt>The file<br>%1<br>couldn't be opened for writing!<br>"
msgstr "<qt>O ficheiro <br>%1<br> não pôde ser aberto para escrita!<br>"

#: filetransfer.cpp:245
msgid "File error"
msgstr "Erro do ficheiro"

#: filetransfer.cpp:309
msgid "<qt>The transfer connection was broken!<br>Maybe your partner aborted.</qt>"
msgstr ""
"<qt>A ligação de transferência foi quebrada!<br>"
"Talvez o teu colega tenha abortado.</qt><"

#: filetransfer.cpp:449
msgid "File Transfer"
msgstr "Transferência de Ficheiro"

#: filetransfer.cpp:452
msgid "File:"
msgstr "Ficheiro:"

#: filetransfer.cpp:455
msgid "From:"
msgstr "De:"

#: filetransfer.cpp:455
msgid "To:"
msgstr "Para:"

#: filetransfer.cpp:458
msgid "Waiting for connection"
msgstr "À espera de ligação"

#: filetransfer.cpp:461 ktalk.cpp:449
msgid "Abort"
msgstr "Abortar"

#: filetransfer.cpp:477
msgid "Waiting for connection, %1 bytes"
msgstr "À espera de ligação, %1 bytes"

#: filetransfer.cpp:479
msgid "%1 / %2 bytes transfered"
msgstr "%1 / %2 bytes transferidos"

#: ktalk.cpp:90
msgid "&New Connection..."
msgstr "&Nova Ligação..."

#: ktalk.cpp:93
msgid "&Disconnect"
msgstr "&Desligar"

#: ktalk.cpp:97
msgid "&Address book..."
msgstr "&Agenda..."

#: ktalk.cpp:100
msgid "&Ping"
msgstr "&Ping"

#: ktalk.cpp:103
msgid "&File transfer"
msgstr "Transferência de &ficheiro"

#: ktalk.cpp:113
msgid "Arrange &horizontal"
msgstr "Organizar &horizontalmente"

#: ktalk.cpp:120
msgid "Arrange &vertical"
msgstr "Organizar &verticalmente"

#: ktalk.cpp:128
msgid "Clear address bar"
msgstr "Limpar a barra de endereços"

#: ktalk.cpp:133
msgid "Address  "
msgstr "Endereço  "

#: ktalk.cpp:137
msgid "Address"
msgstr "Endereço"

#: ktalk.cpp:169
msgid "No connection yet."
msgstr "Ainda sem ligação."

#: ktalk.cpp:303
msgid " [connecting]"
msgstr " [a ligar]"

#: ktalk.cpp:315
msgid "Connection established"
msgstr "Ligação estabelecida"

#: ktalk.cpp:390
msgid "Looking for an invitation..."
msgstr "À espera dum convite..."

#: ktalk.cpp:393
msgid "He invited us. Connecting to him..."
msgstr "Ele convidou-nos. A contactar com ele..."

#: ktalk.cpp:396
msgid "Invitation wasn't up to date. Inviting him ourselves..."
msgstr "O convite não foi actualizado. Vamos convidá-lo a ele..."

#: ktalk.cpp:399
msgid "No invitation, so inviting him..."
msgstr "Sem convite, por isso convidá-lo..."

#: ktalk.cpp:402
msgid "Waiting for him to answer..."
msgstr "À espera que ele responda..."

#: ktalk.cpp:417
msgid "The host can't be found. Be sure that you are online."
msgstr "A máquina não foi encontrada. Verifica que estás 'online'."

#: ktalk.cpp:420
msgid "No talk daemon was found on that host."
msgstr "Nenhum servidor de talk encontrado naquela máquina."

#: ktalk.cpp:423
msgid "No talk daemon was found on our host."
msgstr "Nenhum servidor de talk encontrado na nossa máquina."

#: ktalk.cpp:426
msgid "The talk daemon of the host doesn't answer."
msgstr "O servidor de talk da máquina não responde."

#: ktalk.cpp:429
msgid "The talk daemon of our host doesn't answer."
msgstr "O servidor de talk da nossa máquina não responde."

#: ktalk.cpp:432
msgid "Internal error: A socket couldn't be created!"
msgstr "Erro interno: não se pôde criar um 'socket'!"

#: ktalk.cpp:435
msgid "Our talk daemon does not accept an invitation."
msgstr "O nosso servidor de talk não aceita convites."

#: ktalk.cpp:438
msgid "The user is not logged in or is unknown."
msgstr "O utilizador não está ligado ou é desconhecido."

#: ktalk.cpp:441
msgid "An unexpected error occurred!"
msgstr "Ocorreu um erro inesperado!"

#: ktalk.cpp:446
msgid "Connection Error"
msgstr "Erro de Ligação"

#: ktalk.cpp:447
msgid "<qt>%1</qt>"
msgstr "<qt>%1</qt>"

#: ktalk.cpp:448
msgid "Retry"
msgstr "Repetir"

#: ktalk.cpp:484
msgid "unknown"
msgstr "desconhecido"

#: ktalk.cpp:486
msgid " client: %1 "
msgstr " cliente: %1 "

#: ktalk.cpp:538
msgid " [closed]"
msgstr " [terminado]"

#: ktalk.cpp:539
msgid "Connection closed"
msgstr "Ligação terminada"

#: ktalk.cpp:556
msgid "Ping is running..."
msgstr "O ping está a correr..."

#: ktalk.cpp:568
msgid ""
"<qt>There is no established connection to your talk partner, so file "
"transfer doesn't work now!</qt>"
msgstr ""
"<qt>Não está estabelecida nenhuma ligação com o teu colega, por isso a "
"transferência de ficheiros não funciona agora!</qt>"

#: ktalk.cpp:576
msgid ""
"<qt>"
"Your talk partner doesn't use KTalk or he uses a version that is too old and "
"doesn't yet support file transfer!</qt>"
msgstr ""
"<qt>O teu colega não usa o KTalk ou está a usar uma versão muito velha e "
"que não suporta ainda a transferência de ficheiros!</qt>"

#: ktalk.cpp:578
msgid "File transfer not supported"
msgstr "Transferência de ficheiros não suportada"

#: ktalk.cpp:583 ktalk.cpp:765
msgid "*|Any File"
msgstr "*|Qualquer Ficheiro"

#: ktalk.cpp:584
msgid "Send file"
msgstr "Enviar ficheiro"

#: ktalk.cpp:731
msgid "Ping result: %1 msecs"
msgstr "Resultado do ping: %1 ms"

#: ktalk.cpp:733
msgid "Ping result: %1 secs"
msgstr "Resultado do ping: %1 s"

#: ktalk.cpp:752
msgid "Received a bad file transfer offer"
msgstr "Recebi uma oferta de transferência de ficheiro inválida"

#: ktalk.cpp:756
msgid ""
"<qt>%1 offers<br>a file <b>%2</b><br> (size %3 bytes)<br>for file "
"transfer.<p>Download this file?"
msgstr ""
"<qt>O %1 oferece<br>um ficheiro <b>%2</b><br> (tamanho %3 bytes)<br>"
"para ser transferido.<p>Transferir este ficheiro?"

#: ktalk.cpp:762
msgid "File Transfer Offer"
msgstr "Oferta de Transferência de Ficheiro"

#: ktalk.cpp:766
msgid "Save file as"
msgstr "Gravar o ficheiro como"

#: kt_configdlg.cpp:63
msgid "Option"
msgstr "Opção"

#: kt_configdlg.cpp:63
msgid "Talk Options"
msgstr "Opções do Talk"

#: kt_configdlg.cpp:66
msgid "Identity"
msgstr "Identidade"

#: kt_configdlg.cpp:67
msgid "User Name"
msgstr "Nome do Utilizador"

#: kt_configdlg.cpp:72
msgid "Sound when connection is established"
msgstr "Apitar ao estabelecer a ligação"

#: kt_configdlg.cpp:75
msgid "&Beep"
msgstr "&Campainha"

#: kt_configdlg.cpp:79
msgid "&Play File"
msgstr "Tocar o &Ficheiro"

#: kt_configdlg.cpp:88
msgid "Browse"
msgstr "Explorar"

#: kt_configdlg.cpp:91
msgid "Test"
msgstr "Testar"

#: kt_configdlg.cpp:99
msgid "Talk Font"
msgstr "Tipo de Letra do Talk"

#: kt_configdlg.cpp:103
msgid "KTalk font"
msgstr "Tipo de letra do KTalk"

#: kt_savedlg.cpp:40
msgid "Save Dialog"
msgstr "Janela de Gravação"

#: kt_savedlg.cpp:61
msgid "Save marked text"
msgstr "Gravar o texto marcado"

#: kt_savedlg.cpp:65
msgid "Choose which text to save"
msgstr "Escolher qual o texto a gravar"

#: kt_savedlg.cpp:72
msgid "Your text"
msgstr "O teu texto"

#: kt_savedlg.cpp:77
msgid "Your partner's text "
msgstr "O texto do teu colega"

#: kt_savedlg.cpp:82
msgid "Both text (To be done)"
msgstr "O texto de ambos (a ser feito)"

#: kt_savedlg.cpp:89
msgid "Select encoding"
msgstr "Seleccionar a codificação"

#: kt_savedlg.cpp:96
msgid "Local charset"
msgstr "Codificação local"

#: kt_savedlg.cpp:101
msgid "UTF-8"
msgstr "UTF-8"

#: kt_savedlg.cpp:106
msgid "UTF-16"
msgstr "UTF-16"

#: kt_savedlg.cpp:211
msgid "Couldn't save "
msgstr "Não pude gravar o "

#: main.cpp:38
msgid "Address to talk to: user[@host][#tty]"
msgstr "Endereço a quem ligar: utilizador[@maquina][#tty]"

#: main.cpp:39
msgid "ktalk exits automatically after the last connection is closed"
msgstr "O ktalk termina automaticamente depois da última ligação fechada."

#: main.cpp:46
msgid "KTalk"
msgstr "KTalk"

#: main.cpp:48
msgid "A graphical talk client"
msgstr "Um cliente gráfico de talk"

#: rc.cpp:1
msgid "&Connection"
msgstr "&Ligação"

#: rc.cpp:2
msgid "&Window arrangement"
msgstr "Organização da &janela"

#: talkdlg.cpp:44
msgid "&Talk to:"
msgstr "&Ligar a:"

#: talkdlg.cpp:51
msgid "Address Book &Entries:"
msgstr "R&egistos da Agenda:"

#: talkdlg.cpp:59
msgid "&Addresses:"
msgstr "E&ndereços:"

#: talkdlg.cpp:61
msgid "Talk"
msgstr "Talk"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "José Nuno Coelho Sanarra Pires"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "jncp@rnl.ist.utl.pt"
