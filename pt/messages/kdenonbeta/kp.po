msgid ""
msgstr ""
"Project-Id-Version: kp\n"
"POT-Creation-Date: 2001-07-26 23:49+0200\n"
"PO-Revision-Date: 2000-08-22 15:00+0000\n"
"Last-Translator: José Nuno Coelho Sanarra Pires <jncp@rnl.ist.utl.pt>\n"
"Language-Team: pt <kde@poli.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: none\n"

#: main.cpp:13
msgid "Klasstrophobia - For the fear of classes, use the ultimate class parser!"
msgstr ""
"Klasstrophobia - para quem tem medo de classes, use o último analisador de "
"classes!"

#: main.cpp:21
msgid "Directory to parse"
msgstr "Directoria a analisar"

#: main.cpp:22
msgid "Extensions to parse"
msgstr "Extensões a analisar"

#: main.cpp:29
msgid "Klasstrophobia"
msgstr "Klasstrophobia"

#: main.cpp:32
msgid "Developer"
msgstr "Programador"

#: kpview.cpp:18
msgid "Classes"
msgstr "Classes"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "José Nuno Coelho Sanarra Pires"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "jncp@rnl.ist.utl.pt"
