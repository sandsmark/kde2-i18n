msgid ""
msgstr ""
"Project-Id-Version: kmms\n"
"POT-Creation-Date: 2001-07-26 23:49+0200\n"
"PO-Revision-Date: 2001-07-05 20:39+0000\n"
"Last-Translator: Jose Nuno Coelho Sanarra Pires <jncp@rnl.ist.utl.pt>\n"
"Language-Team: Portuguese <kde@poli.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: none\n"

#: kmms.cpp:31
msgid "Start playing"
msgstr "Começar a tocar"

#: kmms.cpp:32
msgid "Pause playing"
msgstr "Parar de tocar"

#: kmms.cpp:33
msgid "Next Track in Playlist"
msgstr "Próxima Faixa na Lista"

#: kmms.cpp:34
msgid "Previous Track in Playlist"
msgstr "Faixa Anterior na Lista"

#: kmms.cpp:71
msgid ""
"KMMS v0.2 - A remote control applet for XMMS\n"
"\n"
"(c) 2000 by Carsten Pfeiffer <pfeiffer@kde.org>\n"
"(c) 2000 by Daniel Molkentin <molkentin@kde.org>"
msgstr ""
"KMMS v0.2 - Uma 'applet' de controlo do XMMS\n"
"\n"
"(c) 2000 por Carster Pfeiffer <pfeiffer@kde.org>\n"
"(c) 2000 por Daniel Molkentin <molkentin@kde.org>"

#: kmms.cpp:74
msgid "About KMMS"
msgstr "Acerca do KMMS"
