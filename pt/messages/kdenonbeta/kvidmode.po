msgid ""
msgstr ""
"Project-Id-Version: kvidmode\n"
"POT-Creation-Date: 2001-08-04 12:46+0200\n"
"PO-Revision-Date: 2001-07-28 17:16+0000\n"
"Last-Translator: Jose Nuno Coelho Sanarra Pires <jncp@rnl.ist.utl.pt>\n"
"Language-Team: Portuguese <kde@poli.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: none\n"

#: gamma.cpp:57
msgid "Gamma &Values"
msgstr "&Valores de 'Gamma'"

#: gamma.cpp:58
msgid "Gamma &Ramp"
msgstr "&Rampa do Gamma"

#: gamma.cpp:90
msgid "<h1>Gamma</h1> Here, you can set up your gamma."
msgstr "<h1>'Gamma'</h1> Aqui podes configurar o teu 'gamma'."

#: gammaramptab.cpp:45 gammatab.cpp:46
msgid "Sorry, but the X display $1 could not be opened."
msgstr "Desculpa mas não consigo aceder ao 'display' do X $1."

#: gammaramptab.cpp:57
msgid ""
"\n"
"This is the advanced gamma setting for XFree86.\n"
"\n"
"To correctly set the gamma values, change the curve below so that the colors "
"in the upper half of the color bars exactly match the colors in the lower "
"half."
msgstr ""
"\n"
"Esta é a configuração de 'gamma' avançada do XFree86.\n"
"\n"
"Para alterares os valores correctos do 'gamma', altera a curva em baixo de "
"modo a que as cores na parte superior das barras correspondam exactamente "
"às cores na parte inferior."

#: gammaramptab.cpp:67
msgid "Red"
msgstr "Vermelho"

#: gammaramptab.cpp:68
msgid "Green"
msgstr "Verde"

#: gammaramptab.cpp:69
msgid "Blue"
msgstr "Azul"

#: gammaramptab.cpp:71 gammatab.cpp:72
msgid "&Connect"
msgstr "&Ligar"

#: gammatab.cpp:56
msgid ""
"\n"
"This is the gamma setting for XFree86.\n"
"\n"
"To correctly set the gamma values, move the sliders below so that the colors "
"in the left half of the four color bars exactly match the colors in the "
"right half. If the left half is darker than the right half, you have to move "
"the slider to the right."
msgstr ""
"\n"
"Esta é a configuração do 'gamma' para o XFree86.\n"
"\n"
"Para alterares correctamente os valores de 'gamma', move as barras "
"deslizantes de modo a que as cores na metade esquerda das quatro barras de "
"cor correspondam às da metade direita. Se a parte esquerda estiver mais "
"escura que a direita, tens de mover a barra para a direita."

#: gammatab.cpp:68
msgid "&R"
msgstr "&R"

#: gammatab.cpp:69
msgid "&G"
msgstr "&G"

#: gammatab.cpp:70
msgid "&B"
msgstr "&B"

#: kvidmode.cpp:68
msgid "&Video Modes"
msgstr "Modos &Vídeo"

#: kvidmode.cpp:70
msgid "&Gamma Correction"
msgstr "Correcção de '&Gamma'"

#: kvidmode.cpp:72
msgid "&Monitor"
msgstr "&Monitor"

#: kvidmode.cpp:74
msgid "&Fullscreen Mode"
msgstr "Modo Ecrã &Completo"

#: kvidmode.cpp:139
msgid "Save Viewport"
msgstr "Gravar Vista"

#: kvidmode.cpp:141
msgid "Go to Viewport"
msgstr "Ir para a Vista"

#: kvidmode.cpp:144
msgid "Fullscreen"
msgstr "Ecrã completo"

#: kvidmode.cpp:320
msgid "A utility to switch video modes."
msgstr "Um utilitário para mudar de modo vídeo."

#: kvidmode.cpp:326
msgid "Video Mode"
msgstr "Modo Vídeo"

#: modelinetab.cpp:56 vmodetab.cpp:55
msgid "Mode Selection"
msgstr "Selecção do Modo"

#: modelinetab.cpp:59 vmodetab.cpp:58
msgid "&Mode: "
msgstr "&Modo: "

#: monitordialog.cpp:80
msgid "Parse Error in "
msgstr "Analisar erro por "

#: monitordialog.cpp:127
msgid ""
"HSync Range: %1-%2kHz\n"
msgstr ""
"Gama de HSync: %1-%2kHz\n"

#: monitordialog.cpp:132
msgid ""
"VSync Range: %1-%2Hz\n"
msgstr ""
"Gama de VSync: %1-%2Hz\n"

#: monitordialog.cpp:158
msgid "<h1>Monitor</h1> Here you can select your monitor."
msgstr "<h1>Monitor</h1> Aqui podes seleccionar o teu monitor."

#: tunedialog.cpp:45
msgid "&Position"
msgstr "&Posição"

#: tunedialog.cpp:46
msgid "&Size"
msgstr "&Tamanho"

#: tunedialog.cpp:47
msgid "&Mode Selection"
msgstr "Selecção do &Modo"

#: tunedialog.cpp:48
msgid "&Horizontal"
msgstr "&Horizontal"

#: tunedialog.cpp:49
msgid "&Vertical"
msgstr "&Vertical"

#: tunedialog.cpp:50
msgid "&General"
msgstr "&Geral"

#: tunedialog.cpp:110
msgid "Test"
msgstr "Testar"

#: tunedialog.cpp:111 tunedialog.cpp:228
msgid "More >>>"
msgstr "Mais >>>"

#: tunedialog.cpp:115
msgid "Name: "
msgstr "Nome: "

#: tunedialog.cpp:116
msgid "Width: "
msgstr "Largura: "

#: tunedialog.cpp:117
msgid "Height: "
msgstr "Altura: "

#: tunedialog.cpp:118
msgid "Refresh: "
msgstr "Refrescamento: "

#: tunedialog.cpp:132
msgid "+HSync"
msgstr "+HSync"

#: tunedialog.cpp:133
msgid "-HSync"
msgstr "-HSync"

#: tunedialog.cpp:139
msgid "HDisp: "
msgstr "HDisp: "

#: tunedialog.cpp:140
msgid "HSyncStart: "
msgstr "HSyncStart: "

#: tunedialog.cpp:141
msgid "HSyncEnd: "
msgstr "HSyncEnd: "

#: tunedialog.cpp:142
msgid "HTotal: "
msgstr "HTotal: "

#: tunedialog.cpp:143
msgid "HSkew: "
msgstr "HSkew: "

#: tunedialog.cpp:158
msgid "+VSync"
msgstr "+VSync"

#: tunedialog.cpp:159
msgid "-VSync"
msgstr "-VSync"

#: tunedialog.cpp:165
msgid "VDisp: "
msgstr "VDisp: "

#: tunedialog.cpp:166
msgid "VSyncStart: "
msgstr "VSyncStart: "

#: tunedialog.cpp:167
msgid "VSyncEnd: "
msgstr "VSyncEnd: "

#: tunedialog.cpp:168
msgid "VTotal: "
msgstr "VTotal: "

#: tunedialog.cpp:169
msgid "VSkew: "
msgstr "VSkew: "

#: tunedialog.cpp:180
msgid "Interlaced"
msgstr "Entrelaçado"

#: tunedialog.cpp:181
msgid "DoubleScan"
msgstr "DoubleScan"

#: tunedialog.cpp:182
msgid "Composite"
msgstr "Composto"

#: tunedialog.cpp:183
msgid "+CSync"
msgstr "+CSync"

#: tunedialog.cpp:184
msgid "-CSync"
msgstr "-CSync"

#: tunedialog.cpp:188
msgid "Dot Clock: "
msgstr "Relógio: "

#: tunedialog.cpp:219
msgid "Fewer <<<"
msgstr "Menos <<<"

#: videomode.cpp:51
msgid "Mode &Selection"
msgstr "&Selecção do Modo"

#: videomode.cpp:52
msgid "&Edit Modes"
msgstr "&Editar os Modos"

#: videomode.cpp:110
msgid "<h1>Video Mode</h1> Here you can select video modes."
msgstr "<h1>Modo Vídeo</h1> Aqui podes seleccionar os modos vídeo."

#: vmodetab.cpp:60
msgid "&Show icon in system tray"
msgstr "&Mostrar o ícone na bandeja do sistema"

#: xmlmonitors.cpp:174
msgid ""
"fatal parsing error: %1 in line %2, column %3\n"
msgstr ""
"erro de análise fatal: %1 na linha %2, coluna %3\n"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "José Nuno Coelho Sanarra Pires"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "jncp@rnl.ist.utl.pt"
