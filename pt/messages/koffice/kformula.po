msgid ""
msgstr ""
"Project-Id-Version: kformula\n"
"POT-Creation-Date: 2001-05-18 02:08+0200\n"
"PO-Revision-Date: 2001-05-06 01:29+01:00\n"
"Last-Translator: Pedro Morais <morais@kde.org>\n"
"Language-Team: pt <kde@poli.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: none\n"

#: rc.cpp:2
msgid "E&lement"
msgstr "E&lemento"

#: rc.cpp:3
msgid "Add..."
msgstr "Adicionar..."

#: rc.cpp:4
msgid "Vertical align"
msgstr "Alinhamento vertical"

#: rc.cpp:5
msgid "Horizontal align"
msgstr "Alinhamento horizontal"

#: rc.cpp:6
msgid "Fraction vertical distance"
msgstr "Distância vertical da fracção"

#: rc.cpp:7
msgid "Matrix"
msgstr "Matriz"

#: rc.cpp:9
msgid "Increase/Decrease options.."
msgstr "Opções de aumentar/diminuir..."

#: rc.cpp:14
msgid "Element"
msgstr "Elemento"

#: rc.cpp:15
msgid "Symbol"
msgstr "Símbolo"

#: main.cc:30
msgid "File To Open"
msgstr "Ficheiro a Abrir"

#: matrixwidget.cc:34
msgid "KFormula - Matrix Element Setup"
msgstr "KFormula - Configuração dos Elementos da Matriz"

#: matrixwidget.cc:37
msgid "General"
msgstr "Global"

#: matrixwidget.cc:46
msgid "Size & Space:"
msgstr "Tamanho & Espaço:"

#: matrixwidget.cc:51
msgid "Columns:"
msgstr "Colunas:"

#: matrixwidget.cc:58
msgid "Rows"
msgstr "Linhas"

#: matrixwidget.cc:68
msgid "Space:"
msgstr "Espaço:"

#: matrixwidget.cc:81
msgid "Borders:"
msgstr "Contornos:"

#: matrixwidget.cc:84 matrixwidget.cc:90 matrixwidget.cc:96
#: matrixwidget.cc:102 matrixwidget.cc:108 matrixwidget.cc:114
msgid "No Border"
msgstr "Sem Contorno"

#: matrixwidget.cc:85 matrixwidget.cc:91 matrixwidget.cc:97
#: matrixwidget.cc:103 matrixwidget.cc:109 matrixwidget.cc:115
msgid "Single Line"
msgstr "Linha simples"

#: matrixwidget.cc:86 matrixwidget.cc:92 matrixwidget.cc:98
#: matrixwidget.cc:104 matrixwidget.cc:110 matrixwidget.cc:116
msgid "Double Line"
msgstr "Linha Dupla"

#: matrixwidget.cc:121
msgid "Internal Horizontal"
msgstr "Interna Horizontal"

#: matrixwidget.cc:125
msgid "Internal Vertical"
msgstr "Interna Vertical"

#: matrixwidget.cc:156
msgid "Vertical:"
msgstr "Vertical:"

#: matrixwidget.cc:162
msgid "Fixed Row"
msgstr "Linha Fixa"

#: matrixwidget.cc:170
msgid "Half Matrix"
msgstr "Meia Matriz"

#: matrixwidget.cc:179
msgid "Row Midline"
msgstr "Linha Intermédia da Linha"

#: matrixwidget.cc:182
msgid "Over the row"
msgstr "Sobre a linha"

#: matrixwidget.cc:185
msgid "Under the row"
msgstr "Sob a linha"

#: matrixwidget.cc:196
msgid "Horizontal:"
msgstr "Horizontal:"

#: matrixwidget.cc:199
msgid "Center "
msgstr "Centrado "

#: matrixwidget.cc:200
msgid "Left side"
msgstr "Esquerda"

#: matrixwidget.cc:201
msgid "Right side"
msgstr "Direita"

#: kformula_aboutdata.h:27
msgid "KOffice Formula Editor"
msgstr "Editor de Fórmulas do KOffice"

#: kformula_aboutdata.h:32
msgid "KFormula"
msgstr "KFormula"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "José Nuno Coelho Sanarra Pires"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "jncp@rnl.ist.utl.pt"
