# ktetris ko.po
# Copyright (C) 1999 Free Software Foundation, Inc.
# Byeong-Chan Kim <redhands@linux.sarang.net>, 1999.
#
msgid ""
msgstr ""
"Project-Id-Version: ktetris\n"
"POT-Creation-Date: 2001-07-05 13:33+0200\n"
"PO-Revision-Date: 1999-09-30 08:45+0900\n"
"Last-Translator: Byeong-Chan Kim <redhands@linux.sarang.net>\n"
"Language-Team: Korean <ko@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: rc.cpp:1 rc.cpp:3
#, fuzzy
msgid "&Multiplayers"
msgstr "멀티플레이 게임(&M)"

#: common/ai.cpp:88
msgid "Thinking depth"
msgstr ""

#: common/ai.cpp:90
msgid "Number of occupied lines"
msgstr ""

#: common/ai.cpp:91
msgid "Peak-to-peak distance"
msgstr ""

#: common/ai.cpp:92
msgid "Number of holes"
msgstr ""

#: common/ai.cpp:93
msgid "Number of spaces (under the mean height)"
msgstr ""

#: common/ai.cpp:94
#, fuzzy
msgid "Mean height"
msgstr "오른쪽으로 이동"

#: common/ai.cpp:316
msgid "Trigger"
msgstr ""

#: common/ai.cpp:377
msgid "Configure AI"
msgstr ""

#: common/dialogs.cpp:30
#, fuzzy
msgid "Game"
msgstr "게임 종료"

#: common/dialogs.cpp:36
msgid "Initial level"
msgstr ""

#: common/dialogs.cpp:39
msgid "Show tile's shadow"
msgstr ""

#: common/dialogs.cpp:43
#, fuzzy
msgid "Show next tile"
msgstr "메뉴 보임"

#: common/dialogs.cpp:57
msgid "Block size"
msgstr ""

#: common/dialogs.cpp:60
msgid "Enable animations"
msgstr ""

#: common/field.cpp:39
#, fuzzy
msgid "Previous player's name"
msgstr "사용자 이름 :"

#: common/field.cpp:48 common/ghighscores.h:125
msgid "Score"
msgstr "점수"

#: common/field.cpp:55
msgid ""
"<qt>Display the current score.<br/>It turns <font color=\"blue\">blue</font>"
" if it is a highscore and <font color=\"red\">red</font> if it is the best "
"local score.</qt>"
msgstr ""

#: common/field.cpp:79 common/highscores.cpp:16
msgid "Level"
msgstr "등급"

#: common/field.cpp:95
msgid "Lights when a \"gift\" is received from previous player"
msgstr ""

#: common/field.cpp:106
msgid "Previous player's height"
msgstr ""

#: common/field.cpp:123
msgid "Shadow of the current piece"
msgstr ""

#: common/field.cpp:133
msgid "Lights when you send a \"gift\" to the next player"
msgstr ""

#: common/field.cpp:144
msgid "Next player's height"
msgstr ""

#: common/field.cpp:150
#, fuzzy
msgid "Next player's name"
msgstr "사용자 이름 :"

#: common/field.cpp:158
msgid "Next tile"
msgstr "다음 타일"

#: common/field.cpp:269
msgid ""
"%1\n"
"(AI player)"
msgstr ""

#: common/field.cpp:270
msgid ""
"%1\n"
"(Human player)"
msgstr ""

#: common/field.cpp:271
#, fuzzy
msgid ""
"\n"
"Waiting for server start"
msgstr "다른 사용자를 기다림..."

#: common/field.cpp:273 common/field.cpp:346
#, fuzzy
msgid "Press to start"
msgstr "아무키나 누르면 게임 시작"

#: common/field.cpp:334
msgid "Press to resume"
msgstr "계속하려면 아무키나 누르세요"

#: common/field.cpp:335
msgid "Game paused"
msgstr "게임 잠시 멈춤"

#: common/field.cpp:347
msgid "Game over"
msgstr "게임 종료"

#: common/ghighscores.cpp:142 common/ghighscores.h:136
#: common/highscores.cpp:34
#, fuzzy
msgid "anonymous"
msgstr "무명씨"

#: common/ghighscores.cpp:142
#, fuzzy
msgid "Player"
msgstr "사용자 이름 :"

#: common/ghighscores.cpp:176
msgid ""
"Please enter your nickname\n"
"in the settings dialog."
msgstr ""

#: common/ghighscores.cpp:207
#, fuzzy
msgid "Nb of games"
msgstr "게임 시작"

#: common/ghighscores.cpp:213 common/ghighscores.cpp:512
#, fuzzy
msgid "Comment"
msgstr "클라이언트"

#: common/ghighscores.cpp:296
msgid "Please choose a non empty nickname."
msgstr ""

#: common/ghighscores.cpp:362
msgid "Unable to open temporary file"
msgstr ""

#: common/ghighscores.cpp:369
msgid "no data in answer"
msgstr ""

#: common/ghighscores.cpp:381 common/ghighscores.cpp:403
msgid "Invalid answer"
msgstr ""

#: common/ghighscores.cpp:461 common/ghighscores.cpp:499
#, fuzzy
msgid "Highscores"
msgstr "최고 점수"

#: common/ghighscores.cpp:465
#, fuzzy
msgid "Best scores"
msgstr "최고 점수"

#: common/ghighscores.cpp:467
msgid "no score entry"
msgstr ""

#: common/ghighscores.cpp:475
#, fuzzy
msgid "Players"
msgstr "사용자 이름 :"

#: common/ghighscores.cpp:508
msgid "Nickname"
msgstr ""

#: common/ghighscores.cpp:516
msgid "world-wide highscores enabled"
msgstr ""

#: common/ghighscores.h:116
msgid "Rank"
msgstr ""

#: common/ghighscores.h:132
#, fuzzy
msgid "Name"
msgstr "게임 종료"

#: common/ghighscores.h:145
#, fuzzy
msgid "Mean score"
msgstr "최고 점수"

#: common/ghighscores.h:158
#, fuzzy
msgid "Best score"
msgstr "최고 점수"

#: common/highscores.cpp:48
#, fuzzy
msgid "Multiplayers scores"
msgstr "멀티플레이 게임(&M)"

#: common/inter.cpp:18
msgid "Move left"
msgstr "왼쪽으로 이동"

#: common/inter.cpp:19
msgid "Move right"
msgstr "오른쪽으로 이동"

#: common/inter.cpp:20
msgid "Drop down"
msgstr "아래로 내림"

#: common/inter.cpp:21
msgid "One line down"
msgstr "한 줄 아래로"

#: common/inter.cpp:22
msgid "Rotate left"
msgstr "왼쪽으로 회전"

#: common/inter.cpp:23
msgid "Rotate right"
msgstr "오른쪽으로 회전"

#: common/main.cpp:42
msgid "Single Human"
msgstr ""

#: common/main.cpp:44
msgid "Human vs Human"
msgstr ""

#: common/main.cpp:46
msgid "Human vs &Computer"
msgstr ""

#: common/main.cpp:49
msgid "More..."
msgstr ""

#: common/main.cpp:57
#, fuzzy
msgid "Configure AI..."
msgstr "연결 중..."

#: common/main.cpp:127
msgid "Core engine"
msgstr ""

#: kfouleggs/ai.cpp:26
msgid "Number of removed blocks"
msgstr ""

#: kfouleggs/main.cpp:25
msgid "Removed eggs"
msgstr ""

#: kfouleggs/main.cpp:45
msgid "KFoulEggs"
msgstr ""

#: kfouleggs/main.cpp:46
msgid "KFoulEggs is a clone of the well-known (at least in Japan) PuyoPuyo game."
msgstr ""

#: ksirtet/ai.cpp:26
msgid "Number of full lines"
msgstr ""

#: ksirtet/main.cpp:40
msgid "KSirtet"
msgstr ""

#: ksirtet/main.cpp:41
msgid "KSirtet is a clone of the well-known Tetris game."
msgstr ""

#: ksirtet/main.cpp:47
msgid "Removed lines"
msgstr ""

#: lib/meeting.cpp:18
#, fuzzy
msgid "Network meeting"
msgstr "네트워크 게임"

#: lib/meeting.cpp:39
#, fuzzy
msgid "Waiting for clients"
msgstr "클라이언트에서 취소됨"

#: lib/meeting.cpp:54
#, fuzzy
msgid "Start Game"
msgstr "게임 재시작(&R)"

#: lib/meeting.cpp:57
#, fuzzy
msgid "Abort"
msgstr "포트 :"

#: lib/meeting.cpp:70
msgid "A new client has just arrived (#%1)"
msgstr ""

#: lib/meeting.cpp:172
#, fuzzy
msgid "Error reading data from"
msgstr "서버에서 알 수 없는 데이타"

#: lib/meeting.cpp:174
#, fuzzy
msgid "Unknown data from"
msgstr "서버에서 알 수 없는 데이타"

#: lib/meeting.cpp:176
msgid "Error writing to"
msgstr ""

#: lib/meeting.cpp:178
msgid "Link broken or empty data from"
msgstr ""

#: lib/meeting.cpp:244
msgid "%1 client #%2 : disconnect it"
msgstr ""

#: lib/meeting.cpp:267
#, c-format
msgid ""
"Failed to accept incoming client:\n"
"%1"
msgstr ""

#: lib/meeting.cpp:299
msgid "Client rejected for incompatible ID"
msgstr ""

#: lib/meeting.cpp:304
#, fuzzy
msgid "Client #%1 has left"
msgstr "클라이언트가 죽었음"

#: lib/meeting.cpp:428
msgid "Cannot write to client #%1 at game beginning"
msgstr ""

#: lib/meeting.cpp:465
msgid "%1 server : aborting connection"
msgstr ""

#: lib/meeting.cpp:532
#, fuzzy
msgid "Client %1 has left"
msgstr "클라이언트가 죽었음"

#: lib/meeting.cpp:559
msgid ""
"the game has begun without you\n"
"(you have been excluded by the server)"
msgstr ""

#: lib/meeting.cpp:573
#, fuzzy
msgid "the server has aborted the game"
msgstr "서버가 죽었음"

#: lib/miscui.cpp:15
msgid "Ready"
msgstr ""

#: lib/miscui.cpp:20
msgid "Excluded"
msgstr ""

#: lib/miscui.cpp:51
msgid "Human"
msgstr ""

#: lib/miscui.cpp:52
msgid "AI"
msgstr ""

#: lib/miscui.cpp:53
msgid "None"
msgstr ""

#: lib/pline.cpp:68
#, c-format
msgid "Hu=%1"
msgstr ""

#: lib/pline.cpp:69
#, c-format
msgid "AI=%1"
msgstr ""

#: lib/pline.cpp:108
msgid "Settings"
msgstr ""

#: lib/wizard.cpp:53
#, fuzzy
msgid "Create a local game"
msgstr "네트워크 게임"

#: lib/wizard.cpp:54
#, fuzzy
msgid "Create a network game"
msgstr "네트워크 게임"

#: lib/wizard.cpp:55
#, fuzzy
msgid "Join a network game"
msgstr "네트워크 게임"

#: lib/wizard.cpp:61
#, fuzzy
msgid "Network settings"
msgstr "네트워크 대화창"

#: lib/wizard.cpp:66
#, fuzzy
msgid "Port"
msgstr "포트 :"

#: lib/wizard.cpp:71
msgid "Choose game type"
msgstr ""

#: lib/wizard.cpp:98 lib/wizard.cpp:123
#, fuzzy, c-format
msgid "Player #%1"
msgstr "사용자 이름 :"

#: lib/wizard.cpp:112
#, fuzzy
msgid "Configure keys"
msgstr "연결 중..."

#: lib/wizard.cpp:115
msgid "Local players settings"
msgstr ""

#: lib/wizard.cpp:134
#, fuzzy
msgid "Hostname"
msgstr "응답 받기 오류"

#: lib/wizard.cpp:137
#, fuzzy
msgid "the.server.address"
msgstr "서버 주소 :"

#: lib/wizard.cpp:138
#, fuzzy
msgid "Server address"
msgstr "서버 주소 :"

#: lib/wizard.cpp:176
msgid "Error looking up for \"%1\""
msgstr ""

#: lib/wizard.cpp:184
msgid "Error opening socket"
msgstr ""

#: lib/internal.cpp:132
#, fuzzy
msgid "cannot read socket"
msgstr "소켓 생성 실패"

#: lib/internal.cpp:137
#, fuzzy
msgid "cannot write to socket"
msgstr "소켓 생성 실패"

#: lib/internal.cpp:142
msgid "Link broken"
msgstr ""

#: lib/internal.cpp:240
#, fuzzy
msgid "client has not answered in time"
msgstr "클라이언트가 사라짐"

#: lib/types.cpp:20
msgid ""
"\n"
"server : \"%1\"\n"
"client : \"%2\""
msgstr ""

#: lib/types.cpp:25
msgid "The MultiPlayer library of the server is incompatible"
msgstr ""

#: lib/types.cpp:28
msgid "Trying to connect a server for another game type"
msgstr ""

#: lib/types.cpp:31
msgid "The server game version is incompatible"
msgstr ""

#: lib/mp_interface.cpp:112
#, c-format
msgid "Human %1"
msgstr ""

#: lib/mp_interface.cpp:113
#, c-format
msgid "AI %1"
msgstr ""

#: lib/mp_simple_interface.cpp:57
#, fuzzy
msgid "Server has left game!"
msgstr "서버가 사라짐"

#: lib/defines.cpp:9
msgid ""
"%1:\n"
"%2"
msgstr ""

#, fuzzy
#~ msgid "Player #%i"
#~ msgstr "사용자 이름 :"

#, fuzzy
#~ msgid "Player Name"
#~ msgstr "사용자 이름 :"

#, fuzzy
#~ msgid "Pause"
#~ msgstr "게임 잠시 멈춤"

#, fuzzy
#~ msgid "High Scores..."
#~ msgstr "최고 점수"

#~ msgid "Hall of Fame"
#~ msgstr "명예의 전당"

#~ msgid "&Pause game"
#~ msgstr "게임 잠시 멈춤(&P)"

#~ msgid "&High scores"
#~ msgstr "최고 점수(&H)"

#~ msgid "Hide menu"
#~ msgstr "메뉴 숨김"

#~ msgid "Show menu"
#~ msgstr "메뉴 보임"

#, fuzzy
#~ msgid "&New game"
#~ msgstr "게임 시작"

#~ msgid "&Keys"
#~ msgstr "글쇠(&K)"

#, fuzzy
#~ msgid "Network Meeting"
#~ msgstr "네트워크 대화창"

#, fuzzy
#~ msgid "cannot create socket"
#~ msgstr "소켓 생성 실패"

#, fuzzy
#~ msgid "cannot close socket"
#~ msgstr "소켓 생성 실패"

#, fuzzy
#~ msgid "cannot connect socket"
#~ msgstr "소켓 생성 실패"

#, fuzzy
#~ msgid "bind error"
#~ msgstr "바인드 오류"

#, fuzzy
#~ msgid "listen error"
#~ msgstr "응답 받기 오류"

#, fuzzy
#~ msgid "unknown connection from unknown host ??"
#~ msgstr "알 수 없는 사용자에게서 알 수 없는 메시지 (??)"

#, fuzzy
#~ msgid "Host error"
#~ msgstr "응답 받기 오류"

#, fuzzy
#~ msgid "Server error"
#~ msgstr "서버 모드"

#~ msgid "Lines removed"
#~ msgstr "한 줄 지워짐"

#~ msgid "Press Return to replay"
#~ msgstr "다시 시작하려면 리턴키를 누르세요"

#~ msgid "Highscores file not writable !"
#~ msgstr "최고점수 파일에 쓸 수 없음!"

#~ msgid "Illegal port"
#~ msgstr "잘못된 포트"

#~ msgid "Null address"
#~ msgstr "없는 주소"

#~ msgid "Unknown address"
#~ msgstr "알 수 없는 주소"

#~ msgid "at"
#~ msgstr " "

#~ msgid "Connect error"
#~ msgstr "연결 오류"

#~ msgid "Server unreachable"
#~ msgstr "서버에 접근 불가"

#~ msgid "Cancel from server"
#~ msgstr "서버에서 취소됨"

#~ msgid "Cannot send to server"
#~ msgstr "서버로 보내지 못함"

#~ msgid "Client timeout (No message from server)"
#~ msgstr "클라이언트 타임아웃 (서버에서 메시지가 없음)"

#~ msgid "Unknown message from client"
#~ msgstr "클라이언트에서 알 수 없는 메시지"

#~ msgid "Timeout"
#~ msgstr "타임아웃"

#~ msgid "No message from client"
#~ msgstr "클라이언트에서 메시지 없음"

#~ msgid "Unknown data from client"
#~ msgstr "클라이언트에서 알 수 없는 메시지"

#~ msgid "End message from client"
#~ msgstr "클라이언트에서 끝 메시지"

#~ msgid "Cannot send to client"
#~ msgstr "클라이언트에 보내지 못함"

#~ msgid "No option yet !"
#~ msgstr "준비 중인 옵션임!"

#~ msgid "Server"
#~ msgstr "서버"

#~ msgid "Connect"
#~ msgstr "연결"

#~ msgid "Play !"
#~ msgstr "시작!"

#~ msgid "Client mode"
#~ msgstr "클라이언트 모드"

#~ msgid "by"
#~ msgstr " "
