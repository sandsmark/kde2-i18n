# KTranslator Generated File
# kabalone ko.po
# Copyright (C) 1999 Free Software Foundation, Inc.
# LinuxKorea Co. <kde@linuxkorea.co.kr>, 1999.
#
msgid ""
msgstr ""
"Project-Id-Version: kabalone\n"
"POT-Creation-Date: 2001-06-10 17:19+0200\n"
"PO-Revision-Date: 1999-10-05 23:07+0900\n"
"Last-Translator: Byeong-Chan Kim <redhands@linux.sarang.net>\n"
"Language-Team: Korean <ko@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: rc.cpp:4
#, fuzzy
msgid "&Level"
msgstr "등급"

#: rc.cpp:5
#, fuzzy
msgid "&Computer plays"
msgstr "컴퓨터가 둠"

#: AbTop.cpp:268 rc.cpp:8
msgid "Configure Evaluation..."
msgstr ""

#: rc.cpp:9
#, fuzzy
msgid "Moves"
msgstr "천천히 움직임"

#: rc.cpp:10
#, fuzzy
msgid "Push Out"
msgstr "누름"

#: Move.cpp:85 rc.cpp:11
msgid "Push"
msgstr "누름"

#: rc.cpp:12
msgid "Normal"
msgstr "보통"

#: rc.cpp:13
msgid "For every move possible the given points are added to the Evaluation."
msgstr ""

#: rc.cpp:14
#, fuzzy
msgid "Position"
msgstr "위치 저장"

#: rc.cpp:15
msgid "Inner Ring 3"
msgstr ""

#: rc.cpp:16
msgid "Outer most Ring"
msgstr ""

#: rc.cpp:17
#, fuzzy
msgid "Middle Position"
msgstr "위치 저장"

#: rc.cpp:18
msgid "Inner Ring 2"
msgstr ""

#: rc.cpp:19 rc.cpp:20 rc.cpp:21 rc.cpp:23
msgid "+/-"
msgstr ""

#: rc.cpp:22
msgid "Inner most Ring"
msgstr ""

#: rc.cpp:24
msgid ""
"For every ball, the given points are added to the evaluation depending on "
"the balls position. The bonus for a given position is changed randomly in "
"the +/- range."
msgstr ""

#: rc.cpp:25
msgid "In-A-Row"
msgstr ""

#: rc.cpp:26
msgid "Three In-A-Row"
msgstr ""

#: rc.cpp:27
msgid "Two In-A-Row"
msgstr ""

#: rc.cpp:28
msgid "Four In-A-Row"
msgstr ""

#: rc.cpp:29
msgid "Five In-A-Row"
msgstr ""

#: rc.cpp:30
msgid "For a number of balls In-a-Row, the given points are added to the evaluation"
msgstr ""

#: rc.cpp:31
msgid "Count"
msgstr ""

#: rc.cpp:32
msgid "4 Balls more"
msgstr ""

#: rc.cpp:33
msgid "3 Balls more"
msgstr ""

#: rc.cpp:34
msgid "5 Balls more"
msgstr ""

#: rc.cpp:35
msgid "2 Balls more"
msgstr ""

#: rc.cpp:36
msgid "1 Ball more"
msgstr ""

#: rc.cpp:37
msgid ""
"For a difference in the number of balls, the given points are added to the "
"evaluation. A difference of 6 only can be a lost/won game."
msgstr ""

#: rc.cpp:38
msgid "Evaluation Schemes"
msgstr ""

#: rc.cpp:39
msgid "Save as ..."
msgstr ""

#: rc.cpp:41
msgid ""
"Here your Evaluation Scheme, defined in all other tabs of this dialog, can "
"be stored."
msgstr ""

#: rc.cpp:42
msgid "Evaluation of actual Position:"
msgstr ""

#: Move.cpp:22
msgid "RightDown"
msgstr "오른쪽 아래"

#: Move.cpp:23
msgid "LeftDown"
msgstr "왼쪽 아래"

#: Move.cpp:25
msgid "LeftUp"
msgstr "왼쪽 위"

#: Move.cpp:26
msgid "RightUp"
msgstr "오른쪽 위"

#: Move.cpp:84
msgid "Out"
msgstr "아웃"

#: BoardWidget.cpp:885 BoardWidget.cpp:926 BoardWidget.cpp:957
#, fuzzy, c-format
msgid "Board value: %1"
msgstr "보드 값"

#: AbTop.cpp:129
#, fuzzy
msgid "&Stop Search"
msgstr "탐색 중지"

#: AbTop.cpp:132
#, fuzzy
msgid "Take &Back"
msgstr "뒤로 가기"

#: AbTop.cpp:136
#, fuzzy
msgid "&Forward"
msgstr "고급"

#: AbTop.cpp:140
#, fuzzy
msgid "&Hint"
msgstr "힌트"

#: AbTop.cpp:146
#, fuzzy
msgid "&Restore Position"
msgstr "위치 복원"

#: AbTop.cpp:151
#, fuzzy
msgid "&Save Position"
msgstr "위치 저장"

#: AbTop.cpp:158
msgid "&Network Play"
msgstr ""

#: AbTop.cpp:176
#, fuzzy
msgid "&Move Slow"
msgstr "천천히 움직임"

#: AbTop.cpp:180
msgid "&Render Balls"
msgstr ""

#: AbTop.cpp:184
#, fuzzy
msgid "&Spy"
msgstr "스파이"

#: AbTop.cpp:191
#, fuzzy
msgid "&Easy"
msgstr "초보자"

#: AbTop.cpp:197
#, fuzzy
msgid "&Normal"
msgstr "보통"

#: AbTop.cpp:203
#, fuzzy
msgid "&Hard"
msgstr "고급"

#: AbTop.cpp:209
#, fuzzy
msgid "&Challange"
msgstr "전문가"

#: AbTop.cpp:215
#, fuzzy
msgid "&Red"
msgstr "빨강"

#: AbTop.cpp:221
#, fuzzy
msgid "&Yellow"
msgstr "노랑"

#: AbTop.cpp:227
#, fuzzy
msgid "&Both"
msgstr "모두"

#: AbTop.cpp:233
msgid "&None"
msgstr ""

#: AbTop.cpp:567 AbTop.cpp:671
#, fuzzy
msgid "Press %1 for a new game"
msgstr "게임을 하려면 F2를 누르세요"

#: AbTop.cpp:592 AbTop.cpp:651 AbTop.cpp:655
#, fuzzy, c-format
msgid "Move %1"
msgstr "이동"

#: AbTop.cpp:611 Spy.cpp:79
msgid "Spy"
msgstr "스파이"

#: AbTop.cpp:664 AbTop.cpp:684
msgid "Red"
msgstr "빨강"

#: AbTop.cpp:665 AbTop.cpp:684
msgid "Yellow"
msgstr "노랑"

#: AbTop.cpp:677
#, fuzzy
msgid "Red won"
msgstr "빨강"

#: AbTop.cpp:677
#, fuzzy
msgid "Yellow won"
msgstr "노랑"

#: AbTop.cpp:686
msgid "I am thinking..."
msgstr "생각 중입니다..."

#: AbTop.cpp:686
#, fuzzy
msgid "It's your turn!"
msgstr "당신이 둘 차례입니다!"

#: kabalone.cpp:16
msgid "KDE Implementation of Abalone"
msgstr ""

#: kabalone.cpp:21
#, fuzzy
msgid "Use 'host' for network game"
msgstr "게임을 하려면 F2를 누르세요"

#: kabalone.cpp:23
#, fuzzy
msgid "Use 'port' for network game"
msgstr "게임을 하려면 F2를 누르세요"

#: kabalone.cpp:29
#, fuzzy
msgid "KAbalone"
msgstr "아발론"

#: Spy.cpp:25
msgid "Actual examined position:"
msgstr "실제 조사된 위치:"

#: Spy.cpp:50
msgid "Best move so far:"
msgstr "지금까지 최고의 이동:"

#: EvalDlgImpl.cpp:36
msgid "Current"
msgstr ""

#: EvalDlgImpl.cpp:250
msgid "Save Scheme as ..."
msgstr ""

#: EvalDlgImpl.cpp:251
msgid "Name for Scheme"
msgstr ""

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#~ msgid "Right"
#~ msgstr "오른쪽"

#~ msgid "Left"
#~ msgstr "왼쪽"

#~ msgid "Side"
#~ msgstr "측면"

#~ msgid "Stop Search"
#~ msgstr "탐색 중지"

#~ msgid "Hint"
#~ msgstr "힌트"

#~ msgid "Take back"
#~ msgstr "뒤로 가기"

#~ msgid "Restore Position"
#~ msgstr "위치 복원"

#~ msgid "Easy"
#~ msgstr "초보자"

#~ msgid "Hard"
#~ msgstr "고급"

#~ msgid "Challange"
#~ msgstr "전문가"

#~ msgid "Both"
#~ msgstr "모두"

#~ msgid "Abalone"
#~ msgstr "아발론"

#~ msgid ""
#~ "\n"
#~ "\n"
#~ "by Josef Weidendorfer"
#~ msgstr ""
#~ "\n"
#~ "\n"
#~ "만든이: Josef Weidendorfer"

#~ msgid "won"
#~ msgstr "승리"

#~ msgid "Spion"
#~ msgstr "스파이"
