# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Sae-keun Kim <segni@susekorea.net>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-02-13 03:05+0900\n"
"Last-Translator: Sae-keun Kim <segni@susekorea.net>\n"
"Language-Team: Korean <kde-i18n@kldp.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8 bit\n"

#: basictab.cpp:57
msgid "Name"
msgstr "이름"

#: basictab.cpp:58
msgid "Comment"
msgstr "주석"

#: basictab.cpp:59
msgid "Command"
msgstr "명령"

#: basictab.cpp:60
msgid "Type"
msgstr "형식"

#: basictab.cpp:93
msgid "Work Path"
msgstr "작업 경로"

#: basictab.cpp:114
msgid "Run in terminal"
msgstr "테미널에서 실행"

#: basictab.cpp:119
msgid "Terminal Options"
msgstr "터미널 옵션"

#: basictab.cpp:134
msgid "Run as a different user"
msgstr "다른 사용자로 실행"

#: basictab.cpp:139
msgid "Username"
msgstr "사용자 이름"

#: basictab.cpp:161
msgid "Current key"
msgstr "현재 키"

#: desktopfileeditor.cpp:46
msgid "&Reset"
msgstr "초기화(&R)"

#: kmenuedit.cpp:36
msgid "Edit K Menu"
msgstr "K 차림표 편집"

#: kmenuedit.cpp:66
msgid "&New Submenu"
msgstr "새로운 하위 메뉴(&N)"

#: kmenuedit.cpp:67
msgid "New &Item"
msgstr "새로운 항목(&I)"

#: main.cpp:30
msgid "KDE Menu editor"
msgstr "KDE 차림표 편집기"

#: main.cpp:35
msgid "KDE Menu Editor"
msgstr "KDE 차림표 편집기"

#: main.cpp:38
msgid "Maintainer"
msgstr ""

#: main.cpp:39
msgid "Original Author"
msgstr ""

#: namedlg.cpp:33
msgid "Name:"
msgstr "이름:"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Sae-keun Kim"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "segni@susekorea.net"

#: treeview.cpp:573
msgid "NewSubmenu"
msgstr "새로운 하위차림표"

#: treeview.cpp:651
msgid "NewItem"
msgstr "새로운 항목"

#~ msgid "General"
#~ msgstr "일반적인"

#~ msgid "Advanced"
#~ msgstr "진보적인"

#~ msgid "&Apply"
#~ msgstr "적용(&A)"
