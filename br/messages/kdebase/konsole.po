# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Jañ-Mai Drapier <jdrapier@club-internet.fr>, 1999
#
msgid ""
msgstr ""
"Project-Id-Version: konsole-1.1\n"
"POT-Creation-Date: 2001-09-03 09:43+0200\n"
"PO-Revision-Date: 1999-02-01 17:21+0100\n"
"Last-Translator: Jañ-Mai Drapier <jdrapier@club-internet.fr>\n"
"Language-Team: Brezhoneg <Suav.Icb@wanadoo.fr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: rc.cpp:1
msgid "MINIDRAW Tools"
msgstr ""

#: konsole.C:342
msgid "Suspend Task"
msgstr ""

#: konsole.C:343
msgid "Continue Task"
msgstr ""

#: konsole.C:344
msgid "Hangup"
msgstr ""

#: konsole.C:345
msgid "Interrupt Task"
msgstr ""

#: konsole.C:346
msgid "Terminate Task"
msgstr ""

#: konsole.C:347
msgid "Kill Task"
msgstr ""

#: konsole.C:352
msgid "Send Signal"
msgstr ""

#: konsole.C:353
msgid "R&ename session..."
msgstr ""

#: konsole.C:376
#, fuzzy
msgid "&Locale"
msgstr "&Bras"

#: konsole.C:395
#, fuzzy
msgid "Show &Frame"
msgstr "&Stern"

#: konsole.C:400
#, fuzzy
msgid "Scrollbar"
msgstr "Barrenn &dibunañ"

#: konsole.C:403
msgid "&Hide"
msgstr "&Kuzhat"

#: konsole.C:403
msgid "&Left"
msgstr "A-&gleiz"

#: konsole.C:403
#, fuzzy
msgid "&Right"
msgstr "A-&zehou"

#: konsole.C:409
msgid "F&ull-Screen"
msgstr ""

#: konsole.C:414
#, fuzzy
msgid "Size"
msgstr "&Ment"

#: konsole.C:417
#, fuzzy
msgid "40x15 (&Small)"
msgstr "40x15 (&bihan)"

#: konsole.C:418
#, fuzzy
msgid "80x24 (&VT100)"
msgstr "80x24 (&vt100)"

#: konsole.C:419
#, fuzzy
msgid "80x25 (&IBM PC)"
msgstr "80x25 (&ibmpc)"

#: konsole.C:420
#, fuzzy
msgid "80x40 (&XTerm)"
msgstr "80x40 (&xterm)"

#: konsole.C:421
msgid "80x52 (IBM V&GA)"
msgstr ""

#: konsole.C:429
msgid "&Normal"
msgstr "&Boas"

#: konsole.C:430
msgid "&Tiny"
msgstr ""

#: konsole.C:431
msgid "&Small"
msgstr "&Korr"

#: konsole.C:432
msgid "&Medium"
msgstr "K&renn"

#: konsole.C:433
msgid "&Large"
msgstr "&Bras"

#: konsole.C:434
msgid "&Huge"
msgstr "R&amz"

#: konsole.C:436
msgid "&Linux"
msgstr "&Linux"

#: konsole.C:437
#, fuzzy
msgid "&Unicode"
msgstr "&Kuzhat"

#: konsole.C:439
#, fuzzy
msgid "&Custom..."
msgstr "&Diwar-benn ..."

#: konsole.C:444
#, fuzzy
msgid "Schema"
msgstr "&Steuñv"

#: konsole.C:447
#, fuzzy
msgid "History..."
msgstr "&Diwar-benn ..."

#: konsole.C:452
msgid "&Codec"
msgstr ""

#: konsole.C:453
msgid "&Keyboard"
msgstr ""

#: konsole.C:455
msgid "Word Separators..."
msgstr ""

#: konsole.C:467
msgid "&Warn for Open Sessions on Quit"
msgstr ""

#: konsole.C:476
#, fuzzy
msgid "Save &Settings"
msgstr "&Enrollañ an dibaboù"

#: konsole.C:539
msgid "&New"
msgstr ""

#: konsole.C:568
msgid "Sessions"
msgstr "Dalc'hoù"

#: konsole.C:569
#, fuzzy
msgid "Settings"
msgstr "Dalc'hoù"

#: konsole.C:584
msgid ""
"You have open sessions (besides the current one).\n"
"These will be killed if you continue.\n"
"\n"
"Are you sure you want to quit?"
msgstr ""

#: konsole.C:989
#, fuzzy
msgid ""
"Font `%1' not found.\n"
"Check README.linux.console for help."
msgstr ""
"Nodrezh `%s' digavus.\n"
"Gwiriit README.linux.console evit skoazell."

#: konsole.C:1009
msgid "Use the right mouse button to bring back the menu"
msgstr ""

#: konsole.C:1230
msgid "%1 No %2"
msgstr ""

#: konsole.C:1443
#, fuzzy
msgid "`%1' terminated abnormally."
msgstr "`%s' echu direizh."

#: konsole.C:1446
msgid ""
"\n"
"Return code = "
msgstr ""
"\n"
"Kod distro = "

#: konsole.C:1543
#, c-format
msgid ""
"_: Screen is a program controlling screens!\n"
"Screen at %1"
msgstr ""

#: konsole.C:1670
#, fuzzy
msgid "Session name"
msgstr "Dalc'hoù"

#: konsole.C:1697
msgid "History Configuration"
msgstr ""

#: konsole.C:1705
msgid "Enable"
msgstr ""

#: konsole.C:1712
msgid ""
"_: Unlimited (number of lines)\n"
"Unlimited"
msgstr ""

#: konsole.C:1715
msgid "Number of lines : "
msgstr ""

#: konsole.C:1792
msgid ""
"Characters other than alphanumerics considered part of a word when double "
"clicking"
msgstr ""

#: konsole_part.C:91 main.C:123
msgid "Konsole"
msgstr ""

#: kwrited.C:87
msgid "KDE Daemon for receiving 'write' messages."
msgstr ""

#: kwrited.C:94
msgid "kwrited is already running."
msgstr ""

#: main.C:33
msgid "X terminal for use with KDE."
msgstr ""

#: main.C:38
msgid "Set Window Class"
msgstr ""

#: main.C:39
msgid "Start login shell"
msgstr ""

#: main.C:40
msgid "Suppress greeting"
msgstr ""

#: main.C:41
msgid "Set the window title"
msgstr ""

#: main.C:42
msgid "ignored"
msgstr ""

#: main.C:43
msgid "Do not save lines in scroll-back buffer"
msgstr ""

#: main.C:44
msgid "Do not display toolbar"
msgstr ""

#: main.C:45
msgid "Do not use XFT (Anti-Aliasing)"
msgstr ""

#: main.C:46
msgid "Terminal size in columns x lines"
msgstr ""

#: main.C:47
msgid "Open the given session type instead of the default shell"
msgstr ""

#: main.C:48
msgid "Change working directory of the konsole to 'dir'"
msgstr ""

#: main.C:49
msgid "Execute 'command' instead of shell"
msgstr ""

#: main.C:51
msgid "Arguments for 'command'"
msgstr ""

#: main.C:126
msgid "Maintainer"
msgstr ""

#: main.C:127
msgid "Author"
msgstr ""

#: main.C:129 main.C:159 main.C:162 main.C:165 main.C:168
msgid "bug fixing"
msgstr ""

#: main.C:132
msgid "decent marking"
msgstr ""

#: main.C:135
msgid ""
"partification\n"
"Toolbar and session names"
msgstr ""

#: main.C:139
msgid ""
"partification\n"
"overall improvements"
msgstr ""

#: main.C:143
msgid "transparency"
msgstr ""

#: main.C:146
msgid ""
"most of main.C donated via kvt\n"
"overall improvements"
msgstr ""

#: main.C:150
msgid "schema and selection improvements"
msgstr ""

#: main.C:153
msgid "SGI Port"
msgstr ""

#: main.C:156
msgid "FreeBSD port"
msgstr ""

#: main.C:171
msgid "faster startup, bug fixing"
msgstr ""

#: main.C:174
msgid "Solaris support and work on history"
msgstr ""

#: main.C:176
msgid ""
"Thanks to many others.\n"
"The above list only reflects the contributors\n"
"I managed to keep track of."
msgstr ""

#: main.C:207
msgid ""
"You can't use BOTH -ls and -e.\n"
msgstr ""

#: main.C:257
msgid ""
"expected --vt_sz <#columns>x<#lines> ie. 80x40\n"
msgstr ""

#: main.C:344
msgid "Welcome to the console"
msgstr "Degemer mat war al letrin"

#: schema.C:168 schema.C:198
msgid "[no title]"
msgstr ""

#: schema.C:211
msgid "Konsole Default"
msgstr ""

#: schemas.C:1
msgid "Black on Light Yellow"
msgstr ""

#: schemas.C:2
msgid "Black on White"
msgstr ""

#: schemas.C:3
msgid "Marble"
msgstr ""

#: schemas.C:4
msgid "Green on Black"
msgstr ""

#: schemas.C:5
msgid "Green Tint with Transparent MC"
msgstr ""

#: schemas.C:6
msgid "Green Tint"
msgstr ""

#: schemas.C:7
msgid "Paper"
msgstr ""

#: schemas.C:8
msgid "Linux Colors"
msgstr ""

#: schemas.C:9
msgid "System Colors"
msgstr ""

#: schemas.C:10
msgid "Transparent, Dark Background"
msgstr ""

#: schemas.C:11
msgid "Transparent, Light Background"
msgstr ""

#: schemas.C:12
msgid "Transparent for MC"
msgstr ""

#: schemas.C:13
msgid "Transparent Konsole"
msgstr ""

#: schemas.C:14
msgid "VIM Colors"
msgstr ""

#: schemas.C:15
msgid "White on Black"
msgstr ""

#: schemas.C:16
msgid "XTerm Colors"
msgstr ""

#: schemas.C:17
msgid "XTerm (XFree 4.x.x)"
msgstr ""

#: schemas.C:19
msgid "vt100 (historical)"
msgstr ""

#: schemas.C:20
msgid "VT420PC"
msgstr ""

#: schemas.C:21
msgid "XTerm (XFree 3.x.x)"
msgstr ""

#: TEWidget.C:1099
msgid "cd"
msgstr "kemmañ renkell"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#, fuzzy
#~ msgid "&Technical Reference"
#~ msgstr "&Dave kalvezel ..."

#~ msgid "80x52 (ibmv&ga)"
#~ msgstr "80x52 (ibmv&ga)"

#, fuzzy
#~ msgid ""
#~ "%1 version %2 - an X terminal\n"
#~ "Copyright (c) 1997-2000 by\n"
#~ "Lars Doelle <lars.doelle@on-line.de>\n"
#~ "\n"
#~ "This program is free software under the\n"
#~ "terms of the GNU General Public License\n"
#~ "and comes WITHOUT ANY WARRANTY.\n"
#~ "See 'LICENSE.readme' for details."
#~ msgstr ""
#~ "%s doare %s - un dermenell X\n"
#~ "\n"
#~ "Gwirioù eilañ (c) 1998 gant Lars Doelle <lars.doelle@on-line.de>\n"
#~ "\n"
#~ "Ar goulev-mañ a zo ur meziant frank hervez\n"
#~ "termennadur an Aotre Arzel ha dont a ra er-maez\n"
#~ "HEP AN DISTERAÑ KRETADUR.\n"
#~ "Gwelit `LICENSE.readme´ evit munudoù."

#~ msgid "&Menubar"
#~ msgstr "&Barenn al lañser"

#, fuzzy
#~ msgid "&Font"
#~ msgstr "&Nodrezh"

#~ msgid "BS sends &DEL"
#~ msgstr "War-gil a gas &DEL"

#~ msgid "&About ..."
#~ msgstr "&Diwar-benn ..."

#~ msgid "&User's Manual ..."
#~ msgstr "&Dornlevr implij ..."

#~ msgid "Linux (small)"
#~ msgstr "Linux (bihan)"

#~ msgid "&More ..."
#~ msgstr "&Muioc'h..."

#, fuzzy
#~ msgid "About %1"
#~ msgstr "Diwar-benn %s"
