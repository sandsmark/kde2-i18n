# KTranslator Generated File
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Romuald Texier <texierr@worldnet.fr>, 1999
#
msgid ""
msgstr ""
"Project-Id-Version: kpaint-1.1\n"
"POT-Creation-Date: 2001-07-26 23:45+0200\n"
"PO-Revision-Date: Thu Feb 11 1999 07:27:40+0200\n"
"Last-Translator: Romuald Texier <texierr@worldnet.fr>\n"
"Language-Team: Brezhoneg <Suav.Icb@wanadoo.fr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KTranslator v 0.4.0\n"

#: areaselect.cpp:25
msgid "Area Selection"
msgstr "Diuzadenn un dachenn"

#: canvassize.cpp:18
#, fuzzy
msgid "New Canvas Size"
msgstr "Ment ar lien"

#: canvassize.cpp:28
msgid "Width:"
msgstr "Ledander :"

#: canvassize.cpp:29
msgid "Height:"
msgstr "Uhelder :"

#: canvassize.cpp:39
msgid "Keep picture ratio"
msgstr ""

#: circle.cpp:18 circle.cpp:25 manager.cpp:70
msgid "Circle"
msgstr "Kelc'h"

#: depthdialog.cpp:16
#, fuzzy
msgid "Color depth"
msgstr "Donder Liv :"

#: depthdialog.cpp:24
msgid "Image Depth:"
msgstr "Donder ar skeudenn"

#: depthdialog.cpp:31
#, fuzzy
msgid "1 (2 Colors)"
msgstr "1 (2 liv)"

#: depthdialog.cpp:32
#, fuzzy
msgid "4 (16 Colors)"
msgstr "4 (16 liv)"

#: depthdialog.cpp:33
#, fuzzy
msgid "8 (256 Colors)"
msgstr "8 (256 liv)"

#: depthdialog.cpp:34
#, fuzzy
msgid "15 (32k Colors)"
msgstr "15 (32k liv)"

#: depthdialog.cpp:35
#, fuzzy
msgid "16 (64k Colors)"
msgstr "16 (64k liv)"

#: depthdialog.cpp:36
#, fuzzy
msgid "24 (True Color)"
msgstr "24 (livioù gwirion)"

#: depthdialog.cpp:37
#, fuzzy
msgid "32 (True Color)"
msgstr "32 (livioù gwirion)"

#: ellipse.cpp:16 ellipse.cpp:23 manager.cpp:68
msgid "Ellipse"
msgstr "Elipsenn"

#: infodialog.cpp:49
#, fuzzy
msgid "Color Depth:"
msgstr "Donder Liv :"

#: infodialog.cpp:61
#, fuzzy
msgid "Colors Used:"
msgstr "Livioù implijet :"

#: infodialog.cpp:78
msgid "Image Width:"
msgstr "Ledander ar skeudenn :"

#: infodialog.cpp:89
msgid "Image Height:"
msgstr "Uhelder ar skeudenn :"

#: infodialog.cpp:105 palettedialog.cpp:16
msgid "Dismiss"
msgstr "Dilezel"

#: kpaint.cpp:106
msgid ""
"The image has been modified.\n"
"Would you like to save it?"
msgstr ""

#: kpaint.cpp:393
#, fuzzy
msgid "Paste &image"
msgstr "Pegañ evel skeudenn"

#: kpaint.cpp:427
#, fuzzy
msgid "&Information..."
msgstr "Titouroù..."

#: kpaint.cpp:430
#, fuzzy
msgid "&Resize..."
msgstr "Adventañ..."

#: kpaint.cpp:433
#, fuzzy
msgid "&Edit palette..."
msgstr "Aozañ paletenn..."

#: kpaint.cpp:436
#, fuzzy
msgid "&Change color depth..."
msgstr "Kemm donder livioù..."

#: kpaint.cpp:447
#, fuzzy
msgid "&New window"
msgstr "Prenestr nevez"

#: kpaint.cpp:450
#, fuzzy
msgid "&Close window"
msgstr "Serriñ ar prenestr"

#: kpaint.cpp:456
#, fuzzy
msgid "&Tool properties..."
msgstr "Perzhioù ar benveg..."

#: kpaint.cpp:550
#, fuzzy
msgid ""
"KPaint could not open %1 \n"
msgstr "KPaint: ne m'eus ket gallet digeriñ ar restr"

#: kpaint.cpp:694
#, fuzzy
msgid "untitled"
msgstr "diditl.gif"

#: line.cpp:19 manager.cpp:74
msgid "Line"
msgstr "Linenn"

#: main.cpp:20
msgid "KDE Paint Program"
msgstr ""

#: main.cpp:24
msgid "File or URL to open"
msgstr ""

#: main.cpp:30
msgid "KPaint"
msgstr ""

#: manager.cpp:72 pen.cpp:17
msgid "Pen"
msgstr "Kraion"

#: manager.cpp:76 rectangle.cpp:17
msgid "Rectangle"
msgstr "Hirgarrezenn"

#: manager.cpp:78
msgid "Round Angle"
msgstr "Korn round"

#: manager.cpp:80 spraycan.cpp:19
msgid "Spray Can"
msgstr "Bombezenn"

#: manager.cpp:82
msgid "Text"
msgstr "Skrid"

#: manager.cpp:85
msgid "Area Select"
msgstr "Diuz un dachenn"

#: properties.cpp:21
#, fuzzy
msgid "Tool properties"
msgstr "Perzhioù ar binvioù"

#: properties.cpp:24
msgid "Line Properties"
msgstr "Perzhioù al linenn"

#: properties.cpp:26
msgid "Fill Properties"
msgstr "Perzhioù al leuniañ"

#: properties.cpp:72 properties.cpp:127
msgid "Fill Pattern:"
msgstr "Tres leuniañ"

#: properties.cpp:78 properties.cpp:133 properties.cpp:299 properties.cpp:359
msgid "None"
msgstr "Hini ebet"

#: properties.cpp:79 properties.cpp:134 properties.cpp:294 properties.cpp:354
msgid "Solid"
msgstr "Leun"

#: properties.cpp:80 properties.cpp:135
msgid "Vertical Lines"
msgstr "Linennoù a-serzh"

#: properties.cpp:81 properties.cpp:136
msgid "Horizontal Lines"
msgstr "Linennoù a-led"

#: properties.cpp:82 properties.cpp:137
msgid "Grid"
msgstr "Kael"

#: properties.cpp:83 properties.cpp:138
msgid "Diagonal Lines //"
msgstr "Linennoù a-dreuz //"

#: properties.cpp:84 properties.cpp:139
msgid "Diagonal Lines \\"
msgstr "Linennoù a-dreuz \\"

#: properties.cpp:85 properties.cpp:140
msgid "Diagonal Grid"
msgstr "Kael a-dreuz"

#: properties.cpp:95
msgid "Custom"
msgstr "Diouzhoc'h"

#: properties.cpp:98 properties.cpp:153
msgid "Pattern Filename:"
msgstr "Anv restr ar c'haeloù"

#: properties.cpp:108
#, fuzzy
msgid "Set Fill Color..."
msgstr "Lakaat liv al leuniañ"

#: properties.cpp:289 properties.cpp:350
msgid "Line Style:"
msgstr "Doare al linennoù"

#: properties.cpp:295 properties.cpp:355
msgid "Dashed"
msgstr "Tiredoù"

#: properties.cpp:296 properties.cpp:356
msgid "Dotted"
msgstr "Pikoù"

#: properties.cpp:297 properties.cpp:357
msgid "Dash Dot"
msgstr "Pikoù tiredoù"

#: properties.cpp:298 properties.cpp:358
msgid "Dash Dot Dot"
msgstr "Tired pik pik"

#: properties.cpp:303 properties.cpp:362
msgid "Line Width:"
msgstr "Ledander al linenn :"

#: properties.cpp:313 properties.cpp:372
msgid "End style:"
msgstr "Doare ar dibenn :"

#: properties.cpp:318 properties.cpp:331 properties.cpp:376 properties.cpp:383
msgid "Not Yet"
msgstr "Ket c'hoazh"

#: properties.cpp:325 properties.cpp:379
msgid "Join style:"
msgstr "Doare an teuziñ :"

#: properties.cpp:337
#, fuzzy
msgid "Set Line Color..."
msgstr "Spisaat liv al linenn..."

#: rc.cpp:4
msgid "&Image"
msgstr "&Skeudenn"

#: rc.cpp:5
#, fuzzy
msgid "Tools Toolbar"
msgstr "Diskouez barrenn ar binvioù"

#: roundangle.cpp:19
msgid "Roundangle"
msgstr "Korn ront"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#, fuzzy
#~ msgid "&Mask..."
#~ msgstr "Maskl..."

#, fuzzy
#~ msgid ""
#~ "You have unsaved changes. You will lose them if you close this window now."
#~ msgstr ""
#~ "Kemmoù dienroll hoc'h eus, a vo kollet ma serrit ar prenestr-mañ bremañ."

#~ msgid "Unsaved Changes"
#~ msgstr "Kemmoù dienroll"

#, fuzzy
#~ msgid "&Window"
#~ msgstr "Prenestr nevez"

#, fuzzy
#~ msgid "Zoom"
#~ msgstr "Tostoc'h"

#, fuzzy
#~ msgid "Paste Tool"
#~ msgstr "Pegañ an takad"

#, fuzzy
#~ msgid ""
#~ "Could not open file\n"
#~ "KPaint could not open the specified file."
#~ msgstr ""
#~ "Fazi : ne m'eus ket gallet digeriñ ar restr\n"
#~ "N'en deus ket gallet KPaint digeriñ ar restr roet."

#, fuzzy
#~ msgid ""
#~ "Unknown Format\n"
#~ "KPaint does not understand the format of the specified file\n"
#~ "or the file is corrupt."
#~ msgstr ""
#~ "Skoilh : Furmad dianav\n"
#~ "Ne gompren ket KPaint furmad ar restr roet\n"
#~ "pe eo brein ar restr."

#, fuzzy
#~ msgid "Unknown Format"
#~ msgstr "KPaint : Furmad dianav"

#, fuzzy
#~ msgid "Edit Color"
#~ msgstr "Aozañ al livioù"

#, fuzzy
#~ msgid "Open &URL..."
#~ msgstr "Digeriñ diwar URL..."

#, fuzzy
#~ msgid "Save &URL..."
#~ msgstr "Enrollañ war URL"

#~ msgid "New Canvas"
#~ msgstr "Lien nevez"

#~ msgid "Open File"
#~ msgstr "Digeriñ ur restr"

#~ msgid "Save File"
#~ msgstr "Enrollañ ar restr"

#~ msgid "Zoom Out"
#~ msgstr "Pelloc'h"

#~ msgid "Open Image..."
#~ msgstr "Digeriñ ur skeudenn..."

#~ msgid "New Image..."
#~ msgstr "Skeudenn nevez..."

#~ msgid "Save Image"
#~ msgstr "Enrollañ ar skeudenn"

#~ msgid "Save Image As..."
#~ msgstr "Enrollañ ar skeudenn e..."

#~ msgid "Copy Region"
#~ msgstr "Eilañ an takad"

#~ msgid "Cut Region"
#~ msgstr "Troc'hañ an takad"

#~ msgid "Show Commands Toolbar"
#~ msgstr "Diskouez ar barrenn urzhiañ"

#~ msgid "Show Status Bar"
#~ msgstr "Diskouez barrenn a stad"

#~ msgid "Save Options"
#~ msgstr "Enrollañ an dibaboù"

#, fuzzy
#~ msgid ""
#~ "KPaint version %1\n"
#~ "\n"
#~ "(c) %2\n"
#~ "\n"
#~ "KPaint is released under the\n"
#~ "GNU Public License agreement,\n"
#~ "see the online documentation\n"
#~ "for details."
#~ msgstr ""
#~ "KPaint doare %s\n"
#~ "\n"
#~ "(c) %s\n"
#~ "\n"
#~ "Embannet e vez KPaint dindan\n"
#~ "ar GNU Public License,\n"
#~ "sellit ouzh an teuliadur enlinenn\n"
#~ "evit munudoù."

#~ msgid "&Tool"
#~ msgstr "&Benveg"

#, fuzzy
#~ msgid "Cut"
#~ msgstr "Diouzhoc'h"

#, fuzzy
#~ msgid "&Options"
#~ msgstr "Enrollañ an dibaboù"

#~ msgid "File Format:"
#~ msgstr "Furmad ar restr :"

#~ msgid "Image Format..."
#~ msgstr "Furmad ar skeudenn..."
