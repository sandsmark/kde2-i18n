# Danish translations for KDE Multimedia/kmidi
# Copyright (C) 1998.
# Erik Kjær Pedersen <erik@binghamton.edu>, 1999.
#
msgid ""
msgstr ""
"Project-Id-Version: KDE MULTIMEDIA/kmidi\n"
"POT-Creation-Date: 2001-06-01 12:33+0200\n"
"PO-Revision-Date: 2001-07-19 19:12GMT\n"
"Last-Translator: Erik Kjær Pedersen <erik@binghamton.edu>\n"
"Language-Team: danish <da@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.3\n"

#: configdlg.cpp:107
msgid ""
"The led color is used in<br>\n"
"the panel and meter."
msgstr ""
"Lysets farve bruges i<br>\n"
"panelet og måleren."

#: configdlg.cpp:110
msgid "LED Color:"
msgstr "LED-farve:"

#: configdlg.cpp:118
msgid ""
"This color is for<br>\n"
"the panel and meter backgrounds."
msgstr ""
"Denne farve er til<br>\n"
"panelet og målerbaggrunde."

#: configdlg.cpp:121
msgid "Background Color:"
msgstr "Baggrundsfarve:"

#: configdlg.cpp:129
msgid "Show Tool Tips"
msgstr "Vis værktøjsvink"

#: configdlg.cpp:134
msgid ""
"Provide brief descriptions<br>\n"
"when cursor is left over a<br>\n"
"object on the screen"
msgstr ""
"Giv korte beskrivelser<br>\n"
"når markøren er et stykke tid over<br>\n"
"et objekt på skærmen"

#: configdlg.cpp:138
msgid ""
"Try to stop KMidi from<br>\n"
"using up too much of your RAM."
msgstr ""
"Prøv at forhindre KMidi i<br>\n"
"at bruge for meget af din ram."

#: configdlg.cpp:159
msgid "About KMidi"
msgstr "Om KMidi"

#: configdlg.cpp:162
msgid "Initial developer."
msgstr "Oprindelig udvikler."

#: configdlg.cpp:167
msgid "lyrics, IW patches, current maintainer."
msgstr "tekster, IW patcher, aktuel vedligeholder."

#: configdlg.cpp:172
msgid "TiMidity sound, patch loader, interface design, ..."
msgstr "TiMidity lyd, patch indlæser, brugerflade design, ..."

#: configdlg.cpp:175
msgid "soundfonts, window communication"
msgstr "lydfonte, vindueskommunikation"

#: configdlg.cpp:178
msgid "effects: reverb, chorus, phaser, celeste"
msgstr "effekter, efterklang, kor, faser, celeste"

#: configdlg.cpp:181
msgid "mod wheel, portamento ..."
msgstr "mod wheel, portamento ..."

#: configdlg.cpp:184
msgid "drivers, interfaces, configure scripts"
msgstr "drivere, brugerflader, indstil skripter"

#: configdlg.cpp:205
msgid "Where to get sets of patches."
msgstr "Hvor man henter et sæt patcher."

#: configdlg.cpp:208
msgid "links to many sf2 soundfonts"
msgstr "henvisninger til mange sf2 lydfonte"

#: configdlg.cpp:212
msgid "GUS patches -- unrar needed to decompress."
msgstr "GUS patches -- unrar nødvending for at dekomprimere."

#: configdlg.cpp:221
msgid "You may need this msdos-only tool."
msgstr "Muligvis behøves dette ms-dos værktøj."

#: docking.cpp:45 kmidi.cpp:307 kmidi.cpp:745
msgid "Play/Pause"
msgstr "Spil/pause"

#: docking.cpp:49 kmidi.cpp:778
msgid "Next"
msgstr "Næste"

#: docking.cpp:50
msgid "Previous"
msgstr "Forrige"

#: docking.cpp:51
msgid "Eject"
msgstr "Skub ud"

#: kmidi.cpp:171
msgid "Could not load folder.bmp"
msgstr "Kunne ikke indlæse folder.bmp"

#: kmidi.cpp:174
msgid "Could not load cdup.bmp"
msgstr "Kunne ikke indlæse cdup.bmp"

#: kmidi.cpp:177
msgid "Could not load file.bmp"
msgstr "Kunne ikke indlæse file.bmp"

#: kmidi.cpp:306
msgid "Bottom Panel"
msgstr "Bundpanel"

#: kmidi.cpp:309
msgid "Loop Song"
msgstr "Lad sangen gå i løkke"

#: kmidi.cpp:310
msgid "Fast Forward"
msgstr "Hurtigt fremad"

#: kmidi.cpp:311
msgid "Rewind"
msgstr "Spol tilbage"

#: kmidi.cpp:312
msgid "Next Midi"
msgstr "Næste midi"

#: kmidi.cpp:313
msgid "Previous Midi"
msgstr "Forrige midi"

#: kmidi.cpp:314
msgid "Exit Kmidi"
msgstr "Afslut kmidi"

#: kmidi.cpp:315
msgid "What's This?"
msgstr "Hvad er dette?"

#: kmidi.cpp:316 kmidi.cpp:320
msgid "Random Play"
msgstr "Tilfældig rækkefølge"

#: kmidi.cpp:317
msgid "Configure Kmidi"
msgstr "Indstil kmidi"

#: kmidi.cpp:318
msgid "Open Playlist"
msgstr "Åbn spilleliste"

#: kmidi.cpp:319
msgid "Show Info Window"
msgstr "Vis informationsvindue"

#: kmidi.cpp:321
msgid "Volume Control"
msgstr "Lydstyrkekontrol"

#: kmidi.cpp:322
msgid "Time Control"
msgstr "Tidskontrol"

#: kmidi.cpp:324
msgid "Select Patch Set"
msgstr "Vælg patch sæt"

#: kmidi.cpp:325
msgid "Select Song"
msgstr "Vælg sang"

#: kmidi.cpp:326
msgid "Select Playlist"
msgstr "Vælg spilleliste"

#: kmidi.cpp:327
msgid "Sync Meter"
msgstr "Sync måler"

#: kmidi.cpp:328
msgid "Stereo Voice: norm/xtra/off"
msgstr "Stereo stemme: norm/ekstra/fra"

#: kmidi.cpp:329
msgid "Echo: norm/xtra/off"
msgstr "Ekko: norm/ekstra/fra"

#: kmidi.cpp:330
msgid "Detune: norm/xtra/off"
msgstr "Detune: norm/ekstra/fra"

#: kmidi.cpp:331
msgid "Verbosity: norm/xtra/off"
msgstr "Verbositet: norm/ekstra/fra"

#: kmidi.cpp:332
msgid "Effects"
msgstr "Effekter"

#: kmidi.cpp:333
msgid "Set Polyphony"
msgstr "Sæt polyfoni"

#: kmidi.cpp:334
msgid "Filter"
msgstr "Filter"

#: kmidi.cpp:335
msgid "status"
msgstr "status"

#: kmidi.cpp:336
msgid "loading"
msgstr "indlæser"

#: kmidi.cpp:337
msgid "lyrics"
msgstr "tekst"

#: kmidi.cpp:338
msgid "buffer"
msgstr "buffer"

#: kmidi.cpp:339
msgid "interpolation"
msgstr "interpolation"

#: kmidi.cpp:340
msgid "reverb"
msgstr "efterklang"

#: kmidi.cpp:341
msgid "chorus"
msgstr "kor"

#: kmidi.cpp:342
msgid "linear interpolation"
msgstr "lineær interpolation"

#: kmidi.cpp:343
msgid "cspline"
msgstr "cspline"

#: kmidi.cpp:344
msgid "lagrange"
msgstr "lagrange"

#: kmidi.cpp:345
msgid "cspline + filter"
msgstr "cspline + filter"

#: kmidi.cpp:398
msgid "Vol:%03d%%"
msgstr "Vol:%03d%%"

#: kmidi.cpp:401 kmidi.cpp:707
msgid "Vol:%02d%%"
msgstr "Vol:%02d%%"

#: kmidi.cpp:606
msgid ""
"Open up or close lower<br>\n"
"part of panel with<br>\n"
"channel display and<br>\n"
"other controls"
msgstr ""
"Åbn eller luk den nedre<br>\n"
"del af panelet med<br>\n"
"kanalvisning eller<br>\n"
"andre kontroller"

#: kmidi.cpp:612
msgid "open up the play list editor"
msgstr "åbn for spillelisteredigering"

#: kmidi.cpp:614
msgid ""
"open up or close<br>\n"
"the display of info about the<br>\n"
"song being played"
msgstr ""
"åbn eller luk<br>\n"
"visning af info om den sang<br>\n"
"der spilles"

#: kmidi.cpp:624
msgid "exit from KMidi"
msgstr "Afslut KMidi"

#: kmidi.cpp:632
msgid "display of information"
msgstr "Vis information"

#: kmidi.cpp:652
msgid "what's happening now"
msgstr "Hvad sker der nu"

#: kmidi.cpp:682
msgid "select the next song randomly"
msgstr "vælg den næste sang tilfældigt"

#: kmidi.cpp:686
msgid ""
"open up or close<br>\n"
"the configuration window"
msgstr ""
"åbn eller luk<br>\n"
"for indstillingsvinduet"

#: kmidi.cpp:693
msgid "set playing time elapsed"
msgstr "sæt den spilletid der er forløbet"

#: kmidi.cpp:703
msgid "the current volume level"
msgstr "lydstyrke for øjeblikket"

#: kmidi.cpp:728 kmidi.cpp:1829 kmidi.cpp:1837
msgid "Song --/--"
msgstr "Sang --/--"

#: kmidi.cpp:736
msgid ""
"Adjust the volume here.<br>\n"
"(Keep it pretty low, and<br>\n"
"use <i>Kmix</i> to adjust<br>\n"
"the mixer volumes, if<br>\n"
"you want it louder.)"
msgstr ""
"Indstil lydstyrken her.<br>\n"
"(Lad den være temmelig lav<br> \n"
"og brug <i>Kmix</i>  til at justere<br>\n"
"mixerlydstyrken hvis du vil<br>\n"
"have den højere.)"

#: kmidi.cpp:747
msgid ""
"If playing, pause,<br>\n"
"else start playing."
msgstr ""
"Hvis der spilles,<br> \n"
"hold pause, ellers spil."

#: kmidi.cpp:753
msgid "Stop playing this song."
msgstr "Hold op med at spille denne sang."

#: kmidi.cpp:756
msgid "Replay"
msgstr "Spil igen"

#: kmidi.cpp:758
msgid ""
"Keep replaying<br>\n"
" the same song."
msgstr ""
"Bliv ved med at<br>\n"
"spille samme sang."

#: kmidi.cpp:762
msgid "Bwd"
msgstr "Tlb"

#: kmidi.cpp:763
msgid ""
"Go backwards to play<br>\n"
"an earlier part of the song."
msgstr ""
"Gå tilbage for at spille<br>\n"
"en tidligere del af sangen"

#: kmidi.cpp:766
msgid "Fwd"
msgstr "Frm"

#: kmidi.cpp:767
msgid ""
"Go forwards to play<br>\n"
"a later part of the song."
msgstr ""
"Gå fremad for at spille<br> \n"
"en senere del af sangen."

#: kmidi.cpp:774
msgid "Prev"
msgstr "Forrige"

#: kmidi.cpp:775
msgid ""
"Play the previous song<br>\n"
" on the play list."
msgstr ""
"Spil den forrige sang<br>\n"
"på spillelisten."

#: kmidi.cpp:779
msgid ""
"Play the next song<br>\n"
" on the play list."
msgstr ""
"Spil den næste sang<br>\n"
" på spillelisten."

#: kmidi.cpp:785
msgid ""
"These leds show<br>\n"
"the number of notes<br>\n"
"being played."
msgstr ""
"Disse lys viser<br> \n"
"antal noder der spilles."

#: kmidi.cpp:798
msgid ""
"The <i>status</i> led is<br>\n"
"<b>red</b> when KMidi is<br>\n"
"stopped, <b>yellow</b> when<br>\n"
"paused, otherwise:<br>\n"
"<b>blue</b> when looping on<br>\n"
"the same song<br>\n"
"<b>magenta</b> when selecting<br>\n"
"songs randomly<br>\n"
"or <b>green</b> when KMidi is<br>\n"
"playing or ready to play."
msgstr ""
"statuslyset er rødt<br> \n"
"når KMidi er standset<br>\n"
"gult når der holdes<br> \n"
"pause, ellers blåt<br> \n"
"når der spilles i løkke<br>\n"
"magenta når sange vælges<br>\n"
"i tilfældig rækkefølge<br>\n"
"grøn når KMidi spiller eller<br>\n"
"er parat til at spille."

#: kmidi.cpp:809
msgid ""
"The <i>loading</i> led<br>\n"
"blinks <b>yellow</b> when<br>\n"
"patches are being<br>\n"
"loaded from the hard drive."
msgstr ""
"Indlæselyset blinker<br>\n"
"gult når patches bliver<br>\n"
"indlæst fra disken."

#: kmidi.cpp:814
msgid ""
"The <i>lyrics</i> led<br>\n"
"lights up when a<br>\n"
"synchronized midi text<br>\n"
"event is being displayed<br>\n"
"in the <i>info</i> window."
msgstr ""
"Tekstlyset lyser op<br> \n"
"når en synkroniseret<br>\n"
"miditekstbegivenhed<br>\n"
" vises i <i>info</i> vinduet."

#: kmidi.cpp:820
msgid ""
"The <i>buffer</i> led<br>\n"
"is orangeish when there is<br>\n"
"danger of a dropout, but<br>\n"
"greenish when all is ok."
msgstr ""
"bufferlyset er orange<br>\n"
"når der er fare for et<br>\n"
"drop, grønt når alt er ok."

#: kmidi.cpp:825
msgid ""
"The <i>interpolation</i> led<br>\n"
"is on when c-spline or<br>\n"
"LaGrange interpolation<br>\n"
"is being used for resampling."
msgstr ""
"Interpolationslyset er slået til<br>\n"
" når c-spline eller LaGrange <br>\n"
" interpolation bruges til genforsøg."

#: kmidi.cpp:829
msgid ""
"The <i>echo</i> led<br>\n"
"is on when KMidi is<br>\n"
"not too busy to generate<br>\n"
"extra echo notes<br>\n"
"for reverberation effect.<br>\n"
"It's bright when you've<br>\n"
"asked for extra reverberation."
msgstr ""
"Ekkolyset er slået til når KMidi ikke har<br>\n"
"for travlt til at generere<br>\n"
"ekstra ekkonoder<br> \n"
"for en efterklangsseffekt.<br>\n"
"Det er lyst når du har bedt om<br>\n"
"ekstra efterklang."

#: kmidi.cpp:836
msgid ""
"The <i>detune</i> led<br>\n"
"is on when KMidi is<br>\n"
"not too busy to generate<br>\n"
"extra detuned notes<br>\n"
"for chorusing effect.<br>\n"
"It's bright when you've<br>\n"
"asked for extra chorusing."
msgstr ""
"Detuner lyset <br>\n"
"er slået til når KMidi ikke<br>\n"
"har for travlt til at generere<br>\n"
"ekstra detunede noder<br>\n"
"der giver koreffekt.<br>\n"
"Det er lyst når du har bedt<br>\n"
"om ekstra kor."

#: kmidi.cpp:846
msgid "ichecks"
msgstr "itjek"

#: kmidi.cpp:853
msgid "Resample using linear interpolation."
msgstr "Genforsøg ved brug af lineær interpolation."

#: kmidi.cpp:854
msgid "Resample using c-spline interpolation."
msgstr "Genforsøg ved brug af c-spline interpolation."

#: kmidi.cpp:855
msgid "Resample using LaGrange interpolation."
msgstr "Genforsøg ved brug af LaGrange interpolation."

#: kmidi.cpp:856
msgid ""
"Resample using c-spline<br>\n"
"interpolation with<br>\n"
"the low pass filter."
msgstr ""
"Genforsøg ved brug af c-spline<br>\n"
"interpolation med<br> \n"
"lavtgrænsefilter"

#: kmidi.cpp:866
msgid ""
"The display shows notes<br>\n"
" being played on the<br>\n"
"16 or 32 midi channels."
msgstr ""
"Der fremvises noder<br>\n"
" der spilles på de<br>\n"
"16 eller 32 midikanaler."

#: kmidi.cpp:888
msgid ""
"Here is information<br>\n"
"extracted from the midi<br>\n"
"file currently being played."
msgstr ""
"Her er information<br>\n"
"udtrukket fra den midi<br>\n"
"fil der spilles nu."

#: kmidi.cpp:902
msgid ""
"Choose a patchset.<br>\n"
"(You can add patchsets by<br>\n"
"downloading them from somewhere<br>\n"
"and editing the file<br>\n"
"$KDEDIR/share/apps/kmidi/<br>\n"
"config/timidity.cfg .)"
msgstr ""
"Vælg et patchsæt.<br>\n"
"(Du kan tilføje patchsæt ved<br>\n"
"at hente dem hjem fra et <br>\n"
"eller andet sted<br> \n"
"og redigere filen<br>\n"
"$KDEDIR/share/apps/kmidi/<br>\n"
"config/timidity.cfg.)"

#: kmidi.cpp:923
msgid ""
"Select a midi file<br>\n"
"to play from this<br>\n"
"play list."
msgstr ""
"Vælg en midifil<br>\n"
"til at spille fra<br>\n"
"denne spilleliste."

#: kmidi.cpp:932
msgid ""
"These are the playlist files<br>\n"
"you've created using<br>\n"
" the <i>playlist editor</i>.<br>\n"
"Click on one, and its<br>\n"
"contents will replace<br>\n"
"the play list above."
msgstr ""
"Disse er spillelistefiler<br>\n"
"du har lavet ved brug af <br>\n"
"spillelisteredigering.<br>\n"
"Klik på én, og dens<br>\n"
"indhold vil erstatte<br>\n"
"spillelisten ovenfor."

#: kmidi.cpp:972
msgid ""
"There are three settings:<br>\n"
"<b>off</b> for no extra stereo notes<br>\n"
"<b>no change</b> for normal stereo<br>\n"
"<b>checked/on</b> for notes panned<br>\n"
"left and right artificially."
msgstr ""
"Der er tre indstillinger:<br>\n"
"<b>fra</b> for ingen ekstra stereo noder<br>\n"
"<b>skift</b> for normal stereo<br>\n"
"<b>tjekket/på</b> for noder der smides<br>\n"
"kunstigt til venstre og højre."

#: kmidi.cpp:977
msgid ""
"There are three settings:<br>\n"
"<b>off</b> for no extra echo notes<br>\n"
"<b>no change</b> for echoing<br>\n"
"when a patch specifies<br>\n"
"reverberation, or<br>\n"
"<b>checked/on</b> for added reverb<br>\n"
"for all instruments."
msgstr ""
"Der er tre indstillinger:<br>\n"
"<b>fra</b> for ingen ekstra ekko<br>\n"
"<b> ingen ændring</b> for ekko<br>\n"
"når en patch angiver <br>\n"
"efterklang, eller<br>\n"
"tjekket/til</b> for ekstra efterklang<br>\n"
"for alle instrumenter. "

#: kmidi.cpp:983
msgid ""
"There are three settings:<br>\n"
"<b>off</b> for no extra detuned notes<br>\n"
"<b>no change</b> for detuned notes<br>\n"
"when a patch specifies<br>\n"
"chorusing, or<br>\n"
"<b>checked/on</b> for added chorusing<br>\n"
"for all instruments."
msgstr ""
"Der er tre indstillinger:<br>\n"
"<b>fra</b> for ingen ekstra detunede noder<br>\n"
"<b> ingen ændring</b> for detunede noder<br> \n"
"når en patch angiver <br>\n"
"kor, eller<b>tjekket/til</b> for ekstra kor<br>\n"
"for alle instrumenter. "

#: kmidi.cpp:989
msgid ""
"There are three settings:<br>\n"
"<b>off</b> for only name and lyrics<br>\n"
"shown in the <i>info</i> window;<br>\n"
"<b>no change</b> for all text<br>\n"
"messages to be shown, and<br>\n"
"<b>checked/on</b> to display also<br>\n"
"information about patches<br>\n"
"as they are loaded from disk."
msgstr ""
"Der er tre indstillinger:<br>\n"
"<b>fra</b> for kun navn og tekst<br>\n"
"vist i <i>info</i>vinduet;<br>\n"
"<b>ingen ændring</b>for at få alle tekst<br>\n"
"beskeder vist, og<br>\n"
"<b>tjekket/til</b>for også at vise<br>\n"
"information om patches<br>\n"
"når de bliver indlæst fra disken."

#: kmidi.cpp:1002
msgid ""
"When this button is down,<br>\n"
"filters are used for the<br>\n"
"<i>midi</i> channel effects<br>\n"
"<b>chorus</b>, <b>reverberation</b>,<br>\n"
"<b>celeste</b>, and <b>phaser</b>.<br>\n"
"When it is off, <b>chorus</b> is<br>\n"
"done instead with extra detuned<br>\n"
"notes, and <b>reverberation</b> is<br>\n"
"done with echo notes, but<br>\n"
"the other effects are not done."
msgstr ""
"Når denne knap er nede,<br>\n"
"bruges der filtre for<br><i>midi</i>kanal-effekter<br>\n"
"<b>kor</b>,<b>efterklang</b>,<br>\n"
"<b>celeste</b>, og <b>faser</b>.<br>\n"
"Når den er oppe, laves<b>kor</b> i stedet for<br>\n"
"med ekstra detunede noder, og<br>\n"
" <b>efterklang</b> laves med ekkonoder,<br>\n"
"og<br> de andre effekter laves ikke."

#: kmidi.cpp:1020
msgid ""
"Use this to set the maximum<br>\n"
"notes that can be played<br>\n"
"at one time.  Use a lower<br>\n"
"setting to avoid dropouts."
msgstr ""
"Brug dette til at indstille maksimalt antal<br> \n"
"noder der kan spilles på én gang. Brug<br>\n"
"en lavere indstilling for at undgå dropud."

#: kmidi.cpp:1036
msgid ""
"This setting is a delay<br>\n"
"in centiseconds before a played<br>\n"
"note is displayed on the<br>\n"
"channel meter to the left.<br>\n"
"It may help to synchronize the<br>\n"
"display with the music."
msgstr ""
"Denne indstilling er en tøven<br>\n"
"i centisekunder før en spillet node<br>\n"
"bliver vist på kanalmåleren til venstre.<br>\n"
"Det kan måske hjælpe til at<br>\n"
"synkronisere visningen med musikken."

#: kmidi.cpp:1047
msgid ""
"When this filter button<br>\n"
"is on, a low pass filter<br>\n"
"is used for drum patches<br>\n"
"when they are first loaded<br>\n"
"and when the patch itself<br>\n"
"requests this.  The filter<br>\n"
"is also used for melodic voices<br>\n"
"if you have chosed the<br>\n"
"<i>cspline+filter</i><br>\n"
"interpolation option."
msgstr ""
"Når denne filterknap<br>\n"
"er nede, bruges der et lavt filter<br>\n"
"for trommepatch<br>\n"
"når de først indlæses<br>\n"
"og når patchen selv<br>\n"
"kræver dette. Filteret<br>\n"
"bruges også til melodiske<br>\n"
"stemmer hvis du har valgt<br>\n"
"<i>cspline+filter</i><br>interpolations-<br> \n"
"indstillingen."

#: kmidi.cpp:1160 kmidi.cpp:1540 kmidi.cpp:1847 kmidi.cpp:2144
msgid "Ready"
msgstr "Klar"

#: kmidi.cpp:1184 kmidi.cpp:1442 kmidi.cpp:1464 kmidi.cpp:1499 kmidi.cpp:1516
#: kmidi.cpp:1564 kmidi.cpp:1614
msgid "Playing"
msgstr "Spiller"

#: kmidi.cpp:1443 kmidi.cpp:1465
msgid "Random"
msgstr "Tilfældigt"

#: kmidi.cpp:1491
msgid "Paused"
msgstr "Holder pause"

#: kmidi.cpp:1691
msgid "Looping"
msgstr "I løkke"

#: kmidi.cpp:1827
msgid "ERROR"
msgstr "FEJL"

#: kmidi.cpp:1828
msgid "Can't open Output Device."
msgstr "Kan ikke åbne output enhed"

#: kmidi.cpp:2147
#, c-format
msgid "Song: --/%02d"
msgstr "Sang --/%02d"

#: kmidi.cpp:2529
msgid "%1 bit %2 %3"
msgstr "%1 bit %2 %3"

#: kmidi.cpp:2531
msgid "sig"
msgstr "sig"

#: kmidi.cpp:2531
msgid "usig"
msgstr "usig"

#: kmidi.cpp:2532
msgid "uLaw"
msgstr "uLaw"

#: kmidi.cpp:2532
msgid "Linear"
msgstr "lineær"

#: kmidi.cpp:2534
msgid "%1 Hz %2"
msgstr "%1 Hz %2"

#: kmidi.cpp:2536
msgid "Mono"
msgstr "Mono"

#: kmidi.cpp:2536 kmidiframe.cpp:135
msgid "Stereo"
msgstr "Stereo"

#: kmidi.cpp:2560
#, c-format
msgid "Song: %02d/%02d"
msgstr "Sang: %02d/%02d"

#: kmidi.cpp:2562
msgid "Song: --/--"
msgstr "Sang --/--"

#: kmidifiledlg.cpp:44
msgid "Play"
msgstr "Spil"

#: kmidiframe.cpp:80
msgid "Meter shown"
msgstr "Måler vist"

#: kmidiframe.cpp:81
msgid ""
"When you can see this<br>\n"
"menu, the channel meter<br>\n"
"is always turned on."
msgstr ""
"Når du kan se denne<br>\n"
"menu, er kanalmåleren<br>\n"
"altid slået til."

#: kmidiframe.cpp:84
msgid "Meter off"
msgstr "Måler fra"

#: kmidiframe.cpp:85
msgid ""
"This turns off the channel<br>\n"
"meter display, and also hides<br>\n"
"this menu bar."
msgstr ""
"Dette slukker for kanal<br>\n"
"måler visning, og skjuler også<br>\n"
"denne menulinie."

#: kmidiframe.cpp:89
msgid "Info shown"
msgstr "Info vist"

#: kmidiframe.cpp:90
msgid ""
"Shows a window at the<br>\n"
"bottom with info about the<br>\n"
"midi file being played."
msgstr ""
"Viser et vindue i<br>\n"
"bunden med info om den<br>\n"
"midifil der bliver spillet."

#: kmidiframe.cpp:93
msgid "Info off"
msgstr "Info slået fra"

#: kmidiframe.cpp:94
msgid ""
"Turns off the info window<br>\n"
"at the bottom."
msgstr ""
"Slår vindues info i bunden<br>\n"
"fra."

#: kmidiframe.cpp:100
msgid "Info level"
msgstr "Info niveau"

#: kmidiframe.cpp:101
msgid "Lyrics only"
msgstr "Kun tekster"

#: kmidiframe.cpp:102
msgid ""
"The info window will<br>\n"
"show only lyrics, if any,<br>\n"
"and the name of the midi file."
msgstr ""
"Infovinduet vil kun<br>\n"
"vise teksten hvis der er en<br>\n"
"sådan, og navnet på midifilen."

#: kmidiframe.cpp:105
msgid "Normal"
msgstr "Normalt"

#: kmidiframe.cpp:106
msgid ""
"The info window shows<br>\n"
"all displayable midi<br>\n"
"messages."
msgstr ""
"Infovinduet viser<br>\n"
"alle mulige visbare<br>\n"
"midibeskeder."

#: kmidiframe.cpp:109
msgid "Loading msgs"
msgstr "Indlæser beskeder"

#: kmidiframe.cpp:110
msgid ""
"Also shows new instruments<br>\n"
"being loaded for playing<br>\n"
"the next song."
msgstr ""
"Viser også nye instrumenter<br>\n"
"der indlæses for at spille<br>\n"
"den næste sang."

#: kmidiframe.cpp:113
msgid "debug 1"
msgstr "fejlret 1"

#: kmidiframe.cpp:114
msgid ""
"Also shows instrument<br>\n"
"volume computation."
msgstr ""
"Viser også instrument<br>\n"
"lydstyrke udregning."

#: kmidiframe.cpp:116
msgid "debug 2"
msgstr "fejlret 2"

#: kmidiframe.cpp:117
msgid ""
"Shows lots of additional<br>\n"
"information (probably not useful)."
msgstr ""
"Viser masser af ekstra<br>\n"
"information (formodentlig ikke nyttigt)."

#: kmidiframe.cpp:125
msgid "Edit Playlist"
msgstr "Redigér spilleliste"

#: kmidiframe.cpp:126
msgid ""
"Transfer the current<br>\n"
"play list to the Playlist Editor<br>\n"
"and start him up."
msgstr ""
"Overfør den aktuelle<br>\n"
"spilleliste til spilleliste redigering<br>\n"
"og redigér."

#: kmidiframe.cpp:138
msgid "No stereo patch"
msgstr "Ingen stereo patch"

#: kmidiframe.cpp:139
msgid ""
"Prevents playing the<br>\n"
"second instrument for those<br>\n"
"patches that have two<br>\n"
"(you probably don't want<br>\n"
"to choose this option)."
msgstr ""
"Forhindrer at det andet<br>\n"
"instrument spilles for de<br>\n"
"patcher der har to<br>\n"
"(formodentlig vil du ikke<br>\n"
"ønske at bruge denne mulighed)."

#: kmidiframe.cpp:144
msgid "Normal stereo"
msgstr "Normal stereo"

#: kmidiframe.cpp:145
msgid ""
"Plays both instruments<br>\n"
"for those (sf2) patches<br>\n"
"which have two."
msgstr ""
"Spiller begge instrumenter<br>\n"
"for de (sf2) patch<br>\n"
"der har to."

#: kmidiframe.cpp:148
msgid "Extra stereo"
msgstr "Ekstra stereo"

#: kmidiframe.cpp:149
msgid ""
"For keyboard instruments,<br>\n"
"lower notes come from the left<br>\n"
"For other instruments, position is<br>\n"
"made a function of note<br>\n"
"velocity."
msgstr ""
"På klaviatur instrumenter,<br>\n"
"kommer lavere noder fra venstre<br>\n"
"På andre instrumenter, er positionen<br>\n"
"gjort til en funktion af node<br>\n"
"hastigheden."

#: kmidiframe.cpp:155
msgid "Surround stereo"
msgstr "Surround stereo"

#: kmidiframe.cpp:156
msgid ""
"Extra stereo, echo, and<br>detuned notes are spread out more<br>to left and "
"right."
msgstr ""
"Ekstra stereo, ekko, og<br> detunede noder spredes mere<br> til venstre og "
"højre."

#: kmidiframe.cpp:163
msgid "Reverb"
msgstr "Efterklang"

#: kmidiframe.cpp:166
msgid "Dry"
msgstr "Tør"

#: kmidiframe.cpp:167
msgid ""
"With the dry setting,<br>\n"
"after notes are released,<br>\n"
"they are ended by playing<br>\n"
"through the ends of their<br>\n"
"patches (which may cause some<br>\n"
"clicking).  The wet setting makes<br>\n"
"notes continues to the ends of<br>\n"
"their volume envelopes."
msgstr ""
"Efter noder er udløst,<br>\n"
" spilles de igennem til slutningen<br>\n"
"af deres patcher hvis tør indstillingen<br>\n"
" er valgt. Dette kan forårsage<br> \n"
"klikken. Med våd indstillingen<br>\n"
"fortsætter noder til slutningen<br>\n"
"af deres volumen envelopes."

#: kmidiframe.cpp:179
msgid "Reverb level"
msgstr "Efterklangsniveau"

#: kmidiframe.cpp:180 kmidiframe.cpp:212 kmidiframe.cpp:243 kmidiframe.cpp:275
msgid "default"
msgstr "standard"

#: kmidiframe.cpp:181
msgid ""
"The reverberation level<br>\n"
"is set according to the midi<br>\n"
"channel setting and what the<br>\n"
"instrument patch specifies."
msgstr ""
"Efterklangsniveau<br>\n"
"sættes i overensstemmelse med midi<br>\n"
"kanalopsætning og hvad <br>\n"
"instrumentpatchen angiver."

#: kmidiframe.cpp:185 kmidiframe.cpp:217 kmidiframe.cpp:248 kmidiframe.cpp:281
msgid "midi level  32"
msgstr "midi niveau 32"

#: kmidiframe.cpp:186
msgid ""
"The reverberation level<br>\n"
"is set to a minimum level of 32."
msgstr ""
"Efterklangsniveauet<br>\n"
"er sat  til 32 minimalt."

#: kmidiframe.cpp:188 kmidiframe.cpp:220 kmidiframe.cpp:251 kmidiframe.cpp:284
msgid "midi level  64"
msgstr "midi niveau 64"

#: kmidiframe.cpp:189
msgid ""
"The reverberation level<br>\n"
"is set to a minimum level of 64."
msgstr ""
"Efterklangsniveauet<br>\n"
"er sat  til 64 minimalt."

#: kmidiframe.cpp:191 kmidiframe.cpp:223 kmidiframe.cpp:254 kmidiframe.cpp:287
msgid "midi level  96"
msgstr "midi niveau 96"

#: kmidiframe.cpp:192
msgid ""
"The reverberation level<br>\n"
"is set to a minimum level of 96."
msgstr ""
"Efterklangsniveauet<br>\n"
"er sat  til 96 minimalt."

#: kmidiframe.cpp:194 kmidiframe.cpp:226 kmidiframe.cpp:257 kmidiframe.cpp:290
msgid "midi level 127"
msgstr "midi niveau 127"

#: kmidiframe.cpp:195
msgid ""
"The reverberation level<br>\n"
"is set to the maximem level of 127."
msgstr ""
"Efterklangsniveauet<br>\n"
"er sat  til 127 minimalt."

#: kmidiframe.cpp:200
msgid "No echo"
msgstr "Intet ekko"

#: kmidiframe.cpp:201
msgid ""
"Prevents playing extra<br>\n"
"echo notes for reverberation."
msgstr ""
"Forhindrer at der spilles<br>\n"
"ekstra noder for efterklang."

#: kmidiframe.cpp:203
msgid "Normal echo"
msgstr "Normalt ekko"

#: kmidiframe.cpp:204
msgid ""
"Extra echo notes are<br>\n"
"played to get the effect<br>\n"
"of reverberation."
msgstr ""
"Ekstra noder spilles<br>\n"
"for at få en efterklangs<br>\n"
"virkning."

#: kmidiframe.cpp:211
msgid "Echo level"
msgstr "Ekko niveau"

#: kmidiframe.cpp:213
msgid ""
"The level for echo notes<br>\n"
"is set according to the midi<br>\n"
"channel setting and what the<br>\n"
"instrument patch specifies."
msgstr ""
"Niveauet for ekkonoder<br>\n"
"er sat i overensstemmelse med midi<br>\n"
"kanalindstilling og hvad instrument<br>\n"
"patchen angiver."

#: kmidiframe.cpp:218
msgid ""
"The echo level<br>\n"
"is set to a minimum level of 32."
msgstr ""
"Ekko niveauet er sat<br>\n"
"til 32 minimalt."

#: kmidiframe.cpp:221
msgid ""
"The echo level<br>\n"
"is set to a minimum level of 64."
msgstr ""
"Ekko niveauet er sat<br>\n"
"til 64 minimalt."

#: kmidiframe.cpp:224
msgid ""
"The echo level<br>\n"
"is set to a minimum level of 96."
msgstr ""
"Ekko niveauet er sat<br>\n"
"til 96 minimalt."

#: kmidiframe.cpp:227
msgid ""
"The echo level<br>\n"
"is set to the maximem level of 127."
msgstr ""
"Ekko niveauet er sat<br>\n"
"til 127 minimalt."

#: kmidiframe.cpp:236
msgid "Chorus"
msgstr "Kor"

#: kmidiframe.cpp:242
msgid "Chorus level"
msgstr "Korniveau"

#: kmidiframe.cpp:244
msgid ""
"The chorus level<br>\n"
"is set according to the midi<br>\n"
"channel setting and what the<br>\n"
"instrument patch specifies."
msgstr ""
"Niveauet for kor<br>\n"
"er sat i overensstemmelse med midi<br>\n"
"kanalindstilling og hvad instrument<br>\n"
"patchen angiver."

#: kmidiframe.cpp:249
msgid ""
"The choruslevel<br>\n"
"is set to a minimum level of 32."
msgstr ""
"Kor niveauet er sat<br>\n"
"til 32 minimalt."

#: kmidiframe.cpp:252
msgid ""
"The choruslevel<br>\n"
"is set to a minimum level of 64."
msgstr ""
"Kor niveauet er sat<br>\n"
"til 64 minimalt."

#: kmidiframe.cpp:255
msgid ""
"The choruslevel<br>\n"
"is set to a minimum level of 96."
msgstr ""
"Kor niveauet er sat<br>\n"
"til 96 minimalt."

#: kmidiframe.cpp:258
msgid ""
"The choruslevel<br>\n"
"is set to the maximum level of 127."
msgstr ""
"Kor niveauet er sat<br>\n"
"til 127 minimalt."

#: kmidiframe.cpp:263
msgid "No detune"
msgstr "Ingen detune"

#: kmidiframe.cpp:264
msgid ""
"Prevents playing extra<br>\n"
"detuned notes for chorus effect."
msgstr ""
"Forhindrer at der spilles<br>\n"
"ekstra detunede noder for koreffekt."

#: kmidiframe.cpp:266
msgid "Normal detune"
msgstr "Normal detune"

#: kmidiframe.cpp:267
msgid ""
"Extra detuned notes are<br>\n"
"played to get the effect<br>\n"
"of chorusing"
msgstr ""
"Ekstra detunede noder spilles<br>\n"
"for at opnå en koreffekt."

#: kmidiframe.cpp:274
msgid "Detune level"
msgstr "Detune niveau"

#: kmidiframe.cpp:276
msgid ""
"The level for detuned notes<br>\n"
"is set according to the midi<br>\n"
"channel setting and what the<br>\n"
"instrument patch specifies forchorus level."
msgstr ""
"Niveauet for detunede noder<br>\n"
"er sat i overensstemmelse med midi<br>\n"
"kanalindstilling og hvad instrument<br>\n"
"patchen angiver."

#: kmidiframe.cpp:282
msgid ""
"The detuning level<br>\n"
"is set to a minimum level of 32."
msgstr ""
"Detune niveauet er sat<br>\n"
"til 32 minimalt."

#: kmidiframe.cpp:285
msgid ""
"The detuning level<br>\n"
"is set to a minimum level of 64."
msgstr ""
"Detune niveauet er sat<br>\n"
"til 64 minimalt."

#: kmidiframe.cpp:288
msgid ""
"The detuning level<br>\n"
"is set to a minimum level of 96."
msgstr ""
"Detune niveauet er sat<br>\n"
"til 96 minimalt."

#: kmidiframe.cpp:291
msgid ""
"The detuning level<br>\n"
"is set to the maximum level of 127."
msgstr ""
"Detune niveauet er sat<br>\n"
"til 127 minimalt."

#: kmidiframe.cpp:299
msgid "Volume"
msgstr "Lydstyrke"

#: kmidiframe.cpp:304
msgid "Volume Curve"
msgstr "Lydstyrkekurve"

#: kmidiframe.cpp:307 kmidiframe.cpp:323
msgid "linear"
msgstr "lineær"

#: kmidiframe.cpp:308
msgid "The midi volume controller<br>changes the volume linearly."
msgstr "Midi lydstyrke kontrollen<br> ændrer lydstyrken lineært."

#: kmidiframe.cpp:310 kmidiframe.cpp:326
msgid "exp 4"
msgstr "exp 4"

#: kmidiframe.cpp:311 kmidiframe.cpp:314
msgid "The midi volume controller<br>changes the volume exponentially."
msgstr "Midi lydstyrkekontrollen<br>ændrer lydstyrken eksponentielt"

#: kmidiframe.cpp:313 kmidiframe.cpp:329
msgid "exp 6"
msgstr "exp 6"

#: kmidiframe.cpp:320
msgid "Expression Curve"
msgstr "Udtrykskurve"

#: kmidiframe.cpp:324
msgid "The midi expression controller<br>changes the expression linearly."
msgstr "Midi udtrykskontrollen<br>ændrer udtrykket lineært."

#: kmidiframe.cpp:327 kmidiframe.cpp:330
msgid "The midi expression controller<br>changes the expression exponentially."
msgstr "Midi udtrykskontrollen ændrer udtrykket eksponentielt."

#: kmidiframe.cpp:335 playlist.cpp:135
msgid ""
"KDE midi file player\n"
"\n"
"A software synthesizer for playing\n"
"midi songs using Tuukka Toivonen's\n"
"TiMidity"
msgstr ""
"KDE midifil spiller\n"
"\n"
"Syntetisk programmel til at spille\n"
"midisange ved brug af Tuukka Toivinen's\n"
"TiMidity"

#: log.cpp:34
msgid "Info Window"
msgstr "Informationsvindue"

#: midiapplication.cpp:57
msgid "Output to another file (or device)."
msgstr "Uddata til en anden fil (eller enhed)"

#: midiapplication.cpp:58
msgid "Select output mode and format (see below for list)."
msgstr "Vælg uddatatilstand og format (se liste nedenfor) "

#: midiapplication.cpp:59
msgid "Set sampling frequency to f (Hz or kHz)."
msgstr "Sæt prøvefrekvens til f (Hz eller kHz)."

#: midiapplication.cpp:60
msgid "Enable the antialiasing filter."
msgstr "Slå antialias filter til."

#: midiapplication.cpp:61
msgid "Enable fast decay mode."
msgstr "Slå hurtig nedbrudstilstand til."

#: midiapplication.cpp:62
msgid "Enable dry mode."
msgstr "Slå tør tilstand til."

#: midiapplication.cpp:63
msgid "Allow n-voice polyphony."
msgstr "Tillad n-stemme polyfoni."

#: midiapplication.cpp:64
msgid "Amplify volume by n percent (may cause clipping)."
msgstr "Forstærk lydstyrke med n procent (kan føre til clipping)."

#: midiapplication.cpp:65
msgid "Set ratio of sampling and control frequencies."
msgstr "Sæt ratio af prøve og kontrolfrekvenser"

#: midiapplication.cpp:66
msgid "Select patch set n."
msgstr "Vælg patch sæt n."

#: midiapplication.cpp:67
msgid "Effects filter."
msgstr "Effekter filter."

#: midiapplication.cpp:68
msgid "Append dir to search path."
msgstr "Tilføj mappe til søgesti"

#: midiapplication.cpp:69
msgid "Read extra configuration file."
msgstr "Læs ekstra opsætningsfil"

#: midiapplication.cpp:70
msgid "Use program n as the default."
msgstr "Brug program n som standard."

#: midiapplication.cpp:71
msgid "Use patch file for all programs."
msgstr "Brug patch fil til alle programmer."

#: midiapplication.cpp:72
msgid "Play drums on channel n."
msgstr "Spil trommer på kanal n."

#: midiapplication.cpp:73
msgid "Ignore channel n."
msgstr "Ignorér kanal n."

#: midiapplication.cpp:74
msgid "Reverb options (1)(+2)(+4) [7]."
msgstr "Genklangsmuligheder (1)(+2)(+4) [7]."

#: midiapplication.cpp:75
msgid "Resampling interpolation option (0-3) [1]."
msgstr "Genforsøg interpolationsmulighed (0-3) [1]."

#: midiapplication.cpp:76
msgid "Max ram to hold patches (in megabytes) [60]."
msgstr "Max ram til at holde patcher (i megabytes) [60]."

#: midiapplication.cpp:77
msgid "Midi expression is linear (0) or exponential (1-2) [1]."
msgstr "Midi udtryk er lineært (0) eller eksponentiel (1-2) [1]."

#: midiapplication.cpp:78
msgid "Midi volume is linear (0) or exponential (1-2) [1]."
msgstr "Midi lydstyrke er lineær (0) eller eksponentiel (1-2) [1]."

#: midiapplication.cpp:79
msgid "Enable fast panning."
msgstr "Slå hurtig panning til."

#: midiapplication.cpp:80
msgid "Unload instruments from memory between MIDI files."
msgstr "Aflast hukommelsen fra instrumenter mellem midifiler."

#: midiapplication.cpp:81
msgid "Select user interface (letter=d(umb)/n(curses)/s(lang))."
msgstr "Vælg brugerflade (bogstav=d(um)/n(curses)/s(lang))."

#: midiapplication.cpp:82
msgid "Set number of buffer fragments."
msgstr "Sæt antal buffer fragmenter."

#: midiapplication.cpp:83
msgid "MIDI file(s) to play."
msgstr "MIDI file(r) at spille."

#: midiapplication.cpp:183
msgid "KMidi"
msgstr "KMidi"

#: playlist.cpp:55
msgid "Compose Play List"
msgstr "Sæt spilleliste sammen"

#: playlist.cpp:79
msgid "Accept and use the new play list."
msgstr "Godkend og brug ny spilleliste."

#: playlist.cpp:81
msgid ""
"Replace the contents of the<br>\n"
"currently selected playlist<br>\n"
"file with the new play list."
msgstr ""
"Erstat indholdet af den<br>\n"
"aktuelt valgte spilleliste<br>\n"
"fil med den nye spilleliste."

#: playlist.cpp:84
msgid "Save as ..."
msgstr "Gem som ..."

#: playlist.cpp:85
msgid ""
"Make up a name for a<br>\n"
"new playlist file and<br>\n"
"save the current play list there."
msgstr ""
"Lav et navn for<br>\n"
"den nye spillelistefil og<br>\n"
"gem den aktuelle spilleliste <br>\n"
"under dette navn."

#: playlist.cpp:88
msgid "Append"
msgstr "Vedhæng"

#: playlist.cpp:89
msgid ""
"Add the midi files in the<br>\n"
"current play list to the selected<br>\n"
"playlist file."
msgstr ""
"Tilføj midifilerne i den<br>\n"
"aktuelle spillelistefil."

#: playlist.cpp:93
msgid ""
"Delete the currently selected<br>\n"
"playlist file (it's contents<br>\n"
"will be lost)."
msgstr ""
"Slet den valgte<br>\n"
"spillelistefil (dens indhold<br>\n"
"vil gå tabt)."

#: playlist.cpp:98
msgid ""
"Discard the play list<br>\n"
"and leave the play list editor."
msgstr ""
"Kassér spillelisten<br>\n"
"og luk for spilleliste redigering."

#: playlist.cpp:103
msgid "All files"
msgstr "Alle filer"

#: playlist.cpp:104
msgid ""
"Choose whether the file<br>\n"
"list should show only uncompressed<br>\n"
"midi files or all files."
msgstr ""
"Vælg om fillisten<br>\n"
" kun skal vise ikke komprimerede<br>\n"
"midifiler eller alle filer."

#: playlist.cpp:111
msgid ""
"Discard the contents<br>\n"
"of the current play list."
msgstr ""
"Kassér indholdet af<br>\n"
"den aktuelle spilleliste."

#: playlist.cpp:114
msgid ""
"Append all the files<br>\n"
"in the directory listing<br>\n"
"to the play list."
msgstr ""
"Vedlæg alle filer<br>\n"
"i mappen<br>\n"
"til spillelisten."

#: playlist.cpp:118
msgid ""
"Append the selected<br>\n"
"in the directory listing<br>\n"
"file to the play list."
msgstr ""
"Vedlæg de markerede<br>\n"
"filer i mappen<br>\n"
"til spillelisten."

#: playlist.cpp:122
msgid ""
"Remove the selected<br>\n"
"file from the play list."
msgstr ""
"Fjern den markerede<br>\n"
"fil fra spillelisten."

#: playlist.cpp:131
msgid "What?"
msgstr "Hvad?"

#: playlist.cpp:148
msgid ""
"You can drag this<br>\n"
"bar up or down."
msgstr ""
"Du kan trække denne<br>\n"
"linie op eller ned."

#: playlist.cpp:149
msgid ""
"You can drag this<br>\n"
"bar left or right."
msgstr ""
"Du kan trække denne<br>\n"
"linie til venstre eller til højre."

#: playlist.cpp:152
msgid ""
"Here is the play<br>\n"
"list you are composing.<br>\n"
"Double-click to delete<br>\n"
"an entry."
msgstr ""
"Her er spillelisten<br>\n"
"du er ved at komponere.<br>\n"
"Dobbeltklik for at slette<br>\n"
"en indgang."

#: playlist.cpp:162
msgid ""
"Here are the files<br>\n"
"you can add<br>\n"
"to the play list.<br>\n"
"Double-click on a file name<br>\n"
"to add it."
msgstr ""
"Her er filerne<br>\n"
"som du kan tilføje<br>\n"
"til spillelisten.<br>\n"
"Dobbeltklik på et<br> \n"
"filnavn for at tilføje det."

#: playlist.cpp:172
msgid ""
"<u>Playlist files:</u><br>\n"
"Click to select one.<br>\n"
"Doubleclick to append contents<br>\n"
"to the play list."
msgstr ""
"<u>Spillelistefiler:</u><br>\n"
"Klik for at vælge én.<br>\n"
"Dobbeltklik for at tilføje<br> \n"
"indholdet til spillelisten."

#: playlist.cpp:424
msgid ""
"Can not enter directory: %1\n"
msgstr ""
"Kan ikke gå ind i mappen: %1\n"

#: playlist.cpp:510
msgid "Cannot open or read directory."
msgstr "Kan ikke åbne eller læse mappen."
