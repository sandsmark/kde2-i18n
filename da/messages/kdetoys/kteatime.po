# Danish translations for KDE Toys, kteatime
# Copyright (C) 1999.
# Erik Kjær Pedersen <erik@binghamton.edu>,1999.
#
msgid ""
msgstr ""
"Project-Id-Version: KDE TOYS/kteatime\n"
"POT-Creation-Date: 2001-06-01 12:55+0200\n"
"PO-Revision-Date: 2001-07-20 07:48GMT\n"
"Last-Translator: Erik Kjær Pedersen <erik@binghamton.edu>\n"
"Language-Team: danish <da@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.3\n"

#: main.cpp:21
msgid "KDE utility for making fine cup of tea"
msgstr "KDE værktøj til at lave en fin kop te"

#: main.cpp:27
msgid "KTeaTime"
msgstr "KTeaTime"

#: toplevel.cpp:55
msgid "Black Tea (3 min)"
msgstr "Sort te (3 min.)"

#: toplevel.cpp:56
msgid "Earl Grey (5 min)"
msgstr "Earl Grey (5 min.)"

#: toplevel.cpp:57
msgid "Fruit Tea (8 min)"
msgstr "Frugt te (8 min.)"

#: toplevel.cpp:60 toplevel.cpp:288
#, c-format
msgid "Other tea (%1s)"
msgstr "Anden te (%1s)"

#: toplevel.cpp:79
msgid "&Configure..."
msgstr "&Indstil..."

#: toplevel.cpp:93
msgid "The Tea Cooker"
msgstr "Tekogeren"

#: toplevel.cpp:180 toplevel.cpp:181
msgid "The tea is now ready!"
msgstr "Teen er parat nu!"

#: toplevel.cpp:187
msgid "%1 minutes left"
msgstr "%1 minutter tilbage"

#: toplevel.cpp:223
msgid "Configure The Tea Cooker"
msgstr "Indstil tekogeren"

#: toplevel.cpp:237
msgid "Your Tea Time (s):"
msgstr "Din tetid (s):"

#: toplevel.cpp:244
msgid "Action:"
msgstr "Handling:"

#: toplevel.cpp:251
msgid "Beep"
msgstr "Bip"

#: toplevel.cpp:252
msgid "Popup"
msgstr "Pop-op"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Erik Kjær Pedersen"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "erik@binghamton.edu"
