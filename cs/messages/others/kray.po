msgid ""
msgstr ""
"Project-Id-Version: kray\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2000-08-26 15:31+CET\n"
"Last-Translator: Lukáš Tinkl <lukas@kde.org>\n"
"Language-Team: Czech <cs@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: raywidget.cpp:27
#, c-format
msgid "&File"
msgstr "&Soubor"

#: raywidget.cpp:29
msgid "&Help"
msgstr "&Nápověda"

#: raywidget.cpp:32
msgid "&Generate Picture"
msgstr "&Generovat obrázek"

#: raywidget.cpp:33
msgid "E&xit"
msgstr "&Ukončit"

#: raywidget.cpp:95
msgid "Picture Size"
msgstr "Velikost obrázku"

#: raywidget.cpp:104
msgid "The sky:"
msgstr "Obloha:"

#: raywidget.cpp:113
msgid "The hell:"
msgstr "Peklo:"

#: raywidget.cpp:122
msgid "The ground:"
msgstr "Země:"

#: raywidget.cpp:131
msgid "The small sphere:"
msgstr "Malá kulička:"

#: raywidget.cpp:140
msgid "The big sphere:"
msgstr "Velká koule:"

#: raywidget.cpp:148
msgid "Keep 4:3 ratio"
msgstr "Udržovat poměr 4:3"

#: raywidget.cpp:159
msgid "&Generate"
msgstr "&Generovat"

#: raywidget.cpp:169
msgid "Quit"
msgstr "Ukončit"

#: raywidget.cpp:179 raywidget.cpp:190
#: raywidget.cpp:201 raywidget.cpp:217
#: raywidget.cpp:233
msgid "Sky"
msgstr "Obloha"

#: raywidget.cpp:180 raywidget.cpp:191
#: raywidget.cpp:202 raywidget.cpp:218
#: raywidget.cpp:234
msgid "Hell"
msgstr "Peklo"

#: raywidget.cpp:181 raywidget.cpp:192
#: raywidget.cpp:208 raywidget.cpp:224
#: raywidget.cpp:240
msgid "Simple Red"
msgstr "Jednoduchá červená"

#: raywidget.cpp:182 raywidget.cpp:193
#: raywidget.cpp:209 raywidget.cpp:225
#: raywidget.cpp:241
msgid "Simple Green"
msgstr "Jednoduchá zelená"

#: raywidget.cpp:203 raywidget.cpp:219
#: raywidget.cpp:235
msgid "Marble"
msgstr "Mramor"

#: raywidget.cpp:204 raywidget.cpp:220
#: raywidget.cpp:236
msgid "Water"
msgstr "Voda"

#: raywidget.cpp:205 raywidget.cpp:221
#: raywidget.cpp:237
msgid "Glass"
msgstr "Sklo"

#: raywidget.cpp:206 raywidget.cpp:222
#: raywidget.cpp:238
msgid "Mirror"
msgstr "Zrcadlo"

#: raywidget.cpp:207 raywidget.cpp:223
#: raywidget.cpp:239
msgid "Brass"
msgstr "Mosaz"

#: raywidget.cpp:302
msgid ""
"The size of the picture is outside of the allowed parameters\n"
msgstr ""
"Velikost obrázku je mimo rozsah povolených parametrů\n"
