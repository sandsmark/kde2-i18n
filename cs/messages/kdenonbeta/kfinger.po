msgid ""
msgstr ""
"Project-Id-Version: kfinger\n"
"POT-Creation-Date: 2001-06-28 17:35+0200\n"
"PO-Revision-Date: 2000-10-16 13:45 CET\n"
"Last-Translator: Lukáš Tinkl <lukas@kde.org>\n"
"Language-Team: Czech <cs@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: kfingerAddressbook.cpp:52
#, fuzzy
msgid "Finger Addresses"
msgstr "Čekání fingeru: "

#: kfingerAddressbook.cpp:54
#, fuzzy
msgid "Talk Addresses"
msgstr "Neúplná adresa: "

#: kfingerAddressbook.cpp:56
#, fuzzy
msgid "Mail Addresses"
msgstr "Neúplná e-mail adresa: "

#: kfingerAddressbook.cpp:100
#, fuzzy
msgid "Selected Address:"
msgstr "Neúplná adresa: "

#: kfingerAddressbook.cpp:117
#, fuzzy
msgid "User List:"
msgstr "Seznam uživatelů"

#: kfingerAddressbook.cpp:118
#, fuzzy
msgid "Host List:"
msgstr "Seznam uživatelů"

#: kfingerAddressbook.cpp:132 kfingerAddressbook.cpp:197
#: kfingerAddressbook.cpp:243
msgid "&Add"
msgstr "Přid&at"

#: kfingerAddressbook.cpp:142 kfingerMainWindow.cpp:116
#: kfingerMainWindow.cpp:184 kfinger_part.cpp:124
#, fuzzy
msgid "&Finger"
msgstr "Finger"

#: kfingerAddressbook.cpp:164 kfingerAddressbook.cpp:212
msgid "   "
msgstr ""

#: kfingerAddressbook.cpp:182
#, fuzzy
msgid "Selected Talk Address:"
msgstr "Neúplná adresa: "

#: kfingerAddressbook.cpp:189
#, fuzzy
msgid "Talk Addresses:"
msgstr "Neúplná adresa: "

#: kfingerAddressbook.cpp:204 kfingerMainWindow.cpp:118
#: kfingerMainWindow.cpp:186
msgid "&Talk"
msgstr "&Rozhovor"

#: kfingerAddressbook.cpp:230
#, fuzzy
msgid "Selected Mail Address:"
msgstr "Neúplná e-mail adresa: "

#: kfingerAddressbook.cpp:236
#, fuzzy
msgid "Mail Addresses:"
msgstr "Neúplná e-mail adresa: "

#: kfingerAddressbook.cpp:247
msgid "Add to &Talk"
msgstr ""

#: kfingerAddressbook.cpp:256 kfingerMainWindow.cpp:120
#: kfingerMainWindow.cpp:188
msgid "&Mail"
msgstr "E-&mail"

#: kfingerConfigDlg.cpp:50
#, fuzzy
msgid "Configure - Kfinger"
msgstr "Konfigurovat Kfinger"

#: kfingerConfigDlg.cpp:63 kfingerConfigDlg.cpp:80
#, fuzzy
msgid "Settings"
msgstr "Uložit na&stavení"

#: kfingerConfigDlg.cpp:67
msgid "Page Layout"
msgstr ""

#: kfingerConfigDlg.cpp:140
msgid "Periodically Finger"
msgstr ""

#: kfingerConfigDlg.cpp:145
msgid "Refresh Rate"
msgstr ""

#: kfingerConfigDlg.cpp:149
msgid "Finger wait: "
msgstr "Čekání fingeru: "

#: kfingerConfigDlg.cpp:151
#, fuzzy
msgid "Finger Command:"
msgstr "Příkaz pro talk "

#: kfingerConfigDlg.cpp:154
#, fuzzy
msgid "Talk Command:"
msgstr "Příkaz pro talk "

#: kfingerConfigDlg.cpp:157
#, fuzzy
msgid "Mail Command:"
msgstr "Příkaz pro talk "

#: kfingerConfigDlg.cpp:160
#, fuzzy
msgid "Addressbook:"
msgstr "Adresa pro finger"

#: kfingerConfigDlg.cpp:162
msgid "Use Abbrowser"
msgstr ""

#: kfingerConfigDlg.cpp:236
#, fuzzy
msgid "Finger wait: %1 secs."
msgstr "Čekání fingeru: %1\""

#: kfinger.cpp:40
#, fuzzy
msgid "The KDE Network Finger Utility"
msgstr "KDE finger"

#: kfinger.cpp:46
msgid "KFinger"
msgstr "KFinger"

#: kfinger.cpp:55
msgid "Address to Finger"
msgstr "Adresa pro finger"

#: kfingerCSSWidget.cpp:65
msgid "CSS Editing Dialog"
msgstr ""

#: kfingerCSSWidget.cpp:69
msgid "Property"
msgstr ""

#: kfingerCSSWidget.cpp:70
msgid "Value"
msgstr ""

#: kfingerCSSWidget.cpp:79
msgid "Edit Property:"
msgstr ""

#: kfingerCSSWidget.cpp:88
msgid "&Load"
msgstr ""

#: kfingerCSSWidget.cpp:92
#, fuzzy
msgid "&Save As"
msgstr "Uložit na&stavení"

#: kfingerKIOLauncher.cpp:71 kfingerKIOLauncher.cpp:89
#: kfingerKIOLauncher.cpp:108
msgid "Couldn't find: "
msgstr "Nemohu najít: "

#: kfingerKIOLauncher.cpp:72
#, fuzzy
msgid ""
"\n"
"Setting it to 'kfinger'."
msgstr ""
"\n"
"Nastavuji do rozhovoru."

#: kfingerKIOLauncher.cpp:73 kfingerKIOLauncher.cpp:91
#: kfingerKIOLauncher.cpp:110
msgid " not found"
msgstr " nenalezen"

#: kfingerKIOLauncher.cpp:90
#, fuzzy
msgid ""
"\n"
"Setting it to 'kmail'."
msgstr ""
"\n"
"Nastavuji do rozhovoru."

#: kfingerKIOLauncher.cpp:109
#, fuzzy
msgid ""
"\n"
"Setting it to 'konsole -e talk'."
msgstr ""
"\n"
"Nastavuji do rozhovoru."

#: kfingerKIOLauncher.cpp:171
#, fuzzy
msgid "Incomplete finger address: "
msgstr "Neúplná e-mail adresa: "

#: kfingerKIOLauncher.cpp:195
msgid "Incomplete mail address: "
msgstr "Neúplná e-mail adresa: "

#: kfingerKIOLauncher.cpp:221
msgid "Incomplete talk address: "
msgstr "Neúplná adresa: "

#: kfingerKIOLauncher.cpp:248
#, fuzzy
msgid "Incorrect finger url: "
msgstr "Neúplná e-mail adresa: "

#: kfingerMainWindow.cpp:122
#, fuzzy
msgid "&Addressbook"
msgstr "Adresa pro finger"

#: kfingerMainWindow.cpp:135
#, fuzzy
msgid "&Save Settings"
msgstr "Uložit na&stavení"

#: kfingerMainWindow.cpp:140 kfinger_part.cpp:129
#, fuzzy
msgid "&Stop current finger"
msgstr "Zastavit současný finger"

#: kfingerMainWindow.cpp:144
#, fuzzy
msgid "A&utoFinger On/Off"
msgstr "Automatický finger zap./vyp."

#: kfingerMainWindow.cpp:148
#, fuzzy
msgid "Finger Address "
msgstr "Čekání fingeru: "

#: kfingerMainWindow.cpp:154
msgid "Location "
msgstr ""

#: kfingerMainWindow.cpp:176
msgid "KDE finger utility"
msgstr "KDE finger"

#: kfingerMainWindow.cpp:191
#, fuzzy
msgid "&Add Address"
msgstr "Adresa pro finger"

#: kfingerMainWindow.cpp:194
msgid "Run in &Konqueror"
msgstr ""

#: kfingerMainWindow.cpp:361
#, fuzzy
msgid "Finger  %1@%2  -  %3"
msgstr "Finger %2@%1   --   %3"

#: kfingerMainWindow.cpp:471
msgid "Press right mouse button to see options "
msgstr ""

#: kfinger_part.cpp:34
#, fuzzy
msgid "Kfinger KPart"
msgstr "Čekání fingeru: "

#: kfinger_part.cpp:90
msgid "kfinger-part"
msgstr ""

#: kfinger_part.cpp:139
#, fuzzy
msgid "Finger Address: "
msgstr "Čekání fingeru: "

#: rc.cpp:1 rc.cpp:2
msgid "&Action"
msgstr ""

#: rc.cpp:5
msgid "ComboBox"
msgstr ""

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#, fuzzy
#~ msgid "&Configure"
#~ msgstr "Konfigura&ce"

#, fuzzy
#~ msgid "Kfinger - Settings Dialog"
#~ msgstr "Nastavení KFingeru"

#, fuzzy
#~ msgid "&Configuration"
#~ msgstr "Konfigura&ce"

#, fuzzy
#~ msgid "Commands"
#~ msgstr "Příkaz pro talk "

#~ msgid "Finger"
#~ msgstr "Finger"

#~ msgid "Talk"
#~ msgstr "Rozhovor"

#~ msgid "Mail"
#~ msgstr "E-mail"

#, fuzzy
#~ msgid "User list"
#~ msgstr "Seznam uživatelů"

#~ msgid "Misc"
#~ msgstr "Různé"

#~ msgid "User Name"
#~ msgstr "Jméno uživatele"

#, fuzzy
#~ msgid "Host Name"
#~ msgstr "Jméno uživatele"

#~ msgid "F&inger"
#~ msgstr "F&inger"

#~ msgid "Talk..."
#~ msgstr "Rozhovor..."

#~ msgid "Couldn't find mail program: "
#~ msgstr "Nemohu najít program pro e-mail: "

#~ msgid "Talk with..."
#~ msgstr "Rozhovor s..."

#~ msgid ""
#~ "There's no ktalk in KDE2 (yet).\n"
#~ " Plese change your Setting"
#~ msgstr ""
#~ "KDE2 nemá (zatím) program talk.\n"
#~ " Změňte si prosím vaše nastavení"

#~ msgid "Server Name"
#~ msgstr "Jméno serveru"

#~ msgid "Server list"
#~ msgstr "Seznam serverů"

#~ msgid "Timed finger"
#~ msgstr "Časovaný finger"

#, fuzzy
#~ msgid "Default finger port"
#~ msgstr "Výchozí port pro finger"

#~ msgid "&Raw Finger"
#~ msgstr "Fing&er"
