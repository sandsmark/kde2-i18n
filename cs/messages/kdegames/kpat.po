# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kpat\n"
"POT-Creation-Date: 2001-06-10 17:19+0200\n"
"PO-Revision-Date: 2001-07-29 12:57+CET\n"
"Last-Translator: Lukáš Tinkl <lukas@kde.org>\n"
"Language-Team: Czech <cs@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: cardmaps.cpp:108
msgid "please wait, loading cards..."
msgstr "Prosím čekejte, nahrávám karty..."

#: cardmaps.cpp:112
msgid "KPatience - a Solitaire game"
msgstr "KPatience - solitérová hra"

#: clock.cpp:86
msgid "G&randfather's Clock"
msgstr "&Dědečkovy hodiny"

#: computation.cpp:113
msgid "&Calculation"
msgstr "&Výpočet"

#: dealer.cpp:67
msgid "&Hint"
msgstr "R&ada"

#: dealer.cpp:75
msgid "&Demo"
msgstr "&Demo"

#: dealer.cpp:83
msgid "&Redeal"
msgstr "&Rozdat znovu"

#: fortyeight.cpp:127
msgid "F&orty and Eight"
msgstr "Čtyřicet &osm"

#: freecell.cpp:179
msgid "%1 tries - depth %2"
msgstr "pokusů: %1 - hloubka: %2"

#: freecell.cpp:185
msgid "solved after %1 tries"
msgstr "vyřešeno po %1 tazích"

#: freecell.cpp:195
msgid "unsolved after %1 moves"
msgstr "nevyřešeno po %1 tazích"

#: freecell.cpp:323
msgid "%1 moves before finish"
msgstr "%1 tahů(y) před koncem"

#: freecell.cpp:603
msgid "&Freecell"
msgstr "&Freecell"

#: golf.cpp:142
msgid "G&olf"
msgstr "G&olf"

#: grandf.cpp:167
msgid "&Grandfather"
msgstr "&Dědeček"

#: gypsy.cpp:62
msgid "Gy&psy"
msgstr "C&ikán"

#: idiot.cpp:185
msgid "&Aces Up"
msgstr "Es&a nahoru"

#: kings.cpp:53
msgid "&The Kings"
msgstr "&Králové"

#: klondike.cpp:251
msgid "&Klondike"
msgstr "&Klondike"

#: main.cpp:28
msgid "KDE Patience Game"
msgstr "Pasiáns pro KDE"

#: main.cpp:32
msgid "KPatience"
msgstr "KPatience"

#: main.cpp:39
msgid "Some Game Types"
msgstr "Některé typy her"

#: main.cpp:41
msgid "Bug fixes"
msgstr "Opravy chyb"

#: main.cpp:42
msgid "Shuffle algorithm for game numbers"
msgstr "Promíchávací algoritmus čísel her"

#: main.cpp:44
msgid "Freecell Solver"
msgstr "Freecell řešitel"

#: main.cpp:45
msgid "Rewrite and current maintainer"
msgstr "Přepis, současný správce"

#: mod3.cpp:150
msgid "M&od3"
msgstr "M&od3"

#: napoleon.cpp:171
msgid "&Napoleon's Tomb"
msgstr "&Napoleonova hrobka"

#: pwidget.cpp:73
msgid "&Choose game..."
msgstr "&Zvolte novou hru..."

#: pwidget.cpp:75
msgid "&Restart game"
msgstr "&Restartovat hru"

#: pwidget.cpp:80
msgid "&Game Type"
msgstr "Typ &hry  "

#: pwidget.cpp:103
msgid "&Change Background"
msgstr "Změnit &pozadí"

#: pwidget.cpp:117
msgid "&Switch Cards..."
msgstr "Vy&měnit karty..."

#: pwidget.cpp:121
msgid "&Animation on startup"
msgstr "&Animace při spuštění"

#: pwidget.cpp:309
msgid "Could not load background image!"
msgstr "Nemohu nahrát obrázek zadní strany!"

#: pwidget.cpp:320
msgid "Game Number"
msgstr "Číslo hry"

#: pwidget.cpp:321
msgid "Enter a game number (1 to 32000 are the same as in the FreeCell FAQ)"
msgstr "Zadejte číslo hry (1 a 32000 znamenají to samé, viz Freecell FAQ)"

#: pwidget.cpp:337
msgid "Congratulations! We've won!"
msgstr "Gratulace! Vyhráli jsme!"

#: pwidget.cpp:339
msgid "Congratulations! You've won!"
msgstr "Gratulace! Vyhrál(a) jste!"

#: pwidget.cpp:341
msgid "Congratulations!"
msgstr "Gratulace!"

#: pwidget.cpp:367
msgid "The saved game is of unknown type!"
msgstr "Uložená hra je neznámého typu!"

#: simon.cpp:114
msgid "&Simple Simon"
msgstr "Jednoduchý &Simon"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Lukáš Tinkl"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "lukas@kde.org"

#: yukon.cpp:59
msgid "&Yukon"
msgstr "&Yukon"
