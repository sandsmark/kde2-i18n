# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmicons\n"
"POT-Creation-Date: 2001-06-10 02:05+0200\n"
"PO-Revision-Date: 2001-05-24 10:24CEST\n"
"Last-Translator: Miroslav Flídr <flidr@kky.zcu.cz>\n"
"Language-Team: Czech <cs@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.1\n"

#: iconthemes.cpp:78
msgid "Name"
msgstr "Jméno"

#: iconthemes.cpp:79
msgid "Description"
msgstr "Popis"

#: iconthemes.cpp:88
msgid "Install New Theme"
msgstr "Instalovat nový motiv"

#: iconthemes.cpp:91
msgid "Remove Theme"
msgstr "Odstranit motiv"

#: iconthemes.cpp:96
msgid "Select the icon theme you want to use :"
msgstr "Zvolte si motiv ikon, který chcete používat: "

#: iconthemes.cpp:193
msgid ""
"Are you sure you want to remove the %1 icon theme ?\n"
"This will delete the files installed by this theme"
msgstr ""
"Opravdu chcete odstranit motiv ikon '%1'?\n"
"Toto smaže veškeré soubory instalované tímto motivem."

#: iconthemes.cpp:199
msgid "Confirmation"
msgstr "Potvrzení"

#: icons.cpp:61
msgid "Use of Icon"
msgstr "Použití ikony"

#: icons.cpp:82
msgid "Active"
msgstr "Aktivní"

#: icons.cpp:84
msgid "Disabled"
msgstr "Nepřístupné"

#: icons.cpp:96
msgid "Size:"
msgstr "Velikost:"

#: icons.cpp:104
msgid "Double-sized pixels"
msgstr "Dvojnásobně velké pixely"

#: icons.cpp:108
msgid "Blend alpha channel"
msgstr "Smísit alfa kanál"

#: icons.cpp:128
msgid "Set Effect"
msgstr "Nastavit efekt"

#: icons.cpp:144
msgid "Desktop / File Manager"
msgstr "Plocha / Správce souborů"

#: icons.cpp:145
msgid "Toolbar"
msgstr "Nástrojová lišta"

#: icons.cpp:147
msgid "Small Icons"
msgstr "Malé ikony"

#: icons.cpp:148
msgid "Panel"
msgstr "Panel"

#: icons.cpp:392
msgid "Setup Default Icon Effect"
msgstr "Nastavit efekt pro výchozí ikony"

#: icons.cpp:393
msgid "Setup Active Icon Effect"
msgstr "Nastavit efekt pro aktivní ikony"

#: icons.cpp:394
msgid "Setup Disabled Icon Effect"
msgstr "Nastavit efekt pro nepřístupné ikony"

#: icons.cpp:460
msgid "&Effect:"
msgstr "&Efekt:"

#: icons.cpp:464
msgid "No Effect"
msgstr "Bez efektů"

#: icons.cpp:465
msgid "To Gray"
msgstr "Do šeda"

#: icons.cpp:466
msgid "Colorize"
msgstr "Vybarvit"

#: icons.cpp:467
msgid "Gamma"
msgstr "Gamma"

#: icons.cpp:468
msgid "Desaturate"
msgstr "Zmenšit nasycení"

#: icons.cpp:473
msgid "Semi-transparent"
msgstr "Poloprůhledný"

#: icons.cpp:477
msgid "Preview"
msgstr "Náhled"

#: icons.cpp:488
msgid "Effect Parameters"
msgstr "Parametry efektu"

#: icons.cpp:493
msgid "Amount:"
msgstr "Množství:"

#: icons.cpp:499
msgid "Color:"
msgstr "Barva:"

#: main.cpp:52
msgid "&Theme"
msgstr "Mo&tiv"

#: main.cpp:56
msgid "&Advanced"
msgstr "P&okročilé"

#: main.cpp:89
msgid ""
"<h1>Icons</h1>\n"
"This module allows you to choose the icons for your desktop.\n"
"You can also specify effects that should be applied to the icons.\n"
"Use the \"Whats This?\" (Shift+F1) to get help on specific options."
msgstr ""
"<h1>Ikony</h1>\n"
"Tento modul vám umožňuje zvolit si ikony svého prostředí.\n"
"Můžete také specifikovat efekty, které budou aplikovány na ikony.\n"
"Pro obdržení nápovědy k jednotlivým položkám použijte funkci \"Co je "
"toto?\" (Shift+F1)."
