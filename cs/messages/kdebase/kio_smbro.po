# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-08-17 10:21+0200\n"
"PO-Revision-Date: 2001-08-30 10:35CET\n"
"Last-Translator: Miroslav Flídr <flidr@kky.zcu.cz>\n"
"Language-Team: Czech <kde-czech-apps@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.1beta\n"

#: kio_smb.cpp:202 kio_smb.cpp:216 kio_smb.cpp:405 kio_smb.cpp:435
#: kio_smb.cpp:1212 kio_smb.cpp:1243
msgid ""
"\n"
"Make sure that the samba package is installed properly on your system."
msgstr ""
"\n"
"Ujistěte se, že je na vašem systému správně naistalována Samba."

#: kio_smb.cpp:212
msgid ""
"\n"
"There is probably no SMB service running on this host."
msgstr ""
"\n"
"Na tomto hostiteli pravděpodobně neběží SMB služba."

#: kio_smb.cpp:231 kio_smb.cpp:236
msgid ""
"\n"
"Invalid user/password combination."
msgstr ""
"\n"
"Neplatná kombinace uživatel/heslo."

#: kio_smb.cpp:255
msgid "Hmm..."
msgstr "Hmm..."

#: kio_smb.cpp:422 kio_smb.cpp:1230
msgid "Username and password required:"
msgstr "Je požadováno uživatelské jméno a heslo:"

#: kio_smb.cpp:559 kio_smb.cpp:1090
msgid ""
"To access the shares of a host, use smb://hostname\n"
"To get a list of all hosts use lan:/ or rlan:/ .\n"
"See the KDE Control Center under Network, LANBrowsing for more information."
msgstr ""
"K přístupu ke sdíleným prostředkům hostitele použijte smb://hostname\n"
"K získání seznamu všech hostitelů použijte an:/ nebo rlan:/ .\n"
"Více informací naleznete v Ovládacím centru KDE pod položkou Síť, "
"LANBrowsing."

#: kio_smb.cpp:893
msgid ""
"\n"
"To access the shares of a host, use smb://hostname\n"
"To get a list of all hosts use lan:/ or rlan:/ .\n"
"See the KDE Control Center under Network, LANBrowsing for more information."
msgstr ""
"\n"
"K přístupu ke sdíleným prostředkům hostitele použijte smb://hostname\n"
"K získání seznamu všech hostitelů použijte an:/ nebo rlan:/ .\n"
"Více informací naleznete v Ovládacím centru KDE pod položkou Síť, "
"LANBrowsing."

#: kio_smb.cpp:937 kio_smb.cpp:968
msgid ""
"\n"
"Could not create required pipe %1 ."
msgstr ""
"\n"
"Není možné vytvořit požadovanou rouru %1 ."

#: kio_smb.cpp:979 kio_smb.cpp:987
msgid ""
"\n"
"Could not fcntl() pipe %1 ."
msgstr ""
"\n"
"Není možné zavolat fcntl() na požadovanou rouru %1."
