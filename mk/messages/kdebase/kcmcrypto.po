# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2000 Free Software Foundation, Inc.
# Danko Ilik <danko@mindless.com>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmcrypto\n"
"POT-Creation-Date: 2001-07-05 13:30+0200\n"
"PO-Revision-Date: 2000-11-09 19:00+0100\n"
"Last-Translator: Danko Ilik <danko@mindless.com>\n"
"Language-Team: Macedonian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: crypto.cpp:94
msgid "%1 (%2 of %3 bits)"
msgstr ""

#: crypto.cpp:201
msgid "Enable &TLS support if supported by the server."
msgstr ""

#: crypto.cpp:204
msgid ""
"TLS is the newest revision of the SSL protocol.  It integrates better with "
"other protocols and has replaced SSL in protocols such as POP3 and SMTP."
msgstr ""

#: crypto.cpp:209
msgid "Enable SSLv&2"
msgstr "Овозможи SSLv&2"

#: crypto.cpp:212
msgid ""
"SSL v2 is the second revision of the SSL protocol. It is most common to "
"enable v2 and v3."
msgstr ""
"SSL v2 е втората ревизија на SSL протоколот. "
"Вообичаено е да се овозможат и v2 и v3."

#: crypto.cpp:216
msgid "Enable SSLv&3"
msgstr "Овозможи SSLv&3"

#: crypto.cpp:219
msgid ""
"SSL v3 is the third revision of the SSL protocol. It is most common to "
"enable v2 and v3."
msgstr ""
"SSL v3 е третата ревизија на SSL протоколот. "
"Вообичаено е да се овозможат и v2 и v3."

#: crypto.cpp:225
msgid "SSLv2 Ciphers To Use:"
msgstr "SSLv2 кодови што ќе се користат:"

#: crypto.cpp:226
msgid ""
"Select the ciphers you wish to enable when using the SSL v2 protocol.  The "
"actual protocol used will be negotiated with the server at connection time."
msgstr ""
"Изберете ги кодовите што сакате да се "
"користаат за SSL v2 протоколот.  Конкретниот "
"протокол што ќе се користи ќе биде "
"договорен со серверот за време на "
"поврзувањето."

#: crypto.cpp:236
msgid ""
"SSL ciphers cannot be configured because this module was not linked with "
"OpenSSL."
msgstr ""
"SSL кодовите не можат да се конфигурираат "
"бидејќи овој модул не е линкуван со OpenSSL."

#: crypto.cpp:251
msgid "SSLv3 Ciphers To Use:"
msgstr "SSLv3 кодови што ќе се користат:"

#: crypto.cpp:252
msgid ""
"Select the ciphers you wish to enable when using the SSL v3 protocol.  The "
"actual protocol used will be negotiated with the server at connection time."
msgstr ""
"Изберете ги кодовите што сакате да се "
"користаат за SSL v3 протоколот.  Конкретниот "
"протокол што ќе се користи ќе биде "
"договорен со серверот за време на "
"поврзувањето."

#: crypto.cpp:266
msgid "Cipher Wizards..."
msgstr ""

#: crypto.cpp:267
msgid "Most &Compatible"
msgstr ""

#: crypto.cpp:268
#, fuzzy
msgid "&US Ciphers Only"
msgstr "SSLv2 кодирања"

#: crypto.cpp:269
msgid "E&xport Ciphers Only"
msgstr ""

#: crypto.cpp:270
#, fuzzy
msgid "Enable &All"
msgstr "Овозможи SSLv&2"

#: crypto.cpp:276
msgid "Use these buttons to more easily configure the SSL encryption settings."
msgstr ""

#: crypto.cpp:278
msgid "Select the settings found to be most compatible."
msgstr ""

#: crypto.cpp:280
msgid "Select only the US strong (>= 128 bit) encryption ciphers."
msgstr ""

#: crypto.cpp:282
msgid "Select only the weak ciphers (<= 56 bit)."
msgstr ""

#: crypto.cpp:284
msgid "Select all SSL ciphers and methods."
msgstr ""

#: crypto.cpp:291
msgid "Use EGD"
msgstr ""

#: crypto.cpp:297
msgid "Path to EGD:"
msgstr ""

#: crypto.cpp:306
msgid ""
"If selected, OpenSSL will be asked to use the entropy gathering daemon (EGD) "
"for initializing the pseudo-random number generator."
msgstr ""

#: crypto.cpp:309
msgid "Enter the path to the socket created by the entropy gathering daemon here."
msgstr ""

#: crypto.cpp:313
msgid "Click here to browse for the EGD socket file."
msgstr ""

#: crypto.cpp:321
msgid "Warn on &entering SSL mode"
msgstr "Предупредување при влез во SSL режим"

#: crypto.cpp:324
msgid "If selected, you will be notified when entering an SSL enabled site"
msgstr ""
"Ако е избрано, ќе бидете известени при "
"влегување во SSL сајт"

#: crypto.cpp:328
msgid "Warn on &leaving SSL mode"
msgstr "Предупредување при напуштање на SSL режим"

#: crypto.cpp:331
msgid "If selected, you will be notified when leaving an SSL based site."
msgstr ""
"Ако е избрано, ќе бидете известени при "
"напуштање на SSL сајт"

#: crypto.cpp:336
msgid "Warn on sending &unencrypted data"
msgstr ""
"Предупредување при праќање некриптирани "
"податоци"

#: crypto.cpp:339
msgid ""
"If selected, you will be notified before sending unencrypted data via a web "
"browser."
msgstr ""
"Ако е избрано, ќе бидете известени пред "
"праќање на некриптирани податоци преку "
"вев-разгледувачот."

#: crypto.cpp:343
msgid "Warn on &mixed SSL/non-SSL pages"
msgstr "Предупредување на мешано SSL/не-SSL страници."

#: crypto.cpp:346
msgid ""
"If selected, you will be notified if you view a page that has both encrypted "
"and non-encrypted parts."
msgstr ""
"Ако е избрано, ќе бидете известени при "
"гледање страници кои имаат и криптирани и "
"некриптирани делови."

#: crypto.cpp:359
msgid "Enter the path to your OpenSSL shared libraries:"
msgstr ""

#: crypto.cpp:363
msgid "..."
msgstr ""

#: crypto.cpp:365
msgid "&Test..."
msgstr ""

#: crypto.cpp:385
msgid ""
"This list box shows which certificates of yours KDE knows about.  You can "
"easily manage them from here."
msgstr ""
"Оваа листа покажува за кои ваши "
"сертификати КДЕ знае.  Можете лесно да "
"управувате со нив од тука."

#: crypto.cpp:389 crypto.cpp:542
msgid "Common Name"
msgstr ""

#: crypto.cpp:390
msgid "Email Address"
msgstr ""

#: crypto.cpp:393 crypto.cpp:650
msgid "&Import..."
msgstr "Увоз..."

#: crypto.cpp:397 crypto.cpp:545
msgid "&Export..."
msgstr "Извоз..."

#: crypto.cpp:402 crypto.cpp:501
#, fuzzy
msgid "Remo&ve"
msgstr "Отстрани..."

#: crypto.cpp:407
msgid "&Unlock"
msgstr ""

#: crypto.cpp:412 crypto.cpp:662
msgid "Verif&y..."
msgstr "Провери..."

#: crypto.cpp:417
msgid "&Change Password..."
msgstr ""

#: crypto.cpp:427 crypto.cpp:575
msgid "This is the information known about the owner of the certificate."
msgstr ""

#: crypto.cpp:429 crypto.cpp:577
msgid "This is the information known about the issuer of the certificate."
msgstr ""

#: crypto.cpp:432 crypto.cpp:580
msgid "Valid From:"
msgstr ""

#: crypto.cpp:433 crypto.cpp:581
msgid "Valid Until:"
msgstr ""

#: crypto.cpp:438 crypto.cpp:586
msgid "The certificate is valid starting at this date."
msgstr ""

#: crypto.cpp:440 crypto.cpp:588
msgid "The certificate is valid until this date."
msgstr ""

#: crypto.cpp:444
msgid "On SSL Connection..."
msgstr "При SSL конекција..."

#: crypto.cpp:445
msgid "&Use default certificate"
msgstr "Користење на вообичаениот сертификат"

#: crypto.cpp:446
msgid "&List upon connection"
msgstr "Листа при конектирање"

#: crypto.cpp:447
msgid "&Do not use certificates"
msgstr "Не користи сертификати"

#: crypto.cpp:451 crypto.cpp:520 crypto.cpp:626 crypto.cpp:667
msgid ""
"SSL certificates cannot be managed because this module was not linked with "
"OpenSSL."
msgstr ""
"Со SSL сертификати не може да се управува "
"бидејќи овој модул не е линкуван со OpenSSL."

#: crypto.cpp:466
#, fuzzy
msgid "Default Authentication Certificate"
msgstr "Користење на вообичаениот сертификат"

#: crypto.cpp:467
#, fuzzy
msgid "Default Action..."
msgstr "Постави вообичаен..."

#: crypto.cpp:468
msgid "&Send"
msgstr ""

#: crypto.cpp:469 crypto.cpp:614
msgid "&Prompt"
msgstr ""

#: crypto.cpp:470
msgid "D&on't Send"
msgstr ""

#: crypto.cpp:472
#, fuzzy
msgid "Default Certificate:"
msgstr "Користење на вообичаениот сертификат"

#: crypto.cpp:479
msgid "Host Authentication"
msgstr ""

#: crypto.cpp:482
msgid "Host"
msgstr ""

#: crypto.cpp:483
#, fuzzy
msgid "Certificate"
msgstr "Ваши SSL сертификати"

#: crypto.cpp:484 crypto.cpp:611
msgid "Policy"
msgstr ""

#: crypto.cpp:486
msgid "Host:"
msgstr ""

#: crypto.cpp:487
#, fuzzy
msgid "Certificate:"
msgstr "Ваши SSL сертификати"

#: crypto.cpp:494
#, fuzzy
msgid "Action..."
msgstr "Постави вообичаен..."

#: crypto.cpp:495
msgid "Send"
msgstr ""

#: crypto.cpp:496
msgid "Prompt"
msgstr ""

#: crypto.cpp:497
msgid "Don't Send"
msgstr ""

#: crypto.cpp:500
msgid "Ne&w"
msgstr ""

#: crypto.cpp:538
msgid ""
"This list box shows which site and person certificates KDE knows about.  You "
"can easily manage them from here."
msgstr ""
"Оваа листа покажува за кои ваши и "
"сертификати на сајтови КДЕ знае.  Можете "
"лесно да управувате со нив од тука."

#: crypto.cpp:541
msgid "Organization"
msgstr ""

#: crypto.cpp:548
msgid ""
"This button allows you to export the selected certificate to a file of "
"various formats."
msgstr ""

#: crypto.cpp:552 crypto.cpp:658
msgid "&Remove..."
msgstr "Отстрани..."

#: crypto.cpp:555
msgid "This button removes the selected certificate from the certificate cache."
msgstr ""

#: crypto.cpp:559
#, fuzzy
msgid "&Verify..."
msgstr "Провери..."

#: crypto.cpp:562
msgid "This button tests the selected certificate for validity."
msgstr ""

#: crypto.cpp:591
msgid "Cache..."
msgstr ""

#: crypto.cpp:592
msgid "Permanentl&y"
msgstr ""

#: crypto.cpp:593
msgid "&Until..."
msgstr ""

#: crypto.cpp:604
msgid "Select here to make the cache entry permanent."
msgstr ""

#: crypto.cpp:606
msgid "Select here to make the cache entry temporary."
msgstr ""

#: crypto.cpp:608
msgid "The date and time until the certificate cache entry should expire."
msgstr ""

#: crypto.cpp:612
msgid "A&ccept"
msgstr ""

#: crypto.cpp:613
msgid "Re&ject"
msgstr ""

#: crypto.cpp:618
msgid "Select this to always accept this certificate."
msgstr ""

#: crypto.cpp:620
msgid "Select this to always reject this certificate."
msgstr ""

#: crypto.cpp:622
msgid ""
"Select this if you wish to be prompted for action when receiving this "
"certificate."
msgstr ""

#: crypto.cpp:643
msgid ""
"This list box shows which certificate authorities KDE knows about.  You can "
"easily manage them from here."
msgstr ""
"Оваа листа покажува за кои ваши "
"сертификат-авторитети (CA) КДЕ знае.  "
"Можете лесно да управувате со нив од тука."

#: crypto.cpp:654
msgid "&View/Edit..."
msgstr "Преглед/Уредување..."

#: crypto.cpp:680
#, fuzzy
msgid "Warn on &self-signed certificates or unknown CA's"
msgstr ""
"Предупредување при самопотпишани "
"сертификати или непознати "
"сертификат-авторитети"

#: crypto.cpp:682
msgid "Warn on &expired certificates"
msgstr ""
"Предупредување при сертификати со "
"изминат рок"

#: crypto.cpp:684
msgid "Warn on re&voked certificates"
msgstr ""
"Предупредување при отповикани "
"сертификати"

#: crypto.cpp:694
msgid ""
"This list box shows which sites you have decided to accept a certificate "
"from even though the certificate might fail the validation procedure."
msgstr ""
"Оваа листа покажува од кои сајтови сте "
"одлучиле да прифатите сертификат иако "
"сертификатот можел да не ја помине "
"процедурата за валоризација."

#: crypto.cpp:702
msgid "&Add"
msgstr "Додај"

#: crypto.cpp:710
msgid "&Clear"
msgstr "Испразни"

#: crypto.cpp:715
msgid ""
"These options are not configurable because this module was not linked with "
"OpenSSL."
msgstr ""
"Овие опции не може да се конфигурираат "
"бидејќи овој модул не е линкуван со OpenSSL."

#: certexport.cpp:94 certexport.cpp:109 certexport.cpp:117 crypto.cpp:725
#: crypto.cpp:901 crypto.cpp:1191 crypto.cpp:1216 crypto.cpp:1221
#: crypto.cpp:1223 crypto.cpp:1395 crypto.cpp:1413 crypto.cpp:1462
#: crypto.cpp:1488 crypto.cpp:1490
msgid "SSL"
msgstr "SSL"

#: crypto.cpp:727 crypto.cpp:1777 crypto.cpp:1785 crypto.cpp:1789
#, fuzzy
msgid "OpenSSL"
msgstr "SSL"

#: crypto.cpp:729
#, fuzzy
msgid "Your Certificates"
msgstr "Ваши SSL сертификати"

#: crypto.cpp:730
msgid "Authentication"
msgstr ""

#: crypto.cpp:731
#, fuzzy
msgid "Peer SSL Certificates"
msgstr "Други SSL сертификати"

#: crypto.cpp:734
msgid "SSL C.A.s"
msgstr "SSL C.A.-и"

#: crypto.cpp:735
msgid "Validation Options"
msgstr "Опции за валоризација"

#: crypto.cpp:897
msgid ""
"If you don't select at least one SSL algorithm, either SSL will not work or "
"the application may be forced to choose a suitable default."
msgstr ""
"Ако не изберете барем еден SSL алгоритам, "
"или SSL нема да работи или апликацијата "
"може да биде приморана да избере било што "
"соодветно."

#: crypto.cpp:948
msgid "If you don't select at least one cipher, SSLv2 will not work."
msgstr ""
"Ако не изберете барем едно кодирање, SSLv2 "
"нема да работи."

#: crypto.cpp:950
msgid "SSLv2 Ciphers"
msgstr "SSLv2 кодирања"

#: crypto.cpp:965
msgid "If you don't select at least one cipher, SSLv3 will not work."
msgstr ""
"Ако не изберете барем едно кодирање, SSLv3 "
"нема да работи. "

#: crypto.cpp:967
msgid "SSLv3 Ciphers"
msgstr "SSLv3 кодирања"

#: crypto.cpp:1095
msgid ""
"<h1>crypto</h1> This module allows you to configure SSL for use with most "
"KDE applications, as well as manage your personal certificates and the known "
"certificate authorities."
msgstr ""
"<h1>Криптографија</h1> Овој модул ви "
"овозможува да го конфигурирате SSL за "
"употреба со повеќе КДЕ апликации, како и "
"да управувате со вашите лични "
"сертификати и познати "
"сертификат-авторитети."

#: crypto.cpp:1190
#, fuzzy
msgid "Couldn't open the certificate."
msgstr "Не користи сертификати"

#: crypto.cpp:1216
#, fuzzy
msgid "Error obtaining the certificate."
msgstr "Не користи сертификати"

#: crypto.cpp:1221 crypto.cpp:1488
msgid "This certificate passed the verification tests successfully."
msgstr ""

#: crypto.cpp:1223 crypto.cpp:1490
msgid "This certificate has failed the tests and should be considered invalid."
msgstr ""

#: crypto.cpp:1389
msgid "Certificate password"
msgstr ""

#: crypto.cpp:1395
msgid "The certificate file could not be loaded.  Try a different password?"
msgstr ""

#: crypto.cpp:1413
msgid ""
"A certificate with that name already exists.  Are you sure that you wish to "
"replace it?"
msgstr ""

#: crypto.cpp:1444 crypto.cpp:1475 crypto.cpp:1517
#, fuzzy
msgid "Enter the certificate password:"
msgstr "Не користи сертификати"

#: crypto.cpp:1450 crypto.cpp:1481 crypto.cpp:1523 crypto.cpp:1617
msgid "Decoding failed.  Please try again:"
msgstr ""

#: crypto.cpp:1462
msgid "Export failed."
msgstr ""

#: crypto.cpp:1612
msgid "Enter the OLD password for the certificate:"
msgstr ""

#: crypto.cpp:1626
msgid "Enter the new certificate password"
msgstr ""

#: crypto.cpp:1775 crypto.cpp:1783
msgid "Failed to load OpenSSL."
msgstr ""

#: crypto.cpp:1776
msgid "libssl was not found or successfully loaded."
msgstr ""

#: crypto.cpp:1784
msgid "libcrypto was not found or successfully loaded."
msgstr ""

#: crypto.cpp:1789
msgid "OpenSSL was successfully loaded."
msgstr ""

#: crypto.cpp:1805
msgid "Personal SSL"
msgstr ""

#: crypto.cpp:1806
msgid "Server SSL"
msgstr ""

#: crypto.cpp:1807
msgid "S/MIME"
msgstr ""

#: crypto.cpp:1808
msgid "PGP"
msgstr ""

#: crypto.cpp:1809
msgid "GPG"
msgstr ""

#: crypto.cpp:1810
msgid "SSL Personal Request"
msgstr ""

#: crypto.cpp:1811
msgid "SSL Server Request"
msgstr ""

#: crypto.cpp:1812
msgid "Netscape SSL"
msgstr ""

#: crypto.cpp:1813
msgid ""
"_: Server certificate authority\n"
"Server CA"
msgstr ""

#: crypto.cpp:1814
msgid ""
"_: Personal certificate authority\n"
"Personal CA"
msgstr ""

#: crypto.cpp:1815
msgid ""
"_: Secure MIME certificate authority\n"
"S/MIME CA"
msgstr ""

#: crypto.cpp:1897
msgid "None"
msgstr ""

#: certexport.cpp:45
msgid "X509 Certificate Export"
msgstr ""

#: certexport.cpp:47
msgid "Format"
msgstr ""

#: certexport.cpp:48
msgid "&PEM"
msgstr ""

#: certexport.cpp:49
msgid "&Netscape"
msgstr ""

#: certexport.cpp:50
msgid "&DER/ASN1"
msgstr ""

#: certexport.cpp:51
msgid "&Text"
msgstr ""

#: certexport.cpp:55
msgid "Filename:"
msgstr ""

#: certexport.cpp:66
#, fuzzy
msgid "&Export"
msgstr "Извоз..."

#: certexport.cpp:94
msgid "Internal error.  Please report to kfm-devel@kde.org."
msgstr ""

#: certexport.cpp:109
msgid "Error converting the certificate into the requested format."
msgstr ""

#: certexport.cpp:117
msgid "Error opening file for output."
msgstr ""

#: certexport.cpp:133
msgid ""
"*.pem|Privacy Enhanced Mail format\n"
"*.der|DER/ASN1 format"
msgstr ""

#: kdatetimedlg.cpp:41
msgid "Date and Time Selector"
msgstr ""

#: kdatetimedlg.cpp:46
msgid "Hour:"
msgstr ""

#: kdatetimedlg.cpp:51
msgid "Minute:"
msgstr ""

#: kdatetimedlg.cpp:56
msgid "Second:"
msgstr ""

#~ msgid "Use &TLS instead of SSLv2/v3"
#~ msgstr "Користи &TLS наместо SSLv2/v3"

#, fuzzy
#~ msgid ""
#~ "TLS is the proposed new implementation of secure sockets.  Enabling TLS "
#~ "in this form disables SSLv2 and SSLv3.  You should leave this turned off."
#~ msgstr ""
#~ "TLS е предложената нова имплементација на secure sockets.  Овозможувањето "
#~ "на TLS ги оневозможува SSLv2 и SSLv3.  Во повеќето случае оваа опција "
#~ "треба да е исклучена."
