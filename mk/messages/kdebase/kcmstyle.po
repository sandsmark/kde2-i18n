# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2000 Free Software Foundation, Inc.
# Danko Ilik <danko@mindless.com>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmstyle\n"
"POT-Creation-Date: 2001-07-02 17:48+0200\n"
"PO-Revision-Date: 2000-07-12 22:00+0100\n"
"Last-Translator: Danko Ilik <danko@mindless.com>\n"
"Language-Team: Macedonian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: general.cpp:155
#, fuzzy
msgid "Name"
msgstr "Име:"

#: general.cpp:156
#, fuzzy
msgid "Description"
msgstr "Опис:"

#: general.cpp:196
msgid "No description available."
msgstr "Нема опис."

#: general.cpp:306
#, fuzzy
msgid "Widget style and theme"
msgstr "Стил и тема на елементите:"

#: general.cpp:313
#, fuzzy
msgid ""
"Here you can choose from a list of predefined widget styles (e.g. the way "
"buttons are drawn) which may or may not be combined with a theme (additional "
"information like a marble texture or a gradient)."
msgstr ""
"Тука се избира од листа на предефинирани "
"стилови на елементите (на пример, начинот "
"на кој се исцртуваат копчињата) кои може, "
"но не мора, да бидат во комбинација со "
"тема (на пример, градиент или текстура на "
"мармер)."

#: general.cpp:321
msgid "&Import GTK themes..."
msgstr ""

#: general.cpp:323
msgid ""
"This launches KDE GTK style importer which allows you to convert legacy GTK "
"pixmap themes to KDE widget styles."
msgstr ""

#: general.cpp:334
#, fuzzy
msgid "Other settings for drawing"
msgstr "Други поставки за прикажување:"

#: general.cpp:340
msgid "&Menubar on top of the screen in the style of MacOS"
msgstr "Мени на врвот на екранот во стил на MacOS"

#: general.cpp:342
msgid ""
"If this option is selected, applications won't have their menubar attached "
"to their own window anymore. Instead, there is one menu bar at the top of "
"the screen which shows the menu of the currently active application. Maybe "
"you know this behavior from MacOS."
msgstr ""
"Со оваа опција менијата во апликациите "
"нема да бидат се наоѓаат во прозорите на "
"апликациите, туку ќе има единствено мени "
"на врвот од екранот, мени на моментално "
"активната апликација. Ова поведение се "
"сретнува кај MacOS."

#: general.cpp:351
msgid "&Apply fonts and colors to non-KDE apps"
msgstr ""
"Примени ги фонтовите и боите на не-КДЕ "
"апликации"

#: general.cpp:355
msgid ""
"If this option is selected, KDE will try to force your preferences regarding "
"colors and fonts even on non-KDE applications. While this works fine with "
"most applications, it <em>may</em> give strange results sometimes."
msgstr ""
"Со оваа опција, КДЕ ќе се обиде да ги "
"наметне параметрите за изгледот, боите и "
"фонтовите на не-КДЕ апликации. Иако тоа "
"најчесто поминува во ред, понекогаш може "
"да се појават чудни ефекти во изледот."

#: general.cpp:360
#, fuzzy
msgid "Style options for toolbars"
msgstr "Опции за стилот на алатките:"

#: general.cpp:370
msgid "&Icons only"
msgstr "Само икони"

#: general.cpp:371
msgid "&Text only"
msgstr "Само текст"

#: general.cpp:372
msgid "Text a&side icons"
msgstr "Текст покрај икони"

#: general.cpp:373
msgid "Text &under icons"
msgstr "Текст под икони"

#: general.cpp:375
msgid "Shows only icons on toolbar buttons. Best option for low resolutions."
msgstr ""
"Ги прикажува само иконите на копчињата на "
"алатките, што е добро за мали резолуции на "
"екранот."

#: general.cpp:376
msgid "Shows only text on toolbar buttons."
msgstr ""
"Прикажува само текст на копчињата на "
"алатките."

#: general.cpp:377
msgid "Shows icons and text on toolbar buttons. Text is aligned aside the icon."
msgstr ""
"Прикажува и икони и текст на копчињата на "
"алатките. Текстот е порамнет од веднаш до "
"иконата."

#: general.cpp:378
msgid "Shows icons and text on toolbar buttons. Text is aligned below the icon."
msgstr ""
"Прикажува и икони и текст на копчињата на "
"алатките. Текстот е порамнет од веднаш "
"под иконата."

#: general.cpp:380
msgid "&Highlight buttons under mouse"
msgstr "Осветли ги копчињата под глушецот"

#: general.cpp:381
msgid "Tool&bars are transparent when moving"
msgstr "Алатките се провидни при преместување"

#: general.cpp:384
msgid ""
"If this option is selected, toolbar buttons will change their color when the "
"mouse cursor is moved over them."
msgstr ""
"Со оваа опција, копчињата на алатките ја "
"менуваат бојата кога глушецот се движи "
"над нив."

#: general.cpp:386
msgid ""
"If this option is selected, only the skeleton of a toolbar will be shown "
"when moving that toolbar."
msgstr ""
"Со оваа опција, само рамката на панелот со "
"алатки ќе се исцртува при преместување на "
"алатките."

#: general.cpp:407
msgid "Effect options"
msgstr ""

#: general.cpp:417
msgid "No m&enu effect"
msgstr ""

#: general.cpp:418
msgid "&Fade menus"
msgstr ""

#: general.cpp:419
msgid "Anima&te menus"
msgstr ""

#: general.cpp:423
msgid "Animate &combo boxes"
msgstr ""

#: general.cpp:425
msgid "If this option is selected, menus open immediately."
msgstr ""

#: general.cpp:426
msgid "If this option is selected, menus fade in slowly."
msgstr ""

#: general.cpp:427
msgid "If this option is selected, menus are animated to grow to their full size."
msgstr ""

#: general.cpp:428
msgid ""
"If this option is selected, combo boxes are animated to grow to their full "
"size."
msgstr ""

#: general.cpp:447
msgid "Tooltips"
msgstr ""

#: general.cpp:454
msgid "&Disable tool tips"
msgstr ""

#: general.cpp:455
msgid "&Plain tool tips"
msgstr ""

#: general.cpp:456
msgid "Fade tool t&ips"
msgstr ""

#: general.cpp:457
msgid "&Rollout tool t&ips"
msgstr ""

#: general.cpp:458
msgid "If this option is selected, tooltips are disabled in all applications."
msgstr ""

#: general.cpp:459
msgid "If this option is selected, tooltips appear with no effect."
msgstr ""

#: general.cpp:460
msgid "If this option is selected, tooltips fade in slowly."
msgstr ""

#: general.cpp:461
msgid "If this option is selected, tooltips roll out slowly."
msgstr ""

#: general.cpp:656
#, fuzzy
msgid ""
"<h1>Style</h1> In this module you can configure how your KDE applications "
"will look.<p>"
" Styles and themes affect the way buttons, menus etc. are drawn in KDE.  "
"Think of styles as plugins that provide a general look for KDE and of themes "
"as a way to fine-tune those styles. E.g. you might use the \"System\" style "
"but use a theme that provides a color gradient or a marble texture.<p>"
" Apart from styles and themes you can configure here the behavior of "
"menubars and toolbars."
msgstr ""
"<h1>Стил</h1> Во овој модул се конфигурира "
"изгледот на КДЕ апликациите. <p> Стиловите "
"и темите се начинот на кој копчињата, "
"менијата итн. се исцртуваат во КДЕ.  На "
"стиловите може да се гледа како на "
"плагини кои обезбедуваат општ изглед на "
"КДЕ, а на темите како начин за фино "
"штелување на тој изглед. На пример, можеби "
"ќе сакате да го користите \"Системскиот\" "
"стил, а тема која дава градација на боите "
"или текстура на мермер.<p> Освен стиловите "
"и темите тука се конфигурираат и "
"мени-линиите и линиите со алатки."
