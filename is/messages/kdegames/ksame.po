# Icelandic translation.
# Copyright (C) 1998 Bjarni R. Einarsson
# Bjarni R. Einarsson, <bre@mmedia.is>, 1998
#
msgid ""
msgstr ""
"Project-Id-Version: ksame 2.0\n"
"POT-Creation-Date: 2001-06-10 17:19+0200\n"
"PO-Revision-Date: 2001-02-08 23:31-0500\n"
"Last-Translator: Richard Allen <ra@hp.is>\n"
"Language-Team: Icelandic <kde-isl@mmedia.is>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: HighScore.cpp:67
msgid "Highscores"
msgstr "S�na methafa"

#: HighScore.cpp:148
msgid "Please &enter your name:"
msgstr "Sl��u &inn nafni� �itt:"

#: HighScore.cpp:180 HighScore.cpp:219
msgid "Anonymous"
msgstr "Nafnlaus"

#: KSameWidget.cpp:52
msgid "&Restart this board"
msgstr "&Endurr�sa �etta bor�"

#: KSameWidget.cpp:56
msgid "S&how Highscore"
msgstr "S�na &methafa"

#: KSameWidget.cpp:63
msgid "&Random Board"
msgstr "Bo&r� af handah�fi"

#: KSameWidget.cpp:68
msgid "Colors: XX"
msgstr "Litir: XX"

#: KSameWidget.cpp:69
msgid "Board: XXXXXX"
msgstr "Bor�: XXXXXX"

#: KSameWidget.cpp:70
msgid "Marked: XXXXXX"
msgstr "Merkt: XXXXXX"

#: KSameWidget.cpp:71
msgid "Score: XXXXXX"
msgstr "Stig: XXXXXX"

#: KSameWidget.cpp:132
msgid "Do you want to resign?"
msgstr "Viltu gefast upp?"

#: KSameWidget.cpp:146
msgid "Select a board"
msgstr "Veldu bor�"

#: KSameWidget.cpp:216
msgid "%1 Colors"
msgstr "%1 Litir"

#: KSameWidget.cpp:220
#, c-format
msgid "Board: %1"
msgstr "Bor�: %1"

#: KSameWidget.cpp:224
#, c-format
msgid "Marked: %1"
msgstr "Merkt: %1"

#: KSameWidget.cpp:229
msgid "%1 stones removed."
msgstr "%1 steinar fjarl�g�ir."

#: KSameWidget.cpp:233
#, c-format
msgid "Score: %1"
msgstr "Stig: %1"

#: KSameWidget.cpp:242
msgid "You removed even the last stone, great job! This gave you a score of %1 in total."
msgstr ""
"�� fjarl�g�ir jafnvel s��asta steininn. Vel gert! �etta gefur ��r samtals %1 "
"stig."

#: KSameWidget.cpp:246
msgid "There are no more removeable stones. You got a score of %1 in total."
msgstr "�a� eru engir steinar eftir. �� ert me� samtals %1 stig."

#: main.cpp:32
msgid "Same Game - a little game about balls and how to get rid of them"
msgstr "Samaspili� - L�till leikur um bolta og hvernig � a� losna vi� ��"

#: main.cpp:35
msgid "SameGame"
msgstr "Um Samaspili�"

#: _translatorinfo.cpp:1
msgid "_: NAME OF TRANSLATORS\nYour names"
msgstr "Bjarni R. Einarsson,Richard Allen,��rarinn R. Einarsson"

#: _translatorinfo.cpp:3
msgid "_: EMAIL OF TRANSLATORS\nYour emails"
msgstr "bre@mmedia.is,ra@hp.is,thori@mindspring.com"
