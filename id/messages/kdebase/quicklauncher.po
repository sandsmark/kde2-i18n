# quicklauncher (id)
# Copyright (C) 2001 Free Software Foundation, Inc.
# Tedi Heriyanto <tedi-h@usa.net>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-04-06 14:03GMT+0700\n"
"Last-Translator: Tedi Heriyanto <tedi-h@usa.net>\n"
"Language-Team: Indonesian <id@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.8\n"

#: quickbutton.cpp:68 quicklauncher.cpp:78
msgid "Add Application"
msgstr "Tambah Aplikasi"

#: quicklauncher.cpp:136
msgid "Quick Launcher"
msgstr "Quick Launcher"

#: quicklauncher.cpp:137
msgid "A simple application launcher"
msgstr ""

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#~ msgid ""
#~ "Quick Launcher 1.0\n"
#~ "Copyright (c) 2000 by\n"
#~ "Bill Nagel <bn@purdue.edu>"
#~ msgstr ""
#~ "Quick Launcher 1.0\n"
#~ "Copyright (c) 2000 by\n"
#~ "Bill Nagel <bn@purdue.edu>"
