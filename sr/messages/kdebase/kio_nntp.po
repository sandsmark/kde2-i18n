# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-05-23 14:35 CEST\n"
"Last-Translator: Slobodan Marković <twiddle@eunet.yu>\n"
"Language-Team: Serbian <sr@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.1beta\n"

#: nntp.cpp:154
#, c-format
msgid "Invalid special command %1"
msgstr ""

#: nntp.cpp:362
#, c-format
msgid ""
"Could not extract first message number from server response:\n"
"%1"
msgstr ""

#: nntp.cpp:384
#, c-format
msgid ""
"Could not extract first message id from server response:\n"
"%1"
msgstr ""

#: nntp.cpp:411
#, c-format
msgid ""
"Could not extract message id from server response:\n"
"%1"
msgstr ""

#: nntp.cpp:603
msgid ""
"Unexpected server response to %1 command:\n"
"%2"
msgstr ""
