# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Aleksandar Dezelin <deza@ptt.yu>, 2001
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-23 20:39+0200\n"
"PO-Revision-Date: 2001-08-23 19:18CEST\n"
"Last-Translator: Marko Rosić <roske@kde.org.yu>\n"
"Language-Team: Serbian <sr@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: kdm-appear.cpp:79
msgid "&Greeting:"
msgstr "&Pozdrav:"

#: kdm-appear.cpp:84
msgid ""
"This is the string KDM will display in the login window. You may want to put "
"here some nice greeting or information about the operating system.<p> KDM "
"will replace the string [HOSTNAME] with the actual host name of the computer "
"running the X server. Especially in networks this is a good idea."
msgstr ""
"Ovo je poruka koju će KDM prikazati u prozoru za prijavljivanje na sistem. "
"Ako želite, možete je postaviti da prikazuje neku fini pozdrav ili "
"informaciju vezanu za sam sistem.<p> KDM će zameniti string [HOSTNAME] "
"imenomhosta računara na kome se vrti X server. Ovo je posebno dobra ideja "
"ako je računar u mrežnom okruženju."

#: kdm-appear.cpp:98
#, fuzzy
msgid "Logo area:"
msgstr "&Područje logotipa:"

#: kdm-appear.cpp:103 kdm-conv.cpp:83 kdm-users.cpp:161
msgid "&None"
msgstr "&Nijedan"

#: kdm-appear.cpp:104
#, fuzzy
msgid "Show cloc&k"
msgstr "Prikaži časovnik"

#: kdm-appear.cpp:105
#, fuzzy
msgid "Sho&w logo"
msgstr "Prikaži logotip"

#: kdm-appear.cpp:119
msgid ""
"You can choose to display a custom logo (see below), a clock or no logo at "
"all."
msgstr ""
"Možete izabrati prikazivanje vašeg logotipa (pogledajte ispod), časovnika "
"ili ne prikazivanje ničega."

#: kdm-appear.cpp:125
msgid "&Logo:"
msgstr "&Logotip:"

#: kdm-appear.cpp:138
msgid ""
"Click here to choose an image that KDM will display. You can also drag and "
"drop an image onto this button (e.g. from Konqueror)."
msgstr ""
"Kliknite ovde da biste izabrali sliku koju će KDM prikazati. Takođe "
"možete prevući i pustiti neku sliku na ovo dugme (npr. iz Konqueror-a)."

#: kdm-appear.cpp:151
#, fuzzy
msgid "Position:"
msgstr "Poz&icija:"

#: kdm-appear.cpp:156
#, fuzzy
msgid "Cente&red"
msgstr "Centrirano"

#: kdm-appear.cpp:157
#, fuzzy
msgid "Spec&ify"
msgstr "Navedi"

#: kdm-appear.cpp:169
msgid ""
"You can choose whether the login dialog should be centered or placed at "
"specified coordinates."
msgstr ""
"Možete izabrati da li će dijalog za prijavljivanje biti centriran ili će "
"biti pozicioniran na određenim koordinatama."

#: kdm-appear.cpp:178
msgid "&X"
msgstr "&X"

#: kdm-appear.cpp:184
msgid "&Y"
msgstr "&Y"

#: kdm-appear.cpp:190
msgid "Here you specify the coordinates of the login dialog's <em>center</em>."
msgstr "Ovde navodite koordinate <em>centra</em> dijaloga za prijavljivanje."

#: kdm-appear.cpp:204
msgid "GUI S&tyle:"
msgstr "GUI s&til:"

#: kdm-appear.cpp:212
msgid "You can choose a basic GUI style here that will be used by KDM only."
msgstr "Ovde možete izabrati osnovni GUI stil koji će koristiti samo KDM."

#: kdm-appear.cpp:217
#, fuzzy
msgid "Echo &mode:"
msgstr "&Eho režim:"

#: kdm-appear.cpp:220
msgid "No echo"
msgstr "Bez eha"

#: kdm-appear.cpp:221
msgid "One Star"
msgstr "Jedna zvezdica"

#: kdm-appear.cpp:222
msgid "Three Stars"
msgstr "Tri zvezdice"

#: kdm-appear.cpp:226
msgid "You can choose whether and how KDM shows your password when you type it."
msgstr ""
"Možete izabrati da li će, i kako KDM prikazati vašu lozinku kada je "
"budete unosili."

#: kdm-appear.cpp:232
#, fuzzy
msgid "Locale"
msgstr "Po meri ekrana"

#: kdm-appear.cpp:238
#, fuzzy
msgid "Languag&e:"
msgstr "&Jezik:"

#: kdm-appear.cpp:247
msgid ""
"Here you can choose the language used by KDM. This setting doesn't affect a "
"user's personal settings that will take effect after login."
msgstr ""
"Ovde možete izabrati jezik koji će KDM koristiti. Ovo podešavanje ne "
"menja lična podešavanja korisnika; ona će biti uzeta u obzir nakon "
"prijavljivanja na sistem."

#: kdm-appear.cpp:291
msgid "without name"
msgstr "bez imena"

#: kdm-appear.cpp:386 kdm-users.cpp:281
msgid ""
"There was an error loading the image:\n"
"%1\n"
"It will not be saved..."
msgstr ""
"Nastala je greška pri učitavanju slike:\n"
"%1\n"
"Neće biti sačuvana..."

#: kdm-appear.cpp:494
msgid ""
"<h1>KDM - Appearance</h1>"
" Here you can configure the basic appearance of the KDM login manager, i.e. "
"a greeting string, an icon etc.<p> For further refinement of KDM's "
"appearance, see the \"Font\" and \"Background\"  tabs."
msgstr ""
"<h1>Izgled KDM-a</h1> Ovde možete prilagoditi osnovne osobine izgleda KDM "
"Menadžera prijavljivanja, tj. poruku pozdrava, ikonu itd.<p> Za dalje "
"prilagođavanje izgleda KDM-a, pogledajte tabove \"Font\" i \"Pozadina\"."

#: kdm-font.cpp:41
msgid "Select fonts"
msgstr "Izaberite fontove"

#: kdm-font.cpp:42
msgid "Example"
msgstr "Primer"

#: kdm-font.cpp:44
msgid "Shows a preview of the selected font."
msgstr "Prikazuje pregled odbranog fonta."

#: kdm-font.cpp:46
msgid "C&hange font..."
msgstr "P&romeni font..."

#: kdm-font.cpp:50
msgid "Click here to change the selected font."
msgstr "Kliknite ovde da biste promenili izabrani font."

#: kdm-font.cpp:54
msgid "Greeting"
msgstr "Pozdrav"

#: kdm-font.cpp:55
msgid "Fail"
msgstr "Neuspeh"

#: kdm-font.cpp:56
msgid "Standard"
msgstr "Standardan"

#: kdm-font.cpp:59
msgid ""
"Here you can select the font you want to change. KDM knows three fonts: "
"<ul><li><em>Greeting:</em> used to display KDM's greeting string (see "
"\"Appearance\" tab)</li><li><em>Fail:</em> used to display a message when a "
"person fails to login</li><li><em>Standard:</em> used for the rest of the "
"text</li></ul>"
msgstr ""
"Ovde možete izabrati font koji želite da promenite. KDM poznaje tri fonta: "
"<ul><li><em>Pozdrav:</em> se koristi za prikaz pozdravne poruke KDM-a "
"(pogledajte tab \"Izgled\" )</li><li><em>Neuspeh:</em> se koristi za prikaz "
"poruke kada korisnik ne uspe da se prijavi na "
"sistem</li><li><em>Standardni:</em> se koristi za ostali tekst</li></ul>"

#: kdm-font.cpp:172
msgid "Greeting font"
msgstr "Font pozdravne poruke"

#: kdm-font.cpp:176
msgid "Fail font"
msgstr "Font neuspešnog prijavljivanja"

#: kdm-font.cpp:180
msgid "Standard font"
msgstr "Standardni font"

#: kdm-sess.cpp:50
#, fuzzy
msgid "Allow shutdown"
msgstr "Doz&voli gašenje"

#: kdm-sess.cpp:53
#, fuzzy
msgid "Co&nsole:"
msgstr "Samo konzola"

#: kdm-sess.cpp:54 kdm-sess.cpp:60
msgid "Everybody"
msgstr ""

#: kdm-sess.cpp:55 kdm-sess.cpp:61
msgid "Only root"
msgstr ""

#: kdm-sess.cpp:56 kdm-sess.cpp:62
msgid "Nobody"
msgstr ""

#: kdm-sess.cpp:59
#, fuzzy
msgid "Re&mote:"
msgstr "&Ukloni"

#: kdm-sess.cpp:64
#, fuzzy
msgid ""
"Here you can select who is allowed to shutdown the computer using KDM. You "
"can specify different values for local (console) and remote displays. "
"Possible values are:<ul> <li><em>Everybody:</em> everybody can shutdown the "
"computer using KDM</li> <li><em>Only root:</em>"
" KDM will only allow shutdown after the user has entered the root "
"password</li> <li><em>Nobody:</em> nobody can shutdown the computer using "
"KDM</li></ul>"
msgstr ""
"Ovde možete odabrati kome je dozvoljeno gašenje računara korišćenjem  "
"KDM-a. Moguće vrednost su:<ul> <li><em>Svi:</em> Svako može ugasiti "
"računar koristeći KDM</li> <li><em>Samo konzola:</em> samo ga korisnici "
"koji sede za ovim računarom mogu ugasiti koristeći KDM</li> <li><em>Samo "
"'root':</em> KDM će dozvoliti gašenje računara nakon što korisnik unese "
"'root' lozinku</li> <li><em>Niko:</em> niko ne može ugasiti računar "
"koristeći KDM</li></ul>"

#: kdm-sess.cpp:72
msgid "Commands"
msgstr "Komande"

#: kdm-sess.cpp:75
msgid "Ha&lt"
msgstr ""

#: kdm-sess.cpp:78
#, fuzzy
msgid "Command to initiate the system halt. Typical value: /sbin/halt"
msgstr "Komanda koja započinje sekvencu gašenja. Uobičajena vrednost: /sbin/halt"

#: kdm-sess.cpp:83
msgid "&Reboot"
msgstr ""

#: kdm-sess.cpp:86
#, fuzzy
msgid "Command to initiate the system reboot. Typical value: /sbin/reboot"
msgstr ""
"Komanda koja započinje sekvencu ponovnog pokretanja. Uobičajena vrednost: "
"/sbin/reboot"

#: kdm-sess.cpp:91
msgid "Lilo"
msgstr "Lilo"

#: kdm-sess.cpp:93
#, fuzzy
msgid "Show boot opt&ions"
msgstr "Prikaži 'boot' opcije"

#: kdm-sess.cpp:98
msgid "Enable Lilo boot options in the \"Shutdown ...\" dialog."
msgstr ""

#: kdm-sess.cpp:102
msgid "Lilo command"
msgstr "Komanda lilo"

#: kdm-sess.cpp:105
msgid "Command to run Lilo. Typical value: /sbin/lilo"
msgstr "Komanda kojom se startuje Lilo. Uobičajena vrednost: /sbin/lilo"

#: kdm-sess.cpp:110
msgid "Lilo map file"
msgstr "Lilo map fajl"

#: kdm-sess.cpp:113
msgid "Position of Lilo's map file. Typical value: /boot/map"
msgstr "Lokacija lilo map fajla. Uobičajena vrednost: /boot/map"

#: kdm-sess.cpp:118
msgid "Session types"
msgstr "Tipovi sesija"

#: kdm-sess.cpp:121
msgid "New t&ype"
msgstr "Novi &tip"

#: kdm-sess.cpp:128
msgid ""
"To create a new session type, enter its name here and click on <em>Add "
"new</em>"
msgstr ""
"Da biste kreirali novi tip sesije, unesite ovde njeno ime i kliknite na "
"<em>Dodaj</em>"

#: kdm-sess.cpp:132
#, fuzzy
msgid "Add ne&w"
msgstr "Dodaj &novu"

#: kdm-sess.cpp:136
msgid ""
"Click here to add the new session type entered in the <em>New type</em> "
"field to the list of available sessions."
msgstr ""
"Kliknite ovde da biste dodali novi tip sesije unešen u polju <em>Novi "
"tip</em> u listu raspoloživih sesija."

#: kdm-sess.cpp:139
msgid "Available &types"
msgstr "Rapoloživi &tipovi"

#: kdm-sess.cpp:142
msgid ""
"This box lists the available session types that will be presented to the "
"user. Names other than \"default\" and \"failsafe\" are usually treated as "
"program names, but it depends on your Xsession script what the session type "
"means."
msgstr ""
"Ova lista sadrži tipove sesija koji će biti na raspolaganju korisnicima. "
"Nazivi različiti od \"default\" i \"failsafe\" obično se tretiraju kao "
"nazivi programa, ali to zavisi od toga kako se tipovi sesija tretiraju u "
"vašoj Xsession skripti."

#: kdm-sess.cpp:148
msgid "R&emove"
msgstr "&Ukloni"

#: kdm-sess.cpp:152
msgid "Click here to remove the currently selected session type"
msgstr "Kliknite ovde da uklonite trenutno odabran tip sesije."

#: kdm-sess.cpp:163
msgid ""
"With these two arrow buttons, you can change the order in which the "
"available session types are presented to the user"
msgstr ""
"Uz pomoć ova dva navigaciona dugmeta možete promeniti redosled u kojem su "
"raspoloživi tipovi sesija dostupni korisniku."

#: backgnd.cpp:112
msgid "abcdefgh"
msgstr "abcdefgh"

#: backgnd.cpp:126
msgid ""
"Here you can see a preview of how KDM's background will look like using the "
"current settings. You can even set a background picture by dragging it onto "
"the preview (e.g. from Konqueror)."
msgstr ""
"Ovde možete videti kako će KDM pozadina izgledati uz trenutna "
"podešavanja. Možete čak i postaviti sliku pozadine tako što će te je "
"dovući na pregled. (npr. iz Konqueror-a)."

#: backgnd.cpp:136
msgid "Back&ground"
msgstr "&Pozadina"

#: backgnd.cpp:145 backgnd.cpp:211 bgdialogs.cpp:677
msgid "&Mode:"
msgstr "&Režim:"

#: backgnd.cpp:154
msgid ""
"Here you can change the way colors are applied to KDM's background. Apart of "
"just a plain color you can choose gradients, custom patterns or a background "
"program (e.g. xearth)."
msgstr ""
"Ovde možete promeniti način iscrtavanja pozadine KDM-a. Sem jednobojne "
"pozadine, možete izabrati prilagođeni gradient, prilagođenu šaru ili "
"pozadinski program (npr. xearth)."

#: backgnd.cpp:160
msgid "Color &1:"
msgstr "Boja &1:"

#: backgnd.cpp:170
msgid ""
"By clicking on these buttons you can choose the colors KDM will use to paint "
"the background. If you selected a gradient or a pattern, you can choose two "
"colors."
msgstr ""
"Kada kliknete na neki od ovih dugmića možete izabrati boje koje će KDM "
"koristiti da oboji pozadinu. Ako izaberete gradient ili šaru, možete "
"odabrati dve boje."

#: backgnd.cpp:176
msgid "Color &2:"
msgstr "Boja &2:"

#: backgnd.cpp:191 backgnd.cpp:258
msgid "S&etup"
msgstr "&Podešavanja"

#: backgnd.cpp:197
msgid "Click here to setup a pattern or a background program."
msgstr "Kliknite ovde da biste postavili šaru ili neki pozadinski program."

#: backgnd.cpp:202
msgid "Wa&llpaper"
msgstr "&Slika pozadine"

#: backgnd.cpp:220
msgid ""
"Here you can choose the way the selected picture will be used to cover KDM's "
"background. If you select \"No Wallpaper\", the colors settings will be used."
msgstr ""
"Ovde možete izabrati način na koji će izabrana slika prekriti pozadinu "
"KDM-a. Ako izaberete \"Bez slike pozadine\", biće korištene podešene boje."

#: backgnd.cpp:225
msgid "&Wallpaper"
msgstr "&Slika pozadine"

#: backgnd.cpp:235
msgid ""
"Here you can choose from several wallpaper pictures, i.e. pictures that have "
"been installed in the system's or your wallpaper directory. If you want to "
"use other pictures, you can either click on the \"Browse\" button or drag a "
"picture (e.g. from Konqueror) onto the preview."
msgstr ""
"Ovde možete izabrati jednu od nekoliko slika pozadine, npr. od slika koje "
"su ubačene u sam sistem ili u vaš direktorijum slika pozadine. Ako želite "
"da izaberete neku drugu sliku, možete ili kliknuti na dugme \"Pregledaj\" "
"ili prevući sliku do pregleda (npr. iz Konqueror-a)."

#: backgnd.cpp:244
msgid "B&rowse"
msgstr "Pre&gledaj"

#: backgnd.cpp:250
msgid "Click here to choose a wallpaper using a file dialog."
msgstr "Kliknite ovde da biste odabrali sliku pozadine koristeći fajl dijalog."

#: backgnd.cpp:252
msgid "Mul&tiple:"
msgstr "&Višestruka:"

#: backgnd.cpp:303
msgid "Flat"
msgstr "Jednobojna"

#: backgnd.cpp:304 bgdialogs.cpp:361
msgid "Pattern"
msgstr "Šara"

#: backgnd.cpp:305
msgid "Background Program"
msgstr "Pozadinski program"

#: backgnd.cpp:306
msgid "Horizontal Gradient"
msgstr "Horizontalni gradient"

#: backgnd.cpp:307
msgid "Vertical Gradient"
msgstr "Vertikalni gradient"

#: backgnd.cpp:308
msgid "Pyramid Gradient"
msgstr "Piramidalni gradient"

#: backgnd.cpp:309
msgid "Pipecross Gradient"
msgstr "Pipecross gradient"

#: backgnd.cpp:310
msgid "Elliptic Gradient"
msgstr "Eliptičan gradient"

#: backgnd.cpp:322
msgid "No Wallpaper"
msgstr "Bez slike pozadine"

#: backgnd.cpp:323
msgid "Centred"
msgstr "Centrirana"

#: backgnd.cpp:324
msgid "Tiled"
msgstr "Raširena"

#: backgnd.cpp:325
msgid "Center Tiled"
msgstr "Centrirano raširena"

#: backgnd.cpp:326
msgid "Centred Maxpect"
msgstr "Centrirani uveličana"

#: backgnd.cpp:327
msgid "Scaled"
msgstr "Po meri ekrana"

#: backgnd.cpp:591
msgid "Currently are only local wallpapers allowed."
msgstr "Trenutno su dozvoljene samo lokalne slike pozadine."

#: kdm-users.cpp:51
msgid "<default>"
msgstr "<predefinisano>"

#: kdm-users.cpp:57
msgid "&Remaining users"
msgstr "&Preostali korisnici"

#: kdm-users.cpp:60
msgid "S&elected users"
msgstr "&Odabrani korisnici"

#: kdm-users.cpp:63
msgid "No-sho&w users"
msgstr "Korisnici koji se neće &prikazivati"

#: kdm-users.cpp:75
msgid ""
"Click here to add the highlighted user on the left to the list of selected "
"users on the right, i.e. users that should in any case be shown in KDM's "
"user list."
msgstr ""
"Kliknite ovde da biste dodali osvetljenog korisnika sa leve strane, na listu "
"odabranih korisnika na desnoj strani, npr. korisnike koji bi u svakom "
"slučaju trebali da budu prikazani u listi korisnika KDM-a."

#: kdm-conv.cpp:149 kdm-users.cpp:83
msgid "Click here to remove the highlighted user from the list of selected users."
msgstr ""
"Kliknite ovde da biste uklonili osvetljenog korisnika iz liste odabranih "
"korisnika."

#: kdm-users.cpp:93
msgid ""
"Click here to add the highlighted user on the left to the list of users "
"explicitly not shown by KDM."
msgstr ""
"Kliknite ovde da biste dodali osvetljenog korisnika na levoj strani u listu "
"korisnika koji eksplicitno neće biti prikazani od strane KDM-a."

#: kdm-users.cpp:101
msgid ""
"Click here to remove the highlighted user from the list of users explicitly "
"not shown by KDM."
msgstr ""
"Kliknite ovde da biste uklonili osvetljenog korisnika iz liste korisnika "
"koji eksplicitno neće biti prikazani od strane KDM-a."

#: kdm-users.cpp:109
msgid ""
"This is the list of users for which no explicit show policy has been set, "
"i.e. they will only be shown by KDM if \"Show users\" is \"all but no-show\"."
msgstr ""
"Ovo je lista korisnika za koje nije postavljena eksplicitna prikazna "
"politika, tj. oni će biti prikazani od strane KDM-a samo u slučaju da  je "
"\"Prikaži korisnike\" postavljeno na \"Svi sem neprikazani\"."

#: kdm-users.cpp:117
msgid "This is the list of users KDM will show in its login dialog in any case."
msgstr ""
"Ovo je lista korisnika koju će KDM uvek prikazati u svom dijalogu za "
"prijavljivanje."

#: kdm-users.cpp:124
msgid ""
"This is the list of users KDM will not show in its login dialog. Users "
"(except for root) with a user ID less than the one specified in the \"Hide "
"UIDs below\" field will not be shown, too."
msgstr ""
"Ovo je lista korisnika koje KDM neće prikazati u svom dijalogu za "
"prijavljivanje. Korisnici (izuzev 'root' korisnika) koji poseduju "
"korisnički ID manji od onog navedenog u polju \"Sakrij manje UID-ove\" "
"neće biti takođe prikazani."

#: kdm-users.cpp:150
msgid "Click or drop an image here"
msgstr "Ovde kliknite ili dovucite i pustite neku sliku."

#: kdm-users.cpp:152
msgid ""
"Here you can see the username of the currently selected user and the image "
"assigned to this user. Click on the image button to select from a list of "
"images or drag and drop your own image onto the button (e.g. from Konqueror)."
msgstr ""
"Ovde možete videti korisničko ime trenutno odabranog korisnika kao i sliku "
"pridodatu tom korisniku. Kliknite na dugme sa slikom da biste izabrali sliku "
"izliste slika ili prevucite i pustite neku vašu sliku na dugme (npr. iz "
"Konqueror-a)."

#: kdm-users.cpp:158
msgid "Show users"
msgstr "Prikaži korisnike"

#: kdm-users.cpp:162
msgid ""
"If this option is selected, KDM will not show any users. If one of the "
"alternative radio buttons is selected, KDM will show a list of users in its "
"login dialog, so users can click on their name and image rather than typing "
"in their login."
msgstr ""
"Ako je ova opcija odabrana, KDM neće prikazati nijednog korisnika. Ako je "
"neko od alternativnih radio dugmića odabrano, KDM će prikazati listu "
"korisnika u svom dijalogu za prijavljivanje, tako da korisnici mogu da "
"kliknu na svoje ime i sliku umesto da ukucaju isto pri prijavljivanju."

#: kdm-users.cpp:166
msgid "Selected onl&y"
msgstr "Samo &odabrani"

#: kdm-users.cpp:167
msgid ""
"If this option is selected, KDM will only show the users listed in the "
"\"selected users\" listbox in its login dialog."
msgstr ""
"Ako je ova opcija odabrana, KDM će u svom dijalogu za prijavljivanje "
"prikazati samo korisnike izlistane u listbox-u \"odabrani korisnici\"."

#: kdm-users.cpp:169
msgid "A&ll but no-show"
msgstr "&Svi sem neprikazani"

#: kdm-users.cpp:170
msgid ""
"If this option is selected, KDM will show all users but those who are listed "
"in the \"no-show users\" listbox or have user ID less then the one specified "
"in the \"Hide UIDs below\" field (except root)."
msgstr ""
"Ako je ova opcija izabrana, KDM će prikazati sve korisnike izuzev onih koji "
"su navedeni u listbox-u \"neprikazani korisnici\" ili imaju korisnički ID "
"manji od onog navedenog u polju \"Sakrij manje UID-ove\" (izuzev 'root' "
"korisnika)."

#: kdm-users.cpp:173
msgid "Sor&t users"
msgstr "Sor&tiraj korisnike"

#: kdm-users.cpp:175
msgid ""
"This option tells KDM to alphabetically sort the users shown in its login "
"dialog."
msgstr ""
"Ova opcija nalaže KDM-u da korisnike prikazane u svom dijalogu za "
"prijavljivanje rasporedi po abecednom redu."

#: kdm-users.cpp:179
msgid "Hide U&IDs below"
msgstr "Sakrij U&IDs ispod"

#: kdm-users.cpp:180
msgid ""
"In \"All but no-show\"-mode all users with a user ID less than this number "
"are hidden. Note, that the root user is not affected by this and must be "
"explicitly listed in \"No-show users\" to be hidden."
msgstr ""
"U režimu rada \"Svi sem neprikazanih\" svi korisnici koji imaju ID manji od "
"ovog broja su skriveni. Primetite da ovo ne važi za 'root' korisnika koji "
"mora biti eksplicitno naveden u grupi \"Neprikazani korisnici\" da bi bio "
"skriven."

#: kdm-users.cpp:227
msgid "Save image as default image?"
msgstr "Sačuvaj kao predefinisanu sliku?"

#: kdm-users.cpp:237
msgid ""
"There was an error saving the image:\n"
"%1\n"
msgstr ""
"Došlo je do greške pri upisu fajla:\n"
"%1\n"

#: kdm-conv.cpp:51
msgid "Automatic login"
msgstr "Automatsko prijavljivanje"

#: kdm-conv.cpp:53
#, fuzzy
msgid "Enable au&to-login"
msgstr "&Aktiviraj auto-prijavljivanje"

#: kdm-conv.cpp:54
msgid ""
"Turn on the auto-login feature. This applies only to KDM's graphical login. "
"Think twice before enabling this!"
msgstr ""
"Uključi auto-prijavljivanje. Ovo se odnosi samo na grafičko prijavljivanje "
"u KDM-u. Razmislite dvaput pre nego što uključite ovu opciju!"

#: kdm-conv.cpp:60
#, fuzzy
msgid "Truly automatic lo&gin"
msgstr "&Stvarno automatsko prijavljivanje"

#: kdm-conv.cpp:61
msgid ""
"When this option is on, the auto-login will be carried out immediately when "
"KDM starts (i.e., when your computer comes up). When this is off, you will "
"need to initiate the auto-login by crashing the X server (by pressing "
"alt-ctrl-backspace)."
msgstr ""
"Kada je ova opcija aktivirana, automatsko prijavljivanje će biti izvršeno "
"odmah kada je KDM startovan (tj. kada se OS podigne) . Kada je ova opcija "
"deaktivirana, moraćete da startujete automatsko prijavljivanje nasilnim "
"zatvaranjem X servera (pritisnete alt-ctrl-backspace)."

#: kdm-conv.cpp:69
#, fuzzy
msgid "Use&r:"
msgstr "&Korisnici"

#: kdm-conv.cpp:71
msgid "Select the user to be logged in automatically from this list."
msgstr "Odaberite korisnike sa liste koji će biti automatski prijavljeni."

#: kdm-conv.cpp:80
#, fuzzy
msgid "Preselect User"
msgstr "&Odabrani korisnici"

#: kdm-conv.cpp:84
#, fuzzy
msgid "Prev&ious"
msgstr "&Pregledaj komandu"

#: kdm-conv.cpp:85
#, fuzzy
msgid "Specif&y"
msgstr "Navedi"

#: kdm-conv.cpp:88
#, fuzzy
msgid "Us&er:"
msgstr "&Korisnici"

#: kdm-conv.cpp:90
#, fuzzy
msgid "Select the user to be preselected for login from this list."
msgstr "Odaberite korisnike sa liste koji će biti automatski prijavljeni."

#: kdm-conv.cpp:97
msgid "Focus pass&word"
msgstr ""

#: kdm-conv.cpp:98
msgid ""
"When this option is on, KDM will place the cursor in the password field "
"instead of the login field after preselecting a user. This will save one key "
"press per login, if the user name is very seldom changed."
msgstr ""

#: kdm-conv.cpp:104
msgid "Password-less login"
msgstr "Prijavljivanje bez lozinke"

#: kdm-conv.cpp:108
msgid "Enable password-&less logins"
msgstr "Omogući &prijavljivanje bez lozinke"

#: kdm-conv.cpp:109
msgid ""
"When this option is checked, the users from the right list will be allowed "
"to log in without entering their password. This applies only to KDM's "
"graphical login. Think twice before enabling this!"
msgstr ""
"Kada je ova opcija overena, korisnicima iz liste sa desne strane biće "
"omogućeno da se prijave bez unošenja lozinke. Ovo važi samo za "
"prijavljivanje u grafičkom režimu preko KDM-a). Razmislite dva puta pre "
"nego što aktivirate ovu opciju!"

#: kdm-conv.cpp:117
#, fuzzy
msgid "Password re&quired"
msgstr "Lozinka je &neophodna"

#: kdm-conv.cpp:122
msgid "This is the list of users which need to type their password to log in."
msgstr ""
"Ovo je lista korisnika koji moraju da otkucaju svoju lozinku pri "
"prijavljivanju."

#: kdm-conv.cpp:126
msgid "S&kip password check"
msgstr ""

#: kdm-conv.cpp:131
msgid "This is the list of users which are allowed in without typing their password."
msgstr ""
"Ovo je lista korisnika kojima je dozvoljeno da se prijave bez unošenja "
"lozinke."

#: kdm-conv.cpp:141
msgid ""
"Click here to add the highlighted user on the left to the list of selected "
"users on the right, i.e. users that are allowed in without entering their "
"password."
msgstr ""
"Kliknite ovde da biste dodali osvetljenog korisnika sa leve strane u listu "
"odabranih korisnika sa desne  strane, tj. listu korisnika kojima je "
"dozvoljeno da se prijave na sistem bez unošenja lozinke."

#: kdm-conv.cpp:157
#, fuzzy
msgid "Automatically log in again after &X server crash"
msgstr "Prijavi se automatski &ponovo nakon pada X servera"

#: kdm-conv.cpp:158
msgid ""
"When this option is on, a user will be logged in again automatically, when "
"his session is interrupted by an X server crash."
msgstr ""
"Kada je ova opcija aktivirana, korisnik će biti automatski prijavljen ako "
"je njegova sesija prekinuta nasilnim prekidom rada X servera."

#: main.cpp:62
msgid ""
"Sorry, but %1\n"
"does not seem to be an image file\n"
"Please use files with these extensions:\n"
"%2"
msgstr ""
"Izvinite, ali %1\n"
"izgleda da nije fajl sa slikom.\n"
"Molim vas koristite fajlove sledećih ekstenzija:\n"
"%2"

#: main.cpp:87
msgid "A&ppearance"
msgstr "&Izgled"

#: main.cpp:91
msgid "&Font"
msgstr "&Font"

#: main.cpp:95
msgid "&Background"
msgstr "&Pozadina"

#: main.cpp:99
msgid "&Sessions"
msgstr "&Sesija"

#: main.cpp:103
msgid "&Users"
msgstr "&Korisnici"

#: main.cpp:108
msgid "Con&venience"
msgstr "Udo&bnost"

#: main.cpp:123
#, fuzzy
msgid ""
"<h1>Login Manager</h1> In this module you can configure the various aspects "
"of the KDE Login Manager.  This includes the look and feel as well as the "
"users that can be selected for login. Note that you can only make changes if "
"you run the module with superuser rights. If you haven't started the KDE "
"Control Center with superuser rights (which is the completely right thing to "
"do, by the way), click on the <em>Modify</em> button to acquire superuser "
"rights. You will be asked for the superuser password.<h2>Appearance</h2> On "
"this tab page, you can configure how the Login Manager should look, which "
"language it should use, and which GUI style it should use. The language "
"settings made here have no influence on the user's language "
"settings.<h2>Font</h2>Here you can choose the fonts that the Login Manager "
"should use for various purposes like greetings and user names. "
"<h2>Background</h2>If you want to set a special background for the login "
"screen, this is where to do it.<h2>Sessions</h2> Here you can specify which "
"types of sessions the Login Manager should offer you for logging in and who "
"is allowed to shutdown/reboot the machine.<h2>Users</h2>On this tab page, "
"you can select which users the Login Manager will offer you for logging "
"in.<h2>Convenience</h2> Here you can specify a user to be logged in "
"automatically, users not needing to provide a password to log in, and other "
"features ideal for lazy people. ;-)"
msgstr ""
"<h1>Menađer prijava</h1> U ovom modulu možete podesiti razne aspekte KDE "
"Menađera prijava. U ovo su uključeni izgled i osećaj kao i korisnici koji "
"mogu bitiodabrani za prijavljivanje. Primetite da možete praviti izmene "
"samo ako ste modul startovali sa nadkorisničkim privilegijama. Ako niste "
"startovali KDE Kontrolni centar sa nadkorisničkim privilegijama (što je, "
"uzgred, kompletno ispravna stvar) kliknite na dugme <em>Promeni</em> da ih "
"zatražite. Biće od vas traženo da unsete lozinku "
"nadkorisnika.<h2>Izgled</h2> Na ovoj strani možete pogledati kako će "
"Menadžer prijava izgledati, koji će jezik i koji GUI stil koristiti. "
"Jezička podešavanja ovde napravljena nemaju uticaj na korisnička jezička "
"podešavanja.<h2>Font</h2>Ovde možete izabrati fontove koje će Menadžer "
"prijava koristiti za razne namene, kao što su dobrodošlica i korisnička "
"imena. <h2>Pozadina</h2>Ako želite specijalnu pozadinu ekrana za "
"prijavljivanje, ovde ćete to podesiti.<h2>Sesije</h2>Ove možete navesti "
"koje će vam tipove sesija Menadžer prijava ponuditi pri prijavljivanju i "
"kome je dopušteno da ugasi/restartuje računar.<h2>Korisnici</h2>Na ovoj "
"strani možete odabrati korisnike koje će vam Menadžer prijava ponuditi "
"pri prijavljivanju na sistem.<h2>Pogodnost</h2>Ovde možete navesti "
"korisnike koji će biti prijavljeni automatski, korisnike koji ne moraju da "
"navedu lozinku pri prijavljivanju, i ostale pogodnosti idealne za lenje "
"ljude. ;-)"

#: bgdialogs.cpp:45
#, fuzzy
msgid "Select Background Program"
msgstr "Pozadinski program"

#: bgdialogs.cpp:51
#, fuzzy
msgid "Select Background Program:"
msgstr "Pozadinski program"

#: bgdialogs.cpp:58
msgid "Program"
msgstr "Program"

#: bgdialogs.cpp:59 bgdialogs.cpp:362
msgid "Comment"
msgstr "Komentar"

#: bgdialogs.cpp:60
msgid "Refresh"
msgstr "Osveži"

#: bgdialogs.cpp:83 bgdialogs.cpp:385 bgdialogs.cpp:700
#, fuzzy
msgid "&Add..."
msgstr "&Dodaj"

#: bgdialogs.cpp:89 bgdialogs.cpp:391
msgid "&Modify..."
msgstr ""

#: bgdialogs.cpp:135
msgid "%1 min."
msgstr "%1 min"

#: bgdialogs.cpp:170
msgid ""
"Unable to remove the program! The program is global\n"
"and can only be removed by the System Administrator.\n"
msgstr ""
"Ne mogu da uklonim program! Program je zajednički\n"
"i može ga ukloniti samo administrator sistema.\n"

#: bgdialogs.cpp:172
#, fuzzy
msgid "Cannot remove program"
msgstr "Ne mogu da uklonim program"

#: bgdialogs.cpp:176
msgid "Are you sure you want to remove the program `%1'?"
msgstr "Da li ste sigurni da želite da uklonite program `%1'?"

#: bgdialogs.cpp:227
#, fuzzy
msgid "Configure Background Program"
msgstr "Pozadinski program"

#: bgdialogs.cpp:235 bgdialogs.cpp:540
#, fuzzy
msgid "&Name:"
msgstr "&Ime"

#: bgdialogs.cpp:241 bgdialogs.cpp:546
#, fuzzy
msgid "&Comment:"
msgstr "&Komentar"

#: bgdialogs.cpp:247
#, fuzzy
msgid "&Command:"
msgstr "&Komanda"

#: bgdialogs.cpp:253
#, fuzzy
msgid "&Preview cmd:"
msgstr "&Pregledaj komandu"

#: bgdialogs.cpp:259
#, fuzzy
msgid "&Executable:"
msgstr "&Izvršni"

#: bgdialogs.cpp:265
#, fuzzy
msgid "&Refresh time:"
msgstr "&Vreme osvežavanja"

#: bgdialogs.cpp:270 bgdialogs.cpp:673
msgid " minutes"
msgstr " minuta"

#: bgdialogs.cpp:277
msgid "New Command"
msgstr "Nova komanda"

#: bgdialogs.cpp:280
msgid "New Command <%1>"
msgstr "Nova komanda <%1>"

#: bgdialogs.cpp:307 bgdialogs.cpp:601
msgid ""
"You did not fill in the `Name' field.\n"
"This is a required field."
msgstr ""
"Niste popunili polje `Ime'.\n"
"Ovo polje je neophodno."

#: bgdialogs.cpp:315
msgid ""
"There is already a program with the name `%1'.\n"
"Do you want to overwrite it?"
msgstr ""
"Već postoji program sa imenom `%1'.\n"
"Da li želite da ga prepišete?"

#: bgdialogs.cpp:322
msgid ""
"You did not fill in the `Executable' field.\n"
"This is a required field."
msgstr ""
"Niste popunili polje `Izvršni fajl'.\n"
"Ovo polje je neophodno."

#: bgdialogs.cpp:327
msgid ""
"You did not fill in the `Command' field.\n"
"This is a required field."
msgstr ""
"Niste popunili polje `Komanda'.\n"
"Ovo polje je neophodno."

#: bgdialogs.cpp:348
#, fuzzy
msgid "Select Background Pattern"
msgstr "Pozadinski program"

#: bgdialogs.cpp:354
#, fuzzy
msgid "Select Pattern:"
msgstr "Nova šara"

#: bgdialogs.cpp:363
#, fuzzy
msgid "Preview"
msgstr "&Pregledaj komandu"

#: bgdialogs.cpp:480
msgid ""
"Unable to remove the pattern! The pattern is global\n"
"and can only be removed by the System Administrator.\n"
msgstr ""
"Ne mogu da uklonim šaru! Šara je deo sistema\n"
"i može je ukloniti samo administrator sistema.\n"

#: bgdialogs.cpp:482
#, fuzzy
msgid "Cannot remove pattern"
msgstr "Ne mogu da uklonim šaru"

#: bgdialogs.cpp:486
msgid "Are you sure you want to remove the pattern `%1'?"
msgstr "Da li ste sigurni da želite da uklonite šaru `%1'?"

#: bgdialogs.cpp:533
msgid "Configure Background Pattern"
msgstr ""

#: bgdialogs.cpp:552
#, fuzzy
msgid "&Image:"
msgstr "&Slika"

#: bgdialogs.cpp:559
#, fuzzy
msgid "&Browse..."
msgstr "Pre&gledaj"

#: bgdialogs.cpp:565
msgid "New Pattern"
msgstr "Nova šara"

#: bgdialogs.cpp:568
msgid "New Pattern <%1>"
msgstr "Nova šara <%1>"

#: bgdialogs.cpp:609
msgid ""
"There is already a pattern with the name `%1'.\n"
"Do you want to overwrite it?"
msgstr ""
"Već postoji šara sa imenom `%1'.\n"
"Da li želite da je prepišete?"

#: bgdialogs.cpp:616
msgid ""
"You did not fill in the `Image' field.\n"
"This is a required field."
msgstr ""
"Niste popunili polje `Slika'.\n"
"Ovo polje je neophodno."

#: bgdialogs.cpp:656
#, fuzzy
msgid "Configure Wallpapers"
msgstr "Bez slike pozadine"

#: bgdialogs.cpp:668
#, fuzzy
msgid "&Interval:"
msgstr "&Interval"

#: bgdialogs.cpp:679
msgid "In Order"
msgstr "Po redosledu"

#: bgdialogs.cpp:680
msgid "Random"
msgstr "Slučajno"

#: bgdialogs.cpp:686
msgid "You can select files and directories below:"
msgstr "Ispod možete selektovati fajlove i direktorijume:"

#: bgdialogs.cpp:711
#, fuzzy
msgid "Select"
msgstr "Izaberite fontove"
