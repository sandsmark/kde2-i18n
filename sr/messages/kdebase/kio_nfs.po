# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2001-06-10 16:06+0200\n"
"PO-Revision-Date: 2001-06-03 12:48 CEST\n"
"Last-Translator: Slobodan Marković <twiddle@eunet.yu>\n"
"Language-Team: Serbian <kde-yu@kde.org.yu>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.2alpha\n"

#: kio_nfs.cpp:986
msgid "An RPC error occurred."
msgstr "Došlo je do RPC greške."

#: kio_nfs.cpp:1030
msgid "No space left on device"
msgstr "Nema više prostora na uređaju"

#: kio_nfs.cpp:1033
msgid "Read only file system"
msgstr "Fajl sistem je montiran samo za čitanje"

#: kio_nfs.cpp:1036
msgid "Filename too long"
msgstr "Naziv fajla je previše dugačak"

#: kio_nfs.cpp:1043
msgid "Disk quota exceeded"
msgstr "Prekoračena je disk kvota"
